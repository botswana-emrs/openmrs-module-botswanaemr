/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.web.controller;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.openmrs.api.LocationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.PersonService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistryService;
import org.openmrs.module.botswanaemr.api.impl.BotswanaPersonRegistryServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class BotswanaEmrRestControllerTest {
	
	private MockMvc mockMvc;
	
	@InjectMocks
	private BotswanaEmrRestController botswanaEmrRestController;
	
	@Mock
	private BotswanaEmrService botswanaEmrService;
	
	@Mock
	private BotswanaPersonRegistryService botswanaPersonRegistryService;
	
	// mock for patientservice
	@Mock
	private PatientService patientService;
	
	// mock for personService
	@Mock
	private PersonService personService;
	
	// mock for locationService
	@Mock
	private LocationService locationService;
	
	private String personIdNumber;
	
	private String missingPersonIdNumber;
	
	@Before
	public void startService() {
		Mockito.when(Context.getRegisteredComponent("botswanaemr.BotswanaPersonRegistryService",
		    BotswanaPersonRegistryServiceImpl.class))
		        .thenReturn((BotswanaPersonRegistryServiceImpl) botswanaPersonRegistryService);
		
		MockitoAnnotations.openMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(botswanaEmrRestController).build();
		personIdNumber = "235262727";
		missingPersonIdNumber = "xxxxxxxxx";
	}
	
	@Test
	@Ignore
	public void testSearchPatientInBirthAndDeathRegistry_shouldPassIfMatchIsFound() throws Exception {
		MvcResult mvcResult = mockMvc
		        .perform(get(String.format("/rest/v1/botswanaemr/registry/%s", missingPersonIdNumber))
		                .contentType(MediaType.APPLICATION_JSON))
		        .andExpect(status().isOk()).andExpect(jsonPath("$.status").exists()).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("200"), equalTo(true));
		assertThat(content.contains("235262727"), equalTo(true));
	}
	
	@Test
	@Ignore
	public void testSearchPatientInBirthAndDeathRegistry_shouldFailIfMatchIsNotFound() throws Exception {
		MvcResult mvcResult = mockMvc
		        .perform(get(String.format("/rest/v1/botswanaemr/registry/%s", personIdNumber))
		                .contentType(MediaType.APPLICATION_JSON))
		        .andExpect(status().isOk()).andExpect(jsonPath("$.status").exists()).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		
		assertThat(content.contains("404"), equalTo(true));
		assertThat(content.contains("Record not found"), equalTo(true));
		
	}
	
	@Ignore
	@Test
	public void testSearchPatientInRegistry_shouldFailIfMatchIsNotFound() throws Exception {
		MvcResult mvcResult = mockMvc
		        .perform(get(String.format("/rest/v1/botswanaemr/registry/%s/%s", "passportNumber", personIdNumber))
		                .contentType(MediaType.APPLICATION_JSON))
		        .andExpect(status().isOk()).andExpect(jsonPath("$.status").exists()).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		
		assertThat(content.contains("404"), equalTo(true));
		assertThat(content.contains("Record not found"), equalTo(true));
		
	}
	
	@Test
	@Ignore
	public void testSavePatientRegistrationDetails() throws Exception {
		String requestJson = "{\"patientType\":\"Regular\",\"idType\":\"nationalIdNumber\",\"idNumber\":\"123456789\",\"fullname\":\"Wajakoyah Prof Julius\",\"gender\":\"Male\",\"dob\":\"1970-05-15\",\"email\":\"accounts@intellisoftkenya.com\",\"contactNumber\":\"0723862719\",\"occupation\":\"Engineer\",\"employerName\":\"Kenneth O Ochieng\",\"homeaddress\":\"Kisumu\",\"nok\":[{\"nokIdNumber\":\"2222222222\",\"nokFullname\":\"Jay Rodrigues Fe\",\"nokRelationship\":\"Relative\",\"nokContact\":\"0723862719\",\"nokEmail\":\"\"},{\"nokIdNumber\":\"235163321\",\"nokFullname\":\"Kenneth O Ochieng\",\"nokContact\":\"0723862719\",\"nokEmail\":\"\"}],\"altContactNumber\":\"0723862719\"}\n";
		
		MvcResult mvcResult = mockMvc
		        .perform(post(String.format("/rest/v1/botswanaemr/registration")).contentType(MediaType.APPLICATION_JSON)
		                .content(requestJson))
		        .andExpect(status().isOk()).andExpect(jsonPath("$.status").exists()).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("200"), equalTo(true));
		assertThat(content.contains("Success"), equalTo(true));
		
	}
	
	@Ignore
	@Test
	public void testHandleBatchRegistrationUpdate_shouldReturnSuccessWhenAllPatientsAreUpdated() throws Exception {
		String requestJson = "[{\"idNumber\":\"123456789\",\"migrationPatientId\":\"1\",\"migrationFacilityId\":\"1001\",\"location\":\"Location1\"}]";
		
		MvcResult mvcResult = mockMvc
		        .perform(put("/rest/v1/botswanaemr/batchRegistration").contentType(MediaType.APPLICATION_JSON)
		                .content(requestJson))
		        .andExpect(status().isOk()).andExpect(jsonPath("$.status").value(HttpStatus.OK.toString()))
		        .andExpect(jsonPath("$.response").value("Success")).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("123456789"), equalTo(true));
	}
	
	@Ignore
	@Test
	public void testHandleBatchRegistrationUpdate_shouldReturnPartialSuccessWhenSomePatientsFailToUpdate() throws Exception {
		String requestJson = "[{\"idNumber\":\"123456789\",\"migrationPatientId\":\"1\",\"migrationFacilityId\":\"1001\",\"location\":\"Location1\"},"
		        + "{\"idNumber\":\"987654321\",\"migrationPatientId\":\"2\",\"migrationFacilityId\":\"1002\",\"location\":\"Location2\"}]";
		
		MvcResult mvcResult = mockMvc
		        .perform(put("/rest/v1/botswanaemr/batchRegistration").contentType(MediaType.APPLICATION_JSON)
		                .content(requestJson))
		        .andExpect(status().isMultiStatus())
		        .andExpect(jsonPath("$.status").value(HttpStatus.MULTI_STATUS.toString()))
		        .andExpect(jsonPath("$.response").value("Partial Success")).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("123456789"), equalTo(true));
		assertThat(content.contains("987654321"), equalTo(true));
	}
	
	@Ignore
	@Test
	public void testHandleBatchRegistrationUpdate_shouldReturnFailedWhenAllPatientsFailToUpdate() throws Exception {
		String requestJson = "[{\"idNumber\":\"123456789\",\"migrationPatientId\":\"1\",\"migrationFacilityId\":\"1001\",\"location\":\"Location1\"},"
		        + "{\"idNumber\":\"987654321\",\"migrationPatientId\":\"2\",\"migrationFacilityId\":\"1002\",\"location\":\"Location2\"}]";
		
		MvcResult mvcResult = mockMvc
		        .perform(put("/rest/v1/botswanaemr/batchRegistration").contentType(MediaType.APPLICATION_JSON)
		                .content(requestJson))
		        .andExpect(status().isBadRequest()).andExpect(jsonPath("$.status").value(HttpStatus.BAD_REQUEST.toString()))
		        .andExpect(jsonPath("$.response").value("Failed")).andReturn();
		
		String content = mvcResult.getResponse().getContentAsString();
		assertThat(content.contains("123456789"), equalTo(true));
		assertThat(content.contains("987654321"), equalTo(true));
	}
}
