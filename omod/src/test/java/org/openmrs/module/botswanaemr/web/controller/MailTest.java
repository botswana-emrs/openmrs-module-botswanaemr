/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.web.controller;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class MailTest {
	
	private BotswanaEmrService botswanaEmrService = null;
	
	@Before
	public void startService() {
		//        botswanaEmrService = Context.getService(BotswanaEmrService.class);
	}
	
	@Test
	public void sendTemplateMail() throws Exception {
		
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", LocalDate.now().minusWeeks(1).toString());
		params.put("endDate", LocalDate.now().toString());
		params.put("signature", "Powered By BotswanaEMR");
		params.put("missedAppointmentsLink", "http://localhost:8600/openmrs/botswanaemr/appointments/patientFollowup.page");
		params.put("clinicDays", getClinicDaysList());
		String html = getFreemarkerTemplateContent("missedAppointments.ftlh", params);
		sendGeneralEmail("vajeh56251@ezgiant.com", "Testing general mail", html);
	}
	
	public void sendGeneralEmail(String toEmail, String subject, String body) {
		try {
			MimeMessage msg = msg();
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			Log.info("Message is ready :" + msg);
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/HTML;");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport.send(msg);
			Log.info("EMail Sent Successfully!!");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendAttachmentEmail(BotswanaEmrEmailReportConfig config, String filename) {
		try {
			MimeMessage msg = msg();
			msg.setSubject(config.getSubject(), "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(config.getMailTo(), false));
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(config.getMailContent());
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport.send(msg);
			Log.info("EMail Sent Successfully with attachment!!");
		}
		catch (MessagingException e) {
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private MimeMessage msg() throws MessagingException, UnsupportedEncodingException {
		MimeMessage msg = new MimeMessage(getMailSession());
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("format", "flowed");
		msg.addHeader("Content-Transfer-Encoding", "8bit");
		msg.setFrom(new InternetAddress("no_reply@example.com", "Botswana-EMR"));
		msg.setReplyTo(InternetAddress.parse("no_reply@example.com", false));
		
		return msg;
	}
	
	public Session getMailSession() {
		Session session = null;
		Properties p = new Properties();
		p.put("mail.transport.protocol", "smtp");
		p.put("mail.smtp.host", "sandbox.smtp.mailtrap.io");
		p.put("mail.smtp.port", "2525"); // mail.smtp_port
		p.put("mail.smtp.auth", "true");
		p.put("mail.debug", "false");
		p.put("mail.smtp.starttls.enable", "true");
		
		final String user = "154c1cf032b9fc";
		final String password = "e747ad3ee648c7";
		
		if (StringUtils.isNotBlank(user) && StringUtils.isNotBlank(password)) {
			session = Session.getInstance(p, new Authenticator() {
				
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, password);
				}
			});
		} else {
			session = Session.getInstance(p);
		}
		
		return session;
	}
	
	public String getFreemarkerTemplateContent(String template, Map<String, Object> model)
	        throws IOException, TemplateException {
		Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
		configuration.setClassForTemplateLoading(this.getClass(), "/mailtemplates");
		StringWriter stringWriter = new StringWriter();
		configuration.getTemplate(template).process(model, stringWriter);
		return stringWriter.getBuffer().toString();
	}
	
	public List<SimpleObject> getClinicDaysList() {
		List<SimpleObject> days = new ArrayList<>();
		SimpleObject so = new SimpleObject();
		so.put("date", new Date());
		so.put("scheduled", 34);
		so.put("completed", 30);
		so.put("pending", 4);
		days.add(so);
		
		return days;
		
	}
}
