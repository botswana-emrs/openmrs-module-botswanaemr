<?xml version="1.0" encoding="UTF-8"?>
<!--
    This Source Code Form is subject to the terms of the Mozilla Public License,
    v. 2.0. If a copy of the MPL was not distributed with this file, You can
    obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
    the terms of the Healthcare Disclaimer located at http://openmrs.org/license.

    Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
    graphic logo is a trademark of OpenMRS Inc.
-->

<module configVersion="1.2">

    <!-- Base Module Properties -->
    <id>${project.parent.artifactId}</id>
    <name>${project.parent.name}</name>
    <version>${project.parent.version}</version>
    <package>org.openmrs.module.botswanaemr</package>
    <author>IntelliSOFT</author>
    <description>
        ${project.parent.description}
    </description>

    <activator>org.openmrs.module.botswanaemr.BotswanaEmrActivator</activator>

    <!-- <updateURL>https://modules.openmrs.org/modules/download/@MODULE_ID@/update.rdf</updateURL> -->
    <!-- /Base Module Properties -->

    <require_version>${openMRSVersion}</require_version>

    <require_modules>
        <require_module version="${appframeworkVersion}}">org.openmrs.module.appframework</require_module>
        <require_module>org.openmrs.module.uiframework</require_module>
        <require_module>org.openmrs.module.uicommons</require_module>
        <require_module>org.openmrs.module.appui</require_module>
        <require_module>org.openmrs.module.emrapi</require_module>
        <require_module>org.openmrs.module.adminui</require_module>
        <require_module>org.openmrs.module.idgen</require_module>
        <require_module>org.openmrs.module.metadatadeploy</require_module>
        <require_module>org.openmrs.module.referenceapplication</require_module>
        <require_module version="${reportingVersion}">org.openmrs.module.reporting</require_module>
        <require_module version="${reportingcompatibilityVersion}">org.openmrs.module.reportingcompatibility</require_module>
        <require_module version="${calculationVersion}">org.openmrs.calculation</require_module>
        <require_module version="${coraappVersion}">org.openmrs.module.coreapps</require_module>
        <require_module version="${patientqueueingVersion}">org.openmrs.module.patientqueueing</require_module>
        <require_module>org.openmrs.module.registrationcore</require_module>
        <require_module version="${formentryappVersion}">org.openmrs.module.formentryapp</require_module>
        <require_module version="${htmlformentryVersion}">org.openmrs.module.htmlformentry</require_module>
        <require_module version="${htmlformentryuiVersion}">org.openmrs.module.htmlformentryui</require_module>
        <require_module version="${appointmentschedulingVersion}">org.openmrs.module.appointmentscheduling</require_module>
        <require_module version="${locationbasedaccessVersion}">org.openmrs.module.locationbasedaccess</require_module>
        <require_module>org.openmrs.module.botswanaemrInventory</require_module>
        <require_module version="${initializerVersion}" >org.openmrs.module.initializer</require_module>
        <require_module>org.openmrs.module.auditlog</require_module>

    </require_modules>

    <!-- Extensions -->
    <extension>
        <point>org.openmrs.admin.list</point>
        <class>org.openmrs.module.botswanaemr.extension.html.AdminList</class>
    </extension>

    <aware_of_modules>
        <aware_of_module>org.openmrs.module.legacyui</aware_of_module>
    </aware_of_modules>

    <privilege>
        <name>App:botswanaemr.registrationAdminDashboard</name>
        <description>Able to access the patient Registration admin dashboard</description>
    </privilege>
    <privilege>
        <name>App:botswanaemr.auxilliaryNurseDashboard</name>
        <description>Able to access the auxilliary nurse dashboard</description>
    </privilege>
    <!-- AOP
    <advice>
        <point>org.openmrs.api.FormService</point>
        <class>@MODULE_PACKAGE@.advice.DuplicateFormAdvisor</class>
    </advice>
     /AOP -->


    <!-- Required Privileges
    <privilege>
        <name>Form Entry</name>
        <description>Allows user to access Form Entry pages/functions</description>
    </privilege>
     /Required Privileges -->


    <!-- Required Global Properties -->
    <globalProperty>
        <property>botswanaemr.hospital</property>
        <defaultValue>Botswana Hospital</defaultValue>
        <description>
            Hospital name that implement this application
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.fetchSize</property>
        <defaultValue>10</defaultValue>
        <description>
            Configurable value that can be fetched in the trends section
        </description>
    </globalProperty>

    <globalProperty>
        <property>order.drugDispensingUnitsConceptUuid</property>
        <defaultValue>162402AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</defaultValue>
        <description>
           Drug dispensing units default concept uuid
        </description>
    </globalProperty>
    <globalProperty>
        <property>order.drugDosingUnitsConceptUuid</property>
        <defaultValue>162384AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</defaultValue>
        <description>
            Drug dosing units default concept uuid
        </description>
    </globalProperty>
    <globalProperty>
        <property>order.drugRoutesConceptUuid</property>
        <defaultValue>2838de1f-458d-48cc-8c59-5f64d74eb5c0</defaultValue>
        <description>
            Drug routes default concept uuid
        </description>
    </globalProperty>
    <globalProperty>
        <property>order.durationUnitsConceptUuid</property>
        <defaultValue>1732AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</defaultValue>
        <description>
            Drug duration units concept uuid
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.minAgeForRegistrationFee</property>
        <defaultValue>5</defaultValue>
        <description>
            Minimum age eligible for paying registration fees
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.maxAgeForRegistrationFee</property>
        <defaultValue>65</defaultValue>
        <description>
            Maximum age eligible for paying registration fees
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.defaultBillableAmountForLocals</property>
        <defaultValue>5</defaultValue>
        <description>
            Default billable amount for locals
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.defaultBillableAmountForForeigners</property>
        <defaultValue>50</defaultValue>
        <description>
            Default billable amount for foreigners
        </description>
    </globalProperty>
    <globalProperty>
        <property>locationbasedaccess.locationAttributeUuid</property>
        <defaultValue>0a93cbc6-5d65-4886-8091-47a25d3df944</defaultValue>
        <description>UUID for the LocationAttribute-type to store  location information</description>
    </globalProperty>

    <globalProperty>
        <property>botswanaemr.DSTBDrugs</property>
        <defaultValue>
            e0ae875d-15d4-4777-b303-026b48c8bf40,
            fbb7afc3-6a1a-4b3b-9428-fb8edf628af5,
            6cd7233e-1105-466c-b3cf-4cf7cf0be81b,
            d12de6ea-20dd-4f33-92a4-5fc0acd66825,
            038a6cc3-07a3-4396-a94e-ebdba61b6e28,
            02a51202-8e00-493f-8cd7-db2eb98a39dc,
            72681AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        </defaultValue>
        <description>DS-TB drugs</description>
    </globalProperty>
    
    <globalProperty>
        <property>botswanaemr.appointments.fakePatient</property>
        <defaultValue>ec385396-d714-4230-85a1-bf6645099edf</defaultValue>
        <description>UUID for the fake-patient to create task appointments for provider</description>
    </globalProperty>

    <globalProperty>
        <property>botswanaemr.identifier.system.passport</property>
        <defaultValue>http://moh.bw.org/ext/identifier/ppn</defaultValue>
        <description>System Identifier for Passport number</description>
    </globalProperty>

    <globalProperty>
        <property>botswanaemr.identifier.system.omang</property>
        <defaultValue>http://moh.bw.org/ext/identifier/omang</defaultValue>
        <description>System Identifier for OMANG number</description>
    </globalProperty>

    <globalProperty>
        <property>botswanaemr.identifier.system.birth-certificate</property>
        <defaultValue>http://moh.bw.org/ext/identifier/bcn</defaultValue>
        <description>System Identifier for Birth certificate number</description>
    </globalProperty>

    <mappingFiles>
        PaymentMethod.hbm.xml
        ServiceProvider.hbm.xml
        Lab.hbm.xml
        LabTest.hbm.xml
        CaseLinkage.hbm.xml

        <!--regimen mappings-->
<!--        RegimenLine.hbm.xml-->
<!--        Regimen.hbm.xml-->
<!--        RegimenComponent.hbm.xml-->
    </mappingFiles>
    <globalProperty>
        <property>botswanaemr.mpiSearchEnabled</property>
        <defaultValue>false</defaultValue>
        <description>
            Enable/Disable MPI Search
        </description>
    </globalProperty>

    <globalProperty>
        <property>botswanaemr.mpiServerUrl</property>
        <defaultValue>https://core.moh.org.bw</defaultValue>
        <description>
            MPI Server URL
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.mpiServerUsername</property>
        <defaultValue>postman</defaultValue>
        <description>
            MPI Server Authentication Username
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.mpiServerPassword</property>
        <defaultValue>postman</defaultValue>
        <description>
            MPI Server Authentication Password
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.shrBaseUrl</property>
        <defaultValue>http://13.247.70.187:5001/SHR/fhir</defaultValue>
        <description>
            SHR Server Base URL
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.shrUserName</property>
        <defaultValue>bemr-client</defaultValue>
        <description>
            SHR Server Authentication User Name
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.shrPassword</property>
        <defaultValue>postman</defaultValue>
        <description>
            SHR Server Authentication Password
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.mflApiBaseUrl</property>
        <defaultValue>https://mflld.gov.org.bw/api/v1</defaultValue>
        <description>
            MFL API Base URL
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.mflApiAuthorizationToken</property>
        <defaultValue>eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrb2NoaWVuZ0BpbnRlbGxpc29mdGtlbnlhLmNvbSIsImNyZWF0ZWQiOjE2NDg3NDgwNDU2NzgsInJvbGVzIjpbeyJjcmVhdG9yIjpudWxsLCJjcmVhdGVkIjpudWxsLCJ1cGRhdGVyIjpudWxsLCJ1cGRhdGVkIjpudWxsLCJpZCI6ImRhMmZkOTE2LWQxMTMtNDYwMi05MWNhLTNlNWYzMzFmMWIwNiIsInJlZmVyZW5jZSI6bnVsbCwibmFtZSI6IkRldmVsb3BlciIsImRlc2NyaXB0aW9uIjoiRGV2ZWxvcGVyLCBhY2Nlc3MgTUZMIG9wZW4gYXBpIGZvciBpbnRlcm9wZXJhYmlsaXR5IiwiYXV0aG9yaXR5R3JvdXBzIjpbeyJjcmVhdG9yIjpudWxsLCJjcmVhdGVkIjpudWxsLCJ1cGRhdGVyIjpudWxsLCJ1cGRhdGVkIjpudWxsLCJpZCI6ImVmODgxNDdhLTg4NTUtNDg4ZC1hOGE1LWJlNGRlOTE3MTQyOCIsInJlZmVyZW5jZSI6bnVsbCwibmFtZSI6IkRFVkVMT1BFUl9BTEwiLCJkZXNjcmlwdGlvbiI6IkNhbiBhY2Nlc3MgYWxsIHRoZSBvcGVuIE1GTCBBcGlzIn1dfV0sImV4cCI6MzUzMTQ3NDgwNDUsImp0aSI6IjkxYTZkZTA5LWIwZjktMTFlYy1iZTU4LWNkMDY2NTJmNWY5MCJ9.Ofmxwd__T8OEvbdXsenlyeWxlPBkwBcaB8-qBhmF05Xe3XF7_zziZaafGMu9PumdWgnHLEW3Eh0MR0ulvG_zlA</defaultValue>
        <description>
            MFL API Authorization Token
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.instanceUrl</property>
        <defaultValue>https://qa.emr.gov.org.bw/openmrs/</defaultValue>
        <description>
            BotswanaEMR instance URL
        </description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.patientFollowupUrl</property>
        <defaultValue>http://localhost:8600/openmrs/botswanaemr/appointments/patientFollowup.page</defaultValue>
        <description>
            Patient appointment followup url
        </description>
    </globalProperty>
    <globalProperty>
        <property>SchedulerMarksMissed</property>
        <defaultValue>false</defaultValue>
        <description> when it is turned on, scheduler marks scheduled appointments that are past appointment date as missed</description>
    </globalProperty>
    <globalProperty>
        <property>SchedulerMarksMissed</property>
        <defaultValue>false</defaultValue>
        <description> when it is turned on, scheduler marks scheduled appointments that are past appointment date as missed</description>
    </globalProperty>
    <globalProperty>
            <property>AppointmentCloserDaysInterval</property>
            <defaultValue>1</defaultValue>
            <description> A backward interval day count from set appointment date to close appointments</description>
        </globalProperty>
    <globalProperty>
        <property>botswanaemr.oralQuickStockItemCode</property>
        <defaultValue>166754</defaultValue>
        <description>Stock Item Code for the self testing Kits - Oral Quick</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.oralQuickConceptUuid</property>
        <defaultValue>166861AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA</defaultValue>
        <description>Concept Id for the self testing Kits - Oral Quick</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.validateVmmcScreeningOutcome</property>
        <defaultValue>true</defaultValue>
        <description>Validate VMMC screening outcome. Use the screening outcome to determine the forms to display</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.cotrimoxazoleDrugUuid</property>
        <defaultValue>d977a995-48f2-4a39-b269-8491b2f15f71</defaultValue>
        <description>UUID FOR Cotrimoxazole drug</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.prepDrugUuid</property>
        <defaultValue>a43fe97c-d514-46a0-8783-498e409fe9dd</defaultValue>
        <description>UUID FOR Prep drug</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.pepDrugUuid</property>
        <defaultValue>410778cc-46ec-4f2b-ad49-14b3fe478eea</defaultValue>
        <description>UUID FOR Pep drug</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.tptDrugUuid</property>
        <defaultValue>38828cdd-18da-4258-b56a-38a95946bec3</defaultValue>
        <description>UUID FOR TPT drug</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.defaultDateFormat</property>
        <defaultValue>dd/MM/yyyy</defaultValue>
        <description>The default date format to be used across the system.</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.openCRSavePatientUrl</property>
        <defaultValue>https://openhimcore.hie-botswana.jembi.cloud/CR/validate</defaultValue>
        <description>OpenCR Save Patient URL</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.openCRServerFetchurl</property>
        <defaultValue>https://openhimcore.hie-botswana.jembi.cloud/CR/validate</defaultValue>
        <description>OpenCR Fetch Patient URL</description>
    </globalProperty>
    <globalProperty>
        <property>botswanaemr.defaultTaskId</property>
        <defaultValue>154</defaultValue>
        <description>default Task Id</description>
    </globalProperty>
    <globalProperties>
        <property>
            <name>botswanaemr.cr.maxRetries</name>
            <defaultValue>3</defaultValue>
            <description>Maximum number of retry attempts for posting to the CR</description>
        </property>
    </globalProperties>
    <!-- Internationalization -->
    <!-- All message codes should start with @MODULE_ID@.* -->
    <messages>
        <lang>en</lang>
        <file>messages.properties</file>
    </messages>
    <messages>
        <lang>fr</lang>
        <file>messages_fr.properties</file>
    </messages>
    <messages>
        <lang>es</lang>
        <file>messages_es.properties</file>
    </messages>
    <!-- /Internationalization -->

</module>
