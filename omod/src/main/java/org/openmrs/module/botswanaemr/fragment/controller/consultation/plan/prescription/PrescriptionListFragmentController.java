/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.plan.prescription;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.LabOrderService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DRUG_ORDER_ENCOUNTER_TYPE;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncountersByPatient;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.persistEncounter;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortEncountersByEncounterDatetime;

@Slf4j
public class PrescriptionListFragmentController {
	
	@SneakyThrows
	public void controller(FragmentModel fragmentModel,
	        @FragmentParam(value = "patientId", required = false) Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit,
	        @FragmentParam(value = "encounterId", required = false) Encounter encounter, UiSessionContext uiSessionContext) {
		
		LabOrderService labOrderService = Context.getRegisteredComponent("botswana.emr.labOrderService",
		    LabOrderService.class);
		if (encounter == null) {
			// Get existing drug_order encounter for this visit
			encounter = BotswanaEmrUtils.getDrugOrderEncounter(patient, visit, uiSessionContext.getSessionLocation());
		}
		
		List<Order> orders = labOrderService.getAllActiveMedications(patient);
		List<DrugOrder> drugOrders = new ArrayList<>();
		orders.stream().filter(o -> o instanceof DrugOrder).forEach(order -> drugOrders.add((DrugOrder) order));
		
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("encounter", encounter);
		fragmentModel.addAttribute("drugOrders", drugOrders);
		
		List<PrescriptionMapper> prescriptionMappers = new ArrayList<>();
		drugOrders.forEach(drugOrder -> {
			PrescriptionMapper prescriptionMapper = new PrescriptionMapper();
			ObjectMapper mapper = new ObjectMapper();
			try {
				if (drugOrder.getInstructions() != null) {
					DosingInstructions instructions = mapper.readValue(drugOrder.getInstructions(),
					    DosingInstructions.class);
					List<String> builder = new ArrayList<>();
					instructions.getData().forEach(d -> {
						builder.add(d.getFrequencyDisplay() + " from " + d.getFromDate() + " to " + d.getToDate());
					});
					builder.add(instructions.getDosingInstructions());
					prescriptionMapper.setInstructions(builder);
				}
			}
			catch (JsonProcessingException e) {
				log.error("Error parsing dosing instructions", e);
			}
			;
			prescriptionMapper.setDosage(drugOrder.getDose() + " " + drugOrder.getDoseUnits().getDisplayString());
			prescriptionMapper.setDrug(BotswanaEmrUtils.fetchAndformatDrugName(drugOrder.getDrug()));
			prescriptionMapper.setRoute(drugOrder.getRoute().getDisplayString());
			Integer duration = drugOrder.getDuration();
			Concept durationUnits = drugOrder.getDurationUnits();
			prescriptionMapper.setRefillDuration(
			    (duration == null ? 0 : duration) + " " + (durationUnits == null ? "" : durationUnits.getDisplayString()));
			prescriptionMapper.setRefillRepeat(String.valueOf(drugOrder.getNumRefills()));
			prescriptionMapper.setUuid(drugOrder.getUuid());
			
			prescriptionMappers.add(prescriptionMapper);
		});
		
		fragmentModel.addAttribute("prescriptionMappers", prescriptionMappers);
		
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		//fragmentModel.addAttribute("drugOrderExtensions", botswanaEmrService.getDrugOrderExtensionsByPatient(patient));
		//log.error("drugOrderExtensions: {}", botswanaEmrService.getDrugOrderExtensionsByPatient(patient));
		
		//Placeholders metadata - add prescription table
		if (!drugOrders.isEmpty()) {
			DrugOrder drugOrder = drugOrders.get(0);
			fragmentModel.addAttribute("prescription",
			    "V-" + encounter.getVisit().getId() + "D-0" + drugOrder.getId() + "EN-0" + encounter.getEncounterId());
			fragmentModel.addAttribute("issuedBy", encounter.getCreator().getDisplayString());
			fragmentModel.addAttribute("issuedOn",
			    DateUtils.getStringDate(encounter.getVisit().getStartDatetime(), "dd-MM-yyyy"));
		} else {
			fragmentModel.addAttribute("prescription", "");
			fragmentModel.addAttribute("issuedBy", "");
			fragmentModel.addAttribute("issuedOn", "");
		}
		
		if (uiSessionContext.getSessionLocation() != null) {
			fragmentModel.addAttribute("hasActiveVisit", hasActiveVisit(patient, uiSessionContext.getSessionLocation()));
		} else {
			fragmentModel.addAttribute("hasActiveVisit", false);
		}
	}
	
	private boolean hasActiveVisit(@NotNull Patient patient, Location location) {
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, location, false);
		return currentPatientVisit != null;
	}
	
	public SimpleObject getPrescriptionList(@RequestParam(value = "patientId") Integer patientId, @RequestParam(value = "visitId", required = false) Integer visitId,
			@SpringBean("patientService") PatientService patientService, @SpringBean("visitService") VisitService visitService, UiSessionContext uiSessionContext) {
		
		LabOrderService labOrderService = Context.getRegisteredComponent("botswana.emr.labOrderService", LabOrderService.class);
		Patient patient = patientService.getPatient(patientId);
		Visit visit = visitService.getVisit(visitId);
		
		Encounter encounter = getDrugOrderEncounter(patient, visit, uiSessionContext.getSessionLocation());
		
		List<Order> orders = labOrderService.getAllActiveMedications(patient);
		List<DrugOrder> drugOrders = orders.stream()
				.filter(order -> order instanceof DrugOrder)
				.map(order -> (DrugOrder) order)
				.collect(Collectors.toList());
		
		List<SimpleObject> prescriptionMappers = drugOrders.stream().map(drugOrder -> {
			PrescriptionMapper mapper = mapToPrescriptionMapper(drugOrder);
			return SimpleObject.create("drug", mapper.getDrug(), "dosage", mapper.getDosage(),
					"route", mapper.getRoute(), "instructions", mapper.getInstructions(),
					"refillDuration", mapper.getRefillDuration(), "refillRepeat", mapper.getRefillRepeat(),
					"uuid", mapper.getUuid());
		}).collect(Collectors.toList());
		
		String prescription = "";
		String issuedBy = "";
		String issuedOn = "";
		
		if (!drugOrders.isEmpty()) {
			DrugOrder firstDrugOrder = drugOrders.get(0);
			prescription = "V-" + encounter.getVisit().getId() + "D-0" + firstDrugOrder.getId() + "EN-0" + encounter.getEncounterId();
			issuedBy = firstDrugOrder.getCreator().getDisplayString();
			issuedOn = DateUtils.getStringDate(encounter.getVisit().getStartDatetime(), "dd-MM-yyyy");
		}
		
		boolean hasActiveVisit = uiSessionContext.getSessionLocation() != null && hasActiveVisit(patient, uiSessionContext.getSessionLocation());
		
		return SimpleObject.create("patientId", patient.getUuid(), "encounterId", encounter.getUuid(),
				"drugOrders", prescriptionMappers, "prescription", prescription, "issuedBy", issuedBy,
				"issuedOn", issuedOn, "hasActiveVisit", hasActiveVisit);
	}
	
	public static Encounter getDrugOrderEncounter(Patient patient, Visit visit, Location location) {
		if (visit == null) {
			visit = BotswanaEmrUtils.getPatientActiveVisit(patient, location, true);
		}
		
		List<Encounter> drugOrderEncounters = getEncountersByPatient(patient,
				Context.getEncounterService().getEncounterTypeByUuid(DRUG_ORDER_ENCOUNTER_TYPE),
				visit.getLocation(), visit.getStartDatetime(), visit.getStopDatetime());
		
		if (drugOrderEncounters.isEmpty()) {
			return persistEncounter(patient, Context.getEncounterService().getEncounterTypeByUuid(DRUG_ORDER_ENCOUNTER_TYPE), location, visit);
		} else {
			return sortEncountersByEncounterDatetime(drugOrderEncounters).get(0);
		}
	}
	
	private PrescriptionMapper mapToPrescriptionMapper(DrugOrder drugOrder) {
		PrescriptionMapper prescriptionMapper = new PrescriptionMapper();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			if (drugOrder.getInstructions() != null) {
				DosingInstructions instructions = mapper.readValue(drugOrder.getInstructions(), DosingInstructions.class);
				List<String> instructionDetails = new ArrayList<>();
				instructions.getData().forEach(data -> instructionDetails.add(data.getFrequencyDisplay()));
				instructionDetails.add(instructions.getDosingInstructions());
				prescriptionMapper.setInstructions(instructionDetails);
			}
		} catch (JsonProcessingException e) {
			log.error("Error parsing dosing instructions", e);
		}
		
		prescriptionMapper.setDosage(drugOrder.getDose() + " " + drugOrder.getDoseUnits().getDisplayString());
		prescriptionMapper.setDrug(BotswanaEmrUtils.fetchAndformatDrugName(drugOrder.getDrug()));
		prescriptionMapper.setRoute(drugOrder.getRoute().getDisplayString());
		
		Integer duration = drugOrder.getDuration();
		Concept durationUnits = drugOrder.getDurationUnits();
		prescriptionMapper.setRefillDuration(
				(duration == null ? 0 : duration) + " " + (durationUnits == null ? "" : durationUnits.getDisplayString()));
		prescriptionMapper.setRefillRepeat(String.valueOf(drugOrder.getNumRefills()));
		prescriptionMapper.setUuid(drugOrder.getUuid());
		
		return prescriptionMapper;
	}
	
	@NoArgsConstructor
	static class Frequency {
		
		private String frequency;
		
		private String frequencyDisplay;
		
		private String toDate;
		
		private String fromDate;
		
		public String getFrequency() {
			return frequency;
		}
		
		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}
		
		public String getFrequencyDisplay() {
			return frequencyDisplay;
		}
		
		public void setFrequencyDisplay(String frequencyDisplay) {
			this.frequencyDisplay = frequencyDisplay;
		}
		
		public String getToDate() {
			return toDate;
		}
		
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		
		public String getFromDate() {
			return fromDate;
		}
		
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
	}
	
	@NoArgsConstructor
	static class DosingInstructions {
		
		private String dosingInstructions;
		
		private List<Frequency> data;
		
		public String getDosingInstructions() {
			return dosingInstructions;
		}
		
		public void setDosingInstructions(String dosingInstructions) {
			this.dosingInstructions = dosingInstructions;
		}
		
		public List<Frequency> getData() {
			return data;
		}
		
		public void setData(List<Frequency> data) {
			this.data = data;
		}
	}
	
	@NoArgsConstructor
	static class PrescriptionMapper {
		
		private String uuid;
		
		private String drug;
		
		private String dosage;
		
		private String route;
		
		private List<String> instructions;
		
		private String refillDuration;
		
		private String refillRepeat;
		
		public String getUuid() {
			return uuid;
		}
		
		public void setUuid(String uuid) {
			this.uuid = uuid;
		}
		
		public String getDrug() {
			return drug;
		}
		
		public void setDrug(String drug) {
			this.drug = drug;
		}
		
		public String getDosage() {
			return dosage;
		}
		
		public void setDosage(String dosage) {
			this.dosage = dosage;
		}
		
		public String getRoute() {
			return route;
		}
		
		public void setRoute(String route) {
			this.route = route;
		}
		
		public List<String> getInstructions() {
			return instructions;
		}
		
		public void setInstructions(List<String> instructions) {
			this.instructions = instructions;
		}
		
		public String getRefillDuration() {
			return refillDuration;
		}
		
		public void setRefillDuration(String refillDuration) {
			this.refillDuration = refillDuration;
		}
		
		public String getRefillRepeat() {
			return refillRepeat;
		}
		
		public void setRefillRepeat(String refillRepeat) {
			this.refillRepeat = refillRepeat;
		}
	}
}
