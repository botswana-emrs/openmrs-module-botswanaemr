/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.familyplanning;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.Model;
import org.springframework.web.bind.annotation.RequestParam;

public class ReproductiveGynaecologicalPageController {
	
	public void controller(Model model, UiSessionContext sessionContext,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam("patientId") Patient patient) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/srh-gynaecological-history-form.xml";
		
		boolean hasGynaecologicalEncounter = false;
		
		EncounterType gynaecologicalHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_ENCOUNTER_TYPE_UUID);
		
		if (encounter != null) {
			hasGynaecologicalEncounter = true;
			model.addAttribute("gynaecologicalEncounter", encounter);
		} else {
			model.addAttribute("gynaecologicalEncounter", "");
		}
		model.addAttribute("hasGynaecologicalEncounter", hasGynaecologicalEncounter);
		
		model.addAttribute("gynaecologicalDefinitionUiResource", definitionUiResource);
		model.addAttribute("gynaecologicalFormUuid", BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_FORM_UUID);
		model.addAttribute("modalTitle", BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_FORM_MODAL_TITLE);
		List<Encounter> gynaecologicalHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    gynaecologicalHistoryEncounterType, null, null, null);
		model.addAttribute("gynaecologicalHistoryEncounters", gynaecologicalHistoryEncounters);
	}
}
