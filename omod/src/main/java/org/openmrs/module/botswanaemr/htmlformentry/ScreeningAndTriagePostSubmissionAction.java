/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import org.openmrs.*;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.botswanaemr.model.ObsData;
import org.openmrs.module.botswanaemr.utilities.StockUtils;

import java.util.*;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.saveDrugOrderFromDrugObs;

public class ScreeningAndTriagePostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		//queue or exit during enter and edit modes
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		Patient patient = session.getPatient();
		
		Set<EncounterProvider> encounterProviders = session.getEncounter().getEncounterProviders();
		
		Provider provider = null;
		
		if (encounterProviders.size() > 0) {
			provider = new ArrayList<>(encounterProviders).get(0).getProvider();
			closeNurseQueue(patient, provider);
		}
		
		Concept conditionsGrouoConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.CONDITIONS_GROUPING_CONCEPT);
		Set<Obs> groupingObsSet = findGroupingObs(session.getEncounter(), conditionsGrouoConcept);
		if (groupingObsSet.size() > 0) {
			saveConditions(groupingObsSet, patient);
		}
		
		Concept allergyGroupConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.ALLERGY_GROUPING_CONCEPT);
		groupingObsSet = findGroupingObs(session.getEncounter(), allergyGroupConcept);
		if (groupingObsSet.size() > 0) {
			saveAllergies(groupingObsSet, patient);
		}
		
		Concept drugGroupConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.MEDICATIONS_GROUPING_CONCEPT);
		groupingObsSet = findGroupingObs(session.getEncounter(), drugGroupConcept);
		if (!groupingObsSet.isEmpty()) {
			saveDrugOrderFromDrugObs(groupingObsSet, patient, true);
			Set<Obs> obsList= session.getEncounter().getObs();
			StockUtils.dispenseDrugFromObsList(obsList);
		}
		
	}
	
	private void saveConditions(Set<Obs> conditionObs, Patient patient) {
		Set<ObsData> obsDataSet = findObsWithGrouping(conditionObs);
		for (ObsData obsData : obsDataSet) {
			if ((obsData.codedObs == null && obsData.nonCodedObs == null)
			        || (obsData.codedObs != null && conditionExists(patient, obsData.codedObs.getValueCoded()))) {
				continue;
			}
			
			Condition condition = new Condition();
			CodedOrFreeText codedOrFreeText = new CodedOrFreeText();
			if (obsData.codedObs != null) {
				codedOrFreeText.setCoded(obsData.codedObs.getValueCoded());
			}
			
			if (obsData.nonCodedObs != null) {
				codedOrFreeText.setNonCoded(obsData.nonCodedObs.getValueText());
			}
			
			condition.setCondition(codedOrFreeText);
			condition.setPatient(patient);
			condition.setClinicalStatus(ConditionClinicalStatus.ACTIVE);
			
			if (obsData.commentObs != null) {
				condition.setAdditionalDetail(obsData.commentObs.getValueText());
			}
			
			Context.getConditionService().saveCondition(condition);
		}
	}
	
	private boolean conditionExists(Patient patient, Concept concept) {
		if (concept != null) {
			List<Condition> conditions = Context.getConditionService().getActiveConditions(patient);
			for (Condition target : conditions) {
				if (target.getCondition().getCoded() == concept) {
					return true;
				}
			}
		}
		return false;
	}
	
	private void saveAllergies(Set<Obs> allergiesObs, Patient patient) {
		Set<ObsData> obsDataSet = findObsWithGrouping(allergiesObs);
		for (ObsData obsData : obsDataSet) {
			if (obsData.codedObs.getValueCoded() == null) {
				continue;
			}
			
			Allergy allergy = new Allergy();
			allergy.setPatient(patient);
			
			Allergen allergen = new Allergen();
			allergen.setCodedAllergen(obsData.codedObs.getValueCoded());
			allergen.setAllergenType(AllergenType.OTHER);
			allergy.setAllergen(allergen);
			
			if (obsData.commentObs != null) {
				allergy.setComment(obsData.commentObs.getValueText());
			}
			
			if (Context.getPatientService().getAllergies(patient).containsAllergen(allergy)) {
				continue;
			}
			
			Context.getPatientService().saveAllergy(allergy);
		}
	}
	
	public static Set<Obs> findGroupingObs(Encounter encounter, Concept concept) {
		Iterator<Obs> iterator = encounter.getAllObs(false).iterator();
		
		Set<Obs> groupingObs = new HashSet<Obs>();
		
		while (iterator.hasNext()) {
			Obs candidate = iterator.next();
			
			if (candidate.getConcept() == concept && candidate.isObsGrouping()) {
				groupingObs.add(candidate);
			}
		}
		
		return groupingObs;
	}
	
	private Set<ObsData> findObsWithGrouping(Set<Obs> groupingObs) {
		Set<ObsData> obsDataSet = new HashSet<ObsData>();
		for (Obs obs : groupingObs) {
			Set<Obs> childObs = obs.getGroupMembers();
			ObsData obsData = new ObsData();
			obsData.setObs(childObs);
			obsDataSet.add(obsData);
		}
		
		return obsDataSet;
	}
	
	private void closeNurseQueue(Patient patient, Provider provider) {
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		LocationService locationService = Context.getLocationService();
		Location nurseQueueRoom = locationService.getLocationByUuid(BotswanaEmrConstants.NURSING_PORTAL_UUID);
		PatientQueue patientQueue = patientQueueingService.getIncompletePatientQueue(patient, nurseQueueRoom);
		if (patientQueue != null) {
			if (provider != null) {
				patientQueue.setProvider(provider);
			}
			patientQueue.setStatus(PatientQueue.Status.COMPLETED);
			patientQueueingService.savePatientQue(patientQueue);
		}
	}
	
}
