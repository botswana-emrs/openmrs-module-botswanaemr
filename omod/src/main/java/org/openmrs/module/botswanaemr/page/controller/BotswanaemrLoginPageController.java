/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Authenticated;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.ContextAuthenticationException;
import org.openmrs.api.context.UsernamePasswordCredentials;
import org.openmrs.module.appui.AppUiConstants;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.emrapi.utils.GeneralUtils;
import org.openmrs.module.referenceapplication.ReferenceApplicationConstants;
import org.openmrs.module.referenceapplication.ReferenceApplicationWebConstants;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.ui.framework.page.PageRequest;
import org.openmrs.web.user.CurrentUsers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import static org.openmrs.module.referenceapplication.ReferenceApplicationWebConstants.COOKIE_NAME_LAST_SESSION_LOCATION;
import static org.openmrs.module.referenceapplication.ReferenceApplicationWebConstants.REQUEST_PARAMETER_NAME_REDIRECT_URL;
import static org.openmrs.module.referenceapplication.ReferenceApplicationWebConstants.SESSION_ATTRIBUTE_REDIRECT_URL;

/**
 * Spring MVC controller that takes over /login.htm and processes requests to authenticate a user
 */
@Slf4j
@Controller
public class BotswanaemrLoginPageController {
	
	/**
	 * @should redirect the user to the home page if they are already authenticated
	 * @should show the user the login page if they are not authenticated
	 * @should set redirectUrl in the page model if any was specified in the request
	 * @should set the referer as the redirectUrl in the page model if no redirect param exists
	 * @should set redirectUrl in the page model if any was specified in the session
	 * @should not set the referer as the redirectUrl in the page model if referer URL is outside
	 *         context path
	 * @should set the referer as the redirectUrl in the page model if referer URL is within context
	 *         path
	 */
	public String get(PageModel model, UiUtils ui, PageRequest pageRequest) {
		String redirectUrl = getRedirectUrl(pageRequest);
		if (Context.isAuthenticated()) {
			if (StringUtils.isNotBlank(redirectUrl)) {
				return "redirect:" + getRelativeUrl(redirectUrl, pageRequest);
			}
			return "redirect:" + ui.pageLink(ReferenceApplicationConstants.MODULE_ID, "home");
		}
		
		model.addAttribute(REQUEST_PARAMETER_NAME_REDIRECT_URL, getRelativeUrl(redirectUrl, pageRequest));
		
		return null;
	}
	
	private boolean isUrlWithinOpenmrs(PageRequest pageRequest, String redirectUrl) {
		if (StringUtils.isNotBlank(redirectUrl)) {
			if (redirectUrl.startsWith("http://") || redirectUrl.startsWith("https://")) {
				try {
					URL url = new URL(redirectUrl);
					String urlPath = url.getFile();
					String urlContextPath = urlPath.substring(0, urlPath.indexOf('/', 1));
					if (StringUtils.equals(pageRequest.getRequest().getContextPath(), urlContextPath)) {
						return true;
					}
				}
				catch (MalformedURLException | StringIndexOutOfBoundsException e) {
					log.error(e.getMessage());
				}
			} else
				return redirectUrl.startsWith(pageRequest.getRequest().getContextPath());
		}
		return false;
	}
	
	private String getRedirectUrlFromReferer(PageRequest pageRequest) {
		String manualLogout = pageRequest.getSession().getAttribute(AppUiConstants.SESSION_ATTRIBUTE_MANUAL_LOGOUT,
		    String.class);
		String redirectUrl = "";
		if (!Boolean.parseBoolean(manualLogout)) {
			redirectUrl = pageRequest.getRequest().getHeader("Referer");
		} else {
			Cookie cookie = new Cookie(ReferenceApplicationWebConstants.COOKIE_NAME_LAST_USER, null);
			cookie.setMaxAge(0);
			pageRequest.getResponse().addCookie(cookie);
		}
		pageRequest.getSession().setAttribute(AppUiConstants.SESSION_ATTRIBUTE_MANUAL_LOGOUT, null);
		return redirectUrl;
	}
	
	private String getRedirectUrlFromRequest(PageRequest pageRequest) {
		return pageRequest.getRequest().getParameter(REQUEST_PARAMETER_NAME_REDIRECT_URL);
	}
	
	private String getRedirectUrl(PageRequest pageRequest) {
		String redirectUrl = getRedirectUrlFromRequest(pageRequest);
		if (StringUtils.isBlank(redirectUrl)) {
			redirectUrl = getStringSessionAttribute(pageRequest.getRequest());
		}
		if (StringUtils.isBlank(redirectUrl)) {
			redirectUrl = getRedirectUrlFromReferer(pageRequest);
		}
		if (StringUtils.isNotBlank(redirectUrl) && isUrlWithinOpenmrs(pageRequest, redirectUrl)) {
			return redirectUrl;
		}
		return "";
	}
	
	/**
	 * Processes requests to authenticate a user
	 *
	 * @param username
	 * @param password
	 * @param ui {@link UiUtils} object
	 * @param pageRequest {@link PageRequest} object
	 * @param sessionContext
	 * @return
	 * @should redirect the user back to the redirectUrl if any
	 * @should redirect the user to the home page if the redirectUrl is the login page
	 * @should send the user back to the login page if an invalid location is selected
	 * @should send the user back to the login page when authentication fails
	 */
	public String post(@RequestParam(value = "username", required = false) String username,
	        @RequestParam(value = "password", required = false) String password,
	        @SpringBean("locationService") LocationService locationService, UiUtils ui, PageRequest pageRequest,
	        UiSessionContext sessionContext) {
		
		String redirectUrl = getRelativeUrl(pageRequest.getRequest().getParameter(REQUEST_PARAMETER_NAME_REDIRECT_URL),
		    pageRequest);
		
		if (username != null) {
			
			try {
				Authenticated authenticatedUser = Context.authenticate(new UsernamePasswordCredentials(username, password));
				if (authenticatedUser != null) {
					pageRequest.setCookieValue(COOKIE_NAME_LAST_SESSION_LOCATION, "unknown");
					if (Context.isAuthenticated()) {
						if (log.isDebugEnabled()) {
							log.debug("User has successfully authenticated");
						}
						
						sessionContext.setSessionLocation(
						    locationService.getLocationByUuid("8d6c993e-c2cc-11de-8d13-0010c6dffd0f"));
						//we set the username value to check it new or old user is trying to log in
						pageRequest.setCookieValue(ReferenceApplicationWebConstants.COOKIE_NAME_LAST_USER,
						    String.valueOf(username.hashCode()));
						
						// set the locale based on the user's default locale
						Locale userLocale = GeneralUtils.getDefaultLocale(Context.getUserContext().getAuthenticatedUser());
						if (userLocale != null) {
							Context.getUserContext().setLocale(userLocale);
							pageRequest.getResponse().setLocale(userLocale);
							new CookieLocaleResolver().setDefaultLocale(userLocale);
						}
						
						//successful login
						CurrentUsers.addUser(pageRequest.getRequest().getSession(),
						    Context.getUserContext().getAuthenticatedUser());
						
						//on successful auth redirect to pick a  service point
						return "redirect:" + ui.pageLink("botswanaemr", "selectServicePoint");
					}
					
				} else {
					Context.closeSessionWithCurrentUser();
					Context.logout();
					pageRequest.getSession().setAttribute(ReferenceApplicationWebConstants.SESSION_ATTRIBUTE_ERROR_MESSAGE,
					    ui.message("referenceapplication.login.error.invalidLocation", "unknown location for now"));
				}
			}
			catch (ContextAuthenticationException ex) {
				if (log.isDebugEnabled())
					log.debug("Failed to authenticate user");
				
				pageRequest.getSession().setAttribute(ReferenceApplicationWebConstants.SESSION_ATTRIBUTE_ERROR_MESSAGE,
				    ui.message(ReferenceApplicationConstants.MODULE_ID + ".error.login.fail"));
			}
		}
		
		if (log.isDebugEnabled())
			log.debug("Sending user back to login page");
		
		pageRequest.getSession().setAttribute(SESSION_ATTRIBUTE_REDIRECT_URL, redirectUrl);
		return "redirect:" + ui.pageLink(ReferenceApplicationConstants.MODULE_ID, "login");
	}
	
	private String getStringSessionAttribute(HttpServletRequest request) {
		Object attributeValue = request.getSession()
		        .getAttribute(ReferenceApplicationWebConstants.SESSION_ATTRIBUTE_REDIRECT_URL);
		request.getSession().removeAttribute(ReferenceApplicationWebConstants.SESSION_ATTRIBUTE_REDIRECT_URL);
		return attributeValue != null ? attributeValue.toString() : null;
	}
	
	public String getRelativeUrl(String url, PageRequest pageRequest) {
		if (url == null)
			return null;
		
		if (url.startsWith("/") || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			return url;
		}
		
		//This is an absolute url, discard the protocal, domain name/host and port section
		if (url.startsWith("http://")) {
			url = StringUtils.removeStart(url, "http://");
		} else if (url.startsWith("https://")) {
			url = StringUtils.removeStart(url, "https://");
		}
		int indexOfContextPath = url.indexOf(pageRequest.getRequest().getContextPath());
		if (indexOfContextPath >= 0) {
			url = url.substring(indexOfContextPath);
			log.debug("Relative redirect:" + url);
			return url;
		}
		
		return null;
	}
	
}
