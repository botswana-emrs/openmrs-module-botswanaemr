/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ANTENATAL_REGISTRY_ENCOUNTER_TYPE_UUID;

public class AntenatalRegistryFormPageController {
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	public void controller(UiUtils ui, UiSessionContext sessionContext, PageModel pageModal,
	        @RequestParam("patientId") Patient patient, @RequestParam(value = "formUuid") String formUuid,
	        @RequestParam(value = "encounterTypeUuid") String encounterTypeUuid,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "visitId", required = false) Visit visit) {
		if (visit == null) {
			visit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		}
		final String definitionUiResource = "botswanaemr:htmlforms/srh-antenatal-registry-form.xml";
		
		EncounterType encounterType = Context.getEncounterService().getEncounterTypeByUuid(encounterTypeUuid);
		BotswanaEmrUtils.hasRegistryEncounter(pageModal, visit, encounterType);
		pageModal.addAttribute("defaultEncounterDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
		pageModal.addAttribute("patient", patient);
		pageModal.addAttribute("formUuid", formUuid);
		pageModal.addAttribute("definitionUiResource", definitionUiResource);
		pageModal.addAttribute("returnUrl", ui.encodeForSafeURL(
		    HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "srh/srhRegistriesPool", patient, visit, ui)));
	}
}
