/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.visit;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.VisitSimpleObject;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.Date;

public class PatientVisitFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @FragmentParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui) {
		if (StringUtils.isBlank(returnUrl)) {
			returnUrl = ui.pageLinkWithoutContextPath("botswanaemr", "consultation/screeningAndTriage",
			    Collections.singletonMap("patientId", (Object) patient.getId()));
		}
		fragmentModel.addAttribute("returnUrl", returnUrl);
	}
	
	public SimpleObject createPastVisit(@RequestParam(value = "patient") Patient patient,
	        @RequestParam(value = "pastVisitDate") Date pastVisitDate,
	        @RequestParam(value = "programId", required = false) Program program, UiUtils ui,
	        UiSessionContext uiSessionContext) {
		VisitService visitService = Context.getVisitService();
		VisitType facilityVisitType = visitService.getVisitTypeByUuid(BotswanaEmrConstants.FACILITY_VISIT_VISIT_TYPE_UUID);
		Date visitStartDate = DateUtils.getDateStartOfDay(pastVisitDate);
		Date visitStopDate = DateUtils.getDateEndOfDay(pastVisitDate);
		
		Visit visit = new Visit();
		visit.setPatient(patient);
		visit.setVisitType(facilityVisitType);
		visit.setStartDatetime(visitStartDate);
		visit.setStopDatetime(visitStopDate);
		visit.setLocation(uiSessionContext.getSessionLocation());
		visit.setCreator(Context.getAuthenticatedUser());
		visit.setDateCreated(new Date());
		
		//save the visit in th DB
		Visit pastVisit = visitService.saveVisit(visit);
		
		return SimpleObject.create("patientId", patient.getPatientId(), "visitId", pastVisit.getUuid());
	}
}
