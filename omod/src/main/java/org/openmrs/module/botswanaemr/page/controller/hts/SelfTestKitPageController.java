/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;

public class SelfTestKitPageController {
	
	public void controller(PageModel model,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        UiSessionContext uiSessionContext) {
		
		List<Stockroom> destinationStockrooms = new ArrayList<>();
		Stockroom mainStockroom = StockUtils.getMainStockroom(uiSessionContext.getSessionLocation());
		if (mainStockroom != null) {
			destinationStockrooms.add(mainStockroom);
		}
		
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		// Hard coding the uuid of item that am testing with
		Item item = iItemDataService.getByUuid(BotswanaEmrConstants.SELF_TEST_KIT_UUID);
		// Search for operations with this item
		if (item != null) {
			StockOperationSearch stockOperationSearch = new StockOperationSearch();
			StockOperationTemplate stockOperationSearchTemplate = new StockOperationTemplate();
			stockOperationSearchTemplate.setStockroom(mainStockroom);
			stockOperationSearchTemplate.setItem(item);
			stockOperationSearch.setTemplate(stockOperationSearchTemplate);
			
			IStockOperationDataService stockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
			List<StockOperation> stockOperations = stockOperationDataService.getOperations(stockOperationSearch);
			
			model.addAttribute("stockOperations", stockOperations);
		} else {
			model.addAttribute("stockOperations", new ArrayList<>());
		}
		
		model.addAttribute("mainStockRooms", destinationStockrooms);
	}
	
}
