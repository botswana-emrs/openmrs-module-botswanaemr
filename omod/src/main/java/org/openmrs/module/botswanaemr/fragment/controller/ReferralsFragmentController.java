/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class ReferralsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext,
	        @FragmentParam("patientId") Patient patient, @FragmentParam(value = "visit", required = false) Visit visit) {
		fragmentModel.addAttribute("visit", visit);
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(), true);
		BotswanaEmrUtils.setSingleActiveReferralOrder(fragmentModel, patient, activeVisit);
	}
}
