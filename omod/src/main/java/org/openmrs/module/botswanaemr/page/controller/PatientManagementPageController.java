/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.CodedOrFreeText;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.DailyRegisteredListFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.module.reporting.common.DateUtil;

import java.util.*;
import java.util.stream.Collectors;

public class PatientManagementPageController extends DailyRegisteredListFragmentController {
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext, UiUtils ui) {
		Date today = DateUtil.getStartOfDay(new Date());
		String sessionLocationName = "";
		if (sessionContext.getSessionLocation() != null) {
			sessionLocationName = sessionContext.getSessionLocation().getName();
		}
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		pageModel.addAttribute("sessionLocation", sessionLocationName);
		pageModel.addAttribute("pastRegisteredPatientsSummary",
		    getSimplifiedPatientRegistrationDetails(Context.getService(BotswanaEmrService.class)
		            .getPatientsRegisteredOnDate(null, today, sessionContext.getSessionLocation(), limitFetch),
		        ui));
		pageModel.addAttribute("ageCategories", BotswanaEmrUtils.getDefaultAgeCategories());
		pageModel.addAttribute("conditions", conditions());
		
		String currentServiceDeliveryPointUuid = String
		        .valueOf(sessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		pageModel.addAttribute("isLabServicesDeliveryPoint",
		    currentServiceDeliveryPointUuid.equals(BotswanaEmrConstants.LAB_SERVICES_PORTAL_UUID));
	}
	
	public Map<String, String> conditions() {
		List<CodedOrFreeText> codedOrFreeTexts = Context.getService(BotswanaEmrService.class).getAllConditionEverSaved();
		
		Map<String, String> conditionValues = codedOrFreeTexts.stream().filter(a -> a != null && a.getNonCoded() != null)
		        .collect(Collectors.toMap(CodedOrFreeText::getNonCoded, CodedOrFreeText::getNonCoded, (a, b) -> a));
		return conditionValues;
		
	}
}
