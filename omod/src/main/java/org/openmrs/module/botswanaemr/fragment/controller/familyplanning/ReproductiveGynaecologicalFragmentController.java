/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_ENCOUNTER_TYPE_UUID;

public class ReproductiveGynaecologicalFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext,
	        @RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/srh-gynaecological-history-form.xml";
		
		boolean hasGynaecologicalEncounter = false;
		
		EncounterType gynaecologicalHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_GYNAECOLOGICAL_HISTORY_ENCOUNTER_TYPE_UUID);
		
		if (encounter != null) {
			hasGynaecologicalEncounter = true;
			fragmentModel.addAttribute("gynaecologicalEncounter", encounter);
		} else {
			fragmentModel.addAttribute("gynaecologicalEncounter", "");
		}
		fragmentModel.addAttribute("hasGynaecologicalEncounter", hasGynaecologicalEncounter);
		
		fragmentModel.addAttribute("gynaecologicalDefinitionUiResource", definitionUiResource);
		fragmentModel.addAttribute("gynaecologicalFormUuid", BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_FORM_UUID);
		fragmentModel.addAttribute("modalTitle", BotswanaEmrConstants.SRH_GYNAECOLOGICAL_HISTORY_FORM_MODAL_TITLE);
		List<Encounter> gynaecologicalHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    gynaecologicalHistoryEncounterType, null, null, null);
		fragmentModel.addAttribute("gynaecologicalHistoryEncounters", gynaecologicalHistoryEncounters);
	}
}
