/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.USER_BACKUP_EMAIL_ATTRIBUTE_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.USER_SECURITY_NO_ATTRIBUTE_TYPE;

import org.apache.http.HttpStatus;
import org.openmrs.Person;
import org.openmrs.PersonAttribute;
import org.openmrs.User;
import org.openmrs.api.APIAuthenticationException;
import org.openmrs.api.APIException;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.page.controller.UserProfilePageController;
import org.openmrs.module.webservices.validation.ValidationException;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestParam;

public class SecuritySettingsFragmentController extends UserProfilePageController {
	
	@Qualifier("userService")
	@Autowired
	protected UserService userService;
	
	public void controller() {
		
	}
	
	public void updateSecurityPhoneNumber(@RequestParam("newSecurityPhoneNumber") String newSecurityPhoneNumber) {
		User user = Context.getAuthenticatedUser();
		
		Person person = user.getPerson();
		PersonAttribute personAttribute = Context.getPersonService()
		        .getPersonAttributeByUuid(USER_SECURITY_NO_ATTRIBUTE_TYPE);
		if (personAttribute == null) {
			createPersonAttribute(person, newSecurityPhoneNumber, USER_SECURITY_NO_ATTRIBUTE_TYPE);
		} else {
			personAttribute = setPersonAttributeValue(newSecurityPhoneNumber, USER_SECURITY_NO_ATTRIBUTE_TYPE);
			person.addAttribute(personAttribute);
		}
		
		Context.getPersonService().savePerson(person);
	}
	
	public void updateBackupEmail(@RequestParam("newBackupEmail") String newBackupEmail) {
		User user = Context.getAuthenticatedUser();
		
		Person person = user.getPerson();
		PersonAttribute personAttribute = Context.getPersonService()
		        .getPersonAttributeByUuid(USER_BACKUP_EMAIL_ATTRIBUTE_TYPE);
		if (personAttribute == null) {
			createPersonAttribute(person, newBackupEmail, USER_BACKUP_EMAIL_ATTRIBUTE_TYPE);
		} else {
			personAttribute = setPersonAttributeValue(newBackupEmail, USER_BACKUP_EMAIL_ATTRIBUTE_TYPE);
			person.addAttribute(personAttribute);
		}
		
		Context.getPersonService().savePerson(person);
	}
	
	public Object changePassword(@RequestParam("newPassword") String newPassword,
	        @RequestParam("oldPassword") String oldPassword) throws Exception {
		//		String oldPassword = (String)body.get("oldPassword");
		//		String newPassword = (String)body.get("newPassword");
		if (!Context.isAuthenticated()) {
			throw new APIAuthenticationException("Must be authenticated to change your own password");
		}
		
		SimpleObject simpleObject = new SimpleObject();
		try {
			User user = Context.getAuthenticatedUser();
			Context.getUserService().changePassword(user, oldPassword, newPassword);
			simpleObject.put("status", HttpStatus.SC_OK);
			simpleObject.put("response", "Success");
			// userService.changePassword(user, oldPassword, newPassword);
		}
		catch (APIException ex) {
			simpleObject.put("status", HttpStatus.SC_INTERNAL_SERVER_ERROR);
			simpleObject.put("response", "Failed: " + ex.getMessage());
			
		}
		
		return simpleObject;
		
	}
	
}
