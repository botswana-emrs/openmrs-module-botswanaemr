/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CohortRegistryProfilePageController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui, final PageModel pageModel,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("formService") FormService formService, UiSessionContext sessionContext) {
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		boolean hasActiveVisit = currentPatientVisit != null;
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("patient", patient);
		if (currentPatientVisit != null) {
			pageModel.addAttribute("visitId", currentPatientVisit.getVisitId());
		} else {
			pageModel.addAttribute("visitId", "");
		}
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "srh/cohortRegistryProfile", patient, currentPatientVisit, ui)));
		pageModel.addAttribute("cohortRegistryFormUuid", BotswanaEmrConstants.COHORT_REGISTRY_FORM_UUID);
		pageModel.addAttribute("infantFacilityVisitFormUuid", BotswanaEmrConstants.INFANT_FACILITY_VISITS_FORM_UUID);
		
		List<Form> forms = new ArrayList<>();
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.COHORT_REGISTRY_FORM_UUID));
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.INFANT_FACILITY_VISITS_FORM_UUID));
		forms.removeAll(Collections.singleton(null));
		List<Encounter> cohortEncounters = encounterService.getEncounters(
		    new EncounterSearchCriteria(patient, null, null, null, null, forms, null, null, null, null, false));
		pageModel.addAttribute("cohortEncounters", cohortEncounters);
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("visit", visit);
	}
}
