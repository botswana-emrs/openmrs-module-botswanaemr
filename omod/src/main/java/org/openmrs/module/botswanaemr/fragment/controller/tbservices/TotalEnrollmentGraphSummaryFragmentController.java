/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.tbservices;

import org.joda.time.DateTime;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.SeriesDataObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TotalEnrollmentGraphSummaryFragmentController {
	
	public void controller(FragmentModel model,
	        @FragmentParam(value = "type", required = false) BotswanaEmrConstants.summaryType typeOfSummary,
	        @FragmentParam(value = "encounterType", required = false) String encounterTypeUuid,
	        UiSessionContext uiSessionContext) {
		
		if (typeOfSummary == null) {
			typeOfSummary = BotswanaEmrConstants.summaryType.REGISTRATION;
		} else if (typeOfSummary != BotswanaEmrConstants.summaryType.PROGRAM) {
			typeOfSummary = BotswanaEmrConstants.summaryType.SCREENING;
		}
		
		Location location = uiSessionContext.getSessionLocation();
		
		if (typeOfSummary.equals(BotswanaEmrConstants.summaryType.PROGRAM)) {
			List<Encounter> enrollmentEncounters = new ArrayList<>();
			List<SeriesDataObject> seriesData = new ArrayList<>();
			
			EncounterType encounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.TB_ENROLMENT_ENCOUNTER_TYPE_UUID);
			
			if (encounterType != null) {
				for (int i = 1; i <= 12; i++) {
					DateTime startDate = new DateTime().withMonthOfYear(i).dayOfMonth().withMinimumValue();
					DateTime endDate = new DateTime().withMonthOfYear(i).dayOfMonth().withMaximumValue();
					
					String name = startDate.monthOfYear().getAsShortText();
					
					enrollmentEncounters = getEncountersByDates(encounterType, location, startDate.toDate(),
					    endDate.toDate());
					
					SeriesDataObject dataObject = new SeriesDataObject(name, enrollmentEncounters.size());
					seriesData.add(dataObject);
					
				}
			}
			
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("seriesData", seriesData);
			
			model.addAttribute("seriesData", simpleObject.toJson());
		}
	}
	
	private List<Encounter> getEncountersByDates(EncounterType encounterType, Location location, Date fromDate,
	        Date toDate) {
		return BotswanaEmrUtils.getEncounters(encounterType, location, fromDate, toDate);
	}
	
}
