/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */

package org.openmrs.module.botswanaemr.htmlformentry;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Obs;
import org.openmrs.Encounter;
import org.openmrs.TestOrder;
import org.openmrs.api.APIException;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabOrderService;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

@Slf4j
public class HtsDailyRegisterPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		for (Obs obs : session.getEncounter().getAllObs()) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_HIV_TEST_TYPE_UUID)) {
				if (obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.HTS_DNA_PCR_CONCEPT_UUID)
				        || obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.HIV_CONFIRMATORY_TEST_CONCEPT_UUID)
				        || obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.HTS_ST_ORAL_TEST_CONCEPT_UUID)
				        || obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.HTS_WESTERN_BLOT_CONCEPT_UUID)) {
					EncounterService encounterService = Context.getEncounterService();
					Encounter entrySessionEncounter = session.getEncounter();
					if (entrySessionEncounter.getEncounterType().equals(
					    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID))
					        && entrySessionEncounter.getOrders().isEmpty()) {
						try {
							LabOrderService labOrderService = Context.getRegisteredComponent("botswana.emr.labOrderService",
							    LabOrderService.class);
							//add order here
							TestOrder order = labOrderService.createOrder(entrySessionEncounter);
							//add the lab test order
							labOrderService.createLabTest(order);
						}
						catch (APIException e) {
							log.debug(e.getMessage());
							//e.printStackTrace();
							//throw new APIException(e);
						}
					}
				}
			}
		}
		
		HtsKitUsagePostSubmissionAction htsKitUsagePS = new HtsKitUsagePostSubmissionAction();
		htsKitUsagePS.applyAction(session);
	}
}
