/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PERSON_LOCATION_ATTRIBUTE;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.Person;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.context.SessionContext;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.impl.BotswanaPersonRegistryServiceImpl;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.representation.CustomRepresentation;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ConfirmPatientPageController {
	
	public void controller(UiUtils ui, SessionContext sessionContext, PageModel model,
	        @RequestParam(value = "patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "action", required = false) String action) {
		Location location = getPersonLocation(patient.getPerson());
		if (location != null) {
			if (Objects.equals(sessionContext.getSessionLocationId(), location.getLocationId())) {
				model.addAttribute("infoNote", "Patient is registered at this facility.");
				model.addAttribute("location", location.getName());
			} else {
				model.addAttribute("infoNote", "Patient currently registered at <b>" + location.getName() + "</b>. "
				        + "If you proceed, the patient's profile will be transferred to this facility.");
				model.addAttribute("location", location.getName());
			}
			model.addAttribute("locationName", location.getName());
			
		} else {
			model.addAttribute("infoNote",
			    "Facility information not found.</b> The patient's profile will be registered in this facility");
			model.addAttribute("location", "");
			model.addAttribute("locationName", "");
			
		}
		// get all patient identifiers and order by Omang,Birth Certfication, Passport, OpenMRS Id and fetch the first one
		BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
		// get the identifier type and replace the identifierType value in the model with the identifier type name
		String identifierType = (String) model.getAttribute("identifierType");
		if (identifierType != null) {
			if (BotswanaEmrConstants.OMANG_IDENTIFIER_NAME.equals(identifierType)) {
				model.replace("identifierType", BotswanaPersonRegistryServiceImpl.NATIONAL_ID);
			} else if (BotswanaEmrConstants.BIRTH_CERT_IDENTIFIER_NAME.equals(identifierType)) {
				model.replace("identifierType", BotswanaPersonRegistryServiceImpl.BIRTH_CERTIFICATE_NUMBER);
			} else if (BotswanaEmrConstants.PASSPORT_IDENTIFIER_NAME.equals(identifierType)) {
				model.replace("identifierType", BotswanaPersonRegistryServiceImpl.PASSPORT_NUMBER);
			} else {
				model.replace("identifierType", BotswanaPersonRegistryServiceImpl.NATIONAL_ID);
			}
		}
		
		model.addAttribute("patientUuid", patient.getUuid());
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("action", action);
		model.addAttribute("result", ConversionUtil.convertToRepresentation(patient,
		    new CustomRepresentation("uuid,identifiers:(identifierType:(name),identifier),person")));
		model.addAttribute("mpiSearchEnabled",
		    Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.GP_MPI_SEARCH_ENABLED));
		
	}
	
	public static Location getPersonLocation(Person person) {
		String locationAttributeUuid = Context.getAdministrationService()
		        .getGlobalProperty("locationbasedaccess.locationAttributeUuid");
		if (StringUtils.isNotBlank(locationAttributeUuid)) {
			PersonAttributeType personAttributeType = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(locationAttributeUuid);
			PersonAttribute personAttribute = person.getAttribute(personAttributeType);
			if (personAttribute != null) {
				Location personLocation = Context.getLocationService().getLocationByUuid(personAttribute.getValue());
				return personLocation;
			}
		}
		
		return null;
	}
	
	public String post(@RequestParam(value = "patientUuid") Patient patient,
	        @RequestParam(value = "actionPage") String actionPage,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui,
	        @RequestParam(value = "action", required = false) String action, UiSessionContext sessionContext) {
		PersonAttributeType personAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(PERSON_LOCATION_ATTRIBUTE);
		
		PersonAttribute personAttribute = patient.getAttribute(personAttributeType.getName());
		if (personAttribute == null) {
			personAttribute = new PersonAttribute();
		}
		personAttribute.setAttributeType(personAttributeType);
		personAttribute.setValue(sessionContext.getSessionLocation().getUuid());
		patient.addAttribute(personAttribute);
		Context.getPatientService().savePatient(patient);
		
		Map<String, Object> params = new HashMap<>();
		params.put("action", action);
		params.put("patientId", patient.getId());
		if (actionPage.equals("capturePayment")) {
			return "redirect:" + ui.pageLink("botswanaemr", "capturePayment", params);
		} else if (actionPage.equals("captureDelayedPayment")) {
			params.put("startVisit", true);
			params.put("returnUrl", returnUrl);
			return "redirect:" + ui.pageLink("botswanaemr", "assignPatient", params);
		} else {
			params.put("proceed", "true");
			return "redirect:" + ui.pageLink("botswanaemr", "patientProfile", params);
		}
		
	}
	
}
