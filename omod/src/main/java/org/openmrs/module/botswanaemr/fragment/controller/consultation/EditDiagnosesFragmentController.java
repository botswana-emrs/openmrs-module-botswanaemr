/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import java.io.IOException;
import java.util.List;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.ConditionVerificationStatus;
import org.openmrs.Diagnosis;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.DiagnosisAttributeType;
import org.openmrs.DiagnosisAttribute;
import org.openmrs.api.ConceptService;
import org.openmrs.api.DiagnosisService;
import org.openmrs.api.EncounterService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class EditDiagnosesFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam("patientId") String patientId,
	        @FragmentParam(value = "diagnoses", required = true) List<Diagnosis> diagnoses,
	        @FragmentParam(value = "diagnosisAttributes", required = false) HashMap<Integer, String> diagnosisAttributes,
	        UiUtils ui, @SpringBean("conceptService") ConceptService conceptService,
	        @SpringBean("diagnosisService") DiagnosisService diagnosisService) {
		
		// List<Diagnosis> diagnoses = diagnosisService.getDiagnosesByVisit(visit, false, false);
		fragmentModel.addAttribute("hasDiagnoses", diagnoses.size() > 0);
		fragmentModel.addAttribute("diagnoses", diagnoses);
		fragmentModel.addAttribute("diagnosisAttributes", diagnosisAttributes);
	}
	
	/**
	 * Fragment Action for updating and saving new patient diagnoses
	 */
	public void updateDiagnoses(UiUtils ui, @RequestParam("patient") Patient patient,
	        @RequestParam(value = "locationId", required = false) Location location,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "diagnosisEncounterType", required = true) String diagnosisEncounterType,
	        @SpringBean("diagnosisService") DiagnosisService diagnosisService,
	        @SpringBean("conceptService") ConceptService conceptService, @SpringBean("adtService") AdtService adtService,
			@SpringBean("encounterService") EncounterService encounterService,
	        UiSessionContext uiSessionContext) throws IOException {
		if (location == null) {
			location = uiSessionContext.getSessionLocation();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<String> deletedDiagnoses = mapper.readValue(arrayNode, new TypeReference<List<String>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedDiagnoses = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newDiagnoses = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		for (String uuid : deletedDiagnoses) {
			Diagnosis diagnosis = diagnosisService.getDiagnosisByUuid(uuid);
			if (diagnosis != null) {
				diagnosis.setVoided(true);
				diagnosisService.save(diagnosis);
			}
		}
		
		for (SimpleObject simpleObject : editedDiagnoses) {
			Diagnosis diagnosis = diagnosisService.getDiagnosisByUuid((String) simpleObject.get("uuid"));
			String diagnosisStr = (String) simpleObject.get("diagnosis");
			String diagnosisValue = diagnosis.getDiagnosis().getCoded() != null
			        ? diagnosis.getDiagnosis().getCoded().getUuid()
			        : diagnosis.getDiagnosis().getNonCoded();
			if (diagnosisStr != null && !diagnosisStr.equals(diagnosisValue)) {
				// Void old diagnosis and create a new one
				newDiagnoses.add(simpleObject);
				diagnosis.setVoided(true);
			} else {
				if (simpleObject.get("certainty") != null) {
					// Edited certainty only
					String certainty = simpleObject.get("certainty").toString();
					diagnosis.setCertainty(ConditionVerificationStatus.valueOf(certainty));
				}
				if (simpleObject.get("form") != null) {
					// Edited form only
					String form = simpleObject.get("form").toString();
					diagnosis.setFormNamespaceAndPath(form);
				}
				if (simpleObject.get("type") != null) {
					// Edited type
					String diagnosisType = (String) simpleObject.get("type");
					for (DiagnosisAttribute diagnosisAttribute : diagnosis.getActiveAttributes()) {
						if (diagnosisAttribute.getAttributeType().getUuid()
						        .equals(BotswanaEmrConstants.DIAGNOSIS_TYPE_DIAGNOSIS_ATTRIBUTE_TYPE)
						        && !diagnosisAttribute.getValueReference().equals(diagnosisType)) {
							diagnosisAttribute.setValue(diagnosisType);
							diagnosisAttribute.setValueReferenceInternal(diagnosisType);
							diagnosis.setAttribute(diagnosisAttribute);
						}
					}
				}
			}
			diagnosisService.save(diagnosis);
		}
		
		if (visit == null) {
			visit = adtService.ensureActiveVisit(patient, location);
		}
		
		EncounterType encounterType = encounterService.getEncounterTypeByUuid(diagnosisEncounterType);
		Encounter diagnosisEncounter = BotswanaEmrUtils.getOrCreateEncounter(patient, encounterType, visit,
		    location);


		Integer rank = 1;
		for (SimpleObject simpleObject : newDiagnoses) {
			saveNewDiagnosis(simpleObject, patient, diagnosisEncounter, diagnosisService, conceptService, location, rank);
			rank++;
		}
	}
	
	private void saveNewDiagnosis(SimpleObject diagnosisObject, Patient patient, Encounter diagnosisEncounter,
	        DiagnosisService diagnosisService, ConceptService conceptService, Location location, Integer rank) {
		
		Diagnosis diagnosis = new Diagnosis();
		CodedOrFreeText diagnosisConcept = new CodedOrFreeText();
		String diagnosisStr = (String) diagnosisObject.get("diagnosis");
		Concept concept = conceptService.getConceptByUuid(diagnosisStr);
		String certainty = (String) diagnosisObject.get("certainty");
		String diagnosisType = (String) diagnosisObject.get("type");
		String form = (String) diagnosisObject.get("form");
		if (concept != null) {
			diagnosisConcept.setCoded(concept);
			diagnosisConcept.setSpecificName(concept.getName());
		} else {
			diagnosisConcept.setNonCoded(diagnosisStr);
		}
		diagnosis.setDiagnosis(diagnosisConcept);
		diagnosis.setEncounter(diagnosisEncounter);
		diagnosis.setPatient(patient);
		if (certainty != null) {
			diagnosis.setCertainty(ConditionVerificationStatus.valueOf(certainty));
		} else {
			diagnosis.setCertainty(ConditionVerificationStatus.PROVISIONAL);
		}
		if (form != null) {
			diagnosis.setFormNamespaceAndPath(form);
		}
		diagnosis.setRank(rank);
		
		setDiagnosisType(diagnosisService, diagnosisType, diagnosis);
		diagnosisService.save(diagnosis);
	}
	
	private void setDiagnosisType(DiagnosisService diagnosisService, String diagnosisType, Diagnosis diagnosis) {
		DiagnosisAttributeType diagnosisAttributeType = diagnosisService
		        .getDiagnosisAttributeTypeByUuid(BotswanaEmrConstants.DIAGNOSIS_TYPE_DIAGNOSIS_ATTRIBUTE_TYPE);
		if (diagnosisAttributeType != null) {
			DiagnosisAttribute diagnosisAttribute = new org.openmrs.DiagnosisAttribute();
			diagnosisAttribute.setAttributeType(diagnosisAttributeType);
			diagnosisAttribute.setValueReferenceInternal(diagnosisType);
			diagnosisAttribute.setValue(diagnosisType);
			diagnosis.setAttribute(diagnosisAttribute);
		}
	}
}
