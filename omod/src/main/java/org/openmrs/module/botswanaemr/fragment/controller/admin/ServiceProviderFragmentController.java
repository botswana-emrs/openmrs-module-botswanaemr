/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utils.UuidGenerator;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

public class ServiceProviderFragmentController {
	
	public void get(PageModel model) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		model.addAttribute("serviceProvider", service.getAllServiceProvider(false));
		
	}
	
	public String registerServiceProvider(UiUtils uiUtils, @RequestParam(value = "provider") String name,
	        @RequestParam(value = "description") String description) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		ServiceProvider serviceProvider;
		if (StringUtils.isNotEmpty(name)) {
			serviceProvider = service.getAllServiceProvider(true).stream().filter(p -> p.getName().equals(name)).findFirst()
			        .orElse(null);
			if (serviceProvider == null) {
				serviceProvider = new ServiceProvider();
				serviceProvider.setDateCreated(new Date());
				serviceProvider.setCreator(Context.getAuthenticatedUser());
				serviceProvider.setName(name);
				serviceProvider.setUuid(UuidGenerator.getNextUuid());
			} else {
				serviceProvider.setRetired(false);
				serviceProvider.setRetiredBy(null);
				serviceProvider.setRetireReason(null);
			}
			serviceProvider.setDescription(description);
			
			//save the service provider
			service.saveServiceProvider(serviceProvider);
		}
		return null;
	}
	
	public String post(@RequestParam(value = "provider") String name,
	        @RequestParam(value = "description") String description) {
		
		return "";
	}
	
}
