/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDetailDataService;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import java.util.Set;

public class SrhPlanPostSubmissionAction implements CustomFormSubmissionAction {
	
	public static final String DRUG_CONCEPT_UUID = "1282AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	IItemStockDetailDataService itemStockService = BotswanaInventoryContext.getItemStockDetailDataService();
	
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER))) {
			return;
		}
		
		Set<Obs> obsList = session.getEncounter().getObs();
		
		StockUtils.dispenseDrugFromObsList(obsList);
		
		// Submit a drug order (aka prescription)
		Concept drugGroupConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.MEDICATIONS_GROUPING_CONCEPT);
		Set<Obs> groupingObsSet = ScreeningAndTriagePostSubmissionAction.findGroupingObs(session.getEncounter(),
		    drugGroupConcept);
		if (!groupingObsSet.isEmpty()) {
			BotswanaEmrUtils.saveDrugOrderFromDrugObs(groupingObsSet, session.getPatient(), true);
		}
	}
}
