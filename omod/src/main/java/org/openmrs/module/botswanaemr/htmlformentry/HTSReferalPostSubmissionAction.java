/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.openmrs.CareSetting;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterProvider;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Provider;
import org.openmrs.ReferralOrder;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CARE_SETTING_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.RECEIVING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TO_ART_REFERRAL_ENCOUNTER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_DEPARTMENT_CONCEPT_UUID;

public class HTSReferalPostSubmissionAction implements CustomFormSubmissionAction {
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	private final ObsService obsService = Context.getService(ObsService.class);
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		if (!formEntrySession.getEncounter().getOrders().isEmpty()) {
			return;
		}
		
		final String status = "IN_PROGRESS";
		CareSetting careSetting = Context.getOrderService().getCareSettingByUuid(CARE_SETTING_UUID);
		EncounterType referralEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(HTS_TO_ART_REFERRAL_ENCOUNTER_TYPE_UUID);
		OrderType referralOrderType = Context.getOrderService().getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		
		Order.FulfillerStatus referralStatus = Enum.valueOf(Order.FulfillerStatus.class, status);
		
		Concept referringDepartmentConcept = conceptService.getConceptByUuid(REFERRING_DEPARTMENT_CONCEPT_UUID);
		
		Concept receivingDepartmentConcept = conceptService.getConceptByUuid(RECEIVING_DEPARTMENT_CONCEPT_UUID);
		
		Encounter referralEncounter = BotswanaEmrUtils.persistEncounter(formEntrySession.getPatient(), referralEncounterType,
		    formEntrySession.getEncounter().getLocation(), formEntrySession.getEncounter().getVisit());
		
		Obs referringDepartmentObs = BotswanaEmrUtils.creatObs(referralEncounter, referringDepartmentConcept);
		referringDepartmentObs.setObsDatetime(new Date());
		referringDepartmentObs.setValueText(referralEncounter.getLocation().getName());
		// referringDepartmentObs.setComment(referringFacility);
		referringDepartmentObs.setValueCoded(referringDepartmentConcept);
		obsService.saveObs(referringDepartmentObs, "Department referring");
		referralEncounter.addObs(referringDepartmentObs);
		
		Obs receivingDepartmentObs = BotswanaEmrUtils.creatObs(referralEncounter, receivingDepartmentConcept);
		receivingDepartmentObs.setObsDatetime(new Date());
		// Handle addition of facility referred to from the form
		receivingDepartmentObs.setValueText("");
		receivingDepartmentObs.setComment("");
		receivingDepartmentObs.setValueCoded(receivingDepartmentConcept);
		obsService.saveObs(receivingDepartmentObs, "Department receiving");
		referralEncounter.addObs(receivingDepartmentObs);
		
		ReferralOrder referral = new ReferralOrder();
		referral.setEncounter(referralEncounter);
		referral.setPatient(formEntrySession.getPatient());
		Set<EncounterProvider> encounterProviders = referralEncounter.getEncounterProviders();
		
		Provider provider = null;
		
		if (!encounterProviders.isEmpty()) {
			provider = new ArrayList<>(encounterProviders).get(0).getProvider();
			referral.setOrderer(provider);
			
		}
		referral.setOrderType(referralOrderType);
		referral.setConcept(referringDepartmentConcept);
		referral.setLocation(receivingDepartmentConcept);
		referral.setCreator(Context.getAuthenticatedUser());
		referral.setDateCreated(referralEncounter.getDateCreated());
		referral.setDateActivated(new Date());
		referral.setCareSetting(careSetting);
		referral.setOrderReason(
		    Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.HIV_CARE_REFERRAL_ORDER_REASON_CONCEPT_UUID));
		
		referral.setFulfillerStatus(referralStatus);
		// referral.setCommentToFulfiller(referralNotes);
		
		orderService.saveOrder(referral, null);
		referralEncounter.addOrder(referral);
		
	}
}
