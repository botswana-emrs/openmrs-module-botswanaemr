/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class HivStatusFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("obsService") ObsService obsService, @SpringBean("conceptService") ConceptService conceptService) {
		
		Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.HIV_STATUS_RESULT_CONCEPT_UUID, null, null);
		String hivStatus = null;
		if (obs != null && obs.getValueCoded() != null) {
			hivStatus = conceptService.getConcept(obs.getValueCoded().getConceptId()).getName().getName();
		} else {
			hivStatus = "Not Tested";
		}
		
		model.addAttribute("hivStatus", hivStatus);
	}
}
