/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.srh;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CervicalScreeningHistoryFragmentController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "visitId") Visit visit, UiUtils ui, final FragmentModel model,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, UiSessionContext sessionContext) {
		
		EncounterType screeningEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.CERVICAL_SCREENING_ENCOUNTER_TYPE_UUID);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient,
		        sessionContext.getSessionLocation(), null, null, null, null,
		        Collections.singletonList(screeningEncounterType), null, null, null, false);
		List<Encounter> encounters = BotswanaEmrUtils
		        .sortEncountersByEncounterDatetime(Context.getEncounterService().getEncounters(encounterSearchCriteria));
		SimpleObject screeningHistoryObs = new SimpleObject();
		
		if (!encounters.isEmpty() && !encounters.get(0).getObs().isEmpty()) {
			screeningHistoryObs = getScreeningObs(encounters.get(0));
		}
		
		model.addAttribute("screeningHistoryObs", screeningHistoryObs);
		model.addAttribute("screeningHistoryObsObjs", screeningHistoryObs.toJson());
		model.addAttribute("lastScreeningEncounter", !encounters.isEmpty() ? encounters.get(0) : null);
		
	}
	
	private SimpleObject getScreeningObs(Encounter encounter) {
		SimpleObject screeningHistoryObs = new SimpleObject();
		screeningHistoryObs.put("lastScreening", encounter.getEncounterDatetime());
		
		for (Obs obs : encounter.getAllObs(false)) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_SCREENING_PAST_CONCEPT_SCREENED_UUID)) {
				screeningHistoryObs.put("pastScreening", SimpleObject.create("val", obs.getValueCoded().getName().getName(),
				    "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			
			if (obs.getConcept().getUuid()
			        .equals(BotswanaEmrConstants.CERVICAL_TREATMENT_TYPE_PERFORMED_CONCEPT_SCREENED_UUID)) {
				screeningHistoryObs.put("treatmentTypePerformed", SimpleObject.create("val",
				    obs.getValueCoded().getName().getName(), "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
				screeningHistoryObs.put("treatmentDate",
				    SimpleObject.create("val", BotswanaEmrUtils.formatDateWithoutTime(obs.getObsDatetime(), "dd-MMM-yyyy"),
				        "id", obs.getObsId(), "uuid", obs.getConcept().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.SCREENING_THROUGH_CONCEPT_UUID)) {
				screeningHistoryObs.put("screeningThrough", SimpleObject.create("val",
				    obs.getValueCoded().getName().getName(), "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_SCREENING_RESULT_CONCEPT_UUID)) {
				screeningHistoryObs.put("screeningResult", SimpleObject.create("val",
				    obs.getValueCoded().getName().getName(), "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_TREATMENT_PERFORMED_CONCEPT_UUID)) {
				screeningHistoryObs.put("treatmentPerformed", SimpleObject.create("val",
				    obs.getValueCoded().getName().getName(), "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_LMNP_CONCEPT_UUID)) {
				screeningHistoryObs.put("lmnp", SimpleObject.create("val", obs.getValueDate(), "id", obs.getObsId(), "uuid",
				    obs.getConcept().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_LAST_SCREENING_CONCEPT_UUID)) {
				screeningHistoryObs.put("lastScreening",
				    SimpleObject.create("val", BotswanaEmrUtils.formatDateWithoutTime(obs.getValueDate(), "dd-MMM-yyyy"),
				        "id", obs.getObsId(), "uuid", obs.getConcept().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_DATE_NEGATIVE_CONCEPT_UUID)) {
				screeningHistoryObs.put("dateNegativeResults", SimpleObject.create("val", obs.getValueDate(), "id",
				    obs.getObsId(), "uuid", obs.getConcept().getUuid()));
				screeningHistoryObs.put("negativeResult", true);
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_CONCEPT_UUID)) {
				screeningHistoryObs.put("pitc", SimpleObject.create("val", obs.getValueCoded().getName().getName(), "id",
				    obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
				screeningHistoryObs.put("unknownResult", true);
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_RESULT_CONCEPT_UUID)) {
				screeningHistoryObs.put("pitcResults", SimpleObject.create("val", obs.getValueCoded().getName().getName(),
				    "id", obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_DATE_CONCEPT_UUID)) {
				screeningHistoryObs.put("pitcDate", SimpleObject.create("val", obs.getValueDate(), "id", obs.getObsId(),
				    "uuid", obs.getConcept().getUuid()));
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_ON_ART_CONCEPT_UUID)) {
				screeningHistoryObs.put("onArt", SimpleObject.create("val", obs.getValueCoded().getName().getName(), "id",
				    obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
				screeningHistoryObs.put("positiveResult", true);
			}
			
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_ART_SITE_CONCEPT_UUID)) {
				screeningHistoryObs.put("artSite", SimpleObject.create("val", obs.getValueText(), "id", obs.getObsId(),
				    "uuid", obs.getConcept().getUuid()));
				screeningHistoryObs.put("positiveResult", true);
			}
		}
		
		return screeningHistoryObs;
	}
	
	public void saveCervicalScreening(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException, ParseException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		String encounterUuid = jsonNode.get("lastScreeningEncounterUuid").asText();
		Encounter encounter = Context.getEncounterService().getEncounterByUuid(encounterUuid);
		if (encounter == null || !Objects.equals(encounter.getVisit().getVisitId(), visit.getVisitId())) {
			EncounterType encounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.CERVICAL_SCREENING_ENCOUNTER_TYPE_UUID);
			encounter = BotswanaEmrUtils.createEncounter(patient, encounterType, sessionContext.getSessionLocation(), visit);
		} else {
			//TODO: Delete cleared obs
			encounter.getObs().clear();
		}
		
		String screened = jsonNode.get("screened").get("val").asText();
		String hpvResult = jsonNode.get("hpvResult").get("val").asText();
		String hpvTreatment = jsonNode.get("hpvTreatment").get("val").asText();
		String papResult = jsonNode.get("papResult").get("val").asText();
		String screeningType = jsonNode.get("screeningType").get("val").asText();
		String treatmentType = jsonNode.get("treatmentType").get("val").asText();
		String treatmentDate = jsonNode.get("treatmentDate").get("val").asText();
		String papAbnormalDetails = jsonNode.get("papAbnormalDetails").get("val").asText();
		
		ObsService obsService = Context.getObsService();
		if (StringUtils.isNotEmpty(screened)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("screened").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("screened").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_SCREENING_PAST_CONCEPT_SCREENED_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(screened));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(hpvResult)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("hpvResult").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("hpvResult").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SCREENING_RESULT_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(hpvResult));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(hpvTreatment)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("hpvTreatment").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("hpvTreatment").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_TREATMENT_PERFORMED_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(hpvTreatment));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(papResult)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("papResult").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("papResult").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SCREENING_RESULT_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(papResult));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(screeningType)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("screeningType").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("screeningType").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SCREENING_THROUGH_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(screeningType));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(treatmentType)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("treatmentType").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("treatmentType").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter, BotswanaEmrUtils
			                .getConcept(BotswanaEmrConstants.CERVICAL_TREATMENT_TYPE_PERFORMED_CONCEPT_SCREENED_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(treatmentType));
			encounter.addObs(obs);
		}
		
		if (StringUtils.isNotEmpty(treatmentDate)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("treatmentDate").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("treatmentDate").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_TREATMENT_DATE_CONCEPT_SCREENED_UUID));
			obs.setValueDatetime(BotswanaEmrUtils.formatDateFromString(treatmentDate, "YYYY-MM-dd"));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(papAbnormalDetails)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("papAbnormalDetails").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("papAbnormalDetails").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ABNORMAL_RESULTS_CONCEPT_SCREENED_UUID));
			obs.setValueText(papAbnormalDetails);
			encounter.addObs(obs);
		}
		
		if (encounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(encounter);
		}
		
	}
	
	public static Obs obsExistWithConcept(Encounter encounter, String uuid) {
		for (Obs obs : encounter.getObs()) {
			if (obs.getConcept() != null && uuid.equals(obs.getConcept().getUuid())) {
				return obs;
			}
		}
		return BotswanaEmrUtils.creatObs(encounter, BotswanaEmrUtils.getConcept(uuid));
	}
}
