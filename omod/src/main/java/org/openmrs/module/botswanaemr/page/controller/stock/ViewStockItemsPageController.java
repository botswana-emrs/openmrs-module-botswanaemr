/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockTake;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ViewStockItemsPageController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void controller(@RequestParam(value = "stockRoomId") Integer stockRoomId, PageModel model,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) {
		if (uiSessionContext.getCurrentUser() != null) {
			
			Set<SimplifiedStockTake> simplifiedStockList = new HashSet<>();
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			List<ItemStock> itemStockList = BotswanaInventoryContext.getStockRoomDataService().getItemsByRoom(stockroom,
			    null);
			
			for (ItemStock itemStock : itemStockList) {
				SimplifiedStockTake stockTake = new SimplifiedStockTake();
				stockTake.setItemCode(
				    itemStock.getItem().getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
				stockTake.setItemName(itemStock.getItem().getName());
				ItemStockDetail itemStockDetail = itemStock.getDetails().stream().filter(i -> i.getExpiration() != null)
				        .min(Comparator.comparing(ItemStockDetail::getExpiration)).orElse(null);
				if (itemStockDetail != null) {
					Date expiryDate = itemStockDetail.getExpiration();
					stockTake.setExpiryDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(expiryDate));
				} else {
					stockTake.setExpiryDate("");
				}
				
				// stockTake.setTodayDate(BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
				// stockTake.setItemStockUuid(itemStock.getUuid());
				int qty = itemStock.getQuantity();
				stockTake.setItemStockQuantity(qty);
				
				simplifiedStockList.add(stockTake);
			}
			
			model.addAttribute("simplifiedStockList", simplifiedStockList);
			model.addAttribute("selectedStockRoomId", stockroom);
		} else {
			model.addAttribute("simplifiedStockList", "");
		}
	}
}
