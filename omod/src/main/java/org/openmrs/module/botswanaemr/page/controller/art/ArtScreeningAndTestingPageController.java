/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.art;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.RelationshipType;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.context.AppContextModel;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appframework.domain.Extension;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.page.controller.PatientProfilePageController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.module.coreapps.contextmodel.PatientContextModel;
import org.openmrs.module.emrapi.patient.PatientDomainWrapper;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.InjectBeans;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ADMISSION_SIMPLE_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ART_ENROLMENT_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ART_PROGRAM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DISCHARGE_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_LAB_REQUEST_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_LAB_RESULT_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TRANSFER_WITHIN_HOSPITAL_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TRIAGE_SCREENING_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_NOTE_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VITALS_FORM_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterTypeSearchCriteria;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getHivEncounterForms;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLastDiscontinuedPatientProgram;

public class ArtScreeningAndTestingPageController extends PatientProfilePageController {
	
	public void controller(PageModel model, @RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "dashboard") String dashboard,
	        @RequestParam(required = false, value = "app") AppDescriptor app,
	        @InjectBeans PatientDomainWrapper patientDomainWrapper,
	        @SpringBean("programWorkflowService") ProgramWorkflowService programWorkflowService,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("appFrameworkService") AppFrameworkService appFrameworkService,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, UiUtils ui, UiSessionContext sessionContext) {
		
		if (StringUtils.isEmpty(dashboard)) {
			dashboard = "patientDashboard";
		}
		
		patientDomainWrapper.setPatient(patient);
		model.addAttribute("patient", patientDomainWrapper);
		model.addAttribute("app", app);
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		if (visit == null) {
			visit = currentPatientVisit;
		}
		
		model.addAttribute("activeVisit", visit);
		
		AppContextModel contextModel = sessionContext.generateAppContextModel();
		contextModel.put("patient", new PatientContextModel(patient));
		contextModel.put("patientId", patient.getUuid());
		contextModel.put("visit", visit);
		
		List<Extension> includeFragments = appFrameworkService.getExtensionsForCurrentUser(dashboard + ".includeFragments",
		    contextModel);
		Collections.sort(includeFragments);
		
		Program hivProgram = programWorkflowService.getProgramByUuid(ART_PROGRAM_UUID);
		
		List<PatientProgram> patientPrograms = programWorkflowService.getPatientPrograms(patient, hivProgram, null, null,
		    null, null, false);
		
		model.addAttribute("enrolled", patientPrograms.size() > 0);
		
		model.addAttribute("patientPrograms", patientPrograms);
		
		model.addAttribute("includeFragments", includeFragments);
		
		model.addAttribute("baseDashboardUrl", coreAppsProperties.getDashboardUrl());
		
		model.addAttribute("dashboard", dashboard);
		
		model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
		
		model.addAttribute("breadCrumbsFormatters",
		    Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
		
		model.addAttribute("appContextModel", contextModel);
		
		model.addAttribute("hasVisit", visit != null);
		
		EncounterType labRequestEncounterType = encounterService.getEncounterTypeByUuid(LAB_RESULT_ENCOUNTER_TYPE_UUID);
		
		model.addAttribute("educationLevels", BotswanaEmrUtils.getRequiredEducationLevels());
		List<Concept> employmentSectors = getEmploymentSectors();
		model.addAttribute("employmentSectors", employmentSectors);
		
		List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
		HashSet<String> hashSet = new HashSet<>();
		for (RelationshipType relationshipType : relationshipTypes) {
			hashSet.add(relationshipType.getaIsToB());
			hashSet.add(relationshipType.getbIsToA());
		}
		List<String> relationships = new ArrayList<>(hashSet);
		Collections.sort(relationships);
		model.addAttribute("relationships", relationships);
		
		model.addAttribute("eligible",
		    Programs.eligibleForProgramEnrollment(patient,
		        getEncounterTypeSearchCriteria(patient, Collections.singletonList(labRequestEncounterType)),
		        getLastDiscontinuedPatientProgram(patient, hivProgram), hivProgram));
		
		BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
		
		model.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		
		returnUrl = ui.encodeForSafeURL(
		    HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "art/artScreeningAndTesting", patient, visit, ui));
		
		getHtmlFormToLoad(model, patient, returnUrl, htmlFormEntryService, formService, visit, encounterService);
		
		Form hivLabRequestForm = formService.getFormByUuid(HIV_LAB_REQUEST_FORM_UUID);
		HtmlForm hivTestRequestHtmlForm = htmlFormEntryService.getHtmlFormByForm(hivLabRequestForm);
		Form hivLabResultForm = formService.getFormByUuid(HIV_LAB_RESULT_FORM_UUID);
		HtmlForm hivTestResultHtmlForm = htmlFormEntryService.getHtmlFormByForm(hivLabResultForm);
		
		List<HtmlForm> hivTestingAndResultsForms = Arrays.asList(hivTestRequestHtmlForm, hivTestResultHtmlForm);
		
		model.addAttribute("availableTestingForms", hivTestingAndResultsForms);
		
		List<HtmlForm> hivEncounterForms = getHivEncounterForms(patient, htmlFormEntryService, formService);
		
		model.addAttribute("availableArtForms", hivEncounterForms);
		
		List<String> listOfFormsToExcludeUuids = Arrays.asList(VITALS_FORM_UUID, VISIT_NOTE_FORM_UUID,
		    ADMISSION_SIMPLE_FORM_UUID, DISCHARGE_FORM_UUID, TRANSFER_WITHIN_HOSPITAL_FORM_UUID, TRIAGE_SCREENING_FORM_UUID);
		
		List<Encounter> filledEncounterForPatientPerTheCurrentVisit = getHivEncounters(patient, encounterService,
		    Collections.singletonList(currentPatientVisit), hivEncounterForms);
		
		model.addAttribute("encounters", getHivEncounters(patient, encounterService, hivEncounterForms));
		
		List<Encounter> hivTestingEncounters = encounterService.getEncounters(new EncounterSearchCriteria(patient, null,
		        null, null, null, Arrays.asList(hivLabRequestForm, hivLabResultForm), null, null, null, null, false));
		
		model.addAttribute("hivTestingEncounters",
		    hivTestingEncounters == null ? new ArrayList<Encounter>() : hivTestingEncounters);
		
		model.addAttribute("hivTreatmentForms",
		    filledEncounterForPatientPerTheCurrentVisit.stream()
		            .filter(e -> !(e.getForm().equals(hivTestRequestHtmlForm) || e.getForm().equals(hivLabResultForm)))
		            .collect(Collectors.toList()));
		
		if (returnUrl.equals("")) {
			SimpleObject returnParams = SimpleObject.create("patientId", patient.getId(), "visitId", visit.getId(),
			    "programId", hivProgram.getUuid());
			model.addAttribute("returnUrl", ui.pageLink("botswanaemr", "programs/programs", returnParams));
		} else {
			model.addAttribute("returnUrl", returnUrl);
		}
	}
	
	public static List<Encounter> getHivEncounters(Patient patient, EncounterService encounterService,
	        List<Visit> currentPatientVisit, List<HtmlForm> hivEncounterForms) {
		List<Form> hivForms = hivEncounterForms.stream().map(HtmlForm::getForm).collect(Collectors.toList());
		
		return encounterService.getEncounters(new EncounterSearchCriteria(patient, null, null, null, null, hivForms, null,
		        null, null, currentPatientVisit, false));
	}
	
	public static List<Encounter> getHivEncounters(Patient patient, EncounterService encounterService,
	        List<HtmlForm> hivEncounterForms) {
		
		return getHivEncounters(patient, encounterService, null, hivEncounterForms);
		
	}
	
	private void getHtmlFormToLoad(PageModel model, Patient patient, String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, Visit currentPatientVisit,
	        @SpringBean("encounterService") EncounterService encounterService) {
		Form hlrForm = formService.getFormByUuid(HIV_LAB_REQUEST_FORM_UUID);
		HtmlForm hivLabRequestForm = null;
		HtmlForm hivInitialConsultationForm;
		if (hlrForm != null) {
			hivLabRequestForm = htmlFormEntryService.getHtmlFormByForm(hlrForm);
		}
		
		if (patient.getAge() >= 16) {
			Form haicForm = formService.getFormByUuid(HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID);
			hivInitialConsultationForm = htmlFormEntryService.getHtmlFormByForm(haicForm);
		} else {
			Form hpicForm = formService.getFormByUuid(HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID);
			hivInitialConsultationForm = htmlFormEntryService.getHtmlFormByForm(hpicForm);
		}
		
		Form enrolmentForm = formService.getFormByUuid(ART_ENROLMENT_FORM_UUID);
		HtmlForm artEnrolmentForm = htmlFormEntryService.getHtmlFormByForm(enrolmentForm);
		
		model.addAttribute("artEnrolmentForm", artEnrolmentForm);
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("formPatient", patient);
		model.addAttribute("hivLabRequestForm", hivLabRequestForm);
		model.addAttribute("hivInitialConsultationForm", hivInitialConsultationForm);
		model.addAttribute("hivLabRequestEncounter", getHivLabRequestEncounter(currentPatientVisit, encounterService));
	}
	
	private Encounter getHivLabRequestEncounter(Visit visit,
	        @SpringBean("encounterService") EncounterService encounterService) {
		List<Encounter> encounterList = encounterService.getEncountersByVisit(visit, false);
		List<Encounter> list = new ArrayList<>();
		for (Encounter e : encounterList) {
			if (e.getEncounterType().getUuid().equals(BotswanaEmrConstants.LAB_ORDER_ENCOUNTER_TYPE_UUID)) {
				list.add(e);
			}
		}
		encounterList = list;
		if (encounterList.size() > 0) {
			return encounterList.get(0);
		}
		return null;
	}
}
