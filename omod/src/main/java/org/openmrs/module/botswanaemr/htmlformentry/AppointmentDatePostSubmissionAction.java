/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import java.util.Set;

@Slf4j
public class AppointmentDatePostSubmissionAction implements CustomFormSubmissionAction {
	
	public static final String LAB_ORDER_ENCOUNTER_TYPE_UUID = "37010a9e-0840-11ee-be56-0242ac120002";
	
	public static final String TPT_ENCOUNTER_TYPE_UUID = "ea1708a1-0c88-4a72-95d0-646b8e0445e4";
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		Set<Obs> obsList = formEntrySession.getEncounter().getObs();
		Obs artNextDateObs = obsList.stream()
		        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.ART_NEXT_DATE_CONCEPT_UUID)).findFirst()
		        .orElse(null);
		
		//create appointment when Date of next ART appointment: field is not blank
		if (artNextDateObs != null) {
			String appointmentReason = "ART Followup";
			if (formEntrySession.getEncounter().getEncounterType().getUuid().equals(LAB_ORDER_ENCOUNTER_TYPE_UUID)) {
				appointmentReason = "LAB";
			} else if (formEntrySession.getEncounter().getEncounterType().getUuid().equals(TPT_ENCOUNTER_TYPE_UUID)) {
				appointmentReason = "TPT Follow up";
			}
			
			BotswanaEmrUtils.createAppointment(formEntrySession.getPatient(), formEntrySession.getEncounter().getLocation(),
			    artNextDateObs.getValueDate(), appointmentReason);
		}
	}
	
}
