/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.Location;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.model.Institution;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class DirectReceiptPageController {
	
	public void controller(PageModel model, UiSessionContext uiSessionContext) {
		List<Stockroom> stockrooms = new ArrayList<>();
		List<Stockroom> bulkStockrooms = new ArrayList<>(
		        StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation()));
		List<Stockroom> distributionStockrooms = new ArrayList<>(
		        StockUtils.getHtsDistributionStockrooms(uiSessionContext.getSessionLocation()));
		List<Institution> institutions = BotswanaInventoryContext.getInstitutionDataService().getAll(false);

		stockrooms.addAll(bulkStockrooms);
		stockrooms.addAll(distributionStockrooms);

		model.addAttribute("mainStockrooms", stockrooms);
		model.addAttribute("institutions", institutions);
		
	}
	
	public SimpleObject getMainStockRoom(@RequestParam(value = "facility") Location location) {
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("data", StockUtils.getMainStockroom(location));
		return simpleObject;
	}
	
}
