/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

public class VoidPaymentFragmentController {
	
	private final BotswanaEmrService service;
	
	public VoidPaymentFragmentController() {
		service = Context.getService(BotswanaEmrService.class);
	}
	
	public void controller() {
		
	}
	
	public void voidPaymentTransaction(@RequestParam(value = "vRegistrationId") int registrationId,
	        @RequestParam(value = "voidingReason") String voidingReason) {
		
		Registration registration = service.getPayment(registrationId);
		registration.setVoided(true);
		registration.setVoidReason(voidingReason);
		registration.setDateVoided(new Date());
		registration.setVoidedBy(Context.getAuthenticatedUser());
		
		service.savePayment(registration);
		
	}
}
