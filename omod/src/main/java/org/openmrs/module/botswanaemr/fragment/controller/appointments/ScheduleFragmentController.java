/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.appointments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.AppointmentType;
import org.openmrs.module.appointmentscheduling.TimeSlot;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDefaultAppointmentType;

@Slf4j
@SuppressWarnings("unused")
public class ScheduleFragmentController {
	
	public void controller(@SpringBean FragmentModel model, UiSessionContext uiSessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient) {
		//model.addAttribute("providerList", getProviderList());
		model.addAttribute("appointmentTypes", getAppointmentTypeList());
		model.addAttribute("location", uiSessionContext.getSessionLocation());
		model.addAttribute("patient", patient);
		model.addAttribute("events", this.getCalenderEventsForProvider());
	}
	
	public String createAppointment(@RequestParam(value = "appointmentDate", required = false) String appointmentDate,
	        @RequestParam(value = "startTime", required = false) String startTime,
	        @RequestParam(value = "endTime", required = false) String endTime,
	        @RequestParam(value = "flow", required = false) String flow,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "notes", required = false) String notes, UiSessionContext uiSessionContext,
	        Integer locationId) throws ParseException {
		
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		LocationService locationService = Context.getLocationService();
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		Appointment appointment = new Appointment();
		appointment.setDateCreated(new Date());
		appointment.setPatient(patient);
		AppointmentType defaultAppointmentType = getDefaultAppointmentType();
		if (defaultAppointmentType != null) {
			appointment.setAppointmentType(defaultAppointmentType);
		}
		appointment.setDateCreated(new Date());
		appointment.setCreator(Context.getAuthenticatedUser());
		
		if (StringUtils.isNotBlank(notes)) {
			appointment.setReason(notes);
		}
		if (StringUtils.isNotBlank(flow)) {
			appointment.setStatus(Appointment.AppointmentStatus.WALKIN);
			//Start a new visit
			VisitType facilityVisitType = Context.getVisitService()
			        .getVisitTypeByUuid(BotswanaEmrConstants.FACILITY_VISIT_VISIT_TYPE_UUID);
			
			Visit visit = new Visit(patient, facilityVisitType, new Date());
			visit.setLocation(locationService.getLocation(locationId));
			visit = Context.getVisitService().saveVisit(visit);
			appointment.setVisit(visit);
		} else {
			//add this time slot to the appointment
			if (StringUtils.isNotBlank(appointmentDate) && StringUtils.isNotBlank(startTime)
			        && StringUtils.isNotBlank(endTime)) {
				String startDateStr = appointmentDate + " " + startTime;
				String endDateStr = appointmentDate + " " + endTime;
				Date startDate = BotswanaEmrUtils.formatDateFromStringWithTime(startDateStr);
				Date endDate = BotswanaEmrUtils.formatDateFromStringWithTime(endDateStr);
				TimeSlot appointmentTimeSlot = BotswanaEmrUtils.getAppointmentTimeSlot(startDate, endDate, provider,
				    locationId == null ? uiSessionContext.getSessionLocation() : locationService.getLocation(locationId),
				    defaultAppointmentType);
				appointmentService.saveAppointmentBlock(appointmentTimeSlot.getAppointmentBlock());
				appointmentService.saveTimeSlot(appointmentTimeSlot);
				appointment.setTimeSlot(appointmentTimeSlot);
				appointment.setStatus(Appointment.AppointmentStatus.SCHEDULED);
				appointmentService.saveAppointment(appointment);
			}
		}
		return null;
	}
	
	public Appointment getAppointmentForPatient(
	        @RequestParam(value = "appointmentId", required = false) Integer appointmentId,
	        @RequestParam(value = "patientId", required = false) Integer patientId) {
		Appointment appointment = null;
		
		AppointmentService as = Context.getService(AppointmentService.class);
		if (appointmentId != null)
			appointment = as.getAppointment(appointmentId);
		
		if (appointment == null) {
			appointment = new Appointment();
			if (patientId != null)
				appointment.setPatient(Context.getPatientService().getPatient(patientId));
		}
		
		return appointment;
	}
	
	public List<AppointmentType> getAppointmentTypeList() {
		return Context.getService(AppointmentService.class).getAllAppointmentTypesSorted(false);
	}
	
	public List<Provider> getProviderList() {
		return Context.getService(AppointmentService.class).getAllProvidersSorted(false);
	}
	
	public List<Appointment> getAppointmentsByLoggedInProvider() {
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		return new ArrayList<>(Context.getService(AppointmentService.class).getAppointmentsByConstraints(new Date(), null,
		    null, provider, null, null));
	}
	
	public List<Event> getCalenderEventsForProvider() {
		List<Event> events = new ArrayList<>();
		for (Appointment appointment : this.getAppointmentsByLoggedInProvider()) {
			events.add(new Event(
			        appointment.getPatient().getPerson().getPersonName().getFullName() + " "
			                + (appointment.getReason() != null ? appointment.getReason() : ""),
			        appointment.getTimeSlot().getStartDate(), appointment.getTimeSlot().getEndDate()));
		}
		return events;
	}
	
	public List<SimpleObject> getAppointmentByTimeSlotAndProvider(@RequestParam("fromDate") Date startDate,
	        @RequestParam("toDate") Date toDate, @RequestParam("provider") Provider provider, UiUtils ui) {
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		
		List<Appointment> appointmentList = new ArrayList<>(
		        appointmentService.getAppointmentsByConstraints(startDate, toDate, null, provider, null, null));
		return SimpleObject.fromCollection(appointmentList, ui, "id", "patient", "status", "appointmentType");
		
	}
	
	/*
	 *This method is intended to be used when a scheduled appointment is clicked on the calendar
	 * The dialog box that will pop up will have several appointment status that can be adjusted to
	 *
	 */
	public String editAppointments(@RequestParam("appointmentId") Appointment appointment,
	        @RequestParam("appointmentStatusId") Appointment.AppointmentStatus appointmentStatus) {
		
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		if (appointment != null) {
			appointmentService.changeAppointmentStatus(appointment, appointmentStatus);
			
		}
		return null;
	}
	
	public String deleteAppointments(@RequestParam("appointmentId") Appointment appointment,
	        @RequestParam("appointmentStatusId") Appointment.AppointmentStatus appointmentStatus) {
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		if (appointment != null) {
			appointmentService.purgeAppointment(appointment);
			
		}
		
		return null;
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Event implements Serializable {
		
		private String title;
		
		private Date startDate;
		
		private Date endDate;
	}
	
}
