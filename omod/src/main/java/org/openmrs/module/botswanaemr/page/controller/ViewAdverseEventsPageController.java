/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.AdverseEvents;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ViewAdverseEventsPageController {
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "encounterFormUuid", required = false) String encounterFormUuid)
	        throws JsonProcessingException {
		
		encounterFormUuid = BotswanaEmrConstants.ADVERSE_EVENTS_FORM_UUID;
		
		List<Encounter> adverseEventsEncounters = getAdverseEventsEncounters(patient, encounterFormUuid);
		List<AdverseEvents> adverseEventsList = adverseEventsEncounters.stream()
		        .flatMap(encounter -> mapToAdverseEvents(encounter).stream()).collect(Collectors.toList());
		
		String adverseEventsListJson = new ObjectMapper().writeValueAsString(adverseEventsList);
		
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("adverseEventsListJson", adverseEventsListJson);
		pageModel.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
		pageModel.addAttribute("breadCrumbsFormatters",
		    Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
	}
	
	private List<Encounter> getAdverseEventsEncounters(Patient patient, String encounterFormUuid) {
		EncounterType encounterType = BotswanaEmrUtils.getEncounterType(encounterFormUuid);
		return BotswanaEmrUtils.getEncountersByPatient(patient, encounterType, null, null, null);
	}
	
	private List<AdverseEvents> mapToAdverseEvents(Encounter encounter) {
		AdverseEvents adverseEvents = new AdverseEvents();
		adverseEvents.setVisitDate(encounter.getEncounterDatetime());
		adverseEvents.setEncounterId(encounter.getId());
		
		for (Obs obs : encounter.getAllObs()) {
			String conceptName = obs.getConcept().getName().getName();
			
			switch (conceptName) {
				case BotswanaEmrConstants.CONCEPT_ADVERSE_EVENT:
					adverseEvents.setAdverseReaction(obs.getValueAsString(Context.getLocale()));
					break;
				case BotswanaEmrConstants.CONCEPT_DATE_ONSET:
					adverseEvents.setOnset(Optional.ofNullable(obs.getValueDatetime()).map(Date::toString).orElse(null));
					break;
				case BotswanaEmrConstants.CONCEPT_REPORT_DATE:
					adverseEvents.setDateReported(obs.getValueDatetime());
					break;
			}
		}
		return mapToAdverseEventsList(encounter);
	}
	
	private List<AdverseEvents> mapToAdverseEventsList(Encounter encounter) {
		List<AdverseEvents> adverseEventsList = new ArrayList<>();
		
		AdverseEvents currentAdverseEvent = null;
		for (Obs obs : encounter.getAllObs()) {
			String conceptName = obs.getConcept().getName().getName();
			
			switch (conceptName) {
				case BotswanaEmrConstants.CONCEPT_ADVERSE_EVENT:
					if (currentAdverseEvent != null) {
						adverseEventsList.add(currentAdverseEvent);
					}
					currentAdverseEvent = new AdverseEvents();
					currentAdverseEvent.setVisitDate(encounter.getEncounterDatetime());
					currentAdverseEvent.setEncounterId(encounter.getId());
					currentAdverseEvent.setAdverseReaction(obs.getValueAsString(Context.getLocale()));
					break;
				case BotswanaEmrConstants.CONCEPT_DATE_ONSET:
					if (currentAdverseEvent != null) {
						currentAdverseEvent
						        .setOnset(Optional.ofNullable(obs.getValueDatetime()).map(Date::toString).orElse(null));
					}
					break;
				case BotswanaEmrConstants.CONCEPT_REPORT_DATE:
					if (currentAdverseEvent != null) {
						currentAdverseEvent.setDateReported(obs.getValueDatetime());
					}
					break;
			}
		}
		
		if (currentAdverseEvent != null) {
			adverseEventsList.add(currentAdverseEvent);
		}
		
		return adverseEventsList;
	}
	
	public List<PersonAttribute> getBreadCrumbsDetails(Patient patient) {
		String breadCrumbsDetailsAttrTypeUuids = Context.getAdministrationService()
		        .getGlobalProperty("breadCrumbs.details.personAttr.uuids");
		List<PersonAttribute> personNamePersonAttrs = new ArrayList<PersonAttribute>();
		if (StringUtils.isNotBlank(breadCrumbsDetailsAttrTypeUuids)) {
			for (String breadCrumbDetailAttrTypeUuid : breadCrumbsDetailsAttrTypeUuids.split(",")) {
				PersonAttributeType pat = Context.getPersonService()
				        .getPersonAttributeTypeByUuid(breadCrumbDetailAttrTypeUuid);
				PersonAttribute pa = patient.getAttribute(pat);
				if (pa != null) {
					personNamePersonAttrs.add(pa);
				}
			}
		}
		return CollectionUtils.isNotEmpty(personNamePersonAttrs) ? personNamePersonAttrs : null;
	}
}
