/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.appointments;

import org.openmrs.Location;
import org.openmrs.Provider;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.page.PageModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PatientFollowupPageController {
	
	AppointmentService appointmentService = Context.getService(AppointmentService.class);
	
	public Object controller(final PageModel model, UiSessionContext sessionContext) throws ParseException {
		Date currentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.WEEK_OF_YEAR, -3);
		List<Appointment> appointments = getMissedAppointments(new Date(), calendar.getTime(),
		    sessionContext.getSessionLocation(), null);
		
		model.addAttribute("appointmentsList", appointments);
		return null;
	}
	
	public List<SimpleObject> getAllMissedAppointments() {
		
		return null;
	}
	
	public List<Appointment> getMissedAppointments(Date fromDate, Date toDate, Location location, Provider provider) {
		
		return appointmentService.getAppointmentsByConstraints(fromDate, toDate, location, provider, null,
		    Appointment.AppointmentStatus.SCHEDULED);
	}
	
	public List<SimpleObject> getDailyAppointmentSummaryCount() {
		List<SimpleObject> days = new ArrayList<>();
		SimpleObject so = new SimpleObject();
		so.put("date", new Date());
		so.put("scheduled", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.SCHEDULED).size());
		so.put("completed", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.COMPLETED).size());
		so.put("pending", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.MISSED).size());
		days.add(so);
		
		return days;
	}
}
