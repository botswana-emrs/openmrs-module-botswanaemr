/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_REGISTRATION_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.QUEUING_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VMMC_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FEMALE_GENDER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.POST_REGISTRATION_LOCATION_TAG_NAME;

public class ReassignPatientToQueueFragmentController {
	
	protected final Log log = LogFactory.getLog(ReassignPatientToQueueFragmentController.class);
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	private final LocationService locationService = Context.getService(LocationService.class);
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext,
	        @RequestParam("patientId") Patient patient) {
		
		List<Location> queueLocationsList = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(QUEUING_LOCATION_TAG_NAME));
		Location vmmcLocation = locationService.getLocationByUuid(VMMC_PORTAL_UUID);
		
		// Facility filtering
		String currentServicePointUuid = String
		        .valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		Location currentServicePoint = locationService.getLocationByUuid(currentServicePointUuid);
		
		Location patientRegistrationPortal = locationService.getLocationByUuid(PATIENT_REGISTRATION_PORTAL_UUID);
		
		List<Location> queueLocationsBeforeScreening = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(POST_REGISTRATION_LOCATION_TAG_NAME)).stream()
		        .filter(location -> !location.equals(currentServicePoint)).collect(Collectors.toList());
		
		List<Location> queueLocationsAfterScreening = new ArrayList<>(
		        locationService.getLocationsByTag(locationService.getLocationTagByName(QUEUING_LOCATION_TAG_NAME)));
		
		//if patient is female filter out VMMC in the locations
		if (patient.getGender().equalsIgnoreCase(FEMALE_GENDER)) {
			queueLocationsList = queueLocationsList.stream().filter(location -> !location.equals(vmmcLocation))
			        .collect(Collectors.toList());
			
			queueLocationsBeforeScreening = queueLocationsBeforeScreening.stream()
			        .filter(location -> !location.equals(vmmcLocation)).collect(Collectors.toList());
			
			queueLocationsAfterScreening = queueLocationsAfterScreening.stream()
			        .filter(location -> !location.equals(vmmcLocation)).collect(Collectors.toList());
		}
		
		// check if current service point is at the registration
		if (currentServicePoint.equals(patientRegistrationPortal)) {
			boolean isScreeningCompleted = false;
			if (isScreeningCompleted) {
				queueLocationsList = new ArrayList<>(queueLocationsAfterScreening);
			} else {
				queueLocationsList = new ArrayList<>(queueLocationsBeforeScreening);
			}
		} else {
			queueLocationsList = new ArrayList<>(queueLocationsAfterScreening);
		}
		
		List<String> patientQueueLocationsUuidList = new ArrayList<>();
		for (Location queueLocation : queueLocationsBeforeScreening) {
			PatientQueue patientQueue = getPatientQueue(patient, queueLocation);
			if (patientQueue != null) {
				patientQueueLocationsUuidList.add(patientQueue.getLocationTo().getUuid());
			}
		}
		if (!patientQueueLocationsUuidList.isEmpty()) {
			fragmentModel.addAttribute("patientQueueLocationsUuidList", patientQueueLocationsUuidList);
		} else {
			fragmentModel.addAttribute("patientQueueLocationsUuidList", Collections.EMPTY_LIST);
		}
		
		fragmentModel.addAttribute("patientRegistrationPortal", patientRegistrationPortal);
		fragmentModel.addAttribute("queueLocations", queueLocationsList);
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
	}
	
	public void reassignPatientToQueueLocation(@RequestParam(value = "patientId") Patient patient,
	        @RequestParam(value = "queueRoomUuid") String queueRoomUuid,
	        @RequestParam(value = "locationUuid") String locationUuid,
	        @RequestParam(value = "patientQueueId") PatientQueue patientQueue) throws IOException {
		
		Location queueRoom = locationService.getLocationByUuid(queueRoomUuid);
		if (patientQueue != null) {
			if (patientQueue.getStatus() == PatientQueue.Status.PENDING) {
				patientQueue.setLocationTo(queueRoom);
				if (queueRoom != null) {
					patientQueue.setQueueRoom(queueRoom);
					patientQueue.setLocationTo(queueRoom);
				}
			} else {
				BotswanaEmrUtils.createOrUpdatePatientQueue(patient, patientQueue.getLocationFrom(),queueRoom, patientQueue.getEncounter().getVisit(), patient.getCreator(), patientQueueingService);
			}
			Errors errors = new BindException(patientQueue, "patientQueue");
			try {
				patientQueueingService.savePatientQue(patientQueue);
				log.info("\n Saved patientQueue \n" + patientQueue);
			}
			catch (Exception e) {
				log.warn("\n An error occurred while saving patient's queue \n", e);
				log.error("\n" + errors + "\n");
			}
		}
	}
	
	private PatientQueue getPatientQueue(Patient patient, Location location) {
		return patientQueueingService.getIncompletePatientQueue(patient, location);
	}
}
