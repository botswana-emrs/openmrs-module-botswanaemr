/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.Patient;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ArtPatientPoolFragmentController {
	
	private final List<String> artServiceTypes = new ArrayList<String>() {
		
		{
			add(BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PREP_FOLLOW_UP_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PEP_FOLLOW_UP_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.ART_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.ART_FOLLOW_UP_VISIT_TYPE_UUID);
		}
	};
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		
		fragmentModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		fragmentModel.addAttribute("providerList", providerList);
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program artProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.ART_PROGRAM_UUID);
		fragmentModel.addAttribute("artProgramId", artProgram.getUuid());
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		Set<VisitType> visitTypes = new LinkedHashSet<>();
		VisitService visitService = Context.getVisitService();
		
		for (String visitTypeUuid : artServiceTypes) {
			visitTypes.add(visitService.getVisitTypeByUuid(visitTypeUuid));
		}
		visitTypes.remove(null);
		fragmentModel.addAttribute("visitTypes", visitTypes);
	}
	
	public SimpleObject checkPatientActiveVisit(@RequestParam(value = "patientId") Patient patient,
	        UiSessionContext sessionContext) {
		Visit visit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		if (visit != null) {
			return SimpleObject.create("active", true, "visitType", visit.getVisitType().getName());
		} else {
			return SimpleObject.create("active", false);
		}
	}
	
	public SimpleObject startArtServiceByType(@RequestParam(value = "serviceType") Integer serviceType,
	        @RequestParam(value = "patientId") Patient patient,
	        @RequestParam(value = "visitId", required = false) Integer visitId, UiSessionContext sessionContext) {
		VisitType visitType = Context.getVisitService().getVisitType(serviceType);
		if (visitType != null) {
			Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
			
			if (activeVisit != null && Objects.equals(activeVisit.getVisitType().getId(), serviceType)) {
				return SimpleObject.create("status", "Successful", "patientId", patient.getPatientId(), "visitId",
				    activeVisit.getVisitId(), "serviceType", serviceType);
			} else if (activeVisit != null) {
				activeVisit.setStopDatetime(new Date());
				Context.getVisitService().saveVisit(activeVisit);
			}
			
			//No active visit with
			
			Visit visit = new Visit(patient, visitType, new Date());
			visit.setLocation(sessionContext.getSessionLocation());
			visit = Context.getVisitService().saveVisit(visit);
			return SimpleObject.create("status", "Successful", "patientId", patient.getPatientId(), "visitId",
			    visit.getVisitId(), "serviceType", serviceType);
		}
		return SimpleObject.create("status", "Fail");
	}
}
