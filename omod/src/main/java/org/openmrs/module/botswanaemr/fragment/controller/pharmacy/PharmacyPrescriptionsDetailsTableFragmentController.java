/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import java.util.ArrayList;
import java.util.List;
import org.openmrs.Encounter;
import org.openmrs.api.EncounterService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

public class PharmacyPrescriptionsDetailsTableFragmentController {
	
	private static final Logger logger = LoggerFactory.getLogger(PharmacyPrescriptionsDetailsTableFragmentController.class);
	
	public void controller(FragmentModel fragmentModel, @RequestParam("encounterId") Encounter encounter,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "orderDetailsId", required = false) Integer orderDetailsId,
	        UiSessionContext uiSessionContext) {
		
		if (encounter == null) {
			fragmentModel.addAttribute("drugOrder", new ArrayList<>());
		} else {
			fragmentModel.addAttribute("drugOrder", BotswanaEmrUtils.getActivePatientDrugOrders(encounter.getPatient()));
		}
		List<Stockroom> dispensingStockRooms = StockUtils.getDispensingStockrooms(uiSessionContext.getSessionLocation());
		fragmentModel.addAttribute("dispensingStockRooms", dispensingStockRooms);
	}
	
}
