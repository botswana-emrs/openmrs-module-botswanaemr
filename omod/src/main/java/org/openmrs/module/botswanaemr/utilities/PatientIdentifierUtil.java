/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Location;
import org.openmrs.PatientIdentifier;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.service.IdentifierSourceService;
import org.openmrs.validator.PatientIdentifierValidator;

public class PatientIdentifierUtil {
	
	private static IdentifierSource idSource;
	
	/**
	 * Logger for this class and subclasses
	 */
	protected final Log log = LogFactory.getLog(getClass());
	
	public static PatientIdentifier validateOrGenerateIdentifier(String identifierString, Location identifierLocation) {
		IdentifierSourceService identifierSourceService = getIssAndUpdateIdSource();
		identifierLocation = getIdentifierLocation(identifierLocation);
		
		// generate identifier if necessary, otherwise validate
		if (StringUtils.isBlank(identifierString)) {
			identifierString = identifierSourceService.generateIdentifier(idSource, null);
		} else {
			PatientIdentifierValidator.validateIdentifier(identifierString, idSource.getIdentifierType());
		}
		
		PatientIdentifier patientIdentifier = new PatientIdentifier(identifierString, idSource.getIdentifierType(),
		        identifierLocation);
		patientIdentifier.setPreferred(false);
		
		return patientIdentifier;
	}
	
	private static IdentifierSourceService getIssAndUpdateIdSource() {
		IdentifierSourceService identifierSourceService = Context.getService(IdentifierSourceService.class);
		if (idSource == null) {
			String sourceIdentifier = "1";
			if (StringUtils.isNumeric(sourceIdentifier)) {
				idSource = identifierSourceService.getIdentifierSource(Integer.valueOf(sourceIdentifier));
			} else {
				idSource = identifierSourceService.getIdentifierSourceByUuid(sourceIdentifier);
			}
			if (idSource == null) {
				throw new APIException("cannot find identifier source with id:" + sourceIdentifier);
			}
		}
		return identifierSourceService;
	}
	
	private static Location getIdentifierLocation(Location identifierLocation) {
		if (identifierLocation == null) {
			identifierLocation = Context.getLocationService().getDefaultLocation();
			if (identifierLocation == null) {
				throw new APIException("Failed to resolve location to associate to patient identifiers");
			}
		}
		
		return identifierLocation;
	}
}
