/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import liquibase.util.StringUtil;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.InventoryItemSimplifier;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public class AllProductsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService, UiSessionContext uiSessionContext) {
		
		List<InventoryItemSimplifier> simplifiedItems = new ArrayList<>();
		if (uiSessionContext.getCurrentUser() != null) {
			List<Item> items = iItemDataService.getAll(false, new PagingInfo(2, 10));
			
			for (Item item : items) {
				InventoryItemSimplifier inventoryItemSimplifier = InventoryItemSimplifier.simplify(item);
				simplifiedItems.add(inventoryItemSimplifier);
			}
		}
		
		fragmentModel.addAttribute("inventoryItems", simplifiedItems);
	}
	
	private String getItemAttributeValue(Set<ItemAttribute> itemAttributes, ItemAttributeType itemAttributeType) {
		for (ItemAttribute itemAttribute : itemAttributes) {
			if (Objects.equals(itemAttribute.getAttributeType(), itemAttributeType)) {
				return itemAttribute.getValue();
			}
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public SimpleObject getAllProducts(@RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "search[value]", required = false) String searchValue,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService) {
		
		page = page / pageSize + 1;
		List<Item> items;
		if (StringUtil.isEmpty(searchValue)) {
			items = iItemDataService.getAll(false, new PagingInfo(page, pageSize));
		} else {
			ItemSearch itemSearch = new ItemSearch();
			itemSearch.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.LIKE);
			itemSearch.getTemplate().setName("%" + searchValue + "%");
			items = iItemDataService.getItemsByItemSearch(itemSearch, new PagingInfo(page, pageSize));
		}
		
		List<InventoryItemSimplifier> simplifiedItems = new ArrayList<>();
		for (Item item : items) {
			InventoryItemSimplifier inventoryItemSimplifier = InventoryItemSimplifier.simplify(item);
			simplifiedItems.add(inventoryItemSimplifier);
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("draw", draw);
		int totalDisplayRecords = iItemDataService.getAll(false).size();
		simpleObject.put("iTotalDisplayRecords", totalDisplayRecords);
		simpleObject.put("iTotalRecords", totalDisplayRecords);
		simpleObject.put("aaData", simplifiedItems);
		
		return simpleObject;
	}
}
