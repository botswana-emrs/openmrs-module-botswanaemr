/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ReportSetupFragmentController {
	
	public void controller(FragmentModel model) {
		
	}
	
	public String saveMailTemplate(@RequestParam(value = "reportId", required = false) String reportId,
	        UiSessionContext uiSessionContext, @RequestParam(value = "mailFrom", required = false) String mailFrom,
	        @RequestParam(value = "mailSubject", required = false) String mailSubject,
	        @RequestParam(value = "cccList", required = false) String cccList,
	        @RequestParam(value = "bccList", required = false) String bccList,
	        @RequestParam(value = "reportName", required = false) String reportName,
	        @RequestParam(value = "mailContentText", required = false) String mailContentText, Integer locationId) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		Location location = locationId == null ? uiSessionContext.getSessionLocation()
		        : Context.getLocationService().getLocation(locationId);
		BotswanaEmrEmailReportConfig config = new BotswanaEmrEmailReportConfig();//service.getBotswanaEmrEmailReportConfigs(reportId, location).get(0);// nullity check
		if (config == null)
			config = new BotswanaEmrEmailReportConfig();
		config.setFrom(mailFrom);
		config.setMailReportId(reportId);
		config.setReportName(reportName.trim());
		config.setSubject(mailSubject);
		config.setMailContent(mailContentText);
		config.setAddOutputAsAttachment(false);
		config.setAddOutputToBody(false);
		config.setFacilityId(location);
		config.setCccList(cccList);
		config.setBccList(bccList);
		
		service.saveBotswanaEmrEmailReportConfig(config);
		
		return null;
	}
	
	public SimpleObject getMailConfig(@RequestParam(value = "reportId", required = false) String reportId, UiUtils uiUtils,
	        UiSessionContext uiSessionContext, Integer locationId) {
		
		BotswanaEmrEmailReportConfig config = (Context.getService(BotswanaEmrService.class)
		        .getBotswanaEmrEmailReportConfigs(reportId, (locationId == null ? uiSessionContext.getSessionLocation()
		                : Context.getLocationService().getLocation(locationId)))
		        .get(0));
		return SimpleObject.fromObject(config, uiUtils, "mailReportId", "reportName", "cccList", "bccList", "subject",
		    "mailContent");
	}
}
