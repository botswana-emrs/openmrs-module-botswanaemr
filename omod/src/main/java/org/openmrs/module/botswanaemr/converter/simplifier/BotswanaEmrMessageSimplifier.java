/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.stereotype.Component;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

/**
 * Converts a {@link Message} to a {@link SimpleObject}.
 */
@Component
public class BotswanaEmrMessageSimplifier extends BotswanaEmrAbstractSimplifier<Message> {
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	@Override
	protected SimpleObject simplify(Message message) {
		SimpleObject ret = new SimpleObject();
		String time = BotswanaEmrUtils.getDurationSince(message.getDate());
		if ("".equals(time)) {
			time = "1 second ago";
		}
		ret.put("id", message.getMessageId());
		ret.put("content", message.getContent());
		ret.put("author", formatPersonName(message.getAuthoredBy().getPersonName()));
		ret.put("mine", message.getAuthoredBy().equals(Context.getAuthenticatedUser()));
		ret.put("time", time);
		ret.put("discussionId", message.getDiscussion().getDiscussionId());
		return ret;
	}
}
