/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.pharmacy;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Diagnosis;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.DiagnosisService;
import org.openmrs.api.PatientService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.page.controller.PatientProfilePageController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PharmacyPrescriptionDetailsPageController {
	
	private final DiagnosisService diagnosisService = Context.getService(DiagnosisService.class);
	
	public void controller(UiUtils ui, @RequestParam("patientId") Patient patient, final PageModel model,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(required = false, value = "app") AppDescriptor app,
	        @RequestParam(required = false, value = "dashboard") String dashboard,
	        @RequestParam(required = false, value = "patientQueueId") PatientQueue patientQueue,
	        @RequestParam(required = false, value = "encounterId") Encounter orderEncounter,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("providerService") ProviderService providerService,
	        @SpringBean("conceptService") ConceptService conceptService, UiSessionContext sessionContext) {
		
		List<EncounterType> consultationEncounterTypes = BotswanaEmrUtils.getConsultationEncounterTypes();
		List<Diagnosis> diagnoses = diagnosisService.getDiagnosesByVisit(visit, false, false).stream()
		        .filter(d -> d.getVoided().equals(false))
		        .filter(d -> !consultationEncounterTypes.contains(d.getEncounter().getEncounterType()))
		        .collect(Collectors.toList());
		model.addAttribute("hasDiagnoses", diagnoses.size() > 0);
		model.addAttribute("diagnoses", diagnoses);
		model.addAttribute("patient", patient);
		model.addAttribute("encounter", orderEncounter);
		model.addAttribute("visit", visit);
		model.addAttribute("activeVisit", visit);
		model.addAttribute("allergies", patientService.getAllergies(patient));
		model.addAttribute("severity", BotswanaEmrUtils.getPatientSeverityLevel(visit));
		model.addAttribute("queueStatus",
		    (patientQueue == null) ? ""
		            : StringUtils.capitalize(StringUtils.lowerCase(
		                PatientProfilePageController.translateQueueStatus(patientQueue.getStatus()), Context.getLocale())));
		
		String prescriptionStatus = "COMPLETED";
		if (orderEncounter != null && orderEncounter.getOrders() != null) {
			for (Order order : orderEncounter.getOrders()) {
				if (order.getFulfillerStatus() != null
				        && !(order.getFulfillerStatus().name().equals(Order.FulfillerStatus.COMPLETED.name()))) {
					prescriptionStatus = "AWAITING";
					break;
				}
			}
		}
		model.addAttribute("prescriptionStatus",
		    StringUtils.capitalize(StringUtils.lowerCase(prescriptionStatus, Context.getLocale())));
		
		PersonName providerName = Context.getAuthenticatedUser().getPersonName();
		model.addAttribute("provider", providerName == null ? "" : providerName.getFullName());
		model.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
		model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
		
	}
	
	public List<PersonAttribute> getBreadCrumbsDetails(Patient patient) {
		String breadCrumbsDetailsAttrTypeUuids = Context.getAdministrationService()
		        .getGlobalProperty("breadCrumbs.details.personAttr.uuids");
		List<PersonAttribute> personNamePersonAttrs = new ArrayList<>();
		if (StringUtils.isNotBlank(breadCrumbsDetailsAttrTypeUuids)) {
			for (String breadCrumbDetailAttrTypeUuid : breadCrumbsDetailsAttrTypeUuids.split(",")) {
				PersonAttributeType pat = Context.getPersonService()
				        .getPersonAttributeTypeByUuid(breadCrumbDetailAttrTypeUuid);
				PersonAttribute pa = patient.getAttribute(pat);
				if (pa != null) {
					personNamePersonAttrs.add(pa);
				}
			}
		}
		return CollectionUtils.isNotEmpty(personNamePersonAttrs) ? personNamePersonAttrs : null;
	}
	
}
