/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaRegimenService;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import java.util.List;
import java.util.stream.Collectors;

public class AddRegimenFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiUtils ui,
	        @FragmentParam(value = "regimenCategory", required = false) String regimenCategory) {
		BotswanaRegimenService regimenService = Context.getService(BotswanaRegimenService.class);
		
		List<RegimenLine> regimenLines = regimenService.getAllRegimenLine();
		if (regimenCategory != null) {
			regimenLines = regimenLines.stream().filter(r -> r.getUuid().equals(regimenCategory))
			        .collect(Collectors.toList());
		}
		fragmentModel.addAttribute("lines", regimenLines);
	}
}
