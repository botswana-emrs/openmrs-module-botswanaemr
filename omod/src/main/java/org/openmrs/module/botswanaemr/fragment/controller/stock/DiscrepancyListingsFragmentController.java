/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationAttribute;
import org.openmrs.module.botswanaemrInventory.model.StockOperationAttributeType;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;

import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Objects;
import java.util.stream.Collectors;

public class DiscrepancyListingsFragmentController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	private static final IStockOperationAttributeTypeDataService iStockOperationAttributeTypeDataService = BotswanaInventoryContext
	        .getStockOperationAttributeTypeDataService();
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		
		fragmentModel.addAttribute("stockrooms", stockrooms);
		
	}
	
	public List<SimpleObject> getDiscrepancyListingsByStockRoom(@RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "search[value]", required = false) String searchPhrase,
	        UiSessionContext uiSessionContext) {
		
		page = page / pageSize + 1;
		List<StockOperation> stockOperations = new ArrayList<>();
		List<SimpleObject> discrepancyListings = new ArrayList<>();
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomUuid);
		
		if (stockRoom != null) {
			stockOperations = BotswanaInventoryContext.getStockOperationDataService().getOperationsByRoom(stockRoom,
			    new PagingInfo(page, pageSize));
			if (!searchPhrase.trim().isEmpty()) {
				stockOperations = BotswanaInventoryContext.getStockOperationDataService().getByNameFragment(searchPhrase,
				    false, new PagingInfo(page, pageSize));
			}
		} else {
			List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
			        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
			if (uiSessionContext.getCurrentUser() != null) {
				for (Stockroom stockroom : stockrooms) {
					List<StockOperation> stockroomOperations = BotswanaInventoryContext.getStockOperationDataService()
					        .getOperationsByRoom(stockroom, new PagingInfo(page, pageSize));
					if (!stockroomOperations.isEmpty()) {
						stockroomOperations = stockroomOperations.stream()
						        .filter(operation -> operation != null && operation.getName() != null
						                && operation.getName().equals(WellKnownOperationTypes.getReceipt().getName()))
						        .collect(Collectors.toList());
						stockOperations.addAll(stockroomOperations);
					}
				}
			}
		}
		
		for (StockOperation stockOperation : stockOperations) {
			Set<StockOperationAttribute> stockOperationAttributes = stockOperation.getAttributes();
			StockOperationAttributeType operationReceiver = iStockOperationAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.RECEIVER_NAME_ATTRIBUTE_TYPE_UUID);
			for (StockOperationItem operationItem : stockOperation.getItems()) {
				if (operationItem.getVariance() != null) {
					SimpleObject simpleObject = new SimpleObject();
					
					simpleObject.put("draw", draw);
					simpleObject.put("id", stockOperation.getId());
					simpleObject.put("itemName", operationItem.getItem().getName());
					simpleObject.put("itemId", operationItem.getItem().getId());
					simpleObject.put("enteredQuantityInStore", operationItem.getQuantity());
					simpleObject.put("batchNumber", operationItem.getBatchOperation().getOperationNumber());
					simpleObject.put("discrepancy", operationItem.getVariance());
					simpleObject.put("operationDate",
					    BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
					simpleObject.put("institution", stockOperation.getInstitution().getName());
					simpleObject.put("receiver",
					    getStockOperationAttributeValue(stockOperationAttributes, operationReceiver));
					simpleObject.put("destinationStockRoom", stockOperation.getDestination().getName());
					
					discrepancyListings.add(simpleObject);
				}
			}
		}
		
		return discrepancyListings;
	}
	
	private static String getStockOperationAttributeValue(Set<StockOperationAttribute> stockAttributes,
	        StockOperationAttributeType stockOperationAttributeType) {
		for (StockOperationAttribute stockOperationAttribute : stockAttributes) {
			if (Objects.equals(stockOperationAttribute.getAttributeType(), stockOperationAttributeType)) {
				return stockOperationAttribute.getValue();
			}
		}
		return null;
	}
}
