/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.*;
import org.openmrs.api.ConditionService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

public class EditConditionsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui) {
		
		List<Condition> conditionsList = Context.getConditionService().getActiveConditions(patient);
		model.addAttribute("conditions", conditionsList);
	}
	
	/**
	 * Fragment Action for updating and saving new patient conditions
	 */
	public void updateConditions(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data) throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ConditionService cs = Context.getConditionService();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<String> deletedConditions = mapper.readValue(arrayNode, new TypeReference<List<String>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedConditions = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newConditions = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		for (String uuid : deletedConditions) {
			Condition condition = Context.getConditionService().getConditionByUuid(uuid);
			if (condition != null) {
				condition.setVoided(true);
				cs.saveCondition(condition);
			}
		}
		
		for (SimpleObject simpleObject : newConditions) {
			saveNewCondition(simpleObject, patient);
		}
		
		for (SimpleObject simpleObject : editedConditions) {
			Condition condition = cs.getConditionByUuid((String) simpleObject.get("uuid"));
			String conditionStr = (String) simpleObject.get("condition");
			String conditionValue = condition.getCondition().getCoded() != null
			        ? condition.getCondition().getCoded().getUuid()
			        : condition.getCondition().getNonCoded();
			if (!conditionStr.equals(conditionValue)) {
				Concept concept = Context.getConceptService().getConceptByUuid(conditionStr);
				CodedOrFreeText codedOrFreeText = new CodedOrFreeText();
				if (concept != null) {
					codedOrFreeText.setCoded(concept);
				} else {
					codedOrFreeText.setNonCoded(conditionStr);
				}
				
				condition.setCondition(codedOrFreeText);
				condition.setClinicalStatus(ConditionClinicalStatus.ACTIVE);
			}
			condition.setAdditionalDetail((String) simpleObject.get("comment"));
			cs.saveCondition(condition);
		}
	}
	
	private void saveNewCondition(SimpleObject simpleObject, Patient patient) {
		String conditionStr = (String) simpleObject.get("condition");
		Concept concept = Context.getConceptService().getConceptByUuid(conditionStr);
		
		if (conditionExists(patient, concept, conditionStr)) {
			return;
		}
		
		Condition condition = new Condition();
		CodedOrFreeText codedOrFreeText = new CodedOrFreeText();
		
		if (concept != null) {
			codedOrFreeText.setCoded(concept);
			codedOrFreeText.setSpecificName(concept.getName());
		} else {
			codedOrFreeText.setNonCoded(conditionStr);
		}
		
		condition.setCondition(codedOrFreeText);
		condition.setPatient(patient);
		condition.setClinicalStatus(ConditionClinicalStatus.ACTIVE);
		
		condition.setAdditionalDetail((String) simpleObject.get("comment"));
		
		Context.getConditionService().saveCondition(condition);
	}
	
	private boolean conditionExists(Patient patient, Concept concept, String conditionName) {
		List<Condition> conditions = Context.getConditionService().getActiveConditions(patient);
		for (Condition target : conditions) {
			if (concept != null) {
				if (target.getCondition().getCoded() == concept) {
					return true;
				}
			} else {
				if (target.getCondition().getNonCoded().equals(conditionName)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Searches for diagnosis concepts
	 * 
	 * @param searchTerm the diagnosis text
	 * @param ui uiUtils
	 * @return A SimpleObject list of diagnosis
	 */
	public List<SimpleObject> searchConditions(@RequestParam(value = "searchTerm") String searchTerm, UiUtils ui) {
		List<Concept> concepts = Context.getService(BotswanaEmrService.class).searchConcept(searchTerm,
		    BotswanaEmrConstants.DIAGNOSIS_CONCEPT_CLASS);
		return SimpleObject.fromCollection(concepts, ui, "id", "name", "uuid");
	}
}
