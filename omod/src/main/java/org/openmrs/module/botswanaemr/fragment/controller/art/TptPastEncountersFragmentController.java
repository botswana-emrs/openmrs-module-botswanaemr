/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TptPastEncountersFragmentController {
	
	public void controller(FragmentModel fragmentModel, @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("formService") FormService formService, @RequestParam("patientId") Patient patient) {
		Form artTptRegisterForm = formService.getFormByUuid(BotswanaEmrConstants.ART_TPT_REGISTER_FORM_UUID);
		
		List<Encounter> artTptRegisterEncounters = encounterService.getEncounters(new EncounterSearchCriteria(patient, null,
		        null, null, null, Collections.singletonList(artTptRegisterForm), null, null, null, null, false));
		
		// Ensuring artTptRegisterEncounters is not null and has at least three encounters
		List<Encounter> lastThreeEncounters = artTptRegisterEncounters.stream()
		        .skip(Math.max(0, artTptRegisterEncounters.size() - 3)).collect(Collectors.toList());
		
		fragmentModel.addAttribute("tptContraindicationConceptUuid", BotswanaEmrConstants.TPT_CONTRAINDICATION_CONCEPT_UUID);
		fragmentModel.addAttribute("tptEligibleConceptUuid", BotswanaEmrConstants.TPT_ELIGIBLE_CONCEPT_UUID);
		
		fragmentModel.addAttribute("artTptRegisterEncounters", lastThreeEncounters);
		
	}
}
