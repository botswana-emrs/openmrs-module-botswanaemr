/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import java.util.Date;

import org.openmrs.Patient;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonName;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class PrintVitalsPageController {
	
	public void controller(PageModel model, @RequestParam("patientId") Patient patient, @RequestParam("visitId") Visit visit,
	        UiSessionContext sessionContext, UiUtils ui) {
		model.addAttribute("activePatient", patient);
		model.addAttribute("visit", visit);
		model.addAttribute("creatorNames",
		    BotswanaEmrUtils.formatPersonName(sessionContext.getCurrentUser().getPersonName()));
		model.addAttribute("creatorEmail", sessionContext.getCurrentUser().getEmail());
		model.addAttribute("facility", visit.getLocation().getName());
		
		PersonName patientName = patient.getPersonName();
		if (patientName != null) {
			model.addAttribute("patientNames", patientName.getFullName());
		} else {
			model.addAttribute("patientNames", "");
		}
		
		if (patient.getBirthdate() != null) {
			model.addAttribute("patientAge", patient.getAge());
		} else {
			model.addAttribute("patientAge", "");
		}
		
		if (patient.getGender() != null) {
			model.addAttribute("patientGender", patient.getGender().equals("M") ? "Male" : "Female");
		} else {
			model.addAttribute("patientGender", "");
		}
		PersonAttribute creatorPhoneNumber = sessionContext.getCurrentUser().getPerson().getAttribute("Telephone Number");
		if (creatorPhoneNumber != null) {
			model.addAttribute("creatorPhoneNumber", creatorPhoneNumber);
		} else {
			model.addAttribute("creatorPhoneNumber", "");
		}
		
		PersonAddress patientAddresses = BotswanaEmrUtils.getPersonAddress(patient.getAddresses());
		if (patientAddresses != null) {
			model.addAttribute("patientAddresses",
			    SimpleObject.fromObject(patientAddresses, ui, "address2", "cityVillage", "address5"));
		} else {
			model.addAttribute("patientAddresses", "");
		}
		
		PersonAddress creatorAddresses = BotswanaEmrUtils
		        .getPersonAddress(sessionContext.getCurrentUser().getPerson().getAddresses());
		if (creatorAddresses != null) {
			model.addAttribute("creatorAddresses", creatorAddresses);
		} else {
			model.addAttribute("creatorAddresses", "");
		}
		model.addAttribute("currentServicePoint",
		    sessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		if (visit != null) {
			BotswanaEmrUtils.getVitalsSummaryInformation(model, visit);
			model.addAttribute("visitDate", BotswanaEmrUtils.formatDateWithoutTime(visit.getDateCreated(), "yyy-MM-dd"));
		} else {
			model.addAttribute("visitDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyy-MM-dd"));
		}
	}
}
