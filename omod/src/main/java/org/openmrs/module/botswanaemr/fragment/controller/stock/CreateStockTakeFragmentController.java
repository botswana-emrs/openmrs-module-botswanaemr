/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.ItemStockSummary;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class CreateStockTakeFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null) {
			List<Stockroom> stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(),
			    false);
			
			fragmentModel.addAttribute("inventoryStockRooms", stockrooms);
		} else {
			fragmentModel.addAttribute("inventoryStockRooms", null);
		}
	}
	
	public List<SimpleObject> getItemStockData(@RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "q", required = false, defaultValue = "") String searchPhrase,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) {
		
		Stockroom stockRoom = iStockroomDataService.getById(stockRoomUuid);
		List<ItemStock> iStockList;
		if (searchPhrase.trim().length() > 0) {
			ItemSearch search = new ItemSearch(new Item());
			search.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.LIKE);
			search.getTemplate().setName("%" + searchPhrase + "%");
			iStockList = iStockroomDataService.getItems(stockRoom, search, null);
		} else {
			iStockList = iStockroomDataService.getItemsByRoom(stockRoom, null);
		}
		
		List<SimpleObject> itemStockList = new ArrayList<>();
		for (ItemStock itemStock : iStockList) {
			List<ItemStockDetail> itemStockDetails = itemStock.getDetails().stream()
			        .filter(i -> !i.isNullBatch() && i.getExpiration() != null && i.getQuantity() > 0)
			        .collect(Collectors.toList());
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				SimpleObject simpleObject = StockUtils.extractItemStockToSimpleObject(itemStock, itemStockDetail);
				itemStockList.add(simpleObject);
			}
		}
		
		return itemStockList;
	}
	
	public SimpleObject saveItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId)
	        throws JSONException, ParseException {
		
		List<ItemStockSummary> itemStockSummaries = new ArrayList<>();
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					String systemQtyString = ((JSONObject) jsonObject.get(key)).getString("systemQuantity");
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						int systemQty = Integer.parseInt(systemQtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						String batchOperationId = ((JSONObject) jsonObject.get(key)).getString("batchOperationId");
						// String batchNumber = ((JSONObject) jsonObject.get(key)).getString("batchNumber");
						String comments = ((JSONObject) jsonObject.get(key)).getString("comments");
						String variance = ((JSONObject) jsonObject.get(key)).getString("varianceQty");
						String varianceReason = ((JSONObject) jsonObject.get(key)).getString("varianceReason");
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date expiryDate = format.parse(expiryDate1);
						ItemStockSummary iss = new ItemStockSummary();
						iss.setExpiration(expiryDate);
						iss.setItem(itemStock.getItem());
						iss.setActualQuantity(qty);
						iss.setQuantity(systemQty);
						StockOperation batchOperation = BotswanaInventoryContext.getStockOperationDataService()
						        .getById(Integer.parseInt(batchOperationId));
						iss.setBatchOperation(batchOperation);
						iss.setComments(comments);
						if (!variance.isEmpty()) {
							iss.setVariance(Integer.parseInt(variance));
						}
						iss.setVarianceReason(varianceReason);
						
						itemStockSummaries.add(iss);
					}
				}
			}
		}
		
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		
		StockOperation operation = new StockOperation();
		operation.setStatus(StockOperationStatus.NEW);
		operation.setInstanceType(WellKnownOperationTypes.getAdjustment());
		operation.setSource(stockRoom);
		operation.setOperationNumber("STOCK_TAKE:" + UUID.randomUUID().toString());
		operation.setOperationDate(new Date());
		operation.setItems(createOperationsItemSet(operation, itemStockSummaries));
		
		BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		operation.setStatus(StockOperationStatus.COMPLETED);
		BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		simpleObject.put("response", operation.getId());
		return simpleObject;
		
	}
	
	public SimpleObject saveEditedItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam(value = "stockOperationId", required = false) Integer stockOperationId)
	        throws JSONException, ParseException {
		
		List<ItemStockSummary> itemStockSummaries = new ArrayList<>();
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					String systemQtyString = ((JSONObject) jsonObject.get(key)).getString("systemQuantity");
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						int systemQty = Integer.parseInt(systemQtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date expiryDate = format.parse(expiryDate1);
						if (qty != itemStock.getQuantity()) {
							ItemStockSummary iss = new ItemStockSummary();
							iss.setExpiration(expiryDate);
							iss.setItem(itemStock.getItem());
							iss.setActualQuantity(qty);
							iss.setQuantity(systemQty);
							
							itemStockSummaries.add(iss);
						}
					}
				}
			}
		}
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		
		StockOperation oldOperation = BotswanaInventoryContext.getStockOperationDataService().getById(stockOperationId);
		if (oldOperation != null) {
			oldOperation.setStatus(StockOperationStatus.CANCELLED);
			
			StockOperation operation = new StockOperation();
			operation.setStatus(StockOperationStatus.NEW);
			operation.setInstanceType(WellKnownOperationTypes.getAdjustment());
			operation.setSource(stockRoom);
			operation.setOperationNumber("STOCK_TAKE:" + UUID.randomUUID().toString());
			operation.setOperationDate(new Date());
			operation.setItems(createOperationsItemSet(operation, itemStockSummaries));
			
			BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		}
		SimpleObject simpleObject = new SimpleObject();
		
		return simpleObject;
		
	}
	
	private Set<StockOperationItem> createOperationsItemSet(StockOperation operation,
	        List<ItemStockSummary> inventoryStockTakeList) {
		Set<StockOperationItem> items = new HashSet<StockOperationItem>();
		for (ItemStockSummary inventoryItem : inventoryStockTakeList) {
			StockOperationItem item = new StockOperationItem();
			item.setOperation(operation);
			item.setItem(inventoryItem.getItem());
			item.setExpiration(inventoryItem.getExpiration());
			item.setCalculatedExpiration(false);
			item.setBatchOperation(inventoryItem.getBatchOperation());
			item.setPreAdjustmentQuantity(inventoryItem.getQuantity());
			
			int quantity = inventoryItem.getActualQuantity() - inventoryItem.getQuantity();
			item.setQuantity(quantity);
			
			if (inventoryItem.getQuantity() < 0 || inventoryItem.getActualQuantity() == 0) {
				item.setCalculatedBatch(true);
				item.setBatchOperation(null);
			} else {
				item.setCalculatedBatch(true);
			}
			item.setComment(inventoryItem.getComments());
			item.setVariance(inventoryItem.getVariance());
			item.setVarianceReason(inventoryItem.getVarianceReason());
			items.add(item);
		}
		
		return items;
	}
	
}
