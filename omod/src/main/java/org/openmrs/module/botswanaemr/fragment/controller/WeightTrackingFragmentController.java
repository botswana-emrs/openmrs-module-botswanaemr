/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.TbTreatmentService;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.utilities.DateUtils.firstDayOfMonth;
import static org.openmrs.module.botswanaemr.utilities.DateUtils.lastDayOfMonth;

@Slf4j
@SuppressWarnings("unused")
public class WeightTrackingFragmentController {
	
	public void controller(FragmentModel model, UiUtils ui, @RequestParam("patientId") Patient patient,
	        @SpringBean("obsService") ObsService obsService) {
		
		Concept weightConcept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.WEIGHT_CONCEPT_UUID);
		
		List<MonthWeight> monthWeights = new ArrayList<>();
		Optional<PatientProgram> patientProgram = Context
		        .getRegisteredComponent("botswana.emr.tbTreatmentService", TbTreatmentService.class)
		        .getTBEnrollment(patient);
		if (patientProgram.isPresent()) {
			int months = DateUtils.getMonthsBetween(patientProgram.get().getDateEnrolled());
			YearMonth yearMonth = DateUtils.getYearMonth(patientProgram.get().getDateEnrolled());
			for (int i = 0; i <= months; i++) {
				Obs mostRecentObsForMonth;
				//reduce to one db request
				List<Obs> observations = obsService.getObservations(Collections.singletonList(patient), null,
				    Collections.singletonList(weightConcept), null, null, null, null, 1, null, firstDayOfMonth(yearMonth),
				    lastDayOfMonth(yearMonth), false);
				if (!observations.isEmpty()) {
					mostRecentObsForMonth = observations.get(0);
					String yearMonthValue = yearMonth.getMonth().name() + " " + yearMonth.getYear();
					if (monthWeights.stream().noneMatch(m -> m.getMonthYear().equals(yearMonthValue))) {
						monthWeights.add(new MonthWeight(yearMonthValue, mostRecentObsForMonth.getValueNumeric()));
					}
				}
				yearMonth = yearMonth.plusMonths(i);
			}
		}
		model.addAttribute("monthWeights", monthWeights);
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class MonthWeight {
		
		private String monthYear;
		
		private Double weightAtEndOfMonth;
	}
}
