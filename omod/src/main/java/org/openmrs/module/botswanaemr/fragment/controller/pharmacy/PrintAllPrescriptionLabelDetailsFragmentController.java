/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.openmrs.Order;
import org.openmrs.Order.FulfillerStatus;
import org.openmrs.api.EncounterService;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Set;

public class PrintAllPrescriptionLabelDetailsFragmentController {
	
	public void controller(@RequestParam("encounterId") Integer encounterId,
	        @SpringBean("encounterService") EncounterService encounterService, UiUtils uiUtils,
	        FragmentModel fragmentModel) {
		Set<Order> orders = new HashSet<>(encounterService.getEncounter(encounterId).getOrders());
		// filter orders with the fulfiller status of 'COMPLETED'
		orders.removeIf(order -> order.getFulfillerStatus() == null || !order.getFulfillerStatus().equals(FulfillerStatus.COMPLETED));
		
		fragmentModel.addAttribute("orders", orders);
	}
}
