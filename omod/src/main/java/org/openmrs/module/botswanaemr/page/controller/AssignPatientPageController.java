/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Patient;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class AssignPatientPageController {
	
	public void controller(PageModel model, @RequestParam(value = "startVisit", required = false) String startVisit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "action", required = false) String action, @RequestParam("patientId") Patient patient) {
		
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("startVisit", startVisit);
		model.addAttribute("action", action);
		
	}
}
