/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Concept;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.api.ConceptService;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.OrderSearchCriteria;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.RECEIVING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_DEPARTMENT_CONCEPT_UUID;

public class DashboardReferralsFragmentController {
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext,
	        @SpringBean("orderService") OrderService orderService) {
		fragmentModel.addAttribute("hospital",
		    Context.getAuthenticatedUser().getUserProperty(BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		        Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital")));
		Concept receivingObsConcept = conceptService.getConceptByUuid(RECEIVING_DEPARTMENT_CONCEPT_UUID);
		Concept referringObsConcept = conceptService.getConceptByUuid(REFERRING_DEPARTMENT_CONCEPT_UUID);
		if (receivingObsConcept != null) {
			fragmentModel.addAttribute("receivingObsConcept", receivingObsConcept);
		} else {
			fragmentModel.addAttribute("receivingObsConcept", "");
		}
		if (referringObsConcept != null) {
			fragmentModel.addAttribute("referringObsConcept", referringObsConcept);
		} else {
			fragmentModel.addAttribute("referringObsConcept", "");
		}
		OrderType referralOrder = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		OrderSearchCriteria orderSearchCriteria = BotswanaEmrUtils.getOrderSearchCriteria(referralOrder);
		List<Order> referrals = orderService.getOrders(orderSearchCriteria).stream()
		        .filter(order -> order.getFulfillerStatus() == null
		                || !order.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED))
		        .filter(order -> order.getEncounter().getLocation().equals(sessionContext.getSessionLocation()))
		        .filter(Order::isActive).distinct().limit(BotswanaEmrUtils.LIMIT_FETCH_SIZE).collect(Collectors.toList());
		if (!referrals.isEmpty()) {
			fragmentModel.addAttribute("referrals", referrals);
		} else {
			fragmentModel.addAttribute("referrals", "");
		}
	}
}
