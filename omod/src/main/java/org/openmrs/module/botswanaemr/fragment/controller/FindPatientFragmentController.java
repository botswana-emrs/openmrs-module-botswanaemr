/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import java.util.List;
import java.util.Objects;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.representation.CustomRepresentation;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class FindPatientFragmentController extends DailyRegisteredListFragmentController {
	
	public void controller(@RequestParam(value = "action", required = false) String action,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @FragmentParam(value = "isRegistrationPage", required = false) String isRegistrationPage,
	        FragmentModel fragmentModel) {
		fragmentModel.addAttribute("action", action);
		fragmentModel.addAttribute("returnUrl", returnUrl);
		fragmentModel.addAttribute("isRegistrationPage", isRegistrationPage);
		String hospital = Context.getAuthenticatedUser().getUserProperty(
		    BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
		fragmentModel.addAttribute("hospital", hospital);
		
	}
	
	public SimpleObject findPatients(@RequestParam(value = "q", required = false) String q, UiUtils ui) {
		List<Patient> patientList = Context.getService(BotswanaEmrService.class).getBotswanaEmrPatients(q, false, null,
		    null);
		patientList.removeIf(Objects::isNull);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("results", ConversionUtil.convertToRepresentation(patientList,
		    new CustomRepresentation("uuid,identifiers:(identifierType:(name),identifier),person")));
		return simpleObject;
	}
	
}
