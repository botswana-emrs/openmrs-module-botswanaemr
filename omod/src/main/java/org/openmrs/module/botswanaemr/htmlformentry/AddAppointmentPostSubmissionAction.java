/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.EncounterProvider;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.AppointmentType;
import org.openmrs.module.appointmentscheduling.TimeSlot;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntrySession;

import java.util.Date;

public class AddAppointmentPostSubmissionAction implements CustomFormSubmissionAction {
	
	private static final String APPOINTMENT_CANCEL_REASON_CONCEPT_UUID = "21ac1d62-ca62-4c6f-baf5-1cd791777223";
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		Patient patient = formEntrySession.getPatient();
		AppointmentService service = Context.getService(AppointmentService.class);
		
		Obs appointmentIdObs = formEntrySession.getEncounter().getObs().stream()
		        .filter(obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.REMARKS_CONCEPT_UUID)).findFirst()
		        .orElse(null);
		
		Appointment appointment = appointmentIdObs == null ? new Appointment()
		        : service.getAppointment(getAppointmentId(appointmentIdObs));
		boolean newAppointment = appointmentIdObs == null;
		
		Provider provider = Context.getProviderService().getProvidersByPerson(Context.getAuthenticatedUser().getPerson())
		        .stream().findFirst().orElse(null);
		for (EncounterProvider encounterProvider : formEntrySession.getEncounter().getEncounterProviders()) {
			if (encounterProvider.getEncounterRole()
			        .equals(Context.getEncounterService().getEncounterRoleByUuid("240b26f9-dd88-4172-823d-4a8bfeb7841f"))) {
				if (!encounterProvider.getProvider().equals(provider)) {
					provider = encounterProvider.getProvider();
					break;
				}
			}
		}
		
		appointment.setReason("ART Appointment");
		
		for (Obs obs : formEntrySession.getEncounter().getObs()) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.APPOINTMENT_TYPE_CONCEPT_UUID)) {
				AppointmentType appointmentType = createAppointmentType(service, obs.getValueCoded());
				appointment.setAppointmentType(appointmentType);
				break;
			}
		}
		
		boolean appointmentCancelled = false;
		
		for (Obs obs : formEntrySession.getEncounter().getObs()) {
			switch (obs.getConcept().getUuid()) {
				case BotswanaEmrConstants.APPOINTMENT_STATUS_CONCEPT_UUID:
					appointment.setStatus(getAppointmentStatus(obs));
					break;
				case BotswanaEmrConstants.APPOINTMENT_DATE_CONCEPT_UUID:
				case BotswanaEmrConstants.APPOINTMENT_RESCHEDULED_DATE_CONCEPT_UUID:
					Date startDate = BotswanaEmrUtils.getStartTimeOfDay(obs.getValueDate());
					Date endDate = BotswanaEmrUtils.getEndTimeOfDay(obs.getValueDate());
					TimeSlot appointmentTimeSlot = BotswanaEmrUtils.getAppointmentTimeSlot(startDate, endDate, provider,
					    formEntrySession.getEncounter().getLocation(), appointment.getAppointmentType());
					service.saveAppointmentBlock(appointmentTimeSlot.getAppointmentBlock());
					service.saveTimeSlot(appointmentTimeSlot);
					appointment.setTimeSlot(appointmentTimeSlot);
					break;
				case BotswanaEmrConstants.APPOINTMENT_CANCELLED_CONCEPT_UUID:
					if (obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.YES_CONCEPT_UUID)) {
						appointmentCancelled = true;
						appointment.setStatus(Appointment.AppointmentStatus.CANCELLED);
					}
					break;
				case BotswanaEmrConstants.APPOINTMENT_SERVICE_CONCEPT_UUID:
					appointment.setReason(obs.getValueCoded().getUuid());
					break;
				case APPOINTMENT_CANCEL_REASON_CONCEPT_UUID:// BotswanaEmrConstants.APPOINTMENT_CANCEL_REASON_CONCEPT_UUID:
					appointment.setCancelReason(obs.getValueText());
					break;
			}
		}
		if (appointmentCancelled) {
			appointment.setStatus(Appointment.AppointmentStatus.CANCELLED);
			appointment.setCancelReason(null);
		}
		if (newAppointment) {
			appointment.setPatient(patient);
			appointment.setVisit(formEntrySession.getEncounter().getVisit());
			appointment.setDateCreated(new Date());
			appointment.setCreator(Context.getAuthenticatedUser());
		}
		
		appointment = service.saveAppointment(appointment);
		if (newAppointment) {
			Concept concept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.REMARKS_CONCEPT_UUID);
			Obs newAppointmentiDObs = BotswanaEmrUtils.creatObs(formEntrySession.getEncounter(), concept);
			newAppointmentiDObs.setValueText(String.valueOf(appointment.getAppointmentId()));
			Context.getObsService().saveObs(newAppointmentiDObs, "Set appointment id");
		}
		
	}
	
	private AppointmentType createAppointmentType(AppointmentService service, Concept appointmentTypeConcept) {
		AppointmentType appointmentType = service.getAppointmentTypeByUuid(appointmentTypeConcept.getUuid());
		if (appointmentType == null) {
			appointmentType = new AppointmentType();
			appointmentType.setUuid(appointmentTypeConcept.getUuid());
			appointmentType.setName(appointmentTypeConcept.getName().getName());
			appointmentType.setDescription(appointmentTypeConcept.getName().getName());
			appointmentType.setDuration(30);
			
			return service.saveAppointmentType(appointmentType);
		}
		
		return appointmentType;
	}
	
	private Integer getAppointmentId(Obs appointmentIdObs) {
		if (appointmentIdObs != null && StringUtils.isNotEmpty(appointmentIdObs.getValueText())) {
			return Integer.valueOf(appointmentIdObs.getValueText());
		}
		return null;
	}
	
	private AppointmentType createArtAppointmentType(AppointmentService service) {
		AppointmentType appointmentType = new AppointmentType();
		appointmentType.setUuid(BotswanaEmrConstants.ART_APPOINTMENT_TYPE_UUID);
		appointmentType.setName("ART Appointment");
		appointmentType.setDescription("ART Appointment");
		appointmentType.setDuration(30);
		
		return service.saveAppointmentType(appointmentType);
	}
	
	private Appointment.AppointmentStatus getAppointmentStatus(Obs obs) {
		switch (obs.getValueCoded().getUuid()) {
			case "31eecc93-3118-4f09-b531-25363066d7ff":
			case "8d2e52e1-3a2e-4de5-b066-4f13dcc926bc":
				return Appointment.AppointmentStatus.SCHEDULED;
			case "11b82311-e579-451d-9d1f-30478261f798":
				return Appointment.AppointmentStatus.MISSED;
			case "3d0bc93d-963f-4d3c-960a-5b2dca9ed06a":
				return Appointment.AppointmentStatus.RESCHEDULED;
			case "868ab1d6-71ef-41e5-bdb8-8fde746141d4":
				return Appointment.AppointmentStatus.RESCHEDULED;
			default:
				return Appointment.AppointmentStatus.SCHEDULED;
		}
	}
}
