/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import org.glassfish.jaxb.core.v2.WellKnownNamespace;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockAdjustment;
import org.openmrs.module.botswanaemr.model.SimplifiedStockTake;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.*;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

public class CreateStockAdjustmentPageController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void get(@RequestParam(value = "stockOperationId", required = false) Integer stockOperationId, PageModel model,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) {
		if (uiSessionContext.getCurrentUser() != null && stockOperationId != null) {
			Set<SimplifiedStockAdjustment> simplifiedStockAdjustmentList = new HashSet<>();
			StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
			        .getById(stockOperationId);
			
			for (StockOperationItem operationItem : stockOperation.getItems()) {
				ItemStock itemStock = stockroomDataService.getItem(stockOperation.getSource(), operationItem.getItem());
				
				SimplifiedStockAdjustment stockAdjustment = new SimplifiedStockAdjustment();
				stockAdjustment.setId(stockOperationId);
				stockAdjustment.setItemCode(
				    itemStock.getItem().getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
				stockAdjustment.setItemName(itemStock.getItem().getName());
				stockAdjustment
				        .setExpiryDate(BotswanaEmrUtils.formatDateWithoutTime(operationItem.getExpiration(), "yyyy-MM-dd"));
				stockAdjustment.setTodayDate(BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
				stockAdjustment.setItemStockUuid(itemStock.getUuid());
				if (operationItem.getOperation().getInstanceType() == WellKnownOperationTypes.getDisposed()) {
					stockAdjustment.setItemStockQuantity(operationItem.getQuantity() * -1);
					
				} else {
					stockAdjustment.setItemStockQuantity(operationItem.getQuantity());
					
				}
				int qnty = itemStock.getQuantity() - operationItem.getQuantity();
				stockAdjustment.setEnteredQuantityInStore(itemStock.getQuantity());
				stockAdjustment.setBatchNumber(operationItem.getOperation().getOperationNumber());
				if (stockOperation.getSource() != null) {
					stockAdjustment.setStockRoomName(String.valueOf(stockOperation.getSource().getName()));
				} else {
					stockAdjustment.setStockRoomName(String.valueOf(stockOperation.getDestination().getName()));
					
				}
				
				simplifiedStockAdjustmentList.add(stockAdjustment);
			}
			
			model.addAttribute("simplifiedStockAdjustmentList", simplifiedStockAdjustmentList);
			model.addAttribute("selectedStockRoomId", stockOperation.getSource().getId());
			model.addAttribute("stockOperationStatus", stockOperation.getStatus().toString());
			
		} else {
			model.addAttribute("simplifiedStockAdjustmentList", "");
		}
	}
}
