/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import static org.openmrs.module.botswanaemr.fragment.controller.admin.FacilitiesFragmentController.getAttributeValueReference;

public class AppointmentBookingPageController {
	
	public void controller(PageModel pageModel, @RequestParam(value = "facilityId", required = false) int facilityId,
	        @SpringBean("locationService") LocationService locationService) {
		Location location = locationService.getLocation(facilityId);
		
		pageModel.addAttribute("facilityName", location.getName());
		pageModel.addAttribute("facilityMfl",
		    getAttributeValueReference(location, BotswanaEmrConstants.MASTER_FACILITY_CODE_UUID));
	}
}
