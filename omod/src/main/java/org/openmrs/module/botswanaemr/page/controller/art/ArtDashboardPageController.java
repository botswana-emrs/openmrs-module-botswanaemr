/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.art;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

public class ArtDashboardPageController extends QueuePatientFragmentController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date yesterday = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);
		
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		Location artLocation = Context.getLocationService().getLocationByUuid(BotswanaEmrConstants.ART_SERVICES_PORTAL_UUID);
		
		// TODO : Change to enrolled patients
		int allScreeningsCount = 0;
		int todayScreeningsCount = 0;
		int yesterdayScreeningsCount = 0;
		int dailyAverageScreenings = 0;
		
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		
		List<PatientQueue> patientQueueList = new ArrayList<>();
		patientQueueList = patientQueueingService.getPatientQueueList(null, null, null, artLocation, location, null, null);
		
		allScreeningsCount = patientQueueList.size();
		model.addAttribute("allScreeningsCount", allScreeningsCount);
		
		List<Encounter> todayScreeningsList = getAllScreenings(today, today, null, location);
		if (todayScreeningsList != null && todayScreeningsList.size() > 0) {
			todayScreeningsCount = todayScreeningsList.size();
		}
		
		List<Encounter> yesterdayScreeningsList = getAllScreenings(yesterday, yesterday, null, location);
		if (yesterdayScreeningsList != null && yesterdayScreeningsList.size() > 0) {
			yesterdayScreeningsCount = yesterdayScreeningsList.size();
		}
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		model.addAttribute("todayScreeningActivitiesList",
		    patientObjects(getAllScreenings(null, null, limitFetch, location)));
		model.addAttribute("todayScreeningsCount", todayScreeningsCount);
		model.addAttribute("yesterdayScreeningsCount", yesterdayScreeningsCount);
		model.addAttribute("dailyAverageScreenings", getDailyAverageScreenings(location));
		
		patientQueueList = patientQueueingService.getPatientQueueList(null,
		    DateUtils.getDateStartOfDay(new DateTime().withDayOfWeek(DateTimeConstants.MONDAY).toDate()),
		    DateUtils.getDateEndOfDay(new DateTime().withDayOfWeek(DateTimeConstants.SUNDAY).toDate()), artLocation,
		    location, null, null);
		int weekCount = patientQueueList.size();
		if (weekCount > 0) {
			dailyAverageScreenings = weekCount / 7;
		}
		model.addAttribute("dailyAverageEnrollments", dailyAverageScreenings);
		
	}
	
	private List<Encounter> getAllScreenings(Date start, Date end, Integer limit, Location location) {
		List<Encounter> screenings = Context.getService(BotswanaEmrService.class).getArtEncountersList(start, end, limit,
		    location);
		
		if (screenings != null && !screenings.isEmpty()) {
			screenings = screenings.stream().filter(r -> r.getPatient().getAttribute("Person-location-attribute") != null)
			        .filter(
			            r -> r.getPatient().getAttribute("Person-location-attribute").getValue().equals(location.getUuid()))
			        .collect(Collectors.toList());
		}
		
		return screenings;
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(formatPersonCreator(patient));
				generalPatientObject.setDuration(formatDurationSinceRegistration(patient));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
	
	private Integer getDailyAverageScreenings(Location location) {
		
		int totalScreeningsEver = 0;
		Encounter firstPatientEncounter = null;
		Encounter lastEncounter = null;
		int daysBetweenFirstAndLastEncounterDates = 0;
		
		if (getAllScreenings(null, null, null, location) != null
		        && getAllScreenings(null, null, null, location).size() > 0) {
			totalScreeningsEver = getAllScreenings(null, null, null, location).size();
			firstPatientEncounter = getAllScreenings(null, null, null, location).get(0);
			lastEncounter = getAllScreenings(null, null, null, location)
			        .get(getAllScreenings(null, null, null, location).size() - 1);
		}
		
		if (firstPatientEncounter != null && lastEncounter != null) {
			daysBetweenFirstAndLastEncounterDates = BotswanaEmrUtils.unitsSince(lastEncounter.getEncounterDatetime(),
			    firstPatientEncounter.getEncounterDatetime(), "days");
		}
		
		if (daysBetweenFirstAndLastEncounterDates == 0) {
			daysBetweenFirstAndLastEncounterDates = 1;
		}
		
		return totalScreeningsEver / daysBetweenFirstAndLastEncounterDates;
		
	}
}
