/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class NegativeAdjustmentsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		
		stockOperationTemplate.setStatus(StockOperationStatus.REQUESTED);
		
		// Get my main stockrooms
		List<Stockroom> myMainStockrooms = new ArrayList<>(
		        StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation()));
		// Get all main stockrooms in all facilities
		List<Stockroom> allMainStockrooms = BotswanaInventoryContext.getStockRoomDataService().getAll(false);
		// Keep stockrooms from other facilities
		for (Stockroom stockroom : myMainStockrooms) {
			allMainStockrooms.remove(stockroom);
		}
		
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		List<StockOperation> outgoingStockOperations = new ArrayList<>();
		
		for (Stockroom stockroom : allMainStockrooms) {
			
			stockOperationTemplate.setSource(stockroom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				// Filter for only unapproved stock operations
				if (stockOperation.getStockOperationParent() == null
				        || stockOperation.getStockOperationParent().getStatus() != StockOperationStatus.COMPLETED) {
					outgoingStockOperations.add(stockOperation);
				}
			}
		}
		
		List<StockOperation> incomingStockOperations = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			
			stockOperationTemplate.setSource(stockroom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				// Filter for only pending child operations of approved parent stock operations
				//if (stockOperation.getStockOperationParent() != null
				//		&& stockOperation.getStockOperationParent().getStatus() == StockOperationStatus.COMPLETED) {
				incomingStockOperations.add(stockOperation);
				//}
			}
		}
		
		List<StockOperation> outgoingStockOperationsToDispatch = getOutgoingStockOperationsToDispatch(myMainStockrooms);
		
		List<StockOperation> incomingStockOperationsToReceive = getIncomingStockOperationsToReceive(myMainStockrooms); //TODO: Move this to the positive adjustments fragment cont
		
		List<StockOperation> stockOperationsReports = new ArrayList<>();
		
		// for (Stockroom stockroom : myMainStockrooms) {
		// 	//stockOperationTemplate.setDestination(BotswanaInventoryContext.getStockRoomDataService().getById(5));
		// 	//stockOperationTemplate.setDestination(null);
		// 	//stockOperationTemplate.setSource(stockroom);
		// 	stockOperationTemplate.setStockroom(stockroom);
		
		// 	stockOperationTemplate.setStatus(StockOperationStatus.COMPLETED);
		// 	stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getDistribution());
		
		// 	// stockOperationsSearch.setTemplate(stockOperationTemplate);
		// 	// List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
		// 	//         : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
		// 	//             null);
		// 	// // Filter for external operations only
		// 	// stockOperations = stockOperations.stream()
		// 	//         .filter(o -> !o.getSource().getLocation().equals(o.getDestination().getLocation()))
		// 	//         .collect(Collectors.toList());
		// 	// stockOperationsReports.addAll(stockOperations);
		
		// 	// for (StockOperation stockOperation : stockOperations) {
		// 	// 	//	stockOperationsReports.add(stockOperation);
		
		// 	// }
		// }
		
		fragmentModel.addAttribute("outgoingOperations", outgoingStockOperations);
		fragmentModel.addAttribute("incomingStockOperations", incomingStockOperations);
		fragmentModel.addAttribute("outgoingStockOperationsToDispatch", outgoingStockOperationsToDispatch);
		fragmentModel.addAttribute("incomingStockOperationsToReceive", incomingStockOperationsToReceive);
		fragmentModel.addAttribute("stockOperationsReports", stockOperationsReports);
		
	}
	
	private List<StockOperation> getOutgoingStockOperationsToDispatch(List<Stockroom> myMainStockrooms) {
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		List<StockOperation> outgoingStockOperationsToDispatch = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			stockOperationTemplate.setSource(stockroom);
			stockOperationTemplate.setStatus(StockOperationStatus.PROCESSING);
			StockOperationSearch stockOperationsSearch = new StockOperationSearch();
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				outgoingStockOperationsToDispatch.add(stockOperation);
			}
		}
		return outgoingStockOperationsToDispatch;
	}
	
	private List<StockOperation> getIncomingStockOperationsToReceive(List<Stockroom> myMainStockrooms) {
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getTransfer());
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		List<StockOperation> incomingStockOperationsToReceive = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			
			stockOperationTemplate.setDestination(stockroom);
			stockOperationTemplate.setSource(null);
			stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				incomingStockOperationsToReceive.add(stockOperation);
			}
		}
		return incomingStockOperationsToReceive;
	}
}
