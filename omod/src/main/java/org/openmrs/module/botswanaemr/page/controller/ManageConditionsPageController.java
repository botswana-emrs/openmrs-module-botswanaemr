/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Condition;
import org.openmrs.Patient;
import org.openmrs.api.ConditionService;
import org.openmrs.api.context.Context;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

/**
 * Controller for a fragment that manages conditions for a patient
 */
public class ManageConditionsPageController {
	
	public void controller(PageModel model, @RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui,
	        @SpringBean("conditionService") ConditionService conditionService) {
		if (StringUtils.isBlank(returnUrl)) {
			returnUrl = ui.pageLink("botswanaemr", "patientProfile",
			    Collections.singletonMap("patientId", (Object) patient.getId()));
		}
		
		List<Condition> activeConditions = conditionService.getActiveConditions(patient);
		model.addAttribute("activeConditions", activeConditions);
		
		model.addAttribute("patient", patient);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("hasModifyConditionsPrivilege",
		    Context.getAuthenticatedUser().hasPrivilege(CoreAppsConstants.MANAGE_CONDITIONS_PRIVILEGE));
	}
}
