/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.lab;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.openmrs.Concept;
import org.openmrs.Person;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.lab.Lab;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;
import org.openmrs.module.botswanaemr.model.hts.LabQualityTestReason;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedLabQualityControlTest;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedLabQualityTestReason;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

@Slf4j
public class LaboratoryQualityControlFormFragmentController {
	
	IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
	
	IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
	
	IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
	
	HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
	
	public void controller(FragmentModel fragmentModel,
	        @RequestParam(value = "labQualityControlId", required = false) Integer labQualityControlId) {
		
		Set<SimplifiedLabQualityControlTest> simplifiedPositiveLabQualityControlTestList = new HashSet<>();
		Set<SimplifiedLabQualityControlTest> simplifiedNegativeLabQualityControlTestList = new HashSet<>();
		JsonArray result = new JsonArray();
		JsonArray labQualityControlTests = new JsonArray();
		
		if (labQualityControlId != null) {
			LabQualityControl labQualityControl = htsLabControlService.getLabQualityControl(labQualityControlId);
			List<LabQualityControlTest> qualityControlTestSet = labQualityControl.getHtsTest().stream()
			        .sorted(Comparator.comparing(LabQualityControlTest::getId)).collect(Collectors.toList());
			
			labQualityControlTests = (JsonArray) new Gson().toJsonTree(
			    getSimplifiedLabQualityControlTest(qualityControlTestSet),
			    new TypeToken<List<SimplifiedLabQualityControlTest>>() {}.getType());
			
			Set<SimplifiedLabQualityTestReason> testReasons = getSimplifiedLabQualityTestReason(labQualityControl);
			result = (JsonArray) new Gson().toJsonTree(testReasons,
			    new TypeToken<List<SimplifiedLabQualityTestReason>>() {}.getType());
		}
		fragmentModel.addAttribute("labQualityControlId", labQualityControlId);
		fragmentModel.addAttribute("testReasons", result);
		fragmentModel.addAttribute("labQualityControlTests", labQualityControlTests);
		
	}
	
	private List<SimplifiedLabQualityControlTest> getSimplifiedLabQualityControlTest(
	        List<LabQualityControlTest> labQualityControlTests) {
		List<SimplifiedLabQualityControlTest> simplifiedLabQualityControlTests = new ArrayList<>();
		for (LabQualityControlTest labQualityControlTest : labQualityControlTests) {
			SimplifiedLabQualityControlTest simplifiedLabQualityControlTest = new SimplifiedLabQualityControlTest();
			simplifiedLabQualityControlTest.setId(labQualityControlTest.getId());
			if (labQualityControlTest.getQualityControl() != null)
				simplifiedLabQualityControlTest.setQualityControlOption(labQualityControlTest.getQualityControl().getUuid());
			simplifiedLabQualityControlTest.setNameOfTest(labQualityControlTest.getTypeOfTest());
			if (labQualityControlTest.getKitName() != null)
				simplifiedLabQualityControlTest.setKitName(labQualityControlTest.getKitName().getUuid());
			simplifiedLabQualityControlTest.setLotNumber(labQualityControlTest.getLotNumber());
			if (labQualityControlTest.getExpiryDate() != null)
				simplifiedLabQualityControlTest.setTestExpiryDate(
				    BotswanaEmrUtils.formatDateWithoutTime(labQualityControlTest.getExpiryDate(), "yyyy/MM/dd"));
			if (labQualityControlTest.getTestResult() != null)
				simplifiedLabQualityControlTest.setTestResults(labQualityControlTest.getTestResult().getUuid());
			if (labQualityControlTest.getAcceptable() != null)
				simplifiedLabQualityControlTest.setAcceptable(labQualityControlTest.getAcceptable().getUuid());
			if (labQualityControlTest.getAuthorizedBy() != null) {
				simplifiedLabQualityControlTest.setAuthorizedBy(labQualityControlTest.getAuthorizedBy().getUuid());
				simplifiedLabQualityControlTest
				        .setAuthorizedByName(labQualityControlTest.getAuthorizedBy().getPersonName().getFullName());
			}
			if (labQualityControlTest.getStockroom() != null) {
				simplifiedLabQualityControlTest.setStockRoomId(labQualityControlTest.getStockroom().getUuid());
				simplifiedLabQualityControlTest.setStockRoom(labQualityControlTest.getStockroom().getName());
			}
			simplifiedLabQualityControlTest.setIncident(labQualityControlTest.getIncident());
			simplifiedLabQualityControlTest.setCorrectiveActionTaken(labQualityControlTest.getCorrectiveActionTaken());
			
			simplifiedLabQualityControlTests.add(simplifiedLabQualityControlTest);
		}
		return simplifiedLabQualityControlTests;
	}
	
	private void getSimplifiedLabQualityControlTestData(
	        Set<SimplifiedLabQualityControlTest> simplifiedPositiveLabQualityControlTestList,
	        LabQualityControlTest labQualityControlTest, SimplifiedLabQualityControlTest simplifiedLabQualityControlTest) {
		simplifiedLabQualityControlTest.setQualityControlOption(labQualityControlTest.getQualityControl().getUuid());
		simplifiedLabQualityControlTest.setNameOfTest(labQualityControlTest.getTypeOfTest());
		simplifiedLabQualityControlTest.setKitName(labQualityControlTest.getKitName().getUuid());
		simplifiedLabQualityControlTest.setLotNumber(labQualityControlTest.getLotNumber());
		simplifiedLabQualityControlTest.setTestExpiryDate(
		    BotswanaEmrUtils.formatDateWithoutTime(labQualityControlTest.getExpiryDate(), "dd-MMM-yyyy"));
		simplifiedLabQualityControlTest.setTestResults(labQualityControlTest.getTestResult().getUuid());
		
		simplifiedPositiveLabQualityControlTestList.add(simplifiedLabQualityControlTest);
	}
	
	private Set<SimplifiedLabQualityTestReason> getSimplifiedLabQualityTestReason(LabQualityControl labQualityControl) {
		Set<SimplifiedLabQualityTestReason> testReasonSet = new HashSet<>();
		for (LabQualityTestReason testReason : labQualityControl.getReasons()) {
			SimplifiedLabQualityTestReason simplifiedLabQualityTestReason = new SimplifiedLabQualityTestReason();
			simplifiedLabQualityTestReason.setConcept(testReason.getConcept().getUuid());
			simplifiedLabQualityTestReason.setId(testReason.getId());
			testReasonSet.add(simplifiedLabQualityTestReason);
		}
		return testReasonSet;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void saveLabQualityControl(
	        @RequestParam(value = "labQualityControlId", required = false) Integer labQualityControlId,
	        @RequestParam(value = "dateTested", required = false) String dateTested,
	        @RequestParam(value = "operatorName", required = false) Person operator,
	        @RequestParam(value = "labQualityDataTests", required = false) String labQualityDataTests,
	        @RequestParam(value = "numOfTestStrips", required = false) String numOfTestStrips,
	        @RequestParam(value = "published", defaultValue = "false") boolean published,
	        @RequestParam(value = "authorizedBy", required = false) Person authorizedBy,
	        @RequestParam(value = "incident", required = false) String incident,
	        @RequestParam(value = "correctiveActionTaken", required = false) String correctiveActionTaken,
	        @RequestParam(value = "acceptable", required = false) Concept acceptable,
	        @RequestParam(value = "reasonForTest", required = false) Concept reasonForTest,
	        @RequestParam(value = "reasonsForTest", required = false) String reasonsForTest,
	        @RequestParam(value = "otherReasonForTest", required = false) String otherReasonForTest,
	        @RequestParam(value = "testingPoint", required = false) String testingPoint,
			UiSessionContext uiSessionContext)
	        throws ParseException, JSONException {
		
		HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
		
		LabQualityControl labQualityControl = new LabQualityControl();
		if (labQualityControlId != null) {
			labQualityControl = htsLabControlService.getLabQualityControl(labQualityControlId);
		} else {
			labQualityControl.setUuid(UUID.randomUUID().toString());
			labQualityControl.setLocation(uiSessionContext.getSessionLocation());
		}
		if (labQualityControl.getPublished()) {
			return;
		}
		Set<LabQualityControlTest> labQualityControlTestSet = new HashSet<>();
		
		if (operator != null)
			labQualityControl.setOperator(operator);
		if (reasonForTest != null)
			labQualityControl.setReasonForTest(reasonForTest);
		if (StringUtils.isNotEmpty(dateTested))
			labQualityControl.setDateTested(BotswanaEmrUtils.formatDateFromString(dateTested, "dd-MMM-yyyy"));
		if (acceptable != null)
			labQualityControl.setAcceptable(acceptable);
		;
		if (authorizedBy != null)
			labQualityControl.setAuthorizedBy(authorizedBy);
		if (StringUtils.isNotEmpty(incident))
			labQualityControl.setIncident(incident);
		if (StringUtils.isNotEmpty(numOfTestStrips))
			labQualityControl.setNoOfStrips(Integer.valueOf(numOfTestStrips));
		if (StringUtils.isNotEmpty(correctiveActionTaken))
			labQualityControl.setCorrectiveActionTaken(correctiveActionTaken);
		labQualityControl.setCreator(Context.getAuthenticatedUser());
		if (StringUtils.isNotEmpty(otherReasonForTest))
			labQualityControl.setOtherReasonForTest(otherReasonForTest);
		labQualityControl.setDateCreated(new Date());
		labQualityControl.setPublished(published);
		if (testingPoint != null)
			labQualityControl.setTestingPoint(testingPoint);
		
		if (StringUtils.isNotEmpty(reasonsForTest)) {
			JSONTokener tokener = new JSONTokener(reasonsForTest);
			JSONArray jsonArray = new JSONArray(tokener);
			for (int i = 0; i < jsonArray.length(); i++) {
				String uuid = jsonArray.getString(i);
				Concept concept = Context.getConceptService().getConceptByUuid(uuid);
				if (concept != null) {
					LabQualityTestReason testReason = new LabQualityTestReason();
					testReason.setConcept(concept);
					testReason.setLabQualityControl(labQualityControl);
					
					labQualityControl.getReasons().add(testReason);
				}
			}
		}
		
		// Parse the JSON string using JSONTokener to generate array
		JSONTokener tokener = new JSONTokener(labQualityDataTests);
		JSONArray jsonArray = new JSONArray(tokener);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			
			LabQualityControlTest labQualityControlTest = getLabControlTestFromJson(jsonObject);
			labQualityControlTest.setLabQualityControl(labQualityControl);
			
			labQualityControlTestSet.add(labQualityControlTest);
		}
		labQualityControl.setHtsTest(labQualityControlTestSet);
		
		try {
			htsLabControlService.saveLabQualityControl(labQualityControl);
			// System.out.println("++++++ saved  labQuality");
			log.info("\nSaved new Lab Quality Control\n" + labQualityControl);
			if (published) {
				for (LabQualityControlTest qualityControlTest : labQualityControl.getHtsTest()) {
					if (qualityControlTest.getStockroom() != null) {
						StockOperation stockOperation = new StockOperation();
						stockOperation.setOperationDate(new Date());
						stockOperation.setSource(qualityControlTest.getStockroom());
						
						Item item = iItemDataService.getItemsByConcept(qualityControlTest.getKitName()).get(0);
						StockOperation batchOperation = iStockOperationDataService
						        .getOperationByNumber(qualityControlTest.getLotNumber());
						Date expirationDate = qualityControlTest.getExpiryDate();
						stockOperation.addItem(item, 1, expirationDate, batchOperation);
						
						stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
						stockOperation.setDescription("Kits used for lab quality control testing");
						stockOperation.setOperationNumber(UUID.randomUUID().toString());
						stockOperation.setStatus(StockOperationStatus.NEW);
						iStockOperationService.submitOperation(stockOperation);
						stockOperation.setStatus(StockOperationStatus.COMPLETED);
						
						iStockOperationService.submitOperation(stockOperation);
					}
				}
			}
		}
		catch (Exception e) {
			log.warn("\nAn error occurred while Lab Quality Control\n", e);
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
			        "An error occurred while adding new Lab Quality Control: " + e.getMessage());
		}
	}
	
	private LabQualityControlTest getLabControlTestFromJson(JSONObject jsonObject) throws JSONException, ParseException {
		LabQualityControlTest labQualityControlTest = new LabQualityControlTest();
		
		if (jsonObject.has("id")) {
			labQualityControlTest = htsLabControlService
			        .getLabQualityControlTest(Integer.parseInt(jsonObject.getString("id")));
		} else {
			labQualityControlTest = new LabQualityControlTest();
			labQualityControlTest.setUuid(UUID.randomUUID().toString());
		}
		String qualityControl = jsonObject.has("qualityControl") ? jsonObject.getString("qualityControl") : "";
		String nameOfTest = jsonObject.has("nameOfTest") ? jsonObject.getString("nameOfTest") : "";
		String kitName = jsonObject.has("kitName") ? jsonObject.getString("kitName") : "";
		String lotNumber = jsonObject.has("lotNumber") ? jsonObject.getString("lotNumber") : "";
		String testExpiryDate = jsonObject.has("testExpiryDate") ? jsonObject.getString("testExpiryDate") : "";
		String testResults = jsonObject.has("testResults") ? jsonObject.getString("testResults") : "";
		String interpretation = jsonObject.has("interpretation") ? jsonObject.getString("interpretation") : "";
		String acceptableTest = jsonObject.has("acceptable") ? jsonObject.getString("acceptable") : "";
		String testAuthorizedBy = jsonObject.has("authorizedBy") ? jsonObject.getString("authorizedBy") : "";
		String testIncident = jsonObject.has("incident") ? jsonObject.getString("incident") : "";
		String testCorrectiveAction = jsonObject.has("correctiveAction") ? jsonObject.getString("correctiveAction") : "";
		
		if (StringUtils.isNotEmpty(qualityControl)) {
			labQualityControlTest.setQualityControl(BotswanaEmrUtils.getConcept(qualityControl));
		}
		labQualityControlTest.setTypeOfTest(nameOfTest);
		if (StringUtils.isNotEmpty(testExpiryDate))
			labQualityControlTest.setExpiryDate(BotswanaEmrUtils.formatDateFromString(testExpiryDate, "yyyy/MM/dd"));
		if (StringUtils.isNotEmpty(kitName))
			labQualityControlTest.setKitName(BotswanaEmrUtils.getConcept(kitName));
		
		if (StringUtils.isNotEmpty(interpretation))
			labQualityControlTest.setInterpretation(BotswanaEmrUtils.getConcept(interpretation));
		labQualityControlTest.setLotNumber(lotNumber);
		if (StringUtils.isNotEmpty(testResults))
			labQualityControlTest.setTestResult(BotswanaEmrUtils.getConcept(testResults));
		if (StringUtils.isNotEmpty(acceptableTest))
			labQualityControlTest.setAcceptable(BotswanaEmrUtils.getConcept(acceptableTest));
		labQualityControlTest.setAuthorizedBy(Context.getPersonService().getPersonByUuid(testAuthorizedBy));
		labQualityControlTest.setIncident(testIncident);
		labQualityControlTest.setCorrectiveActionTaken(testCorrectiveAction);
		if (jsonObject.has("stockRoomId") && StringUtils.isNotEmpty(jsonObject.getString("stockRoomId"))) {
			labQualityControlTest.setStockroom(StockUtils.getStockroom(jsonObject.getString("stockRoomId")));
		}
		
		return labQualityControlTest;
	}
	
	public void retireLabQualityControl(
	        @RequestParam(value = "labQualityControlId", required = false) Integer labQualityControlId,
	        UiSessionContext sessionContext) {
		HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
		
		if (labQualityControlId != null) {
			LabQualityControl labQualityControl = htsLabControlService.getLabQualityControl(labQualityControlId);
			labQualityControl.setVoided(true);
			labQualityControl.setDateVoided(new Date());
			labQualityControl.setVoidedBy(sessionContext.getCurrentUser());
			
			htsLabControlService.saveLabQualityControl(labQualityControl);
		}
	}
}
