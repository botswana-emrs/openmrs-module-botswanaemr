/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Location;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.locationbasedaccess.utils.LocationUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class SelectServicePointPageController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext) throws Exception {
		User authenticatedUser = sessionContext.getCurrentUser();
		
		List<Location> locations;
		try {
			Context.addProxyPrivilege(VIEW_LOCATIONS);
			Context.addProxyPrivilege(GET_LOCATIONS);
			locations = getAllowedServiceDeliveryPoints(authenticatedUser);
			sessionContext.getSession().setAttribute("allowedServiceDeliveryPoints", locations);
		}
		finally {
			Context.removeProxyPrivilege(VIEW_LOCATIONS);
			Context.removeProxyPrivilege(GET_LOCATIONS);
		}
		
		pageModel.addAttribute("locations", locations);
		pageModel.addAttribute("assignedlocations", getUserAssignedFacilities(authenticatedUser));
		pageModel.addAttribute("authenticatedUserName", authenticatedUser.getPersonName());
	}
	
	public static List<Location> getAllowedServiceDeliveryPoints(User authenticatedUser) {
		List<Location> locations = new ArrayList<>();
		if (authenticatedUser != null) {
			LocationService locationService = Context.getLocationService();
			Location pharmacy = locationService.getLocationByUuid(PHARMACY_PORTAL_UUID);
			Location stockManagementPortal = locationService.getLocationByUuid(STOCK_MANAGEMENT_PORTAL_UUID);
			Location labServicesPortal = locationService.getLocationByUuid(LAB_SERVICES_PORTAL_UUID);
			Location vmmcPortal = locationService.getLocationByUuid(VMMC_PORTAL_UUID);
			Location sexualReproductiveHealthPortal = locationService
			        .getLocationByUuid(SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
			Location doctorsPortal = locationService.getLocationByUuid(DOCTORS_PORTAL_UUID);
			Location registrationDesk = locationService.getLocationByUuid(PATIENT_REGISTRATION_PORTAL_UUID);
			Location artServicesPortal = locationService.getLocationByUuid(ART_SERVICES_PORTAL_UUID);
			Location auxilliaryNursePortal = locationService.getLocationByUuid(NURSING_PORTAL_UUID);
			Location tbServicesPortal = locationService.getLocationByUuid(TB_SERVICES_PORTAL_UUID);
			Location hivTestingServicesPortal = locationService.getLocationByUuid(HIV_TESTING_SERVICES_PORTAL_UUID);
			
			if (authenticatedUser.hasRole(ROLE_NURSE) || authenticatedUser.hasRole(ROLE_REGISTRATION_CLERK)
			        || authenticatedUser.isSuperUser()) {
				locations.add(auxilliaryNursePortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_DOCTOR) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(doctorsPortal);
				
			}
			
			if (authenticatedUser.hasRole(ROLE_PHARMACIST) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(pharmacy);
			}
			
			if (authenticatedUser.hasRole(ROLE_TB_MANAGER) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(tbServicesPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_ART_MANAGER) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(artServicesPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_VMMC_MANAGER) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(vmmcPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_HTS_MANAGER) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(hivTestingServicesPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_SRH_MANAGER) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(sexualReproductiveHealthPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_REGISTRATION_CLERK) || authenticatedUser.isSuperUser()
			        || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN) || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN)) {
				locations.add(registrationDesk);
			}
			
			if (authenticatedUser.hasRole(ROLE_STOCK_MANAGER) || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN)
			        || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN) || authenticatedUser.isSuperUser()) {
				locations.add(stockManagementPortal);
			}
			
			if (authenticatedUser.hasRole(ROLE_LAB_SERVICES) || authenticatedUser.hasRole(ROLE_SYSTEM_ADMIN)
			        || authenticatedUser.hasRole(ROLE_HOSPITAL_ADMIN) || authenticatedUser.isSuperUser()) {
				locations.add(labServicesPortal);
			}
		}
		locations.removeAll(Collections.singleton(null));
		return locations;
	}
	
	public String post(UiUtils ui, UiSessionContext sessionContext,
	        @RequestParam(value = "sessionLocationId", required = false) Integer sessionLocationId,
	        @RequestParam(value = "loggedInLocationId", required = false) Integer loggedInLocationId) {
		
		try {
			Context.addProxyPrivilege(VIEW_LOCATIONS);
			Context.addProxyPrivilege(GET_LOCATIONS);
			
			LocationService locationService = Context.getService(LocationService.class);
			Location servicePointToVisit = locationService.getLocation(sessionLocationId);
			
			Location loggedInLocation = locationService.getLocation(loggedInLocationId);
			sessionContext.setSessionLocation(loggedInLocation);
			
			if (loggedInLocation != null) {
				Context.getUserService().saveUserProperty(USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
				    loggedInLocation.getName());
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(PATIENT_REGISTRATION_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, PATIENT_REGISTRATION_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr",
				    "registrationAdminDashboard?appId=botswanaemr.registrationAdminDashboard");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(NURSING_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, NURSING_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr",
				    "consultation/auxilliaryNurseDashboard?appId=botswanaemr.auxilliaryNurseDashboard");
			}
			
			if (servicePointToVisit != null
			        && servicePointToVisit.getUuid().equals(BotswanaEmrConstants.DOCTORS_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, DOCTORS_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr",
				    "consultation/doctorsPatientPoolDashboard?appId=botswanaemr.auxilliaryNurseDashboard&facilityId="
				            + servicePointToVisit.getLocationId());
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(ART_SERVICES_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, ART_SERVICES_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "art/artDashboard");
			}
			
			if (servicePointToVisit != null
			        && servicePointToVisit.getUuid().equals(BotswanaEmrConstants.PHARMACY_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, PHARMACY_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "pharmacy/pharmacyAllPrescriptions");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(STOCK_MANAGEMENT_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, STOCK_MANAGEMENT_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "stock/stockManagementLandingPg");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(VMMC_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, VMMC_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "vmmc/vmmcDashboard");
			}
			
			if (servicePointToVisit != null
			        && servicePointToVisit.getUuid().equals(SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT,
				    SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "srh/srhDashboard");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(TB_SERVICES_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, TB_SERVICES_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "tbservices/tbServicesDashboard");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(HIV_TESTING_SERVICES_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, HIV_TESTING_SERVICES_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "hts/htsDashboard");
			}
			
			if (servicePointToVisit != null && servicePointToVisit.getUuid().equals(LAB_SERVICES_PORTAL_UUID)) {
				sessionContext.getSession().setAttribute(CURRENT_SERVICE_DELIVERY_POINT, LAB_SERVICES_PORTAL_UUID);
				return "redirect:" + ui.pageLink("botswanaemr", "lab/groupedLaboratoryOrders");
			}
		}
		catch (Exception e) {
			log.warn("Error occurred while updating user currently logged facility", e);
		}
		finally {
			Context.removeProxyPrivilege(VIEW_LOCATIONS);
			Context.removeProxyPrivilege(GET_LOCATIONS);
		}
		
		return "redirect:" + ui.pageLink("botswanaemr", "selectServicePoint");
	}
	
	public static List<Location> getUserAssignedFacilities(@NotNull User user) {
		List<String> accessibleLocationUuids = LocationUtils.getUserAccessibleLocationUuids(user);
		
		List<Location> userLocations = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		
		userLocations.sort(Comparator.comparing(Location::getName));
		
		if (user.isSuperUser()) {
			return userLocations;
		}
		
		return userLocations.stream().filter(e -> accessibleLocationUuids.contains(e.getUuid()))
		        .sorted(Comparator.comparing(Location::getName)).collect(Collectors.toList());
	}
}
