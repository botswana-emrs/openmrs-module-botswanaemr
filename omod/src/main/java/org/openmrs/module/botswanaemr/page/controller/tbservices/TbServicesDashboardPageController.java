/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.tbservices;

import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatEncounterTypeText;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonCreator;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class TbServicesDashboardPageController extends QueuePatientFragmentController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		
		model.addAttribute("allEnrollmentsCount", getAllEnrollments(location));
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		DateTime dateTime = new DateTime();
		model.addAttribute("todayScreeningActivitiesList",
		    patientObjects(getAllScreenings(dateTime.millisOfDay().withMinimumValue().toDate(),
		        dateTime.millisOfDay().withMaximumValue().toDate(), limitFetch, location)));
		model.addAttribute("todayEnrollmentsCount", getTodayEnrollments(location));
		model.addAttribute("yesterdayEnrollmentsCount", getYesterdayEnrollments(location));
		model.addAttribute("dailyAverageEnrollments", getDailyAverageEnrollments(location));
		model.addAttribute("monthlyEnrollments", getThisMonthEnrollments(location));
		
	}
	
	private List<Encounter> getEncountersByDates(Date fromDate, Date toDate, Location location) {
		List<Encounter> tbInitialEncounters = new ArrayList<>();
		
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TB_ENCOUNTER_TYPE_UUID);
		
		if (encounterType != null && location != null) {
			EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, location, fromDate, toDate,
			        null, null, Collections.singletonList(encounterType), null, null, null, false);
			tbInitialEncounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		}
		
		return tbInitialEncounters;
	}
	
	private int getAllEnrollments(Location location) {
		List<Encounter> tbInitialEncounters = getEncountersByDates(null, null, location);
		return tbInitialEncounters.size();
	}
	
	private int getThisMonthEnrollments(Location location) {
		DateTime start = new DateTime().withTimeAtStartOfDay().dayOfMonth().withMinimumValue();
		DateTime end = new DateTime().withTimeAtStartOfDay().dayOfMonth().withMaximumValue().plusDays(1).minusMinutes(1);
		
		return getEncountersByDates(start.toDate(), end.toDate(), location).size();
	}
	
	private int getTodayEnrollments(Location location) {
		DateTime today = new DateTime().withTimeAtStartOfDay();
		return getEncountersByDates(today.toDate(), null, location).size();
	}
	
	private int getDateRangeEnrollments(Date startDate, @Nullable Date endDate) {
		List<PatientProgram> patientPrograms = new ArrayList<>();
		
		Program program = Context.getService(ProgramWorkflowService.class)
		        .getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		if (program != null) {
			patientPrograms = Context.getService(BotswanaEmrService.class).getPatientProgramByProgramAndDate(program,
			    startDate, endDate, false);
		}
		return patientPrograms.size();
	}
	
	private int getYesterdayEnrollments(Location location) {
		DateTime today = new DateTime().withTimeAtStartOfDay();
		DateTime yesterday = today.minusDays(1);
		
		return getEncountersByDates(yesterday.toDate(), null, location).size();
	}
	
	private int getDailyAverageEnrollments(Location location) {
		DateTime startDate = new DateTime(new Date()).withDayOfWeek(DateTimeConstants.MONDAY).withTimeAtStartOfDay();
		DateTime endDate = new DateTime(new Date()).withDayOfWeek(DateTimeConstants.SUNDAY).withTimeAtStartOfDay()
		        .plusDays(1).minusMinutes(1);
		
		List<Encounter> initialEncounters = getEncountersByDates(startDate.toDate(), endDate.toDate(), location);
		
		return initialEncounters.size() / 7;
	}
	
	private List<Encounter> getAllScreenings(Date start, Date end, Integer limit, Location location) {
		
		String[] encounterTypeUuids = { BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID,
		        BotswanaEmrConstants.TB_ENROLMENT_ENCOUNTER_TYPE_UUID, BotswanaEmrConstants.LAB_REQUEST_FORM_UUID,
		        BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID, BotswanaEmrConstants.XRAY_RESULT_ENCOUNTER_TYPE,
		        BotswanaEmrConstants.DRUG_TRACKING_ENCOUNTER_TYPE };
		List<EncounterType> encounterTypes = new ArrayList<>();
		
		for (String uuid : encounterTypeUuids) {
			EncounterType tbEncounterType = Context.getEncounterService().getEncounterTypeByUuid(uuid);
			if (tbEncounterType != null) {
				encounterTypes.add(tbEncounterType);
			}
		}
		
		List<Encounter> screenings = Context.getService(BotswanaEmrService.class).getEncountersList(start, end,
		    encounterTypes, limit, location);
		
		if (screenings != null && !screenings.isEmpty()) {
			screenings = screenings.stream().filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(location.getUuid()))
			        .collect(Collectors.toList());
		}
		
		return screenings;
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(BotswanaEmrUtils.formatPerson(encounter.getCreator().getPerson()));
				generalPatientObject.setDuration(BotswanaEmrUtils.getDurationSince(encounter.getEncounterDatetime()));
				generalPatientObject.setTypeText(formatEncounterTypeText(encounter.getEncounterType().getUuid()));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
}
