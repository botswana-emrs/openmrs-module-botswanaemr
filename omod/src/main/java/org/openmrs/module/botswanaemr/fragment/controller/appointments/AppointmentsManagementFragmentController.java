/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.appointments;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.AppointmentType;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.AppointmentBookingService;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.appointment.AppointmentBookingLimit;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class AppointmentsManagementFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("addAppointmentFormUuid", BotswanaEmrConstants.ADD_APPOINTMENT_FORM_UUID);
		
		Set<Concept> services = new HashSet<>();
		for (String conceptSet : BotswanaEmrConstants.ART_SERVICES_CONCEPT_SETS) {
			Concept concept = Context.getConceptService().getConceptByUuid(conceptSet);
			if (concept != null) {
				services.add(concept);
			}
		}
		fragmentModel.addAttribute("services", services);
	}
	
	public List<SimpleObject> searchAppointmentAdvanced(
	        @RequestParam(value = "nameOrUniqueId", required = false) String nameOrUniqueId,
	        @RequestParam(value = "startDate", required = false) String startDate,
	        @RequestParam(value = "endDate", required = false) String endDate,
	        @RequestParam(value = "service", required = false) String service,
	        @RequestParam(value = "status", required = false) String status,
	        @RequestParam(value = "type", required = false) String type, UiUtils uiUtils, UiSessionContext sessionContext)
	        throws ParseException {
		List<SimpleObject> response = new ArrayList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date start = StringUtils.isEmpty(startDate) ? new Date() : dateFormat.parse(startDate);
		Date end = StringUtils.isEmpty(endDate) ? new Date() : dateFormat.parse(endDate);
		
		start = BotswanaEmrUtils.getStartTimeOfDay(start);
		end = BotswanaEmrUtils.getEndTimeOfDay(end);
		
		Provider provider = sessionContext.getCurrentProvider();
		
		AppointmentType appointmentType = Context.getService(AppointmentService.class)
		        .getAppointmentTypeByUuid(BotswanaEmrConstants.ART_APPOINTMENT_TYPE_UUID);
		List<Appointment> appointmentList = Context.getService(AppointmentService.class).getAppointmentsByConstraints(start,
		    end, sessionContext.getSessionLocation(), null, null, null);
		if (StringUtils.isNotEmpty(service)) {
			appointmentList = appointmentList.stream().filter(x -> x.getReason().equals(service))
			        .collect(Collectors.toList());
		}
		EncounterType artAppointmentEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ART_APPOINTMENT_ENCOUNTER_TYPE);
		for (Appointment appointment : appointmentList) {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("appointmentId", appointment.getAppointmentId());
			simpleObject.put("appointmentDate", appointment.getTimeSlot().getStartDate());
			simpleObject.put("endDate", appointment.getTimeSlot().getEndDate());
			simpleObject.put("patientName", appointment.getPatient().getPerson().getPersonName().getFullName());
			simpleObject.put("appointmentStatus", appointment.getStatus().getName());
			simpleObject.put("reason", appointment.getReason());
			simpleObject.put("appointmentType", appointment.getAppointmentType().getName());
			simpleObject.put("patientId", appointment.getPatient().getPatientId());
			simpleObject.put("pin", appointment.getPatient()
			        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
			if (appointment.getVisit() != null) {
				simpleObject.put("visitId", appointment.getVisit().getVisitId());
			} else {
				simpleObject.put("visitId", "");
			}
			
			List<Obs> serviceObs = BotswanaEmrUtils.getObservations(appointment.getPatient(),
			    BotswanaEmrUtils.getConcept("db7c7ea5-f0a6-4922-bdbd-18a7451e7215"), appointment.getVisit());
			simpleObject.put("service", serviceObs.size() > 0 ? serviceObs.get(0).getValueCoded().getName().getName() : "");
			simpleObject.put("checkedInBy",
			    appointment.getTimeSlot().getAppointmentBlock().getProvider().getPerson().getPersonName().getFullName());
			
			Encounter apptEncounter = Context.getService(BotswanaEmrService.class)
			        .getEncountersList(appointment.getDateCreated(), appointment.getDateCreated(),
			            Collections.singletonList(artAppointmentEncounterType), null, sessionContext.getSessionLocation(),
			            appointment.getPatient())
			        .stream()
			        .filter(
			            encounter -> Objects.equals(encounter.getVisit().getVisitId(), appointment.getVisit().getVisitId()))
			        .findFirst().orElse(null);
			simpleObject.put("encounterId", apptEncounter != null ? apptEncounter.getEncounterId() : "");
			
			response.add(simpleObject);
		}
		//        response = SimpleObject.fromCollection(appointmentList, uiUtils, "appointmentId", "timeSlot", "startDate", "endDate", "visit", "patient", "status", "reason", "cancelReason", "appointmentType");
		
		return response;
	}
	
	public SimpleObject getDayLimit(@RequestParam(value = "locationId", required = false) Location location,
	        @RequestParam(value = "day", required = false) String day,
	        @RequestParam(value = "date", required = false) Date date, UiUtils uiUtils, UiSessionContext sessionContext) {
		SimpleObject simpleObject = new SimpleObject();
		String today = StringUtils.isNotBlank(day) ? day.toUpperCase() : LocalDate.now().getDayOfWeek().name();
		if (location == null) {
			location = sessionContext.getSessionLocation();
		}
		
		AppointmentBookingLimit appointmentBookingLimit = Context.getService(AppointmentBookingService.class)
		        .getAppointmentBookingLimit(location, today);
		int limit = appointmentBookingLimit != null ? appointmentBookingLimit.getLimit() : 0;
		Date appointmentDate = date != null ? date : new Date();
		Date startDate = BotswanaEmrUtils.getStartTimeOfDay(appointmentDate);
		Date endDate = BotswanaEmrUtils.getEndTimeOfDay(appointmentDate);
		AppointmentType appointmentType = Context.getService(AppointmentService.class)
		        .getAppointmentTypeByUuid(BotswanaEmrConstants.ART_APPOINTMENT_TYPE_UUID);
		List<Appointment> appointmentList = Context.getService(AppointmentService.class)
		        .getAppointmentsByConstraints(startDate, endDate, location, null, appointmentType, null);
		Visit visit;
		
		simpleObject.put("limit", limit);
		simpleObject.put("appointments", appointmentList.size());
		simpleObject.put("status", "success");
		return simpleObject;
	}
}
