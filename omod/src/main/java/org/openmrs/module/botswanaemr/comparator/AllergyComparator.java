/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.comparator;

import java.util.Comparator;

import org.openmrs.Allergy;

/**
 * Sorts allergies
 */
public class AllergyComparator implements Comparator<Allergy> {
	
	private Comparator comparator;
	
	public AllergyComparator(Comparator comparator) {
		this.comparator = comparator;
	}
	
	@Override
	public int compare(Allergy allergy1, Allergy allergy2) {
		Object obj1 = allergy1.getAllergen().getCodedAllergen();
		if (!allergy1.getAllergen().isCoded()) {
			obj1 = allergy1.getAllergen().getNonCodedAllergen();
		}
		
		Object obj2 = allergy2.getAllergen().getCodedAllergen();
		if (!allergy2.getAllergen().isCoded()) {
			obj2 = allergy2.getAllergen().getNonCodedAllergen();
		}
		
		//sort non coded allergen at the bottom
		if (obj1 instanceof String && obj2 instanceof String) {
			return obj1.toString().compareToIgnoreCase(obj2.toString());
		}
		
		return comparator.compare(obj1, obj2);
	}
}
