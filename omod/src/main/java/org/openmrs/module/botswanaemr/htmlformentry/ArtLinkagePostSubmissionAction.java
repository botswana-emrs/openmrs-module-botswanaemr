/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TO_ART_REFERRAL_ENCOUNTER_TYPE_UUID;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.openmrs.Encounter;
import org.openmrs.EncounterProvider;
import org.openmrs.Order;
import org.openmrs.ReferralOrder;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;
import org.openmrs.parameter.EncounterSearchCriteria;

public class ArtLinkagePostSubmissionAction implements CustomFormSubmissionAction {
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		if (!isFormInValidState(formEntrySession)) {
			return;
		}
		
		Encounter lastHtsReferralEncounter = getLastHtsReferralEncounter(formEntrySession);
		if (lastHtsReferralEncounter == null) {
			return;
		}
		
		Order lastHtsReferralOrder = getLastHtsReferralOrder(lastHtsReferralEncounter);
		if (lastHtsReferralOrder == null) {
			return;
		}
		
		EncounterProvider encounterProvider = getFirstEncounterProvider(formEntrySession);
		if (encounterProvider == null) {
			return;
		}
		
		cloneAndSaveOrder(lastHtsReferralOrder, formEntrySession.getEncounter(), encounterProvider);
	}
	
	private boolean isFormInValidState(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		return (mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))
		        && formEntrySession.getEncounter().getOrders().isEmpty();
	}
	
	private Encounter getLastHtsReferralEncounter(FormEntrySession formEntrySession) {
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(formEntrySession.getPatient(), null,
		        null, null, null, null,
		        Collections.singletonList(
		            Context.getEncounterService().getEncounterTypeByUuid(HTS_TO_ART_REFERRAL_ENCOUNTER_TYPE_UUID)),
		        null, null, null, false);
		
		List<Encounter> referralEncounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		return referralEncounters.stream().max(Comparator.comparing(Encounter::getEncounterDatetime)).orElse(null);
	}
	
	private Order getLastHtsReferralOrder(Encounter lastHtsReferralEncounter) {
		Set<Order> orders = lastHtsReferralEncounter.getOrders();
		return orders == null || orders.isEmpty() ? null
		        : orders.stream().filter(o -> o instanceof ReferralOrder).findFirst().orElse(null);
	}
	
	private EncounterProvider getFirstEncounterProvider(FormEntrySession formEntrySession) {
		Set<EncounterProvider> encounterProviders = formEntrySession.getEncounter().getEncounterProviders();
		return encounterProviders.isEmpty() ? null : encounterProviders.stream().findFirst().get();
	}
	
	private void cloneAndSaveOrder(Order lastHtsReferralOrder, Encounter encounter, EncounterProvider encounterProvider) {
		final String status = "COMPLETED";
		Order clonedOrder = lastHtsReferralOrder.cloneForRevision();
		clonedOrder.setFulfillerStatus(Order.FulfillerStatus.valueOf(status));
		clonedOrder.setEncounter(encounter);
		clonedOrder.setOrderer(encounterProvider.getProvider());
		orderService.saveOrder(clonedOrder, null);
	}
}
