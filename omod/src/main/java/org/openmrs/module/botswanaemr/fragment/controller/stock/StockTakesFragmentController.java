/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockTake;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StockTakesFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        UiSessionContext uiSessionContext) {
		
		List<SimplifiedStockTake> simplifiedStockTakeList = new ArrayList<>();
		List<StockOperation> stockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		if (uiSessionContext.getCurrentUser() != null) {
			for (Stockroom stockroom : stockrooms) {
				List<StockOperation> stockroomOperations = iStockOperationDataService.getOperationsByRoom(stockroom, null);
				if (!stockroomOperations.isEmpty()) {
					stockroomOperations = stockroomOperations.stream().filter(e -> e.isAdjustmentType())
					        .collect(Collectors.toList());
					stockOperations.addAll(stockroomOperations);
				}
			}
		}
		
		for (StockOperation stockOperation : stockOperations) {
			SimplifiedStockTake stockTake = new SimplifiedStockTake();
			stockTake.setId(stockOperation.getId());
			stockTake.setOperationDate(
			    BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
			stockTake.setOfficer(String.valueOf(stockOperation.getCreator()));
			stockTake.setStatus(String.valueOf(stockOperation.getStatus()));
			
			simplifiedStockTakeList.add(stockTake);
		}
		
		fragmentModel.addAttribute("allStocktake", simplifiedStockTakeList);
		
	}
}
