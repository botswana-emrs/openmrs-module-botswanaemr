/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.field;

import org.openmrs.Location;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utils.BotswanaPrivilegeConstants;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;

public class FacilityFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Location> getLocations = new ArrayList<Location>();
		User currentUSer = uiSessionContext.getCurrentUser();
		List<Location> visitLocations = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName("Visit Location"));
		if (currentUSer != null && currentUSer.hasPrivilege(BotswanaPrivilegeConstants.VIEW_BOTS_REPORTS)
		        && !visitLocations.isEmpty()) {
			getLocations.addAll(visitLocations);
		} else {
			getLocations.add(uiSessionContext.getSessionLocation());
		}
		
		fragmentModel.addAttribute("loggedInLocation", getLocations);
	}
}
