/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.openmrs.BaseOpenmrsData;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.DrugOrderSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PastPrescriptionsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient,
	        UiSessionContext uiSessionContext) {
		List<Encounter> encounterList = BotswanaEmrUtils.sortEncountersByEncounterDatetime(
		    BotswanaEmrUtils.getPrescriptionEncounters(patient, null, null, uiSessionContext.getSessionLocation()));
		
		String pastPrescriptionsLimit = Context.getAdministrationService()
		        .getGlobalProperty("pharmacyPastPrescriptionsLimit", "10");
		Set<DrugOrder> drugOrders = encounterList.stream().flatMap(encounter -> encounter.getOrders().stream())
		        .filter(order -> order instanceof DrugOrder).map(order -> (DrugOrder) order)
				.filter(order -> order.getAction().equals(Order.Action.NEW))
				.filter(order -> order.getPreviousOrder() == null)
		        .filter(order -> order.getFulfillerStatus() == null)
				.sorted(Comparator.comparing(BaseOpenmrsData::getDateCreated))
		        .limit(Integer.parseInt(pastPrescriptionsLimit)).collect(Collectors.toCollection(LinkedHashSet::new));
		
		DrugOrderSimplifier drugOrderSimplifier;
		List<DrugOrderSimplifier> drugOrderSimplifierList = new ArrayList<>();
		for (DrugOrder drugOrder : drugOrders) {
			
			drugOrderSimplifier = DrugOrderSimplifier.simplify(drugOrder);
			if (drugOrder.getDrug() !=null) {
				drugOrderSimplifier.setDrug(BotswanaEmrUtils.fetchAndformatDrugName(drugOrder.getDrug()));
			} else {
				drugOrderSimplifier.setDrug(drugOrder.getDrugNonCoded());
			}
	
			drugOrderSimplifierList.add(drugOrderSimplifier);
		}
		model.addAttribute("drugOrders", drugOrderSimplifierList);
	}
}
