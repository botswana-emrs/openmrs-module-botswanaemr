/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter;

import org.apache.commons.lang.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.springframework.core.convert.converter.Converter;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;

public class StringToStockOperationConverter implements Converter<String, StockOperation> {
	
	@Override
	public StockOperation convert(String id) {
		if (StringUtils.isBlank(id))
			return null;
		return BotswanaInventoryContext.getStockOperationDataService().getById(Integer.valueOf(id));
		
	}
}
