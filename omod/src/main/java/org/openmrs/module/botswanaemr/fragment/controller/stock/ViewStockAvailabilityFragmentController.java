/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.module.botswanaemr.model.StockCardSimplifier;
import org.openmrs.module.botswanaemr.model.TransactionSimplifier;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationTransaction;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ViewStockAvailabilityFragmentController {
	
	private final IItemDataService itemDataService = BotswanaInventoryContext.getItemDataService();
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	private final IStockOperationTypeDataService stockOperationTypeDataService = BotswanaInventoryContext
	        .getStockOperationTypeDataService();
	
	private final IStockOperationDataService stockOperationDataService = BotswanaInventoryContext
	        .getStockOperationDataService();
	
	private final IItemAttributeTypeDataService itemAttributeTypeDataService = BotswanaInventoryContext
	        .getItemAttributeTypeDataService();
	
	public void controller(FragmentModel fragmentModel) {
	}
	
	public SimpleObject changeStockRoom(@RequestParam(value = "stockRoomId") Integer stockRoomId,
	        @RequestParam(value = "itemId", required = false) Integer itemId, UiUtils ui) {
		
		final double MINIMUM_STOCK_LEVEL_THRESHOLD = 1.5;
		final double MAXIMUM_STOCK_LEVEL_THRESHOLD = 2.5;
		final double EMERGENCY_ORDER_POINT_THRESHOLD = 0.75;
		
		StockCardSimplifier simplifiedStockCard = new StockCardSimplifier();
		if (stockRoomId != null && itemId != null) {
			Stockroom stockRoom = getStockRoomById(stockRoomId);
			Item inventoryItem = itemDataService.getById(itemId);
			
			double averageMonthlyConsumption = (double) Math
			        .round(StockUtils.calculateAverageMonthlyConsumption(stockRoom, inventoryItem));
			double minimumStockLevel = averageMonthlyConsumption * MINIMUM_STOCK_LEVEL_THRESHOLD;
			double emergencyOrderPoint = averageMonthlyConsumption * EMERGENCY_ORDER_POINT_THRESHOLD;
			double maximumStockLevel = averageMonthlyConsumption * MAXIMUM_STOCK_LEVEL_THRESHOLD;
			
			List<StockOperationTransaction> stockOperationTransactions = stockroomDataService
			        .getTransactionsByRoomAndItem(stockRoom, inventoryItem, null);
			
			simplifiedStockCard.setAverageMonthlyConsumption(averageMonthlyConsumption);
			simplifiedStockCard.setMinimumStockLevel(minimumStockLevel);
			simplifiedStockCard.setMaximumStockLevel(maximumStockLevel);
			simplifiedStockCard.setEmergencyOrderPoint(emergencyOrderPoint);
			
			List<TransactionSimplifier> simplifiedTransactionList = new ArrayList<>();
			Integer quantityOnHand = 0;
			
			ItemSearch itemSearch = new ItemSearch();
			itemSearch.getTemplate().setName(inventoryItem.getName());
			
			quantityOnHand = Objects
			        .requireNonNull(
			            BotswanaInventoryContext.getStockRoomDataService().getItems(stockRoom, itemSearch, null).get(0))
			        .getQuantity();
			
			for (StockOperationTransaction stockOperationTransaction : stockOperationTransactions) {
				String familyName = stockOperationTransaction.getCreator().getFamilyName();
				String givenName = stockOperationTransaction.getCreator().getGivenName();
				String fullName = familyName.concat(givenName);
				String generatedDescription = "";
				
				if (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getInitial())) {
					quantityOnHand = stockOperationTransaction.getQuantity();
					generatedDescription = "Initial stock";
				}
				
				Integer quantityReceived = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getReceipt())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getReturn())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getReceipt())) ? stockOperationTransaction.getQuantity()
				                        : Integer.valueOf(0);
				
				Integer quantityIssued = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getDistribution())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getTransfer())) ? stockOperationTransaction.getQuantity()
				                        : Integer.valueOf(0);
				
				// Hacky way to Ensure that correct columns are updated appropriately
				if (quantityIssued < 0) {
					quantityIssued = quantityIssued * -1;
				} else if (quantityIssued > 0) {
					quantityReceived = quantityIssued;
					quantityIssued = 0;
				}
				
				Integer adjustment = stockOperationTransaction.getOperation().isAdjustmentType()
				        ? stockOperationTransaction.getQuantity()
				        : 0;
				
				Integer losses = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getDisposed())) ? stockOperationTransaction.getQuantity()
				                : Integer.valueOf(0);
				
				StockOperation batchOperation = stockOperationTransaction.getBatchOperation();
				
				TransactionSimplifier simplifiedTransaction = new TransactionSimplifier();
				simplifiedTransaction.setTransactionReference(stockOperationTransaction.getId());
				simplifiedTransaction.setTransactionDate(stockOperationTransaction.getDateCreated());
				simplifiedTransaction.setBatchNumber(batchOperation == null ? "" : batchOperation.getOperationNumber());
				simplifiedTransaction.setExpiryDate(stockOperationTransaction.getExpiration());
				simplifiedTransaction.setStockRoomId(stockRoom.getId());
				simplifiedTransaction.setStockRoomName(stockRoom.getName());
				simplifiedTransaction.setFacilityName(stockOperationTransaction.getStockroom().getLocation().getName());
				simplifiedTransaction.setInitials(getInitials(fullName));
				simplifiedTransaction.setQuantityOnHand(quantityOnHand);
				simplifiedTransaction.setQuantityReceived(quantityReceived);
				simplifiedTransaction.setQuantityIssued(quantityIssued);
				simplifiedTransaction.setLosses(losses);
				simplifiedTransaction.setAdjustment(adjustment);
				simplifiedTransaction.setRemarks((stockOperationTransaction.getOperation().getDescription() != null)
				        ? stockOperationTransaction.getOperation().getDescription()
				        : StringUtils.defaultString(generatedDescription));
				
				quantityOnHand = quantityOnHand - stockOperationTransaction.getQuantity();
				
				simplifiedTransactionList.add(simplifiedTransaction);
			}
			
			simplifiedStockCard.setTransactionSimplifierList(simplifiedTransactionList);
		}
		
		return StockCardSimplifier.simplify(simplifiedStockCard);
		
	}
	
	private Stockroom getStockRoomById(Integer stockRoomId) {
		return stockroomDataService.getById(stockRoomId);
	}
	
	public static String getInitials(String fullName) {
		int idxLastWhitespace = fullName.lastIndexOf(' ');
		return fullName.charAt(0) + "." + fullName.charAt(idxLastWhitespace + 1);
	}
	
	private String getItemAttributeValue(Set<ItemAttribute> itemAttributes, ItemAttributeType itemAttributeType) {
		for (ItemAttribute itemAttribute : itemAttributes) {
			if (Objects.equals(itemAttribute.getAttributeType(), itemAttributeType)) {
				return itemAttribute.getValue();
			}
		}
		return null;
	}
	
	public List<SimpleObject> getBatchHistory(@RequestParam(value = "itemCode", required = false) String itemCode,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam(value = "batchNumber") String batchNumber) {
		
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		Item inventoryItem = iItemDataService.getItemByCode(itemCode);
		List<SimpleObject> simplifiedTransactionList = new ArrayList<>();
		
		if (inventoryItem != null) {
			StockOperationSearch stockOperationSearch = new StockOperationSearch();
			stockOperationSearch.setOperationNumberComparisonType(BaseObjectTemplateSearch.StringComparisonType.EQUAL);
			// stockOperationSearch.getTemplate().setOperationNumber(batchNumber);
			stockOperationSearch.getTemplate().setItem(inventoryItem);
			// Fetch stock operations associated with the batch number
			IStockOperationDataService stockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
			List<StockOperation> stockOperations = stockOperationDataService.getOperations(stockOperationSearch);
			// Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			
			// Fetch associated Stock Operation transactions
			List<StockOperationTransaction> stockOperationTransactions = new ArrayList<>();
			for (StockOperation stockOperation : stockOperations) {
				stockOperationTransactions.addAll(stockOperation.getTransactions());
			}
			
			// Filter out transactions that are not associated with the batch number
			if (stockOperationTransactions.size() > 0) {
				stockOperationTransactions.removeIf(stockOperationTransaction -> !stockOperationTransaction
				        .getBatchOperation().getOperationNumber().equals(batchNumber));
				
				// Sort by date ascending
				if (stockOperationTransactions.size() > 1) {
					stockOperationTransactions
					        .sort((stockOperationTransaction1, stockOperationTransaction2) -> stockOperationTransaction1
					                .getDateCreated().compareTo(stockOperationTransaction2.getDateCreated()));
				}
			}
			
			int quantityOnHand = 0;
			
			for (StockOperationTransaction stockOperationTransaction : stockOperationTransactions) {
				String familyName = stockOperationTransaction.getCreator().getFamilyName();
				String givenName = stockOperationTransaction.getCreator().getGivenName();
				String fullName = familyName.concat(givenName);
				String generatedDescription = "";
				
				if (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getInitial())) {
					quantityOnHand = stockOperationTransaction.getQuantity();
					generatedDescription = "Initial stock";
				}
				
				Integer quantityReceived = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getReceipt())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getReturn())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getReceipt())) ? stockOperationTransaction.getQuantity()
				                        : Integer.valueOf(0);
				
				Integer quantityIssued = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getDistribution())
				        || stockOperationTransaction.getOperation().getInstanceType()
				                .equals(WellKnownOperationTypes.getTransfer())) ? stockOperationTransaction.getQuantity()
				                        : Integer.valueOf(0);
				
				Integer adjustment = stockOperationTransaction.getOperation().isAdjustmentType()
				        ? stockOperationTransaction.getQuantity()
				        : 0;
				
				Integer losses = (stockOperationTransaction.getOperation().getInstanceType()
				        .equals(WellKnownOperationTypes.getDisposed())) ? stockOperationTransaction.getQuantity()
				                : Integer.valueOf(0);
				
				quantityOnHand = quantityOnHand + quantityReceived - quantityIssued - losses - (adjustment * -1);
				
				StockOperation batchOperation = stockOperationTransaction.getBatchOperation();
				SimpleObject simplifiedTransaction = new SimpleObject();
				// Add transaction properties
				simplifiedTransaction.put("transactionReference", stockOperationTransaction.getId());
				simplifiedTransaction.put("transactionDate", stockOperationTransaction.getDateCreated());
				simplifiedTransaction.put("batchNumber", batchOperation == null ? "" : batchOperation.getOperationNumber());
				simplifiedTransaction.put("expiryDate", stockOperationTransaction.getExpiration());
				simplifiedTransaction.put("facilityName", stockOperationTransaction.getStockroom().getLocation().getName());
				simplifiedTransaction.put("initials", getInitials(fullName));
				simplifiedTransaction.put("quantityOnHand", quantityOnHand);
				simplifiedTransaction.put("quantityReceived", quantityReceived);
				simplifiedTransaction.put("quantityIssued", quantityIssued);
				simplifiedTransaction.put("losses", losses);
				simplifiedTransaction.put("adjustment", adjustment);
				simplifiedTransaction.put("remarks",
				    (stockOperationTransaction.getOperation().getDescription() != null)
				            ? stockOperationTransaction.getOperation().getDescription()
				            : StringUtils.defaultString(generatedDescription));
				
				simplifiedTransactionList.add(simplifiedTransaction);
			}
		}
		return simplifiedTransactionList;
	}
}
