/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedExternalRequisition;
import org.openmrs.module.botswanaemr.model.SimplifiedInternalRequisition;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AllExternalRequisitionFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        UiSessionContext uiSessionContext) {
		List<SimplifiedExternalRequisition> externalRequisitions = new ArrayList<>();
		List<StockOperation> allStockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		if (uiSessionContext.getCurrentUser() != null) {
			for (Stockroom stockroom : stockrooms) {
				if (stockroom.getLocation().equals(uiSessionContext.getSessionLocation())) {
					List<StockOperation> stockOperations = new ArrayList<>();
					stockOperations = iStockOperationDataService.getOperationsByRoom(stockroom, new PagingInfo(1, 10));
					stockOperations = stockOperations.stream()
					        .filter(e -> e.getInstanceType() == WellKnownOperationTypes.getExternalRequisition())
					        .collect(Collectors.toList());
					allStockOperations.addAll(stockOperations);
				}
			}
		}
		for (StockOperation stockOperation : allStockOperations) {
			SimplifiedExternalRequisition externalRequisition = new SimplifiedExternalRequisition();
			externalRequisition.setId(stockOperation.getId());
			externalRequisition.setDescription(stockOperation.getDescription());
			externalRequisition
			        .setDate(BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
			externalRequisition.setRequestFrom(String.valueOf(stockOperation.getCreator()));
			externalRequisition.setStatus(String.valueOf(stockOperation.getStatus()));
			externalRequisition.setUser(stockOperation.getCreator());
			
			externalRequisitions.add(externalRequisition);
		}
		fragmentModel.addAttribute("externalRequisitions", externalRequisitions);
		fragmentModel.addAttribute("currentUser", uiSessionContext.getCurrentUser());
	}
}
