/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.currentRegimen;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.onTreatment;

public class TbProgramFragmentController {
	
	private Visit currentVisit;
	
	private boolean hasActiveVisit;
	
	private boolean isRetrospectiveEnabled = true;
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("formService") FormService formService,
	        @FragmentParam(value = "visitId", required = false) Visit visit,
	        @FragmentParam(value = "encounterId", required = false) Encounter encounter,
	        @RequestParam(value = "form", required = false) Form form,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiSessionContext uiSessionContext) {
		
		if (uiSessionContext.getCurrentUser() != null) {
			List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
			currentVisit = visit;
			if (currentVisit == null) {
				currentVisit = encounter.getVisit();
				if (currentVisit == null) {
					currentVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(),
					    false);
				}
			}
			if (currentVisit != null) {
				if (currentVisit.getEncounters().isEmpty()) {
					currentVisit = visitService.getVisit(currentVisit.getId());
				}
				hasActiveVisit = true;
			}
			
			ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
			
			Program tbProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
			EncounterType labEncounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID);
			List<PatientProgram> allPatientPrograms = programWorkflowService.getPatientPrograms(patient, null, null, null,
			    null, null, false);
			List<PatientProgram> activeProgramEnrollmentForPatients = new ArrayList<PatientProgram>();
			
			for (PatientProgram patientProgram : allPatientPrograms) {
				if (patientProgram.getDateCompleted() == null) {
					activeProgramEnrollmentForPatients.add(patientProgram);
				}
			}
			
			boolean enrolled = activeProgramEnrollmentForPatients.size() > 0;
			boolean eligible = Programs.eligibleForProgramEnrollment(patient,
			    BotswanaEmrUtils.getEncounterTypeSearchCriteria(patient, Arrays.asList(labEncounterType)),
			    BotswanaEmrUtils.getLastDiscontinuedPatientProgram(patient, tbProgram), tbProgram);
			
			//filter out the default form brought in by reffApp
			String vitalsForm = "a000cb34-9ec1-4344-a1c8-f692232f6edd";
			String visitNoteForm = "c75f120a-04ec-11e3-8780-2b40bef9a44b";
			String admissionSimpleForm = "d2c7532c-fb01-11e2-8ff2-fd54ab5fdb2a";
			String dischargeForm = "b5f8ffd8-fbde-11e2-8ff2-fd54ab5fdb2a";
			String transferWithinHospitalForm = "a007bbfe-fbe5-11e2-8ff2-fd54ab5fdb2a";
			String triageScreeningForm = "07fada0b-8c38-44df-bf3f-f1d4120ed697";
			
			// White list of forms
			// Pre-enrolment
			String tbScreeningForm = "76154a58-9143-11ec-aa8e-eb4c1183120e";
			String labRequestForm = "1E88B447-7B50-440E-B475-09844AA0E86E";
			String labResultsForm = "abf79206-16d1-43f8-b5bc-d278420071d6";
			String xRayResultsForm = "33e87547-adaa-4b0b-acff-a6a7fe866e6a";
			String tbContactListingForm = "EBF9BC62-E788-4A8C-883E-21C8CF2F51CD";
			String tbPatientTransferInForm = "4583da2a-5e80-11ed-9b6a-0242ac120002";
			String tbPatientTransferOutForm = "8b3eca14-5c2a-11ed-9b6a-0242ac120002";
			
			// Post-enrolment
			String adverseDrugReactionForm = "28b79b9e-a377-11ec-98f0-477e79c98537";
			String drugTrackingForm = "24ab0124-a2c5-11ec-b909-0242ac120002";
			
			String vitalForm = "c5683586-083c-11ee-be56-0242ac120002";
			
			// TODO: Specify the form uuid for the TB Start regimen form
			Form tbStartRegimenForm = formService.getFormByUuid(BotswanaEmrConstants.ELIGIBILITY_AND_INITIATION_FORM_UUID);
			
			List<String> listOfFormsToIncludeUuids;
			if (enrolled) {
				listOfFormsToIncludeUuids = new ArrayList<>(
				        Arrays.asList(drugTrackingForm, adverseDrugReactionForm, labRequestForm, labResultsForm,
				            tbPatientTransferInForm, tbPatientTransferOutForm, xRayResultsForm, vitalForm));
			} else {
				listOfFormsToIncludeUuids = new ArrayList<>(
				        Arrays.asList(tbScreeningForm, labRequestForm, labResultsForm, xRayResultsForm));
			}
			EncounterSearchCriteria criteria = new EncounterSearchCriteria(patient, uiSessionContext.getSessionLocation(),
			        null, new Date(), null, null,
			        Collections.singletonList(
			            encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID)),
			        null, null, null, false);
			boolean screenedForTb = !encounterService.getEncounters(criteria).isEmpty();
			if (screenedForTb || enrolled) {
				listOfFormsToIncludeUuids.add(tbContactListingForm);
			}
			
			//form that will be clicked
			List<String> listOfFormsToExcludeUuids = Arrays.asList(vitalsForm, visitNoteForm, admissionSimpleForm,
			    dischargeForm, transferWithinHospitalForm, triageScreeningForm);
			//Only retain the forms created by the botswanaemr provider
			// allHtmlForms.removeAll(excludedForms(listOfFormsToExcludeUuids, htmlFormEntryService, formService));
			
			List<HtmlForm> completedHtmlForms = BotswanaEmrUtils.completedHtmlForms(currentVisit, htmlFormEntryService);
			// To ensure the lab request form is always available, we remove it from the list of completed forms
			completedHtmlForms.removeIf(f -> f == null || (f.getForm() != null && f.getForm().getUuid().equals(labRequestForm)));
			allHtmlForms.removeAll(completedHtmlForms);
			List<HtmlForm> list = new ArrayList<>();
			for (String htmlFormUuid : listOfFormsToIncludeUuids) {
				Optional<HtmlForm> htmlForm = allHtmlForms.stream().filter(f -> f.getForm().getUuid().equals(htmlFormUuid))
				        .findFirst();
				htmlForm.ifPresent(list::add);
			}
			
			allHtmlForms = list;
			
			List<String> finalListOfFormsToIncludeUuids = listOfFormsToIncludeUuids;
			List<HtmlForm> result = new ArrayList<>();
			for (HtmlForm f : completedHtmlForms) {
				if (f != null && f.getForm() != null) {
					if (finalListOfFormsToIncludeUuids.contains(f.getForm().getUuid())) {
						result.add(f);
					}
				}
			}
			completedHtmlForms = result;
			
			String givenName = "";
			String familyName = "";
			String middleName = "";
			if (patient != null && StringUtils.isNotBlank(patient.getGivenName())) {
				givenName = patient.getGivenName();
			}
			if (patient != null && StringUtils.isNotBlank(patient.getFamilyName())) {
				familyName = patient.getFamilyName();
			}
			if (patient != null && StringUtils.isNotBlank(patient.getMiddleName())) {
				middleName = patient.getMiddleName();
			}
			
			// Always add the tb contact listing form: This is temporary until we have a better way of handling adding contacts by editing the contact listing form
			HtmlForm tbContactListingHtmlForm = htmlFormEntryService
			        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_CONTACT_LISTING_FORM_UUID));
			if (tbContactListingHtmlForm != null) {
				if (allHtmlForms.stream().noneMatch(f -> f.getForm().getUuid().equals(tbContactListingHtmlForm.getForm().getUuid()))) {
					allHtmlForms.add(tbContactListingHtmlForm);
				}
			}
			
			
			
			model.addAttribute("availableForms", allHtmlForms);
			model.addAttribute("completedForms", completedHtmlForms);
			model.addAttribute("isRetrospectiveEnabled", isRetrospectiveEnabled);
			model.addAttribute("currentVisit", currentVisit);
			model.addAttribute("patient", patient);
			model.addAttribute("hasVisit", hasActiveVisit);
			model.addAttribute("encounters", getFilledEncounterForPatientPerTheCurrentVisit(patient, encounterService,
			    visitService, listOfFormsToExcludeUuids));
			model.addAttribute("patient", patient);
			model.addAttribute("programs", programWorkflowService.getAllPrograms());
			model.addAttribute("names", givenName + " " + familyName + " " + middleName);
			model.addAttribute("enrolled", enrolled);
			model.addAttribute("visitTypes", visitService.getAllVisitTypes());
			// model.addAttribute("currentPatientVisit", currentVisit);
			model.addAttribute("activePrograms", activeProgramEnrollmentForPatients);
			model.addAttribute("programOutComes", Programs.getTreatmentOutcomes());
			model.addAttribute("eligible", eligible);
			model.addAttribute("program", tbProgram);
			HtmlForm tbStartRegimenHtmlForm = htmlFormEntryService.getHtmlFormByForm(tbStartRegimenForm);
			model.addAttribute("tbStartRegimenForm", tbStartRegimenHtmlForm);
			model.addAttribute("returnUrl", 
				HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "programs/programs", patient, currentVisit, ui));
			assert patient != null;
			model.addAttribute("onTreatment", BotswanaEmrUtils.onTreatment(patient));
			model.addAttribute("currentRegimen", BotswanaEmrUtils.currentRegimen(patient));
			model.addAttribute("changeRegimenForm", BotswanaEmrConstants.TB_REGIMEN_CHANGE_FORM_UUID);
			model.addAttribute("tbProgramId", BotswanaEmrConstants.TB_PROGRAM_UUID);
		} else {
			model.addAttribute("availableForms", null);
			model.addAttribute("completedForms", null);
			model.addAttribute("isRetrospectiveEnabled", null);
			model.addAttribute("currentVisit", null);
			model.addAttribute("patient", null);
			model.addAttribute("hasVisit", null);
			model.addAttribute("encounters", null);
			model.addAttribute("patient", null);
			model.addAttribute("programs", null);
			model.addAttribute("names", null);
			model.addAttribute("enrolled", null);
			model.addAttribute("visitTypes", null);
			model.addAttribute("currentPatientVisit", null);
			model.addAttribute("activePrograms", null);
			model.addAttribute("programOutComes", null);
			model.addAttribute("eligible", null);
			model.addAttribute("returnUrl", null);
			model.addAttribute("program", null);
			model.addAttribute("tbStartRegimenForm", null);
			model.addAttribute("onTreatment", null);
			model.addAttribute("currentRegimen", null);
			model.addAttribute("changeRegimenForm", null);
			
		}
	}
	
	public void toggleRetrospectiveDataEntry() {
		isRetrospectiveEnabled = !isRetrospectiveEnabled;
	}
	
	private List<Encounter> getFilledEncounterForPatientPerTheCurrentVisit(Patient patient,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("visitService") VisitService visitService, List<String> listOfFormsToExcludeUuids) {
		List<Encounter> neededEncounters = new ArrayList<>();
		
		if (currentVisit != null) {
			Set<Encounter> encounterList = currentVisit.getEncounters();
			neededEncounters = encounterList.stream()
			        .filter(e -> e.getForm() != null && !listOfFormsToExcludeUuids.contains(e.getForm().getUuid()))
			        .collect(Collectors.toList());
		}
		return neededEncounters;
	}
	
	private List<HtmlForm> excludedForms(List<String> uuids,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService) {
		List<HtmlForm> allFormsToExclude = new ArrayList<HtmlForm>();
		for (String uuid : uuids) {
			Form form = formService.getFormByUuid(uuid);
			if (form != null) {
				HtmlForm htmlForm = htmlFormEntryService.getHtmlFormByForm(form);
				if (htmlForm != null) {
					allFormsToExclude.add(htmlForm);
				}
			}
		}
		return allFormsToExclude;
	}
	
}
