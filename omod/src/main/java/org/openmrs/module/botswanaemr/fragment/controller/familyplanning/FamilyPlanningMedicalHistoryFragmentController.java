/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_ENCOUNTER_TYPE_UUID;

public class FamilyPlanningMedicalHistoryFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext,
	        @RequestParam("patientId") Patient patient) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/medical-surgical-history-form.xml";
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		boolean hasEncounter = false;
		
		EncounterType medicalSurgicalHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_MEDICAL_SURGICAL_HISTORY_ENCOUNTER_TYPE_UUID);
		if (currentPatientVisit != null) {
			List<Encounter> encounters = currentPatientVisit.getNonVoidedEncounters();
			Comparator<Encounter> encounterDateTimeComparator = Comparator.comparing(Encounter::getEncounterDatetime);
			
			Optional<Encounter> medicalSurgicalHistoryEncounter = encounters.stream()
			        .filter(e -> e.getEncounterType().equals(medicalSurgicalHistoryEncounterType))
			        .max(encounterDateTimeComparator);
			
			if (medicalSurgicalHistoryEncounter.isPresent()) {
				hasEncounter = true;
				fragmentModel.addAttribute("encounter", medicalSurgicalHistoryEncounter.get());
			} else {
				fragmentModel.addAttribute("encounter", "");
			}
			fragmentModel.addAttribute("hasEncounter", hasEncounter);
		}
		fragmentModel.addAttribute("definitionUiResource", definitionUiResource);
		fragmentModel.addAttribute("formUuid", BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_FORM_UUID);
		fragmentModel.addAttribute("modalTitle", BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_FORM_MODAL_TITLE);
		List<Encounter> medicalHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    medicalSurgicalHistoryEncounterType, null, null, null);
		fragmentModel.addAttribute("medicalHistoryEncounters", medicalHistoryEncounters);
	}
}
