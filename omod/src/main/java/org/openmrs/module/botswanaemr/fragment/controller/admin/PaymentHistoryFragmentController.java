/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.RegistrationDetailSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

public class PaymentHistoryFragmentController {
	
	private final BotswanaEmrService service;
	
	public PaymentHistoryFragmentController() {
		service = Context.getService(BotswanaEmrService.class);
	}
	
	public void get(FragmentModel model, @RequestParam(value = "patientId", required = false) Patient patient,
	        UiSessionContext sessionContext) {
		
		Patient requiredPatient = null;
		if (patient != null) {
			requiredPatient = patient;
		}
		
		if (sessionContext.getCurrentUser() != null) {
			model.addAttribute("paymentHistory", getSimplifiedDate(requiredPatient, sessionContext.getSessionLocation()));
		} else {
			model.addAttribute("paymentHistory", new ArrayList<RegistrationDetailSimplifier>());
		}
	}
	
	public String post(FragmentModel model, @RequestParam(value = "patientId", required = false) Patient patient,
	        UiSessionContext sessionContext) {
		Patient requiredPatient = null;
		if (patient != null) {
			requiredPatient = patient;
		}
		model.addAttribute("paymentHistory", getSimplifiedDate(requiredPatient, sessionContext.getSessionLocation()));
		return null;
	}
	
	public List<RegistrationDetailSimplifier> getSimplifiedDate(Patient p, Location location) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		List<RegistrationDetailSimplifier> paymentDetailSimplifierList = new ArrayList<RegistrationDetailSimplifier>();
		List<Registration> paymentList = service.getPayment(p, location, null, null, false);
		RegistrationDetailSimplifier paymentDetailSimplifier;
		if (paymentList != null) {
			for (Registration payment : paymentList) {
				paymentDetailSimplifier = new RegistrationDetailSimplifier();
				paymentDetailSimplifier.setIdentifier(payment.getPatient()
				        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
				paymentDetailSimplifier.setregistrationId(payment.getRegistrationId().toString());
				paymentDetailSimplifier.setPaymentMethod(payment.getPaymentMethod().getName());
				paymentDetailSimplifier.setServiceProvider(payment.getServiceProvider().getName());
				paymentDetailSimplifier.setTransactionLocation(payment.getLocation().getName());
				paymentDetailSimplifier.setTransactionDate(
				    BotswanaEmrUtils.formatDateWithTime(payment.getRegistrationDatetime(), "dd/MM/yyyy HH:mm:ss"));
				paymentDetailSimplifier.setAmountPaid(String.valueOf(payment.getAmountPaid()));
				paymentDetailSimplifier.setBillableAmount(String.valueOf(payment.getBillableAmount()));
				paymentDetailSimplifier.setCreatedBy(BotswanaEmrUtils.formatPersonCreator(payment.getPatient()));
				paymentDetailSimplifier.setPaymentCode(payment.getPaymentCode());
				paymentDetailSimplifier
				        .setPatientName(BotswanaEmrUtils.formatPersonName(payment.getPatient().getPersonName()));
				
				//add to the list that is looped through
				paymentDetailSimplifierList.add(paymentDetailSimplifier);
				
			}
		}
		
		return paymentDetailSimplifierList;
	}
	
	public SimpleObject getTransaction(FragmentModel model, UiUtils uiUtils,
	        @RequestParam(value = "registrationId") Integer registrationId) {
		SimpleObject simpleObject = new SimpleObject();
		Registration payment = service.getPayment(registrationId);
		return SimpleObject.fromObject(payment, uiUtils, "registrationId", "paymentMethod", "serviceProvider",
		    "billableAmount", "amountPaid", "registrationDatetime");
	}
}
