/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs.tb;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import liquibase.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterRole;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MEDICATION_QUANTITY_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_TREATMENT_TYPE_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.WEIGHT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDefaultEncounterRole;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getProvider;

@Slf4j
public class DotBatchUpdatesFragmentController {
	
	public void controller(@RequestParam("patientId") Patient patient, PageModel pageModel,
	        UiSessionContext sessionContext) {
		
		Date enrollmentDate = null;
		Date completionDate = null;
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program tbProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, tbProgram, null, null,
		    null, null, false);
		List<PatientProgram> activePrograms = patientProgramList.stream().filter(PatientProgram::getActive)
		        .collect(Collectors.toList());
		
		if (activePrograms.size() > 0) {
			enrollmentDate = activePrograms.get(0).getDateEnrolled();
			completionDate = activePrograms.get(0).getDateCompleted();
		}
		
		pageModel.addAttribute("patient", patient);
		
		String enrollmentDateStr;
		if (enrollmentDate != null) {
			enrollmentDateStr = DateUtils.getStringDate(enrollmentDate, "yyyy-MM-dd");
		} else {
			enrollmentDateStr = "";
		}
		pageModel.addAttribute("enrollmentDate", enrollmentDateStr);
		pageModel.addAttribute("completionDate", completionDate);
		pageModel.addAttribute("treatmentTypes", getTreatmentTypes());
		pageModel.addAttribute("administers", getAdministers());
		pageModel.addAttribute("facilities", getFacilities());
		pageModel.addAttribute("providers", getProviderList());
		Double latestPatientWeight = getLatestPatientWeight(patient);
		pageModel.addAttribute("lastWeight", latestPatientWeight == null ? "" : latestPatientWeight);
		pageModel.addAttribute("drugs", getFixedDoseDrugs());
	}
	
	protected static List<Provider> getProviderList() {
		return Context.getProviderService().getAllProviders(false);
	}
	
	protected static List<Concept> getTreatmentTypes() {
		Concept concept = new Concept();
		return new ArrayList<>(Arrays.asList(
		    Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.INITIAL_TREATMENT_PHASE_UUID),
		    Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.CONTINUATION_TREATMENT_PHASE_UUID)));
	}
	
	@SneakyThrows
	public String dotBatchUpdate(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data, UiSessionContext sessionContext) {
		
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		sessionContext.getSessionLocation();
		
		ObjectMapper mapper = new ObjectMapper();
		List<DotMapper> dotMappers = mapper.readValue(data, new TypeReference<List<DotMapper>>() {
			
		});
		
		List<Encounter> savedEncounters = new ArrayList<>();
		for (DotMapper dotMapper : dotMappers) {
			if (StringUtil.isNotEmpty(dotMapper.getDate())) {
				savedEncounters.add(createDOT(dotMapper, patient, sessionContext));
			}
		}
		return savedEncounters.toString();
	}
	
	protected Encounter createDOT(@NotNull DotMapper dotMapper, Patient patient, UiSessionContext sessionContext)
	        throws ParseException {
		log.debug("Creating DOT encounter for patient: {} with {} ", patient.getId(), dotMapper);
		
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.DOT_ENCOUNTER_TYPE_UUID);
		
		Encounter encounter = createEncounterForRetrospectiveDataEntry(patient, encounterType,
		    sessionContext.getSessionLocation(), dotMapper);
		//set encounter date
		encounter.setEncounterDatetime(BotswanaEmrUtils.formatDateFromString(dotMapper.getDate(), "yyyy-MM-dd"));
		//set encounter provider
		if (dotMapper.getProvider() != null) {
			Provider provider = Context.getProviderService().getProviderByUuid(dotMapper.getProvider());
			EncounterRole encounterRole = Context.getEncounterService()
			        .getEncounterRoleByUuid(BotswanaEmrConstants.DOT_ENCOUNTER_ROLE_UUID);
			if (provider != null && encounterRole != null) {
				encounter.setProvider(encounterRole, provider);
			}
		}
		
		Encounter savedEncounter = Context.getEncounterService().saveEncounter(encounter);
		
		//No grouping concept for DOT
		ConceptService conceptService = Context.getConceptService();
		//159792AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		Concept treatmentTypeQuestion = conceptService.getConceptByUuid(TB_TREATMENT_TYPE_CONCEPT_UUID);
		Concept treatmentType = conceptService.getConceptByUuid(dotMapper.getTreatmentType());
		Obs obs = BotswanaEmrUtils.creatObs(savedEncounter, treatmentTypeQuestion);
		obs.setValueCoded(treatmentType);
		Context.getObsService().saveObs(obs, null);
		savedEncounter.addObs(obs);
		
		Concept drugQuestion = conceptService.getConceptByUuid(BotswanaEmrConstants.CURRENT_DRUGS_CONCEPT_UUID);
		Concept drugAnswer = conceptService.getConceptByUuid(dotMapper.getDrug());
		Obs drugObservation = BotswanaEmrUtils.creatObs(savedEncounter, drugQuestion);
		drugObservation.setValueCoded(drugAnswer);
		Context.getObsService().saveObs(drugObservation, null);
		savedEncounter.addObs(drugObservation);
		
		Concept administerByQuestion = conceptService.getConceptByUuid(BotswanaEmrConstants.ADMINISTERED_CONCEPT_UUID);
		Concept administerByAnswer = conceptService.getConceptByUuid(dotMapper.getAdministerBy());
		Obs obsAdministerBy = BotswanaEmrUtils.creatObs(savedEncounter, administerByQuestion);
		obsAdministerBy.setValueCoded(administerByAnswer);
		Context.getObsService().saveObs(obsAdministerBy, null);
		savedEncounter.addObs(obsAdministerBy);
		
		// Date administered as an Obs
		Concept dateAdministeredQuestion = conceptService
		        .getConceptByUuid(BotswanaEmrConstants.DATE_ADMINISTERED_CONCEPT_UUID);
		Obs obsDateAdministered = BotswanaEmrUtils.creatObs(savedEncounter, dateAdministeredQuestion);
		obsDateAdministered.setValueDate(BotswanaEmrUtils.formatDateFromString(dotMapper.getDate(), "yyyy-MM-dd"));
		Context.getObsService().saveObs(obsDateAdministered, null);
		savedEncounter.addObs(obsDateAdministered);
		
		// Saving Facility as an Obs
		Concept facilityQuestion = conceptService.getConceptByUuid(BotswanaEmrConstants.FACILITY_CONCEPT_UUID);
		Obs obsFacility = BotswanaEmrUtils.creatObs(savedEncounter, facilityQuestion);
		obsFacility.setValueText(dotMapper.getFacility());
		Context.getObsService().saveObs(obsFacility, null);
		savedEncounter.addObs(obsFacility);
		
		Concept weightQuestion = conceptService.getConceptByUuid(WEIGHT_CONCEPT_UUID);
		Obs obsWeight = BotswanaEmrUtils.creatObs(savedEncounter, weightQuestion);
		obsWeight.setValueNumeric(Double.valueOf(dotMapper.getWeight()));
		Context.getObsService().saveObs(obsWeight, null);
		savedEncounter.addObs(obsWeight);
		
		Concept quantityQuestion = conceptService.getConceptByUuid(MEDICATION_QUANTITY_CONCEPT_UUID);
		Obs obsQuantity = BotswanaEmrUtils.creatObs(savedEncounter, quantityQuestion);
		obsQuantity.setValueNumeric(Double.valueOf(dotMapper.getQuantity()));
		Context.getObsService().saveObs(obsQuantity, null);
		savedEncounter.addObs(obsQuantity);
		
		Concept remarks = conceptService.getConceptByUuid(BotswanaEmrConstants.REMARKS_CONCEPT_UUID);
		Obs obsRemarks = BotswanaEmrUtils.creatObs(savedEncounter, remarks);
		obsRemarks.setValueText(dotMapper.getRemarks());
		Context.getObsService().saveObs(obsRemarks, null);
		savedEncounter.addObs(obsRemarks);
		
		Concept dateQuestion = conceptService.getConceptByUuid(BotswanaEmrConstants.DATE_CONCEPT_UUID);
		Obs obsDate = BotswanaEmrUtils.creatObs(savedEncounter, dateQuestion);
		obsDate.setValueDate(BotswanaEmrUtils.formatDateFromString(dotMapper.getDate(), "yyyy-MM-dd"));
		Context.getObsService().saveObs(obsDate, null);
		savedEncounter.addObs(obsDate);
		
		log.debug("created encounter" + savedEncounter);
		
		Encounter encounterToBeUpdateOnceMore = null;
		if (savedEncounter.getAllObs().size() > 0) {
			//Just in case the encounter is not saved, we need to save it again
			log.debug("update encounter with obs...");
			encounterToBeUpdateOnceMore = Context.getEncounterService().saveEncounter(savedEncounter);
		}
		
		// now, close the visit
		if (encounterToBeUpdateOnceMore != null) {
			closeVisit(encounterToBeUpdateOnceMore.getVisit());
		}
		return encounterToBeUpdateOnceMore;
	}
	
	private List<Concept> getFixedDoseDrugs() {
		return Arrays.asList(getDrug("e0ae875d-15d4-4777-b303-026b48c8bf40"),
		    getDrug("fbb7afc3-6a1a-4b3b-9428-fb8edf628af5"), getDrug("6cd7233e-1105-466c-b3cf-4cf7cf0be81b"),
		    getDrug("d12de6ea-20dd-4f33-92a4-5fc0acd66825"), getDrug("038a6cc3-07a3-4396-a94e-ebdba61b6e28"),
		    getDrug("02a51202-8e00-493f-8cd7-db2eb98a39dc"));
	}
	
	private Concept getDrug(@NotNull String drug) {
		return Context.getConceptService().getConceptByUuid(drug);
	}
	
	protected static List<Concept> getAdministers() {
		return new ArrayList<>(
		        Arrays.asList(Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.PHYSICIAN_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.MISSED_CONCEPT_UUID)));
	}
	
	protected static List<Location> getFacilities() {
		return BotswanaEmrUtils.getFacilitiesList();
	}
	
	private Double getLatestPatientWeight(Patient patient) {
		Concept weightConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.WEIGHT_CONCEPT_UUID);
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		
		Obs lastWeightObs = Context.getObsService().getObservationsByPersonAndConcept(patient, weightConcept).stream()
		        .max(obsDateComparator).orElse(null);
		return lastWeightObs != null ? lastWeightObs.getValueNumeric() : null;
	}
	
	protected static Encounter createEncounterForRetrospectiveDataEntry(Patient patient, EncounterType encounterType,
	        Location location, DotMapper dotMapper) throws ParseException {
		// create new encounter
		Encounter encounter = new Encounter();
		encounter.setEncounterType(encounterType);
		encounter.setCreator(Context.getAuthenticatedUser());
		encounter.setProvider(getDefaultEncounterRole(), getProvider(Context.getAuthenticatedUser().getPerson()));
		encounter.setPatient(patient);
		encounter.setLocation(location);
		
		Visit visit = createOPVisitForRetrospectiveDataEntry(patient, location, dotMapper);
		if (visit != null) {
			encounter.setVisit(visit);
		}
		if (dotMapper.getDate() != null) {
			encounter.setEncounterDatetime(BotswanaEmrUtils.formatDateFromString(dotMapper.getDate(), "yyyy-MM-dd"));
		}
		
		return encounter;
	}
	
	/**
	 * Create a new visit for the patient if one does not exist.
	 *
	 * @param patient the patient
	 * @param location the location
	 * @param dotMapper the dot mapper
	 * @return the visit
	 * @throws ParseException
	 */
	private static Visit createOPVisitForRetrospectiveDataEntry(Patient patient, Location location, DotMapper dotMapper)
	        throws ParseException {
		Visit visit = new Visit();
		visit.setPatient(patient);
		visit.setLocation(location);
		//OP visit type
		visit.setVisitType(Context.getVisitService().getVisitTypeByUuid(BotswanaEmrConstants.OUTPATIENT_VISIT_TYPE_UUID));
		visit.setStartDatetime(BotswanaEmrUtils.formatDateFromString(dotMapper.getDate(), "yyyy-MM-dd"));
		
		return Context.getVisitService().saveVisit(visit);
	}
	
	/**
	 * Closes the given visit.
	 *
	 * @param visit the visit to close
	 */
	protected static void closeVisit(Visit visit) {
		visit.setStopDatetime(new Date());
		Context.getVisitService().saveVisit(visit);
	}
	
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class DotMapper {
		
		private String treatmentType;
		
		private String drug;
		
		private String administerBy;
		
		private String date;
		
		private String endDate;
		
		private String remarks;
		
		private String weight;
		
		private String quantity;
		
		private String provider;
		
		private String facility;
	}
}
