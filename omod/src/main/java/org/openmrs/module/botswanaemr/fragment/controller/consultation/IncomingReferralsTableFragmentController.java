/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Provider;
import org.openmrs.Order.FulfillerStatus;
import org.openmrs.api.OrderService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.OrderSearchCriteria;
import org.openmrs.parameter.OrderSearchCriteriaBuilder;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;

public class IncomingReferralsTableFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext,
	        @SpringBean("orderService") OrderService orderService) {
		
		Provider currentProvider = sessionContext.getCurrentProvider();
		fragmentModel.addAttribute("currentProvider", currentProvider);
		fragmentModel.addAttribute("openmrsId", BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME);
		OrderType referralOrder = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		OrderSearchCriteria orderSearchCriteria = BotswanaEmrUtils.getOrderSearchCriteria(referralOrder);
		OrderSearchCriteriaBuilder orderSearchCriteriaBuilder = new OrderSearchCriteriaBuilder();
		orderSearchCriteriaBuilder.setOrderTypes(orderSearchCriteria.getOrderTypes());
		orderSearchCriteriaBuilder.setFulfillerStatus(FulfillerStatus.RECEIVED);
		OrderSearchCriteria orderSearchCriteria1 = orderSearchCriteriaBuilder.build();
		List<Order> referrals = orderService.getOrders(orderSearchCriteria1).stream().filter(Order::isActive)
		        .collect(Collectors.toList());
		if (!referrals.isEmpty()) {
			// Filter referrals targeted to the current facility
			List<Order> incomingReferrals = new ArrayList<>();
			for (Order order : referrals) {
				if (order.getEncounter() != null) {
					if (order.getEncounter().getObs() != null) {
						if (order.getEncounter().getObs().stream().anyMatch(
						    obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.RECEIVING_DEPARTMENT_CONCEPT_UUID)
						            && obs.getComment().equals(sessionContext.getSessionLocation().getName()))) {
							incomingReferrals.add(order);
						}
					}
				}
			}
			
			fragmentModel.addAttribute("incomingReferral", incomingReferrals);
		} else {
			fragmentModel.addAttribute("incomingReferral", "");
		}
	}
}
