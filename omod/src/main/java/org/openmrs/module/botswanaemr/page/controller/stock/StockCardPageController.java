/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.openmrs.Location;
import org.openmrs.LocationAttribute;
import org.openmrs.LocationAttributeType;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.InventoryItemSimplifier;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class StockCardPageController {
	
	private final IItemDataService itemDataService = BotswanaInventoryContext.getItemDataService();
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void controller(PageModel pageModel, UiSessionContext uiSessionContext,
	        @RequestParam(value = "itemId", required = false) Integer itemId,
	        @SpringBean("locationService") LocationService locationService) {
		
		if (uiSessionContext.getCurrentUser() != null && itemId != null) {
			Item inventoryItem = itemDataService.getById(itemId);
			LocationAttributeType locationAttributeType = locationService
			        .getLocationAttributeTypeByUuid(MASTER_FACILITY_CODE_UUID);
			String globalProperty = Context.getAuthenticatedUser().getUserProperty(USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
			    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
			
			List<Location> locations = locationService
			        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
			
			Set<Stockroom> stockRooms = new HashSet<>();
			List<Stockroom> stockrooms = stockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(),
			    false);
			for (Stockroom stockroom : stockrooms) {
				Set<Stockroom> stockroomSet = stockroom.getItems().stream().filter(i -> i.getItem().equals(inventoryItem))
				        .map(ItemStock::getStockroom).collect(Collectors.toSet());
				stockRooms.addAll(stockroomSet);
			}
			
			LocationAttribute facilityCode = null;
			for (Location location : locations) {
				if (location.getName().equals(globalProperty)) {
					facilityCode = location.getAttributes().stream()
					        .filter(a -> a.getAttributeType().equals(locationAttributeType)).findFirst().orElse(null);
				}
			}
			pageModel.addAttribute("facilityCode", facilityCode != null ? facilityCode.getValue() : "");
			pageModel.addAttribute("stockRooms", stockRooms);
			pageModel.addAttribute("itemId", itemId);
			pageModel.addAttribute("hospital", globalProperty);
			InventoryItemSimplifier simplifiedItem = InventoryItemSimplifier.simplify(inventoryItem);
			
			pageModel.addAttribute("commodityCode", simplifiedItem.getCode());
			pageModel.addAttribute("commodityName", simplifiedItem.getName());
			pageModel.addAttribute("unitOfIssue", simplifiedItem.getUnitOfIssue());
			
		} else {
			pageModel.addAttribute("facilityCode", "");
			pageModel.addAttribute("hospital", "");
			pageModel.addAttribute("commodityCode", "");
			pageModel.addAttribute("commodityName", "");
			pageModel.addAttribute("unitOfIssue", "");
			pageModel.addAttribute("stockRooms", "");
			pageModel.addAttribute("itemId", null);
		}
	}
}
