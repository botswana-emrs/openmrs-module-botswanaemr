/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.appointments;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDefaultAppointmentType;

import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.api.context.UserContext;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.AppointmentType;
import org.openmrs.module.appointmentscheduling.TimeSlot;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.webservices.rest.web.response.ObjectNotFoundException;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MyAppointmentsFragmentController {
	
	public void controller(@SpringBean FragmentModel model, UiSessionContext uiSessionContext) {
		model.addAttribute("location", uiSessionContext.getSessionLocation());
		model.addAttribute("events", this.getCalenderEventsForProvider());
		model.addAttribute("taskAppointmentPatient", this.getAppointmentTaskPatient());
	}
	
	public String createAppointment(@RequestParam(value = "appointmentDate", required = false) String appointmentDate,
	        @RequestParam(value = "startTime", required = false) String startTime,
	        @RequestParam(value = "endTime", required = false) String endTime, @RequestParam(value = "type") String type,
	        @RequestParam(value = "flow", required = false) String flow,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "notes", required = false) String notes, UiSessionContext uiSessionContext,
	        Integer locationId) throws ParseException {
		
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		LocationService locationService = Context.getLocationService();
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		
		Appointment appointment = new Appointment();
		appointment.setDateCreated(new Date());
		
		if (patient == null && type.equals("Task")) {
			//Fake appointment patient
			appointment.setPatient(getAppointmentTaskPatient());
		} else {
			appointment.setPatient(patient);
		}
		
		if (StringUtils.isNotBlank(type)) {
			appointment.setAppointmentType(getUserDefinedAppointmentType(type));
		}
		appointment.setDateCreated(new Date());
		appointment.setCreator(Context.getAuthenticatedUser());
		
		if (StringUtils.isNotBlank(notes)) {
			appointment.setReason(notes);
		}
		if (StringUtils.isNotBlank(flow)) {
			appointment.setStatus(Appointment.AppointmentStatus.WALKIN);
			//Start a new visit
			VisitType facilityVisitType = Context.getVisitService()
			        .getVisitTypeByUuid(BotswanaEmrConstants.FACILITY_VISIT_VISIT_TYPE_UUID);
			
			Visit visit = new Visit(patient, facilityVisitType, new Date());
			visit.setLocation(locationService.getLocation(locationId));
			visit = Context.getVisitService().saveVisit(visit);
			appointment.setVisit(visit);
		} else {
			//add this time slot to the appointment
			if (StringUtils.isNotBlank(appointmentDate) && StringUtils.isNotBlank(startTime)
			        && StringUtils.isNotBlank(endTime)) {
				String startDateStr = appointmentDate + " " + startTime;
				String endDateStr = appointmentDate + " " + endTime;
				Date startDate = BotswanaEmrUtils.formatDateFromStringWithTime(startDateStr);
				Date endDate = BotswanaEmrUtils.formatDateFromStringWithTime(endDateStr);
				
				TimeSlot appointmentTimeSlot = BotswanaEmrUtils.getAppointmentTimeSlot(startDate, endDate, provider,
				    locationId == null ? uiSessionContext.getSessionLocation() : locationService.getLocation(locationId),
				    getUserDefinedAppointmentType(type));
				
				appointmentService.saveAppointmentBlock(appointmentTimeSlot.getAppointmentBlock());
				appointmentService.saveTimeSlot(appointmentTimeSlot);
				appointment.setTimeSlot(appointmentTimeSlot);
				appointment.setStatus(Appointment.AppointmentStatus.SCHEDULED);
				appointmentService.saveAppointment(appointment);
			}
		}
		return null;
	}
	
	private AppointmentType getUserDefinedAppointmentType(@NonNull String type) {
		if (type.equals("Task")) {
			//task appointment type
			return getTaskAppointmentType();
		} else {
			// follow-up appointment
			return getDefaultAppointmentType();
		}
	}
	
	private Patient getAppointmentTaskPatient() {
		String patientUuid = Context.getAdministrationService()
		        .getGlobalProperty(BotswanaEmrConstants.TASK_APPOINTMENT_PATIENT_UUID, "Unknown");
		if (patientUuid.equals("Unknown")) {
			// Not configured
			log.error("please configure appointment task patient via GP property {}",
			    BotswanaEmrConstants.TASK_APPOINTMENT_PATIENT_UUID);
			throw new ObjectNotFoundException("Couldn't find fake task appointment patient");
		}
		Patient fakePatient = Context.getPatientService().getPatientByUuid(patientUuid);
		
		if (fakePatient == null) {
			throw new ObjectNotFoundException("Couldn't find patient with uuid " + patientUuid + " for task appointments");
		}
		return fakePatient;
	}
	
	private AppointmentType getTaskAppointmentType() {
		AppointmentService service = Context.getService(AppointmentService.class);
		AppointmentType appointmentType = service.getAppointmentTypeByUuid(BotswanaEmrConstants.TASK_APPOINTMENT_TYPE);
		if (appointmentType == null) {
			appointmentType = createTaskAppointmentType(service);
		}
		return appointmentType;
	}
	
	private AppointmentType createTaskAppointmentType(AppointmentService service) {
		AppointmentType taskAppointmentType = new AppointmentType();
		taskAppointmentType.setUuid(BotswanaEmrConstants.TASK_APPOINTMENT_TYPE);
		taskAppointmentType.setName("Task appointment");
		taskAppointmentType.setDescription("Task appointment type");
		taskAppointmentType.setDuration(20);
		
		return service.saveAppointmentType(taskAppointmentType);
	}
	
	public List<Appointment> getAppointmentsByLoggedInProvider() {
		User user = Context.getUserContext().getAuthenticatedUser();
		Provider provider = null;
		if (user != null) {
			provider = BotswanaEmrUtils.getProvider(user.getPerson());
		}
		
		//Lower limit starts from yesterday to include today's events
		Instant now = Instant.now();
		Instant yesterday = now.minus(1, ChronoUnit.DAYS);
		
		return new ArrayList<>(Context.getService(AppointmentService.class)
		        .getAppointmentsByConstraints(Date.from(yesterday), null, null, provider, null, null));
	}
	
	public List<ScheduleFragmentController.Event> getCalenderEventsForProvider() {
		List<ScheduleFragmentController.Event> events = new ArrayList<>();
		for (Appointment appointment : this.getAppointmentsByLoggedInProvider()) {
			ScheduleFragmentController.Event event = new ScheduleFragmentController.Event();
			if (appointment.getAppointmentType().getUuid().equals(BotswanaEmrConstants.TASK_APPOINTMENT_TYPE)) {
				event.setTitle(appointment.getReason());
			} else {
				event.setTitle(appointment.getPatient().getPerson().getPersonName().getFullName());
			}
			event.setStartDate(appointment.getTimeSlot().getStartDate());
			event.setEndDate(appointment.getTimeSlot().getEndDate());
			events.add(event);
		}
		return events;
	}
	
}
