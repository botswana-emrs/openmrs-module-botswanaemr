/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.anc;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.regimen.SimplifiedRegimenComponentDrug;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AncProfilePageController {
	
	public void controller(PageModel model, UiSessionContext sessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "visitId") Visit visit,
	        @RequestParam(value = "selectedFormUuid", required = false) String selectedFormUuid) {
		model.addAttribute("patient", patient);
		model.addAttribute("visit", visit);
		model.addAttribute("defaultEncounterDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
		
		List<SimpleObject> ancForms = new ArrayList<>();
		ancForms.add(getFormData("Antenatal Enrollment", BotswanaEmrConstants.ANC_ANTENATAL_ENROLMENT_FORM_UUID,
		    BotswanaEmrConstants.ANC_ANTENATAL_ENROLMENT_ENCOUNTER_TYPE_UUID));
		ancForms.add(getFormData("Antenatal History", BotswanaEmrConstants.ANC_ANTENATAL_HISTORY_FORM_UUID,
		    BotswanaEmrConstants.ANTENATAL_REGISTRY_ENCOUNTER_TYPE_UUID));
		ancForms.add(getFormData("ANC New Risks After Booking", BotswanaEmrConstants.ANC_NEW_RISKS_AFTER_BOOKING_FORM_UUID,
		    BotswanaEmrConstants.ANC_NEW_RISKS_AFTER_BOOKING_ENCOUNTER_TYPE_UUID));
		ancForms.add(getFormData("ANC Foetal Growth and Maternity Weight",
		    BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_WEIGHT_FORM_UUID,
		    BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_ENCOUNTER_TYPE));
		model.addAttribute("ancForms", ancForms);
		JsonArray results = (JsonArray) new Gson().toJsonTree(ancForms, new TypeToken<List<SimpleObject>>() {}.getType());
		model.addAttribute("results", results);
		String selected = StringUtils.isNotEmpty(selectedFormUuid) ? selectedFormUuid : "";
		model.addAttribute("selected", selected);
	}
	
	private SimpleObject getFormData(String title, String formUuid, String encounterTypeUuid) {
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("title", title);
		simpleObject.put("form", formUuid);
		simpleObject.put("encounterType", encounterTypeUuid);
		
		return simpleObject;
	}
}
