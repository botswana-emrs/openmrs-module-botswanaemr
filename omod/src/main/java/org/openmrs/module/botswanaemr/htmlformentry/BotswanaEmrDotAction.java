/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.CareSetting;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderFrequency;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.api.OrderContext;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

@Slf4j
public class BotswanaEmrDotAction implements CustomFormSubmissionAction {
	
	private static final String ETHAMBUTANOL_CONCEPT_ID = "d12de6ea-20dd-4f33-92a4-5fc0acd66825";
	
	// For each of the drugs dispensed, create a prescription
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		if (formEntrySession.getEncounter().getEncounterType().equals(
		    Context.getEncounterService().getEncounterTypeByUuid(BotswanaEmrConstants.DRUG_TRACKING_ENCOUNTER_TYPE))) {
			OrderService drugOrderService = Context.getRegisteredComponent("orderService", OrderService.class);
			// Context.getOrderService().saveOrder(drugOrder, orderContext);
			saveDrugOrders(formEntrySession.getPatient(), formEntrySession.getEncounter(), drugOrderService);
		}
		/* 
				Concept stockroomIdConcept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.HTS_STOCKROOM_UUID);
		
				Obs stockroomIdObs = formEntrySession.getEncounter().getObs().stream()
						.filter(o -> o.getConcept().equals(stockroomIdConcept))
						.findFirst().orElse(null);
				Stockroom stockroom = null;
				if (stockroomIdObs != null) {
					String stockroomUuid = stockroomIdObs.getValueText();
					stockroom = BotswanaInventoryContext.getStockRoomDataService().getByUuid(stockroomUuid);
				}
		
				if (stockroom == null) {
					// log debug message
					log.debug("Stockroom not found");
					return;
				}
		
				IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
				IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
				Item item = iItemDataService.getByUuid(BotswanaEmrConstants.SELF_TEST_KIT_UUID);
				for (Obs obs : formEntrySession.getEncounter().getObs()) {
					String lotNumber = "";
					double quantity = 0.0;
					Date expirationDate = new Date();
		
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_TEST_KIT_LOT_UUID)) {
						lotNumber = obs.getValueText();
		
						StockOperation stockOperation = new StockOperation();
						StockOperation batchOperation = iStockOperationDataService.getOperationByNumber(lotNumber);
						stockOperation.addItem(item, (int) Math.floor(quantity), expirationDate, batchOperation);
						stockOperation.setOperationDate(new Date());
		
						stockOperation.setSource(stockroom);
						stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
						stockOperation.setOperationNumber(lotNumber);
						stockOperation.setStatus(StockOperationStatus.NEW);
						BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
						stockOperation.setStatus(StockOperationStatus.COMPLETED);
						BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
					}
				}
		*/
	}
	
	private void saveDrugOrders(Patient patient, Encounter encounter, OrderService orderService) {
		OrderType orderType = orderService.getOrderTypeByUuid(DRUG_ORDER_TYPE_UUID);
		
		// Get non-coded drugs obs from the encounter
		Obs noCodedDrugObs = encounter.getObs().stream()
		        .filter(o -> o.getConcept().getUuid().equals("c27aee17-7782-4eb7-80ac-9c9214e3d693")).findFirst()
		        .orElse(null);
		
		for (Obs obs : encounter.getObs()) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CURRENT_DRUGS_CONCEPT_UUID)) {
				DrugOrder drugOrder = new DrugOrder();
				drugOrder.setPatient(patient);
				drugOrder.setEncounter(encounter);
				
				Concept concept = obs.getValueCoded();
				Drug drug = null;
				if (concept.getUuid().equals(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID)) {
					if (noCodedDrugObs == null) {
						continue;
					}
					drugOrder.setDrugNonCoded(noCodedDrugObs.getValueText());
				} else {
					drug = Context.getConceptService().getDrug(concept.getName().getName());
					if (drug == null || orderType == null) {
						continue;
					}
					drugOrder.setDrug(drug);
				}
				
				Concept weightConcept = BotswanaEmrUtils.getConcept(WEIGHT_CONCEPT_UUID);
				Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
				Obs lastWeightObs = Objects
				        .requireNonNull(Context.getObsService().getObservationsByPersonAndConcept(patient, weightConcept)
				                .stream().max(obsDateComparator).orElse(null));
				Concept treatmentTypeConcept = BotswanaEmrUtils
				        .getConcept(BotswanaEmrConstants.TB_TREATMENT_TYPE_CONCEPT_UUID);
				Obs treatmentTypeObs = encounter.getObs().stream().filter(e -> e.getConcept().equals(treatmentTypeConcept))
				        .findFirst().orElse(null);
				Concept treatmentGroupConcept = BotswanaEmrUtils
				        .getConcept(BotswanaEmrConstants.TB_TREATMENT_GROUP_CONCEPT_UUID);
				Obs treatmentGroupObs = Context.getObsService()
				        .getObservationsByPersonAndConcept(patient, treatmentGroupConcept).stream()
				        .filter(e -> e.getConcept().equals(treatmentGroupConcept)).max(obsDateComparator).orElse(null);
				
				Concept drugAdministrationConcept = BotswanaEmrUtils.getConcept(ADMINISTERED_CONCEPT_UUID);
				Obs drugAdministrationObs = encounter.getObs().stream()
				        .filter(e -> e.getConcept().equals(drugAdministrationConcept)).findFirst().orElse(null);
				
				if (treatmentGroupObs != null && treatmentTypeObs != null && treatmentTypeObs.getValueCoded() != null
				        && treatmentGroupObs.getValueCoded() != null) {
					FixedDoseParameter fixedDoseParameters = getFixedDoseParameter(
					    patient.getAge(encounter.getEncounterDatetime()), lastWeightObs.getValueNumeric(),
					    treatmentTypeObs.getValueCoded(), treatmentGroupObs.getValueCoded(), drug);
					
					drugOrder.setDose(1.0);
					
					Obs medicationDoseQuantityObs = encounter.getObs().stream()
					        .filter(o -> o.getConcept().getUuid().equals(MEDICATION_QUANTITY_CONCEPT_UUID)).findFirst()
					        .orElse(null);
					
					Double medicationDoseQuantity = medicationDoseQuantityObs == null ? fixedDoseParameters.getQuantity()
					        : medicationDoseQuantityObs.getValueNumeric();
					
					if (medicationDoseQuantity > 0) {
						
						drugOrder.setQuantity(medicationDoseQuantity);
						
						drugOrder.setCommentToFulfiller(fixedDoseParameters.regimen);
						
						Concept tablet = BotswanaEmrUtils.getConcept(TABLET_CONCEPT_UUID);
						drugOrder.setDoseUnits(tablet);
						drugOrder.setQuantityUnits(tablet);
						
						Concept oralRoute = BotswanaEmrUtils.getConcept(ORAL_ROUTE_CONCEPT_UUID);
						drugOrder.setRoute(oralRoute);
						
						Concept orderFrequencyConcept = Context.getConceptService()
						        .getConceptByUuid(ONCE_DAILY_CONCEPT_UUID);
						drugOrder.setFrequency(orderService.getOrderFrequencyByConcept(orderFrequencyConcept));
						
						drugOrder.setOrderer(
						    Objects.requireNonNull(encounter.getEncounterProviders().stream().findFirst().orElse(null))
						            .getProvider());
						CareSetting careSetting = Context.getOrderService()
						        .getCareSettingByUuid(BotswanaEmrConstants.CARE_SETTING_UUID);
						drugOrder.setCareSetting(careSetting);
						drugOrder.setNumRefills(0);
						
						if (drugAdministrationObs != null
						        && !drugAdministrationObs.getValueCoded().getUuid().equals(MISSED_CONCEPT_UUID)) {
							drugOrder.setFulfillerStatus(Order.FulfillerStatus.COMPLETED);
						}
						
						// Make the order inactive to prevent duplicate order exceptions
						drugOrder.setAutoExpireDate(new Date());
						
						OrderContext orderContext = new OrderContext();
						orderContext.setOrderType(orderType);
						
						try {
							orderService.saveOrder(drugOrder, orderContext);
						}
						catch (Exception e) {
							log.error("Failed to save drug order", e);
						}
						
					}
				}
				
			}
		}
	}
	
	private FixedDoseParameter getFixedDoseParameter(Integer age, Double weight, Concept tbTtreatmentPhaseConcept,
	        Concept treatmentGroup, Drug drug) {
		FixedDoseParameter fixedDoseParameter = new FixedDoseParameter();
		fixedDoseParameter.setDrug(drug);
		if (!treatmentGroup.getUuid().equals(DRUG_RESISTANT_TB_UUID)) {
			fixedDoseParameter.setRegimen(DRUG_SUSCEPTIBLE_TB_DEFAULT_REGIMEN);
			
			if (tbTtreatmentPhaseConcept.getUuid().equals(INITIAL_TREATMENT_PHASE_UUID)
			        && drug.getConcept().getUuid().equals(ETHAMBUTANOL_CONCEPT_ID)) {
				setQuantity(weight, fixedDoseParameter);
			} else {
				setQuantity(age, weight, fixedDoseParameter);
			}
			
		}
		
		return fixedDoseParameter;
	}
	
	private void setQuantity(Double weight, FixedDoseParameter fixedDoseParameter) {
		
		if (weight < 4) {
			fixedDoseParameter.setQuantity(0.0);
		}
		
		if (weight >= 4 && weight <= 7) {
			fixedDoseParameter.setQuantity(1.0);
		}
		
		if (weight >= 8 && weight <= 11) {
			fixedDoseParameter.setQuantity(2.0);
		}
		
		if (weight >= 12 && weight <= 15) {
			fixedDoseParameter.setQuantity(3.0);
		}
		
		if (weight >= 16 && weight <= 24) {
			fixedDoseParameter.setQuantity(4.0);
		}
	}
	
	private void setQuantity(Integer age, Double weight, FixedDoseParameter fixedDoseParameter) {
		if (age >= 14 || weight > 25) {
			if (weight >= 25 && weight <= 39) {
				fixedDoseParameter.setQuantity(2.0);
			}
			
			if (weight >= 40 && weight <= 54) {
				fixedDoseParameter.setQuantity(3.0);
			}
			
			if (weight >= 55 && weight <= 70) {
				fixedDoseParameter.setQuantity(4.0);
			}
			
			if (weight > 70) {
				fixedDoseParameter.setQuantity(5.0);
			}
		} else {
			if (weight < 4) {
				fixedDoseParameter.setQuantity(0.5);
			}
			
			if (weight >= 4 && weight <= 7) {
				fixedDoseParameter.setQuantity(1.0);
			}
			
			if (weight >= 8 && weight <= 11) {
				fixedDoseParameter.setQuantity(2.0);
			}
			
			if (weight >= 12 && weight <= 15) {
				fixedDoseParameter.setQuantity(3.0);
			}
			
			if (weight >= 16 && weight <= 24) {
				fixedDoseParameter.setQuantity(4.0);
			}
		}
		
	}
	
	private OrderFrequency calculateFrequency() {
		return null;
	}
	
	private Concept calculateRoute() {
		return null;
	}
	
	private Concept calculateQuantityUnits() {
		return null;
	}
	
	private Concept calculateDoseUnits() {
		return null;
	}
	
	private Double calculateQuantity() {
		return 1.0;
	}
	
	private Double calculateDose() {
		return 1.0;
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	private static class FixedDoseParameter {
		
		Double quantity;
		
		String regimen;
		
		Drug drug;
	}
}
