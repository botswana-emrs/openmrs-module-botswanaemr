/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.familyplanning;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_ENCOUNTER_TYPE_UUID;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.Model;
import org.springframework.web.bind.annotation.RequestParam;

public class MedicalHistoryPageController {
	
	public void controller(Model model, UiSessionContext sessionContext,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam("patientId") Patient patient) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/medical-surgical-history-form.xml";
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		boolean hasEncounter = false;
		
		EncounterType medicalSurgicalHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_MEDICAL_SURGICAL_HISTORY_ENCOUNTER_TYPE_UUID);
		if (encounter != null) {
			hasEncounter = true;
			model.addAttribute("encounter", encounter);
		} else {
			model.addAttribute("encounter", "");
		}
		model.addAttribute("hasEncounter", hasEncounter);
		
		model.addAttribute("definitionUiResource", definitionUiResource);
		model.addAttribute("formUuid", BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_FORM_UUID);
		model.addAttribute("modalTitle", BotswanaEmrConstants.SRH_MEDICAL_SURGICAL_HISTORY_FORM_MODAL_TITLE);
		List<Encounter> medicalHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    medicalSurgicalHistoryEncounterType, null, null, null);
		model.addAttribute("medicalHistoryEncounters", medicalHistoryEncounters);
	}
}
