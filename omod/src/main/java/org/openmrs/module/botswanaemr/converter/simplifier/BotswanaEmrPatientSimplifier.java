/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import org.openmrs.Patient;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BotswanaEmrPatientSimplifier extends BotswanaEmrAbstractSimplifier<Patient> {
	
	@Autowired
	private BotswanaEmrPersonSimplifier botswanaEmrPersonSimplifier;
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	@Override
	protected SimpleObject simplify(Patient patient) {
		SimpleObject ret = botswanaEmrPersonSimplifier.convert(patient);
		return ret;
	}
	
}
