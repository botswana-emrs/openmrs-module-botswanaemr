/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Order;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.Date;
import java.util.List;

public class DailyPharmacyGraphSummariesFragmentController {
	
	public void controller(FragmentModel model) {
		
		for (int i = 0; i < 24; i++) {
			String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
			model.addAttribute("t" + hourString, getFilledOrdersPerHour(new Date(), hourString));
		}
	}
	
	private Integer getFilledOrdersPerHour(Date date, String hour) {
		int value = 0;
		List<Order> hourlyFilledOrders = Context.getService(BotswanaEmrService.class)
		        .getPrescriptionsFilledOnDatePerHour(date, hour);
		if (hourlyFilledOrders != null && !hourlyFilledOrders.isEmpty()) {
			
			value = hourlyFilledOrders.size();
		}
		
		return value;
	}
}
