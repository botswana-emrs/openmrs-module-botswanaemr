/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.ui.framework.fragment.Fragment;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

public class NotificationSettingsFragmentController {
	
	protected final Log log = LogFactory.getLog(NotificationSettingsFragmentController.class);
	
	public void controller(FragmentModel model) {
		
	}
	
	public void saveNotificationSetting(
	        @RequestParam(value = "patientSelectedSwitch", required = false) String patientSelectedSwitch,
	        @RequestParam(value = "consultationEditsSwitch", required = false) String consultationEditsSwitch,
	        @RequestParam(value = "labResultsSwitch", required = false) String labResultsSwitch,
	        @RequestParam(value = "successfulAdmissionsSwitch", required = false) String successfulAdmissionsSwitch) {
		
		Map<String, String> propertiesMap = new HashMap<>();
		
		propertiesMap.put(USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY, successfulAdmissionsSwitch);
		
		try {
			Context.getUserService().saveUserProperties(propertiesMap);
		}
		catch (Exception e) {
			log.warn("Error occurred while saving user's details", e);
		}
	}
	
	public void notifyNewPatientsInPoolSetting(
	        @RequestParam(value = "notifyNewPatientsInPool", required = false) String notifyNewPatientsInPool) {
		updateNotificationSetting(USER_PROPERTY_NOTIFY_ON_NEW_PATIENT_IN_POOL_KEY, notifyNewPatientsInPool);
	}
	
	public void notifyPatientSelectedSetting(
	        @RequestParam(value = "notifyPatientSelected", required = false) String notifyPatientSelected) {
		updateNotificationSetting(USER_PROPERTY_NOTIFY_ON_CURRENT_PATIENT_SELECTION_KEY, notifyPatientSelected);
	}
	
	public void notifyConsultationEditsSetting(
	        @RequestParam(value = "notifyConsultationEdits", required = false) String notifyConsultationEdits) {
		updateNotificationSetting(USER_PROPERTY_NOTIFY_ON_CONSULTATION_EDITS_KEY, notifyConsultationEdits);
	}
	
	public void notifyLabResultsSetting(
	        @RequestParam(value = "notifyLabResults", required = false) String notifyLabResults) {
		updateNotificationSetting(USER_PROPERTY_NOTIFY_LAB_RESULTS_KEY, notifyLabResults);
	}
	
	public void notifySuccessfulAdmissionsSetting(
	        @RequestParam(value = "notifySuccessfulAdmissions", required = false) String notifySuccessfulAdmissions) {
		updateNotificationSetting(USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY, notifySuccessfulAdmissions);
	}
	
	public void updateNotificationSetting(String userProperty, String userPropertyValue) {
		
		try {
			Context.getUserService().saveUserProperty(userProperty, userPropertyValue);
			System.out.println("saving succeded");
		}
		catch (Exception e) {
			log.warn("Error occurred while updating user settings detail", e);
		}
	}
	
}
