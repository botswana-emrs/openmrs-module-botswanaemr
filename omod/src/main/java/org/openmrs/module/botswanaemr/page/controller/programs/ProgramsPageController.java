/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.programs;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLastDiscontinuedPatientProgram;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ProgramsPageController {
	
	public void controller(UiUtils ui, PageModel model, @RequestParam(value = "patientId") Patient patient,
	        @RequestParam(value = "currentServicePoint", required = false) String currentServicePoint,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "homeUrl", required = false) String homeUrl,
	        @RequestParam(value = "programId", required = false) String programId,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "ensureActiveVisit", required = false) Boolean ensureActiveVisit,
	        UiSessionContext sessionContext) {
		
		if (!StringUtils.isNotEmpty(currentServicePoint)) {
			model.addAttribute("pageLink", getPageLink(currentServicePoint));
		} else {
			//default to patient registration page
			model.addAttribute("pageLink", getPageLink(PATIENT_REGISTRATION_PORTAL_UUID));
		}
		
		String programPath = "";
		String programName = "";
		if (StringUtils.isEmpty(programId)) {
			programName = "TB Program";
			programPath = "programs/tbProgram";
		} else {
			Program currentProgram = BotswanaEmrUtils.getProgramByIdOrUuid(programId);
			programPath = "programs/" + currentProgram.getName();
			programName = currentProgram.getDescription();
		}
		
		model.addAttribute("programPath", programPath);
		model.addAttribute("currentServicePoint", currentServicePoint);
		model.addAttribute("homeUrl", homeUrl);
		model.addAttribute("patient", patient);
		model.addAttribute("ui", ui);
		model.addAttribute("programName", programName);
		//Encounter lastConsultationEncounter = getLastConsultationVisit(patient);
		//String visitId = lastConsultationEncounter == null ? null : lastConsultationEncounter.getVisit().getVisitId().toString();
		//model.addAttribute("visitId", visit != null ? visit.getId() : visitId);
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		
		Program tbProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		EncounterType labEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID);
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, tbProgram, null, null,
		    null, null, false);
		List<PatientProgram> allPatientPrograms = programWorkflowService.getPatientPrograms(patient, null, null, null, null,
		    null, false);
		List<PatientProgram> activeProgramEnrollmentForPatients = new ArrayList<PatientProgram>();
		boolean hasActiveVisit = false;
		
		Visit currentVisit = null;
		if (patient != null) {
			currentVisit = visit != null ? visit
			        : BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
			            ensureActiveVisit == null ? false : ensureActiveVisit);
		}
		
		hasActiveVisit = currentVisit != null;
		
		for (PatientProgram patientProgram : allPatientPrograms) {
			if (patientProgram.getDateCompleted() == null) {
				activeProgramEnrollmentForPatients.add(patientProgram);
			}
		}
		String givenName = "";
		String familyName = "";
		String middleName = "";
		if (patient != null && org.apache.commons.lang3.StringUtils.isNotBlank(patient.getGivenName())) {
			givenName = patient.getGivenName();
		}
		if (patient != null && org.apache.commons.lang3.StringUtils.isNotBlank(patient.getFamilyName())) {
			familyName = patient.getFamilyName();
		}
		if (patient != null && org.apache.commons.lang3.StringUtils.isNotBlank(patient.getMiddleName())) {
			middleName = patient.getMiddleName();
		}
		VisitService visitService = Context.getVisitService();
		
		model.addAttribute("patient", patient);
		model.addAttribute("programs", programWorkflowService.getAllPrograms());
		model.addAttribute("names", givenName + " " + familyName + " " + middleName);
		model.addAttribute("hasVisit", hasActiveVisit);
		model.addAttribute("visitTypes", visitService.getAllVisitTypes());
		model.addAttribute("currentVisit", currentVisit);
		model.addAttribute("activePrograms", activeProgramEnrollmentForPatients);
		if (programId != null && programId.equals(ART_PROGRAM_UUID)) {
			model.addAttribute("programOutComes", Programs.getArtTreatmentOutcomes());
		} else {
			model.addAttribute("programOutComes", Programs.getTreatmentOutcomes());
		}
		model.addAttribute("eligible",
		    Programs.eligibleForProgramEnrollment(patient,
		        BotswanaEmrUtils.getTbEncounterSearchCriteria(patient, labEncounterType),
		        getLastDiscontinuedPatientProgram(patient, tbProgram), tbProgram));
		
		SimpleObject returnParams = null;
		
		if (patient != null && currentVisit != null) {
			returnParams = SimpleObject.create("patientId", patient.getUuid(), "visitId", currentVisit.getId(), "programId",
			    tbProgram.getUuid());
		}
		String tbProfileUrl = ui.pageLink("botswanaemr", "programs/programs", returnParams);
		model.addAttribute("tbProfileUrl", tbProfileUrl);
		
		model.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "consultation/screeningAndTriage", patient, currentVisit, ui)));
	}
	
	private Encounter getLastConsultationVisit(Patient patient) {
		EncounterService encounterService = Context.getEncounterService();
		List<Encounter> consultationEncounters = BotswanaEmrUtils.getConsultationPatientEncounters(patient, null);
		//order those encounters
		Encounter lastConsultationEncounter = null;
		if (consultationEncounters.size() > 0) {
			lastConsultationEncounter = BotswanaEmrUtils.sortEncountersByEncounterDatetime(consultationEncounters).stream()
			        .findFirst().orElse(null);
		}
		
		return lastConsultationEncounter;
	}
	
	public String getPageLink(String currentServicePoint) {
		if (currentServicePoint != null && currentServicePoint.equals(PATIENT_REGISTRATION_PORTAL_UUID)) {
			return "registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(NURSING_PORTAL_UUID)) {
			return "consultation/auxilliaryNurseDashboard?appId=botswanaemr.auxilliaryNurseDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(DOCTORS_PORTAL_UUID)) {
			return "consultation/doctorDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(ART_SERVICES_PORTAL_UUID)) {
			return "art/artDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(TB_SERVICES_PORTAL_UUID)) {
			return "tbservices/tbServicesDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(BotswanaEmrConstants.PHARMACY_PORTAL_UUID)) {
			return "pharmacy/pharmacyAllPrescriptions";
		}
		if (currentServicePoint != null && currentServicePoint.equals(STOCK_MANAGEMENT_PORTAL_UUID)) {
			return "stock/stockManagementDashboard";
		}
		if (currentServicePoint != null && currentServicePoint.equals(VMMC_PORTAL_UUID)) {
			return "vmmc/vmmcDashboard";
		}
		return "/";
	}
	
}
