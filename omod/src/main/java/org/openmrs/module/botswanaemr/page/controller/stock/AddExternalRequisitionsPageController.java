/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

public class AddExternalRequisitionsPageController {
	
	public void controller(PageModel model, @RequestParam(value = "requestType", required = false) String requestType,
	        @SpringBean("locationService") LocationService locationService, UiSessionContext uiSessionContext) {
		
		List<Location> locations = new ArrayList<>();
		List<Stockroom> destinationStockrooms;
		List<Stockroom> sourceStockrooms = new ArrayList<>(
		        StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation()));
		
		if (requestType.equals("external")) {
			destinationStockrooms = sourceStockrooms;
		} else {
			destinationStockrooms = StockUtils.getNonBulkStockrooms(uiSessionContext.getSessionLocation());
		}
		
		locations = Context.getLocationService()
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		locations.remove(uiSessionContext.getSessionLocation());
		
		model.addAttribute("requestType", requestType);
		model.addAttribute("locations", locations);
		model.addAttribute("sourceStockrooms", sourceStockrooms);
		model.addAttribute("destinationStockrooms", destinationStockrooms);
	}
	
	public SimpleObject getMainStockRoom(@RequestParam(value = "facility") Location location) {
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("data", StockUtils.getMainStockroom(location));
		return simpleObject;
	}
	
}
