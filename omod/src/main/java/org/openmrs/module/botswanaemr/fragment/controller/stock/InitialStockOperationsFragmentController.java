/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class InitialStockOperationsFragmentController {
	
	public void controller(@SpringBean FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		
		List<Stockroom> stockrooms = new ArrayList<>();
		if (uiSessionContext.getSessionLocation() != null) {
			stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		}
		
		fragmentModel.addAttribute("stockrooms", stockrooms);
		
	}
	
	// This method is in search fragment
	// To be removed
	// Fetch Items without stocks (i.e. no entries in the stock rooms)
	public List<SimpleObject> getInitialItems(@RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "updateMissingBatches", required = false) Boolean updateMissingBatches,
	        @RequestParam(value = "search[value]", required = false) String searchPhrase) {
		
		page = page / pageSize + 1;
		// Pull all the products without stock information for the selected stock room
		List<Item> allItemsList;
		if (searchPhrase.trim().length() > 0) {
			allItemsList = BotswanaInventoryContext.getItemDataService().getByNameFragment(searchPhrase, false);
		} else {
			allItemsList = BotswanaInventoryContext.getItemDataService().getAll(false, new PagingInfo(page, pageSize));
		}
		
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomUuid);
		List<SimpleObject> itemList = null;
		if (stockRoom != null) {
			List<ItemStock> itemStockList = BotswanaInventoryContext.getStockRoomDataService().getItemsByRoom(stockRoom,
			    new PagingInfo(page, pageSize));
			
			if (!updateMissingBatches) {
				List<Item> itemsFromStockList = itemStockList.stream().map(ItemStock::getItem).collect(Collectors.toList());
				// Remove allItemsList already contained in the stockroom from the allItemsList list
				allItemsList.removeIf(itemsFromStockList::contains);
			}
			
			itemList = new ArrayList<>();
			for (Item item : allItemsList) {
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.put("code", item.getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
				simpleObject.put("name", item.getName());
				simpleObject.put("uuid", item.getUuid());
				itemList.add(simpleObject);
			}
		}
		
		return itemList;
	}
	
	public SimpleObject saveInitialStockItem(@RequestParam(value = "stockRoomId", required = true) Integer stockRoomId,
	        @RequestParam(value = "operationDate", required = true) Date operationDate,
	        @RequestParam(value = "itemId", required = true) String itemUuid,
	        @RequestParam(value = "batchInfo", required = true) String batchInfo) throws ParseException, JSONException {
		
		//System.out.println(stockRoomId + "" + operationDate + itemUuid + batchInfo);
		JSONObject jsonObject = new JSONObject(batchInfo);
		Iterator<String> keys = jsonObject.keys();
		List<String> operationStatus = new ArrayList<>();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				int qty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("qty"));
				Date expirationDate = new SimpleDateFormat("yyyy-MM-dd")
				        .parse(((JSONObject) jsonObject.get(key)).getString("expirationDate"));
				String batchNo = ((JSONObject) jsonObject.get(key)).getString("batchNo");
				StockOperation returnedStockOperation = StockUtils.getInitialStockStockOperation(qty, stockRoomId,
				    operationDate, expirationDate, batchNo, itemUuid);
				operationStatus.add(String.valueOf(returnedStockOperation.getStatus()));
			}
		}
		
		Calendar calender = Calendar.getInstance();
		
		// Date date = calender.setTime(new Date());
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		simpleObject.put("response", operationStatus);
		
		return simpleObject;
	}
}
