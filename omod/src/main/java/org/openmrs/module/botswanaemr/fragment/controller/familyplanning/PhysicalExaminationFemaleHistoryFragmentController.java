/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_PHYSICAL_EXAM_HISTORY_ENCOUNTER_TYPE_UUID;

public class PhysicalExaminationFemaleHistoryFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam("patientId") Patient patient) {
		fragmentModel.addAttribute("physicalExamFormUuid", BotswanaEmrConstants.SRH_PHYSICAL_EXAM_HISTORY_FORM_UUID);
		EncounterType physicalExamHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_PHYSICAL_EXAM_HISTORY_ENCOUNTER_TYPE_UUID);
		List<Encounter> physicalExamHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    physicalExamHistoryEncounterType, null, null, null);
		fragmentModel.addAttribute("physicalExamHistoryEncounters", physicalExamHistoryEncounters);
	}
}
