/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FACILITY_VISIT_VISIT_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_FORM_UUID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.RelationshipType;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.context.AppContextModel;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appframework.domain.Extension;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.fragment.controller.shared.Prescription;
import org.openmrs.module.botswanaemr.page.controller.PatientProfilePageController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.module.coreapps.contextmodel.PatientContextModel;
import org.openmrs.module.emrapi.patient.PatientDomainWrapper;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.InjectBeans;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ScreeningAndTriagePageController extends PatientProfilePageController {
	
	public void controller(PageModel model, @RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "dashboard") String dashboard,
	        @RequestParam(required = false, value = "app") AppDescriptor app,
	        @InjectBeans PatientDomainWrapper patientDomainWrapper,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("appFrameworkService") AppFrameworkService appFrameworkService,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, UiUtils ui, UiSessionContext sessionContext) {
		
		Boolean isConsultationActive = BotswanaEmrUtils.isConsultationActive(patient);
		
		// Status if consultation is active
		model.addAttribute("isConsultationActive", isConsultationActive);
		
		if (StringUtils.isEmpty(dashboard)) {
			dashboard = "patientDashboard";
		}
		
		patientDomainWrapper.setPatient(patient);
		model.addAttribute("patient", patientDomainWrapper);
		model.addAttribute("app", app);
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), true);
		
		if (visit == null) {
			visit = activeVisit;
		}
		model.addAttribute("activeVisit", visit);
		
		AppContextModel contextModel = sessionContext.generateAppContextModel();
		contextModel.put("patient", new PatientContextModel(patient));
		contextModel.put("patientId", patient.getUuid());
		contextModel.put("visit", visit);
		
		List<Extension> includeFragments = appFrameworkService.getExtensionsForCurrentUser(dashboard + ".includeFragments",
		    contextModel);
		Collections.sort(includeFragments);
		model.addAttribute("includeFragments", includeFragments);
		
		model.addAttribute("baseDashboardUrl", coreAppsProperties.getDashboardUrl());
		
		model.addAttribute("dashboard", dashboard);
		
		model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
		
		model.addAttribute("breadCrumbsFormatters",
		    Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
		
		model.addAttribute("appContextModel", contextModel);
		model.addAttribute("hasVisit", visit != null);
		model.addAttribute("activePatient", patient);
		model.addAttribute("educationLevels", BotswanaEmrUtils.getRequiredEducationLevels());
		List<Concept> employmentSectors = getEmploymentSectors();
		model.addAttribute("employmentSectors", employmentSectors);
		
		List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
		HashSet<String> hashSet = new HashSet<>();
		for (RelationshipType relationshipType : relationshipTypes) {
			hashSet.add(relationshipType.getaIsToB());
			hashSet.add(relationshipType.getbIsToA());
		}
		List<String> relationships = new ArrayList<>(hashSet);
		Collections.sort(relationships);
		model.addAttribute("relationships", relationships);
		
		Encounter encounter = BotswanaEmrUtils.getDrugOrderEncounter(patient, visit, sessionContext.getSessionLocation());
		
		model.addAttribute("encounter", encounter != null ? encounter.getUuid() : null);
		//Add prescription
		Prescription.addPrescription(model, patient);
		
		BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
		
		model.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		
		returnUrl = HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "consultation/screeningAndTriageCompleted",
		    patient, visit, ui);
		
		//call the variables here
		getHtmlFormToLoad(model, patient, sessionContext.getSessionLocation(), returnUrl, htmlFormEntryService, formService,
		    encounterService);
		
		model.addAttribute("tbScreeningFormUuid", TB_SCREENING_FORM_UUID);
		
		boolean hasTbScreeningEncounter = false;
		EncounterType tbScreeningEncounterType = encounterService.getEncounterTypeByUuid(TB_SCREENING_ENCOUNTER_TYPE_UUID);
		
		User user = Context.getAuthenticatedUser();
		model.addAttribute("hasVitalsPrivilege", user.hasPrivilege(BotswanaEmrConstants.PRIVILEGE_SCREENING_VITALS));
		model.addAttribute("hasMedicalDetailsPrivilege",
		    user.hasPrivilege(BotswanaEmrConstants.PRIVILEGE_SCREENING_MEDICAL_DETAILS));
		model.addAttribute("hasSymptomsPrivilege", user.hasPrivilege(BotswanaEmrConstants.PRIVILEGE_SCREENING_SYMPTOMS));
		
		if (visit != null) {
			Set<Encounter> encounters = visit.getEncounters();
			Comparator<Encounter> encounterDateTimeComparator = Comparator.comparing(Encounter::getEncounterDatetime);
			Optional<Encounter> tbScreeningEncounter = encounters.stream()
			        .filter(e -> e.getEncounterType().equals(tbScreeningEncounterType)).max(encounterDateTimeComparator);
			if (tbScreeningEncounter.isPresent()) {
				hasTbScreeningEncounter = true;
				model.addAttribute("tbScreeningEncounterId", tbScreeningEncounter.get().getEncounterId());
			} else {
				model.addAttribute("tbScreeningEncounterId", "");
			}
			model.addAttribute("hasTbScreeningEncounter", hasTbScreeningEncounter);
		}
		
		VisitType visitType = Context.getVisitService().getVisitTypeByUuid(FACILITY_VISIT_VISIT_TYPE_UUID);
		Visit lastPatientVisit = BotswanaEmrUtils.getLastPatientVisit(patient, visitType);
		Boolean tbPresumptive = BotswanaEmrUtils.isTbPresumptive(patient, lastPatientVisit);
		model.addAttribute("presumptive", tbPresumptive);
		
	}
	
	private void getHtmlFormToLoad(PageModel model, Patient patient, Location location, String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService,
	        @SpringBean("encounterService") EncounterService encounterService) {
		String formUuid = "07fada0b-8c38-44df-bf3f-f1d4120ed697";
		Form form = formService.getFormByUuid(formUuid);
		HtmlForm htmlForm = null;
		if (form != null) {
			htmlForm = htmlFormEntryService.getHtmlFormByForm(form);
		}
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, location, false);
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("formPatient", patient);
		model.addAttribute("htmlForm", htmlForm);
		model.addAttribute("screeningAndTriageEncounter",
		    getScreeningAndTriageEncounter(currentPatientVisit, encounterService));
	}
	
	private Encounter getScreeningAndTriageEncounter(Visit visit,
	        @SpringBean("encounterService") EncounterService encounterService) {
		List<Encounter> encounterList = encounterService.getEncountersByVisit(visit, false);
		List<Encounter> list = new ArrayList<>();
		for (Encounter e : encounterList) {
			if (e.getEncounterType().getUuid().equals(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID)) {
				list.add(e);
			}
		}
		
		encounterList = list;
		
		if (encounterList.size() > 0) {
			return encounterList.get(0);
		}
		return null;
	}
}
