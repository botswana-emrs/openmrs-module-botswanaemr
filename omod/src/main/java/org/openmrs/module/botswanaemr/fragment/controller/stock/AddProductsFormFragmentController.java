/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.api.ConceptService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IDepartmentDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.model.Department;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

public class AddProductsFormFragmentController {
	
	protected final Log log = LogFactory.getLog(AddProductsFormFragmentController.class);
	
	private final IItemDataService itemDataService = BotswanaInventoryContext.getItemDataService();
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	private final IDepartmentDataService departmentDataService = BotswanaInventoryContext.getDepartmentDataService();
	
	private final IItemAttributeDataService itemAttributeDataService = BotswanaInventoryContext
	        .getItemAttributeDataService();
	
	private final IItemAttributeTypeDataService itemAttributeTypeDataService = BotswanaInventoryContext
	        .getItemAttributeTypeDataService();
	
	public void controller(FragmentModel fragmentModel) {
		
		ObjectMapper jsonMapper = new ObjectMapper();
		List<Concept> concepts = conceptService.getAllConcepts();
		List<Department> departments = departmentDataService.getAll();
		List<Concept> unitsOfIssue = Context.getOrderService().getDrugDispensingUnits();
		;
		
		List<String> conceptNames = new ArrayList<>();
		for (Concept concept : concepts) {
			conceptNames.add(concept.getName().getName());
		}
		
		String json = "";
		try {
			json = jsonMapper.writeValueAsString(conceptNames);
		}
		catch (Exception e) {
			log.info(e.getMessage());
		}
		fragmentModel.addAttribute("conceptNames", json);
		fragmentModel.addAttribute("departments", departments);
		fragmentModel.addAttribute("unitsOfIssue", unitsOfIssue);
		Set<String> categories = new HashSet<>(Arrays.asList("DRUG", "MEM", "NON DRUG"));
		fragmentModel.addAttribute("categories", categories);
	}
	
	public void saveCreatedProducts(@RequestParam("name") String name, @RequestParam("code") String code,
	        @RequestParam(value = "conceptName", required = false) String conceptName,
	        @RequestParam("description") String description, @RequestParam("unitOfIssue") String unitOfIssue,
	        @RequestParam("departmentId") Integer departmentId, @RequestParam("category") String category,
	        @RequestParam("status") String status,
	        @RequestParam(value = "minimumQuantity", required = false) Integer minimumQuantity,
	        @RequestParam(value = "maximumQuantity", required = false) Integer maximumQuantity,
	        @RequestParam(value = "emergencyOrderPoint", required = false) Integer emergencyOrderPoint,
	        @RequestParam(value = "drugUuid", required = false) String drugUuid) throws ParseException {
		
		Item inventoryItem = new Item();
		
		Concept itemConcept = conceptService.getConceptByName(conceptName);
		
		if (Objects.equals(category, "DRUG")) {
			if (drugUuid != null) {
				itemConcept = Context.getConceptService().getDrugByUuid(drugUuid).getConcept();
			}
		} else {
			itemConcept = Context.getConceptService().getConceptByName(conceptName);
		}
		
		if (codeAlreadyAssigned(code, null)) {
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
			        String.format("Code %s is already being used", code));
		}
		
		Department itemDepartment = departmentDataService.getById(departmentId);
		
		Set<ItemCode> itemCodes = new HashSet<>();
		ItemCode itemCode = new ItemCode();
		itemCode.setName(code);
		itemCode.setItem(inventoryItem);
		itemCode.setCode(code);
		itemCodes.add(itemCode);
		
		ItemAttributeType statusAttributeType = itemAttributeTypeDataService.getByUuid(INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType categoryAttributeType = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType unitOfIssueAttributeType = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType drugUuidAttributeType = itemAttributeTypeDataService
		        .getByUuid(BotswanaEmrConstants.INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType maximumQuantityAttributeType = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_MAX_STOCK_LEVEL_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType emergencyOrderPointAttributeType = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_EMERGENCY_ORDER_POINT_ATTRIBUTE_TYPE_UUID);
		
		ItemAttribute statusItemAttribute = new ItemAttribute();
		statusItemAttribute.setValue(status);
		statusItemAttribute.setAttributeType(statusAttributeType);
		statusItemAttribute.setOwner(inventoryItem);
		
		ItemAttribute categoryItemAttribute = new ItemAttribute();
		categoryItemAttribute.setValue(category);
		categoryItemAttribute.setAttributeType(categoryAttributeType);
		categoryItemAttribute.setOwner(inventoryItem);
		
		ItemAttribute unitOfIssueItemAttribute = new ItemAttribute();
		unitOfIssueItemAttribute.setValue(unitOfIssue);
		unitOfIssueItemAttribute.setAttributeType(unitOfIssueAttributeType);
		unitOfIssueItemAttribute.setOwner(inventoryItem);
		
		ItemAttribute drugUuidItemAttribute = new ItemAttribute();
		drugUuidItemAttribute.setValue(drugUuid);
		drugUuidItemAttribute.setAttributeType(drugUuidAttributeType);
		drugUuidItemAttribute.setOwner(inventoryItem);
		
		ItemAttribute maximumQuantityItemAttribute = null;
		if (maximumQuantity != null) {
			maximumQuantityItemAttribute = new ItemAttribute();
			maximumQuantityItemAttribute.setValue(String.valueOf(maximumQuantity));
			maximumQuantityItemAttribute.setAttributeType(maximumQuantityAttributeType);
			maximumQuantityItemAttribute.setOwner(inventoryItem);
			inventoryItem.addAttribute(maximumQuantityItemAttribute);
		}
		
		ItemAttribute emergencyOrderPointItemAttribute = null;
		if (emergencyOrderPoint != null) {
			emergencyOrderPointItemAttribute = new ItemAttribute();
			emergencyOrderPointItemAttribute.setValue(String.valueOf(emergencyOrderPoint));
			emergencyOrderPointItemAttribute.setAttributeType(emergencyOrderPointAttributeType);
			emergencyOrderPointItemAttribute.setOwner(inventoryItem);
			inventoryItem.addAttribute(emergencyOrderPointItemAttribute);
			
		}
		
		inventoryItem.setName(name);
		inventoryItem.setCodes(itemCodes);
		inventoryItem.setConcept(itemConcept);
		inventoryItem.setDescription(description);
		inventoryItem.setDefaultExpirationPeriod(0);
		inventoryItem.setDateCreated(new Date());
		inventoryItem.setCreator(Context.getAuthenticatedUser());
		inventoryItem.setDepartment(itemDepartment);
		inventoryItem.addAttribute(statusItemAttribute);
		inventoryItem.addAttribute(categoryItemAttribute);
		inventoryItem.addAttribute(unitOfIssueItemAttribute);
		inventoryItem.addAttribute(drugUuidItemAttribute);
		inventoryItem.setMinimumQuantity(minimumQuantity == null ? 0 : minimumQuantity);
		
		inventoryItem.setHasExpiration(true);
		
		try {
			itemDataService.save(inventoryItem);
			log.info("\nSaved new product\n" + inventoryItem);
		}
		catch (Exception e) {
			log.warn("\nAn error occurred while saving product\n", e);
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
			        "An error occurred while saving product: " + e.getMessage());
		}
	}
	
	public void saveEditedProducts(@RequestParam(value = "itemId", required = false) Integer itemId,
	        @RequestParam("name") String name, @RequestParam("code") String code,
	        @RequestParam(value = "conceptName", required = false) String conceptName,
	        @RequestParam("description") String description, @RequestParam("unitOfIssue") String unitOfIssue,
	        @RequestParam("departmentId") Integer departmentId, @RequestParam("category") String category,
	        @RequestParam("status") String status,
	        @RequestParam(value = "minimumQuantity", required = false) Integer minimumQuantity,
	        @RequestParam(value = "maximumQuantity", required = false) Integer maximumQuantity,
	        @RequestParam(value = "emergencyOrderPoint", required = false) Integer emergencyOrderPoint,
	        @RequestParam(value = "drugUuid", required = false) String drugUuid) throws ParseException {
		
		Item inventoryItem;
		
		//The concept auto-complete input forces an auth-user 
		//To select from the options given, preventing NPE(s)
		Concept itemConcept = conceptService.getConceptByName(conceptName);
		
		if (Objects.equals(category, "DRUG")) {
			if (drugUuid != null) {
				itemConcept = Context.getConceptService().getDrugByUuid(drugUuid).getConcept();
			}
		}
		
		if (itemId != null) {
			inventoryItem = itemDataService.getById(itemId);
			Department itemDepartment = departmentDataService.getById(departmentId);
			
			if (codeAlreadyAssigned(code, inventoryItem)) {
				throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
				        String.format("Code %s is already being used", code));
			}
			
			Set<ItemCode> itemCodes = inventoryItem.getCodes();
			inventoryItem.getCodes().clear();
			ItemCode itemCode = new ItemCode();
			itemCode.setName(code);
			itemCode.setItem(inventoryItem);
			itemCode.setCode(code);
			itemCodes.add(itemCode);
			
			Set<ItemAttribute> itemAttributes = inventoryItem.getAttributes();
			ItemAttributeType statusAttributeType = itemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID);
			ItemAttributeType categoryAttributeType = itemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID);
			ItemAttributeType unitOfIssueAttributeType = itemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID);
			ItemAttributeType drugUuidAttributeType = itemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID);
			ItemAttributeType maximumQuantityAttributeType = itemAttributeTypeDataService
			        .getByUuid(INV_ITEM_MAX_STOCK_LEVEL_ATTRIBUTE_TYPE_UUID);
			ItemAttributeType emergencyOrderPointAttributeType = itemAttributeTypeDataService
			        .getByUuid(INV_ITEM_EMERGENCY_ORDER_POINT_ATTRIBUTE_TYPE_UUID);
			
			ItemAttribute statusItemAttribute = getItemAttribute(itemAttributes, statusAttributeType);
			inventoryItem.getAttributes().remove(statusItemAttribute);
			if (statusItemAttribute == null) {
				statusItemAttribute = new ItemAttribute();
			}
			statusItemAttribute.setValue(status);
			statusItemAttribute.setAttributeType(statusAttributeType);
			statusItemAttribute.setOwner(inventoryItem);
			inventoryItem.getAttributes().add(statusItemAttribute);
			
			ItemAttribute categoryItemAttribute = getItemAttribute(itemAttributes, categoryAttributeType);
			inventoryItem.getAttributes().remove(categoryItemAttribute);
			if (categoryItemAttribute == null) {
				categoryItemAttribute = new ItemAttribute();
			}
			categoryItemAttribute.setValue(category);
			categoryItemAttribute.setAttributeType(categoryAttributeType);
			categoryItemAttribute.setOwner(inventoryItem);
			inventoryItem.getAttributes().add(categoryItemAttribute);
			
			ItemAttribute unitOfIssueItemAttribute = getItemAttribute(itemAttributes, unitOfIssueAttributeType);
			inventoryItem.getAttributes().remove(unitOfIssueItemAttribute);
			if (unitOfIssueItemAttribute == null) {
				unitOfIssueItemAttribute = new ItemAttribute();
			}
			unitOfIssueItemAttribute.setValue(unitOfIssue);
			unitOfIssueItemAttribute.setAttributeType(unitOfIssueAttributeType);
			unitOfIssueItemAttribute.setOwner(inventoryItem);
			inventoryItem.getAttributes().add(unitOfIssueItemAttribute);
			
			ItemAttribute drugUuidItemAttribute = getItemAttribute(itemAttributes, drugUuidAttributeType);
			inventoryItem.getAttributes().remove(drugUuidItemAttribute);
			if (drugUuidItemAttribute == null) {
				drugUuidItemAttribute = new ItemAttribute();
			}
			drugUuidItemAttribute.setValue(drugUuid);
			drugUuidItemAttribute.setAttributeType(drugUuidAttributeType);
			drugUuidItemAttribute.setOwner(inventoryItem);
			inventoryItem.getAttributes().add(drugUuidItemAttribute);
			
			if (maximumQuantity != null) {
				ItemAttribute maximumQuantityItemAttribute = getItemAttribute(itemAttributes, maximumQuantityAttributeType);
				inventoryItem.getAttributes().remove(maximumQuantityItemAttribute);
				if (maximumQuantityItemAttribute == null) {
					maximumQuantityItemAttribute = new ItemAttribute();
				}
				maximumQuantityItemAttribute.setValue(String.valueOf(maximumQuantity));
				maximumQuantityItemAttribute.setAttributeType(maximumQuantityAttributeType);
				maximumQuantityItemAttribute.setOwner(inventoryItem);
				inventoryItem.getAttributes().add(maximumQuantityItemAttribute);
			}
			
			if (emergencyOrderPoint != null) {
				ItemAttribute emergencyOrderPointItemAttribute = getItemAttribute(itemAttributes,
				    emergencyOrderPointAttributeType);
				inventoryItem.getAttributes().remove(emergencyOrderPointItemAttribute);
				if (emergencyOrderPointItemAttribute == null) {
					emergencyOrderPointItemAttribute = new ItemAttribute();
				}
				emergencyOrderPointItemAttribute.setValue(String.valueOf(emergencyOrderPoint));
				emergencyOrderPointItemAttribute.setAttributeType(emergencyOrderPointAttributeType);
				emergencyOrderPointItemAttribute.setOwner(inventoryItem);
				inventoryItem.getAttributes().add(emergencyOrderPointItemAttribute);
			}
			
			inventoryItem.setName(name);
			inventoryItem.setCodes(itemCodes);
			inventoryItem.setConcept(itemConcept);
			inventoryItem.setDescription(description);
			inventoryItem.setDefaultExpirationPeriod(0);
			inventoryItem.setDepartment(itemDepartment);
			inventoryItem.setMinimumQuantity(minimumQuantity == null ? 0 : minimumQuantity);
			
			try {
				itemDataService.save(inventoryItem);
				log.info("\nSaved product\n" + inventoryItem);
			}
			catch (Exception e) {
				log.warn("\nAn error occurred while saving product\n", e);
				throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
				        "An error occurred while saving product: " + e.getMessage());
			}
		}
	}
	
	private boolean codeAlreadyAssigned(String code, Item item) {
		Boolean codeIsAssigned = false;
		if (item == null) {
			codeIsAssigned = BotswanaInventoryContext.getItemDataService().getItemByCode(code) != null;
		} else {
			Item itemByCode = BotswanaInventoryContext.getItemDataService().getItemByCode(code);
			if (itemByCode == null) {
				codeIsAssigned = false;
			} else {
				codeIsAssigned = item != itemByCode;
			}
		}
		
		return codeIsAssigned;
	}
	
	private ItemAttribute getItemAttribute(Set<ItemAttribute> itemAttributes, ItemAttributeType itemAttributeType) {
		for (ItemAttribute itemAttribute : itemAttributes) {
			if (Objects.equals(itemAttribute.getAttributeType(), itemAttributeType)) {
				return itemAttribute;
			}
		}
		return null;
	}
	
	private ItemCode getItemCode(Set<ItemCode> itemCodes, String codeName) {
		for (ItemCode itemCode : itemCodes) {
			if (Objects.equals(itemCode.getName(), codeName)) {
				return itemCode;
			}
		}
		return null;
	}
	
	public List<SimpleObject> getDrugs(@RequestParam(value = "q") String searchString) {
		List<Drug> drugList = Context.getConceptService().getDrugs(searchString);
		
		List<SimpleObject> simpleObjects = new ArrayList<>();
		for (Drug drug : drugList) {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("uuid", drug.getUuid());
			simpleObject.put("name", drug.getName());
			simpleObjects.add(simpleObject);
		}
		
		return simpleObjects;
		
	}
}
