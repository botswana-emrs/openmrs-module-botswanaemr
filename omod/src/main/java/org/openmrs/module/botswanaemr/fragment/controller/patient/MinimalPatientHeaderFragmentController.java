/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.patient;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.ui.framework.page.Redirect;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MinimalPatientHeaderFragmentController {
	
	public Object controller(UiUtils ui, @RequestParam("patientId") Patient patient, final PageModel model,
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "returnUrl", required = false) String returnUrl,
			@RequestParam(value = "visitId", required = false) Visit visit,
			@RequestParam(required = false, value = "app") AppDescriptor app,
			@RequestParam(required = false, value = "dashboard") String dashboard,
			@FragmentParam(value = "showHivStatus", required = false) Boolean showHivStatus,
			@SpringBean("visitService") VisitService visitService,
			@SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties,
			@SpringBean("labService") LabService labService,
			@RequestParam(required = false, value = "programId") Program program, UiSessionContext sessionContext) {
		
		// if session is not active, redirect to login page
		if (sessionContext.getCurrentUser() == null) {
			try {
				response.sendRedirect(request.getContextPath() + "/login.htm");
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
		
		try {
			// Check user privileges and patient status
			if (!Context.hasPrivilege(CoreAppsConstants.PRIVILEGE_PATIENT_DASHBOARD)) {
				return new Redirect("coreapps", "noAccess", "");
			} else if (patient.isVoided() || patient.isPersonVoided()) {
				return new Redirect("coreapps", "patientdashboard/deletedPatient", "patientId=" + patient.getId());
			}
			
			// Set default dashboard if not provided
			if (StringUtils.isEmpty(dashboard)) {
				dashboard = "patientDashboard";
			}

			Obs hivStatusObs = BotswanaEmrUtils.getPatientHivStatusObs(patient);
			model.addAttribute("dateTested", hivStatusObs != null ? hivStatusObs.getObsDatetime() : null);

			// Add HIV status to model if required
			if (Boolean.TRUE.equals(showHivStatus)) {
				Concept hivStatus = hivStatusObs != null ? hivStatusObs.getValueCoded() : null;
				model.addAttribute("showHivStatus",
						hivStatus != null
							? hivStatus.getDisplayString()
							: "UNKNOWN");
			} else {
				model.addAttribute("showHivStatus", "");
			}
			
			// Add various attributes to the model
			model.addAttribute("patient", patient);
			model.addAttribute("activePatientVisit", visit != null ? visit
					: BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false));
			model.addAttribute("baseDashboardUrl", coreAppsProperties.getDashboardUrl());
			model.addAttribute("dashboard", dashboard);
			model.addAttribute("returnUrl", returnUrl);
			model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
			model.addAttribute("breadCrumbsFormatters",
				Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
			model.addAttribute("sessionLocation", sessionContext.getSessionLocation().getName());
			BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
			model.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
			
			// Limit the number of visits to 5
			List<Visit> patientVisits = new ArrayList<>(visitService.getVisitsByPatient(patient));
			List<Visit> requiredVisit = patientVisits.size() > 5 ? patientVisits.stream().limit(5).collect(Collectors.toList()) : patientVisits;
			model.addAttribute("patientVisits", requiredVisit);
			model.addAttribute("program", program);
			
			return null;
			
		} catch (NullPointerException ex) {
			return new Redirect("coreapps", "patientdashboard/patientNotFound", "patientId=" + "Not Found");
		}
	}
	
	public List<PersonAttribute> getBreadCrumbsDetails(Patient patient) {
		String breadCrumbsDetailsAttrTypeUuids = Context.getAdministrationService()
				.getGlobalProperty("breadCrumbs.details.personAttr.uuids");
		List<PersonAttribute> personNamePersonAttrs = new ArrayList<>();
		if (StringUtils.isNotBlank(breadCrumbsDetailsAttrTypeUuids)) {
			for (String breadCrumbDetailAttrTypeUuid : breadCrumbsDetailsAttrTypeUuids.split(",")) {
				PersonAttributeType pat = Context.getPersonService()
						.getPersonAttributeTypeByUuid(breadCrumbDetailAttrTypeUuid);
				PersonAttribute pa = patient.getAttribute(pat);
				if (pa != null) {
					personNamePersonAttrs.add(pa);
				}
			}
		}
		return CollectionUtils.isNotEmpty(personNamePersonAttrs) ? personNamePersonAttrs : null;
	}
}
