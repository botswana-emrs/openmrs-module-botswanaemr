/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterProvider;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.TbTreatmentService;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class DrugTrackingCalendarFragmentController {
	
	public void controller(@SpringBean FragmentModel model, UiUtils ui, @RequestParam("patientId") Patient patient,
	        @SpringBean("obsService") ObsService obsService) {
		Date enrollmentDate = null;
		Date completionDate = null;
		Optional<PatientProgram> patientProgram = Context
		        .getRegisteredComponent("botswana.emr.tbTreatmentService", TbTreatmentService.class)
		        .getTBEnrollment(patient);
		if (patientProgram.isPresent()) {
			enrollmentDate = patientProgram.get().getDateEnrolled();
			completionDate = patientProgram.get().getDateCompleted();
		}
		
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, enrollmentDate,
		        completionDate, null, null, Arrays.asList(getDOTEncounterType()), null, null, null, false);
		
		// Get all encounters for the patient that are of the DOT encounter type
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		List<Obs> dotObservations = obsService.getObservations(Collections.singletonList(patient), encounters,
		    getDOTQuestions(), null, null, null, null, null, null, enrollmentDate, completionDate, false);
		
		Map<Encounter, List<Obs>> observationsByEncounter = groupByEncounter(dotObservations);
		
		List<Event> events = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : observationsByEncounter.entrySet()) {
			Encounter encounter = entry.getKey();
			List<Obs> observations = entry.getValue();
			
			List<Obs> administeredByObs = observations.stream().filter(obs -> Objects.requireNonNull(obs.getConcept())
			        .getUuid().equals(BotswanaEmrConstants.ADMINISTERED_CONCEPT_UUID)).collect(Collectors.toList());
			List<Obs> dateAdministeredObs = observations.stream().filter(obs -> Objects.requireNonNull(obs.getConcept())
			        .getUuid().equals(BotswanaEmrConstants.DATE_ADMINISTERED_CONCEPT_UUID)).collect(Collectors.toList());
			
			if (dateAdministeredObs.size() >= 1) {
				Obs dateAdministeredObservation = dateAdministeredObs.get(0);
				Date dateAdministered = dateAdministeredObservation.getValueDatetime();
				
				if (dateAdministered != null) {
					String dateAdministeredString = DateUtils.getStringDate(dateAdministered, "yyyy-MM-dd");
					events.add(new Event(getEncounterProviderInitials(encounter), dateAdministeredString));
				}
			}
		}
		
		model.addAttribute("events", events);
		model.addAttribute("today", DateUtils.getStringDate(new Date(), "yyyy-MM-dd"));
	}
	
	/**
	 * Gets DOT encounter type
	 *
	 * @return DOT encounter type
	 */
	protected EncounterType getDOTEncounterType() {
		return Context.getEncounterService().getEncounterTypeByUuid(BotswanaEmrConstants.DOT_ENCOUNTER_TYPE_UUID);
	}
	
	protected static List<Concept> getDOTQuestions() {
		return new ArrayList<>(
		        Arrays.asList(Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.ADMINISTERED_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.DATE_ADMINISTERED_CONCEPT_UUID)));
	}
	
	protected Map<Encounter, List<Obs>> groupByEncounter(List<Obs> observations) {
		return observations.stream().collect(Collectors.groupingBy(Obs::getEncounter));
	}
	
	private String getEncounterProviderInitials(@NotNull Encounter encounter) {
		//Find a better way
		List<EncounterProvider> encounterProviderSet = new ArrayList<EncounterProvider>(encounter.getEncounterProviders());
		String fullName = "";
		if (!encounterProviderSet.isEmpty()) {
			fullName = encounterProviderSet.get(0).getProvider().getPerson().getPersonName().getFullName();
		}
		//NameUtils.getInitials(fullName)
		return fullName.isEmpty() ? "X" : fullName;
	}
	
	@Data
	@AllArgsConstructor
	public static class Event implements Serializable {
		
		private String title;
		
		private String start;
	}
}
