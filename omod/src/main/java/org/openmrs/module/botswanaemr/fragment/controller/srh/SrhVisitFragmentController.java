/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.srh;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.VisitAttribute;
import org.openmrs.VisitAttributeType;
import org.openmrs.VisitType;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.Objects;

public class SrhVisitFragmentController {
	
	public final String PATIENT_TYPE_ON_VISIT_ATTRIBUTE_UUID = "2eed3db4-d910-4316-8498-e7e1bde4b119";
	
	private final VisitService visitService = Context.getVisitService();
	
	public void controller(@RequestParam("patientId") Patient patient, @FragmentParam("visitId") Visit visit,
	        FragmentModel fragmentModel, UiUtils ui) {
		fragmentModel.addAttribute("visit", visit);
		fragmentModel.addAttribute("patientId", patient.getPatientId());
		fragmentModel.addAttribute("returnUrl", ui.pageLink("botswanaemr",
		    "srh/obstetricProfile.page?patientId=" + patient.getPatientId() + "&visitId=" + visit.getVisitId()));
	}
	
	public void saveVisitDetails(@RequestParam("visitId") Visit visit, @RequestParam("visitDate") String visitDate,
	        @RequestParam("serviceType") String serviceType, @RequestParam("patientType") String patientType)
	        throws ParseException {
		VisitType patientVisitType = null;
		if (StringUtils.isNotBlank(serviceType)) {
			patientVisitType = visitService.getVisitTypeByUuid(serviceType);
		}
		if (StringUtils.isNotBlank(patientType)) {
			VisitAttributeType visitAttributeType = visitService
			        .getVisitAttributeTypeByUuid(PATIENT_TYPE_ON_VISIT_ATTRIBUTE_UUID);
			if (patientVisitType != null && visitAttributeType != null) {
				VisitAttribute visitAttribute = new VisitAttribute();
				visitAttribute.setAttributeType(visitAttributeType);
				visitAttribute.setValue(patientType);
				Objects.requireNonNull(visit).setVisitType(patientVisitType);
				Objects.requireNonNull(visit).setAttribute(visitAttribute);
				Objects.requireNonNull(visit).setStartDatetime(BotswanaEmrUtils.formatDateFromStringWithTime(visitDate));
				visitService.saveVisit(visit);
			}
		}
	}
}
