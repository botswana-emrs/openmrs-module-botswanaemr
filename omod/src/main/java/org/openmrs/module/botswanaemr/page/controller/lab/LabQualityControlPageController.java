/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.lab;

import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedLabQualityControl;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LabQualityControlPageController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		
		HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
		
		Set<SimplifiedLabQualityControl> simplifiedLabQualityControlList = new HashSet<>();
		
		if (sessionContext.getCurrentUser() != null) {
			
			List<LabQualityControl> labQualityControlList = htsLabControlService.getList(sessionContext.getSessionLocation());
			
			for (LabQualityControl labQualityControl : labQualityControlList) {
				SimplifiedLabQualityControl simplifiedLabQualityControl = new SimplifiedLabQualityControl();
				simplifiedLabQualityControl.setId(labQualityControl.getId());
				simplifiedLabQualityControl
				        .setDate(BotswanaEmrUtils.formatDateWithoutTime(labQualityControl.getDateCreated(), "dd-MM-yyyy"));
				simplifiedLabQualityControl.setOperator("");
				if (labQualityControl.getOperator() != null && labQualityControl.getOperator().getPersonName() != null) {
					simplifiedLabQualityControl.setOperator(labQualityControl.getOperator().getPersonName().getFullName());
				}
				simplifiedLabQualityControl.setPublished(labQualityControl.getPublished());
				simplifiedLabQualityControl.setTestingPoint(labQualityControl.getTestingPoint());
				simplifiedLabQualityControlList.add(simplifiedLabQualityControl);
			}
			
		}
		
		model.addAttribute("simplifiedLabQualityControlList", simplifiedLabQualityControlList);
		
	}
	
}
