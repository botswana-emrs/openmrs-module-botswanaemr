/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDurationSince;

public class DoctorsPatientPoolDashboardPageController extends QueuePatientFragmentController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date yesterday = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);
		
		Location sessionLocation = sessionContext.getSessionLocation();
		
		//Check for possibility of null pointer to some results
		int allConsultationsCount = 0;
		int todayConsultationsCount = 0;
		int yesterdayConsultationsCount = 0;
		int dailyAverageConsultations = 0;
		
		List<GeneralPatientObject> todaysDoctorConsultationList = new ArrayList<>();
		
		// Disable this and TODO: use asynchronous approach to fetch data
		//List<Encounter> allConsultationsList = BotswanaEmrUtils.getAllConsultations(null, null, null, sessionLocation);
		//if (allConsultationsList != null && allConsultationsList.size() > 0) {
		//	allConsultationsCount = allConsultationsList.size();
		//}
		
		// List<Encounter> todayConsultationsList = BotswanaEmrUtils.getAllConsultations(today, today, null, sessionLocation);
		// if (todayConsultationsList != null && todayConsultationsList.size() > 0) {
		// 	todayConsultationsCount = todayConsultationsList.size();
		// }
		
		// List<Encounter> yesterdayConsultationsList = BotswanaEmrUtils.getAllConsultations(yesterday, yesterday, null,
		//     sessionLocation);
		// if (yesterdayConsultationsList != null && yesterdayConsultationsList.size() > 0) {
		// 	yesterdayConsultationsCount = yesterdayConsultationsList.size();
		// }
		
		// dailyAverageConsultations = getDailyAverageConsultations(sessionLocation);
		
		// todaysDoctorConsultationList = patientObjects(BotswanaEmrUtils.getAllConsultations(today, null, limitFetch, sessionLocation, pagingInfo))
		// todaysDoctorConsultationList = patientObjects(todayConsultationsList)
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		model.addAttribute("allConsultationsCount", allConsultationsCount);
		model.addAttribute("todayConsultationsCount", todayConsultationsCount);
		model.addAttribute("yesterdayConsultationsCount", yesterdayConsultationsCount);
		model.addAttribute("dailyAverageConsultations", dailyAverageConsultations);
		PagingInfo pagingInfo = new PagingInfo();
		pagingInfo.setPageSize(limitFetch);
		pagingInfo.setPage(1);
		model.addAttribute("todayDoctorsConsultationActivitiesList", todaysDoctorConsultationList);
		
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(BotswanaEmrUtils
				        .formatPerson(new ArrayList<>(encounter.getEncounterProviders()).get(0).getProvider().getPerson()));
				generalPatientObject.setDuration(getDurationSince(encounter.getDateCreated()));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
	
	private Integer getDailyAverageConsultations(Location location) {
		
		int totalConsultationsEver = 0;
		Encounter firstPatientEncounter = null;
		Encounter lastEncounter = null;
		int daysBetweenFirstAndLastEncounterDates = 0;
		
		List<Encounter> allConsultations = BotswanaEmrUtils.getAllConsultations(null, null, null, location);
		if (allConsultations != null && allConsultations.size() > 0) {
			totalConsultationsEver = allConsultations.size();
			firstPatientEncounter = allConsultations.get(0);
			lastEncounter = allConsultations.get(allConsultations.size() - 1);
		}
		
		if (firstPatientEncounter != null && lastEncounter != null) {
			daysBetweenFirstAndLastEncounterDates = BotswanaEmrUtils.unitsSince(lastEncounter.getEncounterDatetime(),
			    firstPatientEncounter.getEncounterDatetime(), "days");
		}
		
		if (daysBetweenFirstAndLastEncounterDates == 0) {
			daysBetweenFirstAndLastEncounterDates = 1;
		}
		
		return totalConsultationsEver / daysBetweenFirstAndLastEncounterDates;
	}
}
