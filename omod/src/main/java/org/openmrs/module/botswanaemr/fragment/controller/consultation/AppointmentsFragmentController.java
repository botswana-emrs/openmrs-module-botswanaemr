/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Provider;
import org.openmrs.User;
import org.openmrs.api.ProviderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedAppointment;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AppointmentsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("appointmentService") AppointmentService appointmentService,
	        @SpringBean("providerService") ProviderService providerService,
	        @FragmentParam(value = "allAppointments", required = false) String allAppointments,
	        UiSessionContext sessionContext) throws ParseException {
		
		User authenticatedUser = Context.getAuthenticatedUser();
		if (authenticatedUser != null) {
			List<Provider> providerList = new ArrayList<>(
			        providerService.getProvidersByPerson(authenticatedUser.getPerson()));
			Provider provider = null;
			boolean loginInUserIsProvider = false;
			if (providerList.size() > 0) {
				loginInUserIsProvider = true;
				provider = providerList.get(0);
			}
			
			Date startDate = DateUtil.getStartOfDay(new Date());
			Date endDate = DateUtil.getEndOfDay(new Date());
			if (allAppointments != null) {
				endDate = null;
			}
			
			fragmentModel.addAttribute("isProvider", loginInUserIsProvider);
			fragmentModel.addAttribute("appointments",
			    simplifiedAppointments(appointmentService.getAppointmentsByConstraints(startDate, endDate,
			        sessionContext.getSessionLocation(), provider, null, null)));
			fragmentModel.addAttribute("appointmentsDateTimeToolTip",
			    appointmentsDateTimeToolTip(appointmentService.getAppointmentsByConstraints(startDate, endDate,
			        sessionContext.getSessionLocation(), provider, null, null)));
			
		} else {
			
			fragmentModel.addAttribute("isProvider", false);
			fragmentModel.addAttribute("appointments", new ArrayList<SimplifiedAppointment>());
			fragmentModel.addAttribute("appointmentsDateTimeToolTip", new ArrayList<>(Collections.emptyList()));
			
		}
	}
	
	private List<SimplifiedAppointment> simplifiedAppointments(List<Appointment> appointments) throws ParseException {
		
		List<SimplifiedAppointment> simplifiedAppointmentList = new ArrayList<>();
		if (appointments != null && appointments.size() > 0) {
			SimplifiedAppointment simplifiedAppointment;
			for (Appointment appointment : appointments) {
				simplifiedAppointment = new SimplifiedAppointment();
				simplifiedAppointment.setAppointmentId(appointment.getAppointmentId());
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
				        .parse(String.valueOf(appointment.getTimeSlot().getStartDate()));
				String appointmentDate = new SimpleDateFormat("H:mm").format(date);
				simplifiedAppointment.setTimeSlot(appointmentDate);
				if (appointment.getVisit() != null) {
					simplifiedAppointment.setVisit(String.valueOf(appointment.getVisit().getVisitId()));
				}
				simplifiedAppointment
				        .setPatient(BotswanaEmrUtils.formatPersonName(appointment.getPatient().getPersonName()));
				simplifiedAppointment.setStatus(appointment.getStatus().getName());
				simplifiedAppointment.setReason(appointment.getReason());
				simplifiedAppointment.setCancelReason(appointment.getCancelReason());
				simplifiedAppointment.setAppointmentType(appointment.getAppointmentType().getName());
				simplifiedAppointment.setAppointmentId(appointment.getAppointmentId());
				simplifiedAppointment.setPatientObject(appointment.getPatient());
				
				//add to the list of appointments
				simplifiedAppointmentList.add(simplifiedAppointment);
			}
		}
		
		return simplifiedAppointmentList;
	}
	
	private String appointmentsDateTimeToolTip(List<Appointment> appointments) {
		List<String> appointmentDates = new ArrayList<>(Collections.emptyList());
		if (appointments != null && appointments.size() > 0) {
			for (Appointment appointment : appointments) {
				appointmentDates.add(BotswanaEmrUtils.formatDateWithTime(appointment.getTimeSlot().getStartDate(), null));
				appointmentDates.add(BotswanaEmrUtils.formatDateWithTime(appointment.getTimeSlot().getEndDate(), null));
			}
		}
		return String.join(" to ", appointmentDates);
	}
}
