/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.assessment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Diagnosis;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class NursingDiagnosisFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam(value = "visitId") Visit visit,
	        @RequestParam("patientId") Patient person, UiSessionContext uiSessionContext) {
		
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
	}
	
	public void addNursingDiagnosis(@RequestParam("patientId") Patient patient, @RequestParam("diagnosis") String diagnosis,
	        @RequestParam(value = "visitId", required = false) Visit visit, @RequestParam("locationId") Location location) {
		EncounterType consultationEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.DIAGNOSIS_ENCOUNTER_TYPE_UUID);
		Encounter consultationsEncounter = BotswanaEmrUtils.createEncounter(patient, consultationEncounterType, location,
		    visit);
		if (StringUtils.isNotBlank(diagnosis)) {
			Obs nursingDiagnosisObs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NURSING_DIAGNOSIS_CONCEPT_UUID));
			nursingDiagnosisObs.setValueText(diagnosis);
			
			//add this observation to the encounter
			consultationsEncounter.addObs(nursingDiagnosisObs);
			Context.getEncounterService().saveEncounter(consultationsEncounter);
		}
	}
	
	public void editNursingDiagnosis(@RequestParam("lastNursingDiagnosisObsId") Integer lastNursingDiagnosisObsId,
	        @RequestParam("editDiagnosis") String editDiagnosis) {
		ObsService obsService = Context.getObsService();
		if (lastNursingDiagnosisObsId != null && StringUtils.isNotBlank(editDiagnosis)) {
			Obs obs = obsService.getObs(lastNursingDiagnosisObsId);
			obs.setValueText(editDiagnosis);
			
			//commit the edited obs in the database
			obsService.saveObs(obs, "Edited Nursing diagnosis");
			
		}
	}
	
	public SimpleObject fetchNursingDiagnosis(@RequestParam(value = "visitId") Integer visitId,
	        @RequestParam("patientId") Integer patientId, UiSessionContext uiSessionContext,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("visitService") VisitService visitService) {
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		
		Patient person = patientService.getPatient(patientId);
		Visit visit = visitService.getVisit(visitId);
		
		List<Obs> nursingDiagnosesObs = botswanaEmrService.getObservation(person, visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NURSING_DIAGNOSIS_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		
		List<Encounter> nursingDiagnosisEncounters = visit.getEncounters().stream().filter(
		    encounter -> encounter.getEncounterType().getUuid().equals(BotswanaEmrConstants.DIAGNOSIS_ENCOUNTER_TYPE_UUID))
		        .collect(Collectors.toList());
		List<Diagnosis> diagnosesList = new ArrayList<>();
		
		for (Encounter encounter : nursingDiagnosisEncounters) {
			diagnosesList.addAll(Context.getDiagnosisService().getDiagnosesByEncounter(encounter, false, false));
		}
		
		List<SimpleObject> nursingDiagnosisList = diagnosesList.stream().map(d -> {
			SimpleObject diagnosis = new SimpleObject();
			diagnosis.put("uuid", d.getUuid());
			diagnosis.put("value", d.getDiagnosis().getCoded() != null ? d.getDiagnosis().getCoded().getName().getName()
			        : d.getDiagnosis().getNonCoded());
			diagnosis.put("dateCreated", d.getDateCreated());
			return diagnosis;
		}).collect(Collectors.toList());
		
		SimpleObject response = new SimpleObject();
		response.put("nursingDiagnosis", nursingDiagnosisList);
		response.put("lastNursingDiagnosis", nursingDiagnosesObs.isEmpty() ? null : nursingDiagnosisList.get(0));
		
		return response;
	}
	
	public SimpleObject getNursingDiagnoses(@RequestParam("visitId") Integer visitId,
			@RequestParam("patientId") Integer patientId,
			@SpringBean("visitService") VisitService visitService,
			@SpringBean("patientService") PatientService patientService,
			UiSessionContext uiSessionContext) {
		
		Visit visit = visitService.getVisit(visitId);
		Patient person = patientService.getPatient(patientId);
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService()
				.getGlobalProperty("botswanaemr.fetchSize"));
		
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		List<Obs> nursingDiagnosesObs = botswanaEmrService.getObservation(
				person,
				visit,
				BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NURSING_DIAGNOSIS_CONCEPT_UUID),
				uiSessionContext.getSessionLocation(),
				limitFetch
		);
		
		SimpleObject response = new SimpleObject();
		List<SimpleObject> serializedDiagnoses = nursingDiagnosesObs.stream()
				.map(obs -> SimpleObject.create(
						"lastNursingDiagnosisObsId", obs.getId(),
						"concept", obs.getConcept().getName().getName(),
						"value", obs.getValueAsString(Context.getLocale()),
						"obsDatetime", obs.getObsDatetime()
				))
				.collect(Collectors.toList());
		
		response.put("nursingDiagnosis", serializedDiagnoses);
		response.put("lastNursingDiagnosis", nursingDiagnosesObs.size() > 0 ? serializedDiagnoses.get(0) : null);
		return response;
	}
}
