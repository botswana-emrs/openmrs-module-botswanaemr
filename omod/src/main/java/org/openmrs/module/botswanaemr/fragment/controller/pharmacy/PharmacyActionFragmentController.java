/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Order;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.OrderRankings;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toMap;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDateFromString;

public class PharmacyActionFragmentController {
	
	public void controller(FragmentModel model, UiUtils ui) {
		model.addAttribute("patientPoolReturnUrl", ui.pageLink("botswanaemr", "pharmacy/pharmacyPatientPool"));
		model.addAttribute("user", Context.getAuthenticatedUser());

	}
	
	public List<SimpleObject> getListOfTheMostRankingDrugsOverPeriodOfTime(
	        @RequestParam(value = "dateString", required = false) String dateString, UiUtils ui,
	        UiSessionContext sessionContext) throws ParseException {
		List<Order> filledPrescription = new ArrayList<>();
		if (StringUtils.isNotBlank(dateString)) {
			String[] parts = dateString.split("-");
			Date startFromDate = DateUtil.getStartOfDay(formatDateFromString(parts[0], "dd/MM/yyyy"));
			Date endTomDate = DateUtil.getEndOfDay(formatDateFromString(parts[1], "dd/MM/yyyy"));
			filledPrescription = BotswanaEmrUtils.getDrugOrders(startFromDate, endTomDate, null,
			    sessionContext.getSessionLocation());
		}
		List<SimpleObject> simpleObjectList = new ArrayList<>();
		List<OrderRankings> orderRankingsList = new ArrayList<>();
		Set<Concept> conceptSet = new HashSet<>();
		Map<Integer, Integer> drugMappings = new HashMap<Integer, Integer>();
		OrderRankings orderRankings;
		if (!(filledPrescription.isEmpty())) {
			
			for (Order outerOrder : filledPrescription) {
				if (outerOrder.getConcept() != null) {
					conceptSet.add(outerOrder.getConcept());
				}
			}
			for (Concept concept : conceptSet) {
				List<Concept> similarConceptList = new ArrayList<>();
				for (Order innerOrder : filledPrescription) {
					if (innerOrder.getConcept().equals(concept)) {
						similarConceptList.add(innerOrder.getConcept());
					}
				}
				drugMappings.put(concept.getConceptId(), similarConceptList.size());
			}
			Map<Integer, Integer> sorted = drugMappings.entrySet().stream()
			        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
			        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
			
			for (Map.Entry<Integer, Integer> entry : sorted.entrySet()) {
				orderRankings = new OrderRankings();
				orderRankings.setDrugName(Context.getConceptService().getConcept(entry.getKey()).getDisplayString());
				orderRankings.setNumberOfOccurences(entry.getValue());
				
				orderRankingsList.add(orderRankings);
			}
			if (!orderRankingsList.isEmpty()) {
				simpleObjectList = SimpleObject.fromCollection(orderRankingsList, ui, "drugName", "numberOfOccurences");
			}
		}
		return simpleObjectList;
	}
}
