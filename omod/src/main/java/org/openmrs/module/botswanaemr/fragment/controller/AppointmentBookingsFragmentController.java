/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Location;
import org.openmrs.User;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.List;
import java.util.ArrayList;

import static org.openmrs.module.botswanaemr.fragment.controller.admin.FacilitiesFragmentController.getAttributeValueReference;
import static org.openmrs.module.botswanaemr.page.controller.SelectServicePointPageController.getUserAssignedFacilities;

public class AppointmentBookingsFragmentController {
	
	public void controller(FragmentModel model, UiSessionContext sessionContext) {
		User user = sessionContext.getCurrentUser();
		if (user == null) {
			return;
		}
		
		List<Location> allLocations = null;
		if (user.isSuperUser()) {
			allLocations = BotswanaEmrUtils.getFacilitiesList();
			model.addAttribute("locationList", allLocations);
		} else {
			allLocations = getUserAssignedFacilities(user);
			model.addAttribute("locationList", allLocations);
		}
		
		List<SimpleObject> locations = new ArrayList<>();
		for (Location location : allLocations) {
			SimpleObject object = new SimpleObject();
			object.put("facilityName", location.getName());
			object.put("facilityId", location.getId());
			object.put("status", getAttributeValueReference(location, BotswanaEmrConstants.MASTER_FACILITY_STATUS_UUID));
			object.put("dateCreated", location.getDateCreated());
			
			locations.add(object);
		}
		model.addAttribute("locations", locations);
	}
	
}
