/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class TransferOutHistoryFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam("patientId") Patient patient,
	        @FragmentParam("visit") Visit visit, @FragmentParam("encounter") Encounter encounter,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl) {
		final String definitionUiResource = "botswanaemr:htmlforms/art-transfer-out-form.xml";
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("definitionUiResource", definitionUiResource);
		fragmentModel.addAttribute("visit", visit);
		fragmentModel.addAttribute("returnUrl", returnUrl);
		fragmentModel.addAttribute("transferEncounter", encounter);
	}
}
