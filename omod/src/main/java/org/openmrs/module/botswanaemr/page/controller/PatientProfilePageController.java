/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Allergies;
import org.openmrs.Allergy;
import org.openmrs.BaseOpenmrsMetadata;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.RelationshipType;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.LocationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.context.AppContextModel;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appframework.domain.Extension;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.comparator.AllergyComparator;
import org.openmrs.module.botswanaemr.comparator.ConceptComparator;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.model.TriageModel;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.CoreAppsConstants;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.module.coreapps.contextmodel.PatientContextModel;
import org.openmrs.module.coreapps.contextmodel.VisitContextModel;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.emrapi.event.ApplicationEventService;
import org.openmrs.module.emrapi.patient.PatientDomainWrapper;
import org.openmrs.module.emrapi.visit.VisitDomainWrapper;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.representation.Representation;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.InjectBeans;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.ui.framework.page.Redirect;
import org.openmrs.ui.util.ByFormattedObjectComparator;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DOCTORS_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NURSING_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatEncounterTypeText;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getConcept;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterSearchCriteriaPatientTypesAndCurrentVisit;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getVitalsConcepts;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.isConsultationActive;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortCaseEncounterByDateCreated;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortEncountersByEncounterDatetime;

public class PatientProfilePageController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	public Object controller(UiUtils ui, @RequestParam("patientId") Patient patient, final PageModel model,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(required = false, value = "app") AppDescriptor app,
	        @RequestParam(required = false, value = "dashboard") String dashboard,
	        @RequestParam(required = false, value = "patientQueueId") PatientQueue patientQueue,
	        @RequestParam(required = false, value = "encounterId") Encounter orderEncounter,
	        @RequestParam(required = false, value = "referralId") Order referralOrder,
	        @InjectBeans PatientDomainWrapper patientDomainWrapper, @SpringBean("adtService") AdtService adtService,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("providerService") ProviderService providerService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("conceptService") ConceptService conceptService,
	        @SpringBean("appFrameworkService") AppFrameworkService appFrameworkService,
	        @SpringBean("applicationEventService") ApplicationEventService applicationEventService,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties, UiSessionContext sessionContext) {
		
		try {
			if (!Context.hasPrivilege(CoreAppsConstants.PRIVILEGE_PATIENT_DASHBOARD)) {
				return new Redirect("coreapps", "noAccess", "");
			} else if (patient.getVoided() || patient.getPersonVoided()) {
				return new Redirect("coreapps", "patientdashboard/deletedPatient", "patientId=" + patient.getId());
			}
			
			if (StringUtils.isEmpty(dashboard)) {
				dashboard = "patientDashboard";
			}
			
			patientDomainWrapper.setPatient(patient);
			model.addAttribute("patient", patientDomainWrapper);
			model.addAttribute("app", app);
			
			Location visitLocation = null;
			try {
				visitLocation = adtService.getLocationThatSupportsVisits(sessionContext.getSessionLocation());
			}
			catch (IllegalArgumentException ignored) {}
			
			Visit currentPatientVisit = visit != null ? visit : adtService.ensureActiveVisit(patient, visitLocation);
			
			VisitDomainWrapper activeVisit = null;
			if (visitLocation != null) {
				if (visit != null) {
					activeVisit = adtService.wrap(visit);
				} else {
					activeVisit = adtService.getActiveVisit(patient, visitLocation);
				}
			}
			
			boolean diagnosisRecorded = false;
			if (currentPatientVisit != null) {
				model.addAttribute("visitStartdate",
				    new SimpleDateFormat("dd-MMM-yyyy").format(currentPatientVisit.getStartDatetime()));
				model.addAttribute("visitStarttime",
				    new SimpleDateFormat("hh:mm:ss").format(currentPatientVisit.getStartDatetime()));
				model.addAttribute("severity", BotswanaEmrUtils.getPatientSeverityLevel(currentPatientVisit));
				
				List<Encounter> encounters = new ArrayList<>(currentPatientVisit.getEncounters());
				encounters = encounters.stream()
				        .filter(
				            d -> d.getEncounterType().getUuid().equals(BotswanaEmrConstants.DIAGNOSIS_ENCOUNTER_TYPE_UUID))
				        .collect(Collectors.toList());
				for (Encounter encounter : encounters) {
					if (encounter.getDiagnoses().size() > 0) {
						diagnosisRecorded = true;
						break;
					}
					if (encounter.getObs().stream().anyMatch(
					    o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.NURSING_DIAGNOSIS_CONCEPT_UUID))) {
						diagnosisRecorded = true;
						break;
					}
				}
			} else {
				model.addAttribute("visitStartdate", "");
				model.addAttribute("visitStarttime", "");
				model.addAttribute("severity", "");
			}
			model.addAttribute("activeVisit", currentPatientVisit);
			model.addAttribute("activePatientVisit", currentPatientVisit);
			model.addAttribute("diagnosisRecorded", diagnosisRecorded);
			
			AppContextModel contextModel = sessionContext.generateAppContextModel();
			contextModel.put("patient", new PatientContextModel(patient));
			contextModel.put("patientId", patient.getUuid());
			contextModel.put("visit", activeVisit == null ? null : new VisitContextModel(activeVisit));
			
			PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
			
			Object sessionLocationId = sessionContext.getSession().getAttribute(CURRENT_SERVICE_DELIVERY_POINT);
			Location currentServiceDeliveryPoint = Context.getLocationService()
			        .getLocationByUuid(String.valueOf(sessionLocationId));
			
			PatientQueue mostRecentPatientQueue = patientQueue != null ? patientQueue
			        : patientQueueingService.getIncompletePatientQueue(patient, currentServiceDeliveryPoint);
			
			if (mostRecentPatientQueue == null && activeVisit != null) {
				List<PatientQueue> patientQueueList = patientQueueingService.getPatientQueueListBySearchParams(
				    patient.getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier(), null,
				    null, currentServiceDeliveryPoint, null, null);
				
				VisitDomainWrapper finalActiveVisit = activeVisit;
				mostRecentPatientQueue = patientQueueList.stream()
				        .filter(q -> finalActiveVisit.getSortedEncounters().contains(q.getEncounter())).findFirst()
				        .orElse(null);
			}
			
			List<PatientQueue> patientQueues = patientQueueingService.getPatientQueueList(null, null, null, null, null,
			    patient, null);
			model.addAttribute("patientQueues", patientQueues);
			
			HashSet<String> consultationEncounterTypeUuids = new HashSet<>();
			consultationEncounterTypeUuids.add(BotswanaEmrConstants.CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID);
			consultationEncounterTypeUuids.add(BotswanaEmrConstants.CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID);
			BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
			List<EncounterType> encounterTypes = new ArrayList<>();
			List<Encounter> patientEncounters = encounterService.getEncountersByPatient(patient);
			List<CaseEncounter> caseEncounterList = sortCaseEncounterByDateCreated(
			    botswanaEmrService.getAllCaseEncounter(patient, false));
			
			Encounter consultationEncounter;
			CaseEncounter caseEncounter = null;
			if ((mostRecentPatientQueue != null) && (mostRecentPatientQueue.getEncounter() != null)
			        && consultationEncounterTypeUuids
			                .contains(mostRecentPatientQueue.getEncounter().getEncounterType().getUuid())) {
				
				consultationEncounter = mostRecentPatientQueue.getEncounter();
			} else {
				consultationEncounter = getCurrentConsultationEncounter(currentPatientVisit, patientEncounters,
				    consultationEncounterTypeUuids);
			}
			if (caseEncounterList != null && caseEncounterList.size() > 0) {
				caseEncounter = caseEncounterList.get(caseEncounterList.size() - 1);
			}
			
			if (consultationEncounter != null) {
				model.addAttribute("encounterId", consultationEncounter.getEncounterId());
				model.addAttribute("consultationStartdate",
				    new SimpleDateFormat("dd-MMM-yyyy").format(consultationEncounter.getEncounterDatetime()));
				model.addAttribute("consultationStarttime",
				    new SimpleDateFormat("hh:mm:ss").format(consultationEncounter.getEncounterDatetime()));
				model.addAttribute("consultationProvider",
				    formatPersonName(consultationEncounter.getCreator().getPersonName()));
			} else {
				model.addAttribute("encounterId", "");
				model.addAttribute("consultationStartdate", "");
				model.addAttribute("consultationStarttime", "");
				model.addAttribute("consultationProvider", "");
			}
			if (caseEncounter != null) {
				model.addAttribute("caseId", caseEncounter.getCases().getCaseId());
			} else {
				model.addAttribute("caseId", "");
			}
			
			contextModel.put("encounterTypes",
			    ConversionUtil.convertToRepresentation(encounterTypes, Representation.DEFAULT));
			contextModel.put("encounters", patientEncounters);
			
			List<Program> programs = new ArrayList<>();
			List<Program> patientActivePrograms = new ArrayList<>();
			List<PatientProgram> patientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, null,
			    null, null, null, null, false);
			for (PatientProgram patientProgram : patientPrograms) {
				programs.add(patientProgram.getProgram());
				if (patientProgram.getDateCompleted() == null) {
					patientActivePrograms.add(patientProgram.getProgram());
				}
			}
			model.addAttribute("patientActivePrograms", patientActivePrograms);
			model.addAttribute("tbProgramUuid", BotswanaEmrConstants.TB_PROGRAM_UUID);
			
			// contextModel.put("patientPrograms", ConversionUtil.convertToRepresentation(programs, Representation.DEFAULT));
			
			model.addAttribute("appContextModel", contextModel);
			
			List<Extension> overallActions = appFrameworkService.getExtensionsForCurrentUser(dashboard + ".overallActions",
			    contextModel);
			Collections.sort(overallActions);
			model.addAttribute("overallActions", overallActions);
			
			List<Extension> visitActions;
			if (activeVisit == null) {
				visitActions = new ArrayList<>();
			} else {
				visitActions = appFrameworkService.getExtensionsForCurrentUser(dashboard + ".visitActions", contextModel);
				Collections.sort(visitActions);
			}
			model.addAttribute("visitActions", visitActions);
			// model.addAttribute("activeVisit", visit);
			
			List<Extension> includeFragments = appFrameworkService
			        .getExtensionsForCurrentUser(dashboard + ".includeFragments", contextModel);
			includeFragments = excludePatientQueingFragments(includeFragments);
			
			Collections.sort(includeFragments);
			model.addAttribute("includeFragments", includeFragments);
			
			List<Extension> firstColumnFragments = appFrameworkService
			        .getExtensionsForCurrentUser(dashboard + ".firstColumnFragments", contextModel);
			Collections.sort(firstColumnFragments);
			model.addAttribute("firstColumnFragments", firstColumnFragments);
			
			List<Extension> secondColumnFragments = appFrameworkService
			        .getExtensionsForCurrentUser(dashboard + ".secondColumnFragments", contextModel);
			Collections.sort(secondColumnFragments);
			model.addAttribute("secondColumnFragments", secondColumnFragments);
			
			List<Extension> otherActions = appFrameworkService.getExtensionsForCurrentUser(
			    ("patientDashboard".equals(dashboard) ? "clinicianFacingPatientDashboard" : dashboard) + ".otherActions",
			    contextModel);
			Collections.sort(otherActions);
			model.addAttribute("otherActions", otherActions);
			
			List<Location> allLocations = Context.getLocationService().getAllLocations();
			List<Location> childLocations = new ArrayList<>();
			List<Location> parentLocations = new ArrayList<>();
			for (Location location : allLocations) {
				if (location.getParentLocation() != null) {
					childLocations.add(location);
				} else {
					parentLocations.add(location);
				}
			}
			
			setLocationsModelAttributes(model, "childLocations", childLocations);
			setLocationsModelAttributes(model, "parentLocations", parentLocations);
			
			Location servicePointVisited = null;
			LocationService locationService = Context.getService(LocationService.class);
			try {
				Context.addProxyPrivilege(VIEW_LOCATIONS);
				Context.addProxyPrivilege(GET_LOCATIONS);
				
				String currentServiceDeliveryPointAttribute = String
				        .valueOf(sessionContext.getSession().getAttribute(CURRENT_SERVICE_DELIVERY_POINT));
				servicePointVisited = locationService.getLocationByUuid(currentServiceDeliveryPointAttribute.toString());
			}
			finally {
				Context.removeProxyPrivilege(VIEW_LOCATIONS);
				Context.removeProxyPrivilege(GET_LOCATIONS);
			}
			Boolean isNursingPortal = servicePointVisited != null
			        && servicePointVisited.getUuid().equals(NURSING_PORTAL_UUID);
			model.addAttribute("isNursingPortal", isNursingPortal);
			
			List<Provider> providers = providerService.getAllProviders();
			model.addAttribute("providers", providers);
			
			List<Program> programList = Context.getProgramWorkflowService().getAllPrograms();
			removeRetiredPrograms(programList);
			removeSomePrograms(programList);
			model.addAttribute("programList", programList);
			
			List<Concept> diagnosisConcepts = conceptService
			        .getConceptsByClass(conceptService.getConceptClassByName("Diagnosis"));
			Comparator<Concept> comparator = new ConceptComparator(new ByFormattedObjectComparator(ui));
			diagnosisConcepts.sort(comparator);
			model.addAttribute("diagnosisConcepts", diagnosisConcepts);
			ObjectMapper jsonMapper = new ObjectMapper();
			List<String> diagnosisConceptNames = new ArrayList<>();
			for (Concept diagnosisConcept : diagnosisConcepts) {
				diagnosisConceptNames.add(diagnosisConcept.getName().getName());
			}
			String json = "";
			try {
				json = jsonMapper.writeValueAsString(diagnosisConceptNames);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute("diagnosisConceptNames", json);
			model.addAttribute("baseDashboardUrl", coreAppsProperties.getDashboardUrl());
			model.addAttribute("dashboard", dashboard);
			model.addAttribute("returnUrl", returnUrl);
			
			model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
			
			model.addAttribute("breadCrumbsFormatters",
			    Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
			
			model.addAttribute("sessionLocation", sessionContext.getSessionLocation().getName());
			
			String serviceDeliveryLocationUuid = String
			        .valueOf(sessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
			
			model.addAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT, serviceDeliveryLocationUuid);
			
			model.addAttribute("isDoctorsPortal",
			    BotswanaEmrConstants.DOCTORS_PORTAL_UUID.equals(serviceDeliveryLocationUuid));
			
			model.addAttribute("allergies", getAllergies(ui, patientService, patient));
			
			//check status of the queue for the patient
			boolean isPendingStatus = false;
			boolean isPickedStatus = false;
			
			PatientQueue.Status queueStatus = PatientQueue.Status.PENDING;
			Integer queueId = null;
			
			// If referral is not null and patient queue is null, then set pending status to true
			// This will allow the provider to begin a new consultation
			if (referralOrder != null && patientQueue == null) {
				isPendingStatus = true;
			} else {
				if (mostRecentPatientQueue != null) {
					queueId = mostRecentPatientQueue.getPatientQueueId();
					queueStatus = mostRecentPatientQueue.getStatus();
					if (queueStatus.equals(PatientQueue.Status.PENDING)) {
						isPendingStatus = true;
					}
					if (queueStatus.equals(PatientQueue.Status.PICKED)) {
						isPickedStatus = true;
					}
				}
			}
			
			/*
			  capitalize() is a static method of the StringUtils class
			  that is used to convert the first character of the given
			  string to title case as per Character.toTitleCase.
			
			  The remaining characters of the string are not changed.
			 */
			model.addAttribute("queueStatus", (mostRecentPatientQueue == null) ? ""
			        : StringUtils.capitalize(StringUtils.lowerCase(translateQueueStatus(queueStatus), Context.getLocale())));
			model.addAttribute("isPendingState", isPendingStatus);
			model.addAttribute("isPickedState", isPickedStatus);
			boolean isCurrentVisitStartedByCurrentProvider = false;
			if (activeVisit != null && activeVisit.getVisit() != null && Context.getAuthenticatedUser() != null) {
				// Get the consultation encounter from the current visit and compare the creator of the encounter with the current authenticated user
				Encounter consultationEncounterFromCurrentVisit = getCurrentConsultationEncounter(activeVisit.getVisit(),
				    patientEncounters, consultationEncounterTypeUuids);
				
				isCurrentVisitStartedByCurrentProvider = consultationEncounterFromCurrentVisit != null
				        && consultationEncounterFromCurrentVisit.getCreator().getPerson()
				                .equals(Context.getAuthenticatedUser().getPerson());
			}
			
			model.addAttribute("isCurrentProvider", isCurrentVisitStartedByCurrentProvider);
			PersonName providerName = Context.getAuthenticatedUser().getPersonName();
			model.addAttribute("provider", providerName == null ? "" : providerName.getFullName());
			model.addAttribute("patientNames", patient.getPersonName().getFullName());
			model.addAttribute("patientQueueId", queueId);
			model.addAttribute("activePatient", patient);
			model.addAttribute("referralId", referralOrder == null ? "" : referralOrder.getOrderId());
			
			String prescriptionStatus = "COMPLETED";
			if (orderEncounter != null && orderEncounter.getOrders() != null) {
				for (Order order : orderEncounter.getOrders()) {
					if (order.getFulfillerStatus() != null
					        && !(order.getFulfillerStatus().name().equals(Order.FulfillerStatus.COMPLETED.name()))) {
						prescriptionStatus = "AWAITING";
						break;
					}
				}
			}
			model.addAttribute("prescriptionStatus",
			    StringUtils.capitalize(StringUtils.lowerCase(prescriptionStatus, Context.getLocale())));
			
			// Status if consultation is active
			Boolean isConsultationActive = isConsultationActive(patient);
			model.addAttribute("isConsultationActive", isConsultationActive);
			
			model.addAttribute("educationLevels", BotswanaEmrUtils.getRequiredEducationLevels());
			
			List<Concept> employmentSectors = getEmploymentSectors();
			model.addAttribute("employmentSectors", employmentSectors);
			
			List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
			HashSet<String> hashSet = new HashSet<>();
			for (RelationshipType relationshipType : relationshipTypes) {
				hashSet.add(relationshipType.getaIsToB());
				hashSet.add(relationshipType.getbIsToA());
			}
			List<String> relationships = new ArrayList<>(hashSet);
			Collections.sort(relationships);
			model.addAttribute("relationships", relationships);
			
			Integer limitFetch = Integer
			        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
			List<Encounter> consultationPatientEncounters = BotswanaEmrUtils.getConsultationPatientEncounters(patient,
			    limitFetch);
			model.addAttribute("consultationPatientEncounters", consultationPatientEncounters);
			//get the last encounter for a patient with triage encounter type
			List<Encounter> lastEncounterForPatientByDoctorsAndNurseConsultation = encounterService.getEncounters(
			    getEncounterSearchCriteriaPatientTypesAndCurrentVisit(patient,
			        Arrays.asList(
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID),
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID),
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID),
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE)),
			        activeVisit.getVisit()));
			//order those encounters
			List<Encounter> consultationEncounters = sortEncountersByEncounterDatetime(consultationPatientEncounters)
			        .stream().limit(BotswanaEmrUtils.LIMIT_FETCH_SIZE).collect(Collectors.toList());
			
			model.addAttribute("consultationEncounters", BotswanaEmrUtils.simplifiedMedicalHistory(consultationEncounters));
			
			boolean isRegistration;
			User authenticatedUser = sessionContext.getCurrentUser();
			isRegistration = authenticatedUser != null && BotswanaEmrUtils.isRegistrationClerk(authenticatedUser);
			model.addAttribute("isRegistration", isRegistration);
			
			Encounter requiredCurrentEncounter = null;
			Encounter requiredPreviousEncounter = null;
			String previousProvider = null;
			List<TriageModel> getCurrentTriageObs = new ArrayList<>();
			List<TriageModel> getPreviousTriageObs = new ArrayList<>();
			TriageModel currentTriageModel;
			TriageModel previousTriageModel;
			Integer currentSystollic = null;
			Integer currentDiastollic = null;
			Integer previousSystollic = null;
			Integer previousDiastollic = null;
			Double currentTemperature = null;
			Double previousTemperature = null;
			List<Encounter> orderEncountersNewChecksFromVitals = new ArrayList<>();
			if (lastEncounterForPatientByDoctorsAndNurseConsultation.size() > 0) {
				//loop through all and pick the ones with vitals recorded
				List<Encounter> doctorsNurseEncounterWithVitalsViaNewChecks = new ArrayList<>();
				for (Encounter encounter : lastEncounterForPatientByDoctorsAndNurseConsultation) {
					Set<Obs> obsSet = encounter.getAllObs();
					//check if this encounter has any of the vitals obs
					for (Obs obs : obsSet) {
						if (getVitalsConcepts().contains(obs.getConcept())) {
							//pick that encounter and exit immediately
							doctorsNurseEncounterWithVitalsViaNewChecks.add(encounter);
							break;
						}
					}
				}
				//order those encounter with vitals
				
				if (doctorsNurseEncounterWithVitalsViaNewChecks.size() > 0) {
					orderEncountersNewChecksFromVitals
					        .addAll(sortEncountersByEncounterDatetime(doctorsNurseEncounterWithVitalsViaNewChecks));
				}
			}
			if (orderEncountersNewChecksFromVitals.size() == 1) {
				requiredCurrentEncounter = orderEncountersNewChecksFromVitals.get(0);
			} else if (orderEncountersNewChecksFromVitals.size() > 1) {
				requiredCurrentEncounter = orderEncountersNewChecksFromVitals
				        .get(orderEncountersNewChecksFromVitals.size() - 1);
				requiredPreviousEncounter = orderEncountersNewChecksFromVitals
				        .get(orderEncountersNewChecksFromVitals.size() - 2);
				previousProvider = requiredPreviousEncounter.getCreator().getPersonName().getFullName();
			}
			
			if (requiredCurrentEncounter != null) {
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				
				for (Obs currentObs : requiredCurrentEncounter.getAllObs(false)) {
					if (currentObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SYSTOLIC_BP))) {
						currentSystollic = currentObs.getValueNumeric().intValue();
					}
					if (currentObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.DIASTOLIC_BP))) {
						currentDiastollic = currentObs.getValueNumeric().intValue();
					}
					if (currentObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.TEMPERATURE))) {
						currentTemperature = currentObs.getValueNumeric();
					}
					if (getVitalsConcepts().contains(currentObs.getConcept())) {
						currentTriageModel = new TriageModel();
						currentTriageModel.setName(currentObs.getConcept().getDisplayString());
						if (currentObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.LMP))) {
							currentTriageModel.setValue(simpleDateFormat.format(currentObs.getValueDate()));
						} else if (currentObs.getConcept()
						        .equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PREGNANCY_STATUS))
						        || currentObs.getConcept()
						                .equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CONSCIOUS))
						        || currentObs.getConcept()
						                .equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.RESPONSIVE))) {
							currentTriageModel.setValue(currentObs.getValueCoded().getName().getName());
						} else {
							currentTriageModel.setValue(currentObs.getValueNumeric().toString());
						}
						//add this object to a list
						getCurrentTriageObs.add(currentTriageModel);
					}
				}
			}
			
			if (requiredPreviousEncounter != null) {
				
				for (Obs previousObs : requiredPreviousEncounter.getAllObs(false)) {
					if (previousObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SYSTOLIC_BP))) {
						previousSystollic = previousObs.getValueNumeric().intValue();
					}
					if (previousObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.DIASTOLIC_BP))) {
						previousDiastollic = previousObs.getValueNumeric().intValue();
					}
					if (previousObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.TEMPERATURE))) {
						previousTemperature = previousObs.getValueNumeric();
					}
					if (getVitalsConcepts().contains(previousObs.getConcept())) {
						previousTriageModel = new TriageModel();
						previousTriageModel.setName(previousObs.getConcept().getDisplayString());
						previousTriageModel.setValue(previousObs.getValueAsString(Locale.ENGLISH));
						//add this object to a list
						getPreviousTriageObs.add(previousTriageModel);
					}
				}
			}
			
			model.addAttribute("currentTriageLastEncounter", requiredCurrentEncounter);
			model.addAttribute("previousTriageLastEncounter", requiredPreviousEncounter);
			model.addAttribute("previousProvider", previousProvider);
			model.addAttribute("currentSystollic", currentSystollic);
			model.addAttribute("currentDiastollic", currentDiastollic);
			model.addAttribute("currentTriageData", getCurrentTriageObs);
			model.addAttribute("previousTriageData", getPreviousTriageObs);
			model.addAttribute("previousSystollic", previousSystollic);
			model.addAttribute("previousDiastollic", previousDiastollic);
			model.addAttribute("previousTemperature", previousTemperature);
			model.addAttribute("currentTemperature", currentTemperature);
			model.addAttribute("temperatureHasChanged", getDiffBetweenValues(currentTemperature, previousTemperature));
			model.addAttribute("allExistingCases", Context.getService(BotswanaEmrService.class).getAllCase(patient, false));
			model.addAttribute("caseState",
			    checkCaseState(Context.getService(BotswanaEmrService.class).getAllCaseEncounter(patient, false)));
			
			applicationEventService.patientViewed(patient, sessionContext.getCurrentUser());
			
			Map<String, PersonAttribute> generalAttributes = new HashMap<>();
			Map<String, PersonAttribute> nextOfKinAttributes = new HashMap<>();
			
			Map<String, PersonAttribute> attributes = patient.getAllAttributeMap();
			for (Map.Entry<String, PersonAttribute> entry : attributes.entrySet()) {
				// Filter keys that start with `NOK`
				// to get the next of kin attributes
				if (entry.getKey().startsWith("NOK")) {
					nextOfKinAttributes.put(entry.getKey(), entry.getValue());
					model.addAttribute("nextOfKinAttributes", nextOfKinAttributes);
				} else if (!entry.getKey().startsWith("NOK")) {
					generalAttributes.put(entry.getKey(), entry.getValue());
					model.addAttribute("generalAttributes", generalAttributes);
				} else {
					return Collections.emptyMap();
				}
			}
			
			BotswanaEmrUtils.setPatientIdentifierAttributes(model, patient);
			
			model.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
			model.addAttribute("isConsultation", false);
			model.addAttribute("sameUserAsCurrent",
			    checkIfLoggedInProviderIsSameAsThatEnteredMostCurrentVitals(requiredCurrentEncounter));
			//add the fields for the edit checks, only load this if there is an encounter to be loaded otherwise do not
			model.addAttribute("hasConsultationToBeEdited",
			    hasConsultationEncounterWithVitalsToLoad(requiredCurrentEncounter));
			BotswanaEmrUtils.loadModelWithVitals(model, requiredCurrentEncounter);
			
			model.addAttribute("currentServicePoint",
			    sessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
			
			Location doctorsPortal = Context.getLocationService().getLocationByUuid(DOCTORS_PORTAL_UUID);
			model.addAttribute("doctorsPortal", doctorsPortal.getUuid());
			
			boolean isBf = sessionContext.getSessionLocation().getTags().stream().anyMatch(
			    locationTag -> locationTag.getName().equals(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME));
			model.addAttribute("isBf", isBf);
			
			model.addAttribute("currentServiceDeliveryPoint", currentServiceDeliveryPoint);
			
			return null;
			
		}
		catch (NullPointerException ex) {
			return new Redirect("botswanaemr", "patientNotFound", "patientId=" + "Not Found");
		}
	}
	
	public static List<Concept> getEmploymentSectors() {
		ConceptService cs = Context.getConceptService();
		List<Concept> employmentSectors = new ArrayList<>();
		for (String uuid : BotswanaEmrConstants.EMPLOYMENT_SECTOR_CONCEPTS) {
			Concept concept = cs.getConceptByUuid(uuid);
			if (concept != null) {
				employmentSectors.add(concept);
			}
		}
		return employmentSectors;
	}
	
	private void removeSomePrograms(List<Program> programList) {
		//remove the programs that are not needed
		programList.removeIf(program -> program.getUuid().equals(BotswanaEmrConstants.MDR_TB_PROGRAM_UUID)
		        || program.getUuid().equals(BotswanaEmrConstants.MALARIA_PROGRAM_UUID)
		        || program.getUuid().equals(BotswanaEmrConstants.OUTPATIENT_PROGRAM_UUID)
		        || program.getUuid().equals(BotswanaEmrConstants.SURGICAL_PROGRAM_UUID));
	}
	
	private void removeRetiredPrograms(List<Program> programList) {
		programList.removeIf(BaseOpenmrsMetadata::getRetired);
	}
	
	public static String translateQueueStatus(PatientQueue.Status status) {
		if (status.equals(PatientQueue.Status.PENDING)) {
			return BotswanaEmrConstants.AWAITING;
		}
		
		if (status.equals(PatientQueue.Status.COMPLETED)) {
			return BotswanaEmrConstants.TREATED;
		}
		
		return status.toString();
	}
	
	private void setLocationsModelAttributes(PageModel model, String modelAttributeName, List<Location> locations) {
		if (!locations.isEmpty()) {
			model.addAttribute(modelAttributeName, locations);
		} else {
			model.addAttribute(modelAttributeName, "");
		}
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<GeneralPatientObject>();
		GeneralPatientObject generalPatientObject = null;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(BotswanaEmrUtils
				        .formatPerson(new ArrayList<>(encounter.getEncounterProviders()).get(0).getProvider().getPerson()));
				generalPatientObject.setDuration(formatDurationSinceRegistration(patient));
				generalPatientObject.setTypeText(formatEncounterTypeText(encounter.getEncounterType().getUuid()));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
	
	private Encounter getCurrentConsultationEncounter(Visit activeVisit, List<Encounter> encounters,
	        HashSet<String> consultationEncounterTypesUuids) {
		Visit finalActiveVisit = activeVisit;
		List<Encounter> consultationEncounters = new ArrayList<>();
		if (activeVisit != null) {
			consultationEncounters = encounters.stream().filter(e -> e.getVisit() != null)
			        .filter(e -> e.getVisit().equals(finalActiveVisit)
			                && (consultationEncounterTypesUuids.contains(e.getEncounterType().getUuid())))
			        .collect(Collectors.toList());
		}
		Encounter consultationEncounter = null;
		if (!consultationEncounters.isEmpty()) {
			consultationEncounter = consultationEncounters.get(0);
		}
		return consultationEncounter;
	}
	
	private List<Extension> excludePatientQueingFragments(List<Extension> includeFragments) {
		List<Extension> list = new ArrayList<>();
		for (Extension f : includeFragments) {
			if (!f.getId().contains("PatientQueue")) {
				list.add(f);
			}
		}
		includeFragments = list;
		return includeFragments;
	}
	
	public List<PersonAttribute> getBreadCrumbsDetails(Patient patient) {
		String breadCrumbsDetailsAttrTypeUuids = Context.getAdministrationService()
		        .getGlobalProperty("breadCrumbs.details.personAttr.uuids");
		List<PersonAttribute> personNamePersonAttrs = new ArrayList<>();
		if (StringUtils.isNotBlank(breadCrumbsDetailsAttrTypeUuids)) {
			for (String breadCrumbDetailAttrTypeUuid : breadCrumbsDetailsAttrTypeUuids.split(",")) {
				PersonAttributeType pat = Context.getPersonService()
				        .getPersonAttributeTypeByUuid(breadCrumbDetailAttrTypeUuid);
				PersonAttribute pa = patient.getAttribute(pat);
				if (pa != null) {
					personNamePersonAttrs.add(pa);
				}
			}
		}
		return CollectionUtils.isNotEmpty(personNamePersonAttrs) ? personNamePersonAttrs : null;
	}
	
	public Allergies getAllergies(UiUtils ui, PatientService patientService, Patient patient) {
		Allergies allergies = patientService.getAllergies(patient);
		Comparator<Allergy> comparator = new AllergyComparator(new ByFormattedObjectComparator(ui));
		allergies.sort(comparator);
		return CollectionUtils.isNotEmpty(allergies) ? allergies : null;
	}
	
	EncounterSearchCriteria getEncounterSearchCriteria(Patient patient, List<EncounterType> types) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		
	}
	
	private Boolean checkIfLoggedInProviderIsSameAsThatEnteredMostCurrentVitals(Encounter currentEncounter) {
		boolean isCurrent = false;
		if (currentEncounter != null && currentEncounter.getCreator() != null
		        && currentEncounter.getCreator().equals(Context.getAuthenticatedUser())) {
			Set<Obs> currentEncounterObs = currentEncounter.getAllObs();
			if (currentEncounterObs != null) {
				for (Obs obs : currentEncounterObs) {
					if (getVitalsConcepts().contains(obs.getConcept())) {
						isCurrent = true;
						break;
					}
				}
			}
		}
		return isCurrent;
	}
	
	private boolean hasConsultationEncounterWithVitalsToLoad(Encounter encounter) {
		boolean isFound = false;
		if (encounter != null) {
			Set<Obs> obsSet = encounter.getAllObs();
			//check if this encounter has any of the vitals obs
			if (obsSet != null) {
				for (Obs obs : obsSet) {
					if (getVitalsConcepts().contains(obs.getConcept())) {
						isFound = true;
					}
				}
			}
		}
		return isFound;
	}
	
	private boolean getDiffBetweenValues(Double current, Double previous) {
		boolean hasDifference = false;
		if (current != null && previous != null && current > previous) {
			hasDifference = true;
		}
		return hasDifference;
	}
	
	private String checkCaseState(List<CaseEncounter> casesList) {
		String state = "";
		if (casesList != null && casesList.size() > 0) {
			List<CaseEncounter> caseEncounterList = sortCaseEncounterByDateCreated(casesList);
			CaseEncounter lastCaseEncounter = caseEncounterList.get(caseEncounterList.size() - 1);
			if (lastCaseEncounter != null) {
				if (lastCaseEncounter.getEncounter().getEncounterType().equals(Context.getEncounterService()
				        .getEncounterTypeByUuid(BotswanaEmrConstants.CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID))) {
					state = "new";
				} else if (lastCaseEncounter.getEncounter().getEncounterType().equals(Context.getEncounterService()
				        .getEncounterTypeByUuid(BotswanaEmrConstants.CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID))) {
					state = "existing";
				}
				
			}
			
		}
		return state;
	}
}
