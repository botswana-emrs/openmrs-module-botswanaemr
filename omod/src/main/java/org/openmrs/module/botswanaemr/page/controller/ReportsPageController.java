/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.Module;
import org.openmrs.module.ModuleFactory;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ReportsPageController {
	
	public void controller(PageModel model, UiSessionContext uiSessionContext) {
		boolean moduleStatus = false;
		
		for (Module mod : ModuleFactory.getLoadedModules()) {
			if (mod.getModuleId().equals("botswanaemrreports") && mod.isStarted()) {
				moduleStatus = true;
			}
		}
		
		model.addAttribute("moduleStatus", moduleStatus);
		model.addAttribute("hospitalName",
		    Context.getAuthenticatedUser().getUserProperty(BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		        Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital")));
		model.addAttribute("userRoles", new ArrayList<>(Context.getService(UserService.class).getAllRoles().stream()
		        .filter(role -> role.getName().startsWith("Org")).collect(Collectors.toList())));
		model.addAttribute("mailConfigs", Context.getService(BotswanaEmrService.class).getBotswanaEmrEmailReportConfigs("",
		    uiSessionContext.getSessionLocation()));
	}
	
}
