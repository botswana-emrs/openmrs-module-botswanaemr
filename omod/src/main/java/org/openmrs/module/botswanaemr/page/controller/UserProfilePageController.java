/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Person;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.NextOfKinInformationFragmentController;
import org.openmrs.module.botswanaemr.fragment.controller.admin.SecuritySettingsFragmentController;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.ui.framework.page.Redirect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

public class UserProfilePageController extends PatientProfilePageController {
	
	protected final Log log = LogFactory.getLog(NextOfKinInformationFragmentController.class);
	
	@Qualifier("userService")
	@Autowired
	protected UserService userService;
	
	public Object controller(PageModel pageModel, UiSessionContext sessionContext, UiUtils ui) {
		User user = Context.getAuthenticatedUser();
		if (user == null) {
			return new Redirect("botswanaemr", "BotswanaemrLogin", "returnUrl=" + ui.pageLink("botswanaemr", "userProfile"));
		}
		pageModel.addAttribute("user", user);
		pageModel.addAttribute("notifyNewPatientsInPool",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_ON_NEW_PATIENT_IN_POOL_KEY));
		pageModel.addAttribute("notifyPatientSelected",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_ON_CURRENT_PATIENT_SELECTION_KEY));
		pageModel.addAttribute("notifyConsultationEdits",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_ON_CONSULTATION_EDITS_KEY));
		pageModel.addAttribute("notifyLabResults", user.getUserProperty(USER_PROPERTY_NOTIFY_LAB_RESULTS_KEY));
		pageModel.addAttribute("notifySuccessfulAdmissions",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY));
		pageModel.addAttribute("notifySuccessfulAdmissions",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY));
		pageModel.addAttribute("notifySuccessfulAdmissions",
		    user.getUserProperty(USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY));
		pageModel.addAttribute("roles", user.getRoles().stream().map(ur -> ur.getName()).collect(Collectors.joining(",")));
		
		return null;
	}
	
	public String post(@RequestParam(value = "givenName", required = false) String givenName,
	        @RequestParam(value = "familyName", required = false) String familyName,
	        @RequestParam(value = "userNationalId", required = false) String userNationalId,
	        @RequestParam(value = "telephoneNumber", required = false) String telephoneNumber,
	        @RequestParam(value = "country", required = false) String country,
	        @RequestParam(value = "streetAddress", required = false) String streetAddress,
	        @RequestParam(value = "email", required = false) String email, UiUtils uiUtils) {
		
		User user = Context.getAuthenticatedUser();
		if (!(givenName.isEmpty())) {
			user.setEmail(email);
			user.getPersonName().setGivenName(givenName);
			user.getPersonName().setFamilyName(familyName);
		}
		
		Set<PersonAttribute> personAttributeSet = user.getPerson().getAttributes();
		Person person = user.getPerson();
		if (personAttributeSet.isEmpty()) {
			createPersonAttribute(person, userNationalId, USER_NATIONAL_ID_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(person, telephoneNumber, USER_TELEPHONE_CONTACT_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(person, country, USER_COUNTRY_ATTRIBUTE_TYPE);
			createPersonAttribute(person, streetAddress, USER_STREET_ADDRESS_ATTRIBUTE_TYPE);
			
		} else {
			for (PersonAttribute attribute : personAttributeSet) {
				updateUserAttribute(attribute, userNationalId, USER_NATIONAL_ID_ATTRIBUTE_TYPE_UUID);
				updateUserAttribute(attribute, telephoneNumber, USER_TELEPHONE_CONTACT_ATTRIBUTE_TYPE_UUID);
				updateUserAttribute(attribute, country, USER_COUNTRY_ATTRIBUTE_TYPE);
				updateUserAttribute(attribute, streetAddress, USER_STREET_ADDRESS_ATTRIBUTE_TYPE);
			}
			person.setAttributes(personAttributeSet);
		}
		
		try {
			Person userPerson = Context.getPersonService().savePerson(person);
			user.setPerson(userPerson);
			Context.getUserService().saveUser(user);
		}
		catch (Exception e) {
			log.warn("Error occurred while saving user's details", e);
		}
		
		return "redirect:" + uiUtils.pageLink("botswanaemr", "userProfile");
	}
	
	private void updateUserAttribute(PersonAttribute userPersonAttribute, String attributeValue, String attributeUuid) {
		if (getPersonAttributeTypeUuid(userPersonAttribute).equals(attributeUuid)) {
			userPersonAttribute.setValue(attributeValue);
		}
	}
	
	private String getPersonAttributeTypeUuid(PersonAttribute personAttribute) {
		return personAttribute.getAttributeType().getUuid();
	}
	
	//Create new Attribute
	public void createPersonAttribute(Person person, String attributeValue, String attributeUuid) {
		PersonAttribute attribute = setPersonAttributeValue(attributeValue, attributeUuid);
		person.addAttribute(attribute);
	}
	
	public PersonAttribute setPersonAttributeValue(String personAttributeValue, String personAttributeTypeUuid) {
		PersonAttributeType personAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(personAttributeTypeUuid);
		PersonAttribute personAttribute = null;
		if (personAttributeType != null) {
			personAttribute = new PersonAttribute();
			personAttribute.setAttributeType(personAttributeType);
			personAttribute.setValue(personAttributeValue);
		}
		
		return personAttribute;
	}
}
