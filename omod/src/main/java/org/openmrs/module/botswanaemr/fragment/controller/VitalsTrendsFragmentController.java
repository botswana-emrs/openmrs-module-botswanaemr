/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterSearchCriteriaUsingEncounterTypes;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getVitalsConcepts;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortEncountersByEncounterDatetime;

public class VitalsTrendsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient) {
		model.addAttribute("patient", patient);
		
	}
	
	public SimpleObject getPatientVitals(@RequestParam("patientId") String patientId) {
		SimpleObject simpleObject = new SimpleObject();
		
		List<String> dateRanges = new ArrayList<>();
		List<VitalObject> weights = new ArrayList<>();
		List<VitalObject> temperatures = new ArrayList<>();
		List<VitalObject> bmis = new ArrayList<>();
		List<VitalObject> heights = new ArrayList<>();
		List<VitalObject> diastolic = new ArrayList<>();
		List<VitalObject> systolic = new ArrayList<>();
		List<VitalObject> pulse = new ArrayList<>();
		
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		EncounterService encounterService = Context.getEncounterService();
		if (patient != null) {
			List<Encounter> lastEncounterForPatientByDoctorsAndNurseConsultation = encounterService
			        .getEncounters(getEncounterSearchCriteriaUsingEncounterTypes(patient,
			            Arrays.asList(
			                BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE),
			                BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID),
			                BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.VITALS_ENCOUNTER_TYPE_UUID))));
			List<Encounter> orderEncountersNewChecksFromVitals = new ArrayList<>();
			
			if (!lastEncounterForPatientByDoctorsAndNurseConsultation.isEmpty()) {
				//loop through all and pick the ones with vitals recorded
				List<Encounter> doctorsNurseEncounterWithVitalsViaNewChecks = new ArrayList<>();
				for (Encounter encounter : lastEncounterForPatientByDoctorsAndNurseConsultation) {
					Set<Obs> obsSet = encounter.getAllObs();
					//check if this encounter has any of the vitals obs
					for (Obs obs : obsSet) {
						if (getVitalsConcepts().contains(obs.getConcept())) {
							//pick that encounter and exit immediately
							doctorsNurseEncounterWithVitalsViaNewChecks.add(encounter);
							break;
						}
					}
				}
				//order those encounter with vitals
				if (!doctorsNurseEncounterWithVitalsViaNewChecks.isEmpty()) {
					orderEncountersNewChecksFromVitals
					        .addAll(sortEncountersByEncounterDatetime(doctorsNurseEncounterWithVitalsViaNewChecks));
				}
			}
			
			for (Encounter encounter : orderEncountersNewChecksFromVitals) {
				Date date = encounter.getEncounterDatetime();
				dateRanges.add(DateUtils.getStringDate(date, "dd-MM-yyyy"));
				for (Obs obs : encounter.getAllObs()) {
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.WEIGHT)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						weights.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.TEMPERATURE)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						temperatures.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.BMI)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						bmis.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HEIGHT)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						heights.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.PULSE)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						pulse.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.SYSTOLIC_BP)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						systolic.add(val);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.DIASTOLIC_BP)) {
						VitalObject val = new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"),
						        obs.getValueNumeric());
						diastolic.add(val);
					}
				}
			}
		}
		
		simpleObject.put("weight", weights);
		simpleObject.put("temperature", temperatures);
		simpleObject.put("bmi", bmis);
		simpleObject.put("height", heights);
		simpleObject.put("pulse", pulse);
		simpleObject.put("diastolic", diastolic);
		simpleObject.put("systolic", systolic);
		simpleObject.put("dateRanges", dateRanges);
		
		return simpleObject;
	}
	
	static class VitalObject {
		
		public String yValue;
		
		public Double xValue;
		
		public VitalObject(String yValue, Double xValue) {
			this.yValue = yValue;
			this.xValue = xValue;
		}
	}
}
