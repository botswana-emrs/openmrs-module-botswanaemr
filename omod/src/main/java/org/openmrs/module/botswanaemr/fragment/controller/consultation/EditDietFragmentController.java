/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EditDietFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiUtils ui, UiSessionContext uiSessionContext) {
		
		model.addAttribute("visit", visit);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		EncounterService encounterService = Context.getEncounterService();
		List<Obs> dietGroupingObs = botswanaEmrService.getObservation(patient,
		    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.DIET_GROUPING_CONCEPT), uiSessionContext.getSessionLocation(),
		    limitFetch);
		Set<Obs> groupingObs = new HashSet<Obs>(dietGroupingObs);
		List<SimpleObject> dietObjects = BotswanaEmrUtils.getSimplifiedDietObjects(groupingObs);
		model.addAttribute("dietObjects", dietObjects);
	}
	
	/**
	 * Fragment Action for updating and saving new patient diet
	 */
	public void updateDiet(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<SimpleObject> deletedDiets = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedDiets = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newDiets = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		saveNewDiets(patient, newDiets, sessionContext.getSessionLocation(), visit);
		
		ObsService os = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		for (SimpleObject simpleObject : deletedDiets) {
			if (simpleObject.get("diet") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("diet"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
				
			}
			if (simpleObject.get("comment") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("comment"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("amount") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("amount"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
				
			}
		}
		
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_NOTES_CONCEPT);
		Concept amountConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_AMOUNT_CONCEPT);
		// Edit existing diets
		for (SimpleObject simpleObject : editedDiets) {
			
			if (simpleObject.get("dietUuid") != null && simpleObject.get("diet") != null) {
				Obs groupingObs = null;
				Obs obs = os.getObsByUuid((String) simpleObject.get("dietUuid"));
				if (!obs.getValueText().equals(simpleObject.get("diet"))) {
					obs.setValueText(String.valueOf(simpleObject.get("diet")));
					os.saveObs(obs, "Edit diet");
				}
				groupingObs = obs.getObsGroup();
				
				if (simpleObject.get("commentUuid") != null && simpleObject.get("comment") != null) {
					Obs commentObs = os.getObsByUuid((String) simpleObject.get("commentUuid"));
					
					if (commentObs != null) {
						if (!commentObs.getValueText().equals(simpleObject.get("comment"))) {
							commentObs.setValueText((String) simpleObject.get("comment"));
							os.saveObs(commentObs, "Edit diet notes");
						}
					} else {
						// New note
						Obs newObs = createNewObsFromGroup(groupingObs, notesConcept);
						if (newObs != null) {
							newObs.setValueText((String) simpleObject.get("comment"));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Diet note");
						}
					}
				}
				
				if (StringUtils.isNotEmpty((String) simpleObject.get("amountUuid"))
				        && StringUtils.isNotEmpty((String) simpleObject.get("amount"))) {
					Obs amountObs = os.getObsByUuid((String) simpleObject.get("amountUuid"));
					if (amountObs != null) {
						if (!amountObs.getValueNumeric().equals(Double.valueOf((String) simpleObject.get("amount")))) {
							amountObs.setValueNumeric(Double.valueOf((String) simpleObject.get("amount")));
							os.saveObs(amountObs, "Edit diet amount");
						}
					} else {
						// New note
						Obs newObs = createNewObsFromGroup(groupingObs, amountConcept);
						if (newObs != null) {
							newObs.setValueNumeric(Double.parseDouble((String) simpleObject.get("amount")));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Diet amount");
						}
					}
				}
			}
			
		}
	}
	
	private Obs createNewObsFromGroup(Obs groupingObs, Concept concept) {
		if (groupingObs != null) {
			return BotswanaEmrUtils.creatObs(groupingObs.getEncounter(), concept);
		}
		return null;
	}
	
	private void saveNewDiets(Patient patient, List<SimpleObject> newDiets, Location sessionLocation, Visit visit) {
		EncounterType vitalsEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		Encounter vitalEncounter = BotswanaEmrUtils.createEncounter(patient, vitalsEncounterType, sessionLocation, visit);
		
		ConceptService cs = Context.getConceptService();
		
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_GROUPING_CONCEPT);
		Concept dietConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_CONCEPT);
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_NOTES_CONCEPT);
		Concept amountConcept = cs.getConceptByUuid(BotswanaEmrConstants.DIET_AMOUNT_CONCEPT);
		
		for (SimpleObject simpleObject : newDiets) {
			Obs groupingObs = BotswanaEmrUtils.creatObs(vitalEncounter, groupingConcept);
			
			if (simpleObject.get("diet") != null) {
				Obs diet = BotswanaEmrUtils.creatObs(vitalEncounter, dietConcept);
				diet.setValueText((String) simpleObject.get("diet"));
				groupingObs.addGroupMember(diet);
				
				if (StringUtils.isNotEmpty(String.valueOf(simpleObject.get("comment")))) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, notesConcept);
					noteObs.setValueText((String) simpleObject.get("comment"));
					groupingObs.addGroupMember(noteObs);
				}
				
				if (StringUtils.isNotEmpty((String) simpleObject.get("amount"))) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, amountConcept);
					noteObs.setValueNumeric(Double.valueOf((String) simpleObject.get("amount")));
					groupingObs.addGroupMember(noteObs);
				}
				vitalEncounter.addObs(groupingObs);
			}
		}
		
		//Save an encounter
		if (vitalEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(vitalEncounter);
		}
	}
	
	EncounterSearchCriteria getEncounterSearchCriteria(Patient patient, List<EncounterType> types) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		
	}
}
