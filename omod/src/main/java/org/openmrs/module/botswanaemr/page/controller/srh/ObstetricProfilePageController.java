/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.openmrs.ConceptName;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.fragment.controller.SearchFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PARTOGRAPH_OBSERVATION_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.POST_PARTUM_OBSERVATION_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PUERPERIUM_FORM_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getSrhObstetricEncounterForms;

public class ObstetricProfilePageController {
	
	public final String PATIENT_TYPE_ON_VISIT_ATTRIBUTE_UUID = "2eed3db4-d910-4316-8498-e7e1bde4b119";
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui, final PageModel pageModel,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, UiSessionContext sessionContext) {
		
		boolean positionOfUterusObs = false;
		boolean bulkyConsistencyOfUterusObs = false;
		SearchFragmentController searchFragmentController = new SearchFragmentController();
		Visit currentPatientVisit = visit != null ? visit
		        : BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		
		boolean hasActiveVisit = currentPatientVisit != null;
		
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("patient", patient);
		
		if (currentPatientVisit != null) {
			pageModel.addAttribute("visitId", currentPatientVisit.getVisitId());
		} else {
			pageModel.addAttribute("visitId", "");
		}
		
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		
		Visit previousPatientVisit;
		
		Collection<VisitType> srhVisitTypes = new ArrayList<>();
		srhVisitTypes.add(Context.getVisitService().getVisitTypeByUuid(BotswanaEmrConstants.ANC_VISIT_TYPE_UUID));
		srhVisitTypes.add(Context.getVisitService().getVisitTypeByUuid(BotswanaEmrConstants.MATERNITY_VISIT_TYPE_UUID));
		
		List<Visit> visitList = Context.getVisitService().getVisits(srhVisitTypes, Collections.singletonList(patient), null,
		    null, null, currentPatientVisit.getStartDatetime(), null, null, null, false, false);
		previousPatientVisit = visitList.stream().filter(v -> v.getVisitId() != currentPatientVisit.getVisitId()).findFirst()
		        .orElse(null);
		
		pageModel.addAttribute("previousPatientVisit", previousPatientVisit);
		
		Obs srhPatientTypeObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.PATIENT_TYPE_CONCEPT_UUID, currentPatientVisit.getStartDatetime(), null);
		
		ConceptName srhPatientType = null;
		if (srhPatientTypeObs != null) {
			srhPatientType = srhPatientTypeObs.getValueCoded().getName();
		}
		
		pageModel.addAttribute("srhPatientType", srhPatientType != null ? srhPatientType.getName() : "");
		pageModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "srh/obstetricProfile", patient, currentPatientVisit, ui)));
		
		Form partographObservationForm = formService.getFormByUuid(PARTOGRAPH_OBSERVATION_FORM_UUID);
		Form puerperiumForm = formService.getFormByUuid(PUERPERIUM_FORM_UUID);
		Form postPartumObservationForm = formService.getFormByUuid(POST_PARTUM_OBSERVATION_FORM_UUID);
		
		List<HtmlForm> srhObstetricFormsEncounterForms = getSrhObstetricEncounterForms(htmlFormEntryService, formService,
		    srhPatientTypeObs != null && srhPatientTypeObs.getValueCoded() != null ? srhPatientTypeObs.getValueCoded()
		            : null);
		
		List<Encounter> filledEncounterForPatientPerTheCurrentVisit = getSrhObstetricEncounters(patient, encounterService,
		    currentPatientVisit, srhObstetricFormsEncounterForms);
		List<Encounter> filledEncounterForPatient = getSrhObstetricEncounters(patient, encounterService,
		    srhObstetricFormsEncounterForms);
		
		List<HtmlForm> tmpAvailableForms;
		if (filledEncounterForPatientPerTheCurrentVisit.isEmpty()) {
			tmpAvailableForms = srhObstetricFormsEncounterForms;
		} else {
			List<Form> filledObstetricForms = filledEncounterForPatientPerTheCurrentVisit.stream().map(Encounter::getForm)
			        .filter(form -> !form.equals(postPartumObservationForm))
			        .filter(form -> !form.equals(partographObservationForm) && !form.equals(puerperiumForm))
			        .collect(Collectors.toList());
			tmpAvailableForms = srhObstetricFormsEncounterForms.stream()
			        .filter(f -> !filledObstetricForms.contains(f.getForm())).collect(Collectors.toList());
		}
		
		Encounter srhPostPartumEncounter = searchFragmentController.latestEncounter(patient,
		    BotswanaEmrConstants.SRH_POST_PARTUM_ENCOUNTER_TYPE_UUID, sessionContext.getSessionLocation());
		
		if (srhPostPartumEncounter != null) {
			for (Obs srhPostPartumObs : srhPostPartumEncounter.getAllObs()) {
				if (srhPostPartumObs.getObsGroup() != null) {
					if (srhPostPartumObs.getObsGroup().getUuid().equals(BotswanaEmrConstants.MOTHER_GROUP_CONCEPT_UUID)) {
						if (srhPostPartumObs.getConcept().getUuid()
						        .equals(BotswanaEmrConstants.POSITION_OF_UTERUS_CONCEPT_UUID)
						        && srhPostPartumObs.getValueCoded().getUuid()
						                .equals(BotswanaEmrConstants.MID_LEVEL_CONCEPT_UUID)) {
							positionOfUterusObs = true;
						}
						if (srhPostPartumObs.getConcept().getUuid()
						        .equals(BotswanaEmrConstants.CONSTISTENCY_OF_UTERUS_CONCEPT_UUID)
						        && srhPostPartumObs.getValueCoded().getUuid()
						                .equals(BotswanaEmrConstants.BULKY_CONCEPT_UUID)) {
							bulkyConsistencyOfUterusObs = true;
						}
					}
				}
			}
		}
		
		EncounterType srhEnrolmentEnconterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.SRH_ENROLMENT_ENCOUNTER_TYPE_UUID);
		List<Encounter> srhEnrolmentEncounters = encounterService
		        .getEncounters(new EncounterSearchCriteria(patient, null, currentPatientVisit.getStartDatetime(), null, null,
		                null, Collections.singleton(srhEnrolmentEnconterType), null, null, null, false));
		if (!srhEnrolmentEncounters.isEmpty()) {
			pageModel.addAttribute("srhEnrolmentEncounter", srhEnrolmentEncounters.get(0));
		} else {
			pageModel.addAttribute("srhEnrolmentEncounter", null);
		}
		
		pageModel.addAttribute("positionOfUterusObs", positionOfUterusObs);
		pageModel.addAttribute("bulkyConsistencyOfUterusObs", bulkyConsistencyOfUterusObs);
		pageModel.addAttribute("obstetricProfileRiskFactors",
		    getObstetricProfileRiskFactors(patient, filledEncounterForPatient));
		pageModel.addAttribute("obstetricForms", tmpAvailableForms);
		pageModel.addAttribute("obstetricEncounters", filledEncounterForPatient);
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("visit", visit);
		
		List<String> ancForms = Arrays.asList(BotswanaEmrConstants.ANC_ANTENATAL_ENROLMENT_FORM_UUID,
		    BotswanaEmrConstants.ANC_ANTENATAL_HISTORY_FORM_UUID, BotswanaEmrConstants.ANC_NEW_RISKS_AFTER_BOOKING_FORM_UUID,
		    BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_WEIGHT_FORM_UUID);
		pageModel.addAttribute("ancForms", ancForms);
	}
	
	public static List<Encounter> getSrhObstetricEncounters(Patient patient, EncounterService encounterService,
	        Visit currentPatientVisit, List<HtmlForm> obstetricEncounterForms) {
		List<Form> obstetricForms = obstetricEncounterForms.stream().map(HtmlForm::getForm).collect(Collectors.toList());
		
		return encounterService
		        .getEncounters(new EncounterSearchCriteria(patient, null, null, null, null, obstetricForms, null, null, null,
		                currentPatientVisit == null ? null : Collections.singletonList(currentPatientVisit), false));
	}
	
	public static List<Encounter> getSrhObstetricEncounters(Patient patient, EncounterService encounterService,
	        List<HtmlForm> obstetricEncounterForms) {
		return getSrhObstetricEncounters(patient, encounterService, null, obstetricEncounterForms);
	}
	
	/**
	 * analyze the patient's obstetric profile and identify if the patient is at high risk if has the
	 * following risk factors HIV positive Has PV bleeding/Bleeding factors Elevated BP and abnormal
	 * vital signs Urinalisys results containing Protein and or Sugar HB Being 10 and Below MPS being
	 * Positive Hepatities being positive Syphilis being Positive
	 */
	public SimpleObject getObstetricProfileRiskFactors(Patient patient, List<Encounter> encounterList) {
		List<String> riskFactors = new ArrayList<>();
		SimpleObject so = new SimpleObject();
		if (BotswanaEmrUtils.isHivPositive(patient)) {
			riskFactors.add("HIV positive");
		}
		if (getPatientPvBleeding(patient, encounterList)) {
			riskFactors.add("PV bleeding/Bleeding Issues");
		}
		if (getPregnancyRiskStatus(patient, encounterList)) {
			riskFactors.add("High Risk Pregnancy");
		}
		if (getHivNegativeDueForRetesting(patient)) {
			riskFactors.add("Client Due for HIV Retesting");
		}
		if (getHivTestingDeclined(patient)) {
			riskFactors.add("Declined HIV Testing - Due for testing.");
		}
		
		so.put("riskFactors", riskFactors);
		
		return so;
	}
	
	public boolean getPatientPvBleeding(Patient patient, List<Encounter> encounterList) {
		boolean pvBstatus = false;
		//
		Obs pvBleedingObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), encounterList,
		    BotswanaEmrConstants.PV_BLEEDING_CONCEPT_UUID, patient.getDateCreated(), new Date());
		
		if (pvBleedingObs != null && pvBleedingObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.YES_CONCEPT_UUID))
			pvBstatus = true;
		
		return pvBstatus;
	}
	
	public boolean getPatientElevatedBp(Patient patient, List<Encounter> encounterList) {
		boolean elevatedBpStatus = false;
		
		return elevatedBpStatus;
	}
	
	public boolean getUrinalysisRiskStatus() {
		boolean riskStatus = false;
		
		return riskStatus;
	}
	
	public boolean getHbRiskStatus() {
		boolean hbRiskStatus = false;
		//flag HB Being 10 and Below
		
		return hbRiskStatus;
	}
	
	public boolean getMpsRiskStatus() {
		boolean mpsRiskStatus = false;
		//flag MPS being Positive
		
		return mpsRiskStatus;
	}
	
	public boolean getHepatitisRiskStatus() {
		boolean hepatitisRiskStatus = false;
		//flag Hepatities being positive
		
		return hepatitisRiskStatus;
	}
	
	public boolean getSyphilisRiskStatus() {
		boolean syphilisRiskStatus = false;
		//flag Syphilis being Positive
		
		return syphilisRiskStatus;
	}
	
	public boolean getPregnancyRiskStatus(Patient patient, List<Encounter> encounterList) {
		Obs primigravidaObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), encounterList,
		    BotswanaEmrConstants.PRIMIGRAVIDA_CONCEPT_UUID, patient.getDateCreated(), new Date());
		Obs heightObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), encounterList,
		    BotswanaEmrConstants.ANTENATAL_HEIGHT_CONCEPT_UUID, patient.getDateCreated(), new Date());
		boolean highRiskPregnancy = false;
		if (primigravidaObs != null && (patient.getAge() < 18 || patient.getAge() > 35) && heightObs != null) {
			if (primigravidaObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.YES_CONCEPT_UUID)
			        && heightObs.getValueNumeric() < 150) {
				highRiskPregnancy = true;
			}
		}
		
		return highRiskPregnancy;
	}
	
	/**
	 * A flag to Cater for those who declined to get tested for HIV as per the last test details
	 */
	public boolean getHivTestingDeclined(Patient patient) {
		boolean hivTestingDeclined = false;
		Obs hivTestAcceptanceObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.HIV_TEST_ACCEPTANCE_CONCEPT_UUID, patient.getDateCreated(), new Date());
		if (hivTestAcceptanceObs != null
		        && !hivTestAcceptanceObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.YES_CONCEPT_UUID)) {
			hivTestingDeclined = true;
		}
		
		return hivTestingDeclined;
	}
	
	/**
	 * HIV negative, the system should flag after 3 months.
	 */
	public boolean getHivNegativeDueForRetesting(Patient patient) {
		boolean dueForRetest = false;
		if (BotswanaEmrUtils.isHivPositive(patient)) {
			return false;
		} else {
			Obs lastHivTestObs = BotswanaEmrUtils.getPatientHivStatusObs(patient);
			if (lastHivTestObs != null) {
				Date lastHivTestDate = lastHivTestObs.getObsDatetime();
				Date currentDate = new Date();
				long diff = currentDate.getTime() - lastHivTestDate.getTime();
				long diffDays = diff / (24 * 60 * 60 * 1000);
				if (diffDays > 90) {
					dueForRetest = true;
				}
			}
			
		}
		return dueForRetest;
	}
	
}
