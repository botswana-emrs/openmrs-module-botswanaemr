/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import org.openmrs.Person;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.stereotype.Component;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonCreator;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

/**
 * Converts a person to a simple object
 */
@Component
public class BotswanaEmrPersonSimplifier extends BotswanaEmrAbstractSimplifier<Person> {
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	@Override
	protected SimpleObject simplify(Person person) {
		SimpleObject ret = new SimpleObject();
		
		ret.put("id", person.getId());
		ret.put("name", formatPersonName(person.getPersonName()));
		ret.put("gender", person.getGender().toLowerCase());
		ret.put("creator", formatPersonCreator(person));
		ret.put("duration", formatDurationSinceRegistration(person));
		
		return ret;
	}
}
