/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.openmrs.Concept;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ProgramFragmentController {
	
	public void controller(FragmentModel model) {
		model.addAttribute("programs", Context.getProgramWorkflowService().getAllPrograms());
	}
	
	public String post(@RequestParam(value = "patient", required = false) Patient patient,
	        @RequestParam(value = "discontinuationDate", required = false) String dateDiscontinued,
	        @RequestParam(value = "programOutcome", required = false) Concept outcome,
	        @RequestParam(value = "programDisco", required = false) Program program,
	        @RequestParam(value = "successUrl", required = false) String successUrl, UiUtils uiUtils) throws ParseException {
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		
		List<PatientProgram> enrolledProgramList = programWorkflowService.getPatientPrograms(patient, program, null, null,
		    null, null, false);
		if (enrolledProgramList.size() > 0 && org.apache.commons.lang3.StringUtils.isNotBlank(dateDiscontinued)
		        && outcome != null && program != null) {
			for (PatientProgram patientProgram : enrolledProgramList) {
				if (patientProgram.getActive()) {
					Date activeProgramEnrollmentDate = patientProgram.getDateEnrolled();
					Date datePickedFromTheUi = BotswanaEmrUtils.formatDateFromString(dateDiscontinued, "yyyy-MM-dd");
					if (activeProgramEnrollmentDate.compareTo(datePickedFromTheUi) > 0) {
						datePickedFromTheUi = new Date();
					}
					patientProgram.setDateCompleted(datePickedFromTheUi);
					patientProgram.setOutcome(outcome);
					programWorkflowService.savePatientProgram(patientProgram);
				}
			}
		}
		return null;
		// Map<String, Object> params = new HashMap<>();
		// params.put("patientId", patient.getUuid());
		// params.put("programId", program.getUuid());
		
		// return "redirect:" + uiUtils.pageLink("botswanaemr", "programs/programs", params);
	}
	
}
