/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.ItemStockSummary;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class CreateLabReturnsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null) {
			fragmentModel.addAttribute("sourceInventoryStockRooms",
			    StockUtils.getHtsDistributionStockrooms(uiSessionContext.getSessionLocation()));
			fragmentModel.addAttribute("destinationInventoryStockRooms",
			    StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation()));
			
		} else {
			fragmentModel.addAttribute("sourceInventoryStockRooms", null);
			fragmentModel.addAttribute("destinationInventoryStockRooms", null);
		}
		SimpleObject testKitNames = new SimpleObject();
		testKitNames.put("Determine", "c2ce43cc-0345-40c0-91b2-24155f458d4e");
		testKitNames.put("Bioline", "166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		testKitNames.put("KHB", "c53f44de-5df2-4810-b99b-7f75e80d1a05");
		testKitNames.put("ICT", "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		testKitNames.put("Unigold", "c2acb8a1-32f6-456a-b322-d31a1d478c01");
		testKitNames.put("Ist Response", "570a1f68-0bea-4980-ba9d-d0d7d21e4be9");
		testKitNames.put("Bio Kit", "c203885b-7bae-4318-8bb8-4aee96f973cd");
		testKitNames.put("Immunoflow", "7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f");
		fragmentModel.addAttribute("testKitNames", testKitNames);
	}
	
	public List<SimpleObject> getItemStockData(@RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "q", required = false, defaultValue = "") String searchPhrase,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) {
		
		Stockroom stockRoom = iStockroomDataService.getById(stockRoomUuid);
		List<ItemStock> iStockList;
		if (searchPhrase.trim().length() > 0) {
			ItemSearch search = new ItemSearch(new Item());
			search.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.LIKE);
			search.getTemplate().setName("%" + searchPhrase + "%");
			iStockList = iStockroomDataService.getItems(stockRoom, search, null);
		} else {
			iStockList = iStockroomDataService.getItemsByRoom(stockRoom, null);
		}
		
		List<SimpleObject> itemStockList = new ArrayList<>();
		for (ItemStock itemStock : iStockList) {
			List<ItemStockDetail> itemStockDetails = itemStock.getDetails().stream()
			        .filter(i -> !i.isNullBatch() && i.getExpiration() != null && i.getQuantity() > 0)
			        .collect(Collectors.toList());
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				SimpleObject simpleObject = StockUtils.extractItemStockToSimpleObject(itemStock, itemStockDetail);
				itemStockList.add(simpleObject);
			}
		}
		
		return itemStockList;
	}
	
	public SimpleObject saveItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = true) String stockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = true) Integer destinationStockRoomId)
	        throws JSONException, ParseException {
		
		Stockroom stockRoom = StockUtils.getStockroom(stockRoomId);
		Stockroom destinationStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		
		StockOperation operation = new StockOperation();
		operation.setStatus(StockOperationStatus.NEW);
		operation.setInstanceType(WellKnownOperationTypes.getTransfer());
		operation.setSource(stockRoom);
		operation.setDestination(destinationStockRoom);
		operation.setOperationNumber("LAB_RETURNS:" + UUID.randomUUID().toString());
		operation.setOperationDate(new Date());
		
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						String batchOperationId = ((JSONObject) jsonObject.get(key)).getString("batchOperationId");
						String returnReason = ((JSONObject) jsonObject.get(key)).getString("returnReason");
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date expiryDate = format.parse(expiryDate1);
						if (qty != itemStock.getQuantity()) {
							StockOperation batchOperation = BotswanaInventoryContext.getStockOperationDataService()
							        .getById(Integer.parseInt(batchOperationId));
							batchOperation.setDescription(returnReason);
							
							operation.addItem(itemStock.getItem(), qty, expiryDate, batchOperation);
							
						}
					}
				}
			}
		}
		
		BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		operation.setStatus(StockOperationStatus.COMPLETED);
		BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		simpleObject.put("response", operation.getId());
		return simpleObject;
		
	}
	
	public SimpleObject saveEditedItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam(value = "stockOperationId", required = false) Integer stockOperationId)
	        throws JSONException, ParseException {
		
		List<ItemStockSummary> itemStockSummaries = new ArrayList<>();
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						String batchOperationId = ((JSONObject) jsonObject.get(key)).getString("batchOperationId");
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date expiryDate = format.parse(expiryDate1);
						if (qty != itemStock.getQuantity()) {
							ItemStockSummary iss = new ItemStockSummary();
							iss.setExpiration(expiryDate);
							iss.setItem(itemStock.getItem());
							iss.setActualQuantity(qty);
							iss.setQuantity(qty);
							StockOperation batchOperation = BotswanaInventoryContext.getStockOperationDataService()
							        .getById(Integer.parseInt(batchOperationId));
							iss.setBatchOperation(batchOperation);
							
							itemStockSummaries.add(iss);
						}
					}
				}
			}
		}
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		
		StockOperation oldOperation = BotswanaInventoryContext.getStockOperationDataService().getById(stockOperationId);
		if (oldOperation != null) {
			oldOperation.setStatus(StockOperationStatus.CANCELLED);
			
			StockOperation operation = new StockOperation();
			operation.setStatus(StockOperationStatus.NEW);
			operation.setInstanceType(WellKnownOperationTypes.getAdjustment());
			operation.setSource(stockRoom);
			operation.setOperationNumber("STOCK_TAKE:" + UUID.randomUUID().toString());
			operation.setOperationDate(new Date());
			operation.setItems(createOperationsItemSet(operation, itemStockSummaries));
			
			BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		}
		SimpleObject simpleObject = new SimpleObject();
		
		return simpleObject;
		
	}
	
	private Set<StockOperationItem> createOperationsItemSet(StockOperation operation,
	        List<ItemStockSummary> inventoryStockTakeList) {
		Set<StockOperationItem> items = new HashSet<StockOperationItem>();
		for (ItemStockSummary inventoryItem : inventoryStockTakeList) {
			StockOperationItem item = new StockOperationItem();
			item.setOperation(operation);
			item.setItem(inventoryItem.getItem());
			item.setExpiration(inventoryItem.getExpiration());
			item.setCalculatedExpiration(false);
			item.setBatchOperation(inventoryItem.getBatchOperation());
			item.setQuantity(inventoryItem.getQuantity());
			item.setCalculatedBatch(true);
			items.add(item);
		}
		
		return items;
	}
	
}
