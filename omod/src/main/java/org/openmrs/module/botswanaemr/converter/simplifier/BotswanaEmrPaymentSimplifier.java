/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDateWithTime;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.stereotype.Component;

/**
 * Converts a {@link Registration} to a {@link SimpleObject}.
 */
@Component
public class BotswanaEmrPaymentSimplifier extends BotswanaEmrAbstractSimplifier<Registration> {
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	@Override
	protected SimpleObject simplify(Registration payment) {
		SimpleObject ret = new SimpleObject();
		
		ret.put("id", payment.getRegistrationId());
		ret.put("paymentDate", formatDateWithTime(payment.getRegistrationDatetime(), "dd/MM/yyyy HH:mm:ss"));
		ret.put("creator", formatPersonName(payment.getCreator().getPersonName()));
		ret.put("amountPaid", payment.getAmountPaid());
		ret.put("billableAmount", payment.getBillableAmount());
		
		return ret;
	}
}
