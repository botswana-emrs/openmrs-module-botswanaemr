/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationAttribute;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public class DirectStockFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		
	}
	
	public SimpleObject saveDirectReceipt(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "receiver", required = false) String receiver,
	        @RequestParam(value = "orderNo", required = false) String orderNo,
	        @RequestParam(value = "gen12No", required = false) String gen12No,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "receiptDate", required = false) String receiptDate,
	        @RequestParam(value = "mainStockroomsId", required = false) Integer mainStockroomsId,
	        @RequestParam(value = "institutionsId", required = false) Integer institutionsId,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		//System.out.println(receiver + "distance" + description + receiptDate + itemId + mainStockroomsId + institutionsId);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		SimpleObject simpleObject = new SimpleObject();
		try {
			JSONObject jsonObject = new JSONObject(itemId);
			Iterator<String> keys = jsonObject.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				if (jsonObject.get(key) instanceof JSONObject) {
					if (jsonObject.get(key) instanceof JSONObject) {
						Item item = BotswanaInventoryContext.getItemDataService()
						        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
						int qty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
						String batch = ((JSONObject) jsonObject.get(key)).getString("batch");
						String expiryString = ((JSONObject) jsonObject.get(key)).getString("expiry");
						int discrepancy = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("discrepancy"));
						String comment = ((JSONObject) jsonObject.get(key)).getString("comment");
						
						StockOperation stockOperation = new StockOperation();
						
						stockOperation.setDescription(description);
						
						stockOperation.setName(WellKnownOperationTypes.getReceipt().getName());
						
						// SimpleDateFormat newDate = new SimpleDateFormat("dd.MMM.yyyy");
						Date dateReceived = dateFormat.parse(receiptDate);
						stockOperation.setOperationDate(dateReceived);
						// Stockroom requestingDestStockRoom =
						// BotswanaInventoryContext.getStockRoomDataService().getById(institutionsId);
						Stockroom requestingSourceStockRoom = BotswanaInventoryContext.getStockRoomDataService()
						        .getById(mainStockroomsId);
						stockOperation.setDestination(requestingSourceStockRoom);
						stockOperation.setInstanceType(WellKnownOperationTypes.getReceipt());
						stockOperation.setInstitution(
						    BotswanaInventoryContext.getInstitutionDataService().getById(institutionsId));
						IStockOperationAttributeTypeDataService iStockOperationAttributeTypeDataService = BotswanaInventoryContext
						        .getStockOperationAttributeTypeDataService();
						
						StockOperationAttribute operationReceiver = new StockOperationAttribute();
						operationReceiver.setAttributeType(iStockOperationAttributeTypeDataService
						        .getByUuid(BotswanaEmrConstants.RECEIVER_NAME_ATTRIBUTE_TYPE_UUID));
						operationReceiver.setValue(receiver);
						operationReceiver.setOwner(stockOperation);
						
						StockOperationAttribute orderNoAttribute = new StockOperationAttribute();
						orderNoAttribute.setAttributeType(iStockOperationAttributeTypeDataService
						        .getByUuid(BotswanaEmrConstants.ORDER_NUMBER_ATTRIBUTE_TYPE_UUID));
						orderNoAttribute.setValue(orderNo);
						orderNoAttribute.setOwner(stockOperation);
						
						StockOperationAttribute gen12NoAttribute = new StockOperationAttribute();
						gen12NoAttribute.setAttributeType(iStockOperationAttributeTypeDataService
						        .getByUuid(BotswanaEmrConstants.GEN_12_NUMBER_ATTRIBUTE_TYPE_UUID));
						gen12NoAttribute.setValue(gen12No);
						gen12NoAttribute.setOwner(stockOperation);
						
						Set<StockOperationAttribute> stockOperationAttributes = new HashSet<>();
						
						stockOperationAttributes.add(operationReceiver);
						stockOperationAttributes.add(gen12NoAttribute);
						stockOperationAttributes.add(orderNoAttribute);
						
						stockOperation.setAttributes(stockOperationAttributes);
						
						StockOperation batchOperation = BotswanaInventoryContext.getStockOperationDataService()
						        .getOperationByNumber(batch);
						
						if (batchOperation == null) {
							stockOperation.setOperationNumber(batch);
							stockOperation.addItem(item, qty, dateFormat.parse(expiryString), qty, discrepancy, null,
							    comment, stockOperation);
						} else {
							stockOperation.setOperationNumber(UUID.randomUUID().toString());
							stockOperation.addItem(item, qty, dateFormat.parse(expiryString), qty, discrepancy, null,
							    comment, batchOperation);
						}
						
						stockOperation.setStatus(StockOperationStatus.NEW);
						StockOperation newStockOperation = BotswanaInventoryContext.getStockOperationService()
						        .submitOperation(stockOperation);
						
						newStockOperation.setStatus(StockOperationStatus.COMPLETED);
						BotswanaInventoryContext.getStockOperationService().submitOperation(newStockOperation);
						simpleObject.put("operationStatus", "Success");
					}
				}
			}
		}
		catch (Exception ex) {
			simpleObject.put("operationStatus", "Fail");
			throw new RuntimeException(ex);
		}
		
		return simpleObject;
		
	}
	
}
