/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.List;

import org.openmrs.api.LocationService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.SearchFragmentController;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.webservices.rest.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.ArrayList;

public class RequisitionItemReceiptPageController {
	
	public void controller(PageModel model, @RequestParam("id") StockOperation stockOperation,
	        @RequestParam(value = "requestType", required = false) String requestType,
	        @RequestParam(value = "actionType", required = false) String actionType,
	        @SpringBean("locationService") LocationService locationService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		
		List<Stockroom> stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(),
		    false);
		
		List<SimpleObject> operationItems = new ArrayList<>();
		// for each item in the stock operation, add available batch details in a simple object
		for (StockOperationItem item : stockOperation.getItems()) {
			Integer quantityOrdered = getQuantityOrdeded(stockOperation, item);
			SimpleObject operationItem = new SimpleObject();
			operationItem.put("item", item);
			operationItem.put("quantityOrdered", quantityOrdered);
			operationItem.put("batchDetails",
			    SearchFragmentController.getBatchDetails(stockOperation.getSource().getId(), item.getItem(), false));
			operationItems.add(operationItem);
		}
		
		model.addAttribute("stockrooms", stockrooms);
		model.addAttribute("items", operationItems);
		model.addAttribute("location", uiSessionContext.getSessionLocation());
		model.addAttribute("stockOperation", stockOperation);
		model.addAttribute("requestType", requestType);
		model.addAttribute("actionType", actionType);
		
	}
	
	private Integer getQuantityOrdeded(StockOperation stockOperation, StockOperationItem item) {
		// iterate through the parent stock operations and get the quantity ordered
		Integer quantityOrdered = 0;
		// given that each stock operation possibly has a parent stock operation, we need to recursively iterate through the parent stock operations until we get the root stock operation
		StockOperation parentStockOperation = stockOperation.getStockOperationParent();
		if (parentStockOperation != null) {
			return getQuantityOrdeded(parentStockOperation, item);
		} else {
			for (StockOperationItem stockOperationItem : stockOperation.getItems()) {
				if (stockOperationItem.getItem().equals(item.getItem())) {
					quantityOrdered = stockOperationItem.getQuantity();
					break;
				}
			}
		}
		
		return quantityOrdered;
	}
}
