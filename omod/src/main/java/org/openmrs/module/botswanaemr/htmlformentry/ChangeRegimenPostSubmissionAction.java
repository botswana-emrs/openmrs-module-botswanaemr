/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import java.util.Set;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntrySession;

public class ChangeRegimenPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		Patient patient = session.getPatient();
		PersonAttributeType patRegimen = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.ART_REGIMEN_ATTRIBUTE_TYPE_UUID);
		if (patRegimen != null) {
			PersonAttribute personAttribute = patient.getPerson().getAttribute(patRegimen);
			if (personAttribute == null) {
				personAttribute = new PersonAttribute();
				personAttribute.setAttributeType(patRegimen);
			}
			String currentRegimen = "";
			Set<Obs> obsList = session.getEncounter().getObs();
			Obs obs = obsList.stream()
			        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.REGIMEN_CONCEPT_UUID)).findFirst()
			        .orElse(null);
			if (obs != null) {
				currentRegimen = obs.getValueCoded().getUuid();
			}
			personAttribute.setValue(currentRegimen);
			patient.addAttribute(personAttribute);
			Context.getPatientService().savePatient(patient);
		}
		
	}
}
