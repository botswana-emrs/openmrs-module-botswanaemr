/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.patient;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Person;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.fragment.controller.SearchFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PatientHeaderFragmentController {
	
	public static final String NEGATIVE_DIAGNOSTIC = "c0226344-c12d-4448-a3b6-03b6d7a126a4";
	
	public void controller(UiUtils ui, @RequestParam("patientId") Patient patient, final PageModel model,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties,
	        @SpringBean("labService") LabService labService,
	        @FragmentParam(value = "widgets", defaultValue = "", required = false) String widgets,
	        UiSessionContext sessionContext) {
		List<String> allowedWidgets = new ArrayList<>();
		Location location = sessionContext.getSessionLocation();
		SearchFragmentController searchFragmentController = new SearchFragmentController();
		
		String hivStatus = "";
		if (patient != null) {
			Encounter encounter = searchFragmentController.latestEncounter(patient,
			    BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE, location);
			Encounter postOpEncounter = searchFragmentController.latestEncounter(patient,
			    BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE, location);
			
			for (String widget : widgets.split(",")) {
				switch (widget) {
					case "1":
						allowedWidgets.add("1");
						Obs hivStatusObs = BotswanaEmrUtils.getPatientHivStatusObs(patient);
						
						if (hivStatusObs != null) {
							Concept hivStatus1 = hivStatusObs.getValueCoded();
							String resultsExpiryStatus = "";
							if (hivStatus1.getUuid().equals(BotswanaEmrConstants.NEGATIVE_UUID)
							        || hivStatus1.getUuid().equals(NEGATIVE_DIAGNOSTIC)) {
								Date latestDate = new Date();
								Date threeMonthsAgo = DateUtils.addMonthsToDate(latestDate, -3);
								Date obsDate = hivStatusObs.getObsDatetime();
								if (obsDate != null && obsDate.before(threeMonthsAgo)) {
									resultsExpiryStatus = " (Expired)";
								}
								
							}
							hivStatus = hivStatus1.getName().getName() + resultsExpiryStatus;
						}
						break;
					case "2":
						
						if (encounter != null) {
							allowedWidgets.add("2");
							boolean res = getFirstStatus(encounter, postOpEncounter, patient);
							model.addAttribute("firstMissed", res);
						}
						break;
					case "3":
						if (encounter != null) {
							allowedWidgets.add("3");
							boolean res = getSecondStatus(encounter, postOpEncounter, patient);
							model.addAttribute("secondMissed", res);
						}
						break;
					case "4":
						if (encounter != null) {
							allowedWidgets.add("4");
							boolean res = getThirdStatus(encounter, postOpEncounter, patient);
							model.addAttribute("thirdMissed", res);
						}
						break;
					case "5":
						allowedWidgets.add("5");
						int count = BotswanaEmrUtils.getCompletedReviews(postOpEncounter, patient);
						model.addAttribute("completed", count);
						break;
				}
			}
			model.addAttribute("hivStatus", hivStatus);
			model.addAttribute("allowedWidgets", allowedWidgets);
		}
		
	}
	
	private boolean getFirstStatus(Encounter encounter, Encounter postOpEcounter, Patient patient) {
		int hours = DateUtils.getHoursInBetween(encounter.getEncounterDatetime(), new Date());
		if (postOpEcounter == null && hours >= 48) {
			return true;
		} else if (postOpEcounter != null) {
			if (hours < 48) {
				return false;
			}
			Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), Collections.singletonList(postOpEcounter),
			    BotswanaEmrConstants.SCM_REVIEW_DATE_CONCEPT_UUID, patient.getDateCreated(), new Date());
			return obs == null;
		}
		return false;
	}
	
	private boolean getSecondStatus(Encounter encounter, Encounter postOpEcounter, Patient patient) {
		int hours = DateUtils.getHoursInBetween(encounter.getEncounterDatetime(), new Date());
		if (postOpEcounter == null && hours >= 168) {
			return true;
		} else if (postOpEcounter != null) {
			if (hours < 168) {
				return false;
			}
			Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), Collections.singletonList(postOpEcounter),
			    BotswanaEmrConstants.SCM_REVIEW_DATE_CONCEPT_UUID, patient.getDateCreated(), new Date());
			
			if (obs == null) {
				return true;
			}
			
			return obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.REVIEW_DATE_1);
		}
		return false;
	}
	
	private boolean getThirdStatus(Encounter encounter, Encounter postOpEcounter, Patient patient) {
		int hours = DateUtils.getHoursInBetween(encounter.getEncounterDatetime(), new Date());
		if (postOpEcounter == null && hours >= 1008) {
			return true;
		} else if (postOpEcounter != null) {
			if (hours < 1008) {
				return false;
			}
			
			Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), Collections.singletonList(postOpEcounter),
			    BotswanaEmrConstants.SCM_REVIEW_DATE_CONCEPT_UUID, patient.getDateCreated(), new Date());
			
			if (obs == null) {
				return true;
			}
			
			return obs.getValueCoded().getUuid().equals(BotswanaEmrConstants.REVIEW_DATE_3);
		}
		return false;
	}
	
	private List<Obs> allObs(Concept concept, Date latestDate, Patient patient) {
		if (patient == null || concept == null) {
			return new ArrayList<Obs>();
		} else {
			List<Person> who = new ArrayList<Person>();
			who.add(patient.getPerson());
			
			List<Concept> questions = new ArrayList<Concept>();
			questions.add(concept);
			
			return Context.getObsService().getObservations(who, null, questions, null, null, null, null, (Integer) null,
			    (Integer) null, (Date) null, latestDate, false);
		}
	}
	
}
