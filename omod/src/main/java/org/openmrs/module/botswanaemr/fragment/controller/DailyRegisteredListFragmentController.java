/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.mapper.BotswanaPatientQueueMapper;
import org.openmrs.module.botswanaemr.model.SimplifiedPatient;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailyRegisteredListFragmentController {
	
	protected EncounterService encounterService;
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	public void controller(FragmentModel model, UiSessionContext sessionContext, UiUtils ui) {
		Location sessionLocation = sessionContext.getSessionLocation();
		
		Date today = DateUtil.getStartOfDay(new Date());
		List<Registration> registered = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(today,
		    today, sessionLocation, null);
		model.addAttribute("todayRegisteredPatientsSummary", getSimplifiedPatientRegistrationDetails(registered, ui));
		
	}
	
	protected List<SimplifiedPatient> getSimplifiedPatients(List<Patient> patientList) {
		List<SimplifiedPatient> allPatients = new ArrayList<SimplifiedPatient>();
		SimplifiedPatient simplifiedPatient = null;
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		
		if (patientList != null) {
			for (Patient patient : patientList) {
				simplifiedPatient = new SimplifiedPatient();
				simplifiedPatient.setIdentifier(
				    patient.getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier()); // BotswanaEmrUtils.formatPatientIdentifier(patient));
				simplifiedPatient.setPatientId(patient.getPatientId());
				simplifiedPatient.setName(BotswanaEmrUtils.formatPersonName(patient.getPersonName()));
				simplifiedPatient.setGender(patient.getPerson().getGender());
				simplifiedPatient.setAge(patient.getAge() == null ? "" : patient.getAge().toString());
				simplifiedPatient.setRegisteredBy(BotswanaEmrUtils.formatPersonCreator(patient.getPerson()));
				simplifiedPatient.setRegisteredDate(
				    BotswanaEmrUtils.formatDateWithTime(patient.getDateCreated(), "dd/MM/yyyy HH:mm:ss"));
				simplifiedPatient.setDateOfBirth(patient.getBirthdate() == null ? ""
				        : BotswanaEmrUtils.formatDateWithoutTime(patient.getBirthdate(), "dd-MM-yyyy"));
				encounterService = Context.getEncounterService();
				List<Encounter> consultationEncounterList = BotswanaEmrUtils.getConsultationPatientEncounters(patient,
				    limitFetch);
				Visit lastPatientConsultationVisit;
				if (!consultationEncounterList.isEmpty()) {
					lastPatientConsultationVisit = BotswanaEmrUtils.getConsultationPatientEncounters(patient, 1).get(0)
					        .getVisit();
				} else {
					lastPatientConsultationVisit = BotswanaEmrUtils.getLastPatientVisit(patient, null);
				}
				
				String visitNumber = lastPatientConsultationVisit != null
				        ? lastPatientConsultationVisit.getVisitId().toString()
				        : "";
				simplifiedPatient.setVisitNumber(visitNumber);
				if (patient.getDead()) {
					simplifiedPatient.setStatus("Expired");
				} else {
					simplifiedPatient.setStatus("Active");
				}
				allPatients.add(simplifiedPatient);
			}
		}
		return allPatients;
		
	}
	
	protected List<SimplifiedPatient> getSimplifiedPatientRegistrationDetails(List<Registration> registrationList,
	        UiUtils ui) {
		List<SimplifiedPatient> allPatients = new ArrayList<SimplifiedPatient>();
		SimplifiedPatient simplifiedPatient = null;
		
		if (registrationList != null) {
			for (Registration payment : registrationList) {
				simplifiedPatient = new SimplifiedPatient();
				simplifiedPatient.setIdentifier(payment.getPatient()
				        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier()); // BotswanaEmrUtils.formatPatientIdentifier(patient));
				simplifiedPatient.setPatientId(payment.getPatient().getPatientId());
				simplifiedPatient.setPatientUuid(payment.getPatient().getUuid());
				simplifiedPatient.setName(BotswanaEmrUtils.formatPersonName(payment.getPatient().getPersonName()));
				simplifiedPatient.setGender(payment.getPatient().getPerson().getGender());
				simplifiedPatient
				        .setAge(payment.getPatient().getAge() == null ? "" : payment.getPatient().getAge().toString());
				simplifiedPatient.setRegisteredBy(BotswanaEmrUtils.formatPerson(payment.getCreator().getPerson()));
				simplifiedPatient.setRegisteredDate(ui.formatDatetimePretty(payment.getDateCreated()));
				simplifiedPatient.setDateOfBirth(payment.getPatient().getBirthdate() == null ? ""
				        : BotswanaEmrUtils.formatDateWithoutTime(payment.getPatient().getBirthdate(), "dd-MM-yyyy"));
				if (payment.getPatient().getDead()) {
					simplifiedPatient.setStatus("Expired");
				} else {
					simplifiedPatient.setStatus("Active");
				}
				
				//most recent queueing location
				PatientQueue queue = patientQueueingService.getMostRecentQueue(payment.getPatient());
				BotswanaPatientQueueMapper patientQueueMapper = new BotswanaPatientQueueMapper();
				
				if (queue != null) {
					Location currentQueueLocation = queue.getQueueRoom();
					
					//QueueId:
					simplifiedPatient.setId(queue.getPatientQueueId());
					
					//assignedTo
					simplifiedPatient.setAssignedTo(currentQueueLocation.toString());
					patientQueueMapper.setRecentQueueLocation(currentQueueLocation.toString());
					
				} else {
					simplifiedPatient.setAssignedTo("");
					patientQueueMapper.setRecentQueueLocation("");
				}
				
				//visitNumber
				String visitNumber = BotswanaEmrUtils
				        .getPatientActiveVisit(payment.getPatient(), payment.getLocation(), true).getVisitId().toString();
				simplifiedPatient.setVisitNumber(visitNumber);
				allPatients.add(simplifiedPatient);
			}
		}
		return allPatients;
		
	}
}
