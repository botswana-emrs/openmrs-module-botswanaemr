/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.*;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;

public class FacilitiesFragmentController extends AddMflFacilitiesFragmentController {
	
	public void controller(FragmentModel model) {
		
		LocationService locationService = Context.getLocationService();
		List<Location> allLocations = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		model.addAttribute("locationList", allLocations);
		
		List<Location> childLocations = new ArrayList<>();
		List<SimpleObject> parentLocations = new ArrayList<>();
		LocationTag locationTag = locationService.getLocationTagByName(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME);
		
		for (Location location : allLocations) {
			if (location.getParentLocation() != null) {
				childLocations.add(location);
			} else {
				SimpleObject so = new SimpleObject();
				so.put("id", location.getUuid());
				so.put("facilityMfl", getAttributeValueReference(location, BotswanaEmrConstants.MASTER_FACILITY_CODE_UUID));
				so.put("facilityName", location.getName());
				so.put("status", getAttributeValueReference(location, BotswanaEmrConstants.MASTER_FACILITY_STATUS_UUID));
				so.put("dateCreated", location.getDateCreated());
				so.put("isBdf", location.getTags().contains(locationTag));
				
				parentLocations.add(so);
			}
		}
		
		model.addAttribute("childLocations", childLocations);
		model.addAttribute("parentLocationsz", parentLocations);
		model.addAttribute("users", new ArrayList<User>());
		
	}
	
	public SimpleObject getFacilityUsers(@RequestParam(value = "facilityLocationUuid") String facilityLocationUuid,
	        @RequestParam(value = "facilityLocationMFlCode") String facilityLocationMFlCode,
	        @SpringBean("locationService") LocationService ls) {
		
		/**
		 * get a list of all system users and mark those that are admins for the passed facility
		 */
		List<User> allSystemUsers = new ArrayList<>(Context.getUserService().getAllUsers());
		List<SimpleObject> userObjects = new ArrayList<>();
		SimpleObject facilitySimpleObject = new SimpleObject();
		
		/* List<Location> matchingLocations = getLocationsWithMFLCode(ls,facilityLocationMFlCode);
		if (!matchingLocations.isEmpty()) {
		    location = matchingLocations.get(0);
		    facilitySimpleObject.put("newFacilityCode", facilityLocationMFlCode);
		    facilitySimpleObject.put("facilityName", location.getName());
		}else {
		    System.out.println("Location is null"+ facilityLocationMFlCode);
		}*/
		Location location = ls.getLocationByUuid(facilityLocationUuid);
		LocationTag locationTag = ls.getLocationTagByName(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME);
		if (location != null) {
			facilitySimpleObject.put("newFacilityCode", facilityLocationMFlCode);
			facilitySimpleObject.put("facilityName", location.getName());
			facilitySimpleObject.put("facilityLocationUuid", facilityLocationUuid);
			facilitySimpleObject.put("isBdf", location.getTags().contains(locationTag));
			String facilityAdmins = getAttributeValueReference(location, BotswanaEmrConstants.FACILITY_ADMINS_UUID);
			
			if (facilityAdmins != null) {
				List<String> facilityAdminUuids = Arrays.asList(facilityAdmins.split(","));
				for (User user : allSystemUsers) {
					SimpleObject adminObject = new SimpleObject();
					adminObject.put("value", user.getUuid());
					adminObject.put("label", user.getNames() + " " + (user.getEmail() != null ? user.getEmail() : ""));
					if (facilityAdminUuids.contains(user.getUuid())) {
						adminObject.put("selected", true);
					} else {
						adminObject.put("selected", false);
					}
					userObjects.add(adminObject);
				}
			} else {
				//populate the dropdown with user options
				for (User user : allSystemUsers) {
					SimpleObject adminObject = new SimpleObject();
					adminObject.put("value", user.getUuid());
					adminObject.put("label", user.getNames() + " " + (user.getEmail() != null ? user.getEmail() : ""));
					adminObject.put("selected", false);
					userObjects.add(adminObject);
				}
			}
			facilitySimpleObject.put("userObjects", userObjects);
		}
		
		return facilitySimpleObject;
	}
	
	public String post(@RequestParam(value = "facilityLocationUuid", required = false) String facilityLocationUuid,
	        @RequestParam(value = "isBdf", defaultValue = "false") String isBdf, String uuid,
	        @SpringBean("locationService") LocationService ls,
	        @RequestParam(value = "facilityAdmins", required = false) String facilityAdmins) {
		boolean bdfFacility = isBdf.equalsIgnoreCase("on");
		Location location = ls.getLocationByUuid(facilityLocationUuid);
		LocationTag locationTag = ls.getLocationTagByName(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME);
		if (location != null) {
			String attrTypeUuid = BotswanaEmrConstants.FACILITY_ADMINS_UUID;
			LocationAttributeType facilityAdminAttrType = ls.getLocationAttributeTypeByUuid(attrTypeUuid);
			if (getAttributeValueReference(location, attrTypeUuid) == null) {
				setAsAttribute(location, facilityAdminAttrType, facilityAdmins);
			} else {
				LocationAttribute facilityAttr = location.getAttributes().stream()
				        .filter(f -> f.getAttributeType().equals(facilityAdminAttrType)).findFirst().orElse(null);
				setAsAttributeVal(location, facilityAttr, facilityAdmins);
			}
			
			if (location.getTags().contains(locationTag) && !bdfFacility) {
				location.removeTag(locationTag);
			} else if (bdfFacility) {
				location.addTag(locationTag);
			}
			
			try {
				Context.getLocationService().saveLocation(location);
				//System.out.println("Saved successfully");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return null;
	}
	
	public void retireFacility(@RequestParam(value = "facilityLocationUuid") String facilityLocationUuid,
	        @SpringBean("locationService") LocationService ls, @RequestParam(value = "retireReason") String retireReason) {
		Location location = ls.getLocationByUuid(facilityLocationUuid);
		if (location != null) {
			try {
				Context.getLocationService().retireLocation(location, retireReason);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static String getAttributeValueReference(Location location, String attrTypeUuid) {
		LocationAttributeType attrType = Context.getLocationService().getLocationAttributeTypeByUuid(attrTypeUuid);
		List<LocationAttribute> attributes = location.getAttributes().stream()
		        .filter(attr -> attr.getAttributeType().equals(attrType)).collect(Collectors.toList());
		if (!attributes.isEmpty()) {
			return attributes.stream().findFirst().get().getValueReference();
		} else {
			return null;
		}
	}
	
	protected void setAsAttributeVal(Location location, LocationAttribute attr, String value) {
		if (StringUtils.isNotBlank(value)) {
			attr.setValue(value.trim());
			location.setAttribute(attr);
		}
	}
}
