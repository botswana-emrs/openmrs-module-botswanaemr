/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.DrugOrder;
import org.openmrs.Order;
import org.openmrs.OrderFrequency;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.web.controller.reponse.GenericResponse;
import org.openmrs.module.botswanaemr.web.controller.request.DrugOrderUpdateRequest;
import org.openmrs.module.webservices.rest.SimpleObject;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.RestConstants;
import org.openmrs.module.webservices.rest.web.representation.Representation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;
import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@Controller
@RequestMapping(value = "/rest/" + RestConstants.VERSION_1 + "/" + BotswanaEmrConstants.BOTSWANAEMR_MODULE_ID)
public class DrugOrderRestController {
	
	final ObjectMapper mapper = new ObjectMapper();
	
	@RequestMapping(value = "/drugOrder/{orderUuid}", method = RequestMethod.POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	@ResponseBody
	public GenericResponse updateDrugOrder(@PathVariable("orderUuid") @NotNull String orderUuid, @RequestBody String payload)
	        throws IOException {
		//TODO: Execute Substitute drug process - Check if drug has changed and run required logic
		OrderService orderService = Context.getOrderService();
		Order order = orderService.getOrderByUuid(orderUuid);
		
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		DrugOrderUpdateRequest updateRequest = mapper.readValue(payload, DrugOrderUpdateRequest.class);
		
		DrugOrder drugOrderToRevise = null;
		if (order instanceof DrugOrder) {
			drugOrderToRevise = ((DrugOrder) order).cloneForRevision();
			
			//Retain the same encounter/orderer(probably should get logged-in user) for now
			drugOrderToRevise.setOrderer(order.getOrderer());
			drugOrderToRevise.setEncounter(order.getEncounter());
			
			Concept route = getConcept(updateRequest.getRoute());
			if (route != null) {
				drugOrderToRevise.setRoute(route);
			}
			
			drugOrderToRevise.setDose(updateRequest.getDosage());
			
			Concept doseUnits = getConcept(updateRequest.getDoseUnits());
			if (doseUnits != null) {
				drugOrderToRevise.setDoseUnits(doseUnits);
			}
			
			Concept durationUnits = getConcept(updateRequest.getDurationUnits());
			if (durationUnits != null) {
				drugOrderToRevise.setDurationUnits(durationUnits);
			}
			
			Drug drug = getDrug(updateRequest.getDrugUuid());
			if (drug != null) {
				drugOrderToRevise.setDrug(drug);
			}
			
			drugOrderToRevise.setNumRefills(updateRequest.getNumRefills());
			drugOrderToRevise.setDuration(updateRequest.getDuration());
			
			OrderFrequency frequency = getFrequency(updateRequest.getFrequency());
			if (frequency != null) {
				drugOrderToRevise.setFrequency(frequency);
			}
			
			drugOrderToRevise.setCommentToFulfiller(updateRequest.getComments());
			
			drugOrderToRevise.setInstructions(updateRequest.getInstructions());
		}
		
		GenericResponse genericResponse = new GenericResponse();
		try {
			Order reviseOrder = orderService.saveOrder(drugOrderToRevise, null);
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.add("revisedOrder", ConversionUtil.convertToRepresentation(reviseOrder, Representation.REF));
			genericResponse.setResponse(simpleObject);
			genericResponse.setStatus(HttpStatus.CREATED.toString());
		}
		catch (Exception exception) {
			genericResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			genericResponse.setResponse("Unable to modified prescription " + exception.getMessage());
			log.error("Error updating prescription", exception);
		}
		
		return genericResponse;
	}
	
	private Concept getConcept(@NotNull String uuid) {
		return Context.getConceptService().getConceptByUuid(uuid);
	}
	
	private Drug getDrug(@NotNull String uuid) {
		return Context.getConceptService().getDrugByUuid(uuid);
	}
	
	private OrderFrequency getFrequency(@NotNull String uuid) {
		return Context.getOrderService().getOrderFrequencyByUuid(uuid);
	}
}
