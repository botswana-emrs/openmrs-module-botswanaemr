/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock.configuration;

import java.text.ParseException;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IInstitutionDataService;
import org.openmrs.module.botswanaemrInventory.model.Institution;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

@Slf4j
public class AddInstitutionFormFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		
	}
	
	public void saveCreatedInstitution(@RequestParam("name") String name, @RequestParam("description") String description,
	        UiSessionContext uiSessionContext) {
		
		IInstitutionDataService iInstitutionDataService = BotswanaInventoryContext.getInstitutionDataService();
		
		Institution institution = new Institution();
		
		institution.setName(name);
		institution.setDescription(description);
		institution.setDateCreated(new Date());
		institution.setCreator(Context.getAuthenticatedUser());
		
		try {
			Institution newlyCreatedInstitution = iInstitutionDataService.save(institution);
			
			//Add the attribute
			
			log.info("\nSaved new Institution\n" + institution);
		}
		catch (Exception e) {
			log.warn("\nAn error occurred while saving stock room\n", e);
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
			        "An error occurred while adding new stock room: " + e.getMessage());
		}
	}
	
	public void saveEditedInstitution(@RequestParam(value = "id", required = false) Integer institutionId,
	        @RequestParam("name") String name, @RequestParam("description") String description,
	        @SpringBean("bemrs.institutionDataService") IInstitutionDataService iInstitutionDataService)
	        throws ParseException {
		
		Institution institution;
		
		if (institutionId != null) {
			institution = iInstitutionDataService.getById(institutionId);
			
			institution.setName(name);
			institution.setDescription(description);
			
			try {
				
				institution = iInstitutionDataService.save(institution);
				
				log.info("\nSaved Stock room\n" + institution);
			}
			catch (Exception e) {
				log.warn("\nAn error occurred while saving stock room\n", e);
				throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
				        "An error occurred while saving stock room: " + e.getMessage());
			}
		}
	}
	
}
