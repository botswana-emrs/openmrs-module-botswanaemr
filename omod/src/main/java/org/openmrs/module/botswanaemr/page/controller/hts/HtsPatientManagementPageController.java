/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_LOCATION_TAG_NAME;

import java.util.Date;
import java.util.List;

import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.DailyRegisteredListFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;

public class HtsPatientManagementPageController extends DailyRegisteredListFragmentController {
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext, UiUtils ui) {
		Date today = DateUtil.getStartOfDay(new Date());
		String sessionLocationName = "";
		if (sessionContext.getSessionLocation() != null) {
			sessionLocationName = sessionContext.getSessionLocation().getName();
		}
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		pageModel.addAttribute("sessionLocation", sessionLocationName);
//		pageModel.addAttribute("pastRegisteredPatientsSummary",
//		    getSimplifiedPatientRegistrationDetails(Context.getService(BotswanaEmrService.class)
//		            .getPatientsRegisteredOnDate(null, today, sessionContext.getSessionLocation(), limitFetch),
//		        ui));
		
		pageModel.addAttribute("ageCategories", BotswanaEmrUtils.getDefaultAgeCategories());
		
		List<Location> testingPoints = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName(HTS_LOCATION_TAG_NAME));
		
		pageModel.addAttribute("testingPoints", testingPoints);
		
	}
	
}
