/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.creatObs;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getConcept;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class NewChecksFragmentController {
	
	public void get(FragmentModel model, @RequestParam(value = "encounterToEdit", required = false) String encounterToEdit) {
	}
	
	public void captureVitals(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "patientQueueId", required = false) PatientQueue patientQueue,
	        @RequestParam(value = "temperature", required = false) String temperature,
	        @RequestParam(value = "sbp", required = false) String sbp,
	        @RequestParam(value = "dbp", required = false) String dbp,
	        @RequestParam(value = "weight", required = false) String weight,
	        @RequestParam(value = "height", required = false) String height,
	        @RequestParam(value = "bmi", required = false) Double bmi,
	        @RequestParam(value = "bsa", required = false) Double bsa,
	        @RequestParam(value = "rRate", required = false) String rRate,
	        @RequestParam(value = "pulse", required = false) String pulse,
	        @RequestParam(value = "hCircumference", required = false) String hCircumference,
	        @RequestParam(value = "glucoseLevel", required = false) Double glucoseLevel,
	        @RequestParam(value = "oxygenSaturation", required = false) Double oxygenSaturation,
	        @RequestParam(value = "lmnp", required = false) String lmnp,
	        @RequestParam(value = "pregnancyKnown", required = false) Integer pregnancyKnown,
	        @RequestParam(value = "conscious", required = false) Integer conscious,
	        @RequestParam(value = "responsive", required = false) Integer responsive,
	        @RequestParam(value = "encounterDate", required = false) Date encounterDate,
	        @RequestParam(value = "vitals", required = false) Boolean vitals, UiSessionContext sessionContext) {
		
		String currentServiceDeliveryPointAttribute = String
		        .valueOf(sessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location servicePointVisited = Context.getLocationService().getLocationByUuid(currentServiceDeliveryPointAttribute);
		EncounterType encounterType = null;
		Encounter doctorsConsultationEncounter = null;
		if (vitals == true) {
			encounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.VITALS_ENCOUNTER_TYPE_UUID);
			doctorsConsultationEncounter = BotswanaEmrUtils.createEncounter(patient, encounterType,
			    sessionContext.getSessionLocation(), null);
		} else {
			if (servicePointVisited.getUuid().equals(BotswanaEmrConstants.NURSING_PORTAL_UUID)) {
				encounterType = Context.getEncounterService()
				        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
				doctorsConsultationEncounter = BotswanaEmrUtils.createEncounter(patient, encounterType,
				    sessionContext.getSessionLocation(), null);
			} else {
				encounterType = Context.getEncounterService()
				        .getEncounterTypeByUuid(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE);
				doctorsConsultationEncounter = BotswanaEmrUtils.createEncounter(patient, encounterType,
				    sessionContext.getSessionLocation(), null);
			}
		}
		
		if (encounterDate != null) {
			doctorsConsultationEncounter.setEncounterDatetime(encounterDate);
		}
		
		if (StringUtils.isNotBlank(temperature)) {
			Obs tempObs = creatObs(doctorsConsultationEncounter, getConcept(TEMPERATURE));
			tempObs.setValueNumeric(Double.valueOf(temperature));
			doctorsConsultationEncounter.addObs(tempObs);
		}
		if (StringUtils.isNotBlank(sbp)) {
			Obs sbpObs = creatObs(doctorsConsultationEncounter, getConcept(SYSTOLIC_BP));
			sbpObs.setValueNumeric(Double.valueOf(sbp));
			doctorsConsultationEncounter.addObs(sbpObs);
		}
		if (StringUtils.isNotBlank(dbp)) {
			Obs dbpObs = creatObs(doctorsConsultationEncounter, getConcept(DIASTOLIC_BP));
			dbpObs.setValueNumeric(Double.valueOf(dbp));
			doctorsConsultationEncounter.addObs(dbpObs);
		}
		if (StringUtils.isNotBlank(weight)) {
			Obs weightObs = creatObs(doctorsConsultationEncounter, getConcept(WEIGHT_CONCEPT_UUID));
			weightObs.setValueNumeric(Double.valueOf(weight));
			doctorsConsultationEncounter.addObs(weightObs);
		}
		if (StringUtils.isNotBlank(height)) {
			Obs HeightObs = creatObs(doctorsConsultationEncounter, getConcept(HEIGHT));
			HeightObs.setValueNumeric(Double.valueOf(height));
			doctorsConsultationEncounter.addObs(HeightObs);
		}
		if (bmi != null) {
			Obs bmiObs = creatObs(doctorsConsultationEncounter, getConcept(BMI));
			bmiObs.setValueNumeric(bmi);
			doctorsConsultationEncounter.addObs(bmiObs);
		}
		if (bsa != null) {
			Obs bsaObs = creatObs(doctorsConsultationEncounter, getConcept(BSA));
			bsaObs.setValueNumeric(bsa);
			doctorsConsultationEncounter.addObs(bsaObs);
		}
		if (StringUtils.isNotBlank(rRate)) {
			Obs rRateObs = creatObs(doctorsConsultationEncounter, getConcept(RESPIRATORY_RATE));
			rRateObs.setValueNumeric(Double.valueOf(rRate));
			doctorsConsultationEncounter.addObs(rRateObs);
		}
		if (StringUtils.isNotBlank(pulse)) {
			Obs pulseObs = creatObs(doctorsConsultationEncounter, getConcept(PULSE));
			pulseObs.setValueNumeric(Double.valueOf(pulse));
			doctorsConsultationEncounter.addObs(pulseObs);
		}
		
		if (StringUtils.isNotBlank(hCircumference)) {
			Obs hCircumferenceObs = creatObs(doctorsConsultationEncounter, getConcept(HEAD_CIRCUMFERENCE));
			hCircumferenceObs.setValueNumeric(Double.valueOf(hCircumference));
			doctorsConsultationEncounter.addObs(hCircumferenceObs);
		}
		if (glucoseLevel != null) {
			Obs glucoseLevelObs = creatObs(doctorsConsultationEncounter, getConcept(GLUCOSE_LEVEL));
			glucoseLevelObs.setValueNumeric(glucoseLevel);
			doctorsConsultationEncounter.addObs(glucoseLevelObs);
		}
		if (oxygenSaturation != null) {
			Obs oxygenoBS = creatObs(doctorsConsultationEncounter, getConcept(OXYGEN_SATURATION));
			oxygenoBS.setValueNumeric(oxygenSaturation);
			doctorsConsultationEncounter.addObs(oxygenoBS);
		}
		if (StringUtils.isNotBlank(lmnp)) {
			Obs lmnpObs = creatObs(doctorsConsultationEncounter, getConcept(LMP));
			lmnpObs.setValueText(lmnp);
			doctorsConsultationEncounter.addObs(lmnpObs);
		}
		if (pregnancyKnown != null) {
			Concept concept = Context.getConceptService().getConcept(pregnancyKnown);
			Obs pregnancyObs = creatObs(doctorsConsultationEncounter, getConcept(PREGNANCY_STATUS));
			pregnancyObs.setValueCoded(concept);
			doctorsConsultationEncounter.addObs(pregnancyObs);
		}
		if (conscious != null) {
			Concept concept = Context.getConceptService().getConcept(conscious);
			Obs concoousObs = creatObs(doctorsConsultationEncounter, getConcept(CONSCIOUS));
			concoousObs.setValueCoded(concept);
			doctorsConsultationEncounter.addObs(concoousObs);
		}
		if (responsive != null) {
			Concept concept = Context.getConceptService().getConcept(conscious);
			Obs responsiveObs = creatObs(doctorsConsultationEncounter, getConcept(RESPONSIVE));
			responsiveObs.setValueCoded(concept);
			doctorsConsultationEncounter.addObs(responsiveObs);
		}
		
		//Save an encounter
		if (doctorsConsultationEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(doctorsConsultationEncounter);
		}
		
	}
	
	public void editCapturedVitals(@RequestParam(value = "etemperature", required = false) String temperature,
	        @RequestParam(value = "esbp", required = false) String sbp,
	        @RequestParam(value = "edbp", required = false) String dbp,
	        @RequestParam(value = "eweight", required = false) String weight,
	        @RequestParam(value = "eheight", required = false) String height,
	        @RequestParam(value = "ebmi", required = false) Double bmi,
	        @RequestParam(value = "ebsa", required = false) Double bsa,
	        @RequestParam(value = "erRate", required = false) String rRate,
	        @RequestParam(value = "epulse", required = false) String pulse,
	        @RequestParam(value = "ehCircumference", required = false) String hCircumference,
	        @RequestParam(value = "eGlucoseLevel", required = false) String hGlucoseLevel,
	        @RequestParam(value = "eOxygenSaturation", required = false) String hOxygenSaturation,
	        @RequestParam(value = "eLmp", required = false) String eLmp,
	        @RequestParam(value = "ePregnancyKnown", required = false) String ePregnancyKnown,
	        @RequestParam(value = "eConscious", required = false) String eConscious,
	        @RequestParam(value = "eResponsive", required = false) String eResponsive,
	        @RequestParam(value = "temperatureObsId", required = false) String temperatureObsId,
	        @RequestParam(value = "systolicBpObsId", required = false) String systolicBpObsId,
	        @RequestParam(value = "diastolicBpObsId", required = false) String diastolicBpObsId,
	        @RequestParam(value = "heightObsId", required = false) String heightObsId,
	        @RequestParam(value = "weightObsId", required = false) String weightObsId,
	        @RequestParam(value = "bmiObsId", required = false) String bmiObsId,
	        @RequestParam(value = "bsaObsId", required = false) String bsaObsId,
	        @RequestParam(value = "rRateObsId", required = false) String rRateObsId,
	        @RequestParam(value = "pRateObsId", required = false) String pRateObsId,
	        @RequestParam(value = "headCircumferenceObsId", required = false) String headCircumferenceObsId,
	        @RequestParam(value = "glucoseLevelObsId", required = false) String glucoseLevelObsId,
	        @RequestParam(value = "oxygenSaturationObsId", required = false) String oxygenSaturationObsId,
	        @RequestParam(value = "lmpObsId", required = false) String lmpObsId,
	        @RequestParam(value = "pregnancyKnownObsId", required = false) String pregnancyKnownObsId,
	        @RequestParam(value = "consciousObsId", required = false) String consciousObsId,
	        @RequestParam(value = "responsiveObsId", required = false) String responsiveObsId,
	        @RequestParam(value = "encounterToEdit", required = false) String encounterToEdit) throws ParseException {
		ObsService obsService = Context.getObsService();
		ConceptService conceptService = Context.getConceptService();
		
		if (StringUtils.isNotBlank(temperature) && StringUtils.isNotBlank(temperatureObsId)) {
			Obs temperatureObs = obsService.getObs(Integer.valueOf(temperatureObsId));
			temperatureObs.setValueNumeric(Double.valueOf(temperature));
			obsService.saveObs(temperatureObs, "Edited temperature values");
		}
		
		if (StringUtils.isNotBlank(sbp) && StringUtils.isNotBlank(systolicBpObsId)) {
			Obs sbpObs = obsService.getObs(Integer.valueOf(systolicBpObsId));
			sbpObs.setValueNumeric(Double.valueOf(sbp));
			obsService.saveObs(sbpObs, "Edited Systolic values");
		}
		
		if (StringUtils.isNotBlank(dbp) && StringUtils.isNotBlank(diastolicBpObsId)) {
			Obs dbpObs = obsService.getObs(Integer.valueOf(diastolicBpObsId));
			dbpObs.setValueNumeric(Double.valueOf(dbp));
			obsService.saveObs(dbpObs, "Edited diastolic values");
		}
		if (StringUtils.isNotBlank(weight) && StringUtils.isNotBlank(weightObsId)) {
			Obs wightObs = obsService.getObs(Integer.valueOf(weightObsId));
			wightObs.setValueNumeric(Double.valueOf(weight));
			obsService.saveObs(wightObs, "Edited weight values");
		}
		if (StringUtils.isNotBlank(height) && StringUtils.isNotBlank(heightObsId)) {
			Obs heightObs = obsService.getObs(Integer.valueOf(heightObsId));
			heightObs.setValueNumeric(Double.valueOf(height));
			obsService.saveObs(heightObs, "Edited height values");
		}
		if (bmi != null && StringUtils.isNotBlank(bmiObsId)) {
			Obs bmiObs = obsService.getObs(Integer.valueOf(bmiObsId));
			bmiObs.setValueNumeric(bmi);
			obsService.saveObs(bmiObs, "Edited BMI values");
		}
		if (bsa != null && StringUtils.isNotBlank(bsaObsId)) {
			Obs bsaObs = obsService.getObs(Integer.valueOf(bsaObsId));
			bsaObs.setValueNumeric(bsa);
			obsService.saveObs(bsaObs, "Edited BSA values");
		}
		if (StringUtils.isNotBlank(rRate) && StringUtils.isNotBlank(rRateObsId)) {
			Obs rRateObs = obsService.getObs(Integer.valueOf(rRateObsId));
			rRateObs.setValueNumeric(Double.valueOf(rRate));
			obsService.saveObs(rRateObs, "Edited Respiratory rate values");
		}
		if (StringUtils.isNotBlank(pulse) && StringUtils.isNotBlank(pRateObsId)) {
			Obs pulseObs = obsService.getObs(Integer.valueOf(pRateObsId));
			pulseObs.setValueNumeric(Double.valueOf(pulse));
			obsService.saveObs(pulseObs, "Edited pulse rate values");
		}
		if (StringUtils.isNotBlank(hCircumference) && StringUtils.isNotBlank(headCircumferenceObsId)) {
			Obs hCircumferenceObs = obsService.getObs(Integer.valueOf(headCircumferenceObsId));
			hCircumferenceObs.setValueNumeric(Double.valueOf(hCircumference));
			obsService.saveObs(hCircumferenceObs, "Edited head circumference values");
		}
		if (StringUtils.isNotBlank(hGlucoseLevel) && StringUtils.isNotBlank(glucoseLevelObsId)) {
			Obs glucoseLevelObsObs = obsService.getObs(Integer.valueOf(glucoseLevelObsId));
			glucoseLevelObsObs.setValueNumeric(Double.valueOf(hGlucoseLevel));
			obsService.saveObs(glucoseLevelObsObs, "Edited glucose level values");
		}
		if (StringUtils.isNotBlank(hOxygenSaturation) && StringUtils.isNotBlank(oxygenSaturationObsId)) {
			Obs oxygenSaturationObs = obsService.getObs(Integer.valueOf(oxygenSaturationObsId));
			oxygenSaturationObs.setValueNumeric(Double.valueOf(hOxygenSaturation));
			obsService.saveObs(oxygenSaturationObs, "Edited oxygen saturation values");
		}
		if (StringUtils.isNotBlank(eLmp) && StringUtils.isNotBlank(lmpObsId)) {
			Obs lmpObs = obsService.getObs(Integer.valueOf(lmpObsId));
			SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			lmpObs.setValueDate(dateTimeFormatter.parse(eLmp));
			obsService.saveObs(lmpObs, "Edited lmp values");
		}
		if (StringUtils.isNotBlank(ePregnancyKnown) && StringUtils.isNotBlank(pregnancyKnownObsId)) {
			Obs pregnancyStatusObs = obsService.getObs(Integer.valueOf(pregnancyKnownObsId));
			Concept concept = conceptService.getConcept(Integer.valueOf(ePregnancyKnown));
			if (concept != null) {
				pregnancyStatusObs.setValueCoded(concept);
				obsService.saveObs(pregnancyStatusObs, "Edited pregnancy status values");
			}
		}
		if (StringUtils.isNotBlank(eConscious) && StringUtils.isNotBlank(consciousObsId)) {
			Obs consciousObs = obsService.getObs(Integer.valueOf(consciousObsId));
			Concept concept = conceptService.getConcept(Integer.valueOf(eConscious));
			if (concept != null) {
				consciousObs.setValueCoded(concept);
				obsService.saveObs(consciousObs, "Edited conscious status values");
			}
		}
		if (StringUtils.isNotBlank(eResponsive) && StringUtils.isNotBlank(responsiveObsId)) {
			Obs responsiveObs = obsService.getObs(Integer.valueOf(responsiveObsId));
			Concept concept = conceptService.getConcept(Integer.valueOf(eResponsive));
			if (concept != null) {
				responsiveObs.setValueCoded(concept);
				obsService.saveObs(responsiveObs, "Edited responsive status values");
			}
		}
		//we take a scenerial where some fields were NOT supplied at the first round and now during editing they are available
		if (StringUtils.isNotBlank(encounterToEdit)) {
			Encounter vitalEncounter = Context.getEncounterService().getEncounter(Integer.valueOf(encounterToEdit));
			if (vitalEncounter != null) {
				if (StringUtils.isNotBlank(temperature) && StringUtils.isBlank(temperatureObsId)) {
					Obs temObs = new Obs();
					temObs.setObsDatetime(new Date());
					temObs.setConcept(BotswanaEmrUtils.getConcept(TEMPERATURE));
					temObs.setValueNumeric(Double.valueOf(temperature));
					temObs.setEncounter(vitalEncounter);
					temObs.setPerson(vitalEncounter.getPatient());
					temObs.setCreator(vitalEncounter.getCreator());
					temObs.setDateCreated(new Date());
					temObs.setLocation(vitalEncounter.getLocation());
					
					obsService.saveObs(temObs, "New temperature values entered during editing");
				}
				if (StringUtils.isNotBlank(sbp) && StringUtils.isBlank(systolicBpObsId)) {
					Obs sbpObs = new Obs();
					sbpObs.setObsDatetime(new Date());
					sbpObs.setConcept(BotswanaEmrUtils.getConcept(SYSTOLIC_BP));
					sbpObs.setValueNumeric(Double.valueOf(sbp));
					sbpObs.setEncounter(vitalEncounter);
					sbpObs.setPerson(vitalEncounter.getPatient());
					sbpObs.setCreator(vitalEncounter.getCreator());
					sbpObs.setDateCreated(new Date());
					sbpObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(sbpObs, "New systolic BP values entered during editing");
				}
				if (StringUtils.isNotBlank(dbp) && StringUtils.isBlank(diastolicBpObsId)) {
					Obs dbpObs = new Obs();
					dbpObs.setObsDatetime(new Date());
					dbpObs.setConcept(BotswanaEmrUtils.getConcept(DIASTOLIC_BP));
					dbpObs.setValueNumeric(Double.valueOf(dbp));
					dbpObs.setEncounter(vitalEncounter);
					dbpObs.setPerson(vitalEncounter.getPatient());
					dbpObs.setCreator(vitalEncounter.getCreator());
					dbpObs.setDateCreated(new Date());
					dbpObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(dbpObs, "New diastolic BP values entered during editing");
				}
				if (StringUtils.isNotBlank(weight) && StringUtils.isBlank(weightObsId)) {
					Obs weightObs = new Obs();
					weightObs.setObsDatetime(new Date());
					weightObs.setConcept(BotswanaEmrUtils.getConcept(WEIGHT));
					weightObs.setValueNumeric(Double.valueOf(weight));
					weightObs.setEncounter(vitalEncounter);
					weightObs.setPerson(vitalEncounter.getPatient());
					weightObs.setCreator(vitalEncounter.getCreator());
					weightObs.setDateCreated(new Date());
					weightObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(weightObs, "New weight values entered during editing");
				}
				if (StringUtils.isNotBlank(height) && StringUtils.isBlank(heightObsId)) {
					Obs heightObs = new Obs();
					heightObs.setObsDatetime(new Date());
					heightObs.setConcept(BotswanaEmrUtils.getConcept(HEIGHT));
					heightObs.setValueNumeric(Double.valueOf(height));
					heightObs.setEncounter(vitalEncounter);
					heightObs.setPerson(vitalEncounter.getPatient());
					heightObs.setCreator(vitalEncounter.getCreator());
					heightObs.setDateCreated(new Date());
					heightObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(heightObs, "New height values entered during editing");
				}
				if (bmi != null && StringUtils.isBlank(bmiObsId)) {
					Obs bmiObs = new Obs();
					bmiObs.setObsDatetime(new Date());
					bmiObs.setConcept(BotswanaEmrUtils.getConcept(BMI));
					bmiObs.setValueNumeric(bmi);
					bmiObs.setEncounter(vitalEncounter);
					bmiObs.setPerson(vitalEncounter.getPatient());
					bmiObs.setCreator(vitalEncounter.getCreator());
					bmiObs.setDateCreated(new Date());
					bmiObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(bmiObs, "New BMI values entered during editing");
				}
				if (bsa != null && StringUtils.isBlank(bsaObsId)) {
					Obs bsaObs = new Obs();
					bsaObs.setObsDatetime(new Date());
					bsaObs.setConcept(BotswanaEmrUtils.getConcept(BSA));
					bsaObs.setValueNumeric(bsa);
					bsaObs.setEncounter(vitalEncounter);
					bsaObs.setPerson(vitalEncounter.getPatient());
					bsaObs.setCreator(vitalEncounter.getCreator());
					bsaObs.setDateCreated(new Date());
					bsaObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(bsaObs, "New BSA values entered during editing");
				}
				if (StringUtils.isNotBlank(rRate) && StringUtils.isBlank(rRateObsId)) {
					Obs rRateObs = new Obs();
					rRateObs.setObsDatetime(new Date());
					rRateObs.setConcept(BotswanaEmrUtils.getConcept(RESPIRATORY_RATE));
					rRateObs.setValueNumeric(Double.valueOf(rRate));
					rRateObs.setEncounter(vitalEncounter);
					rRateObs.setPerson(vitalEncounter.getPatient());
					rRateObs.setCreator(vitalEncounter.getCreator());
					rRateObs.setDateCreated(new Date());
					rRateObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(rRateObs, "New R Rate values entered during editing");
				}
				if (StringUtils.isNotBlank(pulse) && StringUtils.isBlank(pRateObsId)) {
					Obs pulseObs = new Obs();
					pulseObs.setObsDatetime(new Date());
					pulseObs.setConcept(BotswanaEmrUtils.getConcept(PULSE));
					pulseObs.setValueNumeric(Double.valueOf(pulse));
					pulseObs.setEncounter(vitalEncounter);
					pulseObs.setPerson(vitalEncounter.getPatient());
					pulseObs.setCreator(vitalEncounter.getCreator());
					pulseObs.setDateCreated(new Date());
					pulseObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(pulseObs, "New pulse values entered during editing");
				}
				if (StringUtils.isNotBlank(hCircumference) && StringUtils.isBlank(headCircumferenceObsId)) {
					Obs hCircumObs = new Obs();
					hCircumObs.setObsDatetime(new Date());
					hCircumObs.setConcept(BotswanaEmrUtils.getConcept(HEAD_CIRCUMFERENCE));
					hCircumObs.setValueNumeric(Double.valueOf(hCircumference));
					hCircumObs.setEncounter(vitalEncounter);
					hCircumObs.setPerson(vitalEncounter.getPatient());
					hCircumObs.setCreator(vitalEncounter.getCreator());
					hCircumObs.setDateCreated(new Date());
					hCircumObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(hCircumObs, "New Head circumference values entered during editing");
				}
				if (StringUtils.isNotBlank(ePregnancyKnown) && StringUtils.isBlank(pregnancyKnownObsId)) {
					Obs pregnancyKnownObs = new Obs();
					Concept concept = conceptService.getConcept(Integer.valueOf(ePregnancyKnown));
					pregnancyKnownObs.setObsDatetime(new Date());
					pregnancyKnownObs.setConcept(BotswanaEmrUtils.getConcept(PREGNANCY_STATUS));
					pregnancyKnownObs.setValueCoded(concept);
					pregnancyKnownObs.setEncounter(vitalEncounter);
					pregnancyKnownObs.setPerson(vitalEncounter.getPatient());
					pregnancyKnownObs.setCreator(vitalEncounter.getCreator());
					pregnancyKnownObs.setDateCreated(new Date());
					pregnancyKnownObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(pregnancyKnownObs, "New pregnancy status values entered during editing");
				}
				if (StringUtils.isNotBlank(eConscious) && StringUtils.isBlank(consciousObsId)) {
					Obs consciousObs = new Obs();
					Concept concept = conceptService.getConcept(Integer.valueOf(eConscious));
					consciousObs.setObsDatetime(new Date());
					consciousObs.setConcept(BotswanaEmrUtils.getConcept(CONSCIOUS));
					consciousObs.setValueCoded(concept);
					consciousObs.setEncounter(vitalEncounter);
					consciousObs.setPerson(vitalEncounter.getPatient());
					consciousObs.setCreator(vitalEncounter.getCreator());
					consciousObs.setDateCreated(new Date());
					consciousObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(consciousObs, "New conscious values entered during editing");
				}
				if (StringUtils.isNotBlank(eResponsive) && StringUtils.isBlank(responsiveObsId)) {
					Obs responsiveObs = new Obs();
					Concept concept = conceptService.getConcept(Integer.valueOf(eResponsive));
					responsiveObs.setObsDatetime(new Date());
					responsiveObs.setConcept(BotswanaEmrUtils.getConcept(RESPONSIVE));
					responsiveObs.setValueCoded(concept);
					responsiveObs.setEncounter(vitalEncounter);
					responsiveObs.setPerson(vitalEncounter.getPatient());
					responsiveObs.setCreator(vitalEncounter.getCreator());
					responsiveObs.setDateCreated(new Date());
					responsiveObs.setLocation(vitalEncounter.getLocation());
					obsService.saveObs(responsiveObs, "New responsive values entered during editing");
				}
			}
		}
	}
}
