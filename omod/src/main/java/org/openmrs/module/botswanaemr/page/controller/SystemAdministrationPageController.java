/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.Location;
import org.openmrs.Person;
import org.openmrs.PersonName;
import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.adminui.AdminUiConstants;
import org.openmrs.module.adminui.account.Account;
import org.openmrs.module.adminui.account.AccountService;
import org.openmrs.module.providermanagement.Provider;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.util.OpenmrsConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SystemAdministrationPageController {
	
	public Object controller(PageModel model, @SpringBean("adminAccountService") AccountService accountService,
	        @SpringBean("adminService") AdministrationService administrationService, UiUtils ui) throws IOException {
		List<Role> capabilities = accountService.getAllCapabilities();
		model.addAttribute("capabilities", accountService.getAllCapabilities());
		List<Role> privilegeLevels = accountService.getAllPrivilegeLevels();
		model.addAttribute("privilegeLevels", privilegeLevels);
		String privilegeLevelPrefix = AdminUiConstants.ROLE_PREFIX_PRIVILEGE_LEVEL;
		String rolePrefix = AdminUiConstants.ROLE_PREFIX_CAPABILITY;
		model.addAttribute("privilegeLevelPrefix", privilegeLevelPrefix);
		model.addAttribute("rolePrefix", rolePrefix);
		model.addAttribute("allowedLocales", administrationService.getAllowedLocales());
		model.addAttribute("userRoles", new ArrayList<>(Context.getService(UserService.class).getAllRoles().stream()
		        .filter(role -> role.getName().startsWith("Org")).collect(Collectors.toList())));
		model.addAttribute("users", new ArrayList<>(Context.getUserService().getAllUsers()));
		
		Map<String, Integer> propertyMaxLengthMap = new HashMap<>();
		propertyMaxLengthMap.put("familyName",
		    administrationService.getMaximumPropertyLength(PersonName.class, "family_name"));
		propertyMaxLengthMap.put("middleName",
		    administrationService.getMaximumPropertyLength(PersonName.class, "middle_name"));
		propertyMaxLengthMap.put("givenName",
		    administrationService.getMaximumPropertyLength(PersonName.class, "given_name"));
		propertyMaxLengthMap.put("identifier", administrationService.getMaximumPropertyLength(Provider.class, "identifier"));
		propertyMaxLengthMap.put("username", administrationService.getMaximumPropertyLength(User.class, "username"));
		propertyMaxLengthMap.put("password", administrationService.getMaximumPropertyLength(User.class, "password"));
		model.addAttribute("propertyMaxLengthMap", propertyMaxLengthMap);
		model.addAttribute("passwordMinLength",
		    administrationService.getGlobalProperty(OpenmrsConstants.GP_PASSWORD_MINIMUM_LENGTH, "8"));
		
		ObjectMapper mapper = new ObjectMapper();
		SimpleObject so = new SimpleObject();
		for (Role cap : capabilities) {
			String str = ui.format(cap);
			so.put(cap.getUuid(), str.substring(str.indexOf(rolePrefix) + rolePrefix.length()));
		}
		model.addAttribute("capabilitiesJson", mapper.writeValueAsString(so));
		model.addAttribute("account", new Account(new Person()));
		model.addAttribute("customUserPropertyEditFragments", "");
		
		List<Location> allLocations = Context.getLocationService().getAllLocations();
		List<Location> childLocations = new ArrayList<>();
		List<Location> parentLocations = new ArrayList<>();
		for (Location location : allLocations) {
			if (location.getParentLocation() != null) {
				childLocations.add(location);
			} else {
				parentLocations.add(location);
			}
		}
		
		model.addAttribute("childLocations", childLocations);
		model.addAttribute("parentLocations", parentLocations);
		
		return null;
	}
}
