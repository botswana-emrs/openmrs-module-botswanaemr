/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.openmrs.Location;
import org.openmrs.Order;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.Date;
import java.util.List;

public class YearlyPharmacyGraphSummariesFragmentController {
	
	public void controller(FragmentModel model, UiSessionContext sessionContext) {
		
		for (int i = 0; i < 12; i++) {
			
			model.addAttribute("t" + i, getFilledOrdersPerMonth(i, sessionContext.getSessionLocation()));
		}
	}
	
	private Integer getFilledOrdersPerMonth(int i, Location location) {
		int value = 0;
		List<Order> monthlyFilledOrders = BotswanaEmrUtils.getDrugOrders(
		    DateUtil.getStartOfDay(DateUtil.adjustDate(DateUtil.getStartOfYear(new Date()), i, DurationUnit.MONTHS)),
		    DateUtil.getEndOfDay(
		        DateUtil.getEndOfMonth(DateUtil.adjustDate(DateUtil.getStartOfYear(new Date()), i, DurationUnit.MONTHS))),
		    Order.FulfillerStatus.COMPLETED, location);
		if (monthlyFilledOrders != null && !monthlyFilledOrders.isEmpty()) {
			
			value = monthlyFilledOrders.size();
		}
		
		return value;
	}
}
