/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDateWithTime;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLabOrderResultsIfCompleteSet;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLabResultsKeyMap;

public class LabResultsFragmentController {
	
	public void controller(FragmentModel fragmentModel, @SpringBean("labService") LabService labService)
	        throws ParseException {
		fragmentModel.addAttribute("lab", simplifiedLabTestList(labService.getLatestLabTestByDateOnly(new Date())));
	}
	
	private List<SimplifiedLabTest> simplifiedLabTestList(List<LabTest> labTests) {
		List<SimplifiedLabTest> test = new ArrayList<>();
		if (labTests.size() > 0) {
			SimplifiedLabTest simplifiedLabTest;
			for (LabTest labTest : labTests) {
				simplifiedLabTest = new SimplifiedLabTest();
				simplifiedLabTest.setPatientNames(BotswanaEmrUtils.formatPersonName(labTest.getPatient().getPersonName()));
				simplifiedLabTest.setTestName(labTest.getConcept().getDisplayString());
				simplifiedLabTest
				        .setDateAdded(formatDateWithTime(labTest.getOrder().getDateCreated(), "dd/MM/yyyy HH:mm:ss"));
				Encounter encounterResult = labTest.getEncounterResult();
				if (encounterResult != null) {
					simplifiedLabTest.setDateReceived(
					    formatDateWithTime(encounterResult.getEncounterDatetime(), "dd/MM/yyyy HH:mm:ss"));
				}
				simplifiedLabTest.setStatus(labTest.getStatus());
				simplifiedLabTest.setOrder(labTest.getOrder());
				simplifiedLabTest.setLabOrderTestId(labTest.getLabTestId());
				simplifiedLabTest.setPatient(labTest.getPatient());
				
				test.add(simplifiedLabTest);
			}
		}
		return test;
	}
	
	public Map<String, String> getLabResultsFilled(@RequestParam("labSetId") Integer labSetId) {
		LabService labService = Context.getService(LabService.class);
		return getLabResultsKeyMap(getLabOrderResultsIfCompleteSet(labSetId, labService));
	}
}
