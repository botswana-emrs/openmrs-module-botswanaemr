/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.Model;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_SOCIAL_HABITS_ENCOUNTER_TYPE_UUID;

public class SocialHabitsPageController {
	
	public void controller(Model model, UiSessionContext sessionContext,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam("patientId") Patient patient) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/srh-social-habits-form.xml";
		
		boolean hasEncounter = false;
		
		EncounterType socialHabitsEncounterType = BotswanaEmrUtils.getEncounterType(SRH_SOCIAL_HABITS_ENCOUNTER_TYPE_UUID);
		if (encounter != null) {
			hasEncounter = true;
			model.addAttribute("encounter", encounter);
		} else {
			model.addAttribute("encounter", "");
		}
		model.addAttribute("hasEncounter", hasEncounter);
		
		model.addAttribute("definitionUiResource", definitionUiResource);
		model.addAttribute("formUuid", BotswanaEmrConstants.SRH_SOCIAL_HABITS_FORM_UUID);
		model.addAttribute("modalTitle", BotswanaEmrConstants.SRH_SOCIAL_HABITS_FORM_MODAL_TITLE);
		List<Encounter> socialHabitsEncounters = BotswanaEmrUtils.getEncountersByPatient(patient, socialHabitsEncounterType,
		    null, null, null);
		model.addAttribute("socialHabitsEncounters", socialHabitsEncounters);
	}
}
