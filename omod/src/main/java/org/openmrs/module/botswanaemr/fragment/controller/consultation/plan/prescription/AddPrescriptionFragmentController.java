/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.plan.prescription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaRegimenService;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.fragment.controller.shared.Prescription;
import org.openmrs.module.botswanaemr.model.SimplifiedDrug;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedHtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.SpecimenResultMapper;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.SimplifiedRegimenComponentDrug;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.COTRIMOXAZOLE_DRUG_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REGIMEN_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REGIMEN_LINE_CONCEPT_ID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getConcept;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getObservations;

@Slf4j
@SuppressWarnings("unused")
public class AddPrescriptionFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "visitId", required = false) Visit visit,
	        @FragmentParam(value = "patientId") Patient patient,
	        @FragmentParam(value = "autoActivateConsultation", required = false) Boolean autoActivateConsultation,
	        @FragmentParam(value = "onArt", defaultValue = "false") Boolean onArt, UiSessionContext uiSessionContext,
	        UiUtils uiUtils) {
		
		// Get existing drug_order encounter for this visit
		Encounter encounter = BotswanaEmrUtils.getDrugOrderEncounter(patient, visit, uiSessionContext.getSessionLocation());
		
		fragmentModel.addAttribute("patient", patient.getUuid());
		fragmentModel.addAttribute("encounter", encounter != null ? encounter.getUuid() : null);
		fragmentModel.addAttribute("onArt", onArt);
		List<Obs> regimenObs = getObservations(patient, getConcept(REGIMEN_CONCEPT_UUID));
		List<Obs> regimenLineObs = getObservations(patient, getConcept(REGIMEN_LINE_CONCEPT_ID));
		//get the latest regimen from the regimenObs and assign to a currentRegimen variable and ensure regimenObs is not empty and not null
		Obs currentRegimen = regimenObs.isEmpty() ? null
		        : regimenObs.stream().max(Comparator.comparing(Obs::getObsDatetime)).get();
		Obs currentRegimenLine = regimenLineObs.isEmpty() ? null
		        : regimenLineObs.stream().max(Comparator.comparing(Obs::getObsDatetime)).get();
		
		fragmentModel.addAttribute("currentRegimen", currentRegimen);
		fragmentModel.addAttribute("currentRegimenLine", currentRegimenLine);
		List<RegimenComponent> regimenComponents = new ArrayList<>();
		JsonArray results = new JsonArray();
		if (currentRegimen != null) {
			regimenComponents = Context.getService(BotswanaRegimenService.class)
			        .getRegimenComponentsByRegimenConcept(currentRegimen.getValueCoded());
			if (!regimenComponents.isEmpty()) {
				for (RegimenComponent regimenComponent : regimenComponents) {
					List<RegimenComponentDrug> regimenComponentDrugs = regimenComponent.getRegimenComponentDrugs();
					results.add((JsonArray) new Gson().toJsonTree(getSimplifiedRegimenComponentDrug(regimenComponentDrugs),
					    new TypeToken<List<SimplifiedRegimenComponentDrug>>() {}.getType()));
				}
			}
		}
		fragmentModel.addAttribute("regimenComponents", regimenComponents);
		fragmentModel.addAttribute("regimenComponentDrugs", results);
		
		// Add Cotrimaxazole drug
		addDrugToModel(uiUtils, fragmentModel,
		    Context.getAdministrationService().getGlobalProperty(
		        BotswanaEmrConstants.GLOBAL_PROPERTY_COTRIMOXAZOLE_DRUG_UUID, BotswanaEmrConstants.COTRIMOXAZOLE_DRUG_UUID),
		    "cotrimaxDrug");
		
		// Add prep drug
		addDrugToModel(uiUtils, fragmentModel,
		    Context.getAdministrationService().getGlobalProperty(
		        BotswanaEmrConstants.GLOBAL_PROPERTY_COTRIMOXAZOLE_DRUG_UUID, BotswanaEmrConstants.PREP_DRUG_UUID),
		    "prepDrug");
		
		// Add pep drug
		addDrugToModel(uiUtils, fragmentModel,
		    Context.getAdministrationService().getGlobalProperty(
		        BotswanaEmrConstants.GLOBAL_PROPERTY_COTRIMOXAZOLE_DRUG_UUID, BotswanaEmrConstants.PEP_DRUG_UUID),
		    "pepDrug");
		
		// Add tpt drug
		addDrugToModel(uiUtils, fragmentModel,
		    Context.getAdministrationService().getGlobalProperty(
		        BotswanaEmrConstants.GLOBAL_PROPERTY_COTRIMOXAZOLE_DRUG_UUID, BotswanaEmrConstants.TPT_DRUG_UUID),
		    "tptDrug");
		
		//Set to Days
		fragmentModel.addAttribute("durationUnits", BotswanaEmrConstants.DAYS_DURATION_UNIT);
		fragmentModel.addAttribute("type", BotswanaEmrConstants.ORDER_TYPE_TEXT);
		
		//Orderer
		fragmentModel.addAttribute("orderer",
		    BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()).getUuid());
		
		//Medication routes
		//TODO concept routes concepts
		String drugRoutesConceptUuid = Context.getAdministrationService()
		        .getGlobalProperty(BotswanaEmrConstants.GLOBAL_PROPERTY_ORDER_DRUG_ROUTES_CONCEPT_UUID);
		if (drugRoutesConceptUuid == null) {
			drugRoutesConceptUuid = BotswanaEmrConstants.ADMINISTRATION_ROUTES_CONCEPT_UUID;
		}
		
		List<Concept> administrationRouteConcepts = BotswanaEmrUtils.getAdministrationRouteConcepts();
		
		fragmentModel.addAttribute("routes", administrationRouteConcepts);
		
		fragmentModel.addAttribute("dosageUnits", BotswanaEmrUtils.getStrengthUnits());
		fragmentModel.addAttribute("refillDurationUnits", Context.getOrderService().getDurationUnits());
		fragmentModel.addAttribute("dispensingUnits", Context.getOrderService().getDrugDispensingUnits());
		fragmentModel.addAttribute("quantityUnits", Context.getOrderService().getDrugDispensingUnits()); // BotswanaEmrUtils.getQuantityUnits());
		fragmentModel.addAttribute("quantityUnit", BotswanaEmrConstants.EACH_CONCEPT_UUID);
		
		//Better version: Implement lookup field
		List<SimplifiedDrug> simplifiedDrugs = BotswanaEmrUtils.getSimplifiedDrugsList();
		
		//remove already prescribed drugs from option list
		//drugs.removeIf();
		
		fragmentModel.addAttribute("drugs", simplifiedDrugs);
		fragmentModel.addAttribute("frequencies", Context.getOrderService().getOrderFrequencies(false));
		Prescription.addPrescription(fragmentModel, patient);
		
		boolean isConsultationActive;
		
		// Status if consultation is active
		if (autoActivateConsultation != null && autoActivateConsultation) {
			isConsultationActive = true;
		} else {
			isConsultationActive = BotswanaEmrUtils.isConsultationActive(patient);
		}
		fragmentModel.addAttribute("isConsultationActive", isConsultationActive);
		
		Visit currentPatientVisit = visit != null && visit.getVisitType() != null ? visit
		        : BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(), false);
		
		Boolean isPepVisitType = currentPatientVisit != null && currentPatientVisit.getVisitType() != null
		        && (currentPatientVisit.getVisitType().getUuid().equals(BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID)
		                || currentPatientVisit.getVisitType().getUuid()
		                        .equals(BotswanaEmrConstants.PEP_FOLLOW_UP_VISIT_TYPE_UUID));
		fragmentModel.addAttribute("isPepVisitType", isPepVisitType);
		
		Boolean isPrepVisitType = currentPatientVisit != null && currentPatientVisit.getVisitType() != null
		        && (currentPatientVisit.getVisitType().getUuid().equals(BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID)
		                || currentPatientVisit.getVisitType().getUuid()
		                        .equals(BotswanaEmrConstants.PREP_FOLLOW_UP_VISIT_TYPE_UUID));
		fragmentModel.addAttribute("isPrepVisitType", isPrepVisitType);
		BotswanaEmrUtils.getArtVisitTypes(fragmentModel, currentPatientVisit);
		
	}
	
	public List<Drug> removeTBDOTDrugs() {
		String dsTBDrugStringUuid = Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.DS_TB_DRUGS,
		    "");
		List<String> tbDOTDrugUuids = Arrays.asList(dsTBDrugStringUuid.split(","));
		
		return Context.getConceptService().getAllDrugs(false).stream()
		        .filter(drug -> !tbDOTDrugUuids.contains(drug.getConcept().getUuid())).collect(Collectors.toList());
	}
	
	private List<SimplifiedRegimenComponentDrug> getSimplifiedRegimenComponentDrug(
	        List<RegimenComponentDrug> regimenComponentDrugs) {
		List<SimplifiedRegimenComponentDrug> simplifiedRegimenComponentDrugs = new ArrayList<>();
		for (RegimenComponentDrug regimenComponentDrug : regimenComponentDrugs) {
			SimplifiedRegimenComponentDrug simplifiedRegimenComponentDrug = getSimplifiedRegimenComponentDrug(
			    regimenComponentDrug);
			simplifiedRegimenComponentDrugs.add(simplifiedRegimenComponentDrug);
		}
		return simplifiedRegimenComponentDrugs;
	}
	
	private SimplifiedRegimenComponentDrug getSimplifiedRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) {
		SimplifiedRegimenComponentDrug simplifiedRegimenComponentDrug = new SimplifiedRegimenComponentDrug();
		simplifiedRegimenComponentDrug.setRegimenComponentId(regimenComponentDrug.getRegimenComponent().getId());
		simplifiedRegimenComponentDrug.setRegimenId(regimenComponentDrug.getRegimen().getId());
		simplifiedRegimenComponentDrug.setDrug(regimenComponentDrug.getDrug().getId());
		simplifiedRegimenComponentDrug.setDrugName(regimenComponentDrug.getDrug().getName());
		simplifiedRegimenComponentDrug.setDrugUuid(regimenComponentDrug.getDrug().getUuid());
		simplifiedRegimenComponentDrug.setId(regimenComponentDrug.getId());
		simplifiedRegimenComponentDrug.setDose(regimenComponentDrug.getDose());
		if (regimenComponentDrug.getFrequency() != null) {
			simplifiedRegimenComponentDrug.setFrequency(regimenComponentDrug.getFrequency().getName().getName());
			simplifiedRegimenComponentDrug.setFrequencyUuid(regimenComponentDrug.getFrequency().getUuid());
			simplifiedRegimenComponentDrug.setFrequencyId(regimenComponentDrug.getFrequency().getId());
		}
		simplifiedRegimenComponentDrug.setStrength(regimenComponentDrug.getStrength());
		simplifiedRegimenComponentDrug.setUnits(regimenComponentDrug.getUnits().getName().getName());
		simplifiedRegimenComponentDrug.setUnitsId(regimenComponentDrug.getUnits().getId());
		simplifiedRegimenComponentDrug.setUnitsUuid(regimenComponentDrug.getUnits().getUuid());
		return simplifiedRegimenComponentDrug;
	}
	
	private void addDrugToModel(UiUtils uiUtils, FragmentModel fragmentModel, String drugUuid, String attributeName) {
		SimpleObject simpleObject = new SimpleObject();
		Drug drug = Context.getConceptService().getDrugByUuid(drugUuid);
		if (drug != null) {
			simpleObject = SimpleObject.fromObject(drug, uiUtils, "drugId", "dosageForm", "maximumDailyDose",
			    "minimumDailyDose", "strength", "doseLimitUnits", "concept", "uuid");
			simpleObject.put("fullName", drug.getName());
			simpleObject.put("unitsUuid", drug.getDosageForm() == null ? "" : drug.getDosageForm().getUuid());
		}
		fragmentModel.addAttribute(attributeName, simpleObject);
	}
}
