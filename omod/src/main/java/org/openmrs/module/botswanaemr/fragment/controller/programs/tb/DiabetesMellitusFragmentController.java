/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs.tb;

import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.summary.TbTreatmentSummary;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class DiabetesMellitusFragmentController {
	
	public void controller(FragmentModel model, @RequestParam("patientId") Patient patient) {
		TbTreatmentSummary tbTreatmentSummary = Context.getRegisteredComponent("botswana.emr.tbTreatmentSummary",
		    TbTreatmentSummary.class);
		
		model.addAttribute("results", tbTreatmentSummary.getDiabetesScreening(patient));
	}
}
