/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.widget;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.locationbasedaccess.utils.LocationUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

public class FacilityLocationFragmentController {
	
	public void controller(FragmentModel model, HttpSession session,
	        @RequestParam(value = "patientId", required = false) Patient patient) {
		LocationService locationService = Context.getLocationService();
		List<Location> activeLocations = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		model.addAttribute("activeLocations", activeLocations);
		
		model.addAttribute("selectedLocationUuid", null);
		Object sessionLocationId = session.getAttribute(UiSessionContext.LOCATION_SESSION_ATTRIBUTE);
		if (sessionLocationId != null && StringUtils.isNotBlank(sessionLocationId.toString())) {
			Location sessionLocation = locationService.getLocation(Integer.parseInt(sessionLocationId.toString()));
			model.addAttribute("selectedLocationUuid", sessionLocation.getUuid());
		}
		if (patient != null) {
			Location location = LocationUtils.getPersonLocation(patient.getPerson());
			if (location != null) {
				model.addAttribute("selectedLocationUuid", location.getUuid());
			}
		}
	}
}
