/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock.configuration;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IInstitutionDataService;
import org.openmrs.module.botswanaemrInventory.model.Institution;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class AddInstitutionFormPageController {
	
	private final IInstitutionDataService iInstitutionDataService = BotswanaInventoryContext.getInstitutionDataService();
	
	public void controller(@RequestParam(value = "id", required = false) Integer institutionId, PageModel pageModel,
	        UiSessionContext uiSessionContext) {
		
		if (institutionId != null && uiSessionContext.getCurrentUser() != null) {
			Institution institution = iInstitutionDataService.getById(institutionId);
			
			pageModel.addAttribute("institution", institution);
			pageModel.addAttribute("institutionId", institutionId);
			
		} else {
			pageModel.addAttribute("institution", null);
			pageModel.addAttribute("institutionId", null);
		}
	}
	
}
