/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Location;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_REGISTRATION_PORTAL_UUID;

public class PaymentsPortalPageController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	public void controller(PageModel model, UiSessionContext sessionContext,
	        @RequestParam(required = false, value = "section") String section,
	        @SpringBean("locationService") LocationService locationService) {
		String selection;
		
		if (StringUtils.isEmpty(section)) {
			section = "overview";
		}
		selection = "section-" + section;
		
		model.addAttribute("section", section);
		model.addAttribute("selection", selection);
		model.addAttribute("none", BotswanaEmrConstants.NONE);
		
		boolean isRegistration;
		User authenticatedUser = sessionContext.getCurrentUser();
		isRegistration = authenticatedUser != null && BotswanaEmrUtils.isRegistrationClerk(authenticatedUser);
		model.addAttribute("isRegistration", isRegistration);
	}
}
