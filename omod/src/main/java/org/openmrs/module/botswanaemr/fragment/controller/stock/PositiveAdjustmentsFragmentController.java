/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class PositiveAdjustmentsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		
		// Get my main stockrooms
		List<Stockroom> myMainStockrooms = StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation());
		// Get all main stockrooms in all facilities
		List<Stockroom> allMainStockrooms = BotswanaInventoryContext.getStockRoomDataService().getAll(false);
		// Keep stockrooms from other facilities
		for (Stockroom stockroom : myMainStockrooms) {
			allMainStockrooms.remove(stockroom);
		}
		
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		List<StockOperation> outgoingStockOperations = new ArrayList<>();
		
		for (Stockroom stockroom : allMainStockrooms) {
			
			stockOperationTemplate.setSource(stockroom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				// Filter for only unapproved stock operations
				if (stockOperation.getStockOperationParent() == null
				        || stockOperation.getStockOperationParent().getStatus() != StockOperationStatus.COMPLETED) {
					outgoingStockOperations.add(stockOperation);
				}
			}
		}
		
		// sort the list of stock operations by date created if there are any
		if (!outgoingStockOperations.isEmpty()) {
			outgoingStockOperations = outgoingStockOperations.stream()
			        .sorted((o1, o2) -> o2.getDateCreated().compareTo(o1.getDateCreated())).collect(Collectors.toList());
		}
		
		List<StockOperation> incomingStockOperations = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			
			stockOperationTemplate.setSource(stockroom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				// Filter for only pending child operations of approved parent stock operations
				if (stockOperation.getStockOperationParent() != null
				        && stockOperation.getStockOperationParent().getStatus() == StockOperationStatus.COMPLETED) {
					incomingStockOperations.add(stockOperation);
				}
			}
		}
		
		// List<StockOperation> outgoingStockOperationsToDispatch = new ArrayList<>();
		
		// for (Stockroom stockroom : myMainStockrooms) {
		
		// 	stockOperationTemplate.setSource(stockroom);
		// 	stockOperationTemplate.setStatus(StockOperationStatus.COMPLETED);
		// 	stockOperationsSearch.setTemplate(stockOperationTemplate);
		// 	List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
		// 	        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
		// 	            null);
		// 	for (StockOperation stockOperation : stockOperations) {
		// 		// Filter for only approved stock operations ready to dispatch
		// 		if (stockOperation.getDescription().equals("Approved for Transfer")) {
		// 			outgoingStockOperationsToDispatch.add(stockOperation);
		// 		}
		// 	}
		// }
		
		List<StockOperation> itemsToReceive = getIncomingStockOperationsToReceive(myMainStockrooms);
		
		List<StockOperation> completedPositiveAdjustments = getCompletedPositiveAdjustments(myMainStockrooms);
		
		fragmentModel.addAttribute("outgoingOperations", outgoingStockOperations);
		fragmentModel.addAttribute("incomingStockOperations", incomingStockOperations);
		//fragmentModel.addAttribute("outgoingStockOperationsToDispatch", outgoingStockOperationsToDispatch);
		fragmentModel.addAttribute("incomingStockOperationsToReceive", itemsToReceive);
		fragmentModel.addAttribute("completedPositiveAdjustments", completedPositiveAdjustments);
	}
	
	private List<StockOperation> getIncomingStockOperationsToReceive(List<Stockroom> myMainStockrooms) {
		List<StockOperation> incomingStockOperationsToReceive = new ArrayList<>();
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		
		for (Stockroom stockroom : myMainStockrooms) {
			stockOperationTemplate.setDestination(stockroom);
			// stockOperationTemplate.setSource(null);
			
			stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getDistribution());
			stockOperationTemplate.setStatus(StockOperationStatus.PROCESSED);
			
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			
			for (StockOperation stockOperation : stockOperations) {
				incomingStockOperationsToReceive.add(stockOperation);
			}
		}
		return incomingStockOperationsToReceive;
	}
	
	private List<StockOperation> getCompletedPositiveAdjustments(List<Stockroom> myMainStockrooms) {
		List<StockOperation> completedPositiveAdjustments = new ArrayList<>();
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		
		for (Stockroom stockroom : myMainStockrooms) {
			stockOperationTemplate.setDestination(stockroom);
			stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
			stockOperationTemplate.setStockOperationParent(null);
			stockOperationTemplate.setStatus(StockOperationStatus.COMPLETED);
			
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			
			Integer totalRecordCount = BotswanaInventoryContext.getStockRoomDataService()
			        .getOperations(stockroom, stockOperationsSearch, null).size();
			if (totalRecordCount > 0) {
				Integer pageSize = 10;
				Integer totalPages = (int) Math.ceil(totalRecordCount / pageSize);
				PagingInfo pagingInfo = new PagingInfo();
				pagingInfo.setPage(totalPages);
				pagingInfo.setPageSize(pageSize);
				List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
				        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
				            pagingInfo);
				
				for (StockOperation stockOperation : stockOperations) {
					completedPositiveAdjustments.add(stockOperation);
				}
			}
		}
		return completedPositiveAdjustments;
	}
}
