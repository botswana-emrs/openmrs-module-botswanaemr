/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.appointments;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ViewAppointmentFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext,
	        @RequestParam(value = "appointmentId", required = true) String appointmentId,
	        @RequestParam("patientId") Patient patient) {
		Appointment appointment = Context.getService(AppointmentService.class)
		        .getAppointment(Integer.valueOf(appointmentId));
		fragmentModel.addAttribute("appointment", appointment);
		fragmentModel.addAttribute("patient", patient);
		String service = "";
		if (appointment != null && StringUtils.isNotEmpty(appointment.getReason())) {
			Concept concept = Context.getConceptService().getConceptByUuid(appointment.getReason());
			if (concept != null)
				service = concept.getName().getName();
		}
		fragmentModel.addAttribute("service", service);
		
	}
}
