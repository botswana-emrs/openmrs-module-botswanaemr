/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.TimeSlot;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
@SuppressWarnings("unused")
public class ScheduleFollowUpConsultationFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "patientId", required = true) Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("visit", visit);
		
		List<Appointment.AppointmentStatus> appointmentStatuses = new ArrayList<>();
		appointmentStatuses.add(Appointment.AppointmentStatus.SCHEDULED);
		appointmentStatuses.add(Appointment.AppointmentStatus.WAITING);
		appointmentStatuses.add(Appointment.AppointmentStatus.RESCHEDULED);
		
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		List<Appointment> appointments = appointmentService.getAppointmentsByConstraints(visit.getStartDatetime(), null,
		    uiSessionContext.getSessionLocation(), null, null, patient, appointmentStatuses);
		
		fragmentModel.addAttribute("appointments", appointments);
		fragmentModel.addAttribute("appointmentStatuses", appointmentStatuses);
		
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		fragmentModel.addAttribute("provider", provider);
	}
	
	public String updateScheduleAppointment(
	        @RequestParam(value = "appointmentDate", required = false) String appointmentDate,
	        @RequestParam(value = "appointmentId") String appointmentId,
	        @RequestParam(value = "appointmentStatus") String appointmentStatus,
	        @RequestParam(value = "startTime", required = false) String startTime,
	        @RequestParam(value = "endTime", required = false) String endTime, UiSessionContext uiSessionContext,
	        Integer locationId) throws ParseException {
		
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		LocationService locationService = Context.getLocationService();
		
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		Appointment appointment = appointmentService.getAppointment(Integer.valueOf(appointmentId));
		Appointment.AppointmentStatus status = mapStatus(appointmentStatus);
		
		//change status
		if (!appointment.getStatus().equals(status)) {
			appointmentService.changeAppointmentStatus(appointment, status);
		}
		
		//void previous appointment
		//appointmentService.voidTimeSlot(appointment.getTimeSlot(), "API Call: update appointment time");
		
		if (StringUtils.isNotBlank(appointmentDate) && StringUtils.isNotBlank(startTime)
		        && StringUtils.isNotBlank(endTime)) {
			String startDateStr = appointmentDate + " " + startTime;
			String endDateStr = appointmentDate + " " + endTime;
			Date startDate = BotswanaEmrUtils.formatDateFromStringWithTime(startDateStr);
			Date endDate = BotswanaEmrUtils.formatDateFromStringWithTime(endDateStr);
			
			log.debug("startDate: {}", startDate);
			log.debug("endDate: {}", endDate);
			
			//update timeslot
			TimeSlot timeSlot = appointment.getTimeSlot();
			timeSlot.setStartDate(startDate);
			timeSlot.setEndDate(endDate);
			TimeSlot updatedTimeslot = appointmentService.saveTimeSlot(timeSlot);
			log.debug("Updated timeslot: {}", updatedTimeslot);
			appointment.setTimeSlot(timeSlot);
			
			Appointment updatedAppointment = appointmentService.saveAppointment(appointment);
			log.debug("updated appointment: {}", updatedAppointment);
		}
		return null;
	}
	
	public String deleteAppointment(@RequestParam(value = "appointmentId") String appointmentId) {
		AppointmentService appointmentService = Context.getService(AppointmentService.class);
		Appointment appointmentToDelete = appointmentService.getAppointment(Integer.valueOf(appointmentId));
		if (appointmentToDelete != null) {
			appointmentService.purgeAppointment(appointmentToDelete);
		}
		return null;
	}
	
	private Appointment.AppointmentStatus mapStatus(String status) {
		switch (status.toLowerCase()) {
			case "waiting":
				return Appointment.AppointmentStatus.WAITING;
			case "scheduled":
				return Appointment.AppointmentStatus.SCHEDULED;
			case "rescheduled":
				return Appointment.AppointmentStatus.RESCHEDULED;
			default:
				return null;
		}
	}
	
	private Appointment copyAppointment(Appointment appointment) {
		Appointment cloneForRevisionAppointment = new Appointment();
		cloneForRevisionAppointment.setAppointmentType(appointment.getAppointmentType());
		cloneForRevisionAppointment.setTimeSlot(appointment.getTimeSlot());
		cloneForRevisionAppointment.setPatient(appointment.getPatient());
		cloneForRevisionAppointment.setVisit(appointment.getVisit());
		cloneForRevisionAppointment.setStatus(appointment.getStatus());
		cloneForRevisionAppointment.setCancelReason(appointment.getCancelReason());
		cloneForRevisionAppointment.setReason(appointment.getReason());
		cloneForRevisionAppointment.setCreator(appointment.getCreator());
		cloneForRevisionAppointment.setDateCreated(appointment.getDateCreated());
		
		return cloneForRevisionAppointment;
	}
}
