/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.decorator;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class BotswanaHeadlessPageFragmentController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	/**
	 * visit--location--to--be--current-facility encounter-location--be--service-point-served
	 **/
	public void controller(FragmentModel model, UiSessionContext sessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @FragmentParam(value = "isRegistrationPage", required = false) String isRegistrationPage,
	        @FragmentParam(value = "isScreeningPage", required = false) String isScreeningPage,
	        @SpringBean("locationService") LocationService locationService) {
		
		String isPatientQueue = null;
		boolean isAnonymousLocation = false;
		
		try {
			Context.addProxyPrivilege(VIEW_LOCATIONS);
			Context.addProxyPrivilege(GET_LOCATIONS);
			Object sessionLocationId = sessionContext.getSession().getAttribute(CURRENT_SERVICE_DELIVERY_POINT);
			Location serviceDeliverySessionLocation = Context.getLocationService()
			        .getLocationByUuid(String.valueOf(sessionLocationId));
			
			if (serviceDeliverySessionLocation == null) {
				//redirect to the service point selection page
				isAnonymousLocation = true;
			}
		}
		finally {
			Context.removeProxyPrivilege(VIEW_LOCATIONS);
			Context.removeProxyPrivilege(GET_LOCATIONS);
		}
		
		model.addAttribute("patient", patient);
		model.addAttribute("isInPatientContext", checkIfInPatientContext(patient));
		model.addAttribute("isRegistrationPage", isRegistrationPage);
		model.addAttribute("isAnonymousLocation", isAnonymousLocation);
		model.addAttribute("isPatientQueue", isPatientQueue);
		model.addAttribute("user", Context.getAuthenticatedUser());
		model.addAttribute("isScreeningPage", isScreeningPage);
		
		boolean isRegistration;
		User authenticatedUser = sessionContext.getCurrentUser();
		isRegistration = authenticatedUser != null && BotswanaEmrUtils.isRegistrationClerk(authenticatedUser);
		model.addAttribute("isRegistration", isRegistration);
	}
	
	private boolean checkIfInPatientContext(Patient patient) {
		boolean isFound = false;
		if (patient != null) {
			isFound = true;
		}
		return isFound;
	}
}
