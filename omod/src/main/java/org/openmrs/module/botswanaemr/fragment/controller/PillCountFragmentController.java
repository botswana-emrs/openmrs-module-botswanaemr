/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.ObsService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import liquibase.pro.packaged.lA;

import java.text.DecimalFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DRUG_TRACKING_ENCOUNTER_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MEDICATION_QUANTITY_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.WEIGHT_CONCEPT_UUID;

@Slf4j
@SuppressWarnings("unused")
public class PillCountFragmentController {
	
	private static final String ETHAMBUTANOL_CONCEPT_ID = "d12de6ea-20dd-4f33-92a4-5fc0acd66825";
	
	public void controller(FragmentModel model, @RequestParam("patientId") Patient patient, UiUtils ui) {
		ObsService obsService = Context.getObsService();
		Date enrollmentDate = null;
		Date completionDate = null;
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program tbProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, tbProgram, null, null,
		    null, null, false);
		List<PatientProgram> activePrograms = patientProgramList.stream().filter(PatientProgram::getActive)
		        .collect(Collectors.toList());
		
		if (activePrograms.size() > 0) {
			enrollmentDate = activePrograms.get(0).getDateEnrolled();
			completionDate = activePrograms.get(0).getDateCompleted();
		}
		
		// get all encounters for the patient
		EncounterType drugTrackingEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(DRUG_TRACKING_ENCOUNTER_TYPE);
		
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, enrollmentDate,
		        completionDate, null, null, Collections.singletonList(drugTrackingEncounterType), null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		List<Obs> observations = obsService.getObservations(Collections.singletonList(patient), encounters, null, null, null,
		    null, null, null, null, null, null, false);
		
		Map<Encounter, List<Obs>> mappedObs = observations.stream().filter(obs -> obs.getEncounter() != null)
		        .collect(Collectors.groupingBy(Obs::getEncounter));
		
		boolean hasSecondPhase = getNumberOfDaysSinceEnrollment(enrollmentDate) > 60;
		Double secondPhaseNumberOfPills = 0.0;
		Double firstPhaseNumberOfPills = 0.0;
		Double expectedNumberOfPillsPerDay = 0.0;
		if (enrollmentDate != null) {
			secondPhaseNumberOfPills = getActualNumberOfPillsForContinuationPhase(patient, encounters, enrollmentDate,
			    completionDate);
			firstPhaseNumberOfPills = getActualNumberOfPillsForInitialPhase(patient, encounters, enrollmentDate);
			expectedNumberOfPillsPerDay = getExpectedNumberOfPillsPerDay(patient, mappedObs);
		}
		
		model.addAttribute("initialPhaseCompliance",
		    getPercentage((expectedNumberOfPillsPerDay * 60), firstPhaseNumberOfPills));
		model.addAttribute("initialPhaseExpectedPills", (expectedNumberOfPillsPerDay * 60));
		model.addAttribute("initialPillsTaken", firstPhaseNumberOfPills);
		
		model.addAttribute("continuationPhaseCompliance",
		    getPercentage((expectedNumberOfPillsPerDay * 120), secondPhaseNumberOfPills));
		model.addAttribute("continuationPhaseExpectedPills", (expectedNumberOfPillsPerDay * 120));
		model.addAttribute("continuationPhasePillsTaken", secondPhaseNumberOfPills);
		model.addAttribute("hasSecondPhase", hasSecondPhase);
	}
	
	private long getDaysBetween(Date date1, Date date2) {
		return ChronoUnit.DAYS.between(date1.toInstant(), date2.toInstant());
	}
	
	private Double getExpectedNumberOfPillsPerDay(Patient patient, Map<Encounter, List<Obs>> mappedObs) {
		if (patient.getAge() == null) {
			return (double) 0;
		}
		return this.getQuantityBasedOnWeightAndAge(this.getLatestPatientWeight(patient), Double.valueOf(patient.getAge()));
	}
	
	private Double getQuantityBasedOnWeight(Double weight) {
		if (weight < 4) {
			return 0.0;
		} else if (weight >= 4 && weight <= 7) {
			return 1.0;
		} else if (weight >= 8 && weight <= 11) {
			return 2.0;
		} else if (weight >= 12 && weight <= 15) {
			return 3.0;
		} else if (weight >= 16 && weight <= 24) {
			return 4.0;
		} else {
			return 0.0;
		}
	}
	
	private Double getQuantityBasedOnWeightAndAge(Double weight, Double age) {
		double quantity = 0.0;
		if (age >= 14 || weight > 25) {
			if (weight >= 25 && weight <= 39) {
				quantity = 2.0;
			}
			
			if (weight >= 40 && weight <= 54) {
				quantity = 3.0;
			}
			
			if (weight >= 55 && weight <= 70) {
				quantity = 4.0;
			}
			
			if (weight > 70) {
				quantity = 5.0;
			}
		} else {
			if (weight < 4) {
				quantity = 0.5;
			}
			
			if (weight >= 4 && weight <= 7) {
				quantity = 1.0;
			}
			
			if (weight >= 8 && weight <= 11) {
				quantity = 2.0;
			}
			
			if (weight >= 12 && weight <= 15) {
				quantity = 3.0;
			}
			
			if (weight >= 16 && weight <= 24) {
				quantity = 4.0;
			}
		}
		return quantity;
	}
	
	private Double getLatestPatientWeight(Patient patient) {
		Concept weightConcept = BotswanaEmrUtils.getConcept(WEIGHT_CONCEPT_UUID);
		Obs lastWeightObs = Context.getObsService().getObservationsByPersonAndConcept(patient, weightConcept).stream()
		        .max(Comparator.comparing(Obs::getObsDatetime)).orElse(null);
		return lastWeightObs == null ? 0 : lastWeightObs.getValueNumeric();
	}
	
	private Double getActualNumberOfPillsForInitialPhase(Patient patient, List<Encounter> encounters, Date enrollmentDate) {
		Double actualNumberOfPills = 0.0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(enrollmentDate);
		cal.add(Calendar.DATE, 60);
		Date completionDate = cal.getTime();
		
		// Get encounter for the patient of type Drug Tracking
		EncounterType drugTrackingEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(DRUG_TRACKING_ENCOUNTER_TYPE);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, enrollmentDate,
		        completionDate, null, null, Collections.singletonList(drugTrackingEncounterType), null, null, null, false);
		List<Encounter> encountersForDrugTracking = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		List<Obs> observations = Context.getObsService().getObservations(Collections.singletonList(patient),
		    encountersForDrugTracking, Arrays.asList(BotswanaEmrUtils.getConcept(MEDICATION_QUANTITY_CONCEPT_UUID)), null,
		    null, null, null, null, null, enrollmentDate, completionDate, false);
		
		for (Obs o : observations) {
			if (o.getConcept().getUuid().equals(MEDICATION_QUANTITY_CONCEPT_UUID)) {
				if (o.getValueNumeric() != null) {
					actualNumberOfPills += o.getValueNumeric();
				}
			}
		}
		return actualNumberOfPills;
	}
	
	private Double getActualNumberOfPillsForContinuationPhase(Patient patient, List<Encounter> encounters,
	        Date enrollmentDate, Date completionDate) {
		Double actualNumberOfPills = 0.0;
		if (enrollmentDate != null) {
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(enrollmentDate);
			cal.add(Calendar.DATE, 60);
			Date startOfContinuationPhase = cal.getTime();
			log.debug("startOfContinuationPhase: {}", startOfContinuationPhase);
			
			List<Obs> observations = Context.getObsService().getObservations(Collections.singletonList(patient), null,
			    Arrays.asList(BotswanaEmrUtils.getConcept(MEDICATION_QUANTITY_CONCEPT_UUID)), null, null, null, null, null,
			    null, startOfContinuationPhase, completionDate, false);
			
			for (Obs o : observations) {
				if (o.getConcept().getUuid().equals(MEDICATION_QUANTITY_CONCEPT_UUID)) {
					if (o.getValueNumeric() != null) {
						actualNumberOfPills += o.getValueNumeric();
					}
				}
			}
		}
		return actualNumberOfPills;
	}
	
	private String getPercentage(Double expected, Double actual) {
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format(expected == 0 || actual == 0 ? 0 : (actual / expected) * 100);
	}
	
	private long getNumberOfDaysSinceEnrollment(Date enrollmentDate) {
		return enrollmentDate == null ? 0 : getDaysBetween(enrollmentDate, new Date());
	}
}
