/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import static org.openmrs.module.botswanaemr.htmlformentry.ScreeningAndTriagePostSubmissionAction.findGroupingObs;

import java.util.Date;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

@Slf4j
public class SelfTestKitDistributionPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		Concept stockroomIdConcept = Context.getConceptService().getConceptByUuid("a73ff744-0ceb-432e-bcf5-ec1d99e26cad");
		
		Obs stockroomIdObs = session.getEncounter().getObs().stream().filter(o -> o.getConcept().equals(stockroomIdConcept))
		        .findFirst().orElse(null);
		Stockroom stockroom = null;
		if (stockroomIdObs != null) {
			String stockroomUuid = stockroomIdObs.getValueText();
			stockroom = BotswanaInventoryContext.getStockRoomDataService().getByUuid(stockroomUuid);
		}
		
		if (stockroom == null) {
			// log debug message
			log.debug("Stockroom not found");
			return;
		}
		Concept distributionConcept = Context.getConceptService().getConceptByUuid("64b94dfc-7d7a-4bba-aee1-8d250c9cbe7d");
		Set<Obs> groupingObsSet = findGroupingObs(session.getEncounter(), distributionConcept);
		if (!groupingObsSet.isEmpty()) {
			IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
			IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
			Item item = iItemDataService.getByUuid(BotswanaEmrConstants.SELF_TEST_KIT_UUID);
			for (Obs groupingObs : groupingObsSet) {
				Set<Obs> groupMembers = groupingObs.getGroupMembers();
				String lotNumber = "";
				Double quantity = 0.0;
				Date expirationDate = null;
				for (Obs obs : groupMembers) {
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_CONCEPT_QUANTITY_ISSUED_UUID)) {
						quantity = obs.getValueNumeric();
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_TEST_KIT_LOT_UUID)) {
						lotNumber = obs.getValueText();
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_TEST_KIT_EXPIRY_DATE_UUID)) {
						expirationDate = obs.getValueDate();
					}
				}
				StockOperation stockOperation = new StockOperation();
				StockOperation batchOperation = iStockOperationDataService.getOperationByNumber(lotNumber);
				stockOperation.addItem(item, (int) Math.floor(quantity), expirationDate, batchOperation);
				stockOperation.setOperationDate(new Date());
				
				stockOperation.setSource(stockroom);
				stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
				stockOperation.setOperationNumber(lotNumber);
				stockOperation.setStatus(StockOperationStatus.NEW);
				BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
				stockOperation.setStatus(StockOperationStatus.COMPLETED);
				BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			}
		}
		
	}
	
}
