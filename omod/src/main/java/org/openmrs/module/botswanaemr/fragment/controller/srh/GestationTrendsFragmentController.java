/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.srh;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.GraphDataObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterSearchCriteriaUsingEncounterTypes;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortEncountersByEncounterDatetime;

public class GestationTrendsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient) {
		model.addAttribute("patient", patient);
		
		List<Obs> observations = Context.getObsService().getObservationsByPersonAndConcept(patient.getPerson(),
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.BABY_DETAILS_CONCEPT_UUID));
		List<String> groupUuids = observations.stream().map(Obs::getUuid).collect(Collectors.toList());
		model.addAttribute("groupUuids", groupUuids);
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_ENCOUNTER_TYPE);
		Obs obs = BotswanaEmrUtils.getLatestObs(patient, encounterType,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NUMBER_OF_BABIES_CONCEPT_UUID));
		model.addAttribute("numBabies", obs != null ? obs.getValueNumeric() : 1);
	}
	
	public SimpleObject getGestationData(@RequestParam("patientId") String patientId,
	        @RequestParam(value = "babyNumber") Integer babyNumber) {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_ENCOUNTER_TYPE);
		Obs numBabiesObs = BotswanaEmrUtils.getLatestObs(patient, encounterType,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NUMBER_OF_BABIES_CONCEPT_UUID));
		int numBabies = numBabiesObs != null ? numBabiesObs.getValueNumeric().intValue() : 1;
		SimpleObject simpleObject = new SimpleObject();
		
		List<GraphDataObject> motherWeights = new ArrayList<>();
		List<GraphDataObject> babySizes = new ArrayList<>();
		List<GraphDataObject> foetalWeights = new ArrayList<>();
		
		EncounterService encounterService = Context.getEncounterService();
		if (patient != null) {
			List<Encounter> gestationEncounters = encounterService.getEncounters(
			    getEncounterSearchCriteriaUsingEncounterTypes(patient, Collections.singletonList(BotswanaEmrUtils
			            .getEncounterType(BotswanaEmrConstants.ANC_FOETAL_GROWTH_AND_MATERNITY_ENCOUNTER_TYPE))));
			List<Encounter> encountersWithGestationData = new ArrayList<>();
			
			if (!gestationEncounters.isEmpty()) {
				//loop through all and pick the ones with gestation data recorded
				for (Encounter encounter : gestationEncounters) {
					Set<Obs> obsSet = encounter.getAllObs(false);
					//check if this encounter has any of the gestation data  obs
					for (Obs obs : obsSet) {
						if (BotswanaEmrUtils.getGestationTrendsConcepts().contains(obs.getConcept())) {
							//pick that encounter and exit immediately
							encountersWithGestationData.add(encounter);
							break;
						}
					}
				}
				//order those encounter with vitals
				if (!encountersWithGestationData.isEmpty()) {
					encountersWithGestationData = sortEncountersByEncounterDatetime(encountersWithGestationData);
				}
			}
			
			ObsService obsService = Context.getObsService();
			Concept groupingConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.BABY_DETAILS_CONCEPT_UUID);
			for (Encounter encounter : encountersWithGestationData) {
				GraphDataObject babySizeData = new GraphDataObject();
				GraphDataObject maternalWeightData = new GraphDataObject();
				GraphDataObject foetalWeightData = new GraphDataObject();
				
				if (numBabies > 1) {
					List<Obs> groupingObs = obsService.getObservations(Collections.singletonList(patient.getPerson()),
					    Collections.singletonList(encounter), Collections.singletonList(groupingConcept), null, null, null,
					    null, null, null, null, null, false);
					for (Obs obs : groupingObs) {
						if (hasObsWithConceptUuidAndValue(obs.getGroupMembers(false),
						    BotswanaEmrConstants.BABY_NUMBER_CONCEPT_UUID, String.valueOf(babyNumber))) {
							Obs gestationWeekObs = encounter.getAllObs(false).stream().filter(
							    o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.GESTATION_WEEK_CONCEPT_UUID))
							        .findFirst().orElse(null);
							Set<Obs> gestationObs = obs.getGroupMembers(false);
							gestationObs.add(gestationWeekObs);
							processGestationObs(gestationObs, babySizeData, maternalWeightData, foetalWeightData);
						}
					}
				} else {
					List<Obs> gestationDataObs = Context.getObsService().getObservations(
					    Collections.singletonList(patient.getPerson()), Collections.singletonList(encounter),
					    BotswanaEmrUtils.getGestationTrendsConcepts(), null, null, null, null, null, null, null, null, false,
					    null);
					processGestationObs(new HashSet<>(gestationDataObs), babySizeData, maternalWeightData, foetalWeightData);
				}
				
				motherWeights.add(maternalWeightData);
				foetalWeights.add(foetalWeightData);
				babySizes.add(babySizeData);
			}
		}
		sortGraphDataByXAxis(motherWeights);
		sortGraphDataByXAxis(babySizes);
		sortGraphDataByXAxis(foetalWeights);
		
		simpleObject.put("motherWeights", motherWeights);
		simpleObject.put("foetalWeights", foetalWeights);
		simpleObject.put("babySizes", babySizes);
		
		return simpleObject;
	}
	
	private void processGestationObs(Set<Obs> gestationDataObs, GraphDataObject babySizeData,
	        GraphDataObject maternalWeightData, GraphDataObject foetalWeightData) {
		for (Obs obs : gestationDataObs) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.FUNDAL_HEIGHT_CONCEPT_UUID)) {
				babySizeData.setyValue(obs.getValueNumeric());
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.MATERNAL_WEIGHT_CONCEPT_UUID)) {
				maternalWeightData.setyValue(obs.getValueNumeric());
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.FOETAL_WEIGHT_CONCEPT_UUID)) {
				foetalWeightData.setyValue(obs.getValueNumeric());
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.GESTATION_WEEK_CONCEPT_UUID)) {
				babySizeData.setxValue(obs.getValueNumeric());
				maternalWeightData.setxValue(obs.getValueNumeric());
				foetalWeightData.setxValue(obs.getValueNumeric());
			}
		}
	}
	
	private boolean hasObsWithConceptUuidAndValue(Set<Obs> obsList, String targetUuid, String targetValueText) {
		return obsList.stream().filter(obs -> obs.getConcept() != null && targetUuid.equals(obs.getConcept().getUuid()))
		        .anyMatch(obs -> obs.getValueText() != null && obs.getValueText().equals(targetValueText));
	}
	
	private List<GraphDataObject> sortGraphDataByXAxis(List<GraphDataObject> data) {
		Collections.sort(data, new Comparator<GraphDataObject>() {
			
			@Override
			public int compare(GraphDataObject a, GraphDataObject b) {
				// Check for null xValue
				if (a.getxValue() == null && b.getxValue() == null) {
					return 0;
				}
				if (a.getxValue() == null) {
					return 1; // Place nulls at the end
				}
				if (b.getxValue() == null) {
					return -1; // Place nulls at the end
				}
				return a.getxValue().compareTo(b.getxValue());
			}
		});
		return data;
	}
}
