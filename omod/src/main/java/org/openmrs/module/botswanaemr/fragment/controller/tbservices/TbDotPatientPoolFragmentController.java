/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.tbservices;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PERSON_LOCATION_ATTRIBUTE;

import org.joda.time.DateTime;
import org.openmrs.Concept;
import org.openmrs.ConceptAnswer;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.FormService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.SimplifiedPatientProgram;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TbDotPatientPoolFragmentController {
	
	private final HtmlFormEntryService htmlFormEntryService = Context.getService(HtmlFormEntryService.class);
	
	private final FormService formService = Context.getService(FormService.class);
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		
		fragmentModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		fragmentModel.addAttribute("providerList", providerList);
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		fragmentModel.addAttribute("currentUser",
		    BotswanaEmrUtils.formatPersonFull(Context.getAuthenticatedUser().getPerson()));
		
		Concept treatmentTypeConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.TB_TREATMENT_TYPE_CONCEPT_UUID);
		List<ConceptAnswer> phaseAnswers;
		if (treatmentTypeConcept != null) {
			phaseAnswers = BotswanaEmrUtils.getConceptAnswers(treatmentTypeConcept);
			for (int i = 0; i < phaseAnswers.size(); i++) {
				String name = phaseAnswers.get(i).getAnswerConcept().getName().getName().split(",")[0];
				phaseAnswers.get(i).getAnswerConcept().getName().setName(name);
			}
			fragmentModel.addAttribute("phaseAnswers", phaseAnswers);
		}
	}
	
	public List<SimpleObject> getDotPatientsInProgram(UiSessionContext uiSessionContext, UiUtils uiUtils) {
		List<PatientProgram> patientPrograms = new ArrayList<>();
		
		DateTime endDate = new DateTime(new Date());
		DateTime startDate = new DateTime(new Date()).minusMonths(6);
		
		Program program = Context.getService(ProgramWorkflowService.class)
		        .getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		if (program != null) {
			patientPrograms = Context.getService(BotswanaEmrService.class).getPatientProgramByProgramAndDate(program,
			    startDate.toDate(), endDate.toDate(), true);
		}
		
		List<SimplifiedPatientProgram> simplifiedPrograms = getSimplifiedPatients(patientPrograms,
		    uiSessionContext.getSessionLocation());
		
		return SimpleObject.fromCollection(simplifiedPrograms, uiUtils, "identifier", "name", "gender", "dateOfBirth", "age",
		    "registeredDate", "status", "registeredBy", "patientId", "visitNumber", "patientUuid", "programId",
		    "programName", "isTodayDot", "encounterIdentifier", "treatmentPhase");
	}
	
	protected List<SimplifiedPatientProgram> getSimplifiedPatients(List<PatientProgram> patientPrograms, Location location) {
		List<SimplifiedPatientProgram> allPatients = new ArrayList<>();
		SimplifiedPatientProgram simplifiedProgram;
		
		Set<Patient> patientList = new HashSet<>();
		
		List<Encounter> encounterList = getTodayDotEncounters(location);
		Set<Patient> dotPatients = new HashSet<>();
		if (encounterList != null) {
			dotPatients = encounterList.stream().map(Encounter::getPatient).collect(Collectors.toSet());
		}
		
		if (patientPrograms != null) {
			for (PatientProgram patientProgram : patientPrograms) {
				Patient patient = patientProgram.getPatient();
				PersonAttributeType personAttribute = Context.getPersonService()
				        .getPersonAttributeTypeByUuid(PERSON_LOCATION_ATTRIBUTE);
				PersonAttribute personLocationAttribute = patient.getAttribute(personAttribute.getName());
				if (patient != null && !patientList.contains(patient) && personLocationAttribute != null
				        && personLocationAttribute.getValue().equals(location.getUuid())) {
					Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, location, false);
					HtmlForm tbDotForm = htmlFormEntryService
					        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_DOT_FORM_UUID));
					List<HtmlForm> completedHtmlForms = BotswanaEmrUtils.completedHtmlForms(currentPatientVisit,
					    htmlFormEntryService);
					
					simplifiedProgram = new SimplifiedPatientProgram();
					simplifiedProgram.setIdentifier(
					    patient.getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier()); // BotswanaEmrUtils.formatPatientIdentifier(patient));
					simplifiedProgram.setPatientId(patient.getPatientId());
					simplifiedProgram.setName(BotswanaEmrUtils.formatPersonName(patient.getPersonName()));
					simplifiedProgram.setGender(patient.getPerson().getGender());
					simplifiedProgram.setAge(patient.getAge() == null ? "" : patient.getAge().toString());
					simplifiedProgram
					        .setRegisteredBy(BotswanaEmrUtils.formatPersonCreator(patientProgram.getCreator().getPerson()));
					simplifiedProgram.setRegisteredDate(
					    BotswanaEmrUtils.formatDateWithoutTime(patient.getDateCreated(), "dd-MM-yyyy"));
					simplifiedProgram.setDateOfBirth(patient.getBirthdate() == null ? ""
					        : BotswanaEmrUtils.formatDateWithoutTime(patient.getBirthdate(), "dd-MM-yyyy"));
					simplifiedProgram.setProgramId(patientProgram.getProgram().getUuid());
					simplifiedProgram.setProgramName(patientProgram.getProgram().getName());
					simplifiedProgram.setPatientUuid(patient.getUuid());
					simplifiedProgram.setIsTodayDot(dotPatients.contains(patient));
					if (simplifiedProgram.getIsTodayDot()) {
						Encounter encounter = encounterList.stream().filter(p -> p.getPatient() == patient).findAny()
						        .orElse(null);
						if (encounter != null) {
							simplifiedProgram.setEncounterIdentifier(encounter.getEncounterId());
						}
					}
					
					List<Encounter> encounters = getDotEncounters(location, patient);
					
					if (encounters != null && encounters.size() > 0) {
						Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), encounters,
						    BotswanaEmrConstants.TB_TREATMENT_TYPE_CONCEPT_UUID, null, null);
						if (obs != null) {
							String name = obs.getValueCoded().getName().getName().split(",")[0];
							simplifiedProgram.setTreatmentPhase(name);
						}
					}
					
					if (!patient.getDead() && completedHtmlForms.contains(tbDotForm)) {
						simplifiedProgram.setStatus("COMPLETED");
					} else {
						simplifiedProgram.setStatus("IN_PROGRESS");
					}
					
					allPatients.add(simplifiedProgram);
					patientList.add(patient);
					
				}
			}
		}
		return allPatients;
		
	}
	
	private List<Encounter> getTodayDotEncounters(Location location) {
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.DOT_ENCOUNTER_TYPE_UUID);
		List<Encounter> encounterList = Context.getService(BotswanaEmrService.class).getEncountersList(new Date(),
		    new Date(), encounterType, 50, location);
		
		return encounterList;
	}
	
	private List<Encounter> getDotEncounters(Location location, Patient patient) {
		EncounterType encounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.DOT_ENCOUNTER_TYPE_UUID);
		
		return Context.getService(BotswanaEmrService.class).getEncountersList(null, null,
		    Collections.singletonList(encounterType), 50, location, patient);
	}
}
