/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.hts;

import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedProficiencyTesting;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProficiencyTestsListingFragmentController {
	
	private final HtsProficiencyTestingService htsProficiencyTestingService = Context
	        .getService(HtsProficiencyTestingService.class);
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
	}
	
	public List<SimpleObject> getProficiencyTestListings(UiUtils ui) {
		List<HtsProficiencyTesting> testings = htsProficiencyTestingService.getList().stream()
		        .filter(pt -> pt.getVoided().equals(false))
		        .sorted(Comparator.comparing(HtsProficiencyTesting::getId).reversed()).collect(Collectors.toList());
		return SimpleObject.fromCollection(getSimplifiedProficiencyTestings(testings), ui, "proficiencyTestingId", "panelId",
		    "receivedBy", "receivedByName", "datePanelReceived", "testingPoint", "resultsDueDate", "published");
	}
	
	public List<SimplifiedProficiencyTesting> getSimplifiedProficiencyTestings(
	        List<HtsProficiencyTesting> htsProficiencyTestings) {
		List<SimplifiedProficiencyTesting> proficiencyTestings = new ArrayList<>();
		for (HtsProficiencyTesting htsProficiencyTesting : htsProficiencyTestings) {
			SimplifiedProficiencyTesting simplifiedProficiencyTesting = BotswanaEmrUtils
			        .getSimplifiedProficiencyTesting(htsProficiencyTesting);
			
			proficiencyTestings.add(simplifiedProficiencyTesting);
		}
		return proficiencyTestings;
	}
	
	public void voidProficiencyTesting(@RequestParam(value = "proficiencyTestingId") Integer proficiencyTestingId) {
		HtsProficiencyTesting htsProficiencyTesting = htsProficiencyTestingService
		        .getHtsProficiencyTesting(proficiencyTestingId);
		if (htsProficiencyTesting != null) {
			htsProficiencyTesting.setVoided(true);
			htsProficiencyTesting.setVoidReason("Deleted");
			htsProficiencyTestingService.saveHtsProficiencyTesting(htsProficiencyTesting);
		}
	}
}
