/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.referrals;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.CareSetting;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.ReferralOrder;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.OrderService;
import org.openmrs.api.PatientService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.comparator.ConceptComparator;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.util.ByFormattedObjectComparator;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CARE_SETTING_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.RECEIVING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_DESTINATION_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ENCOUNTER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getCurrentObs;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getReferral;

public class MakeReferralFragmentController {
	
	protected final Log log = LogFactory.getLog(MakeReferralFragmentController.class);
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	private static final OrderService orderService = Context.getService(OrderService.class);
	
	private final LocationService locationService = Context.getService(LocationService.class);
	
	private final ProviderService providerService = Context.getService(ProviderService.class);
	
	private final ObsService obsService = Context.getService(ObsService.class);
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient) {
		String currentServicePointUuid = String
		        .valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		fragmentModel.addAttribute("currentServicePointUuid", currentServicePointUuid);
		
		String hospital = Context.getAuthenticatedUser().getUserProperty(USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
		
		fragmentModel.addAttribute("hospital", hospital);
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(), true);
		
		List<Location> referringDepartments = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(REFERRING_LOCATION_TAG_NAME));
		if (!referringDepartments.isEmpty()) {
			fragmentModel.addAttribute("referringDepartments", referringDepartments);
		} else {
			fragmentModel.addAttribute("referringDepartments", Collections.EMPTY_LIST);
		}
		
		List<Location> receivingFacilities = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		if (!receivingFacilities.isEmpty()) {
			fragmentModel.addAttribute("receivingFacilities", receivingFacilities);
		} else {
			fragmentModel.addAttribute("receivingFacilities", Collections.EMPTY_LIST);
		}
		
		List<Location> receivingLocations = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(REFERRAL_DESTINATION_LOCATION_TAG_NAME));
		List<Location> receivingDepartments = new ArrayList<>();
		if (!receivingLocations.isEmpty() && !referringDepartments.isEmpty()) {
			receivingDepartments.addAll(receivingLocations);
			receivingDepartments.addAll(referringDepartments);
			fragmentModel.addAttribute("receivingDepartments", receivingDepartments);
		} else {
			fragmentModel.addAttribute("receivingDepartments", Collections.EMPTY_LIST);
		}
		BotswanaEmrUtils.setSingleActiveReferralOrder(fragmentModel, patient, activeVisit);
	}
	
	public void makePatientReferral(UiSessionContext uiSessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "referralNotes", required = false) String referralNotes,
	        @RequestParam(value = "referringFacility") String referringFacility,
	        @RequestParam(value = "referringDepartmentId") Integer referringDepartmentId,
	        @RequestParam(value = "receivingFacilityId") Integer receivingFacilityId,
	        @RequestParam(value = "receivingDepartmentId") Integer receivingDepartmentId,
	        @RequestParam(value = "providerId", required = false) Integer providerId,
	        @RequestParam(value = "reason") String reason) {
		
		final String status = "RECEIVED";
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(), true);
		
		Location referringDepartment = locationService.getLocation(referringDepartmentId);
		Location receivingDepartment = locationService.getLocation(receivingDepartmentId);
		Location receivingFacility = locationService.getLocation(receivingFacilityId);
		
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		if (providerId != null) {
			provider = providerService.getProvider(providerId);
		}
		
		EncounterType referralEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(REFERRAL_ENCOUNTER_TYPE_UUID);
		
		Concept referringDepartmentConcept = conceptService.getConceptByUuid(REFERRING_DEPARTMENT_CONCEPT_UUID);
		
		Concept receivingDepartmentConcept = conceptService.getConceptByUuid(RECEIVING_DEPARTMENT_CONCEPT_UUID);
		
		CareSetting careSetting = Context.getOrderService().getCareSettingByUuid(CARE_SETTING_UUID);
		
		OrderType referralOrderType = Context.getOrderService().getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		
		Order.FulfillerStatus referralStatus = Enum.valueOf(Order.FulfillerStatus.class, status);
		
		Concept reasonConcept = conceptService.getConceptByName(reason);
		
		Encounter referralEncounter = BotswanaEmrUtils.persistEncounter(patient, referralEncounterType,
		    uiSessionContext.getSessionLocation(), activeVisit);
		
		if (referringDepartment != null && receivingDepartment != null) {
			Obs referringDepartmentObs = BotswanaEmrUtils.creatObs(referralEncounter, referringDepartmentConcept);
			referringDepartmentObs.setObsDatetime(new Date());
			referringDepartmentObs.setValueText(referringDepartment.getName());
			referringDepartmentObs.setComment(referringFacility);
			referringDepartmentObs.setValueCoded(referringDepartmentConcept);
			obsService.saveObs(referringDepartmentObs, "Department referring");
			referralEncounter.addObs(referringDepartmentObs);
			
			Obs receivingDepartmentObs = BotswanaEmrUtils.creatObs(referralEncounter, receivingDepartmentConcept);
			receivingDepartmentObs.setObsDatetime(new Date());
			receivingDepartmentObs.setValueText(receivingDepartment.getName());
			receivingDepartmentObs.setComment(receivingFacility.getName());
			receivingDepartmentObs.setValueCoded(receivingDepartmentConcept);
			obsService.saveObs(receivingDepartmentObs, "Department receiving");
			referralEncounter.addObs(receivingDepartmentObs);
			
			ReferralOrder referral = new ReferralOrder();
			referral.setEncounter(referralEncounter);
			referral.setPatient(patient);
			referral.setOrderer(provider);
			referral.setOrderType(referralOrderType);
			referral.setConcept(referringDepartmentConcept);
			referral.setLocation(receivingDepartmentConcept);
			referral.setCreator(Context.getAuthenticatedUser());
			referral.setDateCreated(referralEncounter.getDateCreated());
			referral.setDateActivated(new Date());
			referral.setCareSetting(careSetting);
			if (reasonConcept != null) {
				referral.setOrderReason(reasonConcept);
			} else {
				referral.setOrderReasonNonCoded(reason);
			}
			referral.setFulfillerStatus(referralStatus);
			referral.setCommentToFulfiller(referralNotes);
			
			orderService.saveOrder(referral, null);
			referralEncounter.addOrder(referral);
			log.info("\n Created referral order \n" + referral);
		}
	}
	
	public SimpleObject getReferralData(UiSessionContext uiSessionContext, UiUtils ui,
			@RequestParam(value = "patientId") Integer patientId,
			@SpringBean("patientService") PatientService patientService) {
		SimpleObject response = new SimpleObject();
		Patient patient = patientService.getPatient(patientId);
		
		String currentServicePointUuid = String.valueOf(
				uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT)
		);
		response.put("currentServicePointUuid", currentServicePointUuid);
		
		String hospital = Context.getAuthenticatedUser().getUserProperty(
				USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
				Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital")
		);
		response.put("hospital", hospital);
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(
				patient,
				uiSessionContext.getSessionLocation(),
				true
		);
		
		List<Location> referringDepartments = locationService
				.getLocationsByTag(locationService.getLocationTagByName(REFERRING_LOCATION_TAG_NAME));
		response.put("referringDepartments", formatLocations(referringDepartments));
		
		List<Location> receivingFacilities = locationService
				.getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		response.put("receivingFacilities", formatLocations(receivingFacilities));
		
		List<Location> receivingLocations = locationService
				.getLocationsByTag(locationService.getLocationTagByName(REFERRAL_DESTINATION_LOCATION_TAG_NAME));
		List<Location> receivingDepartments = new ArrayList<>(receivingLocations);
		receivingDepartments.addAll(referringDepartments);
		response.put("receivingDepartments", formatLocations(receivingDepartments));
		
		List<Concept> diagnosisConcepts = conceptService
				.getConceptsByClass(conceptService.getConceptClassByName("Diagnosis"));
		Comparator<Concept> comparator = new ConceptComparator(new ByFormattedObjectComparator(ui));
		diagnosisConcepts.sort(comparator);
		ObjectMapper jsonMapper = new ObjectMapper();
		List<String> diagnosisConceptNames = new ArrayList<>();
		for (Concept diagnosisConcept : diagnosisConcepts) {
			diagnosisConceptNames.add(diagnosisConcept.getName().getName());
		}
		String json = "";
		try {
			json = jsonMapper.writeValueAsString(diagnosisConceptNames);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		response.put("diagnosisConceptNames", json);
		
		return BotswanaEmrUtils.addReferralDetails(response, patient, activeVisit);
	}
	
	private List<SimpleObject> formatLocations(List<Location> locations) {
		return locations.stream().map(location -> {
			SimpleObject locObj = new SimpleObject();
			locObj.put("locationId", location.getLocationId());
			locObj.put("uuid", location.getUuid());
			locObj.put("name", location.getName());
			locObj.put("description", location.getDescription());
			return locObj;
		}).collect(Collectors.toList());
	}
}
