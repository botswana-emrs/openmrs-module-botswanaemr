/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockAdjustment;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import liquibase.util.StringUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StockAdjustmentsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        UiSessionContext uiSessionContext) {
		
		List<SimplifiedStockAdjustment> simplifiedStockAdjustmentList = new ArrayList<>();
		List<StockOperation> allStockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		if (uiSessionContext.getCurrentUser() != null) {
			for (Stockroom stockroom : stockrooms) {
				if (stockroom.getLocation().equals(uiSessionContext.getSessionLocation())) {
					List<StockOperation> stockOperations = new ArrayList<>();
					
					stockOperations = iStockOperationDataService.getOperationsByRoom(stockroom, new PagingInfo(1, 10));
					stockOperations = stockOperations.stream()
					        .filter(e -> e.getInstanceType() == WellKnownOperationTypes.getDisposed()
					                || e.getInstanceType() == WellKnownOperationTypes.getAdjustment())
					        .collect(Collectors.toList());
					allStockOperations.addAll(stockOperations);
				}
			}
		}
		for (StockOperation stockOperation : allStockOperations) {
			SimplifiedStockAdjustment stockAdjustment = new SimplifiedStockAdjustment();
			stockAdjustment.setId(stockOperation.getId());
			stockAdjustment.setOperationDate(
			    BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
			stockAdjustment.setOfficer(String.valueOf(stockOperation.getCreator()));
			stockAdjustment.setStatus(String.valueOf(stockOperation.getStatus()));
			stockAdjustment.setStockOperationType(String.valueOf(stockOperation.getInstanceType().getName()));
			simplifiedStockAdjustmentList.add(stockAdjustment);
		}
		fragmentModel.addAttribute("allStockAdjustments", simplifiedStockAdjustmentList);
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public SimpleObject getAllAdjustments(
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        @RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "search[value]", required = false) String searchValue, UiSessionContext uiSessionContext) {
		
		page = page / pageSize + 1;
		List<SimplifiedStockAdjustment> simplifiedStockAdjustmentList = new ArrayList<>();
		List<StockOperation> allStockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		
		if (uiSessionContext.getCurrentUser() != null) {
			for (Stockroom stockroom : stockrooms) {
				if (stockroom.getLocation().equals(uiSessionContext.getSessionLocation())) {
					List<StockOperation> stockOperations = new ArrayList<>();
					
					stockOperations = iStockOperationDataService.getOperationsByRoom(stockroom,
					    new PagingInfo(page, pageSize));
					
					StockOperationSearch stockOperationSearch = new StockOperationSearch();
					iStockOperationDataService.getOperations(stockOperationSearch);
					if (StringUtil.isEmpty(searchValue)) {
						stockOperations = stockOperations.stream()
						        .filter(e -> (e.getInstanceType().equals(WellKnownOperationTypes.getDisposed())
						                || e.getInstanceType().equals(WellKnownOperationTypes.getAdjustment()))
						                && e.getDescription() != null)
						        .collect(Collectors.toList());
						
					} else {
						stockOperations = stockOperations.stream()
						        .filter(e -> e.getInstanceType().getName().contains(searchValue))
						        .collect(Collectors.toList());
					}
					allStockOperations.addAll(stockOperations);
				}
			}
		}
		for (StockOperation stockOperation : allStockOperations) {
			SimplifiedStockAdjustment stockAdjustment = new SimplifiedStockAdjustment();
			stockAdjustment.setId(stockOperation.getId());
			stockAdjustment.setOperationDate(
			    BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
			stockAdjustment.setOfficer(String.valueOf(stockOperation.getCreator()));
			stockAdjustment.setStatus(String.valueOf(stockOperation.getStatus()));
			stockAdjustment.setComment(String.valueOf(stockOperation.getDescription()));
			stockAdjustment.setStockOperationType(String.valueOf(stockOperation.getInstanceType().getName()));
			simplifiedStockAdjustmentList.add(stockAdjustment);
		}
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("draw", draw);
		// int totalDisplayRecords = allStockOperations.size();
		simpleObject.put("iTotalDisplayRecords", simplifiedStockAdjustmentList.size());
		simpleObject.put("iTotalRecords", simplifiedStockAdjustmentList.size());
		simpleObject.put("aaData", simplifiedStockAdjustmentList);
		
		return simpleObject;
		
	}
}
