/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.Patient;
import org.openmrs.PersonAttributeType;
import org.openmrs.Relationship;
import org.openmrs.api.PersonService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ClientProfileBioDataFragmentController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @FragmentParam(value = "showPrescription", required = false) Boolean showPrescription,
	        @SpringBean("personService") PersonService personService, UiSessionContext sessionContext,
	        FragmentModel fragmentModel) {
		
		PersonAttributeType citizenshipPersonAttributeType = personService
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID);
		PersonAttributeType educationPersonAttributeType = personService
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID);
		List<SimpleObject> nextOfKins = new ArrayList<>();
		List<Relationship> relationships = Context.getPersonService().getRelationships(patient.getPerson(), null, null);
		for (Relationship relationship : relationships) {
			nextOfKins.add(SimpleObject.create("relationshipType", relationship.getRelationshipType().getaIsToB(), "person",
			    relationship.getPersonB()));
		}
		
		if (showPrescription == null) {
			showPrescription = false;
		}
		
		fragmentModel.addAttribute("nextOfKins", nextOfKins);
		fragmentModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(fragmentModel, patient);
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("citizenship", patient.getPerson().getAttribute(citizenshipPersonAttributeType));
		fragmentModel.addAttribute("educationLeveAttainment",
		    patient.getPerson().getAttribute(educationPersonAttributeType));
		fragmentModel.addAttribute("currentVisit",
		    BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), true));
		fragmentModel.addAttribute("showPrescription", showPrescription);
	}
}
