/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.Date;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Person;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.PersonService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_POST_TEST_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_STATUS_RESULT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INDETERMINATE_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PREVIOUSLY_TESTED_QUESTION_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAST_TEST_RESULTS_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAST_TEST_OPTION_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_CONFIRMATORY_TEST_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_PRIOR_TEST_STATUS_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLatestObs;

public class HtsClientProfilePageController {
	
	public static final String PRIOR_TEST_POSITIVE_OUTCOME_CONCEPT_UUID = "138571AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final LabService labService = Context.getService(LabService.class);
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("personService") PersonService personService,
	        @SpringBean("conceptService") ConceptService conceptService, UiUtils ui, final PageModel pageModel,
	        UiSessionContext sessionContext) {
		
		Date today = DateUtil.getStartOfDay(new Date());
		
		List<Person> persons = Collections.singletonList(patient.getPerson());
		List<Concept> concepts = new ArrayList<>();
		concepts.add(conceptService.getConceptByUuid(BotswanaEmrConstants.HIV_TEST_DIAGNOSIS_RESULT_CONCEPT_UUID));
		concepts.add(conceptService.getConceptByUuid(HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID));
		// concepts.add(conceptService.getConceptByUuid(SUPPLEMENTARY_TEST_RESULTS_CONCEPT_UUID));
		
		List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
		
		String htsRegisterFormUuid = BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID;
		String htsVerificationFormUuid = BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID;
		String htsSupplementaryTestFormUuid = BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID;
		String selfTestKitFormUuid = BotswanaEmrConstants.HTS_SELF_TEST_DISTRIBUTION_FORM_UUID;
		String priorTestFormUuid = BotswanaEmrConstants.HTS_DOCUMENT_PRIOR_TEST_FORM_UUID;
		String htsReferralFormUuid = BotswanaEmrConstants.HTS_HIV_TEST_REFERRAL_FORM_UUID;
		String pnsIndexCaseInterviewFormUuid = BotswanaEmrConstants.HTS_PNS_INDEX_FORM_UUID;
		String hivLabTestRequestFormUuid = BotswanaEmrConstants.HIV_LAB_TEST_REQUEST_FORM_UUID;
		
		Form selfTestKitForm = formService.getFormByUuid(selfTestKitFormUuid);
		Form postTestingForm = formService.getFormByUuid(HTS_POST_TEST_FORM_UUID);
		
		List<Encounter> filledSelfTestKitEncounter = encounterService.getEncounters(new EncounterSearchCriteria(patient,
		        null, today, null, null, Arrays.asList(selfTestKitForm), null, null, null, null, false));
		
		List<Encounter> filledPostTestingEncounter = encounterService.getEncounters(new EncounterSearchCriteria(patient,
		        null, today, null, null, Arrays.asList(postTestingForm), null, null, null, null, false));
		
		List<String> preEnrolmentForms = new ArrayList<>();
		List<String> postEnrolmentForms = Arrays.asList(htsRegisterFormUuid, htsVerificationFormUuid, selfTestKitFormUuid,
		    priorTestFormUuid, htsReferralFormUuid);
		
		List<String> listOfFormsToIncludeUuids = new ArrayList<>();
		
		listOfFormsToIncludeUuids.addAll(preEnrolmentForms);
		listOfFormsToIncludeUuids.addAll(postEnrolmentForms);
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		List<HtmlForm> completedHtmlForms = BotswanaEmrUtils.completedHtmlForms(currentPatientVisit, htmlFormEntryService);
		allHtmlForms.removeAll(completedHtmlForms);
		List<HtmlForm> list = new ArrayList<>();
		for (HtmlForm f : allHtmlForms) {
			if (!f.getForm().getRetired()) {
				if (listOfFormsToIncludeUuids.contains(f.getForm().getUuid())) {
					list.add(f);
				}
			}
		}
		
		List<HtmlForm> htsEncounterForms = BotswanaEmrUtils.getHtsEncounterForms(htmlFormEntryService, formService).stream()
		        .filter(Objects::nonNull).collect(Collectors.toList());
		
		List<Encounter> filledEncounterForPatientForCurrentVisit = BotswanaEmrUtils.getHtsEncounters(patient,
		    encounterService, Collections.singletonList(currentPatientVisit), htsEncounterForms);
		
		pageModel.addAttribute("encounters",
		    BotswanaEmrUtils.getHtsEncounters(patient, encounterService, htsEncounterForms));
		
		List<HtmlForm> tmpAvailableForms;
		if (filledEncounterForPatientForCurrentVisit.isEmpty()) {
			tmpAvailableForms = htsEncounterForms;
		} else {
			List<Form> hivForms = filledEncounterForPatientForCurrentVisit.stream().map(Encounter::getForm)
			        .filter(form -> !form.getUuid().equals(HTS_POST_TEST_FORM_UUID)).collect(Collectors.toList());
			tmpAvailableForms = htsEncounterForms.stream().filter(f -> !hivForms.contains(f.getForm()))
			        .collect(Collectors.toList());
		}
		
		//Remove the children of index information and sex contacts information forms
		List<HtmlForm> listOfHtmlFormsToExclude = new ArrayList<>();
		
		HtmlForm childrenOfIndexInformationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID));
		listOfHtmlFormsToExclude.add(childrenOfIndexInformationForm);
		
		HtmlForm sexContactsInformationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_IPV_SCREENING_FORM_UUID));
		listOfHtmlFormsToExclude.add(sexContactsInformationForm);
		
		tmpAvailableForms.removeAll(listOfHtmlFormsToExclude);
		
		// Remove Post Test details form if the registration form is not filled yet
		HtmlForm postTestHtmlForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(HTS_POST_TEST_FORM_UUID));
		EncounterSearchCriteria htsDiagnosticsTestSearchCriteria = new EncounterSearchCriteriaBuilder()
		        .setEncounterTypes(Collections.singletonList(Context.getEncounterService()
		                .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID)))
		        .setPatient(patient).createEncounterSearchCriteria();
		
		if (encounterService.getEncounters(htsDiagnosticsTestSearchCriteria).isEmpty()
		        && tmpAvailableForms.contains(postTestHtmlForm)) {
			tmpAvailableForms.remove(postTestHtmlForm);
		}
		
		boolean isHivPositive = false;
		boolean isSelfTested = false;
		
		// Determine the hiv status by checking the status from the verification, supplementary test and prior test encounters
		EncounterSearchCriteria searchCriteria = new EncounterSearchCriteria(patient, null, null, null, null,
		        Arrays.asList(formService.getFormByUuid(htsVerificationFormUuid),
		            formService.getFormByUuid(htsSupplementaryTestFormUuid), formService.getFormByUuid(priorTestFormUuid)),
		        null, null, null, null, false);
		List<Encounter> hivStatusEncounters = encounterService.getEncounters(searchCriteria);
		if (!hivStatusEncounters.isEmpty()) {
			List<Concept> hivStatusConcepts = new ArrayList<>();
			hivStatusConcepts
			        .add(conceptService.getConceptByUuid(BotswanaEmrConstants.SUPPLEMENTARY_TEST_RESULTS_CONCEPT_UUID));
			hivStatusConcepts.add(conceptService.getConceptByUuid(HTS_PRIOR_TEST_STATUS_CONCEPT_UUID));
			hivStatusConcepts.add(conceptService.getConceptByUuid(HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID));
			List<Obs> hivStatusObsList = Context.getObsService().getObservations(
			    Collections.singletonList(patient.getPerson()), hivStatusEncounters, hivStatusConcepts, null, null, null,
			    null, 1, null, null, null, false);
			
			if (!hivStatusObsList.isEmpty()) {
				Comparator<Obs> obsDatetimeComparator = Comparator.comparing(Obs::getObsDatetime);
				Obs lastHivStatus = hivStatusObsList.stream().max(obsDatetimeComparator).get();
				
				if (lastHivStatus.getEncounter().getEncounterType().getUuid()
				        .equals(BotswanaEmrConstants.HTS_PRIOR_ENCOUNTER_TYPE)) {
					if (lastHivStatus.getValueCoded().getUuid()
					        .equals(BotswanaEmrConstants.VERIFICATION_POSITIVE_CONCEPT_UUID)) {
						isHivPositive = true;
					}
				} else {
					if (lastHivStatus.getValueCoded().getUuid().equals(BotswanaEmrConstants.HIV_STATUS_POSITIVE)
					        || lastHivStatus.getValueCoded().getUuid()
					                .equals(BotswanaEmrConstants.VERIFICATION_POSITIVE_CONCEPT_UUID)) {
						isHivPositive = true;
					}
				}
				
				if (lastHivStatus.getValueCoded().getUuid().equals(BotswanaEmrConstants.SELF_TESTING_POSITIVE_CONCEPT_UUID)
				        || lastHivStatus.getValueCoded().getUuid()
				                .equals(BotswanaEmrConstants.SELF_TESTING_NEGATIVE_CONCEPT_UUID)) {
					isSelfTested = true;
				}
			}
		}
		
		// Remove HTS Referral form if patient is negative
		HtmlForm htsReferralHtmlForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_HIV_TEST_REFERRAL_FORM_UUID));
		
		if (!isHivPositive) {
			tmpAvailableForms.remove(htsReferralHtmlForm);
		}
		
		Obs priorTestResultsObs = getLatestObs(patient.getPerson(), null, PREVIOUSLY_TESTED_QUESTION_CONCEPT_UUID, null,
		    null);
		Obs lastDiagnosticsHivStatusResultsObs = getLatestObs(patient.getPerson(), null, HIV_STATUS_RESULT_CONCEPT_UUID,
		    null, null);
		Obs lastTestResultsObs = getLatestObs(patient.getPerson(), null, LAST_TEST_RESULTS_CONCEPT_UUID, null, null);
		
		// Include the pns if a positive outcome from both the new diagnostic form, prior diagnostic.
		boolean hasCompletedDiagnosticsTesting = false;
		if (lastDiagnosticsHivStatusResultsObs != null) {
			//check last obs if positive for new diagnostic result
			if (lastDiagnosticsHivStatusResultsObs.getValueCoded().getUuid()
			        .equals(BotswanaEmrConstants.HIV_STATUS_POSITIVE)) {
				hasCompletedDiagnosticsTesting = true;
			}
		}
		
		if (priorTestResultsObs != null) {
			//check last obs if positive for new prior test result
			if (priorTestResultsObs.getValueCoded().getUuid()
			        .equals(BotswanaEmrConstants.HTS_POSITIVE_TEST_RESULTS_CONCEPT_UUID)) {
				hasCompletedDiagnosticsTesting = true;
			}
		}
		
		boolean hasCompletedSupplementaryForm;
		
		List<Obs> indeterminateResultsObs = Context.getObsService().getObservations(persons, null, concepts, null, null,
		    null, null, 1, null, null, null, false);
		
		// check if the patient has an indeterminate outcome and has not completed the supplementary form
		boolean inderminateOutcome = false;
		if (!indeterminateResultsObs.isEmpty()) {
			// check if the obs is the latest obs
			Comparator<Obs> obsDatetimeComparator = Comparator.comparing(Obs::getObsDatetime);
			Obs lastIndeterminateObs = indeterminateResultsObs.stream().max(obsDatetimeComparator).orElse(null);
			if (lastIndeterminateObs != null
			        && lastIndeterminateObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)) {
				
				hasCompletedSupplementaryForm = hasCompletedSupplementaryForm(patient, lastIndeterminateObs.getEncounter());
				
				if (!hasCompletedSupplementaryForm) {
					inderminateOutcome = true;
				}
			}
		}
		
		// Fetch the test option result and determine if the patient is due for lab testing
		boolean isDueForLabTesting = false;
		Concept testOptionConcept = conceptService.getConceptByUuid("881fd68e-00b8-471b-a237-c5bd5aa52571");
		Concept labTestConcept = conceptService.getConceptByUuid("1042AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		List<Obs> htsLabObs = Context.getObsService().getObservations(persons, null,
		    Collections.singletonList(testOptionConcept), null, null, null, null, 1, null, null,
		    encounter != null ? encounter.getEncounterDatetime() : null, false);
		if (!htsLabObs.isEmpty()) {
			// check if the obs is the latest obs
			Comparator<Obs> obsDatetimeComparator = Comparator.comparing(Obs::getObsDatetime);
			Obs lastHtsLabObs = htsLabObs.stream().max(obsDatetimeComparator).orElse(null);
			if (lastHtsLabObs != null && lastHtsLabObs.getValueCoded().getUuid().equals(labTestConcept.getUuid())) {
				isDueForLabTesting = true;
			}
		}
		
		pageModel.addAttribute("isDueForLabTesting", isDueForLabTesting);
		pageModel.addAttribute("indeterminate", inderminateOutcome);
		
		// check if patient's lab test results is "No results obtained"
		boolean noResultsObtained = false;
		if (lastTestResultsObs != null) {
			if (lastTestResultsObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.NO_RESULTS_OBTAINED_CONCEPT_UUID)) {
				noResultsObtained = true;
			}
		}
		
		pageModel.addAttribute("noResultsObtained", noResultsObtained);
		pageModel.addAttribute("hasCompletedDiagnosticsTesting", hasCompletedDiagnosticsTesting);
		
		boolean hasActiveVisit = currentPatientVisit != null;
		
		pageModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "hts/htsClientProfile", patient, currentPatientVisit, ui)));
		
		pageModel.addAttribute("availableForms", tmpAvailableForms);
		List<HtmlForm> hivTestingForms = null;
		HtmlForm selfTestingKitForm = null;
		HtmlForm htsReferralForm = null;
		HtmlForm pnsIndexCaseInterviewForm = null;
		boolean hasIndeterminateOutcome = false;
		Obs verificationOutcomeObs;
		Obs newDiagnosticOutcomeObs;
		Obs newDiagnosticTestOptionObs;
		Obs priorTestOutcomeObs;
		
		if (hasActiveVisit) {
			verificationOutcomeObs = getLatestObs(currentPatientVisit, HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID);
			newDiagnosticOutcomeObs = getLatestObs(currentPatientVisit, HIV_STATUS_RESULT_CONCEPT_UUID);
			newDiagnosticTestOptionObs = getLatestObs(currentPatientVisit, LAST_TEST_OPTION_CONCEPT_UUID);
			priorTestOutcomeObs = getLatestObs(currentPatientVisit, HTS_PRIOR_TEST_STATUS_CONCEPT_UUID);
		} else {
			newDiagnosticOutcomeObs = null;
			verificationOutcomeObs = null;
			newDiagnosticTestOptionObs = null;
			priorTestOutcomeObs = null;
		}
		
		Encounter htsOutcomeEncounter = null;
		if (!tmpAvailableForms.isEmpty()) {
			hivTestingForms = tmpAvailableForms.stream()
			        .filter(f -> f.getForm().getUuid().equals(priorTestFormUuid)
			                || f.getForm().getUuid().equals(htsRegisterFormUuid)
			                || f.getForm().getUuid().equals(HTS_POST_TEST_FORM_UUID)
			                || f.getForm().getUuid().equals(htsVerificationFormUuid) ||
							// Include supplementary form when prior result is indeterminate
			                (priorTestOutcomeObs != null
			                        && priorTestOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)
			                        && f.getForm().getUuid().equals(htsSupplementaryTestFormUuid))
			                ||
							// Include form for supplementary when verification or diagnostic outcome is indeterminate or not matching diagnostic and verification
							// also include supplementary when newDiagnosticTestOptionObs was DNA-PCR
			                (verificationOutcomeObs != null && newDiagnosticOutcomeObs != null
			                        && !newDiagnosticOutcomeObs.getValueCoded().getUuid()
			                                .equals(verificationOutcomeObs.getValueCoded().getUuid())
			                        && f.getForm().getUuid().equals(htsSupplementaryTestFormUuid))
			                || (verificationOutcomeObs != null
			                        && verificationOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)
			                        && f.getForm().getUuid().equals(htsSupplementaryTestFormUuid))
			                || (newDiagnosticOutcomeObs != null
			                        && newDiagnosticOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)
			                        && f.getForm().getUuid().equals(htsSupplementaryTestFormUuid))
			                || (newDiagnosticTestOptionObs != null
			                        && newDiagnosticTestOptionObs.getValueCoded().getUuid()
			                                .equals(HIV_CONFIRMATORY_TEST_CONCEPT_UUID)
			                        && f.getForm().getUuid().equals(htsSupplementaryTestFormUuid)))
			        .collect(Collectors.toList());
							
			if (!hivTestingForms.isEmpty()) {
				hivTestingForms = hivTestingForms.stream().distinct().collect(Collectors.toList());
				Comparator<HtmlForm> htmlFormComparator = Comparator.comparing(HtmlForm::getForm,
				    Comparator.comparing(Form::getName));
				hivTestingForms.sort(htmlFormComparator);
			}
			
			if (verificationOutcomeObs != null && newDiagnosticOutcomeObs != null) {
				if (!newDiagnosticOutcomeObs.getValueCoded().getUuid()
				        .equals(verificationOutcomeObs.getValueCoded().getUuid())
				        && !newDiagnosticOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)) {
					hasIndeterminateOutcome = true;
					htsOutcomeEncounter = verificationOutcomeObs.getEncounter();
				}
			} else if (verificationOutcomeObs != null) {
				if (verificationOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)) {
					hasIndeterminateOutcome = true;
					htsOutcomeEncounter = verificationOutcomeObs.getEncounter();
					
				}
			} else if (newDiagnosticOutcomeObs != null) {
				if (newDiagnosticOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)) {
					hasIndeterminateOutcome = true;
					htsOutcomeEncounter = newDiagnosticOutcomeObs.getEncounter();
					
				}
			}
			
			if (newDiagnosticTestOptionObs != null) {
				if (newDiagnosticTestOptionObs.getValueCoded().getUuid().equals(HIV_CONFIRMATORY_TEST_CONCEPT_UUID)) {
					hasIndeterminateOutcome = true;
					htsOutcomeEncounter = newDiagnosticTestOptionObs.getEncounter();
				}
			}
			
			if (priorTestOutcomeObs != null) {
				if (priorTestOutcomeObs.getValueCoded().getUuid().equals(INDETERMINATE_CONCEPT_UUID)) {
					hasIndeterminateOutcome = true;
					htsOutcomeEncounter = priorTestOutcomeObs.getEncounter();
					// remove new diagnostic form and the new verification test form if prior outcome is indeterminate
					hivTestingForms = hivTestingForms.stream()
					        .filter(f -> !f.getForm().getUuid().contains(htsRegisterFormUuid))
					        .filter(f -> !f.getForm().getUuid().contains(htsVerificationFormUuid))
					        .collect(Collectors.toList());
				}
			}
			
			selfTestingKitForm = htmlFormEntryService.getHtmlFormByForm(formService.getFormByUuid(selfTestKitFormUuid));
			htsReferralForm = tmpAvailableForms.stream().filter(f -> f.getForm().getUuid().equals(htsReferralFormUuid))
			        .findAny().orElse(null);
			pnsIndexCaseInterviewForm = tmpAvailableForms.stream()
			        .filter(f -> f.getForm().getUuid().equals(pnsIndexCaseInterviewFormUuid)).findAny().orElse(null);
		}
		
		// Remove the hts referral form if the patient is not positive
		if (!isHivPositive) {
			// if self test kit form is filled and post test form is not filled, set hts referral form to null
			if (filledPostTestingEncounter.isEmpty() && !filledSelfTestKitEncounter.isEmpty()) {
				htsReferralForm = null;
			}
		}
		
		pageModel.addAttribute("clientSelftested", isSelfTested);
		
		Encounter labOrderEncounter = BotswanaEmrUtils.getLabOrderEncounter(patient);
		int labOrderEncounterId = 0;
		String labOrderEncounterStatus = null;
		
		if (labOrderEncounter != null) {
			LabTest labTestOrder = labService.getLaboratoryTest(labOrderEncounter);
			labOrderEncounterId = labOrderEncounter.getId();
			labOrderEncounterStatus = labTestOrder.getStatus();
		}
		
		pageModel.addAttribute("hivTestingAndCounsellingForms", hivTestingForms);
		pageModel.addAttribute("selftestkit", selfTestingKitForm);
		pageModel.addAttribute("htsReferral", htsReferralForm);
		pageModel.addAttribute("indexCaseInterview", pnsIndexCaseInterviewForm);
		pageModel.addAttribute("hivLabTestRequestFormUuid", hivLabTestRequestFormUuid);
		pageModel.addAttribute("availableForms", tmpAvailableForms);
		pageModel.addAttribute("isHivPositive", isHivPositive);
		pageModel.addAttribute("hasIndeterminateOutcome", hasIndeterminateOutcome);
		pageModel.addAttribute("labOrderEncounterId", labOrderEncounterId);
		pageModel.addAttribute("labOrderEncounterStatus", labOrderEncounterStatus);
		pageModel.addAttribute("htsOutcomeEncounter", htsOutcomeEncounter);
		
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("encounter", encounter);
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "hts/htsClientProfile", patient, currentPatientVisit, ui)));
	}
	
	private static boolean hasCompletedSupplementaryForm(Patient patient, Encounter lastIndeterminateObsEncounter) {
		// Determine if the patient has completed the supplementary form
		boolean hasCompletedSupplementaryForm = false;
		EncounterSearchCriteria searchCriteria = new EncounterSearchCriteria(patient, null,
		        lastIndeterminateObsEncounter.getEncounterDatetime(), null, null,
		        Collections.singletonList(
		            Context.getFormService().getFormByUuid(BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID)),
		        null, null, null, null, false);
		List<Encounter> supplementaryTestEncounters = Context.getEncounterService().getEncounters(searchCriteria);
		if (!supplementaryTestEncounters.isEmpty()) {
			hasCompletedSupplementaryForm = true;
		}
		return hasCompletedSupplementaryForm;
	}
	
}
