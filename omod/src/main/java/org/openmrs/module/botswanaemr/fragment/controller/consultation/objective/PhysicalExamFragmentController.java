/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.objective;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Person;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class PhysicalExamFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam(value = "visitId") Visit visit,
	        @RequestParam("patientId") Patient person, UiSessionContext uiSessionContext) {
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		List<Obs> physicalExamGroupingObs = botswanaEmrService.getObservation(person, visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PHYSICAL_EXAM_GROUPING_CONCEPT),
		    uiSessionContext.getSessionLocation(), limitFetch);
		List<SimpleObject> physicalExamObs = new ArrayList<>();
		for (Obs obs : physicalExamGroupingObs) {
			physicalExamObs.add(BotswanaEmrUtils.getPhysicalExamObs(obs));
		}
		fragmentModel.addAttribute("additionalExams", physicalExamObs);
		// Use the following line to get the last physical examination if at least one exists
		//    Obs lastPhysicalExamObs = physicalExamObs.size()>0? physicalExamObs.get(0): null;
		//    fragmentModel.addAttribute("lastPhysicalExaminations", lastPhysicalExamObs);
		fragmentModel.addAttribute("patient", Context.getPatientService().getPatient(person.getPersonId()));
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
	}
	
	public void addPhysicalExamination(@RequestParam("patientId") Patient patient,
	        @RequestParam("physicalExam") String physicalExam, @RequestParam(value = "heent", required = false) String heent,
	        @RequestParam(value = "respiratory", required = false) String respiratory,
	        @RequestParam(value = "cardiovascular", required = false) String cardiovascular,
	        @RequestParam(value = "skin", required = false) String skin,
	        @RequestParam(value = "endocrine", required = false) String endocrine,
	        @RequestParam(value = "gi", required = false) String gi,
	        @RequestParam(value = "womenHealth", required = false) String womenHealth,
	        @RequestParam(value = "cns", required = false) String cns,
	        @RequestParam(value = "growth", required = false) String growth,
	        @RequestParam(value = "uroGenital", required = false) String uroGenital,
	        @RequestParam(value = "locationId", required = false) Location location,
	        @RequestParam(value = "radiology", required = false) String radiology,
	        @RequestParam(value = "ecg", required = false) String ecg,
	        @RequestParam(value = "ultrasound", required = false) String ultrasound) {
		EncounterType consultationEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE);
		Encounter consultationsEncounter = BotswanaEmrUtils.createEncounter(patient, consultationEncounterType, location,
		    null);
		
		ConceptService cs = Context.getConceptService();
		
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.PHYSICAL_EXAM_GROUPING_CONCEPT);
		Obs groupingObs = BotswanaEmrUtils.creatObs(consultationsEncounter, groupingConcept);
		
		if (StringUtils.isNotBlank(physicalExam)) {
			Obs physicalExamObs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PHYSICAL_EXAMINATION_CONCEPT_UUID));
			physicalExamObs.setValueText(physicalExam);
			
			if (physicalExamObs.getConcept() != null) {
				groupingObs.addGroupMember(physicalExamObs);
			}
		}
		if (StringUtils.isNotBlank(heent)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.HEENT_CONCEPT_UUID));
			obs.setValueText(heent);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(respiratory)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CHEST_EXAMINATION_CONCEPT_UUID));
			obs.setValueText(respiratory);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(cardiovascular)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CARDIOVASCULAR_CONCEPT_UUID));
			obs.setValueText(cardiovascular);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(skin)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SKIN_CONCEPT_UUID));
			obs.setValueText(skin);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(endocrine)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ENDOCRINE_CONCEPT_UUID));
			obs.setValueText(endocrine);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(gi)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.GI_CONCEPT_UUID));
			obs.setValueText(gi);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(womenHealth)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.GU_WOMEN_HEALTH_CONCEPT_UUID));
			obs.setValueText(womenHealth);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(cns)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CNS_CONCEPT_UUID));
			obs.setValueText(cns);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(growth)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.GROWTH_DEVELOPMENT_CONCEPT_UUID));
			obs.setValueText(growth);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		if (StringUtils.isNotBlank(uroGenital)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.URO_GENITAL_CONCEPT_ID));
			obs.setValueText(uroGenital);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		
		//extra fields:
		if (StringUtils.isNotBlank(radiology)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.RADIOLOGY_CONCEPT_UUID));
			obs.setValueText(radiology);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		
		if (StringUtils.isNotBlank(ecg)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ECG_CONCEPT_UUID));
			obs.setValueText(ecg);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		
		if (StringUtils.isNotBlank(ultrasound)) {
			Obs obs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ULTRASOUND_CONCEPT_UUID));
			obs.setValueText(ultrasound);
			if (obs.getConcept() != null) {
				groupingObs.addGroupMember(obs);
			}
		}
		consultationsEncounter.addObs(groupingObs);
		//Save an encounter
		if (consultationsEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(consultationsEncounter);
		}
		
	}
	
	public void editPhysicalExam(@RequestParam("lastPhysicalObsId") Integer lastPhysicalObsId,
	        @RequestParam("obsGroupId") Integer obsGroupId, @RequestParam("editPhysicalExam") String editPhysicalExam,
	        @RequestParam(value = "editHeent", required = false) String editHeent,
	        @RequestParam(value = "editRespiratory", required = false) String editRespiratory,
	        @RequestParam(value = "editCardiovascular", required = false) String editCardiovascular,
	        @RequestParam(value = "editSkin", required = false) String editSkin,
	        @RequestParam(value = "editEndocrine", required = false) String editEndocrine,
	        @RequestParam(value = "editGi", required = false) String editGi,
	        @RequestParam(value = "editWomenHealth", required = false) String editWomenHealth,
	        @RequestParam(value = "editCns", required = false) String editCns,
	        @RequestParam(value = "editGrowth", required = false) String editGrowth,
	        @RequestParam(value = "editUroGenital", required = false) String editUroGenital,
	        @RequestParam(value = "editRadiology", required = false) String editRadiology,
	        @RequestParam(value = "editEcg", required = false) String editEcg,
	        @RequestParam(value = "editUltrasound", required = false) String editUltrasound) {
		ObsService os = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		if (lastPhysicalObsId != null) {
			Obs groupingObs = os.getObs(lastPhysicalObsId);
			
			if (groupingObs != null) {
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.PHYSICAL_EXAMINATION_CONCEPT_UUID,
				    editPhysicalExam);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.HEENT_CONCEPT_UUID, editHeent);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.CHEST_EXAMINATION_CONCEPT_UUID, editRespiratory);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.CARDIOVASCULAR_CONCEPT_UUID, editCardiovascular);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.SKIN_CONCEPT_UUID, editSkin);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.ENDOCRINE_CONCEPT_UUID, editEndocrine);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.GI_CONCEPT_UUID, editGi);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.GU_WOMEN_HEALTH_CONCEPT_UUID, editWomenHealth);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.CNS_CONCEPT_UUID, editCns);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.GROWTH_DEVELOPMENT_CONCEPT_UUID, editGrowth);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.URO_GENITAL_CONCEPT_ID, editUroGenital);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.RADIOLOGY_CONCEPT_UUID, editRadiology);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.ECG_CONCEPT_UUID, editEcg);
				findChildObsByConceptUuid(groupingObs, BotswanaEmrConstants.ULTRASOUND_CONCEPT_UUID, editUltrasound);
			}
			
			//commit the edited obs in the database
			os.saveObs(groupingObs, "Edited Physical examination");
			
		}
	}
	
	private void findChildObsByConceptUuid(Obs obs, String childConceptUuid, String val) {
		if (StringUtils.isEmpty(val)) {
			return;
		}
		Optional<Obs> foundChild = obs.getGroupMembers().stream()
		        .filter(child -> child.getConcept().getUuid().equals(childConceptUuid)).findFirst();
		Obs childObs = foundChild.orElseGet(
		    () -> BotswanaEmrUtils.creatObs(obs.getEncounter(), BotswanaEmrUtils.getConcept(childConceptUuid)));
		childObs.setValueText(val);
		if (childObs.getObsId() == null) {
			obs.addGroupMember(childObs);
		}
		
	}
	
	public List<SimpleObject> getPhysicalExams(@RequestParam(value = "patientId") Integer patientId,
			@RequestParam(value = "visitId") Integer visitId, @SpringBean("patientService") PatientService patientService,
			@SpringBean("visitService") VisitService visitService, UiUtils ui, UiSessionContext uiSessionContext) {
		
		Patient person = patientService.getPatient(patientId);
		Visit visit = visitService.getVisit(visitId);
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		List<Obs> physicalExamGroupingObs = botswanaEmrService.getObservation(person, visit,
				BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PHYSICAL_EXAM_GROUPING_CONCEPT),
				uiSessionContext.getSessionLocation(), limitFetch);
		List<SimpleObject> physicalExamObs = new ArrayList<>();
		for (Obs obs : physicalExamGroupingObs) {
			physicalExamObs.add(getPhysicalExamObservations(obs));
		}
		
		SimpleObject response = new SimpleObject();
		response.put("additionalExams", physicalExamObs);
		return Collections.singletonList(response);
		
	}
	
	public static SimpleObject getPhysicalExamObservations(Obs groupObs) {
		SimpleObject simpleObject = new SimpleObject();
		
		for (Obs obs : groupObs.getGroupMembers(false)) {
			String valueText = obs.getValueAsString(Context.getLocale());
			
			switch (obs.getConcept().getUuid()) {
				case BotswanaEmrConstants.PHYSICAL_EXAMINATION_CONCEPT_UUID:
					simpleObject.put("general", valueText);
					break;
				case BotswanaEmrConstants.HEENT_CONCEPT_UUID:
					simpleObject.put("heent", valueText);
					break;
				case BotswanaEmrConstants.CHEST_EXAMINATION_CONCEPT_UUID:
					simpleObject.put("respiratory", valueText);
					break;
				case BotswanaEmrConstants.CARDIOVASCULAR_CONCEPT_UUID:
					simpleObject.put("cardiovascular", valueText);
					break;
				case BotswanaEmrConstants.SKIN_CONCEPT_UUID:
					simpleObject.put("skin", valueText);
					break;
				case BotswanaEmrConstants.ENDOCRINE_CONCEPT_UUID:
					simpleObject.put("endocrine", valueText);
					break;
				case BotswanaEmrConstants.GI_CONCEPT_UUID:
					simpleObject.put("gi", valueText);
					break;
				case BotswanaEmrConstants.GU_WOMEN_HEALTH_CONCEPT_UUID:
					simpleObject.put("womenHealth", valueText);
					break;
				case BotswanaEmrConstants.CNS_CONCEPT_UUID:
					simpleObject.put("cns", valueText);
					break;
				case BotswanaEmrConstants.GROWTH_DEVELOPMENT_CONCEPT_UUID:
					simpleObject.put("growth", valueText);
					break;
				case BotswanaEmrConstants.URO_GENITAL_CONCEPT_ID:
					simpleObject.put("uroGenital", valueText);
					break;
				case BotswanaEmrConstants.RADIOLOGY_CONCEPT_UUID:
					simpleObject.put("radiology", valueText);
					break;
				case BotswanaEmrConstants.ECG_CONCEPT_UUID:
					simpleObject.put("ecg", valueText);
					break;
				case BotswanaEmrConstants.ULTRASOUND_CONCEPT_UUID:
					simpleObject.put("ultrasound", valueText);
					break;
				default:
					break;
			}
		}
		simpleObject.put("groupingObsId", groupObs.getObsId());
		
		return simpleObject;
	}
}
