/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.list;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.SimplifiedEncounter;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ConsultationListFragmentController {
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
	}
	
	public SimpleObject getPatientConsultationList(UiSessionContext uiSessionContext, UiUtils uiUtils,
	        @RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "search[value]", required = false) String dateRangeString) {
		
		Location sessionLocation = uiSessionContext.getSessionLocation();
		List<SimpleObject> simplifiedEncounters;
		Date startDate = null;
		Date endDate = null;
		if (StringUtils.isNotBlank(dateRangeString)) {
			String startDateString = dateRangeString.split(" , ")[0];
			if (StringUtils.isNotBlank(startDateString)) {
				startDate = DateUtil.parseDate(startDateString, "yyyy-MM-dd");
			}
			
			String endDateString = dateRangeString.split(" , ")[1];
			if (StringUtils.isNotBlank(endDateString)) {
				endDate = DateUtil.parseDate(endDateString, "yyyy-MM-dd");
			}
		}
		Date start = startDate == null ? DateUtil.getStartOfDay(new Date()) : startDate;
		Date end = endDate == null ? DateUtil.getEndOfDay(new Date()) : endDate;
		List<Encounter> consultationEncounters = BotswanaEmrUtils.getAllConsultations(start, end, null, sessionLocation,
		    new PagingInfo(page, pageSize));
		simplifiedEncounters = SimpleObject.fromCollection(getSimplifiedEncounters(consultationEncounters), uiUtils,
		    "visitId", "encounterId", "status", "patientId", "pin", "patientNames", "patientAge", "patientGender",
		    "patientDateOfBirth", "patientRegistrationDate", "consultationDate");
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("draw", draw);
		int totalDisplayRecords = BotswanaEmrUtils.getAllConsultations(start, end, null, sessionLocation).size();
		simpleObject.put("iTotalDisplayRecords", totalDisplayRecords);
		simpleObject.put("iTotalRecords", totalDisplayRecords);
		simpleObject.put("aaData", simplifiedEncounters);
		return simpleObject;
	}
	
	private List<SimplifiedEncounter> getSimplifiedEncounters(List<Encounter> encounters) {
		List<SimplifiedEncounter> consultationEncounters = new ArrayList<>();
		SimplifiedEncounter simplifiedEncounter;
		if (encounters != null) {
			for (Encounter encounter : encounters) {
				simplifiedEncounter = new SimplifiedEncounter();
				simplifiedEncounter.setEncounterId(encounter.getEncounterId());
				simplifiedEncounter.setVisitId(encounter.getVisit().getVisitId());
				simplifiedEncounter.setStatus(encounter.getPatient().getDead().equals(false) ? "Active" : "Expired");
				simplifiedEncounter.setPatientId(encounter.getPatient().getPatientId());
				simplifiedEncounter.setPin(encounter.getPatient()
				        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
				simplifiedEncounter
				        .setPatientNames(BotswanaEmrUtils.formatPersonName(encounter.getPatient().getPersonName()));
				simplifiedEncounter
				        .setPatientGender(encounter.getPatient().getPerson().getGender().equals("M") ? "Male" : "Female");
				simplifiedEncounter.setPatientAge(String.valueOf(encounter.getPatient().getAge()));
				simplifiedEncounter.setPatientRegistrationDate(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getPatient().getDateCreated(), "dd-MM-yyyy"));
				simplifiedEncounter.setPatientDateOfBirth(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getPatient().getBirthdate(), "dd-MM-yyyy"));
				simplifiedEncounter.setConsultationDate(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getEncounterDatetime(), "dd-MM-yyyy"));
				consultationEncounters.add(simplifiedEncounter);
			}
		}
		return consultationEncounters;
	}
}
