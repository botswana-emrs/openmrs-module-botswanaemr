/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.srh;

import org.jetbrains.annotations.NotNull;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterSearchCriteriaUsingEncounterTypes;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.sortEncountersByEncounterDatetime;

public class PartographFragmentController {
	
	public void controller(FragmentModel model, @RequestParam("patientId") Patient patient) {
		model.addAttribute("patient", patient);
	}
	
	public SimpleObject getPartographData(@RequestParam("patientId") String patientId) throws ParseException {
		SimpleObject simpleObject = new SimpleObject();
		List<String> timeRanges = new ArrayList<>();
		List<PartographObject> fetalHeartRates = new ArrayList<>();
		List<PartographObject> cervixSizes = new ArrayList<>();
		List<PartographObject> descentsOfHead = new ArrayList<>();
		List<PartographObject> contractions = new ArrayList<>();
		List<PartographObject> pulses = new ArrayList<>();
		List<PartographObject> diastolic = new ArrayList<>();
		List<PartographObject> systolic = new ArrayList<>();
		List<PartographObject> temperatures = new ArrayList<>();
		List<PartographObject> weights = new ArrayList<>();
		List<PartographObject> urineVolumes = new ArrayList<>();
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		EncounterService encounterService = Context.getEncounterService();
		if (patient != null) {
			List<Encounter> labourAndDeliveryEncounters = encounterService.getEncounters(
			    getEncounterSearchCriteriaUsingEncounterTypes(patient,
			        Arrays.asList(
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.PARTOGRAPH_OBSERVATION_ENCOUNTER_TYPE_UUID),
			            BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.LABOUR_AND_DELIVERY_ENCOUNTER_TYPE_UUID))));
			List<Encounter> encountersWithPartographData = new ArrayList<>();
			if (labourAndDeliveryEncounters.size() > 0) {
				for (Encounter encounter : labourAndDeliveryEncounters) {
					Set<Obs> obsSet = encounter.getAllObs(false);
					for (Obs obs : obsSet) {
						if (BotswanaEmrUtils.getPartographConcepts().contains(obs.getConcept())) {
							encountersWithPartographData.add(encounter);
							break;
						}
					}
				}
				if (encountersWithPartographData.size() > 0) {
					encountersWithPartographData = sortEncountersByEncounterDatetime(encountersWithPartographData);
				}
			}
			
			for (Encounter encounter : encountersWithPartographData) {
				String dateWithTime = encounter.getEncounterDatetime().toString();
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateWithTime);
				String time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
				timeRanges.add(time);
				List<Obs> partographDataObs = Context.getObsService().getObservations(
				    Collections.singletonList(patient.getPerson()), Collections.singletonList(encounter),
				    BotswanaEmrUtils.getPartographConcepts(), null, null, null, null, null, null, null, null, false, null);
				for (Obs obs : partographDataObs) {
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.FETAL_HEART_RATE)) {
						PartographObject fhr = getPartographObject(time, obs.getValueNumeric());
						fetalHeartRates.add(fhr);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVIX_SIZE)) {
						PartographObject cxs = getPartographObject(time, obs.getValueNumeric());
						cervixSizes.add(cxs);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.DECENT_OF_THE_HEAD)) {
						Double valueNumeric = obs.getValueNumeric();
						if (valueNumeric == 0) {
							PartographObject doh = getPartographObject(time, 3.0);
							descentsOfHead.add(doh);
						} else if (valueNumeric == 1) {
							PartographObject doh = getPartographObject(time, 2.0);
							descentsOfHead.add(doh);
						} else if (valueNumeric == 2) {
							PartographObject doh = getPartographObject(time, 1.0);
							descentsOfHead.add(doh);
						} else if (valueNumeric == 3) {
							PartographObject doh = getPartographObject(time, 0.0);
							descentsOfHead.add(doh);
						} else if (valueNumeric == -1 || valueNumeric == -2) {
							PartographObject doh = getPartographObject(time, 4.0);
							descentsOfHead.add(doh);
						} else if (valueNumeric == -3 || valueNumeric == -4) {
							PartographObject doh = getPartographObject(time, 5.0);
							descentsOfHead.add(doh);
						}
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CONTRACTIONS_PER_TEN_MINUTES)) {
						PartographObject cnt = getPartographObject(time, obs.getValueNumeric());
						contractions.add(cnt);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.PULSE)) {
						PartographObject pls = getPartographObject(time, obs.getValueNumeric());
						pulses.add(pls);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.DIASTOLIC_BP)) {
						PartographObject dtc = getPartographObject(time, obs.getValueNumeric());
						diastolic.add(dtc);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.SYSTOLIC_BP)) {
						PartographObject stc = getPartographObject(time, obs.getValueNumeric());
						systolic.add(stc);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.TEMPERATURE)) {
						PartographObject tmp = getPartographObject(time, obs.getValueNumeric());
						temperatures.add(tmp);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.URINE)) {
						PartographObject urn = getPartographObject(time, obs.getValueNumeric());
						urineVolumes.add(urn);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.WEIGHT)) {
						PartographObject tmp = getPartographObject(time, obs.getValueNumeric());
						weights.add(tmp);
					}
				}
			}
		}
		simpleObject.put("fetalHeartRate", fetalHeartRates);
		simpleObject.put("cervixSize", cervixSizes);
		simpleObject.put("descentOfHead", descentsOfHead);
		simpleObject.put("contractions", contractions);
		simpleObject.put("pulse", pulses);
		simpleObject.put("diastolic", diastolic);
		simpleObject.put("systolic", systolic);
		simpleObject.put("temperature", temperatures);
		simpleObject.put("weights", weights);
		simpleObject.put("urineVolume", urineVolumes);
		simpleObject.put("timeRanges", timeRanges);
		
		return simpleObject;
	}
	
	@NotNull
	private PartographObject getPartographObject(String time, double value) {
		return new PartographObject(time, value);
	}
	
	static class PartographObject {
		
		public String yValue;
		
		public Double xValue;
		
		public PartographObject(String yValue, Double xValue) {
			this.yValue = yValue;
			this.xValue = xValue;
		}
	}
}
