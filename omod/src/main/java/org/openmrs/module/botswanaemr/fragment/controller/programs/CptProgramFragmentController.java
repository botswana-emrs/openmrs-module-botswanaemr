/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounterTypeSearchCriteria;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLastDiscontinuedPatientProgram;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
public class CptProgramFragmentController {
	
	private Visit currentPatientVisit;
	
	private boolean hasActiveVisit;
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, @SpringBean("visitService") VisitService visitService,
	        @FragmentParam(value = "visit", required = false) Visit visit,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "form", required = false) Form form,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiSessionContext sessionContext) {
		
		List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
		boolean hasVisit = false;
		currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		if (currentPatientVisit != null) {
			hasActiveVisit = true;
		}
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program cptProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.CPT_PROGRAM_UUID);
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, cptProgram, null, null,
		    null, null, false);
		
		List<PatientProgram> activeProgramEnrollmentForPatients = new ArrayList<>();
		boolean enrolled;
		
		for (PatientProgram patientProgram : patientProgramList) {
			if (patientProgram.getDateCompleted() == null) {
				activeProgramEnrollmentForPatients.add(patientProgram);
			}
		}
		
		enrolled = activeProgramEnrollmentForPatients.size() > 0;
		
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("hasVisit", hasActiveVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("programs", programWorkflowService.getAllPrograms());
		model.addAttribute("enrolled", enrolled);
		model.addAttribute("visitTypes", visitService.getAllVisitTypes());
		model.addAttribute("activePrograms", activeProgramEnrollmentForPatients);
		model.addAttribute("hivOutComes", Programs.getTreatmentOutcomes());
		EncounterType labEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID);
		model.addAttribute("eligible",
		    Programs.eligibleForProgramEnrollment(patient,
		        getEncounterTypeSearchCriteria(patient, Arrays.asList(labEncounterType)),
		        getLastDiscontinuedPatientProgram(patient, cptProgram), cptProgram));
		
		model.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "programs/programs", patient, currentPatientVisit, ui)));
	}
	
	public String enrollIntoCPT(@RequestParam("patientId") String patientId) {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		if (patient == null) {
			patient = Context.getPatientService().getPatientByUuid(patientId);
			if (patient == null) {
				throw new IllegalArgumentException("Patient not found");
			}
		}
		PatientProgram patientProgram = Context.getProgramWorkflowService()
		        .savePatientProgram(BotswanaEmrUtils.createPatientProgram(patient, BotswanaEmrConstants.CPT_PROGRAM_UUID));
		
		return patientProgram.toString();
	}
	
	public String discontinueCPTProgram(UiUtils ui, @RequestParam("patientProgramUuid") String patientProgramUuid,
	        UiSessionContext sessionContext) {
		PatientProgram patientProgram = Context.getProgramWorkflowService().getPatientProgramByUuid(patientProgramUuid);
		if (patientProgram == null) {
			throw new IllegalArgumentException("Patient program not found");
		}
		
		patientProgram.setDateCompleted(new Date());
		Context.getProgramWorkflowService().savePatientProgram(patientProgram);
		
		return "redirect:" + ui.pageLink("botswanaemr", "programs/programs");
	}
}
