/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.vmmc;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class VmmcRecentAdmissionsFragmentController {
	
	public void controller(FragmentModel fragmentModel) {
	}
	
	public SimpleObject getLetterHeadDiv() {
		String hospitalName = Context.getAuthenticatedUser().getUserProperty(
		    BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
		SimpleObject so = new SimpleObject();
		String header = "<div class='col-md-12'>" + " <div class='col-md-12'>"
		        + "        <ul class='list-unstyled components'>" + "            <li class='pt-2'>"
		        + "                <img class='mx-auto ' src='/openmrs/ms/uiframework/resource/botswanaemr/images/moh-logo-01.png'"
		        + "                    alt='logo' style='width: 100px; height: 100px;'/>" + "            </li>"
		        + "            <li>" + "                <span class='ml-3 h4 nav_label'>BotswanaEMR</span>"
		        + "            </li>" + "        </ul>" + "    </div>" + "    <div class='col'>"
		        + "        <h4 class='lead text-white justify-content-center'>" + hospitalName + "        </h4>"
		        + "    </div>" + "</div>";
		so.put("printHeader", header.toString());
		return so;
	}
	
}
