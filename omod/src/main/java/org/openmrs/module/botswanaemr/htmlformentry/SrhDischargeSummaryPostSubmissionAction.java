/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import java.util.Set;

@Slf4j
public class SrhDischargeSummaryPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		Set<Obs> obsList = formEntrySession.getEncounter().getObs();
		Obs pncVisitDateObs = obsList.stream()
		        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.PNC_VISIT_DATE_CONCEPT_UUID)).findFirst()
		        .orElse(null);
		
		Obs cwcBabyVisitDateObs = obsList.stream()
		        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.CWC_BABY_DATE_CONCEPT_UUID)).findFirst()
		        .orElse(null);
		
		//create appointment when Date of PNC visit or CWC baby visit: field is not blank
		if (pncVisitDateObs != null) {
			String appointmentReason = "PNC Visit Followup";
			BotswanaEmrUtils.createAppointment(formEntrySession.getPatient(), formEntrySession.getEncounter().getLocation(),
			    pncVisitDateObs.getValueDate(), appointmentReason);
		} else if (cwcBabyVisitDateObs != null) {
			String appointmentReason = "CWC Baby Visit Followup";
			BotswanaEmrUtils.createAppointment(formEntrySession.getPatient(), formEntrySession.getEncounter().getLocation(),
			    cwcBabyVisitDateObs.getValueDate(), appointmentReason);
		}
	}
	
}
