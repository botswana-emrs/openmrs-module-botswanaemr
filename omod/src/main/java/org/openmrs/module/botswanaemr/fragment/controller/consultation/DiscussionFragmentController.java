/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.*;
import org.openmrs.api.UserService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.converter.simplifier.BotswanaEmrDiscussionSimplifier;
import org.openmrs.module.botswanaemr.converter.simplifier.BotswanaEmrMessageSimplifier;
import org.openmrs.module.botswanaemr.converter.simplifier.BotswanaEmrParticipantSimplifier;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.model.DiscussionParticipant;
import org.openmrs.module.botswanaemr.model.DiscussionParticipantKey;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.notification.Alert;
import org.openmrs.notification.AlertRecipient;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.web.user.CurrentUsers;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class DiscussionFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "visit") Visit visit) {
		fragmentModel.addAttribute("visit", visit);
	}
	
	public SimpleObject getDiscussion(@RequestParam("visitId") Integer visitId,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService) {
		Visit visit = visitService.getVisit(visitId);
		if (visit != null) {
			Discussion discussion = getDiscussion(botswanaEmrService, visit);
			if (discussion != null) {
				return SimpleObject.create("success", true, "data",
				    new BotswanaEmrDiscussionSimplifier(botswanaEmrService).convert(discussion));
			}
		}
		return SimpleObject.create("success", true, "data", new SimpleObject());
	}
	
	public SimpleObject getMessages(@RequestParam("discussionId") Integer discussionId,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService) {
		Discussion discussion = botswanaEmrService.getDiscussionById(discussionId);
		if (discussion != null) {
			BotswanaEmrMessageSimplifier simplifier = new BotswanaEmrMessageSimplifier();
			return SimpleObject.create("success", true, "data",
			    discussion.getMessages().stream().map(m -> simplifier.convert(m)).collect(Collectors.toList()));
		}
		return SimpleObject.create("success", true, "data", new ArrayList<>());
	}
	
	public SimpleObject getParticipants(HttpSession httpSession, @RequestParam("discussionId") Integer discussionId,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService) {
		Discussion discussion = botswanaEmrService.getDiscussionById(discussionId);
		BotswanaEmrParticipantSimplifier simplifier = new BotswanaEmrParticipantSimplifier(
		        CurrentUsers.getCurrentUsernames(httpSession));
		List<SimpleObject> participants = new ArrayList<>();
		if (discussion != null) {
			participants.add(simplifier.convert(discussion.getStartedBy()));
		}
		participants.addAll(botswanaEmrService.getDiscussionParticipantsByDiscussion(discussion).stream()
		        .map(dp -> dp.getUser()).map(u -> simplifier.convert(u)).collect(Collectors.toList()));
		return SimpleObject.create("success", true, "data", participants);
	}
	
	public SimpleObject getCandidates(HttpSession httpSession, @RequestParam("discussionId") Integer discussionId,
	        @SpringBean("userService") UserService userService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService) {
		Discussion discussion = botswanaEmrService.getDiscussionById(discussionId);
		List<User> participants = botswanaEmrService.getDiscussionParticipantsByDiscussion(discussion).stream()
		        .map((dp) -> dp.getUser()).collect(Collectors.toList());
		BotswanaEmrParticipantSimplifier simplifier = new BotswanaEmrParticipantSimplifier(
		        CurrentUsers.getCurrentUsernames(httpSession));
		return SimpleObject.create("success", true, "data",
		    userService.getUsersByRole(userService.getRole(BotswanaEmrConstants.ROLE_DOCTOR)).stream()
		            .filter((c) -> !participants.contains(c) && !c.equals(Context.getAuthenticatedUser()))
		            .map((c) -> simplifier.convert(c)).collect(Collectors.toList()));
	}
	
	public SimpleObject postParticipant(@RequestParam("discussionId") Integer discussionId,
	        @RequestParam("visitId") Integer visitId, @RequestParam("userId") Integer userId,
	        @SpringBean("visitService") VisitService visitService, @SpringBean("userService") UserService userService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService, HttpServletRequest request) {
		Discussion discussion;
		if (discussionId != 0) {
			discussion = botswanaEmrService.getDiscussionById(discussionId);
			if (discussion == null) {
				return SimpleObject.create("success", false, "data", "Discussion not found");
			}
			if (!discussion.getStartedBy().equals(Context.getAuthenticatedUser())) {
				return SimpleObject.create("success", false, "data", "Not allowed to add participants");
			}
		} else {
			Visit visit = visitService.getVisit(visitId);
			discussion = createDiscussion(botswanaEmrService, visit);
		}
		User user = userService.getUser(userId);
		DiscussionParticipant discussionParticipant = new DiscussionParticipant();
		discussionParticipant.setDiscussion(discussion);
		discussionParticipant.setUser(user);
		botswanaEmrService.addDiscussionParticipant(discussionParticipant);
		Patient patient = discussion.getCaze().getPatient();
		String patientName = patient.getGivenName() + " " + patient.getFamilyName();
		String caseUrl = constructCaseUrl(request, "postParticipant", patient.getPatientId(),
		    discussion.getCaze().getCaseId(), visitId);
		createAlert(user, "You have been added to a discussion for <a href='" + caseUrl + "'>" + patientName + "</a>");
		return SimpleObject.create("success", true, "data",
		    new BotswanaEmrDiscussionSimplifier(botswanaEmrService).convert(discussion));
	}
	
	public SimpleObject deleteParticipant(@RequestParam("discussionId") Integer discussionId,
	        @RequestParam("visitId") Integer visitId, @RequestParam("userId") Integer userId,
	        @SpringBean("userService") UserService userService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService, HttpServletRequest request) {
		Discussion discussion = botswanaEmrService.getDiscussionById(discussionId);
		if (discussion == null) {
			return SimpleObject.create("success", false, "data", "Discussion not found");
		}
		if (!discussion.getStartedBy().equals(Context.getAuthenticatedUser())) {
			return SimpleObject.create("success", false, "data", "Not allowed to remove participants");
		}
		User user = userService.getUser(userId);
		if (discussion.getStartedBy().equals(user)) {
			return SimpleObject.create("success", false, "data", "Not allowed to remove discussion owner");
		}
		DiscussionParticipantKey discussionParticipantKey = new DiscussionParticipantKey();
		discussionParticipantKey.setDiscussionId(discussionId);
		discussionParticipantKey.setParticipantId(userId);
		DiscussionParticipant discussionParticipant = new DiscussionParticipant();
		discussionParticipant.setId(discussionParticipantKey);
		discussionParticipant.setDiscussion(discussion);
		discussionParticipant.setUser(user);
		botswanaEmrService.deleteDiscussionParticipant(discussionParticipant);
		Patient patient = discussion.getCaze().getPatient();
		String patientName = patient.getGivenName() + " " + patient.getFamilyName();
		String caseUrl = constructCaseUrl(request, "deleteParticipant", patient.getPatientId(),
		    discussion.getCaze().getCaseId(), visitId);
		createAlert(user, "You have been removed from the discussion for <a href='" + caseUrl + "'>" + patientName + "</a>");
		return SimpleObject.create("success", true);
	}
	
	public SimpleObject postMessage(@RequestParam("discussionId") Integer discussionId,
	        @RequestParam("visitId") Integer visitId, @RequestParam("content") String content,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService) {
		if (StringUtils.isNotBlank(content)) {
			Discussion discussion;
			if (discussionId != 0) {
				discussion = botswanaEmrService.getDiscussionById(discussionId);
			} else {
				Visit visit = visitService.getVisit(visitId);
				discussion = createDiscussion(botswanaEmrService, visit);
			}
			if (discussion != null) {
				if (!userIsAllowedToParticipate(discussion, Context.getAuthenticatedUser())) {
					return SimpleObject.create("success", false, "data", "Discussion not found");
				}
				User authenticatedUser = Context.getAuthenticatedUser();
				Message message = new Message();
				message.setAuthoredBy(authenticatedUser);
				message.setDate(new Date());
				message.setContent(content);
				message.setDiscussion(discussion);
				message = botswanaEmrService.saveMessage(message);
				discussion.getMessages().add(message);
				BotswanaEmrMessageSimplifier simplifier = new BotswanaEmrMessageSimplifier();
				return SimpleObject.create("success", true, "data",
				    discussion.getMessages().stream().map(m -> simplifier.convert(m)).collect(Collectors.toList()));
			} else {
				return SimpleObject.create("success", false, "data", "Discussion not found");
			}
		}
		return SimpleObject.create("success", true, "data", new ArrayList<>());
	}
	
	private Discussion createDiscussion(BotswanaEmrService botswanaEmrService, Visit visit) {
		List<Encounter> encounters = visit.getEncounters().stream().filter(
		    e -> e.getEncounterType().getUuid().equals(BotswanaEmrConstants.CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID) || e
		            .getEncounterType().getUuid().equals(BotswanaEmrConstants.CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID))
		        .collect(Collectors.toList());
		if (encounters.isEmpty()) {
			throw new HttpClientErrorException(HttpStatus.FAILED_DEPENDENCY, "Missing consultation encounter type.");
		}
		Encounter encounter = encounters.get(0);
		CaseEncounter caseEncounter = botswanaEmrService.getCaseByEncounterId(encounter);
		Cases caze = caseEncounter.getCases();
		Discussion discussion = new Discussion();
		discussion.setTitle(caze.getDescription());
		discussion.setCaze(caze);
		discussion.setDateStarted(new Date());
		discussion.setStartedBy(Context.getAuthenticatedUser());
		discussion.setClosed(false);
		return botswanaEmrService.saveDiscussion(discussion);
	}
	
	private Discussion getDiscussion(BotswanaEmrService botswanaEmrService, Visit visit) {
		Discussion discussion = null;
		Cases caze = getCase(botswanaEmrService, visit);
		if (caze != null) {
			discussion = botswanaEmrService.getDiscussionByCase(caze);
		}
		return discussion;
	}
	
	private Cases getCase(BotswanaEmrService botswanaEmrService, Visit visit) {
		List<Encounter> encounters = visit.getEncounters().stream().filter(
		    e -> e.getEncounterType().getUuid().equals(BotswanaEmrConstants.CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID) || e
		            .getEncounterType().getUuid().equals(BotswanaEmrConstants.CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID))
		        .collect(Collectors.toList());
		Encounter encounter = null;
		if (encounters != null && !encounters.isEmpty()) {
			encounter = encounters.get(0);
		}
		if (encounter != null) {
			CaseEncounter caseEncounter = botswanaEmrService.getCaseByEncounterId(encounter);
			if (caseEncounter != null) {
				return caseEncounter.getCases();
			}
		}
		return null;
	}
	
	private boolean userIsAllowedToParticipate(Discussion discussion, User user) {
		if (discussion.getStartedBy().equals(user)) {
			return true;
		} else {
			return !discussion.getParticipants().stream().filter((p) -> p.getUser().equals(user))
			        .collect(Collectors.toList()).isEmpty();
		}
	}
	
	private void createAlert(User user, String message) {
		if (user != null && StringUtils.isNotBlank(message)) {
			AlertRecipient alertRecipient = new AlertRecipient();
			alertRecipient.setRecipient(user);
			Alert alert = new Alert();
			alert.setText(message);
			alert.addRecipient(alertRecipient);
			alert.setCreator(Context.getAuthenticatedUser());
			Context.getAlertService().saveAlert(alert);
		}
	}
	
	private String constructCaseUrl(HttpServletRequest request, String action, Integer patientId, Integer caseId,
	        Integer visitId) {
		StringBuilder builder = new StringBuilder(
		        // request.getRequestURL().toString().replace("discussion/" + action + ".action", "")
		        "$serverUrl/" + request.getContextPath() + "/");
		builder.append("consultation.page?patientId=" + patientId + "&caseId=" + caseId + "&visitId=" + visitId);
		return builder.toString();
	}
}
