/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.hts;

import javax.servlet.http.HttpSession;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationAttribute;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class HtsActionFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiUtils ui) {
		fragmentModel.addAttribute("patientPoolReturnUrl", ui.pageLink("botswanaemr", "hts/htsAllPatientsPool"));
		fragmentModel.addAttribute("user", Context.getAuthenticatedUser());

	}
	
	public SimpleObject saveSelfTestKit(@RequestParam(value = "itemId", required = true) int itemId,
	        @RequestParam(value = "mainStockRoom", required = true) int mainStockRoom,
	        @RequestParam(value = "purpose", required = false) String purpose,
	        @RequestParam(value = "lotNumber", required = false) String lotNumber,
	        @RequestParam(value = "distributor", required = false) String distributor,
	        @RequestParam(value = "date", required = false) String stockingDate,
	        @RequestParam(value = "expiryDate", required = false) String expiryDate,
	        @RequestParam(value = "remarks", required = false) String remarks,
	        @RequestParam(value = "quantity", required = false) Integer quantity, PageModel model, UiUtils ui,
	        HttpSession session, @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		IStockroomDataService iStockRoomDataService = BotswanaInventoryContext.getStockRoomDataService();
		IStockOperationAttributeTypeDataService iStockOperationAttributeTypeDataService = BotswanaInventoryContext
		        .getStockOperationAttributeTypeDataService();
		
		StockOperation stockOperation = new StockOperation();
		//String operationNumber = UUID.randomUUID().toString();
		stockOperation.setOperationNumber(lotNumber);
		stockOperation.setName(WellKnownOperationTypes.getReceipt().getName());
		stockOperation.setInstanceType(WellKnownOperationTypes.getReceipt());
		stockOperation.setDestination(iStockRoomDataService.getById(mainStockRoom));
		stockOperation.setDescription(remarks);
		
		StockOperationAttribute operationPurpose = new StockOperationAttribute();
		operationPurpose.setAttributeType(
		    iStockOperationAttributeTypeDataService.getByUuid(BotswanaEmrConstants.PURPOSE_ATTRIBUTE_TYPE_UUID));
		operationPurpose.setValue(purpose);
		operationPurpose.setOwner(stockOperation);
		
		StockOperationAttribute operationDistributor = new StockOperationAttribute();
		operationDistributor.setAttributeType(
		    iStockOperationAttributeTypeDataService.getByUuid(BotswanaEmrConstants.DISTRIBUTOR_NAME_ATTRIBUTE_TYPE_UUID));
		operationDistributor.setValue(distributor);
		operationDistributor.setOwner(stockOperation);
		Set<StockOperationAttribute> stockOperationAttributes = new HashSet<>();
		stockOperationAttributes.add(operationPurpose);
		stockOperationAttributes.add(operationDistributor);
		
		stockOperation.setAttributes(stockOperationAttributes);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
		Date stockDate = DateFor.parse(stockingDate);
		Date expirationDate = DateFor.parse(expiryDate);
		
		stockOperation.setOperationDate(stockDate);
		// Hard coding the uuid for the now that I have used in my db
		Item item = iItemDataService.getByUuid(BotswanaEmrConstants.SELF_TEST_KIT_UUID);
		stockOperation.addItem(item, quantity, expirationDate);
		
		SimpleObject simpleObject = new SimpleObject();
		stockOperation.setStatus(StockOperationStatus.NEW);
		BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		
		stockOperation.setStatus(StockOperationStatus.COMPLETED);
		StockOperation returnedReceiptStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(stockOperation);
		
		simpleObject.put("operationStatus", returnedReceiptStockOperation.getStatus());
		
		return simpleObject;
		
	}
	
}
