/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.BaseOpenmrsMetadata;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.EncounterProvider;
import org.openmrs.EncounterRole;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderAttribute;
import org.openmrs.OrderAttributeType;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.VisitAttribute;
import org.openmrs.VisitType;
import org.openmrs.Order.Action;
import org.openmrs.LocationAttributeType;
import org.openmrs.LocationAttribute;
import org.openmrs.OrderType;
import org.openmrs.CareSetting;
import org.openmrs.api.APIException;
import org.openmrs.api.ValidationException;
import org.openmrs.api.FormService;
import org.openmrs.api.LocationService;
import org.openmrs.api.OrderService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.BotswanaLabIntegrationService;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.icd11.IcdAPIClient;
import org.openmrs.module.botswanaemr.api.impl.BotswanaLabIntegrationServiceImpl;
import org.openmrs.module.botswanaemr.contract.Icd11ResponseObject;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.DrugOrderSimplifier;
import org.openmrs.module.botswanaemr.model.OrderPrescriptionSimplifier;
import org.openmrs.module.botswanaemr.model.SimplifiedHts;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedVmmc;
import org.openmrs.module.botswanaemr.model.InventoryItemSimplifier;
import org.openmrs.module.botswanaemr.utilities.IDENTIFIER;
import org.openmrs.module.botswanaemr.utilities.PharmacyUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDetailDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.notification.Alert;
import org.openmrs.notification.AlertRecipient;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
public class SearchFragmentController extends DailyRegisteredListFragmentController {
	
	public List<PatientResponse> getPatientsByAdvancedSearch(
	        @RequestParam(value = "startDateRegistered", required = false) String startDateRegistered,
	        @RequestParam(value = "endDateRegistered", required = false) String endDateRegistered,
	        @RequestParam(value = "nameOrUniqueId", required = false) String nameOrUniqueId,
	        @RequestParam(value = "gender", required = false) String gender,
	        @RequestParam(value = "status", required = false) String status,
	        @RequestParam(value = "programId", required = false) String programId,
	        @RequestParam(value = "ageCategory", required = false) String ageCategory,
	        @RequestParam(value = "condition", required = false) String condition, UiUtils uiUtils,
	        UiSessionContext sessionContext) throws APIException, ParseException {
		
		if (StringUtils.isNotEmpty(startDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(startDateRegistered);
			startDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		if (StringUtils.isNotEmpty(endDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(endDateRegistered);
			endDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		List<PatientResponse> patientResponseList;
		
		patientResponseList = Context.getService(BotswanaEmrService.class).getPatientsBySearchParams(nameOrUniqueId,
		    sessionContext.getSessionLocation().getUuid(), programId, gender, status, startDateRegistered, endDateRegistered,
		    condition);
		
		// Assign activeVisitUuid, visitNumber
		for (PatientResponse patientResponse : patientResponseList) {
			Patient patient = Context.getPatientService().getPatient(patientResponse.getPatientId());
			Visit activeVisit = BotswanaEmrUtils.getActiveVisit(patient);
			
			if (activeVisit != null) {
				patientResponse.setActiveVisitUuid(activeVisit.getUuid());
				patientResponse.setVisitNumber(activeVisit.getId());
			}
		}
		
		return patientResponseList;
	}
	
	public List<PatientResponse> getHtsPatientsByAdvancedSearch(
	        @RequestParam(value = "startDateRegistered", required = false) String startDateRegistered,
	        @RequestParam(value = "endDateRegistered", required = false) String endDateRegistered,
	        @RequestParam(value = "nameOrUniqueId", required = false) String nameOrUniqueId,
	        @RequestParam(value = "gender", required = false) String gender,
	        @RequestParam(value = "status", required = false) String status,
	        @RequestParam(value = "programId", required = false) String programId,
	        @RequestParam(value = "testingPoint", required = false) String testingPoint, UiUtils uiUtils,
			@SpringBean("formService") FormService formService,
	        UiSessionContext sessionContext) throws APIException, ParseException {
		
		Form htsForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID);
		Form htsVerificationForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID);
		Form priorTests = formService.getFormByUuid(BotswanaEmrConstants.HTS_DOCUMENT_PRIOR_TEST_FORM_UUID);
		Form supplementaryTestResults = formService.getFormByUuid(BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID);
		
		if (StringUtils.isNotEmpty(startDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(startDateRegistered);
			startDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		if (StringUtils.isNotEmpty(endDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(endDateRegistered);
			endDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		List<PatientResponse> patientResponseList;
		
		patientResponseList = Context.getService(BotswanaEmrService.class).getPatientsByHtsSearchParams(nameOrUniqueId,
				sessionContext.getSessionLocation().getUuid(), gender, status, startDateRegistered, endDateRegistered,
				testingPoint);
		
		for (PatientResponse patientResponse : patientResponseList) {
			Obs hivStatusObs = BotswanaEmrUtils.getPatientHivStatusObs(
					Context.getPatientService().getPatient(patientResponse.getPatientId()));
			Concept hivStatus = hivStatusObs != null ? hivStatusObs.getValueCoded() : null;
			String hivStatusValue = hivStatus != null ? hivStatus.getDisplayString() : "UNKNOWN";
			patientResponse.setHivStatus(hivStatusValue);
			patientResponse.setDateLastTested(hivStatusObs != null ? hivStatusObs.getObsDatetime() : null);
			
			List<Encounter> htsTestingEncounters = Context.getEncounterService().
					getEncounters(new EncounterSearchCriteria(Context.getPatientService().getPatient(patientResponse.getPatientId()), null,
					null, null, null, Arrays.asList(htsForm, htsVerificationForm, priorTests, supplementaryTestResults), null,
					null, null, null, false));
			
			List<Encounter> filteredEncounters = htsTestingEncounters.stream()
					.filter(encounter -> encounter.getObs().stream().noneMatch(obs -> {
						if (obs.getValueCoded() != null) {
							String valueCodedUuid = obs.getValueCoded().getUuid();
							return valueCodedUuid.equals(BotswanaEmrConstants.SELF_TESTING_POSITIVE_CONCEPT_UUID)
									|| valueCodedUuid.equals(BotswanaEmrConstants.SELF_TESTING_NEGATIVE_CONCEPT_UUID);
						}
						return false;
					})).collect(Collectors.toList());
			
			for (Encounter encounter : filteredEncounters) {
				for (Obs obs : encounter.getObs()) {
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_REASON_FOR_TEST_CONCEPT_UUID)) {
						Concept reasonForTest = obs.getValueCoded();
						String reasonForTestValue = reasonForTest != null ? reasonForTest.getDisplayString() : "UNKNOWN";
						patientResponse.setReasonForTest(reasonForTestValue);
					}
				}
			}
		}
		return patientResponseList;
	}
	
	public List<PatientResponse> getVmmcPatientsByAdvancedSearch(
	        @RequestParam(value = "startDateRegistered", required = false) String startDateRegistered,
	        @RequestParam(value = "endDateRegistered", required = false) String endDateRegistered,
	        @RequestParam(value = "nameOrUniqueId", required = false) String nameOrUniqueId,
	        @RequestParam(value = "gender", required = false) String gender,
	        @RequestParam(value = "status", required = false) String status,
	        @RequestParam(value = "programId", required = false) String programId,
	        @RequestParam(value = "visitStatus", required = false) String visitStatus, UiUtils uiUtils,
	        UiSessionContext sessionContext) throws APIException, ParseException {
		
		if (StringUtils.isNotEmpty(startDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(startDateRegistered);
			startDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		if (StringUtils.isNotEmpty(endDateRegistered)) {
			SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
			Date date = input.parse(endDateRegistered);
			endDateRegistered = formatDateWithoutTime(date, "yyyy-MM-dd");
		}
		
		List<PatientResponse> patientResponseList;
		
		gender = "M";
		
		patientResponseList = Context.getService(BotswanaEmrService.class).getPatientsByVmmcSearchParams(nameOrUniqueId,
		    sessionContext.getSessionLocation().getUuid(), gender, status, startDateRegistered, endDateRegistered,
		    visitStatus);
		
		return patientResponseList;
	}
	
	public List<SimpleObject> getLabSummary(@RequestParam("Id") Integer id, UiUtils ui,
	        @SpringBean("labService") LabService labService) {
		LabTest labTest = labService.getLabTestById(id);
		Encounter encounter = labTest.getEncounter();
		Set<Obs> obsSet = encounter.getAllObs();
		String results;
		Obs retreatmentObs = null;
		SimplifiedLabTest simplifiedLabTest;
		List<SimplifiedLabTest> allLabTest = new ArrayList<>();
		List<SimpleObject> simpleObjects = new ArrayList<>();
		if (obsSet != null) {
			for (Obs obs : obsSet) {
				if (obs.getConcept() != null && obs.getValueCoded() != null
				        && obs.getConcept().equals(getConcept(TB_TEST_TYPE))) {
					if (obs.getValueCoded().equals(getConcept(RETREATMENT))) {
						for (Obs obs1 : obsSet) {
							if (obs1.getConcept().equals(getConcept(RETREATMENT))) {
								retreatmentObs = obs1;
							}
						}
					}
					if (obs.getValueCoded().equals(labTest.getConcept())) {
						results = obs.getValueCoded().getDisplayString();
						if (retreatmentObs != null) {
							results += ">>Re-treatment>> " + retreatmentObs.getValueCoded().getDisplayString();
						}
						simplifiedLabTest = new SimplifiedLabTest();
						simplifiedLabTest.setResults(results);
						simplifiedLabTest.setTestName(obs.getConcept().getDisplayString());
						simplifiedLabTest.setStatus(labTest.getStatus());
						allLabTest.add(simplifiedLabTest);
					}
				}
			}
			simpleObjects = SimpleObject.fromCollection(allLabTest, ui, "testName", "status", "results");
		}
		return simpleObjects;
	}
	
	public String endPatientConsultation(@RequestParam("patientId") Patient patient,
	        @SpringBean("patientQueueingService") PatientQueueingService patientQueueingService) {
		PatientQueue patientQueue = patientQueueingService.getMostRecentQueue(patient);
		if (patientQueue.getStatus().equals(PatientQueue.Status.PICKED)) {
			patientQueue.setStatus(PatientQueue.Status.COMPLETED);
			patientQueue.setDateCompleted(new Date());
			
			// save the queue
			patientQueueingService.savePatientQue(patientQueue);
		}
		return null;
	}
	
	public SimpleObject closePatientQueue(@RequestParam(value = "patientQueueId") PatientQueue patientQueue,
	        @RequestParam(value = "encounterId") Encounter encounter) {
		if (!patientQueue.getStatus().equals(PatientQueue.Status.COMPLETED)) {
			patientQueue.setEncounter(encounter);
			patientQueue.setStatus(PatientQueue.Status.COMPLETED);
			patientQueue.setDateCompleted(new Date());
			
			// save the queue
			Context.getService(PatientQueueingService.class).savePatientQue(patientQueue);
		}
		return null;
	}
	
	public SimpleObject getPatientsByIdentifier(@RequestParam("idType") String idType,
	        @RequestParam("idValue") String idValue) {
		
		String idTypeUuid = "";
		if (idType.equals("nationalIdNumber")) {
			idTypeUuid = NATIONAL_ID_IDENTIFIER_TYPE;
		}
		if (idType.equals("passportNumber")) {
			idTypeUuid = PASSPORT_IDENTIFIER_TYPE;
		}
		if (idType.equals("birthCertificateNumber")) {
			idTypeUuid = BIRTH_CERT_IDENTIFIER_TYPE;
		}
		PatientService patientService = Context.getPatientService();
		PatientIdentifierType identifierType = patientService.getPatientIdentifierTypeByUuid(idTypeUuid);
		List<Patient> patients = patientService.getPatients(null, idValue, Collections.singletonList(identifierType), true);
		SimpleObject simpleObject = new SimpleObject();
		if (!patients.isEmpty()) {
			Patient patient = patients.get(0);
			simpleObject.put("patientId", patient.getId());
			simpleObject.put("patientUuid", patient.getUuid());
			simpleObject.put("fullName", patient.getPersonName().getFullName());
			simpleObject.put("age", patient.getAge());
			simpleObject.put("dateOfBirth", formatDateWithoutTime(patient.getBirthdate(), "dd/MM/yyyy"));
			simpleObject.put("gender", patient.getGender());
			
			PersonAttributeType emailAttributeType = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(EMAIL_ATTRIBUTE_TYPE_UUID);
			if (emailAttributeType != null) {
				PersonAttribute emailAttribute = patient.getAttribute(emailAttributeType.getPersonAttributeTypeId());
				if (emailAttribute != null) {
					simpleObject.put("email", emailAttribute.getValue());
				}
			}
			
			PersonAttributeType phoneAttributeType = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			if (phoneAttributeType != null) {
				PersonAttribute phoneAttribute = patient.getAttribute(phoneAttributeType.getPersonAttributeTypeId());
				if (phoneAttribute != null) {
					simpleObject.put("phoneNumber", phoneAttribute.getValue());
				}
			}
			
			PersonAttributeType altPhoneAttributeType = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			if (altPhoneAttributeType != null) {
				PersonAttribute altPhoneAttribute = patient.getAttribute(phoneAttributeType.getPersonAttributeTypeId());
				if (altPhoneAttribute != null) {
					simpleObject.put("altPhoneNumber", altPhoneAttribute.getValue());
				}
			}
			
			//Next of Kin details
			PersonAttributeType nokId = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_ID_ATTRIBUTE_TYPE_UUID);
			if (nokId != null) {
				PersonAttribute nokIdAttribute = patient.getAttribute(nokId.getPersonAttributeTypeId());
				if (nokIdAttribute != null) {
					simpleObject.put("nokId", nokIdAttribute.getValue());
				}
			}
			
			// NOK addresss
			PersonAddress nokAddress = patient.getPersonAddress();
			if (nokAddress != null) {
				simpleObject.put("nokAddress1", nokAddress.getAddress1());
				simpleObject.put("nokAddress2", nokAddress.getAddress2());
				simpleObject.put("nokCity", nokAddress.getCityVillage());
			}
			
			PersonAttributeType nokFullName = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(NOK_FULLNAME_ATTRIBUTE_TYPE_UUID);
			if (nokFullName != null) {
				PersonAttribute nokFullNameAttribute = patient.getAttribute(nokFullName.getPersonAttributeTypeId());
				if (nokFullNameAttribute != null) {
					simpleObject.put("nokFullName", nokFullNameAttribute.getValue());
				}
			}
			
			PersonAttributeType nokRelationship = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
			if (nokRelationship != null) {
				PersonAttribute nokRelationshipAttribute = patient.getAttribute(nokRelationship.getPersonAttributeTypeId());
				if (nokRelationshipAttribute != null) {
					simpleObject.put("nokRelationship", nokRelationshipAttribute.getValue());
				}
			}
			
			PersonAttributeType nokContact = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
			if (nokContact != null) {
				PersonAttribute nokContactAttribute = patient.getAttribute(nokContact.getPersonAttributeTypeId());
				if (nokContactAttribute != null) {
					simpleObject.put("nokContact", nokContactAttribute.getValue());
				}
			}
		} else {
			simpleObject.put("patientId", -1);
		}
		
		return simpleObject;
	}
	
	public Encounter latestEncounter(Patient patient, String encounterTypeUuid, Location location) {
		Encounter encounter = null;
		EncounterType encounterType = Context.getEncounterService().getEncounterTypeByUuid(encounterTypeUuid);
		if (encounterType != null) {
			List<Encounter> encounters = Context.getService(BotswanaEmrService.class).getEncountersList(
			    patient.getDateCreated(), new Date(), Collections.singletonList(encounterType), null, location, patient);
			if (encounters != null) {
				encounter = !encounters.isEmpty() ? encounters.get(0) : null;
			}
		}
		
		return encounter;
	}
	
	public SimpleObject getLatestObsByEncounterType(@RequestParam("patientId") Patient patient,
	        @RequestParam("concept") String concept, @RequestParam("encounterType") String encounterType) {
		
		SimpleObject simpleObject = new SimpleObject();
		
		List<Concept> questions = new ArrayList<>();
		Context.getConceptService().getConceptByUuid(concept);
		
		questions.add(Context.getConceptService().getConceptByUuid(concept));
		List<Obs> obsList = Context.getObsService().getObservations(Collections.singletonList(patient), null, questions,
		    null, null, null, null, null, null, null, null, false, null);
		for (Obs obs : obsList) {
			if (obs.getEncounter().getEncounterType().getUuid().equals(encounterType)) {
				simpleObject.put("location", obs.getLocation().getName());
				simpleObject.put("date", obs.getDateCreated().toString());
				
				break;
			}
			
		}
		
		return simpleObject;
	}
	
	public SimpleObject getLatestVitalsByEncounterType(@RequestParam("patientId") Patient patient,
	        @RequestParam("concept") String concept, @RequestParam("encounterType") String encounterType) {
		
		SimpleObject simpleObject = new SimpleObject();
		
		List<Concept> questions = new ArrayList<>();
		Context.getConceptService().getConceptByUuid(concept);
		
		questions.add(Context.getConceptService().getConceptByUuid(concept));
		List<Obs> obsList = Context.getObsService().getObservations(Collections.singletonList(patient), null, questions,
		    null, null, null, null, null, null, null, null, false, null);
		for (Obs obs : obsList) {
			if (obs.getEncounter().getEncounterType().getUuid().equals(encounterType)) {
				simpleObject.put("obs", obs.getValueNumeric());
				
				break;
			}
			
		}
		
		return simpleObject;
	}
	
	/**
	 * @param nameOrUniqueId
	 * @param prescriptionStartDate
	 * @param prescriptionEndDate
	 * @param facility
	 * @param status
	 * @param ui
	 * @param uiSessionContext
	 * @return
	 * @throws ParseException
	 */
	public List<SimpleObject> getOrderPrescriptions(
	        @RequestParam(value = "nameOrUniqueId", required = false) final String nameOrUniqueId,
	        @RequestParam(value = "prescriptionStartDate", required = false) String prescriptionStartDate,
	        @RequestParam(value = "prescriptionEndDate", required = false) String prescriptionEndDate,
	        @RequestParam(value = "filteredLocation", required = false) Integer facility,
	        @RequestParam(value = "prescriptionStatus", required = false) String status, UiUtils ui,
	        UiSessionContext uiSessionContext) throws ParseException {
		
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		
		List<Patient> listOfPatients;
		
		Date startDate = getStartOfDay();
		Date endDate = getEndOfDay();
		Location location = uiSessionContext.getSessionLocation();
		if (StringUtils.isNotBlank(prescriptionStartDate) && StringUtils.isNotBlank(prescriptionEndDate)) {
			startDate = DateUtil.getStartOfDay(formatDateFromString(prescriptionStartDate, "dd-MMM-yyyy"));
			endDate = DateUtil.getEndOfDay(formatDateFromString(prescriptionEndDate, "dd-MMM-yyyy"));
		}
		if (facility != null) {
			location = Context.getLocationService().getLocation(facility);
		}
		if (nameOrUniqueId.isEmpty()) {
			listOfPatients = new ArrayList<>(getPatientsPrescriptionEncounters(startDate, endDate, location));
		} else {
			listOfPatients = Context.getPatientService().getPatients(nameOrUniqueId);
		}
		
		if (listOfPatients.size() == 1) {
			// If only one result was returned, assume searching by PIN
			// Searching by PIN should return results from all hospitals
			location = null;
			startDate = null;
			endDate = null;
		}
		
		List<Order> allOrders = new ArrayList<>();
		List<Order> completedOrders = getDrugOrders(startDate, endDate, Order.FulfillerStatus.COMPLETED, location);
		if (StringUtils.isNotBlank(status)) {
			if (status.equals("filled")) {
				allOrders = completedOrders;
			} else if (status.equals("awaiting")) {
				allOrders = getDrugOrders(startDate, endDate, null, location);
				allOrders.removeAll(completedOrders);
			}
		} else {
			allOrders = getDrugOrders(startDate, endDate, null, location);
			allOrders.removeAll(completedOrders);
		}
		
		List<OrderPrescriptionSimplifier> orderPrescriptionSimplifierList = new ArrayList<>();
		
		if (allOrders != null && !allOrders.isEmpty()) {
			// Filter orders by patients
			Set<Encounter> getAllOrderPrescriptionsEncounter = allOrders.stream().map(Order::getEncounter)
			        .filter(o -> listOfPatients.contains(o.getPatient())).collect(Collectors.toSet());
			
			OrderPrescriptionSimplifier orderPrescriptionSimplifier;
			if (!getAllOrderPrescriptionsEncounter.isEmpty()) {
				for (Encounter encounter : getAllOrderPrescriptionsEncounter) {
					if (encounter.getOrders() != null) {
						orderPrescriptionSimplifier = new OrderPrescriptionSimplifier();
						orderPrescriptionSimplifier.setOrderId(encounter.getEncounterId());
						Visit visit = encounter.getVisit();
						Set<Encounter> encounterSet;
						String cases = "";
						if (visit != null) {
							// TODO: Ensure that the case id is saved as a visit reference going forward.
							// And generate a script to update old visit
							VisitAttribute visitAttribute = visit.getAttributes().stream()
							        .filter(a -> a.getAttributeType().getUuid().equals(CASE_ID_VISIT_ATTRIBUTE)).findFirst()
							        .orElse(null);
							cases = visitAttribute != null ? visitAttribute.getValueReference() : "";
							if (cases.equals("")) {
								encounterSet = visit.getEncounters().stream().filter(
								    e -> e.getEncounterType().getUuid().equals(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID)
								            || e.getEncounterType().getUuid().equals(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID))
								        .collect(Collectors.toSet());
								for (Encounter enc : encounterSet) {
									CaseEncounter caseEncounter = botswanaEmrService.getCaseByEncounterId(enc);
									if (caseEncounter != null && caseEncounter.getCases() != null) {
										cases = caseEncounter.getCases().getCaseId().toString();
										break;
									}
								}
							}
						}
						orderPrescriptionSimplifier.setCaseId(cases);
						orderPrescriptionSimplifier.setPin(getOpenMrsId(encounter.getPatient()));
						orderPrescriptionSimplifier
						        .setPatientNames(formatPersonName(encounter.getPatient().getPersonName()));
						
						if (encounter.getLocation() != null) {
							orderPrescriptionSimplifier.setFacility(encounter.getLocation().getName());
						} else {
							orderPrescriptionSimplifier.setFacility("");
						}
						orderPrescriptionSimplifier.setDatePrescribed(
						    formatDateWithTime(encounter.getEncounterDatetime(), "dd/MM/yyyy HH:mm:ss"));
						
						String prescriptionStatus = "";
						
						// Update the overall prescription status based on the individual order statuses
						Set<Order> encounterOrders = encounter.getOrders();
						// TODO: filter orders with no preceding orders with an action of DISCONTINUE
						encounterOrders = encounterOrders.stream().filter(o -> !checkIfOrderIsDiscontinued(o)).collect(
						    Collectors.toSet());
						//for (Order order : encounterOrders) {
							 // Recursively check if previous order is a discontinue order
						//	 Boolean isDiscontinued = checkIfOrderIsDiscontinued(order);
						//	 log.info("Order: " + order.getOrderId() + " is discontinued: " + isDiscontinued);
						//}
						
						prescriptionStatus = BotswanaEmrUtils.determinePrescriptionStatus(encounterOrders);
						
						Order orderDetails = encounterOrders.stream().findFirst().orElse(null);
						if (orderDetails != null && orderDetails.getOrderer().getPerson().getPersonName() != null) {
							orderPrescriptionSimplifier
							        .setPrescriber(formatPersonName(orderDetails.getOrderer().getPerson().getPersonName()));
						} else {
							orderPrescriptionSimplifier.setPrescriber("");
						}
						
						orderPrescriptionSimplifier.setStatus(prescriptionStatus);
						orderPrescriptionSimplifier.setEncounterId(encounter.getEncounterId());
						orderPrescriptionSimplifier.setPatientId(encounter.getPatient().getPatientId());
						if (visit != null) {
							orderPrescriptionSimplifier.setVisitId(visit.getVisitId());
						}
						// add the prescription to the collection of the objects
						orderPrescriptionSimplifierList.add(orderPrescriptionSimplifier);
					}
				}
			}
		}
		return SimpleObject.fromCollection(orderPrescriptionSimplifierList, ui, "visitId", "orderId", "caseId", "pin",
		    "patientNames", "prescriber", "facility", "datePrescribed", "status", "encounterId", "patientId");
	}

	private Boolean checkIfOrderIsDiscontinued(Order order) {
		if (order.getAction().equals(Action.DISCONTINUE)) {
			return true;
		}
		Order previousOrder = Context.getOrderService().getRevisionOrder(order);
		if (previousOrder == null) {
			previousOrder = Context.getOrderService().getDiscontinuationOrder(order);
		}
		if (previousOrder == null) {
			return false;
		}
		return checkIfOrderIsDiscontinued(previousOrder);
	}
	
	public void sendNotificationToProvider(@RequestParam(value = "recipientName") Integer personId,
	        @RequestParam(value = "messageForDoctor") String message) {
		List<User> userList = Context.getUserService().getUsersByPerson(Context.getPersonService().getPerson(personId),
		    false);
		if (!userList.isEmpty()) {
			AlertRecipient alertRecipient = new AlertRecipient();
			alertRecipient.setRecipient(userList.get(0));
			// Send an alert to the selected user
			if (StringUtils.isNotBlank(message)) {
				Alert alert = new Alert();
				alert.setText(message);
				alert.addRecipient(alertRecipient);
				// Set the alert so that if any 'reads' it it will be marked as read for
				// everyone who received it
				alert.setSatisfiedByAny(true);
				alert.setCreator(Context.getAuthenticatedUser());
				// save the alert to send it to the user as a notification when they log in
				Context.getAlertService().saveAlert(alert);
			}
		}
	}
	
	public void updateDrugOrder(@RequestParam(value = "orderDetailsId") Integer orderDetailsId,
	        @RequestParam(value = "quantityToFill") Integer quantityToFill,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam(value = "batchNo", required = false) String batchNo,
	        @RequestParam(value = "itemStockDetailId", required = false) Integer itemStockDetailId,
	        @RequestParam(value = "drugId", required = false) Integer drugId,
	        @RequestParam(value = "itemCode", required = false) String itemCode, UiSessionContext uiSessionContext)
	        throws ValidationException {
		if (orderDetailsId != null) {
			DrugOrder order = (DrugOrder) Context.getOrderService().getOrder(orderDetailsId);
			DrugOrder revisedOrder = null;
			Order previousOrder = null;
			
			IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
			
			Stockroom sourceStockRoom;
			
			if (stockRoomId == null) {
				StockUtils.getDispensingStockrooms(uiSessionContext.getSessionLocation());
				sourceStockRoom = StockUtils.getDispensingStockroom(uiSessionContext.getSessionLocation());
			} else {
				sourceStockRoom = iStockroomDataService.getById(stockRoomId);
			}
			String orderFulfillersName = uiSessionContext.getCurrentUser().getPerson().getPersonName().getFullName();
			
			revisedOrder = getRevisedOrder(order, itemStockDetailId, drugId, itemCode, Double.valueOf(quantityToFill), orderFulfillersName, sourceStockRoom);
			
			if (order.getNumRefills() != null && order.getNumRefills() > 0) {
				revisedOrder.setNumRefills(order.getNumRefills() - 1);
				updateOrderFulfillerStatusAsCompleted(order, "Revision order created for refill");
			} else {
				if (Context.getOrderService().getOrder(order.getOrderId()).isActive()) {
					updateOrderFulfillerStatusAsCompleted(order, "Order fulfilled");				
				}
			}
			// save a new order of the parent order
			Context.getOrderService().saveOrder(revisedOrder, null);
			
			// check if this order has reached 0 refills and mark it as complete
			if (revisedOrder != null && revisedOrder.getNumRefills() == 0) {
				Context.getOrderService().updateOrderFulfillerStatus(revisedOrder, Order.FulfillerStatus.COMPLETED,
				    "last refill");
				previousOrder = revisedOrder.getPreviousOrder();
			}
			// check if there was a parent order before this last one and mark it as
			// completed
			// get the parent order form this one if it ever exited
			Order secondPreviousOrder = null;
			Order thirdPreviousOrder = null;
			Order forthPreviousOrder = null;
			if (previousOrder != null) {
				Context.getOrderService().updateOrderFulfillerStatus(previousOrder, Order.FulfillerStatus.COMPLETED,
				    "2nd last refill");
				secondPreviousOrder = previousOrder.getPreviousOrder();
				
			}
			if (secondPreviousOrder != null) {
				Context.getOrderService().updateOrderFulfillerStatus(secondPreviousOrder, Order.FulfillerStatus.COMPLETED,
				    "3rd last refill");
				thirdPreviousOrder = secondPreviousOrder.getPreviousOrder();
			}
			if (thirdPreviousOrder != null) {
				Context.getOrderService().updateOrderFulfillerStatus(thirdPreviousOrder, Order.FulfillerStatus.COMPLETED,
				    "4th last refill");
				forthPreviousOrder = thirdPreviousOrder.getPreviousOrder();
			}
			if (forthPreviousOrder != null) {
				Context.getOrderService().updateOrderFulfillerStatus(forthPreviousOrder, Order.FulfillerStatus.COMPLETED,
				    "5th last refill");
			}
			// update the stock with the available values
			if (quantityToFill > 0) {
				// to code the logic to deduct the stock levels
				IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
				StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
				        .getOperationByNumber(order.getOrderNumber());
				if (stockOperation != null) {
					stockOperation.setStatus(StockOperationStatus.COMPLETED);
					iStockOperationService.submitOperation(stockOperation);
				} else {
					stockOperation = new StockOperation();
					Item item = getItemFromItemCode(itemCode);
					
					if (item != null) {
						IItemStockDetailDataService iItemStockDetailDataService = BotswanaInventoryContext
						        .getItemStockDetailDataService();
						if (itemStockDetailId != null) {
							ItemStockDetail itemStockDetail = iItemStockDetailDataService.getById(itemStockDetailId);
							stockOperation.addItem(item, quantityToFill, itemStockDetail.getExpiration(),
							    itemStockDetail.getBatchOperation());
						} else {
							stockOperation.addItem(item, quantityToFill);
						}
						stockOperation.setOperationDate(new Date());
						// TODO: Locate a dispensing stock room
						
						stockOperation.setSource(sourceStockRoom);
						stockOperation.setPatient(order.getPatient());
						stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
						
						stockOperation.setOperationNumber(order.getId().toString());
						stockOperation.setStatus(StockOperationStatus.NEW);
						iStockOperationService.submitOperation(stockOperation);
						stockOperation.setStatus(StockOperationStatus.COMPLETED);
						iStockOperationService.submitOperation(stockOperation);
					}
				}
			}
		}
		
	}

	private void updateOrderFulfillerStatusAsCompleted(DrugOrder order, String comment) {
		// Sometimes, the order is already discontinued so we cannot update the order. 
		// We therefore just catch the exception and continue
		try {
			Context.getOrderService().updateOrderFulfillerStatus(order, Order.FulfillerStatus.COMPLETED, comment);
		} catch (Exception e) {
			// Continue with the process
		}
	}
	
	/**
	 * Since orders are immutable, we cannot update orders directly. This method creates a new order
	 * with the new drug and sets the previous order to the current order if the drug has changed. If
	 * the drug has not changed, creates a new order with the new drug and sets the previous order to
	 * the current order. If the order has no previous order, discontinue the order this is to ensure
	 * that the order is not duplicated.
	 * 
	 * @param order
	 * @param itemStockDetailId
	 * @param drugId
	 * @param itemCode
	 * @param orderFulfillersName
	 * @param sourceStockRoom
	 * @return DrugOrder
	 */
	private DrugOrder getRevisedOrder(DrugOrder order, Integer itemStockDetailId, Integer drugId, String itemCode, Double quantity, 
	        String orderFulfillersName, Stockroom sourceStockRoom) {
		DrugOrder revisedOrder;
		// if the drug has changed, create a new order with the new drug
		// if the drug has not changed, create a new order with the new drug and set the previous order to the current order
		Drug prescribedDrug = Context.getConceptService().getDrug(drugId);
		if (prescribedDrug != null && order.getDrug().getDrugId() != drugId) {
			revisedOrder = order.copy();
			revisedOrder.setDrug(prescribedDrug);
			revisedOrder.setDrugNonCoded(prescribedDrug.getName());
			revisedOrder.setConcept(prescribedDrug.getConcept());
			revisedOrder.setQuantity(quantity);
			if (revisedOrder.getPreviousOrder() != null) {
				revisedOrder.setPreviousOrder(null);
				revisedOrder.setAction(Action.NEW);
			}
		} else {
			revisedOrder = order.cloneForRevision();
			revisedOrder.setQuantity(quantity);
		}
		revisedOrder.setFulfillerStatus(Order.FulfillerStatus.IN_PROGRESS);
		revisedOrder.setFulfillerComment("Filling the order per the order refills");
		revisedOrder.setEncounter(order.getEncounter());
		revisedOrder.setOrderer(order.getOrderer());
		revisedOrder.setDateActivated(new Date());
		
		revisedOrder = getOrderEnrichedWithFulfillmentOrderAttributes(orderFulfillersName, new Date(), revisedOrder);
		
		revisedOrder = getOrderEnrichedWithItemStockDetails(itemStockDetailId, itemCode, sourceStockRoom, revisedOrder);
		
		// if the order has no previous order, discontinue the order
		// this is to ensure that the order is not duplicated
		if (revisedOrder.getPreviousOrder() == null) {
			Context.getOrderService().discontinueOrder(order, "Drug dispensed changed", new Date(), order.getOrderer(),
			    order.getEncounter());
		}
		return revisedOrder;
	}
	
	private DrugOrder getOrderEnrichedWithFulfillmentOrderAttributes(String orderFulfillersName, Date orderFulfillmentDate,
	        DrugOrder revisedOrder) {
		OrderAttributeType fullfillerNameAttributeType = Context.getOrderService()
		        .getOrderAttributeTypeByUuid(ORDER_FULFILLER_NAME_ATTRIBUTE_TYPE_UUID);
		OrderAttribute fulfillerNameOrderAttribute = getOrderAttribute(revisedOrder, fullfillerNameAttributeType,
		    orderFulfillersName);
		revisedOrder.addAttribute(fulfillerNameOrderAttribute);
		
		OrderAttributeType fulfillmentDateAttributeType = Context.getOrderService()
		        .getOrderAttributeTypeByUuid(ORDER_FULFILLMENT_DATE_ATTRIBUTE_TYPE_UUID);
		OrderAttribute fulfillmentDateOrderAttribute = getOrderAttribute(revisedOrder, fulfillmentDateAttributeType,
		    orderFulfillmentDate.toString());
		revisedOrder.addAttribute(fulfillmentDateOrderAttribute);
		return revisedOrder;
	}
	
	private DrugOrder getOrderEnrichedWithItemStockDetails(Integer itemStockDetailId, String itemCode, Stockroom stockroom,
	        DrugOrder revisedOrder) {
		String batchNo;
		// Enrich the order with stock details i.e. batch number and expiry date if available
		Date itemStockExpirDate = null;
		ItemStockDetail itemStockDetail = null;
		if (itemStockDetailId != null) {
			itemStockDetail = BotswanaInventoryContext.getItemStockDetailDataService().getById(itemStockDetailId);
			itemStockExpirDate = itemStockDetail.getExpiration();
		} else {
			Item item = getItemFromItemCode(itemCode);
			ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom);
			
			if (itemStock != null) {
				Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
				itemStockDetails = itemStockDetails.stream().filter(i -> i.getBatchOperation() != null)
				        .collect(Collectors.toSet());
				// filter out the stock details that are not expired and order by the expiry date and pick the first
				itemStockDetail = itemStockDetails.stream().filter(i -> i.getExpiration().after(new Date()))
				        .sorted(Comparator.comparing(ItemStockDetail::getExpiration)).findFirst().orElse(null);
				if (itemStockDetail != null) {
					itemStockExpirDate = itemStockDetail.getExpiration();
				}
			}
		}
		
		batchNo = itemStockDetail.getBatchOperation().getOperationNumber();
		OrderAttributeType orderBatchNumberAttributeType = Context.getOrderService()
		        .getOrderAttributeTypeByUuid(ORDER_BATCH_NUMBER_ATTRIBUTE_TYPE_UUID);
		OrderAttribute orderBatchNumberAttribute = getOrderAttribute(revisedOrder, orderBatchNumberAttributeType, batchNo);
		revisedOrder.addAttribute(orderBatchNumberAttribute);
		
		if (itemStockExpirDate != null) {
			OrderAttributeType orderExpiryDateAttributeType = Context.getOrderService()
			        .getOrderAttributeTypeByUuid(ORDER_EXPIRY_DATE_ATTRIBUTE_TYPE_UUID);
			OrderAttribute orderExpiryDateAttribute = getOrderAttribute(revisedOrder, orderExpiryDateAttributeType,
			    itemStockExpirDate.toString());
			revisedOrder.addAttribute(orderExpiryDateAttribute);
		}
		
		OrderAttributeType orderItemCodeAttributeType = Context.getOrderService()
		        .getOrderAttributeTypeByUuid(BotswanaEmrConstants.ORDER_ITEM_CODE_ATTRIBUTE_TYPE_UUID);
		OrderAttribute orderItemCodeAttribute = getOrderAttribute(revisedOrder, orderItemCodeAttributeType, itemCode);
		revisedOrder.addAttribute(orderItemCodeAttribute);
		return revisedOrder;
	}
	
	private OrderAttribute getOrderAttribute(DrugOrder revisedOrder, OrderAttributeType orderBatchNumberAttributeType,
	        String attributeValue) {
		OrderAttribute orderBatchNumberAttribute = new OrderAttribute();
		orderBatchNumberAttribute.setOrder(revisedOrder);
		orderBatchNumberAttribute.setAttributeType(orderBatchNumberAttributeType);
		orderBatchNumberAttribute.setValue(attributeValue);
		orderBatchNumberAttribute.setValueReferenceInternal(attributeValue);
		revisedOrder.addAttribute(orderBatchNumberAttribute);
		return orderBatchNumberAttribute;
	}
	
	public static Item getItemFromItemCode(String itemCode) {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		Item item = iItemDataService.getItemByCode(itemCode);
		return item;
	}
	
	public void updateUnavailableDrug(@RequestParam(value = "orderDetailsId") Integer orderDetailsId,
	        UiSessionContext uiSessionContext) {
		if (orderDetailsId != null) {
			DrugOrder order = (DrugOrder) Context.getOrderService().getOrder(orderDetailsId);
			Context.getOrderService().updateOrderFulfillerStatus(order, Order.FulfillerStatus.EXCEPTION,
			    "Drug not available in stock");
		}
	}
	
	public void makeHoldRequest(@RequestParam(value = "orderDetailsId") Integer orderDetailsId,
	        @RequestParam(value = "itemId") Integer itemId, @RequestParam(value = "quantityToFill") Integer quantityToFill,
	        @RequestParam(value = "stockRoomId") Integer stockRoomId, UiSessionContext uiSessionContext) {
		if (orderDetailsId != null) {
			
			DrugOrder order = (DrugOrder) Context.getOrderService().getOrder(orderDetailsId);
			
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			Item item = BotswanaInventoryContext.getItemDataService().getById(itemId);
			
			DrugOrder revisedOrder;
			if (order.getNumRefills() != null && order.getNumRefills() >= 0) {
				if (order.isActive()) {
					revisedOrder = order.cloneForRevision();
					revisedOrder.setPreviousOrder(order);
					revisedOrder.setFulfillerStatus(Order.FulfillerStatus.IN_PROGRESS);
					revisedOrder.setFulfillerComment("Making a hold request at facility " + stockroom.getLocation().getName()
					        + "(" + stockroom.getName() + ")");
					revisedOrder.setEncounter(order.getEncounter());
					revisedOrder.setOrderReasonNonCoded("Hold:" + stockroom.getLocation().getName());
					revisedOrder.setOrderer(order.getOrderer());
					revisedOrder.setDateActivated(new Date());
					// save a new order of the parent order
					Order newOrder = Context.getOrderService().saveOrder(revisedOrder, null);
					
					StockUtils.getStockHoldOperation(order.getPatient(), stockroom, item, quantityToFill,
					    newOrder.getOrderNumber());
					
					Context.getOrderService().updateOrderFulfillerStatus(order, Order.FulfillerStatus.COMPLETED,
					    "Revision order created");
				}
			}
		}
	}
	
	public SimpleObject getSingleOrderDetails(@RequestParam(value = "orderDetailsId") Integer orderDetailsId,
	        @RequestParam(value = "stockRoomId") Integer stockRoomId,
	        @RequestParam(value = "drugId", required = false) Integer drugId, UiUtils ui,
	        UiSessionContext uiSessionContext) {
		DrugOrder orderThatHasRefillsPending = null;
		int initialRefillsRecorded = 0;
		int refillDone = 0;
		DrugOrder secondPreviousOrder = null;
		DrugOrder thirdPreviousOrder = null;
		DrugOrder forthPreviousOrder = null;
		DrugOrder fifthPreviousOrder = null;
		if (orderDetailsId != null) {
			orderThatHasRefillsPending = (DrugOrder) Context.getOrderService().getOrder(orderDetailsId);
			initialRefillsRecorded = orderThatHasRefillsPending.getNumRefills();
			refillDone = 1;
			secondPreviousOrder = (DrugOrder) orderThatHasRefillsPending.getPreviousOrder();
		}
		if (secondPreviousOrder != null) {
			refillDone = 2;
			initialRefillsRecorded = secondPreviousOrder.getNumRefills();
			thirdPreviousOrder = (DrugOrder) secondPreviousOrder.getPreviousOrder();
		}
		if (thirdPreviousOrder != null) {
			refillDone = 3;
			initialRefillsRecorded = thirdPreviousOrder.getNumRefills();
			forthPreviousOrder = (DrugOrder) thirdPreviousOrder.getPreviousOrder();
		}
		if (forthPreviousOrder != null) {
			refillDone = 4;
			initialRefillsRecorded = forthPreviousOrder.getNumRefills();
			fifthPreviousOrder = (DrugOrder) forthPreviousOrder.getPreviousOrder();
		}
		if (fifthPreviousOrder != null) {
			refillDone = 5;
			initialRefillsRecorded = fifthPreviousOrder.getNumRefills();
		}
		DrugOrderSimplifier drugOrderSimplifier = new DrugOrderSimplifier();
		if (orderThatHasRefillsPending != null) {
			drugOrderSimplifier = DrugOrderSimplifier.simplify(orderThatHasRefillsPending);
			
		}
		drugOrderSimplifier.setFillCount(refillDone);
		drugOrderSimplifier.setTotalRefills(initialRefillsRecorded);
		DrugOrder order = (DrugOrder) Context.getOrderService().getOrder(orderDetailsId);
		if (order != null && drugId != null) {
			Drug drug = Context.getConceptService().getDrug(drugId);
			if (drug != null) {
				Item item = StockUtils.getItemByDrug(drug);
				
				Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
				ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom);
				
				Integer stockAvailability = 0;
				// item stock availability is derived from bot expired stock details
				if (itemStock != null) {
					stockAvailability = itemStock.getDetails().stream()
					        .filter(i -> i.getExpiration() != null && i.getExpiration().after(new Date()))
					        .map(ItemStockDetail::getQuantity).reduce(0, Integer::sum);
				}
				
				drugOrderSimplifier.setStockAvailability(stockAvailability);
				
				drugOrderSimplifier.setStatus(order.getFulfillerStatus() == null ? "" : order.getFulfillerStatus().name());
				
				if (item != null) {
					drugOrderSimplifier.setItemId(item.getId());
					drugOrderSimplifier.setItemUuid(item.getUuid());
					drugOrderSimplifier.setItemCode(getItemCode(item.getCodes()));
					drugOrderSimplifier.setDrugOrderId(order.getOrderId());
					drugOrderSimplifier.setDrugOrderUuid(order.getUuid());
				}
			}
		}
		
		return SimpleObject.fromObject(drugOrderSimplifier, ui, "drug", "fillCount", "totalRefills", "drugOrderId",
		    "drugOrderUuid", "dosage", "refillDuration", "refillRepeat", "itemCode", "frequency", "route",
		    "stockAvailability", "itemId", "itemUuid", "drugId", "drugConceptId", "calculatedQuantity", "status");
	}
	
	public SimpleObject getAlternativeDrugs(@RequestParam(value = "conceptId") Concept drugConcept, UiUtils ui) {
		SimpleObject simpleObject = new SimpleObject();
		List<Drug> drugAlternatives = Context.getConceptService().getDrugsByConcept(drugConcept);
		simpleObject.put("drugAlternatives", SimpleObject.fromCollection(drugAlternatives, ui, "uuid", "drugId", "name"));
		
		return simpleObject;
	}
	
	public SimpleObject fetchAvailableBatchesByDrug(@RequestParam(value = "drugId") Integer drugId,
	        @RequestParam(value = "stockRoomId") Integer stockRoomId) {
		Drug drug = Context.getConceptService().getDrug(drugId);
		Item item = StockUtils.getItemByDrug(drug);
		List<SimpleBatch> simpleBatches = SearchFragmentController.getBatchDetails(stockRoomId, item, false);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		simpleObject.put("batches", simpleBatches);
		simpleObject.put("quantity",
		    simpleBatches.stream().reduce(0, (subtotal, batch) -> subtotal + batch.getQuantity(), Integer::sum));
		simpleObject.put("itemCode", item == null ? "" : getItemCode(item.getCodes()));
		
		return simpleObject;
		
	}
	
	public SimpleObject fetchAvailableBatchesByDrugUuid(@RequestParam(value = "drugId") String drugId,
	        @RequestParam(value = "stockRoomUuid") String stockRoomUuid) {
		
		Concept concept = Context.getConceptService().getConcept(drugId);
		Drug drug = Context.getConceptService().getDrug(concept.getName().getName());
		SimpleObject simpleObject = new SimpleObject();
		if (drug != null) {
			Item item = StockUtils.getItemByDrug(drug);
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getByUuid(stockRoomUuid);
			
			List<SimpleBatch> simpleBatches = SearchFragmentController.getBatchDetails(stockroom.getId(), item, false);
			List<String> itemBatchNumbers = simpleBatches.stream().map(SimpleBatch::getBatchNumber)
			        .collect(Collectors.toList());
			
			simpleObject.put("status", "success");
			simpleObject.put("batches", simpleBatches);
			simpleObject.put("batchNumbers", itemBatchNumbers);
			simpleObject.put("quantity",
			    simpleBatches.stream().reduce(0, (subtotal, batch) -> subtotal + batch.getQuantity(), Integer::sum));
			simpleObject.put("itemCode", item == null ? "" : getItemCode(item.getCodes()));
		}
		return simpleObject;
		
	}
	
	public SimpleObject fetchAvailableBatches(@RequestParam(value = "orderId") Integer orderId,
	        @RequestParam(value = "stockRoomId") Integer stockRoomId) {
		Order order = Context.getOrderService().getOrder(orderId);
		
		SimpleObject simpleObject = new SimpleObject();
		if (order instanceof DrugOrder) {
			DrugOrder drugOrder = (DrugOrder) order;
			Item item = StockUtils.getItemByDrug(drugOrder.getDrug());
			
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom);
			List<SimpleBatch> simpleBatches = SearchFragmentController.getBatchDetails(stockRoomId, item, false);
			
			if (itemStock != null) {
				simpleObject.put("status", "success");
				simpleObject.put("batches", simpleBatches);
			} else {
				simpleObject.put("status", "success");
				simpleObject.put("batches", simpleBatches);
				
			}
		} else {
			simpleObject.put("status", "failed");
		}
		
		return simpleObject;
	}
	
	public static List<SimpleBatch> getBatchDetails(Integer stockRoomId, Item item, boolean includeExpired) {
		Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		List<SimpleBatch> simpleBatches = new ArrayList<>();
		if (item != null) {
			ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom);
			if (itemStock != null) {
				Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
				itemStockDetails = itemStockDetails.stream().filter(i -> i.getBatchOperation() != null)
				        .collect(Collectors.toSet());
				for (ItemStockDetail itemStockDetail : itemStockDetails) {
					if (includeExpired) {
						simpleBatches.add(new SimpleBatch(itemStockDetail.getId(),
						        itemStockDetail.getBatchOperation().getOperationNumber(),
						        formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy-MM-dd"),
						        itemStockDetail.getQuantity()));
					} else {
						if (itemStockDetail.getExpiration().after(new Date())) {
							simpleBatches.add(new SimpleBatch(itemStockDetail.getId(),
							        itemStockDetail.getBatchOperation().getOperationNumber(),
							        formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy-MM-dd"),
							        itemStockDetail.getQuantity()));
						}
					}
				}
			}
		}
		return simpleBatches;
	}
	
	public SimpleObject fetchFacilities(UiSessionContext uiSessionContext) {
		SimpleObject simpleObject = new SimpleObject();
		List<Location> visitLocations = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName("Visit Location"));
		List<String> locations = visitLocations.stream().map(BaseOpenmrsMetadata::getName).collect(Collectors.toList());
		simpleObject.put("status", "success");
		simpleObject.put("locations", locations);
		return simpleObject;
	}
	
	public SimpleObject fetchStockRooms(UiSessionContext uiSessionContext, UiUtils ui) {
		SimpleObject simpleObject = new SimpleObject();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		simpleObject.put("stockrooms", SimpleObject.fromCollection(stockrooms, ui, "uuid", "name", "description"));
		simpleObject.put("status", "success");
		return simpleObject;
	}
	
	public SimpleObject fetchHtsStockRooms(UiSessionContext uiSessionContext, UiUtils ui) {
		SimpleObject simpleObject = new SimpleObject();
		List<Stockroom> stockrooms = StockUtils.getHtsDistributionStockrooms(uiSessionContext.getSessionLocation());
		List<SimpleObject> stockroomObjects = new ArrayList<>();
		
		if (stockrooms.isEmpty()) {
			simpleObject.put("stockrooms", new ArrayList<>());
		} else {
			for (Stockroom stockroom : stockrooms) {
				if (stockroom.getItems() != null && !stockroom.getItems().isEmpty()) {
					SimpleObject stockroomObject = new SimpleObject();
					stockroomObject.put("uuid", stockroom.getUuid());
					stockroomObject.put("name", stockroom.getName());
					stockroomObject.put("description", stockroom.getDescription());
					
					stockroomObjects.add(stockroomObject);
				}
			}
			
			simpleObject.put("stockrooms", stockroomObjects);
		}
		simpleObject.put("status", "success");
		return simpleObject;
		
	}
	
	public SimpleObject fetchDispensingStockRooms(UiSessionContext uiSessionContext, UiUtils ui) {
		SimpleObject simpleObject = new SimpleObject();
		List<Stockroom> stockrooms = StockUtils.getDispensingStockrooms(uiSessionContext.getSessionLocation());
		List<SimpleObject> stockroomObjects = new ArrayList<>();
		
		if (stockrooms.isEmpty()) {
			simpleObject.put("stockrooms", new ArrayList<>());
		} else {
			for (Stockroom stockroom : stockrooms) {
				if (stockroom.getItems() != null && !stockroom.getItems().isEmpty()) {
					SimpleObject stockroomObject = new SimpleObject();
					stockroomObject.put("uuid", stockroom.getUuid());
					stockroomObject.put("name", stockroom.getName());
					stockroomObject.put("description", stockroom.getDescription());
					
					stockroomObjects.add(stockroomObject);
				}
			}
			
			simpleObject.put("stockrooms", stockroomObjects);
		}
		simpleObject.put("status", "success");
		return simpleObject;
		
	}
	
	public SimpleObject fetchHtsKitAvailableBatches(@RequestParam(value = "conceptId") Concept conceptId,
	        @RequestParam(value = "stockroomId") String stockroomId, UiSessionContext uiSessionContext) {
		int availableQuantity = 0;
		// Stockroom mainStockroom = StockUtils.getHtsStockroom(uiSessionContext.getSessionLocation());
		Stockroom mainStockroom = StockUtils.getStockroom(stockroomId);
		
		SimpleObject simpleObject = new SimpleObject();
		
		if (conceptId != null) {
			IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
			List<Item> items = iItemDataService.getItemsByConcept(conceptId);
			
			List<SimpleBatch> simpleBatches = new ArrayList<>();
			List<String> batchNumbers = new ArrayList<>();
			for (Item item : items) {
				ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, mainStockroom);
				List<String> itemBatchNumbers = new ArrayList<>();
				List<SimpleBatch> simpleItemBatches = new ArrayList<>();
				
				if (itemStock != null) {
					availableQuantity += itemStock.getQuantity();
					
					Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
					List<ItemStockDetail> sortedItemStockDetails = itemStockDetails.stream()
					        .filter(i -> i.getBatchOperation() != null && i.getExpiration() != null
					                && i.getExpiration().after(new Date())) // if expiry date is null, or if the expiry date is in the past, skip this batch
					        .sorted(Comparator.comparing(ItemStockDetail::getExpiration)).collect(Collectors.toList());
					
					for (ItemStockDetail itemStockDetail : sortedItemStockDetails) {
						simpleItemBatches.add(new SimpleBatch(itemStockDetail.getId(),
						        itemStockDetail.getBatchOperation().getOperationNumber(),
						        formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy/MM/dd"),
						        itemStockDetail.getQuantity(), itemStock.getUuid(),
						        itemStockDetail.getItem().getCodes().stream().findFirst().map(ItemCode::getCode)
						                .orElse(null),
						        itemStockDetail.getItem().getName(),
						        itemStockDetail.getBatchOperation() != null ? itemStockDetail.getBatchOperation().getId()
						                : null));
					}
					itemBatchNumbers = simpleItemBatches.stream().map(SimpleBatch::getBatchNumber)
					        .collect(Collectors.toList());
					
					simpleBatches.addAll(simpleItemBatches);
					batchNumbers.addAll(itemBatchNumbers);
					
				}
			}
			simpleObject.put("status", "success");
			simpleObject.put("batches", simpleBatches);
			simpleObject.put("batchNumbers", batchNumbers);
			simpleObject.put("availableQuantity", availableQuantity);
			
		} else {
			simpleObject.put("status", "failed");
		}
		return simpleObject;
	}
	
	public SimpleObject fetchHtsKitAvailableBatchesByItemCode(@RequestParam(value = "itemId") String itemCode,
	        UiSessionContext uiSessionContext) {
		int availableQuantity;
		Stockroom mainStockroom = StockUtils.getHtsStockroom(uiSessionContext.getSessionLocation());
		
		SimpleObject simpleObject = new SimpleObject();
		
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		Item item = iItemDataService.getItemByCode(itemCode);
		
		if (item != null) {
			ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, mainStockroom);
			availableQuantity = itemStock.getQuantity();
			
			Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
			itemStockDetails = itemStockDetails.stream().filter(i -> i.getBatchOperation() != null)
			        .collect(Collectors.toSet());
			
			List<SimpleBatch> simpleBatches = new ArrayList<>();
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				simpleBatches.add(
				    new SimpleBatch(itemStockDetail.getId(), itemStockDetail.getBatchOperation().getOperationNumber(),
				            formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy-MM-dd"),
				            itemStockDetail.getQuantity()));
			}
			List<String> batchNumbers = simpleBatches.stream().map(SimpleBatch::getBatchNumber).collect(Collectors.toList());
			
			simpleObject.put("status", "success");
			simpleObject.put("batches", simpleBatches);
			simpleObject.put("batchNumbers", batchNumbers);
			simpleObject.put("availableQuantity", availableQuantity);
		} else {
			simpleObject.put("status", "failed");
		}
		return simpleObject;
	}
	
	public SimpleObject fetchBatchesByItemCode(@RequestParam(value = "itemId") String itemCode,
	        @RequestParam(value = "stockroomId", required = false) Integer stockroomId, UiSessionContext uiSessionContext) {
		int availableQuantity = 0;
		
		List<Stockroom> stockrooms = new ArrayList<>();
		if (stockroomId == null) {
			stockrooms = StockUtils.getAllStockrooms(uiSessionContext.getSessionLocation());
		} else {
			IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
			Stockroom sr = iStockroomDataService.getById(stockroomId);
			stockrooms = Collections.singletonList(sr);
		}
		
		SimpleObject simpleObject = new SimpleObject();
		
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		Item item = iItemDataService.getItemByCode(itemCode);
		
		if (item != null) {
			List<SimpleBatch> simpleBatches = new ArrayList<>();
			for (Stockroom stockroom2 : stockrooms) {
				ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom2);
				if (itemStock != null) {
					availableQuantity += itemStock.getQuantity();
					
					Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
					itemStockDetails = itemStockDetails.stream().filter(i -> i.getBatchOperation() != null)
					        .collect(Collectors.toSet());
					
					for (ItemStockDetail itemStockDetail : itemStockDetails) {
						simpleBatches.add(new SimpleBatch(itemStockDetail.getId(),
						        itemStockDetail.getBatchOperation().getOperationNumber(),
						        formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy-MM-dd"),
						        itemStockDetail.getQuantity()));
					}
				}
			}
			List<String> batchNumbers = simpleBatches.stream().map(SimpleBatch::getBatchNumber).collect(Collectors.toList());
			
			simpleObject.put("status", "success");
			simpleObject.put("batches", simpleBatches);
			simpleObject.put("batchNumbers", batchNumbers);
			simpleObject.put("availableQuantity", availableQuantity);
			
		} else {
			simpleObject.put("status", "failed");
		}
		return simpleObject;
	}
	
	public List<SimpleObject> getVmmcData(
	        @RequestParam(value = "nameOrUniqueId", required = false) final String nameOrUniqueId,
	        @RequestParam(value = "operationDate", required = false) String operationDate, UiUtils uiUtils,
	        UiSessionContext sessionContext) throws ParseException {
		
		Location facility = sessionContext.getSessionLocation();
		
		List<PatientResponse> patientResponseList = Context.getService(BotswanaEmrService.class)
		        .getPatientsByVmmcSearchParams(nameOrUniqueId, facility.getUuid(), "M", "Active", operationDate,
		            operationDate, null);
		
		List<SimplifiedVmmc> simplifiedVmmcList = new ArrayList<>();
		Date startDate = null;
		Date endDate = null;
		if (StringUtils.isNotBlank(operationDate)) {
			startDate = DateUtil.getStartOfDay(formatDateFromString(operationDate, "dd-MMM-yyyy"));
			endDate = DateUtil.getEndOfDay(formatDateFromString(operationDate, "dd-MMM-yyyy"));
		}
		List<Encounter> getAllVmmcEncounter = getVmmcEncounters(patientResponseList, startDate, endDate, facility);
		SimplifiedVmmc simplifiedVmmc;
		if (!patientResponseList.isEmpty()) {
			for (Encounter encounter : getAllVmmcEncounter) {
				simplifiedVmmc = new SimplifiedVmmc();
				simplifiedVmmc.setPatientId(encounter.getPatient().getPatientId());
				simplifiedVmmc
				        .setPin(encounter.getPatient().getPatientIdentifier(OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
				simplifiedVmmc.setName(formatPersonName(encounter.getPatient().getPersonName()));
				simplifiedVmmc
				        .setAge(encounter.getPatient().getAge() == null ? "" : encounter.getPatient().getAge().toString());
				simplifiedVmmc.setOperationDate(formatDateWithoutTime(encounter.getEncounterDatetime(), "MMM dd yyyy"));
				simplifiedVmmc.setVisitId(encounter.getVisit().getVisitId());
				simplifiedVmmc.setEncounterId(encounter.getEncounterId());
				List<Encounter> VmmcPostEncounters = getVmmcPostEncounters(encounter.getPatient(), startDate, null,
				    facility);
				if (VmmcPostEncounters.isEmpty()) {
					simplifiedVmmc.setStatus("Awaiting");
				} else {
					simplifiedVmmc.setStatus("Completed");
				}
				simplifiedVmmcList.add(simplifiedVmmc);
			}
		}
		return SimpleObject.fromCollection(simplifiedVmmcList, uiUtils, "patientId", "visitId", "name", "age",
		    "operationDate", "status", "pin");
	}
	
	public List<SimpleObject> getHtsData(
	        @RequestParam(value = "nameOrUniqueId", required = false) final String nameOrUniqueId,
	        @RequestParam(value = "operationDate", required = false) String operationDate, UiUtils uiUtils,
	        UiSessionContext sessionContext) throws ParseException {
		List<Patient> listOfPatients;
		if (nameOrUniqueId.isEmpty()) {
			listOfPatients = Context.getPatientService().getAllPatients(false);
		} else {
			listOfPatients = Context.getPatientService().getPatients(nameOrUniqueId);
		}
		Date startDate = null;
		Date endDate = null;
		Location facility = sessionContext.getSessionLocation();
		if (StringUtils.isNotBlank(operationDate)) {
			startDate = DateUtil.getStartOfDay(formatDateFromString(operationDate, "dd-MMM-yyyy"));
			endDate = DateUtil.getEndOfDay(formatDateFromString(operationDate, "dd-MMM-yyyy"));
		}
		
		List<SimplifiedHts> SimplifiedHtssList = new ArrayList<>();
		List<Encounter> getAllHtsEncounters = getHtsEncounters(listOfPatients, startDate, endDate, facility);
		SimplifiedHts simplifiedHts;
		if (!listOfPatients.isEmpty()) {
			for (Encounter encounter : getAllHtsEncounters) {
				simplifiedHts = new SimplifiedHts();
				simplifiedHts.setPatientId(encounter.getPatient().getPatientId());
				simplifiedHts
				        .setPin(encounter.getPatient().getPatientIdentifier(OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
				simplifiedHts.setName(formatPersonName(encounter.getPatient().getPersonName()));
				simplifiedHts.setGender(encounter.getPatient().getGender().toString());
				simplifiedHts
				        .setAge(encounter.getPatient().getAge() == null ? "" : encounter.getPatient().getAge().toString());
				simplifiedHts.setOperationDate(formatDateWithoutTime(encounter.getEncounterDatetime(), "MMM dd yyyy"));
				
				List<Encounter> VmmcPostEncounters = getVmmcPostEncounters(encounter.getPatient(), startDate, null,
				    facility);
				if (VmmcPostEncounters.isEmpty()) {
					simplifiedHts.setStatus("Awaiting");
				} else {
					simplifiedHts.setStatus("Completed");
				}
				SimplifiedHtssList.add(simplifiedHts);
			}
		}
		return SimpleObject.fromCollection(SimplifiedHtssList, uiUtils, "patientId", "gender", "name", "age",
		    "operationDate", "status", "pin");
	}
	
	public List<SimpleObject> searchIcd11(@RequestParam(value = "q", required = false) String searchTerm, UiUtils uiUtils)
	        throws Exception {
		
		IcdAPIClient api = IcdAPIClient.getInstance();
		List<Icd11ResponseObject> icd11ResponseObjectList = api.searchTerm(searchTerm);
		
		return SimpleObject.fromCollection(icd11ResponseObjectList, uiUtils, "theCode", "id", "title");
	}
	
	public SimpleObject getIcd11ApiToken(UiUtils uiUtils) throws Exception {
		
		IcdAPIClient api = IcdAPIClient.getInstance();
		String accessToken = api.getAccessToken();
		
		SimpleObject tokenResponse = new SimpleObject();
		tokenResponse.put("token", accessToken);
		return tokenResponse;
		
	}
	
	public List<SimpleObject> getItems(@RequestParam(value = "name") String name,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService, UiUtils ui,
	        UiSessionContext uiSessionContext) throws ParseException {
		List<Item> items = iItemDataService.getByNameFragment(name, false);
		List<SimpleObject> simpleObjectList = new ArrayList<>();
		items.forEach(i -> {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("code", Objects.requireNonNull(i.getCodes().stream().findFirst().orElse(null)).getCode());
			simpleObject.put("uuid", i.getUuid());
			simpleObject.put("name", i.getName());
			simpleObjectList.add(simpleObject);
		});
		
		return simpleObjectList;
	}

	public List<SimpleObject> getAvailableItems(@RequestParam(value = "name") String name, @RequestParam(value = "stockRoomId") Integer stockRoomId) {
		List<SimpleObject> simpleObjectList = new ArrayList<>();

		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		
		if (stockRoomId != null) {
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			if (stockroom != null) {
				List<Item> items = iItemDataService.getByNameFragment(name, false);
				items.forEach(i -> {
					ItemStock itemStock = StockUtils.getItemStockByStockRoom(i, stockroom);
					if (itemStock != null && itemStock.getQuantity() > 0) {
						SimpleObject simpleObject = new SimpleObject();
						simpleObject.put("code", Objects.requireNonNull(i.getCodes().stream().findFirst().orElse(null)).getCode());
						simpleObject.put("uuid", i.getUuid());
						simpleObject.put("name", i.getName());
						simpleObjectList.add(simpleObject);	
					}	
				});
			}
			
		}

		return simpleObjectList;		
	}
	
	public List<SimpleObject> getMainStockRoomsByLocation(@RequestParam(value = "id") Location location, UiUtils ui,
	        UiSessionContext uiSessionContext) throws ParseException {
		List<Stockroom> stockrooms = new ArrayList<>(StockUtils.getBulkStockrooms(location));
		return SimpleObject.fromCollection(stockrooms, ui, "name", "id", "uuid", "description");
	}
	
	public SimpleObject getItemQuantityByStockroom(@RequestParam(value = "id") String itemId,
	        @RequestParam(value = "stockroomId") String stockroomId,
	        @SpringBean("bemrs.itemStockDataService") IItemStockDataService iItemStockDataService,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService, UiUtils ui) throws ParseException {
		List<ItemStock> itemStock = iItemStockDataService
		        .getItemStockByItem(iItemDataService.getById(Integer.parseInt(itemId)), null);
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		int quantity = 0;
		for (ItemStock itemStock2 : itemStock) {
			if (itemStock2.getStockroom() == iStockroomDataService.getById(Integer.parseInt(stockroomId))) {
				quantity += itemStock2.getQuantity();
			}
		}
		SimpleObject simpleObject = new SimpleObject();
		
		simpleObject.put("quantity", quantity);
		
		return simpleObject;
		// return SimpleObject.fromCollection(items, ui, "name", "uuid", "codes");
	}
	
	public SimpleObject getItemsAsAnOrder(
	        @RequestParam(value = "operationsRefModalItemOrderId") Integer operationsRefModalItemOrderId, UiUtils ui) {
		OrderService orderService = Context.getOrderService();
		DrugOrder drugOrder = (DrugOrder) orderService.getOrder(operationsRefModalItemOrderId);
		DrugOrderSimplifier drugOrderSimplifier = new DrugOrderSimplifier(drugOrder);
		if (drugOrder != null) {
			drugOrderSimplifier.setDrug(drugOrder.getDrug().getDisplayName());
			drugOrderSimplifier
			        .setDosage(drugOrder.getDose().intValue() + " " + drugOrder.getDoseUnits().getDisplayString());
			drugOrderSimplifier.setRefillDuration(String.valueOf(drugOrder.getDuration().intValue()));
			drugOrderSimplifier.setFrequency(drugOrder.getFrequency().getName());
			drugOrderSimplifier.setRoute(drugOrder.getRoute().getDisplayString());
			drugOrderSimplifier.setRefillRepeat(String.valueOf(drugOrder.getNumRefills().intValue()));
		}
		
		return SimpleObject.fromObject(drugOrderSimplifier, ui, "drug", "dosage", "refillDuration", "refillRepeat",
		    "frequency", "route");
		
	}
	
	private String getItemCode(Set<ItemCode> itemCodeSet) {
		String code = "";
		ItemCode itemCode;
		if (itemCodeSet != null && !itemCodeSet.isEmpty()) {
			List<ItemCode> itemCodeList = new ArrayList<>(itemCodeSet);
			itemCode = itemCodeList.get(0);
			code = itemCode.getCode();
		}
		
		return code;
	}
	
	public List<SimpleObject> getItemStockToSearchInOtherStoreRooms(
	        @RequestParam(value = "operationsRefModalItemValue") Integer operationsRefModalItemValue, UiUtils ui,
	        UiSessionContext uiSessionContext) {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		Item item = iItemDataService.getById(operationsRefModalItemValue);
		DrugOrderSimplifier drugOrderSimplifier;
		List<DrugOrderSimplifier> drugOrderSimplifierList = new ArrayList<>();
		if (item != null) {
			IItemStockDataService iItemStockDataService = BotswanaInventoryContext.getItemStockDataService();
			List<ItemStock> stockByItems = iItemStockDataService.getItemStockByItem(item, null);
			stockByItems = stockByItems.stream()
			        .filter(s -> !s.getStockroom().getLocation().equals(uiSessionContext.getSessionLocation()))
			        .filter(s -> s.getStockroom().getAttributes().stream().anyMatch(sra -> sra.getValue().contains("Bulk")))
			        .collect(Collectors.toList());
			for (ItemStock itemStock : stockByItems) {
				drugOrderSimplifier = new DrugOrderSimplifier();
				drugOrderSimplifier.setStoreRoomLocation(itemStock.getStockroom().getLocation().getName());
				drugOrderSimplifier.setStockroomId(itemStock.getStockroom().getId());
				drugOrderSimplifier.setDrug(itemStock.getItem().getName());
				drugOrderSimplifier.setStockAvailability(Math.max(itemStock.getQuantity(), 0));
				
				drugOrderSimplifierList.add(drugOrderSimplifier);
				
			}
		}
		
		return SimpleObject.fromCollection(drugOrderSimplifierList, ui, "storeRoomLocation", "drug", "stockAvailability",
		    "stockroomId");
		
	}
	
	public SimpleObject approveHoldRequest(@RequestParam("orderId") Order order, @RequestParam("action") String action,
	        @SpringBean("orderService") OrderService orderService) {
		SimpleObject simpleObject = new SimpleObject();
		
		try {
			if (order instanceof DrugOrder) {
				if (action.equals("Accept")) {
					orderService.updateOrderFulfillerStatus(order, Order.FulfillerStatus.RECEIVED, "Hold request approved");
				} else if (action.equals("Reject")) {
					StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
					        .getOperationByNumber(order.getOrderNumber());
					stockOperation.setStatus(StockOperationStatus.CANCELLED);
					BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
					orderService.updateOrderFulfillerStatus(order, Order.FulfillerStatus.EXCEPTION, "Hold request rejected");
				}
			}
			simpleObject.put("status", HttpStatus.OK.toString());
			simpleObject.put("response", "Order status updated successfully");
		}
		catch (Exception ex) {
			simpleObject.put("status", HttpStatus.INTERNAL_SERVER_ERROR.toString());
			simpleObject.put("response", ex.getMessage());
		}
		
		return simpleObject;
	}
	
	public SimpleObject fetchHoldStockOperation(@RequestParam("orderId") Order order, UiUtils ui) {
		
		SimpleObject simpleObject = new SimpleObject();
		
		try {
			StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
			        .getOperationByNumber(order.getOrderNumber());
			
			simpleObject.put("status", HttpStatus.OK.toString());
			SimpleObject s = new SimpleObject();
			s.put("stockroomId", stockOperation.getSource().getId());
			s.put("stockroomName", stockOperation.getSource().getName());
			simpleObject.put("response", s);
		}
		catch (Exception ex) {
			simpleObject.put("status", HttpStatus.INTERNAL_SERVER_ERROR.toString());
			simpleObject.put("response", ex.getMessage());
		}
		
		return simpleObject;
	}
	
	public List<DrugOrderSimplifier> refillsDoneListView(
	        @RequestParam(value = "orderDetailsIdToViews", required = false) Integer orderId) {
		List<DrugOrderSimplifier> orderList = new ArrayList<>();
		DrugOrderSimplifier drugOrderSimplifier;
		if (orderId != null) {
			DrugOrder drugOrder = (DrugOrder) Context.getOrderService().getOrder(orderId);
			Set<Order> orderListRelatedToThisOrder = Context.getEncounterService()
			        .getEncounter(drugOrder.getEncounter().getEncounterId()).getOrders();
			if (orderListRelatedToThisOrder != null) {
				orderListRelatedToThisOrder = orderListRelatedToThisOrder.stream().filter(
				    o -> o.getFulfillerStatus() != null && o.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED))
				        .collect(Collectors.toSet());
				
				for (Order order : orderListRelatedToThisOrder) {
					DrugOrder orderToDrugOrder = Context.getService(BotswanaEmrService.class)
					        .getDrugOrderById(order.getOrderId());
					if (drugOrder.getConcept().equals(order.getConcept())) {
						drugOrderSimplifier = new DrugOrderSimplifier(drugOrder);
						if (order.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED)) {
							if (order.getPreviousOrder() == null || order.getPreviousOrder().getFulfillerStatus() == null) {
								drugOrderSimplifier.setDateRefilled(
								    formatDateWithoutTime(order.getDateActivated(), "yyyy-MM-dd hh:mm a"));
							} else if (order.getPreviousOrder().getFulfillerStatus()
							        .equals(Order.FulfillerStatus.COMPLETED)) {
								drugOrderSimplifier.setDateRefilled(formatDateWithoutTime(
								    order.getPreviousOrder().getEffectiveStopDate(), "yyyy-MM-dd hh:mm a"));
							}
						} else {
							drugOrderSimplifier.setDateRefilled("");
						}
						drugOrderSimplifier.setFacility(order.getEncounter().getLocation().getName());
						drugOrderSimplifier.setPharmacist(getOrderFulfiller(order));
						drugOrderSimplifier
						        .setRefillRepeat(PharmacyUtils.getTheCorrectRefillCount(orderToDrugOrder.getNumRefills(),
						            PharmacyUtils.getTotalRefills(order.getOrderId())) + "/"
						                + PharmacyUtils.getTotalRefills(order.getOrderId()));
						drugOrderSimplifier.setDrug(order.getConcept().getDisplayString());
						
						orderList.add(drugOrderSimplifier);
					}
				}
			}
		}
		return orderList.stream().sorted(Comparator.comparing(DrugOrderSimplifier::getDateRefilled))
		        .collect(Collectors.toList());
	}
	
	private String getOrderFulfiller(Order order) {
		OrderAttribute orderFulfiller = order.getAttributes().stream()
		        .filter(a -> a.getAttributeType().getUuid().equals(ORDER_FULFILLER_NAME_ATTRIBUTE_TYPE_UUID)).findFirst()
		        .orElse(null);
		
		return String.valueOf(orderFulfiller != null ? orderFulfiller.getValueReference() : "");
	}
	
	public List<DrugOrderSimplifier> refillsDoneList(
	        @RequestParam(value = "orderDetailsIdToView", required = false) Integer orderId) {
		List<DrugOrderSimplifier> orderList = new ArrayList<>();
		DrugOrderSimplifier drugOrderSimplifier;
		if (orderId != null) {
			DrugOrder drugOrder = (DrugOrder) Context.getOrderService().getOrder(orderId);
			Set<Order> orderListRelatedToThisOrder = Context.getEncounterService()
			        .getEncounter(drugOrder.getEncounter().getEncounterId()).getOrders();
			if (orderListRelatedToThisOrder != null) {
				orderListRelatedToThisOrder = orderListRelatedToThisOrder.stream().filter(
				    o -> o.getFulfillerStatus() != null && o.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED))
				        .collect(Collectors.toSet());
				for (Order order : orderListRelatedToThisOrder) {
					DrugOrder orderTodrugOrder = Context.getService(BotswanaEmrService.class)
					        .getDrugOrderById(order.getOrderId());
					if (drugOrder.getConcept().equals(order.getConcept())) {
						drugOrderSimplifier = new DrugOrderSimplifier(drugOrder);
						if (order.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED)) {
							if (order.getPreviousOrder() == null) {
								drugOrderSimplifier.setDateRefilled(
								    formatDateWithoutTime(order.getDateActivated(), "yyyy-MM-dd hh:mm a"));
							} else if (order.getPreviousOrder().getFulfillerStatus()
							        .equals(Order.FulfillerStatus.COMPLETED)) {
								drugOrderSimplifier.setDateRefilled(formatDateWithoutTime(
								    order.getPreviousOrder().getEffectiveStopDate(), "yyyy-MM-dd hh:mm a"));
							}
						} else {
							drugOrderSimplifier.setDateRefilled("");
						}
						drugOrderSimplifier.setFacility(order.getEncounter().getLocation().getName());
						drugOrderSimplifier.setPharmacist(getOrderFulfiller(order));
						drugOrderSimplifier
						        .setRefillRepeat(PharmacyUtils.getTheCorrectRefillCount(orderTodrugOrder.getNumRefills(),
						            PharmacyUtils.getTotalRefills(order.getOrderId())) + "/"
						                + PharmacyUtils.getTotalRefills(order.getOrderId()));
						drugOrderSimplifier.setDrug(order.getConcept().getDisplayString());
						
						orderList.add(drugOrderSimplifier);
					}
				}
			}
		}
		
		return orderList.stream().sorted(Comparator.comparing(DrugOrderSimplifier::getDateRefilled))
		        .collect(Collectors.toList());
	}
	
	public SimpleObject sessionStatus(UiSessionContext uiSessionContext) {
		SimpleObject sm = new SimpleObject();
		sm.put("active", uiSessionContext.isAuthenticated());
		return sm;
	}
	
	@Data
	private static class SimpleBatch {
		
		String batchNumber;
		
		String expiryDate;
		
		Integer quantity;
		
		Integer id;
		
		String itemStockUuid;
		
		String code;
		
		String productName;
		
		Integer batchOperation;
		
		public SimpleBatch(Integer id, String batchNumber, String expiryDate, Integer quantity) {
			this.batchNumber = batchNumber;
			this.expiryDate = expiryDate;
			this.quantity = quantity;
			this.id = id;
		}
		
		public SimpleBatch(Integer id, String batchNumber, String expiryDate, Integer quantity, String itemStockUuid,
		    String code, String productName, Integer batchOperation) {
			this.batchNumber = batchNumber;
			this.expiryDate = expiryDate;
			this.quantity = quantity;
			this.id = id;
			this.itemStockUuid = itemStockUuid;
			this.code = code;
			this.productName = productName;
			this.batchOperation = batchOperation;
		}
	}
	
	public List<SimpleObject> getDistributedStockOPerationsByDate(@RequestParam(value = "endDate") Date endDate,
	        @RequestParam(value = "startDate") Date startDate, UiUtils ui,
	        @SpringBean("locationService") LocationService locationService,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        
	        UiSessionContext uiSessionContext) throws ParseException {
		List<Stockroom> stockrooms = new ArrayList<>();
		// stockrooms.add(StockUtils.getMainStockroom(location));
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		
		// Get my main stockrooms
		List<Stockroom> myMainStockrooms = new ArrayList<>();
		myMainStockrooms.add(StockUtils.getMainStockroom(uiSessionContext.getSessionLocation()));
		// Get all main stockrooms in all facilities
		List<Stockroom> allMainStockrooms = BotswanaInventoryContext.getStockRoomDataService().getAll(false);
		// Keep stockrooms from other facilities
		for (Stockroom stockroom : myMainStockrooms) {
			allMainStockrooms.remove(stockroom);
		}
		
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		
		List<StockOperation> stockOperationsReports = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			
			stockOperationTemplate.setStatus(StockOperationStatus.COMPLETED);
			stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getDistribution());
			
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			for (StockOperation stockOperation : stockOperations) {
				if (stockOperation.getOperationDate().compareTo(startDate) >= 0
				        && stockOperation.getOperationDate().compareTo(endDate) <= 0) {
					
					if (sourceStockRoomId != null
					        && BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId) != null) {
						
						if (stockOperation.getSource() == BotswanaInventoryContext.getStockRoomDataService()
						        .getById(sourceStockRoomId)) {
							stockOperationsReports.add(stockOperation);
							
						} else {}
					} else {
						stockOperationsReports.add(stockOperation);
						
					}
				}
			}
		}
		List<Location> locations = new ArrayList<>();
		
		locations = Context.getLocationService()
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		locations.remove(uiSessionContext.getSessionLocation());
		List<Stockroom> sourceStockrooms = new ArrayList<>();
		sourceStockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		Stockroom mainStockroom = StockUtils.getMainStockroom(uiSessionContext.getSessionLocation());
		List<Stockroom> destinationStockrooms = new ArrayList<>();
		
		return SimpleObject.fromCollection(stockOperationsReports, ui, "destination", "id", "uuid", "description", "creator",
		    "destination", "operationDate", "status");
	}
	
	public SimpleObject checkPatientEnrollArt(@RequestParam(value = "patientUuid") String patientUuid) {
		SimpleObject simpleObject = new SimpleObject();
		
		Patient patient = Context.getPatientService().getPatientByUuid(patientUuid);
		//Get Active patient on art program
		
		boolean isArtEnrolled = isEnrolledInProgram(patient, ART_PROGRAM_UUID);
		simpleObject.put("isArtEnrolled", isArtEnrolled);
		return simpleObject;
	}
	
	public SimpleObject checkPatientEnrollTpt(@RequestParam(value = "patientUuid") String patientUuid) {
		SimpleObject simpleObject = new SimpleObject();
		
		Patient patient = Context.getPatientService().getPatientByUuid(patientUuid);
		
		//Get Active patient on tpt program
		boolean isTptEnrolled = isEnrolledInProgram(patient, TPT_PROGRAM_UUID);
		simpleObject.put("isTptEnrolled", isTptEnrolled);
		return simpleObject;
	}
	
	public SimpleObject lastVmmcOperationFormEncounter(@RequestParam(value = "patientUuid") Patient patient,
	        @RequestParam(value = "formUuid") String formUuid) {
		FormService formService = Context.getFormService();
		SimpleObject simpleObject = new SimpleObject();
		
		String locationName = "";
		String surgeonName = "";
		String anesthetistName = "";
		String scrubNurse = "";
		int vmmcTechnique = 0;
		String operationDiagnosis = "";
		String anaestheticTypeRecord = "";
		int anaestheticDrugGivenRecord = 0;
		
		Form form = formService.getFormByUuid(formUuid);
		EncounterRole surgeonEncounterRole = Context.getEncounterService()
		        .getEncounterRoleByUuid(SURGEON_ENCOUNTER_ROLE_UUID);
		EncounterRole anesthetistEncounterRole = Context.getEncounterService()
		        .getEncounterRoleByUuid(ANESTHETIST_ENCOUNTER_ROLE_UUID);
		EncounterRole scrubNurseEncounterRole = Context.getEncounterService()
		        .getEncounterRoleByUuid(SCRUB_NURSE_ENCOUNTER_ROLE_UUID);
		
		List<Encounter> filledFormEncounter = Context.getEncounterService().getEncounters(new EncounterSearchCriteria(
		        patient, null, null, null, null, Arrays.asList(form), null, null, null, null, false));
		String encounterDate = "";
		if (!filledFormEncounter.isEmpty()) {
			Encounter lastFilledEncounter = filledFormEncounter.get(filledFormEncounter.size() - 1);
			locationName = lastFilledEncounter.getLocation().getName();
			final Date[] operationDate = {null};
			lastFilledEncounter.getObs().forEach(obs -> {
				if (obs.getConcept().getUuid().equals("160715AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")) {
					operationDate[0] = obs.getValueDatetime();
				}
			});

			if (operationDate[0] == null) {
				operationDate[0] = lastFilledEncounter.getEncounterDatetime();
			}
			encounterDate = formatDateWithoutTime(operationDate[0], "yyyy-MM-dd");
			
			for (Obs obs : lastFilledEncounter.getAllObs()) {
				if (obs.getConcept().getUuid().equals(VMMC_TECHNIQUE_CONCEPT_UUID)) {
					vmmcTechnique = obs.getValueCoded().getConceptId();
				}
			}
			
			for (EncounterProvider encounterProvider : lastFilledEncounter.getEncounterProviders()) {
				
				if (encounterProvider.getEncounterRole().equals(surgeonEncounterRole)) {
					surgeonName = encounterProvider.getProvider().getName();
				} else if (encounterProvider.getEncounterRole().equals(anesthetistEncounterRole)) {
					anesthetistName = encounterProvider.getProvider().getName();
				} else if (encounterProvider.getEncounterRole().equals(scrubNurseEncounterRole)) {
					scrubNurse = encounterProvider.getProvider().getName();
				}
				
			}
		}
		
		simpleObject.put("locationName", locationName);
		simpleObject.put("encounterDate", encounterDate);
		simpleObject.put("surgeonName", surgeonName);
		simpleObject.put("anesthetistName", anesthetistName);
		simpleObject.put("scrubNurse", scrubNurse);
		simpleObject.put("vmmcTechnique", vmmcTechnique);
		simpleObject.put("operationDiagnosis", operationDiagnosis);
		simpleObject.put("anaestheticTypeRecord", anaestheticTypeRecord);
		simpleObject.put("anaestheticDrugGivenRecord", anaestheticDrugGivenRecord);
		
		return simpleObject;
	}
	
	public int availableTestKitQuantity(String itemUuid, Integer stockRoomId) {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		int quantity = 0;
		
		if (stockRoomId != null && itemUuid != null) {
			ItemSearch itemSearch = new ItemSearch();
			Item item = new Item();
			item.setUuid(itemUuid);
			itemSearch.setTemplate(item);
			iItemDataService.getItemsByItemSearch(itemSearch);
			Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
			if (stockroom != null) {
				ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, stockroom);
				if (itemStock != null) {
					quantity = itemStock.getQuantity();
				}
			}
		}
		
		return quantity;
	}
	
	public SimpleObject getPatientIdentifierAttributes(@RequestParam(value = "patientUuid") Patient patient) {
		EnumMap<IDENTIFIER, PatientIdentifier> patientIdentifiers = new EnumMap<>(IDENTIFIER.class);
		patientIdentifiers.put(IDENTIFIER.PPN, patient.getPatientIdentifier("Passport Number"));
		SimpleObject simpleObject = new SimpleObject();
		for (Map.Entry<IDENTIFIER, PatientIdentifier> patientIdentifier : patientIdentifiers.entrySet()) {
			if (ObjectUtils.firstNonNull(patientIdentifier.getValue() != null)) {
				simpleObject.put("identifierType", patientIdentifier.getKey().getIdentificationType());
				simpleObject.put("identifierValue", patientIdentifier.getValue().getIdentifier());
				break;
			}
			simpleObject.put("identifierType", "");
			simpleObject.put("identifierValue", "");
		}
		return simpleObject;
	}
	
	public SimpleObject fetchEncounterType(@RequestParam(value = "uuid") String uuid) {
		SimpleObject simpleObject = new SimpleObject();
		EncounterType encounterType = Context.getEncounterService().getEncounterTypeByUuid(uuid);
		if (encounterType != null) {
			simpleObject.put("id", encounterType.getEncounterTypeId());
			simpleObject.put("status", "success");
		} else {
			simpleObject.put("status", "failed");
		}
		return simpleObject;
	}
	
	// Fetch HTS service locations and assign to simple object
	public SimpleObject fetchHtsServicePoints(UiSessionContext uiSessionContext, UiUtils ui) {
		List<Location> htsServiceLocations = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName(HTS_LOCATION_TAG_NAME));
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		simpleObject.put("servicePoints", SimpleObject.fromCollection(htsServiceLocations, ui, "name"));
		return simpleObject;
	}
	
	public SimpleObject getDateOfLastHivTest(@RequestParam(value = "patientUuid") Patient patient) {
		
		FormService formService = Context.getFormService();
		Form htsRegisterForm = formService.getFormByUuid(HTS_DIAGNOSTICS_TEST_FORM_UUID);
		SimpleObject simpleObject = new SimpleObject();
		Date htsDailyEncounterDate = null;
		
		Obs lastfilledHivDateObs = getLatestObs(patient.getPerson(), null, HIV_TEST_DATE_CONCEPT_UUID, null, null);
		List<Encounter> filledHtsDailyEncounters = Context.getEncounterService().getEncounters(new EncounterSearchCriteria(
		        patient, null, null, null, null, Arrays.asList(htsRegisterForm), null, null, null, null, false));
		
		if (filledHtsDailyEncounters != null && !filledHtsDailyEncounters.isEmpty()) {
			Encounter encounter = filledHtsDailyEncounters.get(0);
			htsDailyEncounterDate = encounter.getEncounterDatetime();
		}
		
		if (htsDailyEncounterDate != null && lastfilledHivDateObs != null) {
			simpleObject.put("lastfilledHivDateObs",
			    formatDateWithoutTime(lastfilledHivDateObs.getDateCreated(), "yyyy-MM-dd"));
			simpleObject.put("htsDailyEncounterDate", formatDateWithoutTime(htsDailyEncounterDate, "yyyy-MM-dd"));
		} else {
			simpleObject.put("lastfilledHivDateObs", "");
			simpleObject.put("htsDailyEncounterDate", "");
		}
		return simpleObject;
	}
	
	public SimpleObject getCountOfEncounters(@RequestParam(value = "patientUuid") Patient patient,
	        @RequestParam(value = "encounterTypeUuid") String encounterTypeUuid) {
		EncounterType encounterType = Context.getEncounterService().getEncounterTypeByUuid(encounterTypeUuid);
		SimpleObject simpleObject = new SimpleObject();
		
		List<Encounter> filledFormEncounters = Context.getEncounterService().getEncounters(new EncounterSearchCriteria(
		        patient, null, null, null, null, null, Collections.singletonList(encounterType), null, null, null, false));
		
		if (!filledFormEncounters.isEmpty()) {
			simpleObject.put("filledFormEncounterSize", filledFormEncounters.size());
		} else {
			simpleObject.put("filledFormEncounterSize", 0);
		}
		
		return simpleObject;
	}
	
	public SimpleObject getLastestDisclosurePlanObs(@RequestParam(value = "patientUuid") Patient patient,
	        @RequestParam(value = "encounterTypeUuid") String encounterTypeUuid) {
		SimpleObject simpleObject = new SimpleObject();
		EncounterType encounterType = Context.getEncounterService().getEncounterTypeByUuid(encounterTypeUuid);
		boolean partner = false;
		boolean family = false;
		boolean other = false;
		boolean none = false;
		
		List<Encounter> filledFormEncounter = Context.getEncounterService().getEncounters(new EncounterSearchCriteria(
		        patient, null, null, null, null, null, Collections.singletonList(encounterType), null, null, null, false));
		
		if (!filledFormEncounter.isEmpty()) {
			Encounter lastFilledEncounter = filledFormEncounter.get(filledFormEncounter.size() - 1);
			for (Obs obs : lastFilledEncounter.getAllObs()) {
				if (obs.getConcept().getUuid().equals(DISCLOSURE_PLAN_CONCEPT_UUID)) {
					if (obs.getValueCoded().getUuid().equals(HIV_STATUS_DISCLOSED_PARTNER_CONCEPT_UUID)
					        || obs.getValueCoded().getUuid().equals(CURRENT_SEX_PARTNER_CONCEPT_UUID)) {
						partner = true;
					}
					if (obs.getValueCoded().getUuid().equals(HIV_DISCLOSED_TO_OTHER_FAMILY_MEMBERS_CONCEPT_UUID)) {
						family = true;
					}
					if (obs.getValueCoded().getUuid().equals(HIV_DISCLOSED_TO_FRIENDS_AND_NEIGHBOURS_CONCEPT_UUID)
					        || obs.getValueCoded().getUuid().equals(RELIGIOUS_LEADER_CONCEPT_UUID)) {
						other = true;
					}
					if (obs.getValueCoded().getUuid().equals(NO_DISCLOSURE_PLAN_CONCEPT_UUID)) {
						none = true;
					}
				}
			}
		}
		
		simpleObject.put("partner", partner);
		simpleObject.put("family", family);
		simpleObject.put("other", other);
		simpleObject.put("none", none);
		
		return simpleObject;
	}
	
	// Get the latest patient hiv status
	public SimpleObject getLatestHivStatus(@RequestParam(value = "patientUuid") Patient patient) {
		SimpleObject simpleObject = new SimpleObject();
		Concept latestHivStatus = BotswanaEmrUtils.getPatientHivStatus(patient);
		
		if (latestHivStatus != null) {
			simpleObject.put("latestHivStatus", latestHivStatus.getDisplayString());
		} else {
			simpleObject.put("latestHivStatus", "");
		}
		
		return simpleObject;
	}
	
	// Get the latest patient hiv test date
	public SimpleObject getLastestHivTestDate(@RequestParam(value = "patientUuid") Patient patient) {
		SimpleObject simpleObject = new SimpleObject();
		Obs latestHivTestDateObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null, HIV_TEST_DATE_CONCEPT_UUID,
		    patient.getDateCreated(), new Date());
		
		if (latestHivTestDateObs != null) {
			simpleObject.put("latestHivTestDate",
			    DateUtils.getStringDate(latestHivTestDateObs.getValueDate(), "dd/MM/yyyy"));
		} else {
			simpleObject.put("latestHivTestDate", "");
		}
		
		return simpleObject;
	}
	
	public SimpleObject getLastestPostOperationObs(@RequestParam(value = "patientUuid") Patient patient,
	        UiSessionContext uiSessionContext) {
		SimpleObject simpleObject = new SimpleObject();
		Encounter lastFilledPostOpEncounter = latestEncounter(patient,
		    BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE, uiSessionContext.getSessionLocation());
		int count = BotswanaEmrUtils.getCompletedReviews(lastFilledPostOpEncounter, patient);
		
		Obs latestReviewDateObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(),
		    Collections.singletonList(lastFilledPostOpEncounter), BotswanaEmrConstants.ART_NEXT_DATE_CONCEPT_UUID,
		    patient.getDateCreated(), new Date());
		
		simpleObject.put("completedPostEncounters", count);
		
		if (latestReviewDateObs != null) {
			simpleObject.put("latestReviewDateObs",
			    DateUtils.getStringDate(latestReviewDateObs.getValueDate(), "dd/MM/yyyy"));
		} else {
			simpleObject.put("latestReviewDateObs", "");
		}
		
		return simpleObject;
	}
	
	public SimpleObject getStockCardDetails(UiSessionContext uiSessionContext,
	        @RequestParam(value = "itemCode", required = false) String itemCode,
	        @SpringBean("locationService") LocationService locationService) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		SimpleObject simpleObject = new SimpleObject();
		
		if (uiSessionContext.getCurrentUser() != null && itemCode != null) {
			Item inventoryItem = BotswanaInventoryContext.getItemDataService().getItemByCode(itemCode);
			LocationAttributeType locationAttributeType = locationService
			        .getLocationAttributeTypeByUuid(MASTER_FACILITY_CODE_UUID);
			String globalProperty = Context.getAuthenticatedUser().getUserProperty(USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
			    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
			
			List<Location> locations = locationService
			        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
			
			Set<Stockroom> stockRooms = new HashSet<>();
			
			for (Stockroom stockroom : stockrooms) {
				Set<Stockroom> stockroomSet = stockroom.getItems().stream().filter(i -> i.getItem().equals(inventoryItem))
				        .map(ItemStock::getStockroom).collect(Collectors.toSet());
				stockRooms.addAll(stockroomSet);
			}
			
			LocationAttribute facilityCode = null;
			for (Location location : locations) {
				if (location.getName().equals(globalProperty)) {
					facilityCode = location.getAttributes().stream()
					        .filter(a -> a.getAttributeType().equals(locationAttributeType)).findFirst().orElse(null);
				}
			}
			
			simpleObject.put("facilityCode", facilityCode != null ? facilityCode.getValue() : "");
			simpleObject.put("stockRooms", stockRooms);
			simpleObject.put("itemCode", itemCode);
			simpleObject.put("hospital", globalProperty);
			InventoryItemSimplifier simplifiedItem = InventoryItemSimplifier.simplify(inventoryItem);
			
			simpleObject.put("commodityCode", simplifiedItem.getCode());
			simpleObject.put("commodityName", simplifiedItem.getName());
			simpleObject.put("unitOfIssue", simplifiedItem.getUnitOfIssue());
			
		} else {
			simpleObject.put("facilityCode", "");
			simpleObject.put("hospital", "");
			simpleObject.put("commodityCode", "");
			simpleObject.put("commodityName", "");
			simpleObject.put("unitOfIssue", "");
			simpleObject.put("stockRooms", "");
			simpleObject.put("itemCode", null);
		}
		
		return simpleObject;
	}
	
	public SimpleObject fetchLabResultsFromShr(@RequestParam(value = "taskId", required = true) String taskId,
	        UiUtils uiUtils) {
		BotswanaLabIntegrationService labIntegrationService = Context.getRegisteredComponent(
		    "botswanaemr.BotswanaLabIntegrationServiceImpl", BotswanaLabIntegrationServiceImpl.class);
		BotswanaRegistryResponse fetchLabResultFromShr = labIntegrationService.fetchLabResultFromShr(taskId);
		Object response = fetchLabResultFromShr.getResponse();
		// Extract FHRI Observation from the response
		if (response instanceof List) {
			List<Object> observations = (List<Object>) response;
			List<SimpleObject> simpleObjects = new ArrayList<>();
			for (Object observation : observations) {
				// extract the code and value from the unknown object observation
				simpleObjects.add(SimpleObject.fromObject(observation, uiUtils, "value"));
			}
			return SimpleObject.create("status", fetchLabResultFromShr.getStatus(), "taskStatus",
			    fetchLabResultFromShr.getTaskStatus(), "message", fetchLabResultFromShr.getMessage(), "response",
			    simpleObjects);
		}
		
		return SimpleObject.fromObject(fetchLabResultFromShr, uiUtils, "status", "message", "response");
	}
	
	public List<SimpleObject> getActiveDrugOrdersByPatientAndDrugConcept(
	        @RequestParam(value = "patientId", required = false) int patientId,
	        @RequestParam(value = "drugConceptUuid", required = false) String drugConceptUuid) {
		List<SimpleObject> activeDrugOrdersList = new ArrayList<>();
		OrderService orderService = Context.getOrderService();
		Concept drugConcept = Context.getConceptService().getConceptByUuid(drugConceptUuid);
		OrderType drugOrderType = orderService.getOrderTypeByName("Drug Order");
		CareSetting careSetting = orderService.getCareSettingByUuid(BotswanaEmrConstants.DEFAULT_CARE_SETTING);
		Patient patient = Context.getPatientService().getPatient(patientId);
		List<Order> activeDrugOrders = orderService.getActiveOrders(patient, drugOrderType, careSetting, new Date());
		activeDrugOrders = activeDrugOrders.stream().filter(o -> o.getConcept().equals(drugConcept))
		        .collect(Collectors.toList());
		
		for (Order order : activeDrugOrders) {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("date", BotswanaEmrUtils.formatDateWithoutTime(order.getDateActivated(), "yyyy-MM-dd"));
			simpleObject.put("orderer", order.getOrderer().getPerson().getPersonName().getFullName());
			activeDrugOrdersList.add(simpleObject);
		}
		
		return activeDrugOrdersList;
	}
	
	public void updateOrderAttributes(@RequestParam(value = "orderUuid") String orderUuid,
	        @RequestParam(value = "facility") String facility, @RequestParam(value = "prescriber") String prescriber, UiSessionContext uiSessionContext)
	        throws ValidationException {
		
		DrugOrder order = (DrugOrder) Context.getOrderService().getOrderByUuid(orderUuid);
		
		if (order.isActive()) {
			DrugOrder revisedOrder = order.cloneForRevision();
			revisedOrder.setEncounter(order.getEncounter());
			revisedOrder.setOrderer(order.getOrderer());
			OrderAttributeType sourceFacilityAttributeType = Context.getOrderService()
			        .getOrderAttributeTypeByUuid(ORDER_SOURCE_FACILITY_NAME_ATTRIBUTE_TYPE_UUID);
			OrderAttribute sourceFacilityOrderAttribute = getOrderAttribute(revisedOrder, sourceFacilityAttributeType,
			    facility);
			
			OrderAttributeType prescriberAttributeType = Context.getOrderService()
					.getOrderAttributeTypeByUuid(PRESCRIBER_ATTRIBUTE_TYPE);
			
			OrderAttribute prescriberOrderAttribute = getOrderAttribute(revisedOrder, prescriberAttributeType, prescriber);
			revisedOrder.addAttribute(prescriberOrderAttribute);
			revisedOrder.addAttribute(sourceFacilityOrderAttribute);
			
			Context.getOrderService().saveOrder(revisedOrder, null);
		}
	}
	
	public SimpleObject fetchPatientFlags(@RequestParam("patientId") Patient patient, UiUtils ui) {
		List<PatientFlag> patientFlags = new ArrayList<>();
		Boolean hivPositive = BotswanaEmrUtils.isHivPositive(patient);
		
		VisitType visitType = Context.getVisitService().getVisitTypeByUuid(FACILITY_VISIT_VISIT_TYPE_UUID);
		Visit lastPatientVisit = BotswanaEmrUtils.getLastPatientVisit(patient, visitType);
		// Check if the patient is TB presumptive and not yet enrolled in TB treatment
		Boolean tbPresumptive = BotswanaEmrUtils.isTbPresumptive(patient, lastPatientVisit);
		Boolean tbEnrolled = BotswanaEmrUtils.isEnrolledInProgram(patient, BotswanaEmrConstants.TB_PROGRAM_UUID);
		
		Concept xpertResultConcept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.GENE_XPERT_RESULTS);
		// Check XPERT results
		Obs xpertResultObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null, xpertResultConcept.getUuid(), null,
		    null);
		
		// Check Rifampicin resistance
		Concept rifampicinResistanceConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.RIFAMPICIN_RESISTANCE_CONCEPT_UUID);
		Obs rifampicinResistanceObs = null;
		
		if (xpertResultObs != null) {
			rifampicinResistanceObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(),
			    Collections.singletonList(xpertResultObs.getEncounter()), rifampicinResistanceConcept.getUuid(), null, null);
		}
		
		// Check MTB Detection level
		Concept mtbDetectionLevelConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.MTB_DETECTED_CONCEPT_UUID);
		Obs mtbDetectionLevelObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    mtbDetectionLevelConcept.getUuid(), null, null);
		
		Boolean mtbDetected = false;
		Boolean mtbNotDetected = false;
		Boolean rifampicinResistance = false;
		Boolean noRifampicinResistance = false;
		Boolean mtbDetectedTrace = false;
		
		if (!tbEnrolled && xpertResultObs != null) {
			mtbDetected = xpertResultObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.Mycobacterium_tuberculosis);
			mtbNotDetected = xpertResultObs.getValueCoded().getUuid()
			        .equals(BotswanaEmrConstants.MTB_NOT_DETECTED_CONCEPT_UUID);
			rifampicinResistance = rifampicinResistanceObs != null && rifampicinResistanceObs.getValueCoded().getUuid()
			        .equals(BotswanaEmrConstants.Mycobacterium_tuberculosis_detected_with_rifampin_resistance);
			noRifampicinResistance = rifampicinResistanceObs != null && rifampicinResistanceObs.getValueCoded().getUuid()
			        .equals(BotswanaEmrConstants.Mycobacterium_tuberculosis_detected_without_rifampin_resistance_misc);
			mtbDetectedTrace = mtbDetectionLevelObs != null
			        && mtbDetectionLevelObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.Trace);
		}
		
		if (patient.getAge() > 12) {
			if (tbPresumptive && !tbEnrolled) {
				PatientFlag tbPresumptiveFlag = new PatientFlag("TB Presumptive", "info", 1,
				        "Collect one sputum sample for Xpert. "
				                + "Test for HIV if HIV unknown or previous HIV test was negative");
				
				patientFlags.add(tbPresumptiveFlag);
			}
			
			// MTB Detection flags for the patient. Patient should be a TB presumptive and not yet enrolled to TB treatment
			
			if (mtbDetected) {
				if (noRifampicinResistance) {
					// TB Diagnosed & sensitive to RIF
					PatientFlag mtbFlag = new PatientFlag("MTB Detected, No RIF resistancs", "risk", 3,
					        "1) Start drug sensitive TB (DS TB) regimen. 2) Collect new sample for culture and DST. 3) Provide ART & CPT if HIV positive");
					patientFlags.add(mtbFlag);
				} else if (rifampicinResistance) {
					// Presumptive MDR-TB. TB Diagnosed & RIF Resistant
					PatientFlag mtbFlag = new PatientFlag("MTB Detected, RIF resistance detected", "risk", 3,
					        "1. Refer to DR-TB site for DR-TB Managment. 2) Collect new sample for LPA, Culture & DST. 3) Provide ART & CPT if HIV positive");
					patientFlags.add(mtbFlag);
				} else {
					if (mtbDetectedTrace) {
						// For patients who are HIV+ve and those with Extra Pulmonary TB (EPTB)
						if (hivPositive) {
							PatientFlag mtbFlag = new PatientFlag("MTB Detected, RIF Inderminate", "risk", 3,
							        "1) Start DS-TB 2) collect new sample for LPA, Culture & DST");
							patientFlags.add(mtbFlag);
						} else {
							PatientFlag mtbFlag = new PatientFlag("MTB Detected, RIF Inderminate", "risk", 3,
							        "Repeat Xpert assay using a fresh sample");
							patientFlags.add(mtbFlag);
						}
					} else {
						// TB Diagnosed & no result available for RIF resistance
						PatientFlag mtbNotDetectedFlag = new PatientFlag("TB Not Diagnosed", "info", 3,
						        "1) Repeat Xpert & collect sample for culture & DST "
						                + "2) Re-assess patient clinically, including for EPTB, PCP etc "
						                + "3) Do chest x-ray & other investigations as needed");
						patientFlags.add(mtbNotDetectedFlag);
					}
				}
			} else if (mtbNotDetected) {
				// TB Not Diagnosed by Xpert
				if (hivPositive) {
					PatientFlag mtbNotDetectedFlag = new PatientFlag("TB Not Diagnosed", "info", 3,
					        "1) Repeat Xpert & collect sample for culture & DST 2) Re-assess patient clinically, including for EPTB, PCP etc 3. Do chest x-ray & other investigations as needed");
					patientFlags.add(mtbNotDetectedFlag);
				} else {
					PatientFlag mtbNotDetectedFlag = new PatientFlag("TB Not Diagnosed", "info", 3,
					        "1) Consider other diagnoses & manage with appropriate antibiotics 2) Repeat Xpert & collect sample for culture & DST 3) Reassess patient after 1 week");
					patientFlags.add(mtbNotDetectedFlag);
				}
			}
			
		} else {
			if (tbPresumptive && !tbEnrolled) {
				PatientFlag tbPresumptiveFlag = new PatientFlag("TB Presumptive", "info", 1,
				        "1. Collect one sputum sample or gastric aspirate sample or induced sputum "
				                + " & split inot 2 for Xpert & Culture & DST(C/DST) or send stoll for Xpert as initial test "
				                + "2. CXR (PA & Lateral) " + "3. TST");
				
				patientFlags.add(tbPresumptiveFlag);
			}
			
			if (mtbDetected) {
				if (noRifampicinResistance) {
					// MTB Detected, No rifampicin resistance
					PatientFlag mtbFlag = new PatientFlag("MTB Detected, No RIF resistancs", "risk", 3,
					        "1) Start DS TB regimen if no TB Tretament history. 2) Follow up culture results");
					patientFlags.add(mtbFlag);
				} else if (rifampicinResistance) {
					// MTB Detected, RIF resistance detected
					PatientFlag mtbFlag = new PatientFlag("MTB Detected, RIF resistance detected", "risk", 3,
					        "1) Refer to MDR-TB Site for MDR TB treatment initiation. 2) Follow sample sent for culture & DST and request LPA on sample");
					patientFlags.add(mtbFlag);
				} else {
					// MT Detected  or trace/Rif resistance indetrminate or unknown
					PatientFlag mtbFlag = new PatientFlag("MTB Detected, RIF resistance unknown", "risk", 3,
					        "1) Send another sample for repease Xpert 2) While waiting, start DS-TB Regimen. 3) If xpert remains indeterminate, send another sample for LPA, Culture & DST");
					patientFlags.add(mtbFlag);
				}
			} else if (mtbNotDetected) {
				// MTD NOt detected
				PatientFlag mtbNotDetectedFlag = new PatientFlag("MTB Not Diagnosed", "risk", 3,
				        "Consider other results and follow guideline algorithm");
				patientFlags.add(mtbNotDetectedFlag);
			} else {
				PatientFlag mtbNotDetectedFlag = new PatientFlag("Invalid/ error", "info", 3,
				        "1) Collect another sample 2) Repeat Xpert - if the specimen is pleural fluid, send for culture and DST and not genXpert. If specimen is bloody, repeat the sample. Xpert cannot be performed on bloody samples");
				patientFlags.add(mtbNotDetectedFlag);
			}
		}
		
		SimpleObject patientFlagsObject = new SimpleObject();
		patientFlagsObject.put("status", "success");
		patientFlagsObject.put("results",
		    SimpleObject.fromCollection(patientFlags, ui, "flagName", "tag", "priority", "message"));
		
		return patientFlagsObject;
	}
	
	@Data
	final class PatientFlag {
		
		public PatientFlag() {
		}
		
		public PatientFlag(String flagName, String tag, int priority, String message) {
			this.flagName = flagName;
			this.tag = tag;
			this.priority = priority;
			this.message = message;
		}
		
		String flagName;
		
		String tag;
		
		Integer priority;
		
		String message;
	}
}
