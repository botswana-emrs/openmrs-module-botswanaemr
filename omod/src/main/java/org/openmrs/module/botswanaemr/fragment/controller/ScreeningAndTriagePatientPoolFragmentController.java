/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Location;
import org.openmrs.Provider;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_FORM_UUID;

public class ScreeningAndTriagePatientPoolFragmentController extends QueuePatientFragmentController {
	
	public void controller(UiSessionContext uiSessionContext, @SpringBean FragmentModel pageModel) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		pageModel.addAttribute("tbScreeningFormUuid", TB_SCREENING_FORM_UUID);
		pageModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		pageModel.addAttribute("providerList", providerList);
		pageModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location location = Context.getLocationService().getLocationByUuid(BotswanaEmrConstants.NURSING_PORTAL_UUID);
		if (location != null) {
			pageModel.addAttribute("nurseServicePoint", location.getUuid());
		}
	}
}
