package org.openmrs.module.botswanaemr.fragment.controller.widget;

import org.openmrs.Patient;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class PatientFlagsFragmentController {
	
	public void controller(@FragmentParam("patientId") Patient patient, @FragmentParam("category") String category,
	        FragmentModel model) {
		model.addAttribute("patient", patient);
		model.addAttribute("category", category);
	}
	
}
