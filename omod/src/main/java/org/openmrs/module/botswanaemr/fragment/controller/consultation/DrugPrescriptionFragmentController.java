/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Order;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.DrugOrderExtension;
import org.openmrs.module.botswanaemr.model.SimplifiedDrug;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

public class DrugPrescriptionFragmentController {
	
	public void controller() {
		
	}
	
	public String saveDrugOrderExtension(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "orderUuid", required = false) String orderUuid, UiSessionContext sessionContext) {
		
		Order order = Context.getOrderService().getOrderByUuid(orderUuid);
		
		DrugOrderExtension drugOrderExtension = new DrugOrderExtension();
		if (order != null) {
			drugOrderExtension.setOrder(order);
			drugOrderExtension.setDosingInstructions(order.getInstructions());
		}
		
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		DrugOrderExtension saveDrugOrderExtension = botswanaEmrService.saveDrugOrderExtension(drugOrderExtension);
		
		return saveDrugOrderExtension.toString();
	}
	
	public String voidOrder(UiUtils ui, @RequestParam(value = "orderUuid", required = false) String orderUuid,
	        UiSessionContext sessionContext) {
		Order order = Context.getOrderService().getOrderByUuid(orderUuid);
		if (order != null) {
			Context.getOrderService().voidOrder(order, "Voided by Botswana Emr - Drug Prescription not completed");
		}
		return "";
	}
	
	public SimpleObject getSimplifiedDrugs() {
		List<SimplifiedDrug> simplifiedDrugs = BotswanaEmrUtils.getSimplifiedDrugsList();
		
		return SimpleObject.create("success", true, "data", simplifiedDrugs);
	}
}
