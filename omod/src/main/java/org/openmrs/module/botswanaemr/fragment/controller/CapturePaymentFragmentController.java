/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.http.HttpStatus;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.converter.simplifier.BotswanaEmrPaymentSimplifier;
import org.openmrs.module.botswanaemr.utils.UuidGenerator;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class CapturePaymentFragmentController {
	
	private final BotswanaEmrService botswanaEmrService;
	
	private final List<PaymentMethod> paymentMethodList;
	
	private final List<ServiceProvider> serviceProviderList;
	
	public CapturePaymentFragmentController() {
		botswanaEmrService = Context.getService(BotswanaEmrService.class);
		paymentMethodList = botswanaEmrService.getAllPaymentMethods(false);
		serviceProviderList = botswanaEmrService.getAllServiceProvider(false);
	}
	
	public String get(UiUtils uiUtils, UiSessionContext uiSessionContext, FragmentModel model,
	        @RequestParam("patientId") Patient patient) {
		// Patients below 5 years and patients above 65 Years do not pay
		//the registration fee
		if (patient.getAge() != null) {
			if (patient.getAge() < MIN_AGE_FOR_REG_FEE_PAYMENT || patient.getAge() > MAX_AGE_FOR_REG_FEE_PAYMENT) {
				// Post zero payment to signify registration
				postPaymentWithDefaultValues(patient, 0, 0, uiSessionContext.getSessionLocation());
				Map<String, Object> params = new HashMap<>();
				params.put("patientId", patient.getId());
				params.put("startVisit", "true");
				return "redirect:" + uiUtils.pageLink("botswanaemr", "assignPatient", params);
			}
		}
		
		model.addAttribute("patient", patient);
		// TODO: Move these to global properties
		PatientIdentifier identifier = null;
		boolean localIdExists = false;
		for (PatientIdentifier patientIdentifier : patient.getActiveIdentifiers()) {
			if (patientIdentifier.getIdentifierType().getUuid().equals(BotswanaEmrConstants.NATIONAL_ID_IDENTIFIER_TYPE)
			        || patientIdentifier.getIdentifierType().getUuid()
			                .equals(BotswanaEmrConstants.BIRTH_CERT_IDENTIFIER_TYPE)) {
				localIdExists = true;
			} else if (patientIdentifier.getIdentifierType().getUuid()
			        .equals(BotswanaEmrConstants.PASSPORT_IDENTIFIER_TYPE)) {
				identifier = patientIdentifier;
			}
		}
		
		if (!localIdExists && identifier != null) {
			model.addAttribute("minimumBillableAmount", DEFAULT_BILLABLE_AMOUNT_FOR_FOREIGNERS);
			model.addAttribute("maximumBillableAmount", DEFAULT_BILLABLE_AMOUNT_FOR_FOREIGNERS);
		} else {
			model.addAttribute("minimumBillableAmount", DEFAULT_BILLABLE_AMOUNT_FOR_LOCALS);
			model.addAttribute("maximumBillableAmount", DEFAULT_BILLABLE_AMOUNT_FOR_LOCALS);
		}
		
		List<PaymentMethod> paymentMethods = paymentMethodList.stream()
		        .filter(p -> !Objects.equals(p.getName(), BotswanaEmrConstants.NONE)).collect(Collectors.toList());
		model.addAttribute("paymentMethodList", paymentMethods);
		
		return null;
	}
	
	public String post(UiUtils uiUtils, @RequestParam("patientId") Patient patient,
	        @RequestParam("billableAmount") double billableAmount, @RequestParam("paymentRadio") Integer paymentRadio,
	        @RequestParam(value = "paymentMethod", required = false) Integer paymentMethodId,
	        @RequestParam(value = "serviceProvider", required = false) Integer serviceProviderId,
	        @RequestParam("paymentCode") String paymentCode, UiSessionContext sessionContext) {
		
		Registration payment = new Registration();
		
		//set the attributes needed
		payment.setPatient(patient);
		PaymentMethod method;
		if (paymentMethodId != null) {
			method = paymentMethodList.stream().filter(p -> Objects.equals(p.getPaymentMethodId(), paymentMethodId))
			        .collect(Collectors.toList()).get(0);
		} else {
			method = getDefaultPaymentMethod();
		}
		payment.setPaymentMethod(method);
		payment.setLocation(sessionContext.getSessionLocation());
		ServiceProvider provider;
		if (serviceProviderId != null) {
			provider = serviceProviderList.stream()
			        .filter(sp -> Objects.equals(sp.getServiceProviderId(), serviceProviderId)).collect(Collectors.toList())
			        .get(0);
		} else {
			provider = getDefaultServiceProvider();
		}
		payment.setServiceProvider(provider);
		payment.setPaymentCode(paymentCode);
		payment.setBillableAmount(billableAmount);
		if (paymentRadio == 1) {
			payment.setAmountPaid(billableAmount);
		} else {
			payment.setAmountPaid(0);
		}
		payment.setRegistrationDatetime(new Date());
		payment.setCreator(Context.getAuthenticatedUser());
		payment.setDateCreated(new Date());
		payment.setUuid(UuidGenerator.getNextUuid());
		
		//save the object back to the database
		botswanaEmrService.savePayment(payment);
		
		Map<String, Object> params = new HashMap<>();
		params.put("patientId", patient.getUuid());
		params.put("startVisit", "true");
		return "redirect:" + uiUtils.pageLink("botswanaemr", "assignPatient", params);
	}
	
	public Object postDelayedPayment(UiSessionContext uiSessionContext, @RequestParam("patientId") Patient patient) {
		
		SimpleObject simpleObject = new SimpleObject();
		try {
			Registration payment = postPaymentWithDefaultValues(patient, DEFAULT_BILLABLE_AMOUNT_FOR_LOCALS, 0,
			    uiSessionContext.getSessionLocation());
			
			simpleObject.put("status", HttpStatus.SC_OK);
			simpleObject.put("response", new BotswanaEmrPaymentSimplifier().convert(payment));
		}
		catch (APIException ex) {
			simpleObject.put("status", HttpStatus.SC_INTERNAL_SERVER_ERROR);
			simpleObject.put("response", "Failed: " + ex.getMessage());
			
		}
		
		return simpleObject;
	}
	
	public Object postZeroPayment(UiSessionContext uiSessionContext, Patient patient) {
		
		SimpleObject simpleObject = new SimpleObject();
		try {
			Registration payment = postPaymentWithDefaultValues(patient, 0, 0, uiSessionContext.getSessionLocation());
			simpleObject.put("status", HttpStatus.SC_OK);
			simpleObject.put("response", new BotswanaEmrPaymentSimplifier().convert(payment));
		}
		catch (APIException ex) {
			simpleObject.put("status", HttpStatus.SC_INTERNAL_SERVER_ERROR);
			simpleObject.put("response", "Failed: " + ex.getMessage());
		}
		
		return simpleObject;
	}
	
	private Registration postPaymentWithDefaultValues(Patient patient, Integer billableAmount, Integer amountPaid,
	        Location location) {
		Registration payment = new Registration();
		
		//set the attributes needed
		payment.setPatient(patient);
		
		PaymentMethod method = getDefaultPaymentMethod();
		payment.setPaymentMethod(method);
		payment.setLocation(location);
		
		ServiceProvider provider = getDefaultServiceProvider();
		payment.setServiceProvider(provider);
		payment.setPaymentCode("NA");
		payment.setBillableAmount(billableAmount);
		payment.setAmountPaid(amountPaid);
		payment.setRegistrationDatetime(new Date());
		payment.setCreator(Context.getAuthenticatedUser());
		payment.setDateCreated(new Date());
		payment.setUuid(UuidGenerator.getNextUuid());
		
		payment = botswanaEmrService.savePayment(payment);
		
		return payment;
	}
	
	private ServiceProvider getDefaultServiceProvider() {
		ServiceProvider serviceProvider = serviceProviderList.stream()
		        .filter(s -> s.getName().equals(BotswanaEmrConstants.NONE)).collect(Collectors.toList()).get(0);
		return serviceProvider;
	}
	
	private List<PaymentMethod> paymentMethod() {
		return paymentMethodList;
	}
	
	private PaymentMethod getDefaultPaymentMethod() {
		PaymentMethod paymentMethod = paymentMethodList.stream().filter(p -> p.getName().equals(BotswanaEmrConstants.NONE))
		        .collect(Collectors.toList()).get(0);
		return paymentMethod;
		
	}
	
	private List<ServiceProvider> serviceProvider() {
		return serviceProviderList;
	}
}
