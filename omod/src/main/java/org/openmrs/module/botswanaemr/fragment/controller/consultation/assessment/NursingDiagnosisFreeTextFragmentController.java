package org.openmrs.module.botswanaemr.fragment.controller.consultation.assessment;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public class NursingDiagnosisFreeTextFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam(value = "visitId") Visit visit,
	        @RequestParam("patientId") Patient person, UiSessionContext uiSessionContext) {
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		List<Obs> nursingDiagnosesObs = botswanaEmrService.getObservation(person, visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.NURSING_DIAGNOSIS_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		fragmentModel.addAttribute("nursingDiagnosis", nursingDiagnosesObs);
		fragmentModel.addAttribute("lastNursingDiagnosis",
		    nursingDiagnosesObs.size() > 1 ? nursingDiagnosesObs.get(0) : null);
		fragmentModel.addAttribute("patient", person);
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
	}
	
}
