/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.openmrs.Allergies;
import org.openmrs.Allergy;
import org.openmrs.AllergyReaction;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.module.botswanaemr.comparator.AllergyComparator;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.util.ByFormattedObjectComparator;
import org.springframework.web.bind.annotation.RequestParam;

public class AllergiesFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("patientService") PatientService patientService) {
		
		Allergies allergies = patientService.getAllergies(patient);
		Comparator<Allergy> comparator = new AllergyComparator(new ByFormattedObjectComparator(ui));
		Collections.sort(allergies, comparator);
		
		model.addAttribute("allergies", allergies);
	}
	
	public Map<String, Object> getAllergies(@RequestParam(value = "patientId") String patientId,
	        @SpringBean("patientService") PatientService patientService, UiUtils ui) {
		
		Patient patient = patientService.getPatientByUuid(patientId);
		List<Allergy> allergies = patientService.getAllergies(patient);
		Comparator<Allergy> comparator = new AllergyComparator(new ByFormattedObjectComparator(ui));
		Collections.sort(allergies, comparator);
		
		List<SimpleObject> simpleAllergiesObject = new ArrayList<>();
		
		for (Allergy allergy : allergies) {
			SimpleObject simpleAllergyObject = new SimpleObject();
			simpleAllergyObject.put("id", allergy.getId());
			
			String allergenName;
			if (allergy.getAllergen().isCoded())
				allergenName = allergy.getAllergen().getCodedAllergen().getDisplayString();
			else
				allergenName = allergy.getAllergen().getNonCodedAllergen();
			
			simpleAllergyObject.put("allergen", allergenName);
			
			StringBuilder stringBuilder = getReactions(allergy);
			simpleAllergyObject.put("reactions", stringBuilder.toString());
			simpleAllergyObject.put("comment", allergy.getComment() != null ? allergy.getComment() : "");
			simpleAllergiesObject.add(simpleAllergyObject);
		}
		
		Map<String, Object> result = new HashMap<>();
		result.put("allergies", simpleAllergiesObject);
		return result;
	}
	
	@NotNull
	private static StringBuilder getReactions(Allergy allergy) {
		StringBuilder reactions = new StringBuilder();
		for (int i = 0; i < allergy.getReactions().size(); i++) {
			AllergyReaction reaction = allergy.getReactions().get(i);
			if (i == 0) {
				reactions.append("→ ");
			} else {
				reactions.append(", ");
			}
			String reactionDisplay = reaction.getReactionNonCoded() != null ? reaction.getReactionNonCoded()
			        : String.valueOf(reaction.getReaction());
			reactions.append(reactionDisplay);
		}
		return reactions;
	}
}
