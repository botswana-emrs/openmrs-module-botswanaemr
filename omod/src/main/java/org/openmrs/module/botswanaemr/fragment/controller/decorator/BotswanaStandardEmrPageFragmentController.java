/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.decorator;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import org.jfree.util.Log;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class BotswanaStandardEmrPageFragmentController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	/**
	 * visit--location--to--be--current-facility encounter-location--be--service-point-served
	 **/
	public void controller(FragmentModel model, UiSessionContext sessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @FragmentParam(value = "isRegistrationPage", required = false) String isRegistrationPage,
	        @FragmentParam(value = "isScreeningPage", required = false) String isScreeningPage,
	        @SpringBean("locationService") LocationService locationService) {
		
		Location registrationDesk = null;
		Location nurseDesk = null;
		Location doctorsPortal = null;
		Location art = null;
		Location pharmacy = null;
		Location stockManagementPortal = null;
		Location labServicesPortal = null;
		Location vmmcPortal = null;
		Location sexualReproductiveHealthPortal = null;
		Location tbServicesPortal = null;
		Location hivTestingServicesPortal = null;
		String isPatientQueue = null;
		boolean isAnonymousLocation = false;
		
		if (sessionContext.getSession() == null) {
			isAnonymousLocation = true;
		} else {
			try {
				Context.addProxyPrivilege(VIEW_LOCATIONS);
				Context.addProxyPrivilege(GET_LOCATIONS);
				Object sessionLocationId = sessionContext.getSession().getAttribute(CURRENT_SERVICE_DELIVERY_POINT);
				Location serviceDeliverySessionLocation = Context.getLocationService()
				        .getLocationByUuid(String.valueOf(sessionLocationId));
				
				if (serviceDeliverySessionLocation != null) {
					if (serviceDeliverySessionLocation.equals(locationService.getLocationByUuid(DOCTORS_PORTAL_UUID))) {
						doctorsPortal = locationService.getLocationByUuid(DOCTORS_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(NURSING_PORTAL_UUID))) {
						nurseDesk = locationService.getLocationByUuid(NURSING_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(ART_SERVICES_PORTAL_UUID))) {
						art = locationService.getLocationByUuid(ART_SERVICES_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(PHARMACY_PORTAL_UUID))) {
						pharmacy = locationService.getLocationByUuid(PHARMACY_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(STOCK_MANAGEMENT_PORTAL_UUID))) {
						stockManagementPortal = locationService.getLocationByUuid(STOCK_MANAGEMENT_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(LAB_SERVICES_PORTAL_UUID))) {
						labServicesPortal = locationService.getLocationByUuid(LAB_SERVICES_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation.equals(locationService.getLocationByUuid(VMMC_PORTAL_UUID))) {
						vmmcPortal = locationService.getLocationByUuid(VMMC_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID))) {
						sexualReproductiveHealthPortal = locationService
						        .getLocationByUuid(SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(PATIENT_REGISTRATION_PORTAL_UUID))) {
						registrationDesk = locationService.getLocationByUuid(PATIENT_REGISTRATION_PORTAL_UUID);
						isRegistrationPage = "isRegistration";
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(TB_SERVICES_PORTAL_UUID))) {
						tbServicesPortal = locationService.getLocationByUuid(TB_SERVICES_PORTAL_UUID);
					} else if (serviceDeliverySessionLocation
					        .equals(locationService.getLocationByUuid(HIV_TESTING_SERVICES_PORTAL_UUID))) {
						hivTestingServicesPortal = locationService.getLocationByUuid(HIV_TESTING_SERVICES_PORTAL_UUID);
					} else { // setup default dashboard where user can select available queue
						isPatientQueue = "isPatientQueue";
					}
				} else {
					//redirect to the service point selection page
					isAnonymousLocation = true;
				}
			}
			catch (Exception e) {
				Log.error("Session not found", e);
				isAnonymousLocation = true;
			}
			finally {
				Context.removeProxyPrivilege(VIEW_LOCATIONS);
				Context.removeProxyPrivilege(GET_LOCATIONS);
			}
		}
		
		model.addAttribute("registration", registrationDesk);
		model.addAttribute("nurse", nurseDesk);
		model.addAttribute("doctor", doctorsPortal);
		model.addAttribute("patient", patient);
		model.addAttribute("art", art);
		model.addAttribute("pharmacy", pharmacy);
		model.addAttribute("stockManagementPortal", stockManagementPortal);
		model.addAttribute("labServicesPortal", labServicesPortal);
		model.addAttribute("vmmcPortal", vmmcPortal);
		model.addAttribute("sexualReproductiveHealthPortal", sexualReproductiveHealthPortal);
		model.addAttribute("isInPatientContext", checkIfInPatientContext(patient));
		model.addAttribute("isRegistrationPage", isRegistrationPage);
		model.addAttribute("isAnonymousLocation", isAnonymousLocation);
		model.addAttribute("isPatientQueue", isPatientQueue);
		model.addAttribute("user", Context.getAuthenticatedUser());
		model.addAttribute("tbServicesPortal", tbServicesPortal);
		model.addAttribute("hivTestingServicesPortal", hivTestingServicesPortal);
		model.addAttribute("isScreeningPage", isScreeningPage);
		
		boolean isRegistration;
		User authenticatedUser = sessionContext.getCurrentUser();
		isRegistration = authenticatedUser != null && BotswanaEmrUtils.isRegistrationClerk(authenticatedUser);
		model.addAttribute("isRegistration", isRegistration);
		model.addAttribute("isSuperUser", authenticatedUser.isSuperUser());
	}
	
	private boolean checkIfInPatientContext(Patient patient) {
		boolean isFound = false;
		if (patient != null) {
			isFound = true;
		}
		return isFound;
	}
}
