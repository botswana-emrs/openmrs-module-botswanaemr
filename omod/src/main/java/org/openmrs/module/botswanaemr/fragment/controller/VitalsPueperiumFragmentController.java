/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import lombok.Data;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Arrays;
import java.util.Set;
import java.util.Date;
import java.util.LinkedHashMap;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

public class VitalsPueperiumFragmentController {
	
	public void controller(FragmentModel model, @RequestParam(value = "patientId", required = false) Patient patient) {
		model.addAttribute("patient", patient);
		
	}
	
	public SimpleObject getMotherAndChildrenVitals(@RequestParam("patientId") String patientId) {
		SimpleObject simpleObject = new SimpleObject();
		
		List<String> dateRanges = new ArrayList<>();
		List<ChildObject> childrenData = new ArrayList<>();
		List<Map<String, Object>> childrenList = new ArrayList<>();
		Integer childNumber = 1;
		
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		EncounterService encounterService = Context.getEncounterService();
		if (patient != null) {
			List<Encounter> pueperiumEncounters = encounterService.getEncounters(
			    getEncounterSearchCriteriaUsingEncounterTypes(patient,
			        Arrays.asList(BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.PUERPERIUM_ENCOUNTER_TYPE_UUID))));
			
			List<Encounter> pueperiumEncountersFromVitals = new ArrayList<>();
			
			if (!pueperiumEncounters.isEmpty()) {
				//loop through all and pick the ones with vitals recorded
				List<Encounter> pueperiumEncounterWithVitals = new ArrayList<>();
				for (Encounter encounter : pueperiumEncounters) {
					Set<Obs> obsSet = encounter.getAllObs();
					//check if this encounter has any of the vitals obs
					for (Obs obs : obsSet) {
						if (getVitalsConcepts().contains(obs.getConcept())) {
							//pick that encounter and exit immediately
							pueperiumEncounterWithVitals.add(encounter);
							break;
						}
					}
				}
				if (!pueperiumEncounterWithVitals.isEmpty()) {
					pueperiumEncountersFromVitals.addAll(sortEncountersByEncounterDatetime(pueperiumEncounterWithVitals));
				}
			}
			
			for (Encounter encounter : pueperiumEncountersFromVitals) {
				Date date = encounter.getEncounterDatetime();
				ChildObject childData = null;
				Integer childGroupId = 0;
				dateRanges.add(DateUtils.getStringDate(date, "dd-MM-yyyy"));
				
				for (ChildObject data : childrenData) {
					if (data.getGroupId().equals(childGroupId)) {
						childData = data;
						break;
					}
				}
				
				for (Obs obs : encounter.getAllObs()) {
					
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.PUEPERIUM_PULSE)) {
						childData.getPulses()
						        .add(new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"), obs.getValueNumeric()));
					}
					
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.RESPIRATORY_RATE)) {
						childData.getRespirations()
						        .add(new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"), obs.getValueNumeric()));
					}
					
					if (obs.getObsGroup() != null) {
						childGroupId = obs.getObsGroup().getObsId();
						
						if (childData == null) {
							childData = new ChildObject(childGroupId, childNumber);
							childrenData.add(childData);
							childNumber++;
						}
						
						if (obs.getObsGroup().getObsId().equals(childGroupId)) {
							// Update data for each child
							if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.WEIGHT)) {
								childData.getWeights().add(
								    new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"), obs.getValueNumeric()));
								
							}
							if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.TEMPERATURE)) {
								childData.getTemperatures().add(
								    new VitalObject(DateUtils.getStringDate(date, "dd-MM-yyyy"), obs.getValueNumeric()));
							}
							
						}
					}
				}
				
			}
			for (ChildObject child : childrenData) {
				Map<String, Object> childMap = new LinkedHashMap<>();
				childMap.put("groupId", child.getGroupId());
				childMap.put("number", child.getNumber().toString());
				childMap.put("pulse", child.getPulses());
				childMap.put("temperature", child.getTemperatures());
				childMap.put("weight", child.getWeights());
				childMap.put("respiration", child.getRespirations());
				childrenList.add(childMap);
			}
		}
		
		simpleObject.put("children", childrenList);
		simpleObject.put("dateRanges", dateRanges);
		
		return simpleObject;
	}
	
	@Data
	static class ChildObject {
		
		private Integer groupId;
		
		private Integer number;
		
		private List<VitalObject> weights;
		
		private List<VitalObject> temperatures;
		
		private List<VitalObject> pulses;
		
		private List<VitalObject> respirations;
		
		public ChildObject(Integer groupId, Integer number) {
			this.groupId = groupId;
			this.number = number;
			this.weights = new ArrayList<>();
			this.temperatures = new ArrayList<>();
			this.pulses = new ArrayList<>();
			this.respirations = new ArrayList<>();
		}
	}
	
	static class VitalObject {
		
		public String yValue;
		
		public Double xValue;
		
		public VitalObject(String yValue, Double xValue) {
			this.yValue = yValue;
			this.xValue = xValue;
		}
	}
}
