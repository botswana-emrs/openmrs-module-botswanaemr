/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.Location;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NURSING_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_REGISTRATION_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.POST_REGISTRATION_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.QUEUING_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VMMC_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FEMALE_GENDER;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.createOrUpdatePatientQueue;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getPatientQueue;

public class QueuePatientFragmentController {
	
	protected final Log log = LogFactory.getLog(QueuePatientFragmentController.class);
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	public void controller(@SpringBean FragmentModel pageModel, @RequestParam("patientId") Patient patient,
	        @FragmentParam(value = "startVisit", required = false) String startVisit,
	        @FragmentParam(value = "returnUrl", required = false) String returnUrl,
	        @FragmentParam(value = "action", required = false) String action,
	        @FragmentParam(value = "isScreeningCompleted", defaultValue = "false") boolean isScreeningCompleted,
	        UiSessionContext uiSessionContext) {
		
		LocationService locationService = Context.getService(LocationService.class);
		
		String currentServicePointUuid = String
		        .valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		Location patientScreeningPortal = locationService.getLocationByUuid(NURSING_PORTAL_UUID);
		
		Location patientRegistrationPortal = locationService.getLocationByUuid(PATIENT_REGISTRATION_PORTAL_UUID);
		
		Location vmmcLocation = locationService.getLocationByUuid(VMMC_PORTAL_UUID);
		
		pageModel.addAttribute("patientId", patient.getPatientId());
		
		Location currentServicePoint = locationService.getLocationByUuid(currentServicePointUuid);
		
		List<Location> queueLocationsBeforeScreening = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(POST_REGISTRATION_LOCATION_TAG_NAME)).stream()
		        .filter(location -> !location.equals(currentServicePoint)).collect(Collectors.toList());
		
		List<Location> queueLocationsAfterScreening = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(QUEUING_LOCATION_TAG_NAME)).stream()
		        .collect(Collectors.toList());
		
		// Considering there are self/quick registrations, we want to allow queuing at current location e.g pharmacy or HTS
		
		List<Location> queueLocationsList = new ArrayList<>();
		
		if (action != null && (action.equals("quick") || action.equals("self"))) {
			queueLocationsList.add(locationService.getLocationByUuid(currentServicePoint.getUuid()));
		} else {
			if (currentServicePoint.equals(patientRegistrationPortal)) {
				if (isScreeningCompleted) {
					queueLocationsList = new ArrayList<>(queueLocationsAfterScreening);
				} else {
					queueLocationsList = new ArrayList<>(queueLocationsBeforeScreening);
				}
			} else {
				queueLocationsList = new ArrayList<>(queueLocationsAfterScreening);
			}
		}
		
		//if patient is female filter out VMMC in the locations
		if (patient.getGender().equalsIgnoreCase(FEMALE_GENDER)) {
			queueLocationsList = queueLocationsList.stream().filter(location -> !location.equals(vmmcLocation))
			        .collect(Collectors.toList());
		}
		
		List<String> patientQueueLocationsUuidList = new ArrayList<>();
		for (Location queueLocation : queueLocationsBeforeScreening) {
			PatientQueue patientQueue = getPatientQueue(patient, queueLocation);
			if (patientQueue != null) {
				patientQueueLocationsUuidList.add(patientQueue.getLocationTo().getUuid());
			}
		}
		if (!patientQueueLocationsUuidList.isEmpty()) {
			pageModel.addAttribute("patientQueueLocationsUuidList", patientQueueLocationsUuidList);
		} else {
			pageModel.addAttribute("patientQueueLocationsUuidList", Collections.EMPTY_LIST);
		}
		pageModel.addAttribute("queueLocationsList", queueLocationsList);
		pageModel.addAttribute("startVisit", startVisit);
		pageModel.addAttribute("returnUrl", returnUrl == null ? "" : returnUrl);
		pageModel.addAttribute("currentServicePoint", currentServicePointUuid);
		pageModel.addAttribute("patientRegistrationPortal", patientRegistrationPortal);
		pageModel.addAttribute("patientScreeningPortal", patientScreeningPortal);
	}
	
	public String post(@RequestParam(value = "patientId") Patient patient, UiUtils uiUtils,
	        @RequestParam(value = "queueRoom", required = false) Location queueRoom,
	        @RequestParam(value = "startVisit", required = false) String startVisit,
	        @RequestParam(value = "redirectUrl", required = false) String redirectUrl,
	        @RequestParam(value = "isBpScreening", defaultValue = "false") String isBpScreening,
	        @SpringBean("adtService") AdtService adtService, UiSessionContext sessionContext) throws IOException {
		
		Visit visit;
		Location visitLocation = sessionContext.getSessionLocation();
		boolean bpScreening = isBpScreening.equalsIgnoreCase("on");
		if (startVisit.equals("true")) {
			if (bpScreening) {
				VisitType visitType = Context.getVisitService()
				        .getVisitTypeByUuid(BotswanaEmrConstants.BP_SCREENING_VISIT_VISIT_TYPE_UUID);
				visit = new Visit(patient, visitType, new Date());
				visit.setLocation(visitLocation);
				visit = Context.getVisitService().saveVisit(visit);
			} else {
				visit = adtService.ensureActiveVisit(patient, visitLocation);
			}
		} else {
			visit = adtService.getActiveVisit(patient, visitLocation).getVisit();
		}
		
		if (bpScreening) {
			// update patient queue status to completed
			PatientQueue patientQueue = getPatientQueue(patient, queueRoom);
			if (patientQueue != null) {
				patientQueue.setStatus(PatientQueue.Status.COMPLETED);
				patientQueueingService.savePatientQue(patientQueue);
			}
		} else {
			
			PatientQueue savedQueue = createOrUpdatePatientQueue(patient, visitLocation, queueRoom, visit,
			    sessionContext.getCurrentUser(), patientQueueingService);
			
			log.info("Object to be saved is :" + savedQueue);
		}
		
		if (StringUtils.isNotBlank(redirectUrl)) {
			return "redirect:" + redirectUrl;
		}
		return "redirect:" + uiUtils.pageLink("botswanaemr", "registrationAdminDashboard");
	}
	
}
