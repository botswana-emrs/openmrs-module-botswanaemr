/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.utilities.PatientIdentifierUtil;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_GROUPING_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_PATIENT_NAME_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_PERSON_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_PHONE_NUMBER_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_P_NAME_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NATIONAL_ID_IDENTIFIER_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_AGE_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PHONE_NUMBER_ATTRIBUTE_TYPE_UUID;

@Slf4j
public class TBScreeningPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		List<Obs> groupingObs = findGroupingObs(session,
		    Context.getConceptService().getConceptByUuid(CONTACT_GROUPING_CONCEPT_UUID));
		
		if (!groupingObs.isEmpty()) {
			//Maintain a list of all the contact persons that have been added to the form
			//order is salient, so we can add them in the correct order
			List<String> names = new ArrayList<>();
			List<String> phoneNumbers = new ArrayList<>();
			List<String> patientIDs = new ArrayList<>();
			List<Double> patientAges = new ArrayList<>();
			for (Obs obsGroup : groupingObs) {
				for (Obs member : obsGroup.getGroupMembers()) {
					if (member.getConcept().getUuid().equals(CONTACT_P_NAME_CONCEPT_UUID)) {
						names.add(member.getValueText());
					} else if (member.getConcept().getUuid().equals(CONTACT_PHONE_NUMBER_CONCEPT_UUID)) {
						phoneNumbers.add(member.getValueText());
					} else if (member.getConcept().getUuid().equals(CONTACT_PATIENT_NAME_CONCEPT_UUID)) {
						patientIDs.add(member.getValueText());
					} else if (member.getConcept().getUuid().equals(PATIENT_AGE_CONCEPT_UUID)) {
						patientAges.add(member.getValueNumeric());
					}
				}
				
				// Comparing sizes for max size
				int maxSize = Math.max(Math.max(names.size(), phoneNumbers.size()),
				    Math.max(patientIDs.size(), patientAges.size()));
				
				// Filling missing entries for sizes to be equal and support easy array insertion
				while (names.size() < maxSize)
					names.add(null);
				while (phoneNumbers.size() < maxSize)
					phoneNumbers.add(null);
				while (patientIDs.size() < maxSize)
					patientIDs.add(null);
				while (patientAges.size() < maxSize)
					patientAges.add(null);
			}
			
			List<ContactPerson> contactPersons = new ArrayList<>();
			for (int i = 0; i < names.size(); i++) {
				ContactPerson contactPerson = new ContactPerson();
				if (names.get(i) != null) {
					contactPerson.setName(names.get(i));
				}
				
				if (phoneNumbers.get(i) != null) {
					contactPerson.setPhoneNumber(phoneNumbers.get(i));
				}
				
				if (patientIDs.get(i) != null) {
					contactPerson.setPatientID(patientIDs.get(i));
				}
				
				if (patientAges.get(i) != null) {
					contactPerson.setPatientAge(patientAges.get(i));
				}
				
				contactPersons.add(contactPerson);
			}
			
			//register patients
			for (ContactPerson contactPerson : contactPersons) {
				if (contactPerson != null && contactPerson.getName() != null && contactPerson.getPatientAge() != null) {
					this.registerPatientFromContactPerson(contactPerson, session.getPatient());
				}
			}
		}
	}
	
	protected void registerPatientFromContactPerson(@NotNull ContactPerson contactPerson, @NotNull Patient patient) {
		log.info("Registering patient {} with ID {}", contactPerson.getName(), contactPerson.getPatientID());
		Patient newlyPatient = new Patient();
		newlyPatient.setGender("Unknown");
		newlyPatient.setBirthdateFromAge(contactPerson.getPatientAge().intValue(), null);
		newlyPatient.setBirthdateEstimated(true);
		
		//set patient names
		this.addPatientNames(newlyPatient, contactPerson);
		
		//set person attributes
		this.addPersonAttributes(newlyPatient, patient, contactPerson);
		
		//set patient identifiers
		newlyPatient.addIdentifier(createOpenmrsIDPatientIdentifier());
		if (contactPerson.getPatientID() != null) {
			newlyPatient.addIdentifier(createNationalIDPatientIdentifier(contactPerson));
		}
		
		// persist patient
		Patient registeredPatient = Context.getPatientService().savePatient(newlyPatient);
		log.info("Successfully registered patient {} with ID {}", contactPerson.getName(), registeredPatient.getPatientId());
		if (registeredPatient == null) {
			// Failed to register the patient
			throw new APIException("Failed to register " + newlyPatient.getPersonName().getFullName());
		}
	}
	
	/**
	 * Create a OpenMRS ID patient identifier
	 *
	 * @return PatientIdentifier
	 */
	protected PatientIdentifier createOpenmrsIDPatientIdentifier() {
		//Automatically generate a OpenMRS ID for the patient
		return PatientIdentifierUtil.validateOrGenerateIdentifier("", null);
	}
	
	/**
	 * Create a patient identifier for the contact person
	 * 
	 * @param contactPerson the contact person
	 * @return PatientIdentifier
	 */
	protected PatientIdentifier createNationalIDPatientIdentifier(@NotNull ContactPerson contactPerson) {
		PatientIdentifier nationalIDIdentifier = new PatientIdentifier();
		if (contactPerson.getPatientID() != null) {
			nationalIDIdentifier.setIdentifier(contactPerson.getPatientID());
			nationalIDIdentifier.setPreferred(true);
			//set national identifier type
			nationalIDIdentifier.setIdentifierType(
			    Context.getPatientService().getPatientIdentifierTypeByUuid(NATIONAL_ID_IDENTIFIER_TYPE));
		}
		return nationalIDIdentifier;
	}
	
	/**
	 * Create person name from contact person
	 *
	 * @param contactPerson contact person
	 * @return a set of person names
	 */
	private void addPatientNames(@NotNull Patient newPatient, @NotNull ContactPerson contactPerson) {
		String[] nameArr = contactPerson.getName().split(" ");
		if (nameArr.length == 1) {
			newPatient.addName(new PersonName(contactPerson.getName(), null, null));
		} else if (nameArr.length == 2) {
			newPatient.addName(new PersonName(nameArr[0], null, nameArr[1]));
		} else if (nameArr.length == 3) {
			newPatient.addName(new PersonName(nameArr[0], nameArr[1], nameArr[2]));
		}
	}
	
	/**
	 * Creates person attributes for the given patient.
	 *
	 * @param patient the patient to create attributes for.
	 * @param newlyPatient new patient to register
	 * @param contactPerson the contact person
	 */
	protected void addPersonAttributes(Patient newlyPatient, Patient patient, ContactPerson contactPerson) {
		//set contact person attribute
		newlyPatient.addAttribute(new PersonAttribute(this.getContactPersonType(), patient.getUuid()));
		
		// set telephone number person attribute
		if (contactPerson.getPhoneNumber() != null) {
			newlyPatient.addAttribute(new PersonAttribute(this.getTelephoneAttributeType(), contactPerson.getPhoneNumber()));
		}
	}
	
	/**
	 * Finds the obs group that contains the given concept
	 *
	 * @param session the encounter to search in
	 * @param groupingConcept the concept to search for
	 * @return Obs
	 */
	protected List<Obs> findGroupingObs(FormEntrySession session, Concept groupingConcept) {
		List<Obs> groupingObs = new ArrayList<>();
		for (Obs obs : session.getEncounter().getObsAtTopLevel(false)) {
			if (obs.isObsGrouping() && obs.hasGroupMembers(false)
			        && obs.getConcept().getUuid().equals(groupingConcept.getUuid())) {
				groupingObs.add(obs);
			}
		}
		return groupingObs;
	}
	
	/**
	 * Gets person attribute type by uuid
	 *
	 * @return PersonAttributeType
	 */
	protected PersonAttributeType getContactPersonType() {
		return Context.getPersonService().getPersonAttributeTypeByUuid(CONTACT_PERSON_ATTRIBUTE_TYPE_UUID);
	}
	
	/**
	 * Gets telephone attribute type by uuid
	 *
	 * @return PersonAttributeType
	 */
	protected PersonAttributeType getTelephoneAttributeType() {
		return Context.getPersonService().getPersonAttributeTypeByUuid(PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
	}
	
	@Data
	@NoArgsConstructor
	static class ContactPerson {
		
		private String name;
		
		private String phoneNumber;
		
		private Double patientAge;
		
		private String patientID;
		
		public ContactPerson(String name, String phoneNumber, String patientID) {
			this.name = name;
			this.phoneNumber = phoneNumber;
			this.patientID = patientID;
		}
		
		public String getName() {
			return name;
		}
		
		public String getPhoneNumber() {
			return phoneNumber;
		}
		
		public String getPatientID() {
			return patientID;
		}
		
		public Double getPatientAge() {
			return patientAge;
		}
	}
}
