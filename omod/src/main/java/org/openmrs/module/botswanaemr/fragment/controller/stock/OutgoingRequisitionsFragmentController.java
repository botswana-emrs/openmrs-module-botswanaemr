/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class OutgoingRequisitionsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getInternalRequisition());
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		stockOperationsSearch.setTemplate(stockOperationTemplate);
		List<StockOperation> stockOperations = new ArrayList<>();
		// Get all bulk stockrooms
		List<Stockroom> stockrooms = StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation());
		// Fetch operations for each stockroom Add to the Stock operations list
		for (Stockroom stockroom : stockrooms) {
			stockOperationTemplate.setSource(stockroom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			stockOperations.addAll(
			    BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch, null));
		}
		// 
		fragmentModel.addAttribute("operations", stockOperations);
	}
	
	public SimpleObject saveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		StockOperation stockOperation = null;
		if (stockOperationUuid != "") {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.CANCELLED); // Cancel old requisition
			stockOperation.setDescription("Cancelled for Updating");
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			
		}
		StockOperation updatedStockOperation = new StockOperation();
		if (stockOperation != null) {
			updatedStockOperation.setStockOperationParent(stockOperation);
		}
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					updatedStockOperation.addItem(item, qnty);
				}
			}
		}
		
		if (requisitionType.equals("external")) {
			updatedStockOperation.setName(WellKnownOperationTypes.getExternalRequisition().getName());
			
		} else {
			updatedStockOperation.setName(WellKnownOperationTypes.getInternalRequisition().getName());
			
		}
		
		updatedStockOperation.setDescription(description);
		SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
		Date date = DateFor.parse(requisitionDate);
		updatedStockOperation.setOperationDate(date);
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		updatedStockOperation.setDestination(destStockRoom);
		updatedStockOperation.setSource(sourceStockRoom);
		if (requisitionType.equals("external")) {
			updatedStockOperation.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		} else {
			updatedStockOperation.setInstanceType(WellKnownOperationTypes.getInternalRequisition());
			
		}
		updatedStockOperation.setOperationNumber(UUID.randomUUID().toString());
		updatedStockOperation.setStatus(StockOperationStatus.NEW); // Create updated requisition
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(updatedStockOperation);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
		
	}
	
	public SimpleObject approveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		SimpleObject simpleObject = new SimpleObject();
		
		StockOperation stockOperation = null;
		if (!stockOperationUuid.equals("")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.REQUESTED); // Cancel old requisition
			//stockOperation.setDescription("Cancelled for Approval");
			
			// StockOperation updatedStockOperation = new StockOperation();
			if (stockOperation != null) {
				// updatedStockOperation.setStockOperationParent(stockOperation);
			}
			//JSONObject jsonObject = new JSONObject(itemId);
			//Iterator<String> keys = jsonObject.keys();
			//while (keys.hasNext()) {
			//	String key = keys.next();
			//	if (jsonObject.get(key) instanceof JSONObject) {
			//		if (jsonObject.get(key) instanceof JSONObject) {
			//			Item item = BotswanaInventoryContext.getItemDataService()
			//					.getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
			//			int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
			//			updatedStockOperation.addItem(item, qnty);
			//		}
			//	}
			//}
			//if (requisitionType.equals("external")) {
			//updatedStockOperation.setName(WellKnownOperationTypes.getExternalRequisition().getName());
			//updatedStockOperation.setDescription(description);
			
			//} else {
			//	updatedStockOperation.setName(WellKnownOperationTypes.getInternalRequisition().getName());
			//	updatedStockOperation.setDescription("Approved Internal Transfer");
			
			//}
			//updatedStockOperation.setName(WellKnownOperationTypes.getRequisition().getName());
			//SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd");
			//Date date = DateFor.parse(requisitionDate);
			//updatedStockOperation.setOperationDate(date);
			//Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
			//Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
			//updatedStockOperation.setDestination(destStockRoom);
			//updatedStockOperation.setSource(sourceStockRoom);
			//if (requisitionType.equals("external")) {
			//	updatedStockOperation.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
			//} else {
			//	updatedStockOperation.setInstanceType(WellKnownOperationTypes.getInternalRequisition());
			
			//}
			//updatedStockOperation.setOperationNumber(UUID.randomUUID().toString());
			//updatedStockOperation.setStatus(StockOperationStatus.NEW); // Create updated requisition
			//StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
			//		.submitOperation(updatedStockOperation);
			//returnedStockOperation.setStatus(StockOperationStatus.REQUESTED); // Approve requisition
			//returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
			//		.submitOperation(updatedStockOperation);
			
			//StockOperation approvedStockOperation = new StockOperation();
			//if (requisitionType.equals("external")) {
			//	approvedStockOperation.setName(WellKnownOperationTypes.getExternalRequisition().getName());
			//	approvedStockOperation.setDescription(returnedStockOperation.getDescription());
			//	approvedStockOperation.setOperationDate(returnedStockOperation.getOperationDate());
			//	approvedStockOperation.setDestination(returnedStockOperation.getDestination());
			//	approvedStockOperation.setSource(returnedStockOperation.getSource());
			//	if (requisitionType.equals("external")) {
			//		approvedStockOperation.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
			//	} else {
			//		approvedStockOperation.setInstanceType(WellKnownOperationTypes.getInternalRequisition());
			
			//	}
			//approvedStockOperation.setInstanceType(WellKnownOperationTypes.getRequisition());
			//	approvedStockOperation.setOperationNumber(UUID.randomUUID().toString());
			//	approvedStockOperation.setStatus(StockOperationStatus.NEW); // Create a copy of the approved requisition
			//	approvedStockOperation.setStockOperationParent(returnedStockOperation);
			
			//	for (StockOperationItem stockOperationItem : returnedStockOperation.getItems()) {
			//		approvedStockOperation.addItem(stockOperationItem.getItem(), stockOperationItem.getQuantity());
			//	}
			
			// Submit original stock transaction
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			
			//	StockOperation returnedApprovedStockOperation = BotswanaInventoryContext.getStockOperationService()
			//			.submitOperation(approvedStockOperation);
			//	simpleObject.put("operationStatus", returnedApprovedStockOperation.getStatus());
			
			//} else {
			//	approvedStockOperation.setName(WellKnownOperationTypes.getInternalRequisition().getName());
			//	simpleObject.put("operationStatus", returnedStockOperation.getStatus());
			//}
		} else {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		return simpleObject;
		
	}
}
