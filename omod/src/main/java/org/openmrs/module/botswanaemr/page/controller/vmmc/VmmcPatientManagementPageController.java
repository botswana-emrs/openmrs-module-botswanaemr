/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.vmmc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.DailyRegisteredListFragmentController;
import org.openmrs.module.botswanaemr.model.AgeCategory;
import org.openmrs.ui.framework.page.PageModel;

public class VmmcPatientManagementPageController extends DailyRegisteredListFragmentController {
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext) {
		
		List<AgeCategory> ageCategoryList = new ArrayList<>();
		ageCategoryList.add(new AgeCategory("<1", "<1 Year", 0, 0.9));
		ageCategoryList.add(new AgeCategory("1-9", "1-9 Years", 1, 9));
		ageCategoryList.add(new AgeCategory("10-14", "10-14 Years", 10, 14));
		ageCategoryList.add(new AgeCategory("15-19", "15-19 Years", 15, 19));
		ageCategoryList.add(new AgeCategory("20-24", "20-24 Years", 20, 24));
		ageCategoryList.add(new AgeCategory("25-29", "25-29 Years", 25, 29));
		ageCategoryList.add(new AgeCategory("30-39", "30-39 Years", 30, 39));
		ageCategoryList.add(new AgeCategory("40-49", "40-49 Years", 40, 49));
		ageCategoryList.add(new AgeCategory("50=>", "50+ Years", 50, 200));
		
		pageModel.addAttribute("ageCategories", ageCategoryList);
		
		List<String> filterVisitStatus = Arrays.asList("Counselled", "Booked", "Defaulted", "Circumcised", "48Hrs review",
		    "7Days Review", "42Days Review", "Enrolled", "Declined");
		
		pageModel.addAttribute("filterVisitStatus", filterVisitStatus);
		
	}
	
}
