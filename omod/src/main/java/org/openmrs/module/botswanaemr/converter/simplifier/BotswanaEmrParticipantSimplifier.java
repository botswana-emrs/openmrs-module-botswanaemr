/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import org.openmrs.User;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Converts a {@link User} to a {@link SimpleObject}.
 */
@Component
public class BotswanaEmrParticipantSimplifier extends BotswanaEmrAbstractSimplifier<User> {
	
	private final List<String> loggedInUsers;
	
	public BotswanaEmrParticipantSimplifier(List<String> loggedInUsers) {
		this.loggedInUsers = loggedInUsers;
	}
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	@Override
	protected SimpleObject simplify(User user) {
		SimpleObject ret = new SimpleObject();
		String name = BotswanaEmrUtils.formatPersonName(user.getPersonName());
		ret.put("value", user.getUserId());
		ret.put("userId", user.getUserId());
		ret.put("name", name);
		if (loggedInUsers != null && loggedInUsers.contains(user.getUsername())) {
			ret.put("online", true);
			ret.put("label", name + " - online");
		} else {
			ret.put("online", false);
			ret.put("label", name + " - offline");
		}
		return ret;
	}
}
