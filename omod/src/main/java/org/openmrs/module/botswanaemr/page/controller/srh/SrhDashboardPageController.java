/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SrhDashboardPageController extends QueuePatientFragmentController {
	
	public void get(PageModel model, UiUtils ui, UiSessionContext uiSessionContext) {
		model.addAttribute("patientPoolReturnUrl", ui.pageLink("botswanaemr", "srh/srhDashboard"));
		
		int allEnrolledCount = 0;
		int todayEnrolledCount = 0;
		int yesterdayEnrolledCount = 0;
		int dailyAverageEnrollments = 0;
		
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		Location location = Context.getLocationService()
		        .getLocationByUuid(BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
		Location currentLocation = uiSessionContext.getSessionLocation();
		
		List<PatientQueue> patientQueueList = new ArrayList<>();
		patientQueueList = patientQueueingService.getPatientQueueList(null, null, null, location, currentLocation, null,
		    null);
		
		allEnrolledCount = patientQueueList.size();
		model.addAttribute("allEnrolledCount", allEnrolledCount);
		
		patientQueueList = patientQueueingService.getPatientQueueList(null,
		    fromDate(new DateTime().withDayOfWeek(DateTimeConstants.MONDAY).toDate()),
		    toDate(new DateTime().withDayOfWeek(DateTimeConstants.SUNDAY).toDate()), location, currentLocation, null, null);
		int weekCount = patientQueueList.size();
		if (weekCount > 0) {
			dailyAverageEnrollments = weekCount / 7;
		}
		model.addAttribute("dailyAverageEnrollments", dailyAverageEnrollments);
		
		patientQueueList = patientQueueingService.getPatientQueueList(null, fromDate(new Date()), toDate(new Date()),
		    location, currentLocation, null, null);
		todayEnrolledCount = patientQueueList.size();
		model.addAttribute("todayEnrolledCount", todayEnrolledCount);
		
		patientQueueList = patientQueueingService.getPatientQueueList(null, fromDate(new DateTime().minusDays(1).toDate()),
		    toDate(new DateTime().minusDays(1).toDate()), location, currentLocation, null, null);
		yesterdayEnrolledCount = patientQueueList.size();
		model.addAttribute("yesterdayEnrolledCount", yesterdayEnrolledCount);
	}
	
	private Date fromDate(Date startDate) {
		return new DateTime(startDate).withTimeAtStartOfDay().toDate();
	}
	
	private Date toDate(Date endDate) {
		return new DateTime(endDate).withTime(23, 59, 59, 999).toDate();
	}
}
