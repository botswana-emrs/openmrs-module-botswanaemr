/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.hts;

import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HtsPastTestsFragmentController {
	
	public void controller(FragmentModel fragmentModel, @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("formService") FormService formService, @RequestParam("patientId") Patient patient,
	        UiSessionContext uiSessionContext) {
		Form htsForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID);
		Form htsVerificationForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID);
		Form priorTests = formService.getFormByUuid(BotswanaEmrConstants.HTS_DOCUMENT_PRIOR_TEST_FORM_UUID);
		Form supplementaryTestResults = formService.getFormByUuid(BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID);
		
		List<Encounter> htsTestingEncounters = encounterService.getEncounters(new EncounterSearchCriteria(patient, null,
		        null, null, null, Arrays.asList(htsForm, htsVerificationForm, priorTests, supplementaryTestResults), null,
		        null, null, null, false));
		
		List<Encounter> filteredEncounters = htsTestingEncounters.stream()
		        .filter(encounter -> encounter.getObs().stream().noneMatch(obs -> {
			        if (obs.getValueCoded() != null) {
				        String valueCodedUuid = obs.getValueCoded().getUuid();
				        return valueCodedUuid.equals(BotswanaEmrConstants.SELF_TESTING_POSITIVE_CONCEPT_UUID)
				                || valueCodedUuid.equals(BotswanaEmrConstants.SELF_TESTING_NEGATIVE_CONCEPT_UUID);
			        }
			        return false;
		        })).collect(Collectors.toList());
		
		fragmentModel.addAttribute("newDiagnosticEncounterType", BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID);
		fragmentModel.addAttribute("verificationEncounterType", BotswanaEmrConstants.HTS_VERIFICATION_ENCOUNTER_TYPE_UUID);
		fragmentModel.addAttribute("supplementaryTestEncounterType",
		    BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_ENCOUNTER_TYPE_UUID);
		fragmentModel.addAttribute("priorTestOutcomeConceptUuid",
		    BotswanaEmrConstants.PREVIOUSLY_TESTED_QUESTION_CONCEPT_UUID);
		
		fragmentModel.addAttribute("planConceptUuid", "2a95db8c-d96a-40dd-9c96-3c066c4ed9b7");
		fragmentModel.addAttribute("reasonForTestConceptUuid", "164126AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		fragmentModel.addAttribute("acceptedConceptUuid", "c13f95c1-484f-4594-aa41-bd800aa605ce");
		fragmentModel.addAttribute("resultsConceptUuid", "1169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		fragmentModel.addAttribute("typeConceptUuid", "159936AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		fragmentModel.addAttribute("verificationOutcomeConceptUuid", "4eb02164-0054-4f27-822e-3c8fdd1007fe");
		fragmentModel.addAttribute("supplementaryTestOutcomeConceptUuid", "9eb8e860-d137-405e-b067-01505b853834");
		
		fragmentModel.addAttribute("htsTestingEncounters", filteredEncounters);
		
	}
}
