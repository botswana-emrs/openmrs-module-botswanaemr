/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class HivStatusBarFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @RequestParam(required = false, value = "visitId") Visit visit, @SpringBean("obsService") ObsService obsService,
	        @SpringBean("conceptService") ConceptService conceptService, UiSessionContext sessionContext) {
		
		Obs verificationhivStatusObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID, null, null);
		Obs hivStatusObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.HIV_STATUS_RESULT_CONCEPT_UUID, null, null);
		Obs pitcAcceptedObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.CERVICAL_PITC_CONCEPT_UUID, null, null);
		
		// consider verificationhivObs if present instead of diagnostic hiv obs
		if (verificationhivStatusObs != null) {
			hivStatusObs = verificationhivStatusObs;
		}
		
		model.addAttribute("hivStatusObs", hivStatusObs);
		model.addAttribute("pitcAcceptedObs", pitcAcceptedObs);
		
		String hivStatus = "Unknown";
		if (hivStatusObs != null) {
			if (hivStatusObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.POSITIVE)) {
				hivStatus = "Positive";
			} else if (hivStatusObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.NEGATIVE)) {
				hivStatus = "Negative";
			}
		}
		
		Visit currentPatientVisit = visit != null ? visit
		        : BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		
		EncounterType screeningEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.HIV_STATUS_ENCOUNTER_TYPE_UUID);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient,
		        sessionContext.getSessionLocation(), null, null, null, null,
		        Collections.singletonList(screeningEncounterType), null, null, null, false);
		List<Encounter> encounters = BotswanaEmrUtils
		        .sortEncountersByEncounterDatetime(Context.getEncounterService().getEncounters(encounterSearchCriteria));
		SimpleObject simpleObject = new SimpleObject();
		if (!encounters.isEmpty() && !encounters.get(0).getObs().isEmpty()) {
			simpleObject = getHivStatusObs(encounters.get(0));
		}
		
		model.addAttribute("visit", currentPatientVisit);
		model.addAttribute("hivStatus", hivStatus);
		model.addAttribute("hivStatusObs", simpleObject);
		model.addAttribute("hivStatusObsObjs", simpleObject.toJson());
		model.addAttribute("lastHivStatusEncounter", !encounters.isEmpty() ? encounters.get(0) : null);
		
		model.addAttribute("positiveUuid", BotswanaEmrConstants.POSITIVE);
		model.addAttribute("negativeUuid", BotswanaEmrConstants.NEGATIVE);
		model.addAttribute("unknownUuid", BotswanaEmrConstants.UNKNOWN_CONCEPT_UUID);
		model.addAttribute("yesUuid", BotswanaEmrConstants.YES_CONCEPT_UUID);
		model.addAttribute("noUuid", BotswanaEmrConstants.NO_CONCEPT_UUID);
		
		model.addAttribute("onArt", BotswanaEmrUtils.onArt(patient));
	}
	
	private SimpleObject getHivStatusObs(Encounter encounter) {
		SimpleObject simpleObject = new SimpleObject();
		
		for (Obs obs : encounter.getAllObs(false)) {
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_DATE_NEGATIVE_CONCEPT_UUID)) {
				simpleObject.put("negativeResultDate", SimpleObject.create("val", obs.getValueDate(), "id", obs.getObsId(),
				    "uuid", obs.getConcept().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_CONCEPT_UUID)) {
				simpleObject.put("pitcAccepted", SimpleObject.create("val", obs.getValueCoded().getName().getName(), "id",
				    obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_RESULT_CONCEPT_UUID)) {
				simpleObject.put("pitcResults", SimpleObject.create("val", obs.getValueCoded().getName().getName(), "id",
				    obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_PITC_DATE_CONCEPT_UUID)) {
				simpleObject.put("unknownResultDate", SimpleObject.create("val", obs.getValueDate(), "id", obs.getObsId(),
				    "uuid", obs.getConcept().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_ON_ART_CONCEPT_UUID)) {
				simpleObject.put("onArt", SimpleObject.create("val", obs.getValueCoded().getName().getName(), "id",
				    obs.getObsId(), "uuid", obs.getValueCoded().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_ART_SITE_CONCEPT_UUID)) {
				simpleObject.put("artSite", SimpleObject.create("val", obs.getValueText(), "id", obs.getObsId(), "uuid",
				    obs.getConcept().getUuid()));
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.CERVICAL_ART_REFERRED_TO_CONCEPT_UUID)) {
				simpleObject.put("referredTo", SimpleObject.create("val", obs.getValueText(), "id", obs.getObsId(), "uuid",
				    obs.getConcept().getUuid()));
			}
		}
		
		return simpleObject;
	}
	
	public void saveHivStatus(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException, ParseException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		String encounterUuid = jsonNode.get("lastHivStatusEncounter").asText();
		Encounter encounter = Context.getEncounterService().getEncounterByUuid(encounterUuid);
		if (encounter == null || !Objects.equals(encounter.getVisit().getVisitId(), visit.getVisitId())) {
			EncounterType encounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.HIV_STATUS_ENCOUNTER_TYPE_UUID);
			encounter = BotswanaEmrUtils.createEncounter(patient, encounterType, sessionContext.getSessionLocation(), visit);
		} else {
			//TODO: Delete cleared obs
			encounter.getObs().clear();
		}
		
		String negativeResultDate = jsonNode.has("negativeResultDate")
		        ? jsonNode.get("negativeResultDate").get("val").asText()
		        : "";
		String pitcAccepted = jsonNode.has("pitcAccepted") ? jsonNode.get("pitcAccepted").get("val").asText() : "";
		String pitcResults = jsonNode.has("pitcResults") ? jsonNode.get("pitcResults").get("val").asText() : "";
		String unknownResultDate = jsonNode.has("unknownResultDate") ? jsonNode.get("unknownResultDate").get("val").asText()
		        : "";
		String onArt = jsonNode.has("onArt") ? jsonNode.get("onArt").get("val").asText() : "";
		String artSite = jsonNode.has("artSite") ? jsonNode.get("artSite").get("val").asText() : "";
		String referredTo = jsonNode.has("referredTo") ? jsonNode.get("referredTo").get("val").asText() : "";
		
		ObsService obsService = Context.getObsService();
		if (StringUtils.isNotEmpty(negativeResultDate)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("negativeResultDate").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("negativeResultDate").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_DATE_NEGATIVE_CONCEPT_UUID));
			obs.setValueDatetime(BotswanaEmrUtils.formatDateFromString(negativeResultDate, "YYYY-MM-dd"));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(pitcAccepted)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("pitcAccepted").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("pitcAccepted").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_PITC_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(pitcAccepted));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(pitcResults)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("pitcResults").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("pitcResults").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_PITC_RESULT_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(pitcResults));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(unknownResultDate)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("unknownResultDate").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("unknownResultDate").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_PITC_DATE_CONCEPT_UUID));
			obs.setValueDatetime(BotswanaEmrUtils.formatDateFromString(unknownResultDate, "YYYY-MM-dd"));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(onArt)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("onArt").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("onArt").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_ON_ART_CONCEPT_UUID));
			obs.setValueCoded(Context.getConceptService().getConceptByUuid(onArt));
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(artSite)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("artSite").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("artSite").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_ART_SITE_CONCEPT_UUID));
			obs.setValueText(artSite);
			encounter.addObs(obs);
		}
		if (StringUtils.isNotEmpty(referredTo)) {
			Obs obs = StringUtils.isNotEmpty(jsonNode.get("referredTo").get("id").asText())
			        ? obsService.getObs(Integer.valueOf(jsonNode.get("referredTo").get("id").asText()))
			        : BotswanaEmrUtils.creatObs(encounter,
			            BotswanaEmrUtils.getConcept(BotswanaEmrConstants.CERVICAL_ART_REFERRED_TO_CONCEPT_UUID));
			obs.setValueText(referredTo);
			encounter.addObs(obs);
		}
		if (encounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(encounter);
		}
		
	}
}
