package org.openmrs.module.botswanaemr.htmlformentry;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntrySession;

import java.util.Set;

public class ChangeTBRegimenPostSubmissionAction implements CustomFormSubmissionAction {
	@Override
	public void applyAction(FormEntrySession session) {
		Patient patient = session.getPatient();
		PersonAttributeType personAttributeType = Context.getPersonService()
				.getPersonAttributeTypeByUuid(BotswanaEmrConstants.TB_PATIENT_REGIMEN_UUID);
		if (personAttributeType != null) {
			PersonAttribute personAttribute = patient.getPerson().getAttribute(personAttributeType);
			if (personAttribute == null) {
				personAttribute = new PersonAttribute();
				personAttribute.setAttributeType(personAttributeType);
			}
			String currentRegimen = "";
			Set<Obs> obsList = session.getEncounter().getObs();
			Obs obs = obsList.stream()
					.filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.REGIMEN_CONCEPT_UUID)).findFirst()
					.orElse(null);
			if (obs != null) {
				currentRegimen = obs.getValueCoded().getUuid();
			}
			personAttribute.setValue(currentRegimen);
			patient.addAttribute(personAttribute);
			Context.getPatientService().savePatient(patient);
		}
		
	}
	
}
