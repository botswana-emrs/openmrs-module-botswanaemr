/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LifeStyleFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "visit", required = false) Visit visit) {
		fragmentModel.addAttribute("visit", visit);
		
	}
	
	public List<SimpleObject> getLifeStylesData(@RequestParam(value = "patientId") String patientId,
	        @RequestParam(value = "visitId") String visitId, @SpringBean("patientService") PatientService patientService,
	        @SpringBean("visitService") VisitService visitService, UiUtils ui, UiSessionContext uiSessionContext) {
		Patient patient = patientService.getPatientByUuid(patientId);
		Visit visit = visitService.getVisitByUuid(visitId);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		EncounterService encounterService = Context.getEncounterService();
		List<Obs> dietGroupingObs = botswanaEmrService.getObservation(patient,
		    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.LIFESTYLE_GROUPING_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		
		Set<Obs> groupingObs = new HashSet<>(dietGroupingObs);
		List<SimpleObject> lifestyleObjects = BotswanaEmrUtils.getSimplifiedLifeStyleObjects(groupingObs);
		return getSimplifiedLifeStyleObjects(lifestyleObjects);
	}
	
	private List<SimpleObject> getSimplifiedLifeStyleObjects(List<SimpleObject> lifestyleObjects) {
		List<SimpleObject> simplifiedObjects = new ArrayList<>();
		
		for (SimpleObject lifestyleObject : lifestyleObjects) {
			SimpleObject simplifiedObject = new SimpleObject();
			if (lifestyleObject.containsKey("lifestyle")) {
				Object lifestyle = lifestyleObject.get("lifestyle");
				if (lifestyle instanceof Obs) {
					Obs lifestyleObs = (Obs) lifestyle;
					SimpleObject lifestyleSimplified = new SimpleObject();
					lifestyleSimplified.put("valueText", lifestyleObs.getValueText());
					simplifiedObject.put("lifestyle", lifestyleSimplified);
				}
			}
			
			if (lifestyleObject.containsKey("comment")) {
				Object comment = lifestyleObject.get("comment");
				if (comment instanceof Obs) {
					Obs commentObs = (Obs) comment;
					SimpleObject commentSimplified = new SimpleObject();
					commentSimplified.put("valueText", commentObs.getValueText());
					simplifiedObject.put("comment", commentSimplified);
				}
			}
			simplifiedObjects.add(simplifiedObject);
		}
		return simplifiedObjects;
	}
}
