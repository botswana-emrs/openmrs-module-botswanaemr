/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.pharmacy;

import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class PrintPrescriptionLabelsPageController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam(value = "orderId", required = false) String orderId, PageModel pageModel) {
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("encounter", encounter);
		pageModel.addAttribute("visit", visit);
		pageModel.addAttribute("orderId", orderId);
	}
}
