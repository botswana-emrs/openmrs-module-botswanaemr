/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ViewStockItemsFragmentController {
	
	public void controller(@RequestParam(value = "stockRoomId") Integer stockRoomId, FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null) {
			Stockroom stockroom = iStockroomDataService.getById(stockRoomId);
			
			fragmentModel.addAttribute("stockRoomId", stockroom.getId());
			fragmentModel.addAttribute("stockRoomName", stockroom.getName());
		} else {
			fragmentModel.addAttribute("stockRoomId", "");
			fragmentModel.addAttribute("stockRoomName", "");
		}
	}
	
	public List<SimpleObject> getItemStockData(@RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "q", required = false, defaultValue = "") String searchPhrase,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) {
		
		Stockroom stockRoom = iStockroomDataService.getById(stockRoomUuid);
		List<ItemStock> iStockList;
		if (searchPhrase.trim().length() > 0) {
			ItemSearch search = new ItemSearch(new Item());
			search.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.LIKE);
			search.getTemplate().setName("%" + searchPhrase + "%");
			iStockList = iStockroomDataService.getItems(stockRoom, search, null);
		} else {
			iStockList = iStockroomDataService.getItemsByRoom(stockRoom, null);
		}
		
		List<SimpleObject> itemStockList = new ArrayList<>();
		for (ItemStock itemStock : iStockList) {
			List<ItemStockDetail> itemStockDetails = itemStock.getDetails().stream()
			        .filter(i -> !i.isNullBatch() && i.getExpiration() != null && i.getQuantity() > 0)
			        .collect(Collectors.toList());
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				SimpleObject simpleObject = StockUtils.extractItemStockToSimpleObject(itemStock, itemStockDetail);
				itemStockList.add(simpleObject);
			}
		}
		
		return itemStockList;
	}
}
