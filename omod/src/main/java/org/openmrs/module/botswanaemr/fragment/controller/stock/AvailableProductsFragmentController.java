/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import liquibase.util.StringUtil;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.InventoryItemSimplifier;
import org.openmrs.module.botswanaemr.model.InventoryItemStockSimplifier;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

public class AvailableProductsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		
		List<InventoryItemSimplifier> simplifiedItems = new ArrayList<>();
		if (uiSessionContext.getCurrentUser() != null) {
			List<Item> items = iItemDataService.getAll(false, new PagingInfo(2, 10));
			
			for (Item item : items) {
				InventoryItemSimplifier inventoryItemSimplifier = InventoryItemSimplifier.simplify(item);
				simplifiedItems.add(inventoryItemSimplifier);
			}
		}
		List<Stockroom> stockrooms = new ArrayList<>();
		if (uiSessionContext.getSessionLocation() != null) {
			stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		}
		
		fragmentModel.addAttribute("inventoryItems", simplifiedItems);
		fragmentModel.addAttribute("stockrooms", stockrooms);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public SimpleObject getAvailableProducts(@RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "search[value]", required = false) String searchValue,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService,
	        @RequestParam(value = "stockRoomId") Integer stockRoomId, UiSessionContext uiSessionContext) {
		
		page = page / pageSize + 1;
		List<ItemStock> itemStocks;
		
		if (StringUtil.isEmpty(searchValue)) {
			if (stockRoomId == 0) {
				List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
				        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
				itemStocks = BotswanaInventoryContext.getItemStockDataService().getItemStockByStockRooms(stockrooms,
				    new PagingInfo(page, pageSize));
			} else {
				Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
				itemStocks = BotswanaInventoryContext.getStockRoomDataService().getItemsByRoom(stockRoom,
				    new PagingInfo(page, pageSize));
			}
		} else {
			if (stockRoomId == 0) {
				
				List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
				        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
				
				itemStocks = BotswanaInventoryContext.getItemStockDataService()
				        .getItemStockByStockRooms(stockrooms, new PagingInfo(page, pageSize)).stream()
				        .filter(is -> is.getItem().getName().contains(searchValue)).collect(Collectors.toList());
				
			} else {
				Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
				
				itemStocks = BotswanaInventoryContext.getStockRoomDataService()
				        .getItemsByRoom(stockRoom, new PagingInfo(page, pageSize)).stream()
				        .filter(is -> is.getItem().getName().contains(searchValue)).collect(Collectors.toList());
			}
		}
		
		List<InventoryItemStockSimplifier> simplifiedItemStocks = new ArrayList<>();
		for (ItemStock iStock : itemStocks) {
			InventoryItemStockSimplifier inventoryItemStockSimplifier = InventoryItemStockSimplifier.simplify(iStock);
			simplifiedItemStocks.add(inventoryItemStockSimplifier);
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("draw", draw);
		int totalDisplayRecords = itemStocks.size();
		simpleObject.put("iTotalDisplayRecords", totalDisplayRecords);
		simpleObject.put("iTotalRecords", totalDisplayRecords);
		simpleObject.put("aaData", simplifiedItemStocks);
		
		return simpleObject;
	}
}
