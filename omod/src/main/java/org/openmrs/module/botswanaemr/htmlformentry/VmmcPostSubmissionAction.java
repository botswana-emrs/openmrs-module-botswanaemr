/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.YES_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.createAppointment;

import java.util.Set;

import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

public class VmmcPostSubmissionAction implements CustomFormSubmissionAction {
	
	private static final String GP_AUTO_DISPENSE_AFTER_VMMC_OPERATION = "botswanaemr.autoDispenseDrugsAfterVmmcOperation";
	
	public static final String DISPENSED_MEDICATION_CONCEPT_UUID = "167380AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
		
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER))) {
			return;
		}
		
		Set<Obs> obsList = session.getEncounter().getObs();
		Obs artNextDateObs = obsList.stream()
		        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.ART_NEXT_DATE_CONCEPT_UUID)).findFirst()
		        .orElse(null);
		
		//create appointment when Date of next ART appointment: field is not blank
		if (artNextDateObs != null) {
			createAppointment(session.getPatient(), session.getEncounter().getLocation(), artNextDateObs.getValueDate(),
			    "VMMC Followup");
		}
		
		Obs dispenseMedicationObs = obsList.stream()
		        .filter(o -> o.getConcept().getUuid().equals(DISPENSED_MEDICATION_CONCEPT_UUID)).findFirst().orElse(null);
		
		boolean dispenseMedication = false;
		if (dispenseMedicationObs != null) {
			dispenseMedication = dispenseMedicationObs.getValueCoded().getUuid().equals(YES_CONCEPT_UUID);
		}
		
		if (dispenseMedication) {
			StockUtils.dispenseDrugFromObsList(obsList);
		}
		
		// Submit a drug order (aka prescription)
		Concept drugGroupConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.MEDICATIONS_GROUPING_CONCEPT);
		Set<Obs> groupingObsSet = ScreeningAndTriagePostSubmissionAction.findGroupingObs(session.getEncounter(),
		    drugGroupConcept);
		if (!groupingObsSet.isEmpty()) {
			BotswanaEmrUtils.saveDrugOrderFromDrugObs(groupingObsSet, session.getPatient(), true);
		}
	}
}
