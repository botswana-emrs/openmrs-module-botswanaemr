/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class IncomingExternalRequisitionsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		Stockroom stockRoom = StockUtils.getMainStockroom(uiSessionContext.getSessionLocation());
		stockOperationTemplate.setSource(stockRoom);
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		stockOperationsSearch.setTemplate(stockOperationTemplate);
		List<StockOperation> stockOperations = stockRoom == null ? new ArrayList<>()
		        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockRoom, stockOperationsSearch, null);
		fragmentModel.addAttribute("operations", stockOperations);
	}
	
	// Approved Positive adjustments
	public SimpleObject saveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (!stockOperationUuid.equals("")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Cancel old requisition
			//stockOperation.setDescription("Cancelled for Approval");
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		} else {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		StockOperation updatedStockOperation = new StockOperation();
		if (stockOperation != null) {
			updatedStockOperation.setStockOperationParent(stockOperation);
		}
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					updatedStockOperation.addItem(item, qnty);
				}
			}
		}
		
		updatedStockOperation.setName("Approve Requisition");
		updatedStockOperation.setDescription(stockOperation.getDescription());
		//updatedStockOperation.setDescription("Approved for Transfer");
		
		Date date;
		try {
			SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			date = DateFor.parse(requisitionDate);
		}
		catch (ParseException e) {
			date = new Date();
		}
		//date = DateFor.parse(requisitionDate);
		updatedStockOperation.setOperationDate(date);
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		updatedStockOperation.setDestination(destStockRoom);
		updatedStockOperation.setSource(sourceStockRoom);
		
		updatedStockOperation.setInstanceType(WellKnownOperationTypes.getTransfer());
		updatedStockOperation.setOperationNumber(stockOperation.getOperationNumber());
		updatedStockOperation.setStatus(StockOperationStatus.NEW); // Create updated requisition
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(updatedStockOperation);
		returnedStockOperation.setStatus(StockOperationStatus.PROCESSING); // Approve requisition
		returnedStockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(updatedStockOperation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
	}
	
	// Approved Negative adjustments
	public SimpleObject approveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (!stockOperationUuid.equals("")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Cancel old requisition
			//stockOperation.setDescription("Cancelled for Approval");
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		} else {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		StockOperation updatedStockOperation = new StockOperation();
		if (stockOperation != null) {
			updatedStockOperation.setStockOperationParent(stockOperation);
		}
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					updatedStockOperation.addItem(item, qnty);
				}
			}
		}
		
		updatedStockOperation.setName("Approve Requisition");
		updatedStockOperation.setDescription(stockOperation.getDescription());
		//updatedStockOperation.setDescription("Approved for Transfer");
		
		Date date;
		try {
			SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			date = DateFor.parse(requisitionDate);
		}
		catch (ParseException e) {
			date = new Date();
		}
		//date = DateFor.parse(requisitionDate);
		updatedStockOperation.setOperationDate(date);
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		updatedStockOperation.setDestination(destStockRoom);
		updatedStockOperation.setSource(sourceStockRoom);
		
		updatedStockOperation.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		updatedStockOperation.setOperationNumber(stockOperation.getOperationNumber());
		updatedStockOperation.setStatus(StockOperationStatus.NEW); // Create updated requisition
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(updatedStockOperation);
		returnedStockOperation.setStatus(StockOperationStatus.PROCESSING); // Approve requisition
		returnedStockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(updatedStockOperation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
	}
	
	// Initiate Transfer by issuing by triggering a distribution stock operation
	public SimpleObject issueRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (stockOperationUuid != "") {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation == null) {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		StockOperation issueRequisitionStockOperation = new StockOperation();
		if (stockOperation != null) {
			//stockOperation.setDescription("Approved for Transfer and Initiated Transfer");
			stockOperation.setStatus(StockOperationStatus.COMPLETED);
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			issueRequisitionStockOperation.setStockOperationParent(stockOperation);
		}
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					issueRequisitionStockOperation.addItem(item, qnty);
				}
			}
		}
		issueRequisitionStockOperation.setName("Issue Requisition");
		issueRequisitionStockOperation.setDescription("Issue Requisition items");
		issueRequisitionStockOperation.setOperationDate(stockOperation.getOperationDate());
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		issueRequisitionStockOperation.setDestination(destStockRoom);
		issueRequisitionStockOperation.setSource(sourceStockRoom);
		issueRequisitionStockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
		issueRequisitionStockOperation.setOperationNumber(stockOperation.getOperationNumber());
		issueRequisitionStockOperation.setStatus(StockOperationStatus.NEW); // Create distribution operation
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(issueRequisitionStockOperation);
		returnedStockOperation.setStatus(StockOperationStatus.PROCESSED); // Complete distribution operation
		BotswanaInventoryContext.getStockOperationService().submitOperation(returnedStockOperation);
		// Initiate receipt at the requesting stockroom
		//StockOperation receiptStockOperation = new StockOperation();
		
		//JSONObject receiptJsonObject = new JSONObject(itemId);
		//Iterator<String> receiptKeys = receiptJsonObject.keys();
		//while (receiptKeys.hasNext()) {
		//	String key = receiptKeys.next();
		//	if (jsonObject.get(key) instanceof JSONObject) {
		//		if (jsonObject.get(key) instanceof JSONObject) {
		//			Item item = BotswanaInventoryContext.getItemDataService()
		//					.getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
		//			int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
		//			receiptStockOperation.addItem(item, qnty);
		//		}
		//	}
		//}
		//receiptStockOperation.setName(WellKnownOperationTypes.getReceipt().getName());
		//receiptStockOperation.setDescription("External requisition transfer ");
		//SimpleDateFormat newDate = new SimpleDateFormat("dd.MMM.yyyy");
		//Date newRequisitionDate = newDate.parse(requisitionDate);
		//receiptStockOperation.setOperationDate(newRequisitionDate);
		//Stockroom requestingDestStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		//Stockroom requestingSourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		//receiptStockOperation.setDestination(requestingDestStockRoom);
		//receiptStockOperation.setSource(requestingSourceStockRoom);
		
		//receiptStockOperation.setInstanceType(WellKnownOperationTypes.getReceipt());
		//receiptStockOperation.setOperationNumber(UUID.randomUUID().toString());
		//receiptStockOperation.setStatus(StockOperationStatus.NEW); // Create transfer operation
		//StockOperation returnedReceiptStockOperation = BotswanaInventoryContext.getStockOperationService()
		//		.submitOperation(receiptStockOperation);
		
		SimpleObject simpleObject = new SimpleObject();
		//simpleObject.put("operationStatus", returnedReceiptStockOperation.getStatus());
		simpleObject.put("operationStatus", stockOperation.getStatus());
		
		return simpleObject;
	}
	
	// Receive Requisition by issuing by triggering a receipt stock operation
	public SimpleObject receiveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (stockOperationUuid != "") {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation == null) {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.COMPLETED);
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		}
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		List<SimpleObject> simpleObjectList = new ArrayList<>();
		
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					int discrepancy = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("discrepancy"));
					String batchNumber = ((JSONObject) jsonObject.get(key)).getString("batchNumber");
					String expiryDateString = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
					Date expiryDate = new SimpleDateFormat("yyyy-MM-dd").parse(expiryDateString);
					String discrepancyReason = ((JSONObject) jsonObject.get(key)).getString("discrepancyReason");
					
					StockOperation receiveRequisitionStockOperation = new StockOperation();
					receiveRequisitionStockOperation.setStockOperationParent(stockOperation);
					receiveRequisitionStockOperation.setOperationNumber(batchNumber);
					receiveRequisitionStockOperation.setName("Receive Requisition");
					receiveRequisitionStockOperation.setDescription("Receive Requisition items");
					receiveRequisitionStockOperation.setOperationDate(stockOperation.getOperationDate());
					receiveRequisitionStockOperation.setDestination(destStockRoom);
					receiveRequisitionStockOperation.setSource(sourceStockRoom);
					receiveRequisitionStockOperation.setInstanceType(WellKnownOperationTypes.getReceipt());
					receiveRequisitionStockOperation.setStatus(StockOperationStatus.NEW); // Create transfer operation
					
					receiveRequisitionStockOperation.addItem(item, qnty, expiryDate, qnty, discrepancy, discrepancyReason,
					    "", receiveRequisitionStockOperation);
					
					StockOperation returnedReceiptStockOperation = BotswanaInventoryContext.getStockOperationService()
					        .submitOperation(receiveRequisitionStockOperation);
					returnedReceiptStockOperation.setStatus(StockOperationStatus.COMPLETED); // Complete transfer operation
					BotswanaInventoryContext.getStockOperationService().submitOperation(returnedReceiptStockOperation);
					
					SimpleObject simpleObject = new SimpleObject();
					simpleObject.put("operationStatus", returnedReceiptStockOperation.getStatus());
					simpleObjectList.add(simpleObject);
				}
			}
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", simpleObjectList);
		
		return simpleObject;
	}
}
