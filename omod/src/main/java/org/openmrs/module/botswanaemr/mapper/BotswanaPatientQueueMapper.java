/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.mapper;

import org.openmrs.module.patientqueueing.mapper.PatientQueueMapper;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.AWAITING;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TREATED;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.labStatus.COMPLETED;
import static org.openmrs.module.patientqueueing.model.PatientQueue.Status.PENDING;

public class BotswanaPatientQueueMapper extends PatientQueueMapper {
	
	private String pin;
	
	private Boolean presumptive;
	
	private String returnUrl;
	
	private Boolean hasTbScreeningEncounter;
	
	private Integer encounterIdentifier;
	
	private Integer queueEncounterId;
	
	private String dateTimeRegistered;
	
	private String durationOfWait;
	
	private String treatmentStartTime;
	
	private String recentQueueLocation;

	private boolean hivPositive;
	
	private  String hivStatus;
	
	public String getHivStatus() {
		return hivStatus;
	}
	
	public void setHivStatus(String hivStatus) {
		this.hivStatus = hivStatus;
	}
	
	public String getRecentQueueLocation() {
		return recentQueueLocation;
	}
	
	public void setRecentQueueLocation(String recentQueueLocation) {
		this.recentQueueLocation = recentQueueLocation;
	}
	
	public String getTreatmentStartTime() {
		return treatmentStartTime;
	}
	
	public void setTreatmentStartTime(String treatmentStartTime) {
		this.treatmentStartTime = treatmentStartTime;
	}
	
	public String getDateTimeRegistered() {
		return dateTimeRegistered;
	}
	
	public void setDateTimeRegistered(String dateTimeRegistered) {
		this.dateTimeRegistered = dateTimeRegistered;
	}
	
	public String getDurationOfWait() {
		return durationOfWait;
	}
	
	public void setDurationOfWait(String durationOfWait) {
		this.durationOfWait = durationOfWait;
	}
	
	@Override
	public String getStatus() {
		String status = super.getStatus();
		if (status.equals(PENDING.toString())) {
			return AWAITING;
		}
		
		if (status.equals(COMPLETED.toString())) {
			return TREATED;
		}
		return status;
	}
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public Boolean getPresumptive() {
		return presumptive;
	}
	
	public void setPresumptive(Boolean presumptive) {
		this.presumptive = presumptive;
	}
	
	public String getReturnUrl() {
		return returnUrl;
	}
	
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	
	public Boolean getHasTbScreeningEncounter() {
		return hasTbScreeningEncounter;
	}
	
	public void setHasTbScreeningEncounter(Boolean hasTbScreeningEncounter) {
		this.hasTbScreeningEncounter = hasTbScreeningEncounter;
	}
	
	public Integer getEncounterIdentifier() {
		return encounterIdentifier;
	}
	
	public void setEncounterIdentifier(Integer encounterIdentifier) {
		this.encounterIdentifier = encounterIdentifier;
	}
	
	public Integer getQueueEncounterId() {
		return queueEncounterId;
	}
	
	public void setQueueEncounterId(Integer queueEncounterId) {
		this.queueEncounterId = queueEncounterId;
	}

	public boolean getHivPositive() {
		return hivPositive;
	}

	public void setHivPositive(boolean hivPositive) {
		this.hivPositive = hivPositive;
	}
}
