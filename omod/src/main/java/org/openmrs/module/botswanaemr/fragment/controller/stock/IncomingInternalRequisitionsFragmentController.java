/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.api.APIException;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.text.SimpleDateFormat;

public class IncomingInternalRequisitionsFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam("action") String action,
	        UiSessionContext uiSessionContext) {
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getInternalRequisition());
		if (action.equals("approve")) {
			stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		} else if (action.equals("receive")) {
			stockOperationTemplate.setStatus(StockOperationStatus.APPROVED);
		} else {
			stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		}
		
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		stockOperationsSearch.setTemplate(stockOperationTemplate);
		List<StockOperation> stockOperations = new ArrayList<>();
		List<Stockroom> bulkStockRooms = StockUtils.getBulkStockrooms(uiSessionContext.getSessionLocation());
		for (Stockroom bulkStockRoom : bulkStockRooms) {
			stockOperationTemplate.setSource(bulkStockRoom);
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> bulkStockOperations = BotswanaInventoryContext.getStockRoomDataService()
			        .getOperations(bulkStockRoom, stockOperationsSearch, null);
			stockOperations.addAll(bulkStockOperations);
		}
		
		fragmentModel.addAttribute("operations", stockOperations);
		fragmentModel.addAttribute("action", action);
	}
	
	// Receive the requisition
	public SimpleObject receiveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = true) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = true) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = true) Integer destinationStockRoomId, PageModel model,
	        UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		SimpleObject simpleObject = new SimpleObject();
		
		if (!stockOperationUuid.equals("")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation != null && stockOperation.getStatus().equals(StockOperationStatus.APPROVED)) {
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Cancel old requisition
			stockOperation.setDescription("Approved requisition");
			
			// The next steps should trigger the transfer of items between the source (Main) and destination (Dispensing) stockrooms
			// Most importantly, if the items are moving from the main stockroom to a dispensing stockroom, there needs to be a conversion
			// from packed units to dispensing units
			// We submit 2 operations concurrently, one that deducts from the source stockroom (Distribution) and another one that adds converted
			// units into the destination stockroom (Receipt)
			
			Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
			Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
			
			StockOperation sourceStockOperation = new StockOperation();
			String operationNumber = UUID.randomUUID().toString();
			sourceStockOperation.setName(WellKnownOperationTypes.getInternalRequisition().getName() + " Source stockroom");
			sourceStockOperation.setDescription(description);
			
			Date today = new Date();
			sourceStockOperation.setOperationDate(today);
			
			JSONObject jsonObject = new JSONObject(itemId);
			Iterator<String> keys = jsonObject.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				if (jsonObject.get(key) instanceof JSONObject) {
					if (jsonObject.get(key) instanceof JSONObject) {
						Item item = BotswanaInventoryContext.getItemDataService()
						        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
						int qty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
						int discrepancy = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("discrepancy"));
						int itemStockDetailId = Integer
						        .parseInt(((JSONObject) jsonObject.get(key)).getString("batchNumber"));
						String expiryDateString = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						Date expiryDate = new SimpleDateFormat("yyyy-MM-dd").parse(expiryDateString);
						String discrepancyReason = ((JSONObject) jsonObject.get(key)).getString("discrepancyReason");
						
						ItemStockDetail itemStockDetail = BotswanaInventoryContext.getItemStockDetailDataService()
						        .getById(itemStockDetailId);
						if (itemStockDetail != null) {
							sourceStockOperation.addItem(item, qty, expiryDate, qty, discrepancy, discrepancyReason, "",
							    itemStockDetail.getBatchOperation());
						} else {
							simpleObject.put("status", "error");
							simpleObject.put("response",
							    "No Stock items found for " + item.getName() + " in the selected stockroom");
							throw new RuntimeException(
							        "No Stock items found for " + item.getName() + " in the selected stockroom");
						}
						
						// // -----
						// // TODO: Set a way to pick the batches from the UI
						// // TODO: Display unit conversion values on the UI and allow user to allow conversion or otherwise
						// int runningBalanceQty = qty;
						// ItemStock itemStock = StockUtils.getItemStockByStockRoom(item, sourceStockRoom);
						// if (itemStock == null) {
						// 	simpleObject.put("status", "error");
						// 	simpleObject.put("response",
						// 	    "No Stock items found for " + item.getName() + " in the selected stockroom");
						// 	throw new RuntimeException(
						// 	        "No Stock items found for " + item.getName() + " in the selected stockroom");
						
						// } else {
						// 	Set<ItemStockDetail> itemStockDetails = itemStock.getDetails();
						// 	if (!itemStockDetails.isEmpty()) {
						// 		// Ensure that the batches are arranged per the expiry date
						// 		Comparator<Date> nullSafeStringComparator = Comparator.nullsFirst(Date::compareTo);
						// 		for (ItemStockDetail itemStockDetail : itemStockDetails.stream()
						// 		        .filter(i -> i.getQuantity() > 0 && i.getExpiration() != null)
						// 		        .sorted(
						// 		            Comparator.comparing(ItemStockDetail::getExpiration, nullSafeStringComparator))
						// 		        .collect(Collectors.toList())) {
						// 			int availableInBatchQty = itemStockDetail.getQuantity();
						// 			int qtyToDispatch;
						// 			if (runningBalanceQty > 0) {
						// 				if (runningBalanceQty >= availableInBatchQty) {
						// 					qtyToDispatch = availableInBatchQty;
						// 					runningBalanceQty = 0;
						// 				} else {
						// 					qtyToDispatch = runningBalanceQty;
						// 					runningBalanceQty -= qtyToDispatch;
						// 				}
						// 				sourceStockOperation.addItem(item, qtyToDispatch, itemStockDetail.getExpiration(),
						// 				    itemStockDetail.getBatchOperation());
						// 			}
						// 		}
						// 	}
						// }
						// // -----
					}
				}
			}
			
			sourceStockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
			
			StockOperation destinationStockOperation = new StockOperation();
			destinationStockOperation
			        .setName(WellKnownOperationTypes.getInternalRequisition().getName() + "Destination stockroom");
			destinationStockOperation.setOperationDate(today);
			
			for (StockOperationItem item : sourceStockOperation.getItems()) {
				// Get the unpacked quantities
				int qty = item.getQuantity() * StockUtils.getUnitConversionValue(item.getItem());
				destinationStockOperation.addItem(item.getItem(), qty, item.getExpiration(), item.getBatchOperation());
			}
			
			destinationStockOperation.setInstanceType(WellKnownOperationTypes.getReceipt());
			
			// Submit the stock transactions //
			// CLose the application stock transaction
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			// CLose the application source stock transaction
			StockOperation returnedSourceStockOperation = submitStockOperation(sourceStockOperation, stockOperation,
			    description, operationNumber, sourceStockRoom, null);
			// CLose the application destination stock transaction
			StockOperation returnedDestinationStockOperation = submitStockOperation(destinationStockOperation,
			    stockOperation, description, operationNumber, null, destStockRoom);
			
			simpleObject.put("status", "success");
			simpleObject.put("response",
			    returnedSourceStockOperation.getStatus() + " : " + returnedDestinationStockOperation.getStatus());
		} else {
			simpleObject.put("status", "error");
			simpleObject.put("response", "No pending Requisition with the given uuid to update");
			throw new IllegalArgumentException("No pending Requisition with the given uuid to update");
		}
		
		return simpleObject;
	}
	
	// Approve the requisition
	public SimpleObject approveRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "requisitionType", required = false) String requisitionType,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (!stockOperationUuid.equals("")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation != null) {
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Cancel old requisition
			//stockOperation.setDescription("Cancelled for Approval");
			BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		} else {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		
		StockOperation updatedStockOperation = new StockOperation();
		if (stockOperation != null) {
			updatedStockOperation.setStockOperationParent(stockOperation);
		}
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					updatedStockOperation.addItem(item, qnty);
				}
			}
		}
		
		updatedStockOperation.setName("Approve Requisition");
		updatedStockOperation.setDescription(stockOperation.getDescription());
		//updatedStockOperation.setDescription("Approved for Transfer");
		
		Date date;
		try {
			SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			date = DateFor.parse(requisitionDate);
		}
		catch (ParseException e) {
			date = new Date();
		}
		//date = DateFor.parse(requisitionDate);
		updatedStockOperation.setOperationDate(date);
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		updatedStockOperation.setDestination(destStockRoom);
		updatedStockOperation.setSource(sourceStockRoom);
		
		updatedStockOperation.setInstanceType(stockOperation.getInstanceType());
		updatedStockOperation.setOperationNumber(stockOperation.getOperationNumber());
		updatedStockOperation.setStatus(StockOperationStatus.NEW); // Create updated requisition
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(updatedStockOperation);
		returnedStockOperation.setStatus(StockOperationStatus.APPROVED); // Approve requisition
		returnedStockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(updatedStockOperation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
	}
	
	private StockOperation submitStockOperation(StockOperation stockOperation, StockOperation parentStockOperation,
	        String description, String operationNumber, Stockroom sourceStockroom, Stockroom destinationStockroom)
	        throws APIException {
		
		if (parentStockOperation != null) {
			stockOperation.setStockOperationParent(parentStockOperation);
		}
		
		stockOperation.setOperationNumber(operationNumber);
		stockOperation.setDescription(description);
		
		if (sourceStockroom != null) {
			stockOperation.setSource(sourceStockroom);
		}
		if (destinationStockroom != null) {
			stockOperation.setDestination(destinationStockroom);
		}
		
		StockOperation returnedStockOperation;
		try {
			stockOperation.setStatus(StockOperationStatus.NEW); // Create updated approved requisition
			returnedStockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			returnedStockOperation.setStatus(StockOperationStatus.COMPLETED); // Approve requisition
			returnedStockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		}
		catch (Exception e) {
			BotswanaInventoryContext.getStockOperationService().rollbackOperation(parentStockOperation);
			throw new APIException(e.getMessage());
		}
		
		return returnedStockOperation;
	}
	
	public SimpleObject transferRequisition(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (!Objects.equals(stockOperationUuid, "")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
		}
		if (stockOperation == null) {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		StockOperation transferStockOperation = new StockOperation();
		transferStockOperation.setStockOperationParent(stockOperation);
		JSONObject jsonObject = new JSONObject(itemId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					Item item = BotswanaInventoryContext.getItemDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					int qnty = Integer.parseInt(((JSONObject) jsonObject.get(key)).getString("quantity"));
					transferStockOperation.addItem(item, qnty);
				}
			}
		}
		transferStockOperation.setName(WellKnownOperationTypes.getTransfer().getName());
		transferStockOperation.setDescription("Transferred from Requisition");
		Date date = BotswanaEmrUtils.parseDateFromString(requisitionDate);
		transferStockOperation.setOperationDate(date);
		Stockroom destStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(destinationStockRoomId);
		Stockroom sourceStockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(sourceStockRoomId);
		transferStockOperation.setDestination(destStockRoom);
		transferStockOperation.setSource(sourceStockRoom);
		transferStockOperation.setInstanceType(WellKnownOperationTypes.getTransfer());
		transferStockOperation.setOperationNumber(UUID.randomUUID().toString());
		transferStockOperation.setStatus(StockOperationStatus.NEW); // Create transfer operation
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(transferStockOperation);
		stockOperation.setDescription("Initiated transfer");
		BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
	}
	
	public SimpleObject receiveItems(@RequestParam(value = "itemId", required = true) String itemId,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "stockOperationUuid", required = false) String stockOperationUuid,
	        @RequestParam(value = "requisitionDate", required = false) String requisitionDate,
	        @RequestParam(value = "sourceStockRoomId", required = false) Integer sourceStockRoomId,
	        @RequestParam(value = "destinationStockRoomId", required = false) Integer destinationStockRoomId,
	        PageModel model, UiUtils ui, HttpSession session,
	        @SpringBean("bemrs.stockOperationService") IStockOperationService iStockOperationService,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService)
	        throws JSONException, ParseException {
		
		StockOperation stockOperation = null;
		if (!Objects.equals(stockOperationUuid, "")) {
			stockOperation = iStockOperationDataService.getByUuid(stockOperationUuid);
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Create Receipt operation
			
		}
		if (stockOperation == null) {
			throw new IllegalArgumentException("No Requisition with the given uuid to update");
		}
		StockOperation returnedStockOperation = BotswanaInventoryContext.getStockOperationService()
		        .submitOperation(stockOperation);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("operationStatus", returnedStockOperation.getStatus());
		
		return simpleObject;
	}
	
}
