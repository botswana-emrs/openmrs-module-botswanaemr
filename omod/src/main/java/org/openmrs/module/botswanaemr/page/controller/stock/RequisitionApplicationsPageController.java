/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.List;

import org.openmrs.api.LocationService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class RequisitionApplicationsPageController {
	
	public void controller(PageModel model, @RequestParam("id") StockOperation stockOperation,
	        @RequestParam(value = "requestType", required = false) String requestType,
	        @RequestParam(value = "actionType", required = false) String actionType,
	        @SpringBean("locationService") LocationService locationService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		
		List<Stockroom> stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(),
		    false);
		model.addAttribute("stockrooms", stockrooms);
		model.addAttribute("items", stockOperation.getItems());
		model.addAttribute("location", uiSessionContext.getSessionLocation());
		model.addAttribute("stockOperation", stockOperation);
		model.addAttribute("requestType", requestType);
		model.addAttribute("actionType", actionType);
		
	}
}
