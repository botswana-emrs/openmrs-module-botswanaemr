/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs.tb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.Visit;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CONTACT_PERSON_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_CONTACT_SCREENING_ENCOUNTER_TYPE;

@Slf4j
@SuppressWarnings("unused")
public class ListOfcontactsFragmentController {
	
	private Visit currentPatientVisit;
	
	public void controller(@RequestParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("visitService") VisitService visitService, FragmentModel fragmentModel,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiSessionContext sessionContext) {
		
		currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		Collection<Patient> patientsInContact = botswanaEmrService.findPersonInContactWithTBPatients(patient);
		
		List<TBContactPerson> tbContactPersons = new ArrayList<>();
		for (Patient p : patientsInContact) {
			TBContactPerson tbContactPerson = new TBContactPerson();
			tbContactPerson.setPatient(p);
			tbContactPerson.setScreen(isPatientScreenForTB(p));
			tbContactPerson.setVisit(BotswanaEmrUtils.getPatientActiveVisit(p, sessionContext.getSessionLocation(), true));
			tbContactPersons.add(tbContactPerson);
		}
		
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("currentVisit", currentPatientVisit);
		fragmentModel.addAttribute("patientsInContact", tbContactPersons);
		
		String returnUrlParam = ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "programs/programs", patient, currentPatientVisit, ui));
		
		fragmentModel.addAttribute("returnUrl", returnUrlParam);
	}
	
	protected List<Patient> getTbInContacts(@NotNull Patient patient) {
		PatientService patientService = Context.getPatientService();
		
		PersonAttributeType personContactAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(CONTACT_PERSON_ATTRIBUTE_TYPE_UUID);
		List<PersonAttribute> personContactAttributes = patient.getAttributes(personContactAttributeType);
		
		List<Patient> tbInContacts = new ArrayList<>();
		for (PersonAttribute attribute : personContactAttributes) {
			Patient tbInContact = patientService.getPatientByUuid(attribute.getValue());
			if (tbInContact != null) {
				tbInContacts.add(tbInContact);
			}
		}
		return tbInContacts;
	}
	
	public boolean isPatientScreenForTB(@NotNull Patient patient) {
		EncounterType tbContactScreenEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(TB_CONTACT_SCREENING_ENCOUNTER_TYPE);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, null, null, null, null,
		        Collections.singletonList(tbContactScreenEncounterType), null, null, null, false);
		
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		List<Obs> observations = Context.getObsService().getObservations(Collections.singletonList(patient.getPerson()),
		    encounters, null, null, null, null, null, null, null, null, null, false);
		
		return observations.size() > 0;
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class TBContactPerson {
		
		private Patient patient;
		
		private Visit visit;
		
		private boolean isScreen;
	}
}
