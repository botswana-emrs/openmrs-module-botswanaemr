/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Person;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.SimplifiedEncounter;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Date;
import java.util.stream.Collectors;

public class PnsClientProfilePageController {
	
	private static final String PNS_CONTACT_RECORD_ID_CONCEPT_UUID = "4c528a7b-f7ec-4efa-aaad-17adc229042f";
	
	private static final String PNS_CONTACT_RECORD_NAME_CONCEPT_UUID = "5ac051bb-b94c-497f-9c08-cc28c0e6369b";
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("conceptService") ConceptService conceptService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService,
	        @SpringBean("encounterService") EncounterService encounterService, UiUtils ui, final PageModel pageModel,
	        UiSessionContext sessionContext) {
		
		Concept contactsObsGroupConcept = conceptService
		        .getConceptByUuid(BotswanaEmrConstants.PNS_CONTACTS_OBS_GROUP_CONCEPT_UUID);
		List<Obs> observations = new ArrayList<>(BotswanaEmrUtils.getObservations(patient, contactsObsGroupConcept));
		
		List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
		boolean hasCompletedHtsDailyForm = false;
		boolean hasIpvScreeningResult = false;
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		List<HtmlForm> completedHtmlForms = BotswanaEmrUtils.completedHtmlForms(currentPatientVisit, htmlFormEntryService);
		allHtmlForms.removeAll(completedHtmlForms);
		
		List<HtmlForm> htsPnsEncounterForms = BotswanaEmrUtils.getPnsEncounterForms(htmlFormEntryService, formService)
		        .stream().filter(Objects::nonNull).collect(Collectors.toList());
		
		List<Encounter> filledEncounterForPatientPerTheCurrentVisit = BotswanaEmrUtils.getHtsEncounters(patient,
		    encounterService, htsPnsEncounterForms);
		
		List<SimpleObject> simplifiedContacts = simplifiedContacts(observations,
		    filledEncounterForPatientPerTheCurrentVisit);
		
		// Sort the simplifiedContacts list by date in descending order as the last encounter date
		Collections.sort(simplifiedContacts, new Comparator<SimpleObject>() {
			
			@Override
			public int compare(SimpleObject o1, SimpleObject o2) {
				if (o1.get("date") == null || o2.get("date") == null) {
					return 0;
				}
				
				Date date1;
				Date date2;
				try {
					date1 = (Date) o1.get("date");
					date2 = (Date) o2.get("date");
					
					// Use obs datetime as fallback if date is null
					if (date1 == null) {
						date1 = (Date) o1.get("obsDatetime");
					}
					if (date2 == null) {
						date2 = (Date) o2.get("obsDatetime");
					}
				}
				catch (Exception e) {
					return 0;
				}
				
				return date2.compareTo(date1);
			}
		});
		
		pageModel.addAttribute("contacts", simplifiedContacts);
		
		pageModel.addAttribute("encounters", getSimplifiedEncounters(filledEncounterForPatientPerTheCurrentVisit));
		
		List<HtmlForm> tmpAvailableForms;
		if (filledEncounterForPatientPerTheCurrentVisit.isEmpty()) {
			tmpAvailableForms = htsPnsEncounterForms;
		} else {
			List<Form> hivForms = filledEncounterForPatientPerTheCurrentVisit.stream().map(Encounter::getForm)
			        .collect(Collectors.toList());
			tmpAvailableForms = htsPnsEncounterForms.stream().filter(f -> !hivForms.contains(f.getForm()))
			        .collect(Collectors.toList());
		}
		
		HtmlForm childrenOfIndexInformationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID));
		
		HtmlForm ipvScreeningForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_IPV_SCREENING_FORM_UUID));
		
		HtmlForm partnerCharacteristicAndHivForm = htmlFormEntryService.getHtmlFormByForm(
		    formService.getFormByUuid(BotswanaEmrConstants.HTS_PARTNER_CHARACTERISTIC_AND_HIV_FORM_UUID));
		
		HtmlForm contactsForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_CONTACTS_FORM_UUID));
		
		HtmlForm referralsAndOutcomeForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_REFERRALS_AND_OUTCOME_FORM_UUID));
		
		for (Encounter htsEncounter : filledEncounterForPatientPerTheCurrentVisit) {
			if (htsEncounter.getForm().getEncounterType() == Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID)) {
				hasCompletedHtsDailyForm = true;
			}
			
			if (htsEncounter.getForm().getEncounterType() == Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_IPV_SCREENING_ENCOUNTER_TYPE_UUID)) {
				Obs ipvScreeningResultObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
				    BotswanaEmrConstants.HTS_IPV_SCREENING_RESULT_CONCEPT_UUID, null, null);
				Obs ipvScreeningNotificationOptionsObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
				    BotswanaEmrConstants.HTS_IPV_SCREENING_NOTIFICATION_OPTIONS_CONCEPT_UUID, null, null);
				
				if ((ipvScreeningResultObs != null
				        && ipvScreeningResultObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.NO_CONCEPT_UUID))
				        || (ipvScreeningNotificationOptionsObs != null
				                && Objects.requireNonNull(ipvScreeningNotificationOptionsObs).getValueCoded().getUuid()
				                        .equals(BotswanaEmrConstants.HTS_ACTIVE_PARTNER_NOTIFICATION_CONCEPT_UUID))
				        || (ipvScreeningNotificationOptionsObs != null
				                && Objects.requireNonNull(ipvScreeningNotificationOptionsObs).getValueCoded().getUuid()
				                        .equals(BotswanaEmrConstants.HTS_MASK_HIV_TESTING_CONCEPT_UUID))) {
					hasIpvScreeningResult = true;
				}
			}
		}
		String sexualPartnerConceptId = conceptService.getConceptByUuid(BotswanaEmrConstants.SEXUAL_PARTNER_CONCEPT_UUID)
		        .getId().toString();
		String ivduConceptId = conceptService.getConceptByUuid(BotswanaEmrConstants.IVDU_CONCEPT_UUID).getId().toString();
		
		pageModel.addAttribute("sexualPartnerConceptId", sexualPartnerConceptId);
		pageModel.addAttribute("ivduConceptId", ivduConceptId);
		
		pageModel.addAttribute("hasCompletedHtsDailyForm", hasCompletedHtsDailyForm);
		
		boolean hasActiveVisit = currentPatientVisit != null;
		
		pageModel.addAttribute("availableForms", tmpAvailableForms);
		
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		
		pageModel.addAttribute("childrenOfIndexInformationForm", childrenOfIndexInformationForm);
		pageModel.addAttribute("ipvScreeningForm", ipvScreeningForm);
		pageModel.addAttribute("partnerCharacteristicAndHivForm", partnerCharacteristicAndHivForm);
		pageModel.addAttribute("contactsForm", contactsForm);
		pageModel.addAttribute("hasIpvScreeningResult", hasIpvScreeningResult);
		pageModel.addAttribute("referralsAndOutcomeForm", referralsAndOutcomeForm);
		pageModel.addAttribute("observations", observations);
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("encounter", encounter);
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "hts/pnsClientProfile", patient, currentPatientVisit, ui)));
	}
	
	public static List<SimpleObject> simplifiedContacts(List<Obs> groupingObs, List<Encounter> filledEncounters) {
		List<SimpleObject> simplifiedContacts = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				Encounter lastIpvEncounter = getLastIpvScreeningEncounter(groupMember);
				Obs ipvScreeningResult = getIpvScreeningResult(lastIpvEncounter);
				Obs ipvNotificationOption = getNotificationOption(lastIpvEncounter);
				
				simpleObject.put("pnsContactRecordId", obs.getObsId());
				boolean hasIpvScreeningResult = ipvScreeningResult != null && ipvScreeningResult.getValueCoded() != null;
				boolean hasNotificationOption = ipvNotificationOption != null
				        && ipvNotificationOption.getValueCoded() != null;
				boolean ipvResult = false;
				boolean hasFilledChildrenIndexForm = false;
				
				if (hasNotificationOption || hasIpvScreeningResult) {
					ipvResult = true;
				}
				simpleObject.put("hasIpvScreeningResult", ipvResult);
				
				if (ipvResult) {
					simpleObject.put("ipvScreeningEncounter", lastIpvEncounter);
					boolean ipvHtsActivePartnerNotication = ipvNotificationOption != null
					        && ipvNotificationOption.getValueCoded().getUuid()
					                .equals(BotswanaEmrConstants.HTS_ACTIVE_PARTNER_NOTIFICATION_CONCEPT_UUID);
					boolean ipvHtMaskHivTesting = ipvNotificationOption != null && ipvNotificationOption.getValueCoded()
					        .getUuid().equals(BotswanaEmrConstants.HTS_MASK_HIV_TESTING_CONCEPT_UUID);
					
					if (ipvHtsActivePartnerNotication || ipvHtMaskHivTesting || (ipvScreeningResult != null
					        && ipvScreeningResult.getValueCoded().getUuid().equals(BotswanaEmrConstants.NO_CONCEPT_UUID))) {
						simpleObject.put("ipvScreeningResult", "Negative");
					} else if (ipvScreeningResult != null
					        && ipvScreeningResult.getValueCoded().getUuid().equals(BotswanaEmrConstants.YES_CONCEPT_UUID)) {
						simpleObject.put("ipvScreeningResult", "Positive");
					} else {
						simpleObject.put("ipvScreeningResult", "Not done");
					}
				} else {
					simpleObject.put("ipvScreeningResult", "Not done");
				}
				
				if (groupMember.getConcept().getUuid().equals(BotswanaEmrConstants.CONTACT_P_NAME_CONCEPT_UUID)) {
					String name = groupMember.getValueCoded() != null ? groupMember.getValueCoded().getName().getName()
					        : groupMember.getValueText();
					simpleObject.put("name", name);
					
					if (!getSimplifiedEncounters(filledEncounters).isEmpty()) {
						for (SimplifiedEncounter simplifiedEncounter : getSimplifiedEncounters(filledEncounters)) {
							if (name != null) {
								if (simplifiedEncounter.getPartnerName().equals(name)) {
									hasFilledChildrenIndexForm = true;
									simpleObject.put("childrenOfIndexInformationEncounter", simplifiedEncounter);
								}
							}
						}
					}
					simpleObject.put("hasFilledChildrenIndexForm", hasFilledChildrenIndexForm);
				}
				if (groupMember.getConcept().getUuid().equals(BotswanaEmrConstants.CHILD_NOT_CHILD_CONCEPT_UUID)) {
					simpleObject.put("relation",
					    groupMember.getValueCoded() != null ? groupMember.getValueCoded().getConceptId().toString()
					            : groupMember.getValueText());
				}
				if (groupMember.getConcept().getUuid().equals(BotswanaEmrConstants.LAST_ENCOUNTER_DATE_CONCEPT_UUID)) {
					simpleObject.put("date",
					    groupMember.getValueDate() != null ? groupMember.getValueDate() : groupMember.getValueText());
				}
			}
			if (!simpleObject.isEmpty()) {
				simplifiedContacts.add(simpleObject);
			}
		}
		return simplifiedContacts;
	}
	
	private static Obs getIpvScreeningResult(Encounter lastIpvEncounter) {
		
		// Get ipv screening results from the latest IPV encounter
		Obs lastIpvScreeningResult = null;
		if (lastIpvEncounter != null) {
			lastIpvScreeningResult = lastIpvEncounter.getAllObs().stream()
			        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_IPV_SCREENING_RESULT_CONCEPT_UUID))
			        .findFirst().orElse(null);
		}
		return lastIpvScreeningResult;
	}
	
	private static Obs getNotificationOption(Encounter lastIpvEncounter) {
		
		// Get ipv screening results from the latest IPV encounter
		Obs lastNotificationOption = null;
		if (lastIpvEncounter != null) {
			lastNotificationOption = lastIpvEncounter.getAllObs().stream()
			        .filter(o -> o.getConcept().getUuid()
			                .equals(BotswanaEmrConstants.HTS_IPV_SCREENING_NOTIFICATION_OPTIONS_CONCEPT_UUID))
			        .findFirst().orElse(null);
		}
		return lastNotificationOption;
	}
	
	private static Encounter getLastIpvScreeningEncounter(Obs groupMember) {
		// Fetch IPV screening result from an IPV encounter containing contactRecordId obs value corresponding to the obsGroupId
		Concept pnsContactRecordIdConcept = Context.getConceptService().getConceptByUuid(PNS_CONTACT_RECORD_ID_CONCEPT_UUID);
		Person indexClient = groupMember.getPerson();
		List<Obs> obs = Context.getObsService().getObservations(Collections.singletonList(indexClient), null,
		    Collections.singletonList(pnsContactRecordIdConcept), null, null, null, null, null, null, null, null, false);
		List<Encounter> ipvScreeningEncounters = new ArrayList<>();
		for (Obs ob : obs) {
			if (Objects.equals(ob.getValueNumeric(), Double.valueOf(groupMember.getObsGroup().getId()))) {
				Encounter encounter = ob.getEncounter();
				EncounterType encounterType = encounter.getEncounterType();
				if (encounterType.getUuid().equals(BotswanaEmrConstants.HTS_IPV_SCREENING_ENCOUNTER_TYPE_UUID)) {
					ipvScreeningEncounters.add(encounter);
				}
			}
		}
		
		// Get ipv screening results from the latest IPV encounter
		Obs lastIpvScreeningResult = null;
		Encounter lastIpvEncounter = null;
		if (!ipvScreeningEncounters.isEmpty()) {
			lastIpvEncounter = ipvScreeningEncounters.stream().max(Comparator.comparing(Encounter::getEncounterDatetime))
			        .orElse(null);
		}
		return lastIpvEncounter;
	}
	
	private static List<SimplifiedEncounter> getSimplifiedEncounters(List<Encounter> encounters) {
		List<SimplifiedEncounter> pnsEncounters = new ArrayList<>();
		SimplifiedEncounter simplifiedEncounter;
		if (encounters != null) {
			for (Encounter encounter : encounters) {
				simplifiedEncounter = new SimplifiedEncounter();
				simplifiedEncounter.setEncounterUuid(encounter.getUuid());
				simplifiedEncounter.setVisitId(encounter.getVisit().getVisitId());
				simplifiedEncounter.setStatus(encounter.getPatient().getDead().equals(false) ? "Active" : "Expired");
				simplifiedEncounter.setPatientId(encounter.getPatient().getPatientId());
				simplifiedEncounter.setPin(encounter.getPatient()
				        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
				simplifiedEncounter
				        .setPatientNames(BotswanaEmrUtils.formatPersonName(encounter.getPatient().getPersonName()));
				simplifiedEncounter
				        .setPatientGender(encounter.getPatient().getPerson().getGender().equals("M") ? "Male" : "Female");
				simplifiedEncounter.setPatientAge(String.valueOf(encounter.getPatient().getAge()));
				simplifiedEncounter.setPatientRegistrationDate(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getPatient().getDateCreated(), "dd-MM-yyyy"));
				simplifiedEncounter.setPatientDateOfBirth(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getPatient().getBirthdate(), "dd-MM-yyyy"));
				simplifiedEncounter.setConsultationDate(
				    BotswanaEmrUtils.formatDateWithoutTime(encounter.getEncounterDatetime(), "dd-MM-yyyy"));
				simplifiedEncounter.setEncounterDatetime(encounter.getEncounterDatetime());
				simplifiedEncounter.setCreator(encounter.getCreator());
				simplifiedEncounter.setForm(encounter.getForm());
				
				// Extract partner name from observations and set it as an additional attribute
				String partnerName = encounter.getObs().stream()
				        .filter(obs -> obs.getConcept().getUuid().equals(PNS_CONTACT_RECORD_NAME_CONCEPT_UUID))
				        .map(Obs::getValueText).findFirst().orElse("N/A");
				simplifiedEncounter.setPartnerName(partnerName);
				
				pnsEncounters.add(simplifiedEncounter);
			}
		}
		return pnsEncounters;
	}
}
