/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.lab;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.Concept;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class AddLaboratoryTestOrdersPageController {
	
	public static final String LAB_PANEL_CATEGORIES = "4391b449-2ba6-438b-8ff4-5300141af098";
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "encounterId") Encounter associatedEncounter,
	        UiSessionContext uiSessionContext, @SpringBean("labService") LabService labService,
	        @SpringBean("visitService") VisitService visitService, final PageModel pageModel) {
		EncounterType labOrderEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.LAB_ORDER_ENCOUNTER_TYPE_UUID);
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("location", uiSessionContext.getSessionLocation());
		pageModel.addAttribute("labOrderingSites", BotswanaEmrUtils.getLabOrderingSites());
		List<SimplifiedLabTest> labTests = new ArrayList<>();
		Visit currentVisit;
		Set<Encounter> allEncountersForThisVisit = null;
		List<Encounter> resultantEncounters = new ArrayList<>();
		
		boolean isConsultation = uiSessionContext.getSession()
		        .getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT)
		        .equals(BotswanaEmrConstants.DOCTORS_PORTAL_UUID);
		pageModel.addAttribute("isConsultation", isConsultation);
		
		List<Visit> currentVisitForThePatient = visitService.getActiveVisitsByPatient(patient);
		if (currentVisitForThePatient != null && !currentVisitForThePatient.isEmpty()) {
			currentVisit = currentVisitForThePatient.get(currentVisitForThePatient.size() - 1);
			if (currentVisit != null) {
				allEncountersForThisVisit = new HashSet<>(currentVisit.getEncounters());
			}
		}
		if (allEncountersForThisVisit != null && allEncountersForThisVisit.size() > 0) {
			for (Encounter encounter : allEncountersForThisVisit) {
				if (encounter.getEncounterType().equals(labOrderEncounterType)) {
					resultantEncounters.add(encounter);
				}
			}
		}
		if (!resultantEncounters.isEmpty()) {
			for (Encounter encounter : resultantEncounters) {
				labTests.addAll(BotswanaEmrUtils
				        .getSimplifiedLabTests(labService.getLaboratoryTestListByEncounter(encounter), labService));
			}
		}
		
		// Fetch all the lab profiles as concepts - These are set member of the labset concept 4391b449-2ba6-438b-8ff4-5300141af098 (Lab categories))
		Concept labPanelParent = Context.getConceptService().getConceptByUuid(LAB_PANEL_CATEGORIES);
		// Fetch all the lab test panels
		// Iterate over all the lab profiles and fetch concepts that are set members of the lab profile
		List<SimpleObject> labPanels = new ArrayList<>();
		// if labPanelParent is null, initialize all the model attributes to empty
		if (labPanelParent != null) {
			for (Concept labPanel : labPanelParent.getSetMembers()) {
				// Check if the lab panel has already been added to the labPanels list before adding it
				if (!labPanels.stream().anyMatch(panel -> panel.get("panelId").equals(labPanel.getConceptId()))) {
					SimpleObject labPanelObj = new SimpleObject();
					labPanelObj.put("panelId", labPanel.getConceptId());
					labPanelObj.put("name", labPanel.getName().getName());
					labPanelObj.put("uuid", labPanel.getUuid());
					
					labPanels.add(labPanelObj);
				}
			}
			
			// pageModel.addAttribute("labPanelParent", labPanelParent);
			pageModel.addAttribute("labPanels", labPanels);
			pageModel.addAttribute("labTests", labTests);
			pageModel.addAttribute("location", uiSessionContext.getSessionLocation());
			pageModel.addAttribute("patient", patient);
			pageModel.addAttribute("currentUser", formatPersonName(uiSessionContext.getCurrentUser().getPersonName()));
			pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
			pageModel.addAttribute("encounter", associatedEncounter);
		} else {
			pageModel.addAttribute("labPanels", new ArrayList<>());
			pageModel.addAttribute("labTests", new ArrayList<>());
			pageModel.addAttribute("location", uiSessionContext.getSessionLocation());
			pageModel.addAttribute("patient", patient);
			pageModel.addAttribute("currentUser", formatPersonName(uiSessionContext.getCurrentUser().getPersonName()));
			pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
			pageModel.addAttribute("encounter", associatedEncounter);
		}
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
	}
}
