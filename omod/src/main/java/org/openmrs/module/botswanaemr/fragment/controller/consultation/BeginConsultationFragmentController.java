/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class BeginConsultationFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiUtils uiUtils, @RequestParam("patientId") Patient patient) {
		fragmentModel.addAttribute("defaultCaseDescription", "Consultation for " + patient.getPersonName().getFullName());
		fragmentModel.addAttribute("patient", patient);
	}
	
	public String post(@RequestParam("patientId") Patient patient, UiUtils uiUtils,
	        @RequestParam(value = "cases", required = false) String caseId,
	        @RequestParam(value = "caseradiogroup", required = false) String option,
	        @RequestParam(value = "description", required = false) String description,
	        @RequestParam(value = "referralId", required = false) String referralId,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("botswanaEmrService") BotswanaEmrService botswanaEmrService,
	        @SpringBean("orderService") OrderService orderService,
	        @RequestParam(value = "complete", required = false) String action, UiSessionContext uiSessionContext) {
		
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		Cases actualCase = null;
		Order referral = null;
		if (StringUtils.isNotBlank(referralId)) {
			referral = orderService.getOrder(Integer.valueOf(referralId));
		}
		if (StringUtils.isNotBlank(caseId)) {
			actualCase = botswanaEmrService.getCase(Integer.valueOf(caseId));
		}
		if (action.equals("start")) {
			Visit lastPatientVisit = BotswanaEmrUtils.getLastPatientVisit(patient, null);
			String visitNumber = lastPatientVisit != null ? lastPatientVisit.getVisitId().toString() : "";
			
			BotswanaEmrUtils.startConsultation(patient, option, description, patientQueueingService, encounterService,
			    uiSessionContext.getSessionLocation(), actualCase, botswanaEmrService, orderService, referral,
			    lastPatientVisit, uiSessionContext.getCurrentUser());
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("patientId", patient.getPatientId());
			params.put("visitId", visitNumber);
			return "redirect:" + uiUtils.pageLink("botswanaemr", "consultation/consultation", params);
		}
		return null;
	}
}
