/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.objective;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.LabOrderService;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class LabOrderFragmentController {
	
	private final ObsService obsService = Context.getService(ObsService.class);
	
	protected final Log log = LogFactory.getLog(LabOrderFragmentController.class);
	
	public void controller(FragmentModel fragmentModel, @FragmentParam("patient") Patient patient,
			UiSessionContext uiSessionContext) {
		EncounterType labOrderEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE);
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
	}
	
	public List<SimpleObject> getLabTests(@RequestParam(value = "query") String query, UiUtils ui) {
		List<Concept> tests = Context.getService(LabService.class).searchLabTest(query);
		return SimpleObject.fromCollection(tests, ui, "id", "name", "uuid");
	}
	
	public List<SimpleObject> getLabTestsFromPanel(@RequestParam(value = "query", required = false) String query,
	        @RequestParam(value = "labPanelIds", required = false) String labPanelIds, UiUtils ui) {
		if (StringUtils.isEmpty(labPanelIds)) {
			return new ArrayList<>();
		}
		String[] labPanelConceptIds = labPanelIds.split(",");
		List<SimpleObject> simpleObjects = new ArrayList<>();
		for (String labPanelConceptId : labPanelConceptIds) {
			//TODO: Use concept UUIDs instead of conceptIds
			Concept labPanelConcept = Context.getConceptService().getConceptByUuid(labPanelConceptId);
			if (labPanelConcept == null || labPanelConcept.getSetMembers() == null) {
				continue;
			}
			for (Concept concept : labPanelConcept.getSetMembers()) {
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.put("id", concept.getConceptId());
				simpleObject.put("name", concept.getName().getName());
				simpleObject.put("uuid", concept.getUuid());
				simpleObject.put("panelId", labPanelConcept.getUuid());
				simpleObjects.add(simpleObject);
			}
		}
		
		return simpleObjects;
		// return SimpleObject.fromCollection(tests, ui, "id", "name", "uuid");
	}
	
	public void addLabOrders(@RequestParam("labList") String labList, @RequestParam("patientId") Patient patient,
	        @RequestParam("locationId") Location location,
	        @RequestParam(required = false, value = "encounterId") Encounter associatedEncounter,
	        @RequestParam(value = "dateOfLastTest", required = false) String dateOfLastTest,
	        @RequestParam(value = "orderingSiteId", required = false) Location orderingSite,
	        @RequestParam(value = "requestedBy", required = false) String requestedBy) {
		LabOrderService labOrderService = Context.getRegisteredComponent("botswana.emr.labOrderService",
		    LabOrderService.class);
		EncounterService encounterService = Context.getEncounterService();
		VisitService visitService = Context.getVisitService();
		//get active visit for this current patient
		List<Visit> currentVisits = visitService.getActiveVisitsByPatient(patient);
		String[] labListArray = labList.split(",");
		List<String> allTestsId = new ArrayList<>(Arrays.asList(labListArray));
		List<Concept> conceptsToAssociateWithOrder = new ArrayList<>();
		//Create an encounter
		Encounter labEncounter = BotswanaEmrUtils.createEncounter(patient,
		    BotswanaEmrUtils.getEncounterType(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE), location, null);
		
		Obs obs = BotswanaEmrUtils.creatObs(labEncounter,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.TEST_ORDERED_CONCEPT_UUID));
		//get all encounters for the active visit
		Visit requiredVisit;
		Set<Encounter> visitEncounterSet = null;
		if (!currentVisits.isEmpty()) {
			requiredVisit = currentVisits.get(currentVisits.size() - 1);
			visitEncounterSet = new HashSet<>(requiredVisit.getEncounters());
		}
		Set<Obs> allObsForEncounters = new HashSet<>();
		List<Encounter> filteredEncountersPerEncounterType = new ArrayList<>();
		EncounterType labOrderEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.LAB_ORDER_ENCOUNTER_TYPE_UUID);
		if (visitEncounterSet != null && !visitEncounterSet.isEmpty()) {
			for (Encounter encounter : visitEncounterSet) {
				if (encounter.getEncounterType().equals(labOrderEncounterType)) {
					filteredEncountersPerEncounterType.add(encounter);
				}
			}
		}
		//Now check through the filtered encounters and fetch all the obs into a set
		if (!filteredEncountersPerEncounterType.isEmpty()) {
			for (Encounter encounter : filteredEncountersPerEncounterType) {
				allObsForEncounters.addAll(encounter.getAllObs());
			}
		}
		//Loop through the obs and only pick those that have the test ordered questions
		List<Concept> conceptsFoundInAlreadySavedObs = new ArrayList<>();
		if (!allObsForEncounters.isEmpty()) {
			for (Obs obs1 : allObsForEncounters) {
				if (obs1.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.TEST_ORDERED_CONCEPT_UUID))) {
					conceptsFoundInAlreadySavedObs.add(obs1.getValueCoded());
				}
			}
		}
		Set<Obs> obsSet = new HashSet<>();
		if (!allTestsId.isEmpty()) {
			for (String concept : allTestsId) {
				if (!(conceptsFoundInAlreadySavedObs.contains(BotswanaEmrUtils.getConcept(concept)))) {
					obs.setValueCoded(BotswanaEmrUtils.getConcept(concept));
					obsSet.add(obs);
					//add this concepts to a list that can be reused
					conceptsToAssociateWithOrder.add(BotswanaEmrUtils.getConcept(concept));
				}
			}
		}
		if (!obsSet.isEmpty()) {
			labEncounter.setObs(obsSet);
			//save the encounter
			encounterService.saveEncounter(labEncounter);
			
			if (StringUtils.isNotEmpty(requestedBy)) {
				Concept requestedByConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.REMARKS_CONCEPT_UUID);
				Obs requestedByObs = BotswanaEmrUtils.creatObs(labEncounter, requestedByConcept);
				requestedByObs.setValueText(requestedBy);
				requestedByObs.setValueDatetime(new Date());
				requestedByObs.setComment("Requested By");
				requestedByObs.setValueCoded(requestedByConcept);
				obsService.saveObs(requestedByObs, "Requested By");
				labEncounter.addObs(requestedByObs);
			}
			
			if (StringUtils.isNotEmpty(dateOfLastTest)) {
				try {
					Date dateFromString = BotswanaEmrUtils.formatDateFromString(dateOfLastTest, "yyyy-MM-dd");
					Concept dateOfLastTestConcept = BotswanaEmrUtils.getConcept("162078AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
					Obs dateOfLastTestObs = BotswanaEmrUtils.creatObs(labEncounter, dateOfLastTestConcept);
					dateOfLastTestObs.setValueText(dateOfLastTest);
					dateOfLastTestObs.setDateCreated(new Date());
					dateOfLastTestObs.setValueDatetime(dateFromString);
					dateOfLastTestObs.setComment("Date Of Last Test");
					dateOfLastTestObs.setValueCoded(dateOfLastTestConcept);
					obsService.saveObs(dateOfLastTestObs, "Date Of Last Test");
					labEncounter.addObs(dateOfLastTestObs);
				}
				catch (ParseException e) {
					log.info(e.getMessage());
				}
			}
			
			// Add the ordering site to the encounter if it is not null as a value text
			if (orderingSite != null) {
				Concept orderingSiteConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ORDERING_SITE_CONCEPT_UUID);
				Obs orderingSiteObs = BotswanaEmrUtils.creatObs(labEncounter, orderingSiteConcept);
				orderingSiteObs.setValueText(orderingSite.getUuid());
				orderingSiteObs.setComment("Ordering Site");
				obsService.saveObs(orderingSiteObs, "Ordering Site");
			}
			
			if (associatedEncounter == null) {
				associatedEncounter = labEncounter;
			}
			// Associate the lab order encounter with the associated encounter
			if (associatedEncounter != null) {
				Concept encounterNoteConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.ENCOUNTER_NOTE_CONCEPT_UUID);
				Obs encounterNoteObs = BotswanaEmrUtils.creatObs(labEncounter, encounterNoteConcept);
				encounterNoteObs.setValueText(associatedEncounter.getUuid());
				encounterNoteObs.setComment("Associated Encounter");
				obsService.saveObs(encounterNoteObs, "Associated Encounter");
			}
			
			//add order here
			Set<Order> orderSet = labOrderService.createOrderByEncounterAndListOfConcepts(labEncounter,
			    conceptsToAssociateWithOrder);
			//add the lab test order
			labOrderService.createLabTestsFromOrdersSet(orderSet);
		}
	}
	
	private List<SimplifiedLabTest> simplifiedLabTestList(List<LabTest> labTests, LabService labService) {
		List<SimplifiedLabTest> allTests = new ArrayList<>();
		SimplifiedLabTest simplifiedLabTest;
		if (labTests != null && labTests.size() > 0) {
			for (LabTest labTest : labTests) {
				simplifiedLabTest = new SimplifiedLabTest();
				simplifiedLabTest.setTestName(labTest.getConcept().getDisplayString());
				simplifiedLabTest.setStatus(labTest.getStatus());
				simplifiedLabTest
				        .setDateAdded(BotswanaEmrUtils.formatDateWithoutTime(labTest.getAcceptDate(), "yyyy-MM-dd HH:mm"));
				simplifiedLabTest.setResultsList(BotswanaEmrUtils.getLabResultsKeyMap(
				    BotswanaEmrUtils.getLabOrderResultsIfCompleteSet(labTest.getLabTestId(), labService)));
				//add the test to the results list
				allTests.add(simplifiedLabTest);
			}
		}
		return allTests;
	}
	
	public List<SimplifiedLabTest> getLabTestsForPatient(@RequestParam(value = "patientId") Integer patientId,
	        UiSessionContext uiSessionContext, @SpringBean("labService") LabService labService,
	        @SpringBean("visitService") VisitService visitService, @SpringBean("patientService") PatientService patientService) {
		
		EncounterType labOrderEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE);
		
		Patient patient = patientService.getPatient(patientId);
		
		List<SimplifiedLabTest> labTests = new ArrayList<>();
		Visit currentVisit;
		Set<Encounter> allEncountersForThisVisit = null;
		List<Encounter> resultantEncounters = new ArrayList<>();
		
		List<Visit> currentVisitForThePatient = visitService.getActiveVisitsByPatient(patient);
		if (currentVisitForThePatient != null && !currentVisitForThePatient.isEmpty()) {
			currentVisit = currentVisitForThePatient.get(currentVisitForThePatient.size() - 1);
			if (currentVisit != null) {
				allEncountersForThisVisit = new HashSet<>(currentVisit.getEncounters());
			}
		}
		
		if (allEncountersForThisVisit != null && !allEncountersForThisVisit.isEmpty()) {
			for (Encounter encounter : allEncountersForThisVisit) {
				if (encounter.getEncounterType().equals(labOrderEncounterType)) {
					resultantEncounters.add(encounter);
				}
			}
		}
		
		String facility = Context.getAuthenticatedUser()
		        .getUserProperty(BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY);
		Location location = Context.getLocationService().getLocation(facility);
		EncounterType labEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE);
		
		List<Encounter> encounters = Context.getService(BotswanaEmrService.class)
		        .getEncountersList(new Date(), new Date(), labEncounterType, null, location).stream()
		        .filter(encounter -> encounter.getPatient().equals(patient))
		        .filter(encounter -> !encounter.getOrders().isEmpty()).collect(Collectors.toList());
		
		if (!encounters.isEmpty()) {
			for (Encounter encounter : encounters) {
				labTests.addAll(simplifiedLabTestList(labService.getLaboratoryTestListByEncounter(encounter), labService));
			}
		}
		
		return labTests;
	}
}
