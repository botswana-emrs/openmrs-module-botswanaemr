/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.util.List;

import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class StockQuantityFragmentController {
	
	public void controller(@SpringBean FragmentModel fragmentModel,
	        @SpringBean("bemrs.itemStockDataService") IItemStockDataService iItemStockDataService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService, UiUtils ui,
	        @FragmentParam(value = "id", required = true) String itemId,
	        @FragmentParam(value = "stockroomId", required = true) String stockroomId, UiSessionContext uiSessionContext) {
		List<ItemStock> itemStock = iItemStockDataService
		        .getItemStockByItem(iItemDataService.getById(Integer.valueOf(itemId)), null);
		int quantity = 0;
		for (ItemStock itemStock2 : itemStock) {
			if (itemStock2.getStockroom() == iStockroomDataService.getById(Integer.valueOf(stockroomId))) {
				quantity += itemStock2.getQuantity();
			}
		}
		fragmentModel.addAttribute("quantity", quantity);
	}
}
