/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin.provider;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.admin.ServiceProviderFragmentController;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class EditServiceProviderFragmentController extends ServiceProviderFragmentController {
	
	public void get(PageModel model, @FragmentParam(value = "providerId", required = false) String providerId) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		ServiceProvider provider = service.getServiceProvider(Integer.valueOf(providerId));
		if (provider == null) {
			provider = new ServiceProvider();
		}
		model.addAttribute("provider", provider);
	}
	
	public SimpleObject getServiceProvider(FragmentModel model, @RequestParam(value = "providerId") Integer providerId) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		ServiceProvider provider = service.getServiceProvider(providerId);
		if (provider == null) {
			provider = new ServiceProvider();
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("id", provider.getServiceProviderId());
		simpleObject.put("name", provider.getName());
		simpleObject.put("description", provider.getDescription());
		return simpleObject;
	}
	
	public void editServiceProvider(@RequestParam(value = "editProviderId") Integer providerId,
	        @RequestParam(value = "editProviderName") String name,
	        @RequestParam(value = "editDescription") String description, UiUtils uiUtils) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		ServiceProvider serviceProvider = service.getServiceProvider(providerId);
		if (serviceProvider != null && StringUtils.isNotEmpty(name)) {
			serviceProvider.setName(name);
			serviceProvider.setDescription(description);
			
			//save the service provider
			service.saveServiceProvider(serviceProvider);
		}
		
	}
	
	public String voidServiceProvider(@RequestParam(value = "providerId") Integer providerId) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		ServiceProvider serviceProvider = service.getServiceProvider(providerId);
		if (serviceProvider != null) {
			serviceProvider.setRetired(true);
			serviceProvider.setRetireReason("deleted");
			serviceProvider.setDateRetired(new Date());
			
			//save the payment method
			service.saveServiceProvider(serviceProvider);
		}
		
		return null;
	}
	
}
