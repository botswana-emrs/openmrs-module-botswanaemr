/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.apache.commons.lang.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.RegistrationDetailSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

public class DelayedPaymentFragmentController extends PaymentHistoryFragmentController {
	
	private final BotswanaEmrService service;
	
	public DelayedPaymentFragmentController() {
		service = Context.getService(BotswanaEmrService.class);
	}
	
	public void get(FragmentModel model, UiSessionContext sessionContext) {
		//TODO review the filter for what to include in the delayed payments list
		if (sessionContext.getCurrentUser() != null) {
			model.addAttribute("allDelayedPayments",
			    getSimplifiedDate(null,
			        sessionContext.getSessionLocation())
			                .stream()
			                .filter(paymentDetailSimplifier -> Double.parseDouble(paymentDetailSimplifier
			                        .getAmountPaid()) < Double.parseDouble(paymentDetailSimplifier.getBillableAmount()))
			                .collect(Collectors.toList()));
		} else {
			model.addAttribute("allDelayedPayments", new ArrayList<RegistrationDetailSimplifier>());
		}
	}
	
	public void updatePayment(@RequestParam(value = "registrationId") int registrationId,
	        @RequestParam(value = "paymentMethodId") int paymentMethodId,
	        @RequestParam(value = "paymentDate") String paymentDate, @RequestParam(value = "amountPaid") Double amountPaid)
	        throws ParseException {
		
		Registration payment = service.getPayment(registrationId);
		payment.setAmountPaid(amountPaid);
		PaymentMethod method = service.getAllPaymentMethods(false).stream()
		        .filter(p -> Objects.equals(p.getPaymentMethodId(), paymentMethodId)).collect(Collectors.toList()).get(0);
		payment.setPaymentMethod(method);
		if (StringUtils.isNotEmpty(paymentDate)) {
			payment.setRegistrationDatetime(BotswanaEmrUtils.formatDateFromString(paymentDate, "yyyy-MM-dd"));
		} else {
			payment.setRegistrationDatetime(new Date());
		}
		
		service.savePayment(payment);
	}
	
	public SimpleObject getRebateAmount(FragmentModel model,
	        @RequestParam(value = "registrationId") Integer registrationId) {
		SimpleObject simpleObject = new SimpleObject();
		Registration payment = service.getPayment(registrationId);
		simpleObject.put("registrationId", payment.getRegistrationId());
		simpleObject.put("billableAmount", payment.getBillableAmount());
		simpleObject.put("amountPaid", payment.getAmountPaid());
		
		return simpleObject;
	}
}
