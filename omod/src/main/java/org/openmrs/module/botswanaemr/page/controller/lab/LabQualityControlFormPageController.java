/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.lab;

import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class LabQualityControlFormPageController {
	
	public void controller(@RequestParam(value = "labQualityControlId", required = false) Integer labQualityControlId,
	        PageModel pageModel, UiSessionContext uiSessionContext) {
		HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
		LabQualityControl labQualityControl = null;
		
		if (labQualityControlId != null && uiSessionContext.getCurrentUser() != null) {
			labQualityControl = htsLabControlService.getLabQualityControl(labQualityControlId);
			
			pageModel.addAttribute("labQualityControlId", labQualityControl.getId());
			pageModel.addAttribute("labQualityControlDateTested",
			    BotswanaEmrUtils.formatDateWithoutTime(labQualityControl.getDateTested(), "dd-MMM-yyyy"));
		}
		
		pageModel.addAttribute("labQualityControl", labQualityControl);
	}
}
