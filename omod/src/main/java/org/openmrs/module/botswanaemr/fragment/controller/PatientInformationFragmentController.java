/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.Person;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.uicommons.UiCommonsConstants;
import org.openmrs.module.uicommons.util.InfoErrorMessageUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.BindParams;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.session.Session;
import org.openmrs.validator.PatientValidator;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

public class PatientInformationFragmentController {
	
	protected final Log log = LogFactory.getLog(PatientInformationFragmentController.class);
	
	public void get(FragmentModel fragmentModel, UiSessionContext sessionContext) {
		boolean isBf = sessionContext.getSessionLocation().getTags().stream()
		        .anyMatch(locationTag -> locationTag.getName().equals(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME));
		fragmentModel.addAttribute("isBf", isBf);
	}
	
	public String post() {
		return null;
	}
	
	public String editPatientDetails(@RequestParam(value = "patientId", required = false) @BindParams Patient patient,
	        @RequestParam(value = "idType", required = false) String idType,
	        @RequestParam(value = "citizenType", required = false) String citizenType,
	        @RequestParam(value = "idNumber", required = false) String idNumber,
	        @RequestParam(value = "givenName") String givenName,
	        @RequestParam(value = "middleName", required = false) String middleName,
	        @RequestParam(value = "familyName") String familyName, @RequestParam(value = "gender") String gender,
	        @RequestParam(value = "dateOfBirth", required = false) String dateOfBirth,
	        @RequestParam(value = "age", required = false) Integer age,
	        @RequestParam(value = "dobEstimate", required = false) String dobEstimate,
	        @RequestParam(value = "country", required = false) String country,
	        @RequestParam(value = "address2") String address2, @RequestParam(value = "cityVillage") String cityVillage,
	        @RequestParam(value = "address4", required = false) String address4,
	        @RequestParam(value = "address5", required = false) String address5,
	        @RequestParam(value = "address6", required = false) String address6,
	        @RequestParam(value = "address7", required = false) String address7,
	        @RequestParam(value = "email", required = false) String email,
	        @RequestParam(value = "maritalStatus", required = false) String maritalStatus,
	        @RequestParam(value = "occupation", required = false) String occupation,
	        @RequestParam(value = "affiliation", required = false) String affiliation,
	        @RequestParam(value = "forceNumber", required = false) String forceNumber,
	        @RequestParam(value = "rank", required = false) String rank,
	        @RequestParam(value = "education", required = false) String education,
	        @RequestParam(value = "otherOccupation", required = false) String otherOccupation,
	        @RequestParam(value = "nameOfEmployer", required = false) String nameOfEmployer,
	        @RequestParam(value = "employmentSector", required = false) String employmentSector,
	        @RequestParam(value = "otherEmploymentSector", required = false) String otherEmploymentSector,
	        @RequestParam(value = "telephoneNumber", required = false) String telephoneNumber,
	        @RequestParam(value = "phoneNumberCode", required = false) String phoneNumberCode,
	        @RequestParam(value = "facilityLocationField") String facilityLocation,
	        @RequestParam(value = "phoneNumber1", required = false) String phoneNumber1,
	        @RequestParam(value = "phoneNumber1Code", required = false) String phoneNumber1Code,
	        @RequestParam(value = "phoneNumber2", required = false) String phoneNumber2,
	        @RequestParam(value = "phoneNumber2Code", required = false) String phoneNumber2Code,
	        @RequestParam(value = "phoneNumber3", required = false) String phoneNumber3,
	        @RequestParam(value = "phoneNumber3Code", required = false) String phoneNumber3Code,
	        @RequestParam(value = "phoneNumber4", required = false) String phoneNumber4,
	        @RequestParam(value = "phoneNumber4Code", required = false) String phoneNumber4Code,
	        @RequestParam(value = "phoneNumber5", required = false) String phoneNumber5,
	        @RequestParam(value = "phoneNumber5Code", required = false) String phoneNumber5Code,
	        @RequestParam(value = "numberType", required = false) String numberType,
	        @RequestParam(value = "extraNumberType1", required = false) String extraNumberType1,
	        @RequestParam(value = "extraNumberType2", required = false) String extraNumberType2,
	        @RequestParam(value = "extraNumberType3", required = false) String extraNumberType3,
	        @RequestParam(value = "extraNumberType4", required = false) String extraNumberType4,
	        @SpringBean("patientService") PatientService patientService, HttpServletRequest request, Session session,
	        UiUtils ui, @SpringBean("patientValidator") PatientValidator patientValidator) throws ParseException {
		// TODO: Handle for deletes
		
		Person person = patientService.getPatient(patient.getId()).getPerson();
		
		BindingResult errors = new BeanPropertyBindingResult(patient, "patient");
		patientValidator.validate(patient, errors);
		
		boolean birthEstimated = false;
		
		if (StringUtils.isNotEmpty(dobEstimate)) {
			birthEstimated = dobEstimate.toLowerCase().equals("on");
		}
		
		PersonName currentName = person.getPersonName();
		PersonName desiredName = new PersonName();
		if (StringUtils.isNotBlank(familyName)) {
			desiredName.setFamilyName(familyName);
		}
		if (StringUtils.isNotBlank(givenName)) {
			desiredName.setGivenName(givenName);
		}
		if (StringUtils.isNotBlank(middleName)) {
			desiredName.setMiddleName(middleName);
		}
		if (!currentName.equalsContent(desiredName)) {
			patient.addName(desiredName);
			currentName.setVoided(true);
		}
		
		if (gender.equalsIgnoreCase("Male")) {
			patient.setGender(MALE_GENDER);
		} else if (gender.equalsIgnoreCase("Female")) {
			patient.setGender(FEMALE_GENDER);
		} else {
			patient.setGender(OTHER_GENDER);
		}
		
		if (StringUtils.isNotBlank(dateOfBirth)) {
			Date birthDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(dateOfBirth);
			patient.setBirthdate(birthDate);
			patient.setBirthdateEstimated(birthEstimated);
		} else {
			patient.setBirthdateFromAge(age, new Date());
			patient.setBirthdateEstimated(birthEstimated);
		}
		
		if (StringUtils.isNotEmpty(telephoneNumber)) {
			telephoneNumber = phoneNumberCode + " " + telephoneNumber;
		}
		if (StringUtils.isNotEmpty(phoneNumber1)) {
			phoneNumber1 = phoneNumber1Code + " " + phoneNumber1;
		}
		if (StringUtils.isNotEmpty(phoneNumber2)) {
			phoneNumber2 = phoneNumber2Code + " " + phoneNumber2;
		}
		if (StringUtils.isNotEmpty(phoneNumber3)) {
			phoneNumber3 = phoneNumber3Code + " " + phoneNumber3;
		}
		if (StringUtils.isNotEmpty(phoneNumber4)) {
			phoneNumber4 = phoneNumber4Code + " " + phoneNumber4;
		}
		if (StringUtils.isNotEmpty(phoneNumber5)) {
			phoneNumber5 = phoneNumber5Code + " " + phoneNumber5;
		}
		
		switch (idType) {
			case "nationalIdNumber":
				idType = NATIONAL_ID_IDENTIFIER_TYPE;
				break;
			case "passportNumber":
				idType = PASSPORT_IDENTIFIER_TYPE;
				break;
			case "birthCertificateNumber":
				idType = BIRTH_CERT_IDENTIFIER_TYPE;
				break;
			case "noIdentification":
				idType = TEMPORARY_ID_IDENTIFIER_TYPE_UUID;
				break;
			default:
				idType = OPENMRS_ID_IDENTIFIER_TYPE;
		}
		PatientIdentifierType identifierType = Context.getPatientService().getPatientIdentifierTypeByUuid(idType);
		//Fetches existing patient identifiers such as NIN, PN or BCN for editing.
		PatientIdentifier patientIdentifier = patient.getPatientIdentifier(identifierType);
		if (patientIdentifier == null) {
			//Fetches the default OpenMRS ID in case no other ID types are found to avoid (NPE).
			patientIdentifier = patient.getPatientIdentifier();
		}
		final String identifierTypeName = patientIdentifier.getIdentifierType().getName();
		if (StringUtils.isNotBlank(identifierTypeName) && identifierTypeName.equals(TEMPORARY_ID_IDENTIFIER_NAME)) {
			
		} else if (!StringUtils.isNotBlank(identifierTypeName)
		        || identifierTypeName.equals(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME)) {
			Set<PatientIdentifier> patientIdentifiers = patient.getIdentifiers();
			PatientIdentifier identifier = new PatientIdentifier();
			identifier.setIdentifierType(identifierType);
			identifier.setIdentifier(idNumber);
			identifier.setPreferred(true);
			patientIdentifiers.add(identifier);
			patient.setIdentifiers(patientIdentifiers);
		} else if (StringUtils.isNotBlank(idNumber)) {
			patientIdentifier.setIdentifierType(identifierType);
			patientIdentifier.setIdentifier(idNumber);
			patientIdentifier.setPreferred(true);
			patient.addIdentifier(patientIdentifier);
		}
		PersonAddress personAddress = person.getPersonAddress();
		
		//Only set a new personAddress if the personAddress is blank or null
		if (personAddress == null || personAddress.isBlank()) {
			personAddress = new PersonAddress();
		}
		personAddress.setCountry(country);
		personAddress.setAddress2(address2);
		personAddress.setCityVillage(cityVillage);
		personAddress.setAddress4(address4);
		personAddress.setAddress5(address5);
		personAddress.setAddress6(address6);
		personAddress.setAddress7(address7);
		patient.addAddress(personAddress);
		
		Set<PersonAttribute> personAttributes = person.getAttributes();
		
		HashMap<String, Boolean> tracker = new HashMap<>();
		if (personAttributes == null || personAttributes.isEmpty()) {
			createPersonAttribute(patient, citizenType, CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, email, EMAIL_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, occupation, OCCUPATION_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, affiliation, PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, forceNumber, FORCE_NUMBER_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, rank, RANK_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, otherOccupation, OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nameOfEmployer, EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, education, EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, telephoneNumber, PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, phoneNumber1, ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, phoneNumber2, ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, phoneNumber3, ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, phoneNumber4, ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, phoneNumber5, ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, maritalStatus, MARITAL_STATUS_ATTRIBUTE_TYPE);
			createPersonAttribute(patient, employmentSector, EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE);
			createPersonAttribute(patient, otherEmploymentSector, OTHER_EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE);
			createPersonAttribute(patient, numberType, NUMBER_TYPE_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, extraNumberType1, ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, extraNumberType2, ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, extraNumberType3, ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, extraNumberType4, ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID);
		} else {
			for (PersonAttribute personAttribute : personAttributes) {
				if (getPersonAttributeTypeUuid(personAttribute).equals(CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(citizenType);
					tracker.put("citizenType", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(EMAIL_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(email);
					tracker.put("email", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(OCCUPATION_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(occupation);
					tracker.put("occupation", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(affiliation);
					tracker.put("affiliation", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(FORCE_NUMBER_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(forceNumber);
					tracker.put("forceNumber", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(RANK_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(rank);
					tracker.put("rank", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(otherOccupation);
					tracker.put("otherOccupation", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(education);
					tracker.put("education", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(nameOfEmployer);
					tracker.put("employer_name", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(PHONE_NUMBER_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(telephoneNumber);
					tracker.put("phone_number", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(PERSON_LOCATION_ATTRIBUTE)) {
					personAttribute.setValue(facilityLocation);
					tracker.put("LocationAttribute", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(phoneNumber1);
					tracker.put("phoneNumber1", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(phoneNumber2);
					tracker.put("phoneNumber2", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(phoneNumber3);
					tracker.put("phoneNumber3", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(phoneNumber4);
					tracker.put("phoneNumber4", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(phoneNumber5);
					tracker.put("phoneNumber5", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(MARITAL_STATUS_ATTRIBUTE_TYPE)) {
					personAttribute.setValue(maritalStatus);
					tracker.put("maritalStatus", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE)) {
					personAttribute.setValue(employmentSector);
					tracker.put("employmentSector", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(OTHER_EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE)) {
					personAttribute.setValue(otherEmploymentSector);
					tracker.put("otherEmploymentSector", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(NUMBER_TYPE_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(numberType);
					tracker.put("numberType", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(extraNumberType1);
					tracker.put("extraNumberType1", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(extraNumberType2);
					tracker.put("extraNumberType2", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(extraNumberType3);
					tracker.put("extraNumberType3", true);
				} else if (getPersonAttributeTypeUuid(personAttribute).equals(ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID)) {
					personAttribute.setValue(extraNumberType4);
					tracker.put("extraNumberType4", true);
				} else {
					if (!StringUtils.isNotBlank(getPersonAttributeTypeUuid(personAttribute))) {
						createPersonAttribute(patient, personAttribute.getValue(), personAttribute.getUuid());
					}
				}
			}
			if (!tracker.containsKey("citizenType")) {
				createPersonAttribute(patient, citizenType, CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("email")) {
				createPersonAttribute(patient, email, EMAIL_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("occupation")) {
				createPersonAttribute(patient, occupation, OCCUPATION_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("affiliation")) {
				createPersonAttribute(patient, affiliation, PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("forceNumber")) {
				createPersonAttribute(patient, forceNumber, FORCE_NUMBER_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("rank")) {
				createPersonAttribute(patient, rank, RANK_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("education")) {
				createPersonAttribute(patient, education, EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("otherOccupation")) {
				createPersonAttribute(patient, otherOccupation, OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("employer_name")) {
				createPersonAttribute(patient, nameOfEmployer, EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phone_number")) {
				createPersonAttribute(patient, telephoneNumber, PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phoneNumber1")) {
				createPersonAttribute(patient, phoneNumber1, ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phoneNumber2")) {
				createPersonAttribute(patient, phoneNumber2, ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phoneNumber3")) {
				createPersonAttribute(patient, phoneNumber3, ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phoneNumber4")) {
				createPersonAttribute(patient, phoneNumber4, ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("phoneNumber5")) {
				createPersonAttribute(patient, phoneNumber5, ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("LocationAttribute")) {
				createPersonAttribute(patient, facilityLocation, PERSON_LOCATION_ATTRIBUTE);
			}
			if (!tracker.containsKey("maritalStatus")) {
				createPersonAttribute(patient, maritalStatus, MARITAL_STATUS_ATTRIBUTE_TYPE);
			}
			if (!tracker.containsKey("employmentSector")) {
				createPersonAttribute(patient, employmentSector, EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE);
			}
			if (!tracker.containsKey("otherEmploymentSector")) {
				createPersonAttribute(patient, employmentSector, OTHER_EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE);
			}
			if (!tracker.containsKey("numberType")) {
				createPersonAttribute(patient, numberType, NUMBER_TYPE_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("extraNumberType1")) {
				createPersonAttribute(patient, extraNumberType1, ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("extraNumberType2")) {
				createPersonAttribute(patient, extraNumberType2, ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("extraNumberType3")) {
				createPersonAttribute(patient, extraNumberType3, ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID);
			}
			if (!tracker.containsKey("extraNumberType4")) {
				createPersonAttribute(patient, extraNumberType4, ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID);
			}
			patient.setAttributes(personAttributes);
		}
		
		if (!errors.hasErrors()) {
			try {
				patientService.savePatient(patient);
				InfoErrorMessageUtil.flashInfoMessage(request.getSession(), ui.message("Details saved successfully",
				    patient.getPersonName() != null ? ui.encodeHtml(patient.getPersonName().toString()) : ""));
				return null;
			}
			catch (Exception e) {
				log.warn("\nError occurred while saving patient's details\n", e);
				session.setAttribute(UiCommonsConstants.SESSION_ATTRIBUTE_ERROR_MESSAGE, "registrationapp.save.fail");
			}
			
		} else {
			log.warn("Error occurred while saving patient's details");
		}
		return null;
	}
	
	//Create new Attribute
	private void createPersonAttribute(Patient patient, String attributeValue, String attributeUuid) {
		PersonAttribute attribute = setPersonAttributeValue(attributeValue, attributeUuid);
		patient.addAttribute(attribute);
	}
	
	//Returns a PersonAttributeType uuid given a personAttribute
	private String getPersonAttributeTypeUuid(PersonAttribute personAttribute) {
		return personAttribute.getAttributeType().getUuid();
	}
	
	// Only used to set new person Attributes when the
	// Attributes <Set> is Empty: Emergency patient UseCase
	private PersonAttribute setPersonAttributeValue(String personAttributeValue, String personAttributeTypeUuid) {
		PersonAttributeType personAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(personAttributeTypeUuid);
		PersonAttribute personAttribute = new PersonAttribute();
		personAttribute.setAttributeType(personAttributeType);
		personAttribute.setValue(personAttributeValue);
		return personAttribute;
	}
	
}
