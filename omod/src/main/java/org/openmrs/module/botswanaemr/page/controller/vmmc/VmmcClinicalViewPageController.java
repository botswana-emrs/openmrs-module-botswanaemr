/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.vmmc;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.context.AppContextModel;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.SearchFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.contextmodel.PatientContextModel;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getVmmcEncounterForms;

public class VmmcClinicalViewPageController {
	
	private static final String HYPOSPADIAS_CONCEPT_UUID = "db3f6b4c-5b69-40e1-8650-7af5fa9334ab";
	
	private static final String WASTING_CONCEPT_UUID = "c7910ccd-7147-4790-b10d-095a685b8a46";
	
	public static final String SCREENING_OUTCOME_CONCEPT_UUID = "166664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String VALIDATE_VMMC_SCREENING_OUTCOME_GP = "botswanaemr.validateVmmcScreeningOutcome";
	
	public void controller(PageModel model, @RequestParam("patientId") Patient patient,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, UiUtils ui, UiSessionContext sessionContext) {
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		SearchFragmentController searchFragmentController = new SearchFragmentController();
		AppContextModel contextModel = sessionContext.generateAppContextModel();
		contextModel.put("patient", new PatientContextModel(patient));
		contextModel.put("patientId", patient.getUuid());
		contextModel.put("visit", currentPatientVisit);
		
		returnUrl = ui.encodeForSafeURL(
		    HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "vmmc/vmmcClinicalView", patient, visit, ui));
		
		Form vmmcInitialScreeningForm = formService.getFormByUuid(VMMC_INITIAL_SCREENING_FORM_UUID);
		Form vmmcOperationForm = formService.getFormByUuid(VMMC_OPERATION_FORM_UUID);
		Form vmmcPostOperationForm = formService.getFormByUuid(VMMC_POST_OPERATION_FORM_UUID);
		Form vmmcPostOperationAssessmentForm = formService.getFormByUuid(VMMC_POST_OPERATION_ASSESSMENT_FORM_UUID);
		Form vmmcConsentForm = formService.getFormByUuid(VMMC_CONSENT_FORM_UUID);
		Form vmmcCounsellingForm = formService.getFormByUuid(VMMC_COUNSELLING_FORM_UUID);
		Form vmmcBookingForm = formService.getFormByUuid(VMMC_BOOKING_FORM_UUID);
		
		List<HtmlForm> vmmcEncounterForms = getVmmcEncounterForms(htmlFormEntryService, formService);
		List<Form> allVmmcForms = vmmcEncounterForms.stream().map(HtmlForm::getForm).collect(Collectors.toList());
		List<Form> oneTimeForms = Arrays.asList(vmmcConsentForm, vmmcOperationForm, vmmcCounsellingForm, vmmcBookingForm);
		List<HtmlForm> preOperationForms = vmmcEncounterForms.stream()
		        .filter(f -> f.getForm().equals(vmmcInitialScreeningForm) || f.getForm().equals(vmmcBookingForm) ||
				// f.getForm().equals(vmmcConsentForm) ||
		                f.getForm().equals(vmmcCounsellingForm))
		        .collect(Collectors.toList());
		List<HtmlForm> postOperationForms = vmmcEncounterForms.stream().filter(f -> !preOperationForms.contains(f))
		        .collect(Collectors.toList());
		
		//		List<Encounter> filledEncounterForPatientPerTheCurrentVisit = encounterService.getEncounters(
		//				new EncounterSearchCriteria(patient, null, null, null, null,
		//						allVmmcForms, null, null,
		//						null, Collections.singletonList(currentPatientVisit), false));
		
		List<Encounter> filledEncounterForPatient = encounterService.getEncounters(
		    new EncounterSearchCriteria(patient, null, null, null, null, allVmmcForms, null, null, null, null, false));
		
		List<HtmlForm> tmpAvailableForms = vmmcEncounterForms;
		//		if (filledEncounterForPatient.isEmpty()) {
		//			tmpAvailableForms = vmmcEncounterForms;
		//		} else {
		//			List<Form> vmmcForms = filledEncounterForPatientPerTheCurrentVisit.stream().map(Encounter::getForm).collect(
		//					Collectors.toList());
		//			// Only the operation form is filled once, so remove it from the filter list
		//			if (!vmmcForms.isEmpty()) {
		//				vmmcForms = vmmcForms.stream().filter(f -> f.equals(vmmcOperationForm)).collect(Collectors.toList());
		//			}
		//
		//			List<Form> finalVmmcForms = vmmcForms;
		//			tmpAvailableForms = vmmcEncounterForms
		//					.stream()
		//					.filter(f -> !finalVmmcForms.contains(f.getForm()))
		//					.collect(Collectors.toList());
		//		}
		
		boolean filledOperationForm = false;
		boolean filledInitialScreeningForm = false;
		if (!filledEncounterForPatient.isEmpty()) {
			filledInitialScreeningForm = filledEncounterForPatient.stream()
			        .anyMatch(f -> f.getForm().equals(vmmcInitialScreeningForm));
			boolean filledConsentForm = filledEncounterForPatient.stream()
			        .anyMatch(f -> f.getForm().equals(vmmcConsentForm));
			boolean filledCounsellingForm = filledEncounterForPatient.stream()
			        .anyMatch(f -> f.getForm().equals(vmmcCounsellingForm));
			filledOperationForm = filledEncounterForPatient.stream().anyMatch(f -> f.getForm().equals(vmmcOperationForm));
		}
		
		boolean eligibleForVmmc = false;
		boolean hasDisagreedOrDeferred = false;
		
		EncounterType vmmcCounsellingEncounterType = BotswanaEmrUtils.getEncounterType(VMMC_COUNSELLING_ENCOUNTER_TYPE);
		Obs vmmcCounsellingOutcomeObs = BotswanaEmrUtils.getLatestObs(patient, vmmcCounsellingEncounterType,
		    BotswanaEmrUtils.getConcept(VMMC_COUNSELING_OUTCOME_CONCEPT_UUID));
		
		if (filledInitialScreeningForm) {
			EncounterType vmmcScreeningEncounterType = BotswanaEmrUtils
			        .getEncounterType(VMMC_INITIAL_SCREENING_ENCOUNTER_TYPE);
			
			Obs screeningOutcomeObs = BotswanaEmrUtils.getLatestObs(patient, vmmcScreeningEncounterType,
			    BotswanaEmrUtils.getConcept(SCREENING_OUTCOME_CONCEPT_UUID));
			
			Concept vmmcScreeningOutcomeValue = screeningOutcomeObs == null ? null : screeningOutcomeObs.getValueCoded();
			
			Concept vmmcCounsellingOutcomeValue = vmmcCounsellingOutcomeObs == null ? null
			        : vmmcCounsellingOutcomeObs.getValueCoded();
			
			Concept positiveVmmcScreeningConcept = BotswanaEmrUtils.getConcept(POSITIVE);
			
			Concept disagreedConcept = BotswanaEmrUtils.getConcept(DISAGREED_CONCEPT_UUID);
			
			Concept deferredConcept = BotswanaEmrUtils.getConcept(DEFERRED_CONCEPT_UUID);
			
			eligibleForVmmc = vmmcScreeningOutcomeValue != null
			        && vmmcScreeningOutcomeValue.equals(positiveVmmcScreeningConcept);
			
			hasDisagreedOrDeferred = vmmcCounsellingOutcomeValue != null
			        && (vmmcCounsellingOutcomeValue.equals(disagreedConcept)
			                || vmmcCounsellingOutcomeValue.equals(deferredConcept));
			
			boolean validateVmmcScreeningOutcome = Boolean.parseBoolean(Context.getAdministrationService()
			        .getGlobalProperty(VALIDATE_VMMC_SCREENING_OUTCOME_GP, String.valueOf(true)));
			
			if (validateVmmcScreeningOutcome) {
				if (!eligibleForVmmc) {
					// if negative screening outcome i.e. ineligible for circumcision, avail pre-operation forms
					tmpAvailableForms = preOperationForms;
				} else {
					// If Initial screeening is positive, hide the screening form.
					tmpAvailableForms = tmpAvailableForms.stream().filter(f -> !f.getForm().equals(vmmcInitialScreeningForm))
					        .collect(Collectors.toList());
				}
				
				if (hasDisagreedOrDeferred) {
					tmpAvailableForms = preOperationForms;
				}
			}
		} else {
			tmpAvailableForms = preOperationForms;
		}
		
		if (!filledEncounterForPatient.isEmpty()) {
			// Remove one-time forms(consent, counseling, booking, operation) if already filled
			List<Form> filledForms = filledEncounterForPatient.stream().map(Encounter::getForm).distinct()
			        .collect(Collectors.toList());
			List<Form> filledOneTimeForms = filledForms.stream().filter(oneTimeForms::contains).collect(Collectors.toList());
			
			tmpAvailableForms = tmpAvailableForms.stream().filter(f -> !filledOneTimeForms.contains(f.getForm()))
			        .collect(Collectors.toList());
		}
		
		if (filledOperationForm) {
			HtmlForm tempOperationForm = vmmcEncounterForms.stream().filter(f -> f.getForm().equals(vmmcOperationForm))
			        .findFirst().orElse(null);
			// If operation form has been filled, remove operation form from the available forms
			tmpAvailableForms = tmpAvailableForms.stream().filter(f -> !f.equals(tempOperationForm))
			        .collect(Collectors.toList());
		} else {
			// if operation form has not been filled, remove post operation form and post operation assessment forms
			HtmlForm tempPostOperationForm = postOperationForms.stream()
			        .filter(f -> f.getForm().equals(vmmcPostOperationForm)).findFirst().orElse(null);
			HtmlForm tempPostOperationAssessmentForm = postOperationForms.stream()
			        .filter(f -> f.getForm().equals(vmmcPostOperationAssessmentForm)).findFirst().orElse(null);
			tmpAvailableForms = tmpAvailableForms.stream()
			        .filter(f -> !f.equals(tempPostOperationForm) && !f.equals(tempPostOperationAssessmentForm))
			        .collect(Collectors.toList());
		}
		
		// Iteratively arrange the forms in the order they should be filled in this order
		// Counselling firm, Initial screening form, Booking form, consent form, operation form, post operation assessment form,post operation reviews form
		List<HtmlForm> arrangedForms;
		if (tmpAvailableForms.size() > 1) {
			arrangedForms = new ArrayList<>();
			List<String> formUuids = Arrays.asList(VMMC_COUNSELLING_FORM_UUID, VMMC_BOOKING_FORM_UUID,
			    VMMC_INITIAL_SCREENING_FORM_UUID, VMMC_OPERATION_FORM_UUID, VMMC_POST_OPERATION_FORM_UUID,
			    VMMC_POST_OPERATION_ASSESSMENT_FORM_UUID);
			for (String formUuid : formUuids) {
				for (HtmlForm form : tmpAvailableForms) {
					if (form.getForm().getUuid().equals(formUuid)) {
						arrangedForms.add(form);
					}
				}
			}
		} else {
			arrangedForms = tmpAvailableForms;
		}
		Concept screeningHardStopConditionsConcept = BotswanaEmrUtils.getConcept("167141AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		Obs latestObs = BotswanaEmrUtils.getLatestObs(patient,
		    Context.getEncounterService().getEncounterTypeByUuid(VMMC_INITIAL_SCREENING_ENCOUNTER_TYPE),
		    screeningHardStopConditionsConcept);
		String screeningHardStopConditions = latestObs == null ? "" : latestObs.getValueText();
		
		String vmmcCounsellingOutcomeLastestObs = vmmcCounsellingOutcomeObs == null ? ""
		        : vmmcCounsellingOutcomeObs.getValueCoded().getDisplayString();
		model.addAttribute("vmmcEncounters", filledEncounterForPatient);
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("hasVisit", currentPatientVisit != null);
		model.addAttribute("availableVmmcForms", arrangedForms);
		model.addAttribute("filledInitialScreeningForm", filledInitialScreeningForm);
		model.addAttribute("eligibleForVmmc", eligibleForVmmc);
		model.addAttribute("hasDisagreedOrDeferred", hasDisagreedOrDeferred);
		model.addAttribute("vmmcCounsellingOutcomeLastestObs", vmmcCounsellingOutcomeLastestObs);
		model.addAttribute("screeningHardStopConditions", screeningHardStopConditions);
	}
	
	public boolean hasFilledForm(Visit currentPatientVisit, Patient patient, Form vmmcForm,
	        EncounterService encounterService) {
		boolean filledForm = false;
		if (currentPatientVisit != null) {
			filledForm = encounterService
			        .getEncounters(new EncounterSearchCriteria(patient, null, null, null, null,
			                Collections.singletonList(vmmcForm), null, null, null, null, false))
			        .stream().anyMatch(encounter -> encounter.getForm().equals(vmmcForm));
		}
		return filledForm;
	}
}
