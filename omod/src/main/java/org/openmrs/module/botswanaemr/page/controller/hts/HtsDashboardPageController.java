/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.fragment.controller.SearchFragmentController;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonCreator;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDurationSince;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;

public class HtsDashboardPageController {
	
	public void controller(PageModel pageModel, UiSessionContext uiSessionContext,
	        @SpringBean("bemrs.itemStockDataService") IItemStockDataService iItemStockDataService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        @SpringBean("bemrs.itemDataService") IItemDataService iItemDataService) throws Exception {
		
		SearchFragmentController searchFragmentController = new SearchFragmentController();
		EncounterType htsEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID);
		List<Encounter> htsEncounters = Context.getEncounterService().getEncounters(new EncounterSearchCriteriaBuilder()
		        .setEncounterTypes(Arrays.asList(htsEncounterType)).setIncludeVoided(false).createEncounterSearchCriteria());
		
		List<Encounter> htsEncountersToday = Context.getEncounterService()
		        .getEncounters(new EncounterSearchCriteriaBuilder().setEncounterTypes(Arrays.asList(htsEncounterType))
		                .setIncludeVoided(false).setFromDate(BotswanaEmrUtils.getStartOfDay())
		                .setToDate(BotswanaEmrUtils.getEndOfDay()).createEncounterSearchCriteria());
		
		List<Encounter> htsEncountersYesterday = Context.getEncounterService()
		        .getEncounters(new EncounterSearchCriteriaBuilder().setEncounterTypes(Arrays.asList(htsEncounterType))
		                .setIncludeVoided(false).setFromDate(BotswanaEmrUtils.getStartOfYesterday())
		                .setToDate(BotswanaEmrUtils.getEndOfYesterday()).createEncounterSearchCriteria());
		
		pageModel.addAttribute("totalTestedPatientCount", htsEncounters.size());
		pageModel.addAttribute("totalTestedPatientCountToday", htsEncountersToday.size());
		pageModel.addAttribute("totalTestedPatientCountYesterday", htsEncountersYesterday.size());
		
		int determineQuantity = 0;
		int unigoldQuantity = 0;
		
		Location location = null;
		if (uiSessionContext.getSessionLocation() != null) {
			location = uiSessionContext.getSessionLocation();
			Stockroom stockroom = StockUtils.getHtsStockroom(location);
			if (stockroom != null) {
				determineQuantity = searchFragmentController
				        .availableTestKitQuantity(BotswanaEmrConstants.DETERMINE_KIT_UUID, stockroom.getId());
				unigoldQuantity = searchFragmentController.availableTestKitQuantity(BotswanaEmrConstants.UNIGOLD_KIT_UUID,
				    stockroom.getId());
			}
		}
		
		pageModel.addAttribute("determineQuantity", determineQuantity);
		pageModel.addAttribute("unigoldQuantity", unigoldQuantity);
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		
		pageModel.addAttribute("todayHTSList",
		    patientObjects(BotswanaEmrUtils.getAllHtsPatients(null, null, limitFetch, location)));
		
		HtsLabControlService htsLabControlService = Context.getService(HtsLabControlService.class);
		List<Location> htsServiceLocations = Context.getLocationService()
		        .getLocationsByTag(Context.getLocationService().getLocationTagByName(HTS_LOCATION_TAG_NAME));
		List<String> testingPoints = htsServiceLocations.stream().map(Location::getName).collect(Collectors.toList());
		
		List<LabQualityControl> weeklyQc = htsLabControlService.getLabQualityControlsThisWeekByTestingPoints(testingPoints);
		List<String> existingTestingPoints = weeklyQc.stream().map(LabQualityControl::getTestingPoint)
		        .collect(Collectors.toList());
		
		List<String> missingTestingPoints = testingPoints.stream()
		        .filter(testingPoint -> !existingTestingPoints.contains(testingPoint)).collect(Collectors.toList());
		Gson gson = new Gson();
		String jsonArray = gson.toJson(missingTestingPoints);
		
		pageModel.addAttribute("missingTestingPoints", jsonArray);
		
		pageModel.addAttribute("qcCompleted", missingTestingPoints.isEmpty());
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(formatPersonCreator(patient));
				generalPatientObject.setDuration(getDurationSince(encounter.getDateCreated()));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
}
