/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs;

import org.openmrs.Visit;
import org.openmrs.Patient;
import org.openmrs.Program;
import org.openmrs.EncounterType;
import org.openmrs.PatientProgram;
import org.openmrs.Encounter;
import org.openmrs.Form;

import org.openmrs.api.FormService;
import org.openmrs.api.VisitService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

public class VmmcProgramFragmentController {
	
	private Visit currentPatientVisit;
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, @SpringBean("visitService") VisitService visitService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiSessionContext sessionContext) {
		
		List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
		currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		boolean hasActiveVisit = currentPatientVisit != null;
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		
		Program vmmcProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.VMMC_PROGRAM_UUID);
		EncounterType labEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_RESULT_ENCOUNTER_TYPE_UUID);
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, vmmcProgram, null, null,
		    null, null, false);
		
		List<PatientProgram> activeProgramEnrollmentForPatients = new ArrayList<PatientProgram>();
		boolean enrolled = false;
		
		for (PatientProgram patientProgram : patientProgramList) {
			if (patientProgram.getDateCompleted() == null) {
				activeProgramEnrollmentForPatients.add(patientProgram);
			}
		}
		
		enrolled = !activeProgramEnrollmentForPatients.isEmpty();
		
		List<String> preEnrolmentForms = Arrays.asList(BotswanaEmrConstants.LAB_REQUEST_FORM_UUID,
		    BotswanaEmrConstants.LAB_RESULTS_FORM_UUID);
		List<String> postEnrolmentForms = Arrays.asList(VMMC_OPERATION_FORM_UUID, VMMC_POST_OPERATION_FORM_UUID,
		    VMMC_COUNSELLING_FORM_UUID, VMMC_CONSENT_FORM_UUID, VMMC_BOOKING_FORM_UUID);
		List<String> postOperationForms = Collections.singletonList(VMMC_POST_OPERATION_ASSESSMENT_FORM_UUID);
		
		List<String> listOfFormsToIncludeUuids = new ArrayList<>();
		
		listOfFormsToIncludeUuids.addAll(preEnrolmentForms);
		listOfFormsToIncludeUuids.addAll(postEnrolmentForms);
		listOfFormsToIncludeUuids.addAll(postOperationForms);
		
		List<HtmlForm> completedHtmlForms = completedHtmlForms(patient, visitService, htmlFormEntryService);
		allHtmlForms.removeAll(completedHtmlForms);
		List<HtmlForm> list = new ArrayList<HtmlForm>();
		for (HtmlForm f : allHtmlForms) {
			if (!f.getForm().getRetired()) {
				if (listOfFormsToIncludeUuids.contains(f.getForm().getUuid())) {
					list.add(f);
				}
			}
		}
		
		List<HtmlForm> vmmcEncounterForms = getVmmcEncounterForms(htmlFormEntryService, formService);
		
		List<Encounter> filledEncounterForPatientPerTheCurrentVisit = getVmmcEncounters(patient, encounterService,
		    currentPatientVisit, vmmcEncounterForms);
		model.addAttribute("encounters", filledEncounterForPatientPerTheCurrentVisit == null ? new ArrayList<Encounter>()
		        : filledEncounterForPatientPerTheCurrentVisit);
		
		List<HtmlForm> tmpAvailableForms;
		if (filledEncounterForPatientPerTheCurrentVisit != null) {
			if (filledEncounterForPatientPerTheCurrentVisit.isEmpty()) {
				tmpAvailableForms = vmmcEncounterForms;
			} else {
				List<Form> vmmcForms = filledEncounterForPatientPerTheCurrentVisit.stream().map(Encounter::getForm)
				        .collect(Collectors.toList());
				tmpAvailableForms = vmmcEncounterForms.stream().filter(f -> !vmmcForms.contains(f.getForm()))
				        .collect(Collectors.toList());
			}
		} else {
			tmpAvailableForms = vmmcEncounterForms;
		}
		
		// Iteratively arrange the forms in the order they should be filled in this order
		// Counselling firm, Initial screening form, Booking form, consent form, operation form, post operation assessment form,post operation reviews form
		List<HtmlForm> arrangedForms = new ArrayList<>();
		List<String> formUuids = Arrays.asList(VMMC_COUNSELLING_FORM_UUID, VMMC_INITIAL_SCREENING_FORM_UUID,
		    VMMC_BOOKING_FORM_UUID, VMMC_CONSENT_FORM_UUID, VMMC_OPERATION_FORM_UUID,
		    VMMC_POST_OPERATION_ASSESSMENT_FORM_UUID, VMMC_POST_OPERATION_FORM_UUID);
		for (String formUuid : formUuids) {
			for (HtmlForm form : tmpAvailableForms) {
				if (form.getForm().getUuid().equals(formUuid)) {
					arrangedForms.add(form);
				}
			}
		}
		
		model.addAttribute("availableVmmcForms", arrangedForms);
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("hasVisit", hasActiveVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("programs", programWorkflowService.getAllPrograms());
		model.addAttribute("enrolled", enrolled);
		model.addAttribute("visitTypes", visitService.getAllVisitTypes());
		model.addAttribute("activePrograms", activeProgramEnrollmentForPatients);
		model.addAttribute("hivOutComes", Programs.getTreatmentOutcomes());
		
		model.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "programs/programs", patient, currentPatientVisit, ui)));
	}
	
	private List<HtmlForm> completedHtmlForms(Patient patient, @SpringBean("visitService") VisitService visitService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService) {
		List<HtmlForm> completedHtmlForms = new ArrayList<>();
		for (Form form : completedForms(patient, visitService)) {
			completedHtmlForms.add(htmlFormEntryService.getHtmlFormByForm(form));
		}
		return completedHtmlForms;
	}
	
	private List<Form> completedForms(Patient patient, @SpringBean("visitService") VisitService visitService) {
		List<Form> completedForms = new ArrayList<Form>();
		
		if (currentPatientVisit != null) {
			List<Encounter> encounterList = currentPatientVisit.getNonVoidedEncounters();
			for (Encounter encounter : encounterList) {
				completedForms.add(encounter.getForm());
			}
		}
		return completedForms;
	}
	
	public static List<Encounter> getVmmcEncounters(Patient patient, EncounterService encounterService,
	        Visit currentPatientVisit, List<HtmlForm> vmmcEncounterForms) {
		
		List<Form> vmmcForms = vmmcEncounterForms.stream().map(HtmlForm::getForm).collect(Collectors.toList());
		return encounterService.getEncounters(new EncounterSearchCriteria(patient, null, null, null, null, vmmcForms, null,
		        null, null, Collections.singletonList(currentPatientVisit), false));
	}
}
