/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */

package org.openmrs.module.botswanaemr.htmlformentry;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Obs;

import java.util.Date;
import java.util.UUID;

import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TEST_KIT_SET_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TEST_KIT__UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TEST_KIT_LOT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_TEST_KIT_EXPIRY_DATE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HTS_STOCKROOM_UUID;

@Slf4j
public class HtsKitUsagePostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession session) {
		FormEntryContext.Mode mode = session.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
		IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		
		StockOperation stockOperation = new StockOperation();
		
		Obs stockRoomObs = session.getEncounter().getObs().stream()
		        .filter(e -> e.getConcept().getUuid().equals(HTS_STOCKROOM_UUID)).findFirst().orElse(null);
		
		if (stockRoomObs != null) {
			for (Obs obs : session.getEncounter().getAllObs()) {
				//System.out.println(obs.getConcept().getUuid());
				if (obs.isObsGrouping() && obs.getConcept().getUuid().equals(HTS_TEST_KIT_SET_UUID)) {
					Item item = null;
					StockOperation batchOperation = null;
					Date expirationDate = null;
					for (Obs obs2 : obs.getGroupMembers()) {
						
						if (obs2.getConcept().getUuid().equals(HTS_TEST_KIT__UUID)) {
							item = iItemDataService.getItemsByConcept(obs2.getValueCoded()).get(0);
							
						}
						if (obs2.getConcept().getUuid().equals(HTS_TEST_KIT_LOT_UUID)) {
							batchOperation = iStockOperationDataService.getOperationByNumber(obs2.getValueText());
							
						}
						if (obs2.getConcept().getUuid().equals(HTS_TEST_KIT_EXPIRY_DATE_UUID)) {
							expirationDate = obs2.getValueDate();
							
						}
					}
					
					stockOperation.addItem(item, 1, expirationDate, batchOperation);
				}
				
				if (obs.getConcept().getUuid().equals(HTS_STOCKROOM_UUID)) {
					stockOperation.setSource(iStockroomDataService.getByUuid(obs.getValueText()));
				}
			}
			
			stockOperation.setOperationDate(new Date());
			stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
			stockOperation.setDescription("Kits used to test the patient");
			stockOperation.setOperationNumber(UUID.randomUUID().toString());
			stockOperation.setStatus(StockOperationStatus.NEW);
			iStockOperationService.submitOperation(stockOperation);
			stockOperation.setStatus(StockOperationStatus.COMPLETED);
			iStockOperationService.submitOperation(stockOperation);
		}
		
	}
}
