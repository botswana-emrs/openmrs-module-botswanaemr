/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.api.APIException;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.ItemStockSummary;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationItem;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class CreateStockAdjustmentFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null) {
			List<Stockroom> stockrooms = iStockroomDataService.getStockroomsByLocation(uiSessionContext.getSessionLocation(),
			    false);
			
			fragmentModel.addAttribute("inventoryStockRooms", stockrooms);
		} else {
			fragmentModel.addAttribute("inventoryStockRooms", null);
		}
	}
	
	public List<SimpleObject> getItemStockData(@RequestParam(value = "stockRoomId") Integer stockRoomUuid,
	        @RequestParam(value = "q", required = false, defaultValue = "") String searchPhrase,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) {
		
		Stockroom stockRoom = iStockroomDataService.getById(stockRoomUuid);
		List<ItemStock> iStockList;
		if (searchPhrase.trim().length() > 0) {
			ItemSearch search = new ItemSearch(new Item());
			search.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.LIKE);
			search.getTemplate().setName("%" + searchPhrase + "%");
			iStockList = iStockroomDataService.getItems(stockRoom, search, null);
		} else {
			iStockList = iStockroomDataService.getItemsByRoom(stockRoom, null);
		}
		
		List<SimpleObject> itemStockList = new ArrayList<>();
		for (ItemStock itemStock : iStockList) {
			List<String> batchNumbers = new ArrayList<>();
			List<Date> expirations = new ArrayList<>();
			Map<Integer, MyObject> map = new HashMap<>();
			
			SimpleObject simpleObject = StockUtils.extractItemStockToSimpleObject(itemStock, null);
			Set<ItemStockDetail> itemStockDetail = itemStock.getDetails();
			int index = 0;
			for (ItemStockDetail itemStockDetail1 : itemStockDetail) {
				if (itemStockDetail1.getBatchOperation() != null
				        && itemStockDetail1.getBatchOperation().getOperationNumber() != null
				        && itemStockDetail1.getExpiration() != null) {
					map.put(index, new MyObject(itemStockDetail1.getBatchOperation().getOperationNumber(),
					        itemStockDetail1.getExpiration()));
					index = index + 1;
					batchNumbers.add(itemStockDetail1.getBatchOperation().getOperationNumber());
				}
				
			}
			simpleObject.put("batchNumbers", batchNumbers);
			simpleObject.put("expiryDates", expirations);
			simpleObject.put("myMap", map);
			itemStockList.add(simpleObject);
		}
		
		return itemStockList;
	}
	
	public SimpleObject saveItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId)
	        throws JSONException, ParseException {
		
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					String commentString = ((JSONObject) jsonObject.get(key)).getString("comment");
					
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						String batchNumber = ((JSONObject) jsonObject.get(key)).getString("batchNumber");
						StockOperation batchStockOperation = BotswanaInventoryContext.getStockOperationDataService()
						        .getOperationByNumber(batchNumber);
						Date expiryDate = BotswanaEmrUtils.parseDateFromString(expiryDate1);
						StockOperation stockOperation = new StockOperation();
						if (qty > 0) {
							stockOperation.setInstanceType(WellKnownOperationTypes.getAdjustment());
							stockOperation.setDestination(stockRoom);
							stockOperation.setDescription(commentString);
							stockOperation.setOperationNumber(UUID.randomUUID().toString());
							stockOperation.setOperationDate(new Date());
							stockOperation.setSource(stockRoom);
							stockOperation.addItem(itemStock.getItem(), qty, expiryDate, batchStockOperation);
						} else {
							stockOperation.setDescription(commentString);
							stockOperation.setOperationNumber(UUID.randomUUID().toString());
							stockOperation.setInstanceType(WellKnownOperationTypes.getDisposed());
							stockOperation.setSource(stockRoom);
							stockOperation.setDestination(stockRoom);
							stockOperation.setOperationDate(new Date());
							stockOperation.addItem(itemStock.getItem(), qty * -1, expiryDate, batchStockOperation);
						}
						submitStockOperation(stockOperation);
					}
				}
			}
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		return simpleObject;
		
	}
	
	public SimpleObject disposeExpiredProducts(@RequestParam(value = "id") Integer id,
	        @RequestParam(value = "comment") String comment, @RequestParam(value = "batchNumber") String batchNumber,
	        @RequestParam(value = "expiration") String expiration, @RequestParam(value = "quantity") Integer quantity,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId) throws ParseException {
		
		//System.out.println(comment + batchNumber + expiration+quantity +stockRoomId);
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date expiryDate = format.parse(expiration);
		
		Item item = BotswanaInventoryContext.getItemStockDetailDataService().getById(id).getItem();
		//ItemStock itemStock = BotswanaInventoryContext.getItemStockDetailDataService().getById(id).getItemStock();
		
		StockOperation stockOperation = new StockOperation();
		stockOperation.setDescription(comment);
		stockOperation.setOperationNumber(UUID.randomUUID().toString());
		stockOperation.setSource(stockRoom);
		stockOperation.setDestination(stockRoom);
		stockOperation.setOperationDate(new Date());
		stockOperation.addItem(item, quantity, expiryDate,
		    BotswanaInventoryContext.getStockOperationDataService().getOperationByNumber(batchNumber));
		stockOperation.setInstanceType(WellKnownOperationTypes.getDisposed());
		submitStockOperation(stockOperation);
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("status", "success");
		return simpleObject;
	}
	
	private StockOperation submitStockOperation(StockOperation stockOperation) throws APIException {
		
		try {
			stockOperation.setStatus(StockOperationStatus.NEW); // Create updated approved requisition
			stockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
			stockOperation.setStatus(StockOperationStatus.COMPLETED); // Approve requisition
			stockOperation = BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
		}
		catch (Exception e) {
			throw new APIException(e.getMessage());
		}
		
		return stockOperation;
	}
	
	public SimpleObject saveEditedItemStockData(@RequestParam(value = "itemStock", required = true) String itemStockId,
	        @RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam(value = "stockOperationId", required = false) Integer stockOperationId)
	        throws JSONException, ParseException {
		
		List<ItemStockSummary> itemStockSummaries = new ArrayList<>();
		JSONObject jsonObject = new JSONObject(itemStockId);
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (jsonObject.get(key) instanceof JSONObject) {
				if (jsonObject.get(key) instanceof JSONObject) {
					ItemStock itemStock = BotswanaInventoryContext.getItemStockDataService()
					        .getByUuid(((JSONObject) jsonObject.get(key)).getString("uuid"));
					String qtyString = ((JSONObject) jsonObject.get(key)).getString("quantity");
					if (!qtyString.equals("")) {
						int qty = Integer.parseInt(qtyString);
						String expiryDate1 = ((JSONObject) jsonObject.get(key)).getString("expiryDate");
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date expiryDate = format.parse(expiryDate1);
						if (qty != itemStock.getQuantity()) {
							ItemStockSummary iss = new ItemStockSummary();
							iss.setExpiration(expiryDate);
							iss.setItem(itemStock.getItem());
							iss.setActualQuantity(qty);
							iss.setQuantity(itemStock.getQuantity());
							itemStockSummaries.add(iss);
						}
					}
				}
			}
		}
		Stockroom stockRoom = BotswanaInventoryContext.getStockRoomDataService().getById(stockRoomId);
		
		StockOperation oldOperation = BotswanaInventoryContext.getStockOperationDataService().getById(stockOperationId);
		if (oldOperation != null) {
			oldOperation.setStatus(StockOperationStatus.CANCELLED);
			StockOperation operation = new StockOperation();
			operation.setStatus(StockOperationStatus.NEW);
			operation.setInstanceType(WellKnownOperationTypes.getAdjustment());
			operation.setSource(stockRoom);
			operation.setOperationNumber("STOCK_TAKE:" + UUID.randomUUID().toString());
			operation.setOperationDate(new Date());
			operation.setItems(createOperationsItemSet(operation, itemStockSummaries));
			
			BotswanaInventoryContext.getStockOperationService().submitOperation(operation);
		}
		SimpleObject simpleObject = new SimpleObject();
		
		return simpleObject;
		
	}
	
	private Set<StockOperationItem> createOperationsItemSet(StockOperation operation,
	        List<ItemStockSummary> inventoryStockTakeList) {
		Set<StockOperationItem> items = new HashSet<StockOperationItem>();
		for (ItemStockSummary invitem : inventoryStockTakeList) {
			StockOperationItem item = new StockOperationItem();
			item.setOperation(operation);
			item.setItem(invitem.getItem());
			item.setExpiration(invitem.getExpiration());
			item.setCalculatedExpiration(false);
			
			// int quantity = invitem.getActualQuantity() - invitem.getQuantity();
			int quantity = invitem.getQuantity() - invitem.getActualQuantity();
			
			item.setQuantity(invitem.getActualQuantity());
			
			if (quantity < 0 || invitem.getActualQuantity() == 0) {
				item.setCalculatedBatch(true);
				item.setBatchOperation(null);
			} else {
				item.setCalculatedBatch(true);
			}
			items.add(item);
		}
		return items;
	}
	
	class MyObject {
		
		private String batchNumber;
		
		private Date expiration;
		
		public MyObject(String batchNumber, Date expiration) {
			this.batchNumber = batchNumber;
			this.expiration = expiration;
		}
		
		public String getBatchNumber() {
			return batchNumber;
		}
		
		public Date getExpiration() {
			return expiration;
		}
	}
}
