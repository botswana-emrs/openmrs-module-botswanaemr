/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttribute;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
public class AllStockRoomsFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext, UiUtils ui) {
		
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		
		List<StockRoomExtended> allStockRooms = new ArrayList<>();
		for (Stockroom stockroom : stockrooms) {
			StockRoomExtended stockRoomExtended = new StockRoomExtended();
			stockRoomExtended.setStockroom(stockroom);
			stockRoomExtended.setType(getStockRoomType(stockroom));
			allStockRooms.add(stockRoomExtended);
		}
		
		fragmentModel.addAttribute("allStockRooms", allStockRooms);
	}
	
	public void retireStockRoom(@RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) {
		Stockroom stockroom;
		
		if (stockRoomId != null) {
			stockroom = iStockroomDataService.getById(stockRoomId);
			iStockroomDataService.retire(stockroom, "retire StockRoom");
		}
	}
	
	private String getStockRoomType(@NonNull Stockroom stockroom) {
		Set<StockRoomAttribute> attributes = stockroom.getActiveAttributes();
		if (attributes != null) {
			for (StockRoomAttribute activeAttribute : attributes) {
				if (activeAttribute.getAttributeType().getUuid()
				        .equals(BotswanaEmrConstants.INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID)) {
					return activeAttribute.getValue();
				}
			}
		}
		return "";
	}
	
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class StockRoomExtended {
		
		private Stockroom stockroom;
		
		private String type;
	}
}
