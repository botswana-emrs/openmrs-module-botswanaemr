/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openmrs.DrugOrder;
import org.openmrs.Order;
import org.openmrs.api.EncounterService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.DrugOrderSimplifier;
import org.openmrs.module.botswanaemr.utilities.PharmacyUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class PharmacyHoldRequestsTableFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam("encounterId") Integer encounterId,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "orderDetailsId", required = false) Integer orderDetailsId,
	        UiSessionContext uiSessionContext) {
		
		List<Order> orderListOriginal = encounterService.getEncounter(encounterId).getOrders().stream()
		        .filter(o -> o.getOrderReasonNonCoded() != null && o.getOrderReasonNonCoded().contains("Hold")
		                && !o.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED))
		        .collect(Collectors.toList());
		
		List<DrugOrderSimplifier> drugOrderSimplifierList = new ArrayList<>();
		List<DrugOrder> uniqueOrders = PharmacyUtils.getGroupedDrugOrders(orderListOriginal, encounterService, encounterId);
		
		for (DrugOrder drugOrder : uniqueOrders) {
			
			DrugOrderSimplifier drugOrderSimplifier = DrugOrderSimplifier.simplify(drugOrder);
			
			drugOrderSimplifier.setCurrentFacility(uiSessionContext.getSessionLocation().getName());
			drugOrderSimplifier.setRequestingFacility(drugOrder.getEncounter().getLocation().getName());
			drugOrderSimplifier.setHoldingFacility(drugOrder.getOrderReasonNonCoded().replace("Hold:", ""));
			if (drugOrder.getFulfillerStatus() != null) {
				if (drugOrder.getFulfillerStatus().equals(Order.FulfillerStatus.IN_PROGRESS)) {
					drugOrderSimplifier.setStatus("Hold requested");
				} else if (drugOrder.getFulfillerStatus().equals(Order.FulfillerStatus.RECEIVED)) {
					drugOrderSimplifier.setStatus("Hold accepted");
				} else {
					drugOrderSimplifier.setStatus(Order.FulfillerStatus.COMPLETED.name());
				}
			}
			
			drugOrderSimplifierList.add(drugOrderSimplifier);
		}
		fragmentModel.addAttribute("holdDrugOrder", drugOrderSimplifierList);
	}
	
}
