/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EditPatientComplaintFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiUtils ui, UiSessionContext uiSessionContext) {
		model.addAttribute("visit", visit);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		EncounterService encounterService = Context.getEncounterService();
		List<Obs> dietGroupingObs = botswanaEmrService.getObservation(patient,
		    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PATIENT_COMPLAINT_GROUPING_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		Set<Obs> groupingObs = new HashSet<Obs>(dietGroupingObs);
		List<SimpleObject> complaintObjects = BotswanaEmrUtils.getSimplifiedComplaintsObjects(groupingObs);
		model.addAttribute("complaintsObjects", complaintObjects);
	}
	
	public void updateComplaints(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<SimpleObject> deletedComplaints = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedComplaints = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newComplaints = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		saveNewComplaints(patient, newComplaints, sessionContext.getSessionLocation(), visit);
		
		ObsService os = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		
		// Delete complaints
		for (SimpleObject simpleObject : deletedComplaints) {
			if (simpleObject.get("complaint") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("complaint"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("comment") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("comment"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
		}
		
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_NOTE_CONCEPT);
		// Edit existing complaint
		for (SimpleObject simpleObject : editedComplaints) {
			if (simpleObject.get("uuid") != null && simpleObject.get("complaint") != null) {
				Obs groupingObs;
				Obs obs = os.getObsByUuid((String) simpleObject.get("uuid"));
				if (!obs.getValueText().equals(simpleObject.get("complaint"))) {
					obs.setValueText((String) simpleObject.get("complaint"));
					os.saveObs(obs, "Edit complaint");
				}
				groupingObs = obs.getObsGroup();
				
				if (simpleObject.get("commentUuid") != null && simpleObject.get("comment") != null) {
					Obs notesObs = os.getObsByUuid((String) simpleObject.get("commentUuid"));
					if (notesObs != null) {
						if (!notesObs.getValueText().equals(simpleObject.get("comment"))) {
							notesObs.setValueText((String) simpleObject.get("comment"));
							os.saveObs(notesObs, "Edit complaint");
						}
					} else {
						//New note
						Obs newObs = createNewObsFromGroup(groupingObs, notesConcept);
						if (newObs != null) {
							newObs.setValueText((String) simpleObject.get("comment"));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Complaint note");
						}
					}
				}
			}
		}
	}
	
	private Obs createNewObsFromGroup(Obs groupingObs, Concept concept) {
		if (groupingObs != null) {
			return BotswanaEmrUtils.creatObs(groupingObs.getEncounter(), concept);
		}
		return null;
	}
	
	private void saveNewComplaints(Patient patient, List<SimpleObject> newComplaints, Location sessionLocation,
	        Visit visit) {
		EncounterType vitalsEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		Encounter vitalEncounter = BotswanaEmrUtils.createEncounter(patient, vitalsEncounterType, sessionLocation, visit);
		
		ConceptService cs = Context.getConceptService();
		
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_COMPLAINT_GROUPING_CONCEPT_UUID);
		Concept compolaintConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_COMPLAINT_CONCEPT_UUID);
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_NOTE_CONCEPT);
		
		for (SimpleObject simpleObject : newComplaints) {
			Obs groupingObs = BotswanaEmrUtils.creatObs(vitalEncounter, groupingConcept);
			
			if (StringUtils.isNotEmpty(String.valueOf(simpleObject.get("complaint")))) {
				Obs complaint = BotswanaEmrUtils.creatObs(vitalEncounter, compolaintConcept);
				complaint.setValueText(String.valueOf(simpleObject.get("complaint")));
				groupingObs.addGroupMember(complaint);
				
				if (simpleObject.get("comment") != null) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, notesConcept);
					noteObs.setValueText((String) simpleObject.get("comment"));
					groupingObs.addGroupMember(noteObs);
				}
				vitalEncounter.addObs(groupingObs);
			}
		}
		
		//Save an encounter
		if (vitalEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(vitalEncounter);
		}
	}
}
