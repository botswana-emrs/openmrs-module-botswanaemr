/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import java.util.Set;

import org.openmrs.Obs;
import org.openmrs.Visit;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;

public class SrhEnrolmentPostSubmissionAction implements CustomFormSubmissionAction {
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT)) {
			
			Visit visit = formEntrySession.getEncounter().getVisit();
			
			Set<Obs> obsList = formEntrySession.getEncounter().getObs();
			Obs serviceTypeObs = obsList.stream()
			        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.SERVICE_TYPE_CONCEPT_UUID)).findFirst()
			        .orElse(null);
			
			VisitService visitService = Context.getVisitService();
			
			if (serviceTypeObs != null && serviceTypeObs.getValueCoded() != null && visit != null) {
				if (serviceTypeObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.MATERNITY_CONCEPT_UUID)) {
					visit.setVisitType(visitService.getVisitTypeByUuid(BotswanaEmrConstants.MATERNITY_VISIT_TYPE_UUID));
				} else if (serviceTypeObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.ANC_CONCEPT_UUID)) {
					visit.setVisitType(visitService.getVisitTypeByUuid(BotswanaEmrConstants.ANC_VISIT_TYPE_UUID));
				}
				
				visitService.saveVisit(visit);
			}
		}
	}
}
