/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import liquibase.pro.packaged.co;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.Person;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.Relationship;
import org.openmrs.RelationshipType;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.APIException;
import org.openmrs.api.ConceptService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.PersonService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.auditlog.AuditLog;
import org.openmrs.module.auditlog.api.AuditLogService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.PatientRegistration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.AdvancedSearchRegistryPerson;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BatchResponse;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistryService;
import org.openmrs.module.botswanaemr.api.impl.BotswanaEmrServiceImpl;
import org.openmrs.module.botswanaemr.api.impl.BotswanaPersonRegistryServiceImpl;
import org.openmrs.module.botswanaemr.contract.NokAttributes;
import org.openmrs.module.botswanaemr.contract.NokMapper;
import org.openmrs.module.botswanaemr.contract.PatientContactPerson;
import org.openmrs.module.botswanaemr.contract.PatientNok;
import org.openmrs.module.botswanaemr.contract.PatientTbScreening;
import org.openmrs.module.botswanaemr.contract.search.SimplifiedAuditLog;
import org.openmrs.module.botswanaemr.model.RegistrySource;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.service.IdentifierSourceService;
import org.openmrs.module.registrationcore.RegistrationData;
import org.openmrs.module.webservices.rest.SimpleObject;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.RestConstants;
import org.openmrs.module.webservices.rest.web.representation.Representation;
import org.openmrs.validator.PatientIdentifierValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.BIRTH_CERT_IDENTIFIER_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.COUGH;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DECREASED_PLAYFULNESS;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.EMAIL_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.EMERGENCY_PATIENT_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ENLARGED_LYMPH_NODES;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FEMALE_GENDER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FEVER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FORCE_NUMBER_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MALE_GENDER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MARITAL_STATUS_ATTRIBUTE_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MIGRATION_FACILITY_ATTRIBUTE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MIGRATION_PATIENT_ATTRIBUTE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NATIONAL_ID_IDENTIFIER_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NEGATIVE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NIGHT_SWEAT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_TYPE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_EMAIL_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_ID_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NO_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NUMBER_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.OCCUPATION_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.OTHER_GENDER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.OTHER_NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PASSPORT_IDENTIFIER_TYPE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PERSON_LOCATION_ATTRIBUTE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PHONE_NUMBER_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.POSITIVE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.RANK_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REGULAR_PATIENT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_CONTACT_LAST_2_YEARS;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_FORM_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TEMPORARY_ID_IDENTIFIER_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.UNKNOWN_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.WEIGHT_LOSS;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.YES_CONCEPT_UUID;

@Controller
@RequestMapping(value = "/rest/" + RestConstants.VERSION_1 + "/" + BotswanaEmrConstants.BOTSWANAEMR_MODULE_ID)
public class BotswanaEmrRestController {
	
	public static final String ID_NUMBER = "idNumber";
	
	public static final String FULLNAME = "fullname";
	
	public static final String RELATIONSHIP = "relationship";
	
	public static final String CONTACT = "contact";
	
	public static final String EXTRA_CONTACT = "extraContact";
	
	public static final String THIRD_CONTACT = "thirdContact";
	
	public static final String FOURTH_CONTACT = "fourthContact";
	
	public static final String FIFTH_CONTACT = "fifthContact";
	
	public static final String EMAIL = "email";
	
	public static final String ID_TYPE = "idType";
	
	public static final String NATIONAL_ID = "nationalIdNumber";
	
	public static final String PASSPORT_NUMBER = "passportNumber";
	
	public static final String BIRTH_CERTIFICATE_NUMBER = "birthCertificateNumber";
	
	private static IdentifierSource idSource;
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private LocationService locationService;
	
	final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
	
	private BotswanaPersonRegistryService botswanaPersonRegistryService = Context
	        .getRegisteredComponent("botswanaemr.BotswanaPersonRegistryService", BotswanaPersonRegistryServiceImpl.class); // new BotswanaPersonRegistryServiceImpl();
	
	private Map<String, PersonAttributeType> personAttributeTypeMap = new HashMap<>();
	
	@RequestMapping(value = "/registry/{personIdNumber}", method = RequestMethod.GET)
	public @ResponseBody BotswanaRegistryResponse SearchInRegistry(@PathVariable String personIdNumber) {
		
		BotswanaRegistryResponse searchResults = botswanaPersonRegistryService.searchPerson(RegistrySource.CUSTOM,
		    personIdNumber);
		
		return searchResults;
	}
	
	@RequestMapping(value = "/registry/omang/{personIdNumber}", method = RequestMethod.GET)
	public @ResponseBody BotswanaRegistryResponse SearchPatientInOmang(@PathVariable String personIdNumber) {
		
		BotswanaRegistryResponse searchResults = botswanaPersonRegistryService.searchPerson(RegistrySource.OMANG,
		    personIdNumber);
		
		return searchResults;
	}
	
	@RequestMapping(value = "/registry/bdrs/{personIdNumber}", method = RequestMethod.GET)
	public @ResponseBody BotswanaRegistryResponse SearchPatientInBdrs(@PathVariable String personIdNumber) {
		
		BotswanaRegistryResponse searchResults = botswanaPersonRegistryService.searchPerson(RegistrySource.BIRTH_CERTIFICATE,
		    personIdNumber);
		
		return searchResults;
	}
	
	@RequestMapping(value = "/registry/{identifierType}/{personIdNumber}", method = RequestMethod.GET)
	public @ResponseBody BotswanaRegistryResponse SearchPatientInRegistry(@PathVariable String identifierType,
	        @PathVariable String personIdNumber) {
		
		RegistrySource registrySource = null;
		
		switch (identifierType) {
			case NATIONAL_ID:
				registrySource = RegistrySource.OMANG;
				break;
			case PASSPORT_NUMBER:
				registrySource = RegistrySource.PASSPORT;
				break;
			case BIRTH_CERTIFICATE_NUMBER:
				registrySource = RegistrySource.BIRTH_CERTIFICATE;
				break;
		}
		BotswanaRegistryResponse searchResults = botswanaPersonRegistryService.searchPerson(registrySource, personIdNumber);
		
		return searchResults;
	}
	
	@RequestMapping(value = "/update-nok-attributes/{patientuuid}", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BotswanaRegistryResponse UpdateNokAttributes(@PathVariable String patientuuid,
	        @RequestBody Map<String, Object> payload) throws ParseException {
		// TODO: Handle already existing patients in local registry
		boolean updatePending = false;
		Patient patient = Context.getPatientService().getPatientByUuid(patientuuid);
		
		NokAttributes nokAttributes = mapper.convertValue(payload, NokAttributes.class);
		Person person = null;
		boolean newNokRelationShip = true;
		boolean newPerson = false;
		String relationShipVal = "";
		for (org.openmrs.ui.framework.SimpleObject simpleObject : nokAttributes.getAttributes()) {
			if (simpleObject.get("name") != null && simpleObject.get("name").equals("nokUuid")) {
				String personUuid = (String) simpleObject.get("value");
				newPerson = personUuid.isEmpty();
				person = Context.getPersonService().getPersonByUuid(personUuid);
			}
			if (simpleObject.get("name") != null && simpleObject.get("name").equals("relationShip")) {
				relationShipVal = (String) simpleObject.get("value");
			}
			if (person != null && !relationShipVal.isEmpty()) {
				break;
			}
		}
		
		Relationship currentRelationship = null;
		List<Relationship> relationshipList = new ArrayList<>();
		if (person != null) {
			relationshipList = Context.getPersonService().getRelationshipsByPerson(person);
			for (Relationship relationship : relationshipList) {
				if (relationship.getPersonA() == patient.getPerson() || relationship.getPersonB() == patient.getPerson()) {
					currentRelationship = relationship;
					newNokRelationShip = !(relationship.getRelationshipType().getaIsToB().equalsIgnoreCase(relationShipVal)
					        || relationship.getRelationshipType().getbIsToA().equalsIgnoreCase(relationShipVal));
					break;
				}
			}
		}
		
		if (person == null) {
			person = new Person();
		}
		
		// Reset relationships
		relationshipList = new ArrayList<>();
		List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
		PersonName personName = new PersonName();
		PersonAddress personAddress = person.getPersonAddress() != null ? person.getPersonAddress() : new PersonAddress();
		
		for (org.openmrs.ui.framework.SimpleObject simpleObject : nokAttributes.getAttributes()) {
			String value = String.valueOf(simpleObject.get("value"));
			if (simpleObject.get("uuid") != null && StringUtils.isNotEmpty(String.valueOf(simpleObject.get("uuid")))) {
				PersonAttribute attribute = Context.getPersonService()
				        .getPersonAttributeByUuid(String.valueOf(simpleObject.get("uuid")));
				if (attribute != null && !attribute.getValue().equals(value)) {
					updatePending = true;
					attribute.setValue(value);
					person.addAttribute(attribute);
				}
			} else {
				if (simpleObject.get("name") != null) {
					String typename = String.valueOf(simpleObject.get("name"));
					
					// Update person names
					if (typename.equals("givenName") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(person.getGivenName())) {
							if (newPerson) {
								personName.setGivenName(value);
							} else {
								person.getPersonName().setGivenName(value);
							}
							updatePending = true;
						}
					} else if (typename.equals("middleName") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(person.getMiddleName())) {
							if (newPerson) {
								personName.setMiddleName(value);
							} else {
								person.getPersonName().setMiddleName(value);
							}
							updatePending = true;
						}
					} else if (typename.equals("familyName") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(person.getFamilyName())) {
							if (newPerson) {
								personName.setFamilyName(value);
							} else {
								person.getPersonName().setFamilyName(value);
							}
							updatePending = true;
						}
					} else if (typename.equals("district") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(personAddress.getAddress2())) {
							if (person.getPersonAddress() != null) {
								person.getPersonAddress().setAddress2(value);
							} else {
								personAddress.setAddress2(value);
							}
							updatePending = true;
						}
					} else if (typename.equals("city") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(personAddress.getCityVillage())) {
							if (person.getPersonAddress() != null) {
								person.getPersonAddress().setCityVillage(value);
							} else {
								personAddress.setCityVillage(value);
							}
							updatePending = true;
						}
					} else if (typename.equals("landmark") && StringUtils.isNotEmpty(value)) {
						if (!value.equals(personAddress.getAddress4())) {
							if (person.getPersonAddress() != null) {
								person.getPersonAddress().setAddress4(value);
							} else {
								personAddress.setAddress4(value);
							}
							updatePending = true;
						}
					} else {
						// Save new attribute
						PersonAttributeType personAttributeType = getPersonAttributeType(typename);
						if (personAttributeType != null) {
							PersonAttribute personAttribute = new PersonAttribute();
							personAttribute.setAttributeType(personAttributeType);
							personAttribute.setValue(value);
							person.addAttribute(personAttribute);
							updatePending = true;
						}
					}
				}
			}
			
		}
		if (updatePending) {
			if (newPerson) {
				person.addName(personName);
				person.addAddress(personAddress);
			}
			person = Context.getPersonService().savePerson(person);
			
			if (newNokRelationShip && currentRelationship == null) {
				// Set relationship
				for (RelationshipType relationshipType : relationshipTypes) {
					if (relationshipType.getaIsToB().toLowerCase().contains(relationShipVal.toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						relationship.setPersonB(person);
						relationship.setPersonA(patient.getPerson());
						Context.getPersonService().saveRelationship(relationship);
						break;
					} else if (relationshipType.getbIsToA().toLowerCase().contains(relationShipVal.toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						relationship.setPersonA(person);
						relationship.setPersonB(patient.getPerson());
						Context.getPersonService().saveRelationship(relationship);
						break;
					}
				}
			} else if (newNokRelationShip) {
				for (RelationshipType relationshipType : relationshipTypes) {
					if (relationshipType.getaIsToB().toLowerCase().contains(relationShipVal.toLowerCase())) {
						currentRelationship.setRelationshipType(relationshipType);
						currentRelationship.setPersonB(person);
						currentRelationship.setPersonA(patient.getPerson());
						Context.getPersonService().saveRelationship(currentRelationship);
						break;
					} else if (relationshipType.getbIsToA().toLowerCase().contains(relationShipVal.toLowerCase())) {
						currentRelationship.setRelationshipType(relationshipType);
						currentRelationship.setPersonA(person);
						currentRelationship.setPersonB(patient.getPerson());
						relationshipList.add(currentRelationship);
						Context.getPersonService().saveRelationship(currentRelationship);
						break;
					}
				}
			}
			
			updatePending = false;
		}
		
		BotswanaRegistryResponse response = new BotswanaRegistryResponse();
		response.setStatus(HttpStatus.OK.toString());
		return response;
	}
	
	private PersonAttributeType getPersonAttributeType(String name) {
		PersonAttributeType personAttributeType = null;
		if (name.equalsIgnoreCase("idType")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contactType")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_TYPE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("idNumber")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_ID_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("relationship")) {
			personAttributeType = Context.getPersonService()
			        .getPersonAttributeTypeByUuid(NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("email")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_EMAIL_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contact1")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contact2")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contact3")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contact4")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID);
		} else if (name.equalsIgnoreCase("contact5")) {
			personAttributeType = Context.getPersonService().getPersonAttributeTypeByUuid(NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID);
		}
		return personAttributeType;
	}
	
	@RequestMapping(value = "/nokRelationship/{personId}", method = RequestMethod.GET)
	public @ResponseBody List<PatientNok> GetPatientNok(@PathVariable Integer personId) {
		List<PatientNok> patientNoks = new ArrayList<>();
		
		Person person = Context.getPersonService().getPerson(personId);
		if (person != null) {
			for (Relationship relationship : Context.getPersonService().getRelationshipsByPerson(person)) {
				patientNoks.add(NokMapper.mapToResponse(relationship, person));
			}
			
		}
		
		return patientNoks;
	}
	
	@RequestMapping(value = "/batchTbScreening", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BatchResponse handleBatchTbScreening(HttpSession httpSession,
	        @RequestBody(required = false) List<PatientTbScreening> payload) {
		BatchResponse batchResponse = new BatchResponse();
		List<SimpleObject> failed = new ArrayList<>();
		List<SimpleObject> succeeded = new ArrayList<>();
		ConceptService cs = Context.getConceptService();
		ObsService os = Context.getObsService();
		
		Concept yesConcept = cs.getConceptByUuid(YES_CONCEPT_UUID);
		Concept noConcept = cs.getConceptByUuid(NO_CONCEPT_UUID);
		Concept positiveConcept = cs.getConceptByUuid(POSITIVE);
		Concept negativeConcept = cs.getConceptByUuid(NEGATIVE_UUID);
		Concept unknownConcept = cs.getConceptByUuid(UNKNOWN_CONCEPT_UUID);
		
		for (PatientTbScreening tbScreening : payload) {
			Integer visitId = null;
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.add("patientId", tbScreening.getPatientId());
			simpleObject.add("migrationPatientId", tbScreening.getMigrationPatientId());
			simpleObject.add("migrationFacilityId", tbScreening.getMigrationFacilityId());
			simpleObject.add("recordId", tbScreening.getRecordId());
			
			try {
				// Search DB to check if patient is already registered, ignore if patient records already exist.
				// List<Person> people = Context.getService(BotswanaEmrService.class).getPatientByMigrationMetadata(
				//    tbScreening.getMigrationFacilityId(), tbScreening.getMigrationPatientId());
				
				List<PatientIdentifierType> allowedIdTypes = new ArrayList<>();
				PatientIdentifierType patientIdentifierType = getIdentifierType(tbScreening.getIdType());
				allowedIdTypes.add(patientIdentifierType);
				List<Patient> people = Context.getPatientService().getPatients(null, tbScreening.getIdNumber(),
				    allowedIdTypes, true);
				
				if (people == null || people.isEmpty()) {
					throw new Exception(String.format("Patient with id %s not found", tbScreening.getMigrationPatientId()));
				}
				if (people.size() > 1) {
					throw new Exception(
					        String.format("Multiple Patient records with id %s found", tbScreening.getMigrationPatientId()));
				}
				
				Patient patient = Context.getPatientService().getPatient(people.get(0).getPersonId());
				
				// Create visit
				VisitType visitType = Context.getVisitService()
				        .getVisitTypeByUuid(BotswanaEmrConstants.FACILITY_VISIT_VISIT_TYPE_UUID);
				Date startDate = (new SimpleDateFormat("yyyy-MM-dd")).parse(tbScreening.getVisitDate());
				Location location = getLocation(tbScreening.getMigrationFacilityId(), tbScreening.getLocation());
				
				Visit visit = createVisit(patient, visitType, startDate, location);
				visitId = visit.getVisitId();
				
				//Create Encounter
				Form form = Context.getFormService().getFormByUuid(TB_SCREENING_FORM_UUID);
				EncounterType tbScreeningEncounterType = Context.getEncounterService()
				        .getEncounterTypeByUuid(TB_SCREENING_ENCOUNTER_TYPE_UUID);
				Encounter encounter = BotswanaEmrUtils.createEncounter(patient, tbScreeningEncounterType, location, form,
				    visit);
				encounter.setEncounterDatetime(startDate);
				
				//Save obs
				// Fever
				
				if (StringUtils.isNotEmpty(tbScreening.getFever())) {
					Obs obs = createObs(encounter, FEVER,
					    tbScreening.getFever().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// Cough
				if (StringUtils.isNotEmpty(tbScreening.getCough())) {
					Obs obs = createObs(encounter, COUGH,
					    tbScreening.getCough().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// Weight loss
				if (StringUtils.isNotEmpty(tbScreening.getWeightLoss())) {
					Obs obs = createObs(encounter, WEIGHT_LOSS,
					    tbScreening.getWeightLoss().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// Night sweats
				if (StringUtils.isNotEmpty(tbScreening.getNightSweat())) {
					Obs obs = createObs(encounter, NIGHT_SWEAT,
					    tbScreening.getNightSweat().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// TB positive contact
				if (StringUtils.isNotEmpty(tbScreening.getTbPositiveContact())) {
					Obs obs = createObs(encounter, TB_CONTACT_LAST_2_YEARS,
					    tbScreening.getTbPositiveContact().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// Enlarged lymph nodes
				if (StringUtils.isNotEmpty(tbScreening.getEnlargedLymphNode())) {
					Obs obs = createObs(encounter, ENLARGED_LYMPH_NODES,
					    tbScreening.getEnlargedLymphNode().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// Decreased playfulness
				if (StringUtils.isNotEmpty(tbScreening.getDecreasedPlayfulness())) {
					Obs obs = createObs(encounter, DECREASED_PLAYFULNESS,
					    tbScreening.getDecreasedPlayfulness().equalsIgnoreCase("yes") ? yesConcept : noConcept, "");
					encounter.addObs(obs);
				}
				
				// HIV status
				if (StringUtils.isNotEmpty(tbScreening.getHivStatus())) {
					Concept concept = positiveConcept;
					if (tbScreening.getHivStatus().equalsIgnoreCase("unknown")) {
						concept = unknownConcept;
					} else if (tbScreening.getHivStatus().equalsIgnoreCase("negative")) {
						concept = negativeConcept;
					}
					Obs obs = createObs(encounter, DECREASED_PLAYFULNESS, concept, "");
					encounter.addObs(obs);
				}
				
				Context.getEncounterService().saveEncounter(encounter);
				
				DateTime dateTime = new DateTime(startDate).millisOfDay().withMaximumValue();
				Context.getVisitService().endVisit(visit, dateTime.toDate());
				succeeded.add(simpleObject);
			}
			catch (Exception e) {
				simpleObject.add("message", e.getMessage());
				failed.add(simpleObject);
				if (visitId != null) {
					Visit visit = Context.getVisitService().getVisit(visitId);
					if (visit != null) {
						visit.setVoided(true);
						Context.getVisitService().voidVisit(visit, "exception handling");
					}
				}
			}
		}
		
		if (failed.size() == 0) {
			batchResponse.setStatus(HttpStatus.OK.toString());
			batchResponse.setResponse("Success");
		} else if (failed.size() < payload.size()) {
			batchResponse.setStatus(HttpStatus.MULTI_STATUS.toString());
			batchResponse.setResponse("Partial Success");
		} else {
			batchResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			batchResponse.setResponse("Failed");
		}
		batchResponse.setFailed(failed);
		batchResponse.setSucceeded(succeeded);
		return batchResponse;
	}
	
	private Visit createVisit(Patient patient, VisitType visitType, Date startTime, Location location) {
		Visit visit = new Visit(patient, visitType, startTime);
		visit.setLocation(location);
		return Context.getVisitService().saveVisit(visit);
	}
	
	private Obs createObs(Encounter encounter, String conceptUuid, Concept coded, String valueText) {
		Concept concept = Context.getConceptService().getConceptByUuid(conceptUuid);
		Obs obs = BotswanaEmrUtils.creatObs(encounter, concept);
		if (coded != null) {
			obs.setValueCoded(coded);
		} else if (StringUtils.isNotEmpty(valueText)) {
			obs.setValueText(valueText);
		}
		
		return obs;
	}
	
	@RequestMapping(value = "/batchPatientContacts", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BatchResponse handleBatchPatientContactPersons(HttpSession httpSession,
	        @RequestBody(required = false) List<PatientContactPerson> payload) {
		BatchResponse batchResponse = new BatchResponse();
		List<SimpleObject> failed = new ArrayList<>();
		List<SimpleObject> succeeded = new ArrayList<>();
		int counter = 0;
		
		for (PatientContactPerson contactPerson : payload) {
			counter++;
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.add("patientId", contactPerson.getIdNumber());
			simpleObject.add("migrationPatientId", contactPerson.getMigrationPatientId());
			simpleObject.add("migrationFacilityId", contactPerson.getMigrationFacilityId());
			simpleObject.add("recordId", contactPerson.getRecordId());
			
			try {
				// Search DB to check if patient is already registered, ignore if patient records already exist.
				// List<Person> people = Context.getService(BotswanaEmrService.class).getPatientByMigrationMetadata(
				//    contactPerson.getMigrationFacilityId(), contactPerson.getMigrationPatientId());
				
				List<PatientIdentifierType> allowedIdTypes = new ArrayList<>();
				PatientIdentifierType patientIdentifierType = getIdentifierType(contactPerson.getIdType());
				allowedIdTypes.add(patientIdentifierType);
				List<Patient> people = Context.getPatientService().getPatients(null, contactPerson.getIdNumber(),
				    allowedIdTypes, true);
				
				if (people == null || people.isEmpty()) {
					throw new Exception(
					        String.format("Patient with id %s not found", contactPerson.getMigrationPatientId()));
				}
				if (people.size() > 1) {
					throw new Exception(String.format("Multiple Patient records with id %s found",
					    contactPerson.getMigrationPatientId()));
				}
				
				Patient patient = Context.getPatientService().getPatient(people.get(0).getPersonId());
				
				if (patient == null) {
					patient = Context.getPatientService().getPatientByUuid(contactPerson.getPatientId());
				}
				
				if (patient == null) {
					throw new Exception(
					        String.format("Patient with id %s not found", contactPerson.getMigrationPatientId()));
				}
				
				// TODO: Check for existing patients
				Person nokPerson = new Person();
				
				//Set nok name
				String[] names = contactPerson.getNokFullName().split("\\s+");
				PersonName nokPersonName = new PersonName();
				if (names.length < 2) {
					break;
				} else if (names.length == 2) {
					nokPersonName.setFamilyName(names[0]);
					nokPersonName.setGivenName(names[1]);
				} else {
					nokPersonName.setFamilyName(names[0]);
					nokPersonName.setGivenName(names[1]);
					nokPersonName.setMiddleName(names[2]);
				}
				
				PersonAddress nokAddress = new PersonAddress();
				nokAddress.setAddress2(contactPerson.getDistrict());
				nokAddress.setCityVillage(contactPerson.getCity());
				nokPerson.addAddress(nokAddress);
				
				Set<PersonName> nokNames = new TreeSet<PersonName>();
				nokNames.add(nokPersonName);
				nokPerson.setNames(nokNames);
				
				// Nok ID Type
				PersonAttribute nokIdTypeAttribute = setPersonAttributeValue(contactPerson.getIdType(),
				    NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokIdTypeAttribute);
				
				// Nok ID
				PersonAttribute nokIdAttribute = setPersonAttributeValue(contactPerson.getNokIdNumber(),
				    NOK_ID_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokIdAttribute);
				
				// Nok Relationship
				PersonAttribute nokRelationshipAttribute = setPersonAttributeValue(contactPerson.getNokRelationship(),
				    NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokRelationshipAttribute);
				
				// Nok Contact1
				PersonAttribute nokContact1Attribute = setPersonAttributeValue(contactPerson.getNokContact(),
				    NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokContact1Attribute);
				
				// Nok Contact2
				PersonAttribute nokContact2Attribute = setPersonAttributeValue(contactPerson.getExtraNokContact(),
				    NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokContact2Attribute);
				
				// Nok Contact3
				PersonAttribute noContact3Attribute = setPersonAttributeValue(contactPerson.getThirdContact(),
				    NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(noContact3Attribute);
				
				// Nok Contact4
				PersonAttribute nokContact4Attribute = setPersonAttributeValue(contactPerson.getFourthContact(),
				    NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokContact4Attribute);
				
				// Nok Contact5
				PersonAttribute nokContact5Attribute = setPersonAttributeValue(contactPerson.getFifthContact(),
				    NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokContact5Attribute);
				
				// Nok email
				PersonAttribute nokEmailAttribute = setPersonAttributeValue(contactPerson.getNokEmail(),
				    NOK_EMAIL_ATTRIBUTE_TYPE_UUID);
				nokPerson.addAttribute(nokEmailAttribute);
				
				// Contact person type
				PersonAttribute nokTypeAttribute = setPersonAttributeValue(contactPerson.getContactPersonType(),
				    NOK_CONTACT_TYPE_TYPE_UUID);
				nokPerson.addAttribute(nokTypeAttribute);
				
				Person savedPerson = Context.getPersonService().savePerson(nokPerson);
				
				// Set relationship
				List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
				
				for (RelationshipType relationshipType : relationshipTypes) {
					if (relationshipType.getaIsToB().toLowerCase()
					        .contains(contactPerson.getNokRelationship().toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						relationship.setPersonB(savedPerson);
						relationship.setPersonA(patient.getPerson());
						Context.getPersonService().saveRelationship(relationship);
						break;
					} else if (relationshipType.getbIsToA().toLowerCase()
					        .contains(contactPerson.getNokRelationship().toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						relationship.setPersonA(savedPerson);
						relationship.setPersonB(patient.getPerson());
						Context.getPersonService().saveRelationship(relationship);
						break;
					}
				}
				
				succeeded.add(simpleObject);
				
			}
			catch (Exception e) {
				simpleObject.add("message", e.getMessage());
				failed.add(simpleObject);
			}

			if (counter % 20 == 0) {
				counter = 0;
				Context.flushSession();
			}
		}
		
		if (failed.size() == 0) {
			batchResponse.setStatus(HttpStatus.OK.toString());
			batchResponse.setResponse("Success");
		} else if (failed.size() < payload.size()) {
			batchResponse.setStatus(HttpStatus.MULTI_STATUS.toString());
			batchResponse.setResponse("Partial Success");
		} else {
			batchResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			batchResponse.setResponse("Failed");
		}
		batchResponse.setFailed(failed);
		batchResponse.setSucceeded(succeeded);
		
		return batchResponse;
	}
	
	@RequestMapping(value = "/batchRegistration", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BatchResponse handleBatchRegistration(HttpSession httpSession,
	        @RequestBody(required = false) List<PatientRegistration> payload) throws ParseException {
		BatchResponse batchResponse = new BatchResponse();
		List<SimpleObject> failed = new ArrayList<>();
		List<SimpleObject> succeeded = new ArrayList<>();
		int counter = 0;
		for (PatientRegistration patientRegistration : payload) {
			counter += 1;
			Location location = null;
			try {
				// Can we pass the Facility MFL Code here ?
				location = getLocation(patientRegistration.getMigrationFacilityId(), patientRegistration.getLocation());
				
				BotswanaRegistryResponse response = registerPatient(patientRegistration,
				    location == null ? null : location.getLocationId());
				
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.add("patientId", patientRegistration.getIdNumber());
				simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
				simpleObject.add("migrationFacilityId", patientRegistration.getMigrationFacilityId());
				simpleObject.add("message", response.getResponse());
				
				if (!response.getStatus().equals(HttpStatus.OK.toString())) {
					failed.add(simpleObject);
				} else {
					succeeded.add(simpleObject);
				}
			}
			catch (Exception e) {
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.add("patientId", patientRegistration.getIdNumber());
				simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
				simpleObject.add("migrationFacilityId", patientRegistration.getMigrationFacilityId());
				simpleObject.add("message", e.getMessage());
				failed.add(simpleObject);
			}
			
			if (counter % 20 == 0) {
				counter = 0;
				Context.flushSession();
			}
		}
		
		if (failed.size() == 0) {
			batchResponse.setStatus(HttpStatus.OK.toString());
			batchResponse.setResponse("Success");
		} else if (failed.size() < payload.size()) {
			batchResponse.setStatus(HttpStatus.MULTI_STATUS.toString());
			batchResponse.setResponse("Partial Success");
		} else {
			batchResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			batchResponse.setResponse("Failed");
		}
		batchResponse.setFailed(failed);
		batchResponse.setSucceeded(succeeded);
		
		return batchResponse;
	}
	
	@RequestMapping(value = "/batchRegistration", method = RequestMethod.PUT, consumes = "application/json")
	public @ResponseBody BatchResponse handleBatchRegistrationUpdate(HttpSession httpSession,
	        @RequestBody(required = false) List<PatientRegistration> payload) throws ParseException {
		
		BatchResponse batchResponse = new BatchResponse();
		List<SimpleObject> failed = new ArrayList<>();
		List<SimpleObject> succeeded = new ArrayList<>();
		int counter = 0;
		for (PatientRegistration patientRegistration : payload) {
			counter += 1;
			Location location = null;
			try {
				// Can we pass the Facility MFL Code here ?
				location = getLocation(patientRegistration.getMigrationFacilityId(), patientRegistration.getLocation());
				
				BotswanaRegistryResponse response = updatePatient(patientRegistration,
				    location == null ? null : location.getLocationId());
				
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.add("patientId", patientRegistration.getIdNumber());
				simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
				simpleObject.add("migrationFacilityId", patientRegistration.getMigrationFacilityId());
				simpleObject.add("message", response.getResponse());
				
				if (!response.getStatus().equals(HttpStatus.OK.toString())) {
					failed.add(simpleObject);
				} else {
					succeeded.add(simpleObject);
				}
			}
			catch (Exception e) {
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.add("patientId", patientRegistration.getIdNumber());
				simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
				simpleObject.add("migrationFacilityId", patientRegistration.getMigrationFacilityId());
				simpleObject.add("message", e.getMessage());
				failed.add(simpleObject);
			}
			
			if (counter % 20 == 0) {
				counter = 0;
				Context.flushSession();
			}
		}
		
		if (failed.size() == 0) {
			batchResponse.setStatus(HttpStatus.OK.toString());
			batchResponse.setResponse("Success");
		} else if (failed.size() < payload.size()) {
			batchResponse.setStatus(HttpStatus.MULTI_STATUS.toString());
			batchResponse.setResponse("Partial Success");
		} else {
			batchResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			batchResponse.setResponse("Failed");
		}
		batchResponse.setFailed(failed);
		batchResponse.setSucceeded(succeeded);
		
		return batchResponse;
	}
	
	// Batch NOK details registration
	@RequestMapping(value = "/batchNokRegistration", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BatchResponse handleBatchNokRegistration(HttpSession httpSession,
	        @RequestBody(required = false) List<PatientRegistration> payload) throws ParseException {
		BatchResponse batchResponse = new BatchResponse();
		List<SimpleObject> failed = new ArrayList<>();
		List<SimpleObject> succeeded = new ArrayList<>();
		int counter = 0;
		
		for (PatientRegistration patientRegistration : payload) {
			Patient patient = findPatient(patientRegistration.getIdType(), patientRegistration.getIdNumber());
			if (patient != null) {
				List<Relationship> relationshipList = retrieveRelationshipList(patientRegistration);
				RegistrationData registrationData = new RegistrationData();
				registrationData.setPatient(patient);
				
				registrationData.setRelationships(relationshipList);
				try {
					saveRegistrationDetails(registrationData);
					SimpleObject responseObject = new SimpleObject();
					responseObject.add("patientId", patientRegistration.getIdNumber());
					responseObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
					responseObject.add("migrationFacilityId", patientRegistration.getMigrationFacilityId());
					
					SimpleObject messageObject = new SimpleObject();
					messageObject.add("Patient", ConversionUtil.convertToRepresentation(patient, Representation.REF));
					messageObject.add("Message", "Patient NOK details updated successfully");
					responseObject.add("message", messageObject);
					succeeded.add(responseObject);
					
				}
				catch (APIException e) {
					SimpleObject simpleObject = new SimpleObject();
					simpleObject.add("patientId", patientRegistration.getIdNumber());
					simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
					simpleObject.add("message", e.getMessage());
					failed.add(simpleObject);
				}
			} else {
				SimpleObject simpleObject = new SimpleObject();
				simpleObject.add("patientId", patientRegistration.getIdNumber());
				simpleObject.add("migrationPatientId", patientRegistration.getMigrationPatientId());
				simpleObject.add("message", "Patient not found");
				failed.add(simpleObject);
			}
			if (counter % 20 == 0) {
				counter = 0;
				Context.flushSession();
			}
		}
		
		if (failed.size() == 0) {
			batchResponse.setStatus(HttpStatus.OK.toString());
			batchResponse.setResponse("Success");
		} else if (failed.size() < payload.size()) {
			batchResponse.setStatus(HttpStatus.MULTI_STATUS.toString());
			batchResponse.setResponse("Partial Success");
		} else {
			batchResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			batchResponse.setResponse("Failed");
		}
		
		batchResponse.setFailed(failed);
		batchResponse.setSucceeded(succeeded);
		
		return batchResponse;
		
	}
	
	private Location getLocation(String mflCode, String locationName) {
		List<Location> locations = BotswanaEmrUtils.getLocationsWithMFLCode(mflCode);
		
		Location location = null;
		if (locations == null || locations.isEmpty()) {
			if (StringUtils.isNotEmpty(locationName)) {
				location = BotswanaEmrUtils.getLocation(locationName);
			}
		} else {
			location = locations.get(0);
		}
		return location;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody BotswanaRegistryResponse SavePatientRegistrationDetails(HttpSession httpSession,
	        @RequestBody Map<String, Object> payload) throws ParseException {
		Integer locationId = (Integer) httpSession.getAttribute(UiSessionContext.LOCATION_SESSION_ATTRIBUTE);
		
		PatientRegistration patientRegistration = mapper.convertValue(payload, PatientRegistration.class);
		return registerPatient(patientRegistration, locationId);
	}
	
	private BotswanaRegistryResponse registerPatient(PatientRegistration patientRegistration, @Nullable Integer locationId)
	        throws ParseException {
		Patient patient = new Patient();
		
		BotswanaRegistryResponse botswanaRegistryResponse = new BotswanaRegistryResponse();
		
		// Search DB to check if patient is already registered, ignore if patient records already exist.
		// List<Person> people = Context.getService(BotswanaEmrService.class).getPatientByMigrationMetadata(
		//    patientRegistration.getMigrationFacilityId(), patientRegistration.getMigrationPatientId());
		
		Person person = findPatient(patientRegistration.getIdType(), patientRegistration.getIdNumber());
		// if people is not null and not empty, check if at least one of the patients has the same name as the one being registered
		Boolean existingPatient = false;
		if (person != null) {
			// check if the person has the same name as the one being registered
			if (person.getPersonName().getFullName().contains(patientRegistration.getGivenName())
			        || person.getPersonName().getFullName().contains(patientRegistration.getFamilyName())) {
				existingPatient = true;
			}
			
			if (!existingPatient) {
				//botswanaRegistryResponse.setResponse("Patient record already migrated");
				botswanaRegistryResponse
				        .setResponse(String.format("Patient with id %s already exists", patientRegistration.getIdNumber()));
				botswanaRegistryResponse.setStatus(HttpStatus.CONFLICT.toString());
				return botswanaRegistryResponse;
			}
		}
		
		// if (people == null || people.isEmpty()) {
		if (!existingPatient) {
			PersonName personName = new PersonName();
			PatientIdentifier openMrsId = validateOrGenerateIdentifier("", null);
			patient.addIdentifier(openMrsId);
			
			personName.setFamilyName(patientRegistration.getFamilyName());
			personName.setGivenName(patientRegistration.getGivenName());
			personName.setMiddleName(patientRegistration.getMiddleName());
			Set<PersonName> personNames = new TreeSet<>();
			personNames.add(personName);
			patient.setNames(personNames);
			
			String gender = patientRegistration.getGender();
			if (gender.equalsIgnoreCase("Male")) {
				patient.setGender(MALE_GENDER);
			} else if (gender.equalsIgnoreCase("Female")) {
				patient.setGender(FEMALE_GENDER);
			} else {
				patient.setGender(OTHER_GENDER);
			}
			
			// Set patient dob
			String dob = patientRegistration.getDob();
			Date birthDate = (new SimpleDateFormat(
			        Context.getAdministrationService().getGlobalProperty("botswanaemr.defaultDateFormat", "dd/MM/yyyy")))
			                .parse(dob);
			patient.setBirthdate(birthDate);
			patient.setBirthdateEstimated(patientRegistration.isDobEstimated());
			
			// Patient Type
			PersonAttribute patientTypeAttribute = setPersonAttributeValue(patientRegistration.getPatientType(),
			    PATIENT_TYPE_ATTRIBUTE_TYPE_UUID);
			PersonAttribute patientFacilityLocationAttribute = setPersonAttributeValue(
			    patientRegistration.getPatientFacilityLocation(), PERSON_LOCATION_ATTRIBUTE);
			patient.addAttribute(patientTypeAttribute);
			patient.addAttribute(patientFacilityLocationAttribute);
			
			PersonAttribute migrationPatientIdAttribute = setPersonAttributeValue(
			    patientRegistration.getMigrationPatientId(), MIGRATION_PATIENT_ATTRIBUTE);
			patient.addAttribute(migrationPatientIdAttribute);
			
			PersonAttribute migrationFacilityIdAttribute = setPersonAttributeValue(
			    patientRegistration.getMigrationFacilityId(), MIGRATION_FACILITY_ATTRIBUTE);
			patient.addAttribute(migrationFacilityIdAttribute);
			
			// Set patient identifier
			if (StringUtils.isNotEmpty(patientRegistration.getIdNumber())) {
				PatientIdentifier patientIdentifier = getDefaultIdentifier(patientRegistration.getIdNumber(),
				    patientRegistration.getIdType(), locationId);
				patient.addIdentifier(patientIdentifier);
			}
			
			if (patientRegistration.getPatientType().equals(REGULAR_PATIENT)) {
				// Set address
				PersonAddress personAddress = new PersonAddress();
				if (StringUtils.isNotEmpty(patientRegistration.getCountry())) {
					personAddress.setCountry(patientRegistration.getCountry());
				}
				personAddress.setAddress2(patientRegistration.getAddress2());
				personAddress.setCityVillage(patientRegistration.getCityVillage());
				personAddress.setAddress4(patientRegistration.getAddress4());
				personAddress.setAddress5(patientRegistration.getAddress5());
				personAddress.setAddress6(patientRegistration.getAddress6());
				personAddress.setAddress7(patientRegistration.getAddress7());
				patient.addAddress(personAddress);
				
				Set<PersonAttribute> personAttributes = setPersonAttributes(patientRegistration, patient);
				
				patient.setAttributes(personAttributes);
				
			} else {
				// Emergency patient type
				PersonAttribute attribute = setPersonAttributeValue(patientRegistration.getEmergencyPatientType(),
				    EMERGENCY_PATIENT_TYPE_ATTRIBUTE_TYPE_UUID);
				patient.addAttribute(attribute);
			}
			
		}
		
		List<Relationship> relationshipList = retrieveRelationshipList(patientRegistration);
		
		RegistrationData registrationData = new RegistrationData();
		registrationData.setPatient(patient);
		// registrationData.setIdentifier(openMrsId.getIdentifier());
		
		//		ArrayList<org.openmrs.ui.framework.SimpleObject> nokRelationShips = patientRegistration.getRelationShips();
		//		if (nokRelationShips != null) {
		//			List<Relationship> relationships = getNewPatientRelationShips(nokRelationShips);
		//			registrationData.setRelationships(relationships);
		//		}
		
		registrationData.setRelationships(relationshipList);
		
		try {
			// TODO: Move the Patient Registration logic to a custom class that extends the RegistrationCoreService
			// registrationCoreService.registerPatient(registrationData);
			Patient newPatient = saveRegistrationDetails(registrationData);
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.add("Patient", ConversionUtil.convertToRepresentation(newPatient, Representation.REF));
			simpleObject.add("Message", "Patient registered successfully");
			botswanaRegistryResponse.setResponse(simpleObject);
			botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
			
		}
		catch (Exception e) {
			botswanaRegistryResponse.setResponse("Unable to register patient: " + e.getMessage());
			botswanaRegistryResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
		// } else {
		// 	botswanaRegistryResponse.setResponse("Patient record already migrated");
		// 	botswanaRegistryResponse.setStatus(HttpStatus.CONFLICT.toString());
		// }
		
		return botswanaRegistryResponse;
	}
	
	private BotswanaRegistryResponse updatePatient(PatientRegistration patientRegistration, @Nullable Integer locationId)
	        throws ParseException {
		
		BotswanaRegistryResponse botswanaRegistryResponse = new BotswanaRegistryResponse();
		
		// Search DB to check if patient is already registered, ignore if patient records already exist.
		// List<Person> people = Context.getService(BotswanaEmrService.class).getPatientByMigrationMetadata(
		//    patientRegistration.getMigrationFacilityId(), patientRegistration.getMigrationPatientId());
		
		Patient patient = findPatient(patientRegistration.getIdType(), patientRegistration.getIdNumber());
		// if people is not null and not empty, check if at least one of the patients has the same name as the one being registered
		if (patient == null) {
			botswanaRegistryResponse.setResponse("Patient record not found");
			botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
			return botswanaRegistryResponse;
		}
		
		Set<PersonAttribute> personAttributes = setPersonAttributes(patientRegistration, patient);
		
		patient.setAttributes(personAttributes);
		
		RegistrationData registrationData = new RegistrationData();
		registrationData.setPatient(patient);
		
		try {
			Patient udatedPatient = saveRegistrationDetails(registrationData);
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.add("Patient", ConversionUtil.convertToRepresentation(udatedPatient, Representation.REF));
			simpleObject.add("Message", "Patient updated successfully");
			botswanaRegistryResponse.setResponse(simpleObject);
			botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
			
		}
		catch (Exception e) {
			botswanaRegistryResponse.setResponse("Unable to update patient: " + e.getMessage());
			botswanaRegistryResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
		}
		return botswanaRegistryResponse;
		
	}
	
	private Set<PersonAttribute> setPersonAttributes(PatientRegistration patientRegistration, Person patient) {
		Set<PersonAttribute> personAttributes = patient.getAttributes();
		
		if (personAttributes == null) {
			personAttributes = new HashSet<>();
		}
		
		// Update the person contact details
		// Set Citizen type
		updatePersonAttributes(CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID, patientRegistration.getCitizenType(), personAttributes);
		
		// Occupation
		updatePersonAttributes(OCCUPATION_ATTRIBUTE_TYPE_UUID, patientRegistration.getOccupation(), personAttributes);
		
		// ForceType
		updatePersonAttributes(PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID, patientRegistration.getAffiliation(),
		    personAttributes);
		
		// ForceNumber
		updatePersonAttributes(FORCE_NUMBER_ATTRIBUTE_TYPE_UUID, patientRegistration.getForceNumber(), personAttributes);
		
		// Rank
		updatePersonAttributes(RANK_ATTRIBUTE_TYPE_UUID, patientRegistration.getRank(), personAttributes);
		
		// Other Occupation
		updatePersonAttributes(OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID, patientRegistration.getOtherOccupation(),
		    personAttributes);
		
		// Employer Name
		updatePersonAttributes(EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID, patientRegistration.getEmployerName(), personAttributes);
		
		// Contact Number
		updatePersonAttributes(PHONE_NUMBER_ATTRIBUTE_TYPE_UUID, patientRegistration.getContactNumber(), personAttributes);
		
		// Alternative Contact Number
		updatePersonAttributes(ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltContactNumber(),
		    personAttributes);
		
		// Alternative Contact Number 2
		updatePersonAttributes(ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltContactNumber2(),
		    personAttributes);
		
		// Alternative Contact Number 3
		updatePersonAttributes(ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltContactNumber3(),
		    personAttributes);
		
		// Alternative Contact Number 4
		updatePersonAttributes(ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltContactNumber4(),
		    personAttributes);
		
		// Alternative Contact Number 5
		updatePersonAttributes(ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltContactNumber5(),
		    personAttributes);
		
		// Number Type
		updatePersonAttributes(NUMBER_TYPE_ATTRIBUTE_TYPE_UUID, patientRegistration.getNumberType(), personAttributes);
		
		// Alt Number type # 1
		updatePersonAttributes(ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltNumberType1(),
		    personAttributes);
		
		// Alt Number type # 2
		updatePersonAttributes(ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltNumberType2(),
		    personAttributes);
		
		// Alt Number type # 3
		updatePersonAttributes(ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltNumberType3(),
		    personAttributes);
		
		// Alt Number type # 4
		updatePersonAttributes(ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID, patientRegistration.getAltNumberType4(),
		    personAttributes);
		
		// Set Email
		updatePersonAttributes(EMAIL_ATTRIBUTE_TYPE_UUID, patientRegistration.getEmail(), personAttributes);
		
		// Set Education level
		updatePersonAttributes(EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID, patientRegistration.getEducation(), personAttributes);
		
		// Set EMPLOYMENT SECTOR
		updatePersonAttributes(EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE, patientRegistration.getEmploymentSector(),
		    personAttributes);
		
		// Marital Status
		updatePersonAttributes(MARITAL_STATUS_ATTRIBUTE_TYPE, patientRegistration.getMaritalStatus(), personAttributes);
		
		return personAttributes;
	}
	
	private void updatePersonAttributes(String attributeTypeUuid, String attributeValue,
	        Set<PersonAttribute> personAttributes) {
		if (StringUtils.isNotEmpty(attributeValue)) {
			if (personAttributes != null) {
				// personAttributes
				//        .removeIf(personAttribute -> personAttribute.getAttributeType().getUuid().equals(attributeTypeUuid));
				Iterator<PersonAttribute> iterator = personAttributes.iterator();
				while (iterator.hasNext()) {
					PersonAttribute personAttribute = iterator.next();
					if (personAttribute.getAttributeType().getUuid().equals(attributeTypeUuid)) {
						personAttributes.remove(personAttribute);
						// void the attribute and add it again to avoid duplicates
						personAttribute.setVoided(true);
						personAttribute.setVoidReason("Updating attribute");
						personAttribute.setVoidedBy(Context.getAuthenticatedUser());
						personAttribute.setDateVoided(new Date());
						personAttributes.add(personAttribute);
						break;
					}
				}
			}
			
			PersonAttribute employerNameAttribute = setPersonAttributeValue(attributeValue, attributeTypeUuid);
			personAttributes.add(employerNameAttribute);
		}
	}
	
	private Patient findPatient(String idType, String idNumber) {
		List<PatientIdentifierType> allowedIdTypes = new ArrayList<>();
		PatientIdentifierType patientIdentifierType = getIdentifierType(idType);
		allowedIdTypes.add(patientIdentifierType);
		List<Patient> people = Context.getPatientService().getPatients(null, idNumber, allowedIdTypes, true);
		
		if (people == null || people.isEmpty()) {
			return null;
		} else if (people.size() > 1) {
			throw new RuntimeException(String.format("Multiple Patient records with id %s (%s) found", idNumber, idType));
		} else {
			return people.get(0);
		}
	}
	
	private List<Relationship> retrieveRelationshipList(PatientRegistration patientRegistration) {
		List<Relationship> relationshipList = new ArrayList<>();
		for (PatientRegistration.NextOfKin nextOfKin : patientRegistration.getNok()) {
			// TODO: Check for existing patients
			Person nokPerson = new Person();
			
			//Set nok name
			String[] names = nextOfKin.getNokFullName().split("\\s+");
			PersonName nokPersonName = new PersonName();
			if (names.length < 2) {
				break;
			} else if (names.length == 2) {
				nokPersonName.setFamilyName(names[0]);
				nokPersonName.setGivenName(names[1]);
			} else {
				nokPersonName.setFamilyName(names[0]);
				nokPersonName.setGivenName(names[1]);
				nokPersonName.setMiddleName(names[2]);
			}
			
			PersonAddress nokAddress = new PersonAddress();
			nokAddress.setAddress2(nextOfKin.getDistrict());
			nokAddress.setCityVillage(nextOfKin.getCity());
			nokPerson.addAddress(nokAddress);
			
			Set<PersonName> nokNames = new TreeSet<PersonName>();
			nokNames.add(nokPersonName);
			nokPerson.setNames(nokNames);
			
			// Nok ID Type
			PersonAttribute nokIdTypeAttribute = setPersonAttributeValue(nextOfKin.getIdType(),
			    NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokIdTypeAttribute);
			
			// Nok ID
			PersonAttribute nokIdAttribute = setPersonAttributeValue(nextOfKin.getNokIdNumber(), NOK_ID_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokIdAttribute);
			
			// Nok Relationship
			PersonAttribute nokRelationshipAttribute = setPersonAttributeValue(nextOfKin.getNokRelationship(),
			    NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokRelationshipAttribute);
			
			PersonAttribute otherNokRelationshipAttribute = setPersonAttributeValue(nextOfKin.getOtherNokRelationship(),
			    OTHER_NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(otherNokRelationshipAttribute);
			
			// Nok Contact1
			PersonAttribute nokContact1Attribute = setPersonAttributeValue(nextOfKin.getNokContact(),
			    NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokContact1Attribute);
			
			// Nok Contact2
			PersonAttribute nokContact2Attribute = setPersonAttributeValue(nextOfKin.getExtraNokContact(),
			    NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokContact2Attribute);
			
			// Nok Contact3
			PersonAttribute noContact3Attribute = setPersonAttributeValue(nextOfKin.getThirdContact(),
			    NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(noContact3Attribute);
			
			// Nok Contact4
			PersonAttribute nokContact4Attribute = setPersonAttributeValue(nextOfKin.getFourthContact(),
			    NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokContact4Attribute);
			
			// Nok Contact5
			PersonAttribute nokContact5Attribute = setPersonAttributeValue(nextOfKin.getFifthContact(),
			    NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokContact5Attribute);
			
			// Nok email
			PersonAttribute nokEmailAttribute = setPersonAttributeValue(nextOfKin.getNokEmail(),
			    NOK_EMAIL_ATTRIBUTE_TYPE_UUID);
			nokPerson.addAttribute(nokEmailAttribute);
			
			// Contact person type
			PersonAttribute nokTypeAttribute = setPersonAttributeValue(nextOfKin.getContactPersonType(),
			    NOK_CONTACT_TYPE_TYPE_UUID);
			nokPerson.addAttribute(nokTypeAttribute);
			
			Person savedPerson = Context.getPersonService().savePerson(nokPerson);
			
			// Set relationship
			List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
			for (RelationshipType relationshipType : relationshipTypes) {
				if (relationshipType.getaIsToB().toLowerCase().contains(nextOfKin.getNokRelationship().toLowerCase())) {
					Relationship relationship = new Relationship();
					relationship.setRelationshipType(relationshipType);
					relationship.setPersonB(savedPerson);
					relationshipList.add(relationship);
					break;
				} else if (relationshipType.getbIsToA().toLowerCase()
				        .contains(nextOfKin.getNokRelationship().toLowerCase())) {
					Relationship relationship = new Relationship();
					relationship.setRelationshipType(relationshipType);
					relationship.setPersonA(savedPerson);
					relationshipList.add(relationship);
					break;
				}
			}
		}
		return relationshipList;
	}
	
	@RequestMapping(value = "/auditLogs", method = RequestMethod.GET)
	public @ResponseBody List<SimplifiedAuditLog> getAuditLogs(@RequestParam String objectId,
	        @RequestParam String objectType) {
		List<SimplifiedAuditLog> simplifiedAuditLogs = new ArrayList<>();
		
		if (objectType.equals("Encounter")) {
			List<AuditLog> auditLogs = Context.getService(AuditLogService.class).getAuditLogs(objectId, Encounter.class,
			    null, null, null, false);
			for (AuditLog auditLog : auditLogs) {
				try {
					SimplifiedAuditLog simplifiedAuditLog = new SimplifiedAuditLog(auditLog);
					simplifiedAuditLogs.add(simplifiedAuditLog);
				}
				catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		return simplifiedAuditLogs;
	}
	
	@RequestMapping(value = "/auditLogsWithList", method = RequestMethod.GET)
	public @ResponseBody List<SimplifiedAuditLog> auditLogsWithList(@RequestParam List<String> objectIds,
	        @RequestParam String objectType) {
		List<SimplifiedAuditLog> simplifiedAuditLogs = new ArrayList<>();
		
		if (objectType.equals("Encounter")) {
			List<AuditLog> auditLogs = Context.getService(AuditLogService.class).getAuditLogsWithIds(objectIds,
			    Encounter.class, null, null, null, false, null, null);
			for (AuditLog auditLog : auditLogs) {
				try {
					SimplifiedAuditLog simplifiedAuditLog = new SimplifiedAuditLog(auditLog);
					simplifiedAuditLogs.add(simplifiedAuditLog);
				}
				catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		return simplifiedAuditLogs;
	}
	
	private PersonAttribute setPersonAttributeValue(String personAttributeValue, String personAttributeTypeUuid) {
		if (personAttributeTypeMap.get(personAttributeTypeUuid) == null) {
			personAttributeTypeMap.put(personAttributeTypeUuid,
			    Context.getPersonService().getPersonAttributeTypeByUuid(personAttributeTypeUuid));
		}
		
		PersonAttributeType personAttributeType = personAttributeTypeMap.get(personAttributeTypeUuid);
		PersonAttribute personAttribute = new PersonAttribute();
		personAttribute.setAttributeType(personAttributeType);
		personAttribute.setValue(personAttributeValue);
		
		return personAttribute;
	}
	
	private List<Relationship> getNewPatientRelationShips(
	        ArrayList<org.openmrs.ui.framework.SimpleObject> nokRelationShips) {
		List<Relationship> relationshipList = new ArrayList<>();
		List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
		
		for (RelationshipType relationshipType : relationshipTypes) {
			for (org.openmrs.ui.framework.SimpleObject simpleObject : nokRelationShips) {
				if (simpleObject.get("patientId") != null && simpleObject.get("relationship") != null) {
					if (relationshipType.getaIsToB().toLowerCase()
					        .contains(simpleObject.get("relationship").toString().toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						Patient nokPatient = Context.getPatientService().getPatient((Integer) simpleObject.get("patientId"));
						relationship.setPersonB(nokPatient.getPerson());
						relationshipList.add(relationship);
					} else if (relationshipType.getbIsToA().toLowerCase()
					        .contains(simpleObject.get("relationship").toString().toLowerCase())) {
						Relationship relationship = new Relationship();
						relationship.setRelationshipType(relationshipType);
						Patient nokPatient = Context.getPatientService().getPatient((Integer) simpleObject.get("patientId"));
						relationship.setPersonA(nokPatient.getPerson());
						relationshipList.add(relationship);
					}
				}
			}
		}
		return relationshipList;
	}
	
	public Patient saveRegistrationDetails(RegistrationData registrationData) throws APIException {
		Patient patient = registrationData.getPatient();
		
		if (patient == null) {
			throw new APIException("Patient cannot be null");
		}
		
		patient = patientService.savePatient(patient);
		
		List<Relationship> relationships = registrationData.getRelationships();
		ArrayList<String> relationshipUuids = new ArrayList<String>();
		for (Relationship relationship : relationships) {
			if (relationship.getPersonA() == null) {
				relationship.setPersonA(patient);
			} else if (relationship.getPersonB() == null) {
				relationship.setPersonB(patient);
			} else {
				throw new APIException("Only one side of a relationship should be specified");
			}
			personService.saveRelationship(relationship);
			relationshipUuids.add(relationship.getUuid());
		}
		
		return patient;
	}
	
	private PatientIdentifier getDefaultIdentifier(String identifierString, String idType, Integer locationId) {
		
		PatientIdentifier pId = new PatientIdentifier();
		
		if (idType.equals("noIdentification")) {
			Location location = null;
			if (locationId != null) {
				location = Context.getLocationService().getLocation(locationId);
			}
			String generatedId = BotswanaEmrUtils.generatePatientTemporaryId(location);
			if (StringUtils.isEmpty(generatedId)) {
				throw new APIException("An error occurred. Contact the system administrator");
			}
			identifierString = StringUtils.isNotBlank(generatedId) ? generatedId : identifierString;
		}
		pId.setIdentifier(identifierString);
		PatientIdentifierType IdentifierType = getIdentifierType(idType);
		pId.setIdentifierType(IdentifierType);
		pId.setPreferred(true);
		
		return pId;
	}
	
	private PatientIdentifierType getIdentifierType(String idType) {
		String identifierTypeUuid = "";
		
		switch (idType) {
			case "nationalIdNumber":
				identifierTypeUuid = NATIONAL_ID_IDENTIFIER_TYPE;
				
				break;
			case "passportNumber":
				identifierTypeUuid = PASSPORT_IDENTIFIER_TYPE;
				
				break;
			case "birthCertificateNumber":
				identifierTypeUuid = BIRTH_CERT_IDENTIFIER_TYPE;
				
				break;
			case "noIdentification":
				identifierTypeUuid = TEMPORARY_ID_IDENTIFIER_TYPE_UUID;
				break;
			case "":
				identifierTypeUuid = TEMPORARY_ID_IDENTIFIER_TYPE_UUID;
				
				break;
			
			default:
				throw new APIException("Invalid Patient Identifier Type");
		}
		
		return Context.getPatientService().getPatientIdentifierTypeByUuid(identifierTypeUuid);
	}
	
	private PatientIdentifier validateOrGenerateIdentifier(String identifierString, Location identifierLocation) {
		IdentifierSourceService iss = getIssAndUpdateIdSource();
		identifierLocation = getIdentifierLocation(identifierLocation);
		
		// generate identifier if necessary, otherwise validate
		if (StringUtils.isBlank(identifierString)) {
			identifierString = iss.generateIdentifier(idSource, null);
		} else {
			PatientIdentifierValidator.validateIdentifier(identifierString, idSource.getIdentifierType());
		}
		
		PatientIdentifier pId = new PatientIdentifier(identifierString, idSource.getIdentifierType(), identifierLocation);
		pId.setPreferred(false);
		
		return pId;
	}
	
	private IdentifierSourceService getIssAndUpdateIdSource() {
		IdentifierSourceService iss = Context.getService(IdentifierSourceService.class);
		if (idSource == null) {
			String sourceIdentifier = "1";
			if (StringUtils.isNumeric(sourceIdentifier)) {
				idSource = iss.getIdentifierSource(Integer.valueOf(sourceIdentifier));
			} else {
				idSource = iss.getIdentifierSourceByUuid(sourceIdentifier);
			}
			if (idSource == null) {
				throw new APIException("cannot find identifier source with id:" + sourceIdentifier);
			}
		}
		return iss;
	}
	
	private Location getIdentifierLocation(Location identifierLocation) {
		if (identifierLocation == null) {
			identifierLocation = locationService.getDefaultLocation();
			if (identifierLocation == null) {
				throw new APIException("Failed to resolve location to associate to patient identifiers");
			}
		}
		
		return identifierLocation;
	}
	
	@RequestMapping(value = "/patient/{query}", method = RequestMethod.GET)
	public @ResponseBody List<Patient> findPatientInBotswanaEmr(@PathVariable String query) {
		List<Patient> matchingPatients = new ArrayList<>();
		BotswanaEmrService botswanaEmrService = new BotswanaEmrServiceImpl();
		matchingPatients = botswanaEmrService.getBotswanaEmrPatients(query, true, null, null);
		
		return matchingPatients;
	}
	
	@RequestMapping(path = "/registry/getPatientByAdvancedSearch", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
	public ResponseEntity<BotswanaRegistryResponse> getPatientByAdvancedSearch(
	        @RequestBody AdvancedSearchRegistryPerson advancedSearchRegistryPerson) {
		return ResponseEntity.status(HttpStatus.OK)
		        .body(botswanaPersonRegistryService.getPatientByAdvancedSearch(advancedSearchRegistryPerson));
	}
	
	@RequestMapping(path = "/registry/savePatientInRegistry", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
	public ResponseEntity<BotswanaRegistryResponse> savePatientInClientRegistry(
	        @RequestBody AdvancedSearchRegistryPerson advancedSearchRegistryPerson) throws Exception {
		Patient openmrsPatient = Context.getPatientService().getPatientByUuid(advancedSearchRegistryPerson.getPatientUuid());
		String loggedInAt = advancedSearchRegistryPerson.getLoggedInAt();
		if (openmrsPatient == null) {
			throw new Exception(
			        String.format("Patient with id %s not found", advancedSearchRegistryPerson.getPatientUuid()));
		} else {
			return ResponseEntity.status(HttpStatus.OK)
			        .body(botswanaPersonRegistryService.savePatientInClientRegistry(openmrsPatient, loggedInAt));
		}
	}
}
