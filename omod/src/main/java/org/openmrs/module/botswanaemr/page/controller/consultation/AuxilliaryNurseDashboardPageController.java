/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonCreator;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class AuxilliaryNurseDashboardPageController {
	
	private List<Encounter> allScreenings;
	
	private final String format = "yyyy-MM-dd";
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date yesterDay = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);
		
		//Check for possibility of null pointer to some results
		int allScreeningsCount = 0;
		int todayScreeningsCount = 0;
		int yesterdayScreeningsCount = 0;
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		List<Encounter> allScreeningsList = getAllScreenings(null, null, null, location);
		if (allScreeningsList != null && allScreeningsList.size() > 0) {
			allScreeningsCount = allScreeningsList.size();
		}
		
		if (allScreeningsList != null) {
			List<Encounter> todayScreeningsList = allScreeningsList.stream()
			        .filter(s -> BotswanaEmrUtils.formatDateWithoutTime(s.getEncounterDatetime(), format)
			                .equals(BotswanaEmrUtils.formatDateWithoutTime(today, format)))
			        .collect(Collectors.toList());
			
			if (todayScreeningsList.size() > 0) {
				todayScreeningsCount = todayScreeningsList.size();
			}
			
			List<Encounter> yesterdayScreeningsList = allScreeningsList.stream()
			        .filter(s -> BotswanaEmrUtils.formatDateWithoutTime(s.getEncounterDatetime(), format)
			                .equals(BotswanaEmrUtils.formatDateWithoutTime(yesterDay, format)))
			        .collect(Collectors.toList());
			if (yesterdayScreeningsList.size() > 0) {
				yesterdayScreeningsCount = yesterdayScreeningsList.size();
			}
		}
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		model.addAttribute("allScreeningsCount", allScreeningsCount);
		model.addAttribute("todayScreeningsCount", todayScreeningsCount);
		model.addAttribute("yesterdayScreeningsCount", yesterdayScreeningsCount);
		model.addAttribute("dailyAverageScreenings", getDailyAverageScreenings(location));
		model.addAttribute("todayScreeningActivitiesList", new ArrayList<>());
		// model.addAttribute("todayScreeningActivitiesList",
		//		patientObjects(getAllScreenings(null, null, limitFetch, location)));
	}
	
	private List<Encounter> getAllScreenings(Date start, Date end, Integer limit, Location location) {
		if (allScreenings == null) {
			
			List<Encounter> screenings = Context.getService(BotswanaEmrService.class).getTriageScreeningEncountersList(start,
			    end, limit, location);
			
			if (screenings != null && !screenings.isEmpty()) {
				allScreenings = screenings.stream().filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
				        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(location.getUuid()))
				        .filter(r -> r.getForm() != null).collect(Collectors.toList());
			}
		}
		
		return allScreenings;
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(formatPersonCreator(patient));
				generalPatientObject.setDuration(formatDurationSinceRegistration(patient));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
	
	private Integer getDailyAverageScreenings(Location location) {
		
		int totalScreeningsEver = 0;
		Encounter firstPatientEncounter = null;
		Encounter lastEncounter = null;
		int daysBetweenFirstAndLastEncounterDates = 0;
		
		if (getAllScreenings(null, null, null, location) != null
		        && getAllScreenings(null, null, null, location).size() > 0) {
			totalScreeningsEver = getAllScreenings(null, null, null, location).size();
			firstPatientEncounter = getAllScreenings(null, null, null, location).get(0);
			lastEncounter = getAllScreenings(null, null, null, location)
			        .get(getAllScreenings(null, null, null, location).size() - 1);
		}
		
		if (firstPatientEncounter != null && lastEncounter != null) {
			daysBetweenFirstAndLastEncounterDates = BotswanaEmrUtils.unitsSince(lastEncounter.getEncounterDatetime(),
			    firstPatientEncounter.getEncounterDatetime(), "days");
		}
		
		if (daysBetweenFirstAndLastEncounterDates == 0) {
			daysBetweenFirstAndLastEncounterDates = 1;
		}
		
		return totalScreeningsEver / daysBetweenFirstAndLastEncounterDates;
		
	}
	
}
