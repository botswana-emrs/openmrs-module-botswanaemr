/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.openmrs.Location;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.StockItemSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ItemsLowInStockFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext) {
		final double MINIMUM_STOCK_LEVEL = 1.5;
		
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		List<StockItemSimplifier> stockItemSimplifiers = new ArrayList<>();
		
		if (location != null) {
			List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
			    false);
			Set<ItemStock> itemStocks = null;
			Stockroom mainStockroom = StockUtils.getMainStockroom(sessionContext.getSessionLocation());
			
			for (Stockroom stockRoom : stockrooms) {
				itemStocks = stockRoom.getItems();
				for (ItemStock itemStock : itemStocks) {
					StockItemSimplifier stockItemSimplifier = new StockItemSimplifier();
					double averageMonthlyConsumption = (double) Math
					        .round(StockUtils.calculateAverageMonthlyConsumption(stockRoom, itemStock.getItem()));
					double minimumStockLevel = averageMonthlyConsumption * MINIMUM_STOCK_LEVEL;
					
					if (itemStock.getQuantity() > minimumStockLevel) {
						ItemStockDetail itemStockDetail = itemStock.getDetails().stream().findFirst().orElse(null);
						stockItemSimplifier.setStockRoomName(itemStock.getStockroom().getName());
						stockItemSimplifier.setItemId(itemStock.getItem().getId());
						stockItemSimplifier.setItemName(itemStock.getItem().getName());
						stockItemSimplifier.setQuantity(itemStock.getQuantity());
						stockItemSimplifier.setMinimumQuantity((int) minimumStockLevel);
						if (itemStockDetail != null) {
							StockOperation batchOperation = itemStockDetail.getBatchOperation();
							String operationNumber = batchOperation == null ? "" : batchOperation.getOperationNumber();
							stockItemSimplifier.setBatchNumber(operationNumber);
							if (itemStockDetail.getExpiration() != null) {
								stockItemSimplifier.setExpiryDate(
								    BotswanaEmrUtils.formatDateWithoutTime(itemStockDetail.getExpiration(), "yyyy-MM-dd"));
							} else {
								stockItemSimplifier.setExpiryDate("");
							}
							
							if (mainStockroom != null) {
								if (mainStockroom.equals(itemStockDetail.getStockroom())) {
									stockItemSimplifier.setUnits("Packs");
								} else {
									stockItemSimplifier.setUnits("units");
								}
							}
						} else {
							stockItemSimplifier.setUnits("");
						}
						stockItemSimplifiers.add(stockItemSimplifier);
					}
				}
			}
		}
		
		fragmentModel.addAttribute("itemList", stockItemSimplifiers);
	}
}
