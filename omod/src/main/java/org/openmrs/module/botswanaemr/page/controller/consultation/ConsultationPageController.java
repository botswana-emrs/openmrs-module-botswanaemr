/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.OrderService;
import org.openmrs.api.PatientService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.domain.AppDescriptor;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.page.controller.PatientProfilePageController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.emrapi.event.ApplicationEventService;
import org.openmrs.module.emrapi.patient.PatientDomainWrapper;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.InjectBeans;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Comparator;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;

public class ConsultationPageController extends PatientProfilePageController {
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	@Override
	public Object controller(UiUtils ui, @RequestParam("patientId") Patient patient, final PageModel model,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(required = false, value = "app") AppDescriptor app,
	        @RequestParam(required = false, value = "dashboard") String dashboard,
	        @RequestParam(required = false, value = "patientQueueId") PatientQueue patientQueue,
	        @RequestParam(required = false, value = "encounterId") Encounter orderEncounter,
	        @RequestParam(required = false, value = "referralId") Order referralOrder,
	        @InjectBeans PatientDomainWrapper patientDomainWrapper, @SpringBean("adtService") AdtService adtService,
	        @SpringBean("visitService") VisitService visitService,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("providerService") ProviderService providerService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("conceptService") ConceptService conceptService,
	        @SpringBean("appFrameworkService") AppFrameworkService appFrameworkService,
	        @SpringBean("applicationEventService") ApplicationEventService applicationEventService,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties, UiSessionContext sessionContext) {
		super.controller(ui, patient, model, visit, returnUrl, app, dashboard, patientQueue, orderEncounter, referralOrder,
		    patientDomainWrapper, adtService, visitService, patientService, providerService, encounterService,
		    conceptService, appFrameworkService, applicationEventService, coreAppsProperties, sessionContext);
		model.addAttribute("isConsultation", true);
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), true);
		Comparator<Order> orderDateComparator = Comparator.comparing(Order::getDateCreated);
		OrderType referralOrderType = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		
		Optional<Order> referral = BotswanaEmrUtils.getReferral(patient, orderDateComparator, referralOrderType,
		    activeVisit);
		if (referral.isPresent()) {
			model.addAttribute("hasExistingReferral", true);
		} else {
			model.addAttribute("hasExistingReferral", false);
		}
		return null;
	}
}
