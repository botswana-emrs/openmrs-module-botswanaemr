/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.time.DateUtils;
import org.openmrs.BaseOpenmrsMetadata;
import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedItemStockDetail;
import org.openmrs.module.botswanaemr.model.StockActivitySimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

public class StockManagementLandingPgPageController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		
		Location location = null;
		if (sessionContext.getSessionLocation() == null) {
			model.addAttribute("stockActivities", new ArrayList<>());
			model.addAttribute("expiredItems", 0);
			model.addAttribute("closeToExpire", 0);
			model.addAttribute("todayOrders", 0);
		} else {
			location = sessionContext.getSessionLocation();
			List<StockActivitySimplifier> stockActivities = getAllStockOperationsActivities(location);
			model.addAttribute("stockActivities", stockActivities);
			
			String numToAdjust = Context.getAdministrationService().getGlobalProperty("numberOfDaysCloseToExpiry", "90");
			Date today = DateUtil.getStartOfDay(new Date());
			Date closeToExpiryDays = DateUtil.adjustDate(today, Integer.parseInt(numToAdjust), DurationUnit.DAYS);
			int todayOrders;
			
			List<SimplifiedItemStockDetail> expiredItems = new ArrayList<>();
			List<SimplifiedItemStockDetail> closeToExpire = new ArrayList<>();
			
			List<SimplifiedItemStockDetail> simplifiedItemStockDetails = getAllItems(location);
			
			for (SimplifiedItemStockDetail simplifiedItemStockDetail : simplifiedItemStockDetails) {
				if (simplifiedItemStockDetail.getQuantity() != null && simplifiedItemStockDetail.getQuantity() > 0) {
					if (simplifiedItemStockDetail.getExpiryDate() != null
					        && simplifiedItemStockDetail.getExpiryDate().before(today)) {
						expiredItems.add(simplifiedItemStockDetail);
					} else if (simplifiedItemStockDetail.getExpiryDate() != null
					        && simplifiedItemStockDetail.getExpiryDate().before(closeToExpiryDays)) {
						closeToExpire.add(simplifiedItemStockDetail);
					}
					
				}
			}
			
			todayOrders = getOrderStockOperations(today, location).size();
			
			model.addAttribute("expiredItems", expiredItems.size());
			model.addAttribute("closeToExpire", closeToExpire.size());
			model.addAttribute("todayOrders", todayOrders);
		}
		
	}
	
	private List<SimplifiedItemStockDetail> getAllItems(Location location) {
		List<SimplifiedItemStockDetail> simplifiedItemStockDetails = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		for (Stockroom stockroom : stockrooms) {
			List<ItemStockDetail> itemStockDetails = BotswanaInventoryContext.getItemStockDetailDataService()
			        .getItemStockDetailsByStockroom(stockroom, null);
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				if (itemStockDetail.getExpiration() != null) {
					SimplifiedItemStockDetail simplifiedItemStockDetail = new SimplifiedItemStockDetail();
					simplifiedItemStockDetail.setItemName(itemStockDetail.getItemStock().getItem().getName());
					simplifiedItemStockDetail.setExpiryDate(itemStockDetail.getExpiration());
					simplifiedItemStockDetail.setQuantity(itemStockDetail.getQuantity());
					simplifiedItemStockDetails.add(simplifiedItemStockDetail);
				}
			}
		}
		return simplifiedItemStockDetails;
	}
	
	private List<StockOperation> getOrderStockOperations(Date today, Location location) {
		List<StockOperation> orderStockOperations = new ArrayList<>();
		List<StockOperation> stockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		for (Stockroom stockroom : stockrooms) {
			List<StockOperation> operations = BotswanaInventoryContext.getStockOperationDataService()
			        .getOperationsByRoom(stockroom, null);
			stockOperations.addAll(operations);
		}
		
		for (StockOperation stockOperation : stockOperations) {
			if (stockOperation.getInstanceType().equals(WellKnownOperationTypes.getInternalRequisition())
			        || stockOperation.getInstanceType().equals(WellKnownOperationTypes.getExternalRequisition())) {
				if (DateUtils.isSameDay(stockOperation.getDateCreated(), today)) {
					orderStockOperations.add(stockOperation);
				}
			}
		}
		
		return orderStockOperations;
	}
	
	private List<StockActivitySimplifier> getAllStockOperationsActivities(Location location) {
		List<StockOperation> stockOperations = new ArrayList<>();
		List<StockActivitySimplifier> stockActivitySimplifiers = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		for (Stockroom stockroom : stockrooms) {
			List<StockOperation> operations = BotswanaInventoryContext.getStockOperationDataService()
			        .getOperationsByRoom(stockroom, new PagingInfo(1, BotswanaEmrUtils.LIMIT_FETCH_SIZE));
			stockOperations.addAll(operations);
		}
		if (stockOperations.size() > BotswanaEmrUtils.LIMIT_FETCH_SIZE) {
			stockOperations = stockOperations.stream()
			        .sorted(Comparator.comparing(BaseOpenmrsMetadata::getDateCreated, Comparator.reverseOrder()))
			        .collect(Collectors.toList()).subList(0, BotswanaEmrUtils.LIMIT_FETCH_SIZE);
		}
		
		for (StockOperation stockOperation : stockOperations) {
			if (stockOperation != null) {
				StockActivitySimplifier stockActivitySimplifier = new StockActivitySimplifier();
				stockActivitySimplifier.setStockCreatorName(String.valueOf(stockOperation.getCreator()));
				stockActivitySimplifier.setDuration(BotswanaEmrUtils.getDurationSince(stockOperation.getDateCreated()));
				stockActivitySimplifier.setInstanceType(String.valueOf(stockOperation.getInstanceType().getName()));
				stockActivitySimplifiers.add(stockActivitySimplifier);
			}
		}
		
		return stockActivitySimplifiers;
	}
}
