/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appframework.domain.AdministrativeNotification;
import org.openmrs.module.appframework.domain.AdministrativeNotificationAction;
import org.openmrs.module.appframework.domain.Extension;
import org.openmrs.module.appframework.service.AdministrativeNotificationService;
import org.openmrs.module.appframework.service.AppFrameworkService;
import org.openmrs.module.appui.AppUiExtensions;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.notification.Alert;
import org.openmrs.notification.AlertService;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.openmrs.module.botswanaemr.page.controller.SelectServicePointPageController.getAllowedServiceDeliveryPoints;

public class BotswanaEMRHeaderFragmentController {
	
	// RA-592: don't use PrivilegeConstants.VIEW_LOCATIONS
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext, HttpServletRequest request,
	        HttpServletResponse response,
	        @FragmentParam(value = "isRegistrationPage", required = false) String isRegistrationPage,
	        @RequestParam(value = "action", required = false) String action,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean AppFrameworkService appFrameworkService, @SpringBean("alertService") AlertService alertService,
	        @SpringBean("administrativeNotificationService") AdministrativeNotificationService administrativeNotificationService,
	        UiUtils ui) {
		
		// if session is not active, redirect to login page
		if (uiSessionContext.getCurrentUser() == null) {
			try {
				response.sendRedirect(request.getContextPath() + "/login.htm");
			}
			catch (Exception e) {
				// e.printStackTrace();
			}
		}
		
		LocationService locationService = Context.getService(LocationService.class);
		
		try {
			Context.addProxyPrivilege(GET_LOCATIONS);
			Context.addProxyPrivilege(VIEW_LOCATIONS);
			
			List<Location> facilities = BotswanaEmrUtils.getFacilitiesList();
			
			List<Location> allowedServiceDeliveryPoints;
			if (uiSessionContext.getSession().getAttribute("allowedServiceDeliveryPoints") != null) {
				allowedServiceDeliveryPoints = (List<Location>) uiSessionContext.getSession()
				        .getAttribute("allowedServiceDeliveryPoints");
			} else {
				allowedServiceDeliveryPoints = getAllowedServiceDeliveryPoints(uiSessionContext.getCurrentUser());
			}
			
			List<Location> servicePoints;
			if (allowedServiceDeliveryPoints != null && !allowedServiceDeliveryPoints.isEmpty()) {
				servicePoints = allowedServiceDeliveryPoints;
			} else {
				servicePoints = new ArrayList<>(locationService.getLocationsByTag(
				    locationService.getLocationTagByName(BotswanaEmrConstants.LOGIN_LOCATION_TAG_NAME)));
			}
			
			String currentServiceDeliveryPointAttribute = String.valueOf(
			    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
			Location servicePointVisited = locationService.getLocationByUuid(currentServiceDeliveryPointAttribute);
			
			servicePoints.sort(Comparator.comparing(Location::getName));
			
			fragmentModel.addAttribute("facilities", facilities);
			fragmentModel.addAttribute("servicePoints", servicePoints);
			fragmentModel.addAttribute("servicePointVisited", servicePointVisited);
			
			List<Extension> exts = appFrameworkService.getExtensionsForCurrentUser(AppUiExtensions.HEADER_CONFIG_EXTENSION);
			Map<String, Object> configSettings = exts.size() > 0 ? exts.get(0).getExtensionParams() : null;
			fragmentModel.addAttribute("configSettings", configSettings);
			
			List<Extension> userAccountMenuItems = appFrameworkService
			        .getExtensionsForCurrentUser(AppUiExtensions.HEADER_USER_ACCOUNT_MENU_ITEMS_EXTENSION);
			
			List<Alert> alertNotifications = alertService.getAllActiveAlerts(Context.getAuthenticatedUser());
			//for each of the alerts, replace the server path with the context path
			for (Alert alert : alertNotifications) {
				alert.setText(alert.getText().replace("$serverUrl/" + request.getContextPath(),
				    Context.getAdministrationService().getGlobalProperty("botswanaemr.instanceUrl")));
			}
			
			fragmentModel.addAttribute("alertNotifications", alertNotifications);
			
			fragmentModel.addAttribute("userAccountMenuItems", userAccountMenuItems);
			fragmentModel.addAttribute("hospital",
			    Context.getAuthenticatedUser().getUserProperty(BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
			        Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital")));
			
			List<AdministrativeNotification> administrativeNotifications = administrativeNotificationService
			        .getAdministrativeNotifications();
			List<AdministrativeNotificationAction> administrativeNotificationActions = new ArrayList<>();
			for (AdministrativeNotification administrativeNotification : administrativeNotifications) {
				administrativeNotificationActions.addAll(administrativeNotification.getActions());
			}
			fragmentModel.addAttribute("administrativeActions", administrativeNotificationActions);
			fragmentModel.addAttribute("isRegistrationPage", isRegistrationPage);
			fragmentModel.addAttribute("uiUtils", ui);
			fragmentModel.addAttribute("artServicesUuid", BotswanaEmrConstants.ART_SERVICES_PORTAL_UUID);
			fragmentModel.addAttribute("pharmacyPortalUuid", BotswanaEmrConstants.PHARMACY_PORTAL_UUID);
			fragmentModel.addAttribute("registrationPageUuid", BotswanaEmrConstants.PATIENT_REGISTRATION_PORTAL_UUID);
			fragmentModel.addAttribute("doctorsPortalUuid", BotswanaEmrConstants.DOCTORS_PORTAL_UUID);
			fragmentModel.addAttribute("nursePortalUuid", BotswanaEmrConstants.NURSING_PORTAL_UUID);
			fragmentModel.addAttribute("vmmcPortalUuid", BotswanaEmrConstants.VMMC_PORTAL_UUID);
			fragmentModel.addAttribute("stockManagementPortalUuid", BotswanaEmrConstants.STOCK_MANAGEMENT_PORTAL_UUID);
			fragmentModel.addAttribute("labServicesPortalUuid", BotswanaEmrConstants.LAB_SERVICES_PORTAL_UUID);
			fragmentModel.addAttribute("sexualReproductiveHealthPortalUuid",
			    BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
			fragmentModel.addAttribute("tbServicesUuid", BotswanaEmrConstants.TB_SERVICES_PORTAL_UUID);
			fragmentModel.addAttribute("hivTestingServicesPortal", BotswanaEmrConstants.HIV_TESTING_SERVICES_PORTAL_UUID);
			fragmentModel.addAttribute("hivTestingServicesPortal", BotswanaEmrConstants.HIV_TESTING_SERVICES_PORTAL_UUID);
			fragmentModel.addAttribute("action", action);
			fragmentModel.addAttribute("returnUrl", returnUrl);
		}
		finally {
			Context.removeProxyPrivilege(GET_LOCATIONS);
			Context.removeProxyPrivilege(VIEW_LOCATIONS);
		}
	}
	
	public SimpleObject setCurrentServiceDeliveryLocation(@RequestParam(value = "locationId") Location location,
	        UiSessionContext sessionContext) {
		String uuid = location.getUuid();
		sessionContext.getSession().setAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT, uuid);
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("locationUuid", uuid);
		return simpleObject;
	}
}
