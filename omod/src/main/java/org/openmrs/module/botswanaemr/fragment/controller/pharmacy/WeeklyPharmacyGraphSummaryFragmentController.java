/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.openmrs.Location;
import org.openmrs.Order;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class WeeklyPharmacyGraphSummaryFragmentController {
	
	public void controller(FragmentModel model, UiSessionContext sessionContext) throws ParseException {
		
		for (int i = 0; i < 7; i++) {
			
			model.addAttribute("t" + i, getPatientsSeenOnDay(i, sessionContext.getSessionLocation()));
		}
	}
	
	private Integer getPatientsSeenOnDay(int shift, Location location) throws ParseException {
		int value = 0;
		List<Order> filledPrescription = BotswanaEmrUtils.getDrugOrders(
		    DateUtil.getStartOfDay(DateUtil.adjustDate(DateUtil.getStartOfWeek(new Date()), shift, DurationUnit.DAYS)),
		    DateUtil.getEndOfDay(DateUtil.adjustDate(DateUtil.getStartOfWeek(new Date()), shift, DurationUnit.DAYS)),
		    Order.FulfillerStatus.COMPLETED, location);
		if (!filledPrescription.isEmpty()) {
			value = filledPrescription.size();
		}
		
		return value;
	}
}
