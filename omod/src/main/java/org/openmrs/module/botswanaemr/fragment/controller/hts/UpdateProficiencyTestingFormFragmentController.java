/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.hts;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.model.hts.DtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedDtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedHtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedHtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.SpecimenResultMapper;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.openmrs.module.botswanaemr.fragment.controller.hts.ProficiencyTestingFormFragmentController.DEFINITION_UI_RESOURCE;
import static org.openmrs.module.botswanaemr.fragment.controller.hts.ProficiencyTestingFormFragmentController.HIV_TEST_RESULTS_FORM_UUID;

public class UpdateProficiencyTestingFormFragmentController {
	
	private final HtsProficiencyTestingService htsProficiencyTestingService = Context
	        .getService(HtsProficiencyTestingService.class);
	
	public void controller(@RequestParam(value = "proficiencyTestingId", required = false) Integer proficiencyTestingId,
	        FragmentModel fragmentModel, UiUtils ui) {
		HtsProficiencyTesting htsProficiencyTesting = htsProficiencyTestingService
		        .getHtsProficiencyTesting(proficiencyTestingId);
		List<SimplifiedHtsSpecimenResult> specimenResults = SpecimenResultMapper.simplifySpecimenResults(
		    (List<HtsProficiencyTestingTest>) htsProficiencyTesting.getHtsProficiencyTestingTest());
		
		JsonArray result = (JsonArray) new Gson().toJsonTree(specimenResults,
		    new TypeToken<List<SimplifiedHtsSpecimenResult>>() {}.getType());
		
		fragmentModel.addAttribute("result", result);
		
		fragmentModel.addAttribute("formUuid", HIV_TEST_RESULTS_FORM_UUID);
		fragmentModel.addAttribute("definitionUiResource", DEFINITION_UI_RESOURCE);
		fragmentModel.addAttribute("returnUrl", ui.pageLink("botswanaemr", "hts/proficiencyTestsForm"));
		
		HashMap<String, String> kitNames = new HashMap<>();
		kitNames.put("Determine", "c2ce43cc-0345-40c0-91b2-24155f458d4e");
		kitNames.put("Bioline", "166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitNames.put("KHB", "c53f44de-5df2-4810-b99b-7f75e80d1a05");
		kitNames.put("ICT", "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		HashMap<String, String> secondTestKitNames = new HashMap<>();
		secondTestKitNames.put("U-Unigold", "c2acb8a1-32f6-456a-b322-d31a1d478c01");
		secondTestKitNames.put("FR-1st Response", "570a1f68-0bea-4980-ba9d-d0d7d21e4be9");
		secondTestKitNames.put("BK-Bio KIt", "c203885b-7bae-4318-8bb8-4aee96f973cd");
		secondTestKitNames.put("IF-Immunoflow", "7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f");
		
		HashMap<String, String> kitResults = new HashMap<>();
		kitResults.put("Reactive", "1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitResults.put("Non Reactive", "1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitResults.put("Invalid", "163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		HashMap<String, String> hivStatus = new HashMap<>();
		hivStatus.put("Negative", "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		hivStatus.put("Positive", "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		hivStatus.put("Indeterminate", "1138AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		fragmentModel.addAttribute("kitNames", kitNames);
		fragmentModel.addAttribute("secondTestKitNames", secondTestKitNames);
		fragmentModel.addAttribute("kitResults", kitResults);
		fragmentModel.addAttribute("hivStatus", hivStatus);
		
		if (htsProficiencyTesting != null) {
			fragmentModel.addAttribute("htsProficiencyTesting",
			    SimpleObject.fromObject(BotswanaEmrUtils.getSimplifiedProficiencyTesting(htsProficiencyTesting), ui,
			        "proficiencyTestingId", "panelId", "receivedBy", "datePanelReceived", "testingPoint", "resultsDueDate",
			        "dateTested", "resultsReceivedBy", "resultsReceivedByName", "performanceScore", "percentageScore",
			        "comment", "reportReviewedBy", "reportReviewedByName", "dateReviewed", "published"));
			fragmentModel.addAttribute("simplifiedHtsProficiencyTestingTests",
			    SimpleObject.fromCollection(
			        getSimplifiedProficiencyTestingTests(htsProficiencyTesting.getHtsProficiencyTestingTest()), ui,
			        "proficiencyTestingTestId", "specimenId", "lotNumber", "expiryDate", "conditionOfSpecimen",
			        "reasonForUnacceptableSpecimen", "correctiveActions"));
			fragmentModel.addAttribute("simplifiedHtsProficiencyTestingBuffers",
			    SimpleObject.fromCollection(
			        getSimplifiedProficiencyTestingBuffers(htsProficiencyTesting.getDtsProficiencyTestingBuffer()), ui,
			        "proficiencyTestingBufferId", "dtsBufferId", "dtsLotNumber", "dtsExpiryDate", "dtsConditionOfBuffer",
			        "dtsReasonForUnacceptableBuffer", "dtsCorrectiveActions"));
		}
	}
	
	public void saveProficiencyTestingTest(@RequestParam(value = "ptId") String ptId,
	        @RequestParam(value = "specimenId") String specimenId, @RequestParam(value = "lotNumber") String lotNumber,
	        @RequestParam(value = "expiryDate") String expiryDate, @RequestParam(value = "ptCondition") String ptCondition,
	        @RequestParam(value = "ptCorrectiveActions", required = false) String ptCorrectiveActions,
	        @RequestParam(value = "reasonForUnacceptablePt", required = false) String reasonForUnacceptablePt)
	        throws ParseException {
		
		HtsProficiencyTesting htsProficiencyTesting;
		if (StringUtils.isNotBlank(ptId)) {
			htsProficiencyTesting = htsProficiencyTestingService.getHtsProficiencyTesting(Integer.parseInt(ptId));
			HtsProficiencyTestingTest proficiencyTestingTest = new HtsProficiencyTestingTest();
			proficiencyTestingTest.setSpecimenId(specimenId);
			proficiencyTestingTest.setLotNumber(lotNumber);
			proficiencyTestingTest.setExpiryDate(BotswanaEmrUtils.formatDateFromString(expiryDate, "yyyy-MM-dd"));
			proficiencyTestingTest.setConditionOfSpecimen(ptCondition);
			proficiencyTestingTest.setReasonForUnacceptableSpecimen(reasonForUnacceptablePt);
			proficiencyTestingTest.setCorrectiveActions(ptCorrectiveActions);
			proficiencyTestingTest.setHtsProficiencyTesting(htsProficiencyTesting);
			htsProficiencyTestingService.saveOrUpdateHtsProficiencyTestingTest(proficiencyTestingTest);
		}
	}
	
	public void saveProficiencyTestingBuffer(@RequestParam(value = "ptDtsId") String ptDtsId,
	        @RequestParam(value = "dtsBufferId") String dtsBufferId,
	        @RequestParam(value = "dtsLotNumber") String dtsLotNumber,
	        @RequestParam(value = "dtsExpiryDate") String dtsExpiryDate,
	        @RequestParam(value = "dtsCondition") String dtsCondition,
	        @RequestParam(value = "dtsBufferCorrectiveActions", required = false) String dtsBufferCorrectiveActions,
	        @RequestParam(value = "reasonForUnacceptableDts", required = false) String reasonForUnacceptableDts)
	        throws ParseException {
		HtsProficiencyTesting htsProficiencyTesting;
		if (StringUtils.isNotBlank(ptDtsId)) {
			htsProficiencyTesting = htsProficiencyTestingService.getHtsProficiencyTesting(Integer.parseInt(ptDtsId));
			DtsProficiencyTestingBuffer proficiencyTestingBuffer = new DtsProficiencyTestingBuffer();
			proficiencyTestingBuffer.setDtsBufferId(dtsBufferId);
			proficiencyTestingBuffer.setDtsLotNumber(dtsLotNumber);
			proficiencyTestingBuffer.setDtsExpiryDate(BotswanaEmrUtils.formatDateFromString(dtsExpiryDate, "yyyy-MM-dd"));
			proficiencyTestingBuffer.setDtsConditionOfSpecimen(dtsCondition);
			proficiencyTestingBuffer.setDtsReasonForUnacceptableSpecimen(reasonForUnacceptableDts);
			proficiencyTestingBuffer.setDtsCorrectiveActions(dtsBufferCorrectiveActions);
			proficiencyTestingBuffer.setDtsProficiencyTesting(htsProficiencyTesting);
			htsProficiencyTestingService.saveOrUpdateDtsProficiencyTestingBuffer(proficiencyTestingBuffer);
		}
	}
	
	public void updateProficiencyTestingForm(@RequestParam(value = "proficiencyTestingId") String proficiencyTestingId,
	        @RequestParam(value = "panelId") String panelId,
	        @RequestParam(value = "datePanelReceived") String datePanelReceived,
	        @RequestParam(value = "receivedBy") String receivedBy,
	        @RequestParam(value = "dateResultsDue") String dateResultsDue,
	        @RequestParam(value = "testingPoint") String testingPoint,
	        @RequestParam(value = "specimenData", required = false) String specimenData,
	        @RequestParam(value = "dtsBuffersData", required = false) String dtsBuffersData,
	        @RequestParam(value = "reportData", required = false) String reportData) throws ParseException, IOException {
		HtsProficiencyTesting htsProficiencyTesting;
		if (StringUtils.isNotBlank(proficiencyTestingId)) {
			htsProficiencyTesting = htsProficiencyTestingService
			        .getHtsProficiencyTesting(Integer.parseInt(proficiencyTestingId));
			User authenticatedUser = Context.getAuthenticatedUser();
			if (StringUtils.isNotBlank(panelId) && StringUtils.isNotBlank(datePanelReceived)
			        && StringUtils.isNotBlank(receivedBy) && StringUtils.isNotBlank(dateResultsDue)
			        && StringUtils.isNotBlank(testingPoint)) {
				htsProficiencyTesting.setId(Integer.valueOf(proficiencyTestingId));
				htsProficiencyTesting.setPanelId(panelId);
				htsProficiencyTesting
				        .setDatePanelReceived(BotswanaEmrUtils.formatDateFromString(datePanelReceived, "yyyy-MM-dd"));
				htsProficiencyTesting.setReceivedBy(authenticatedUser.getPerson());
				htsProficiencyTesting.setResultsDueDate(BotswanaEmrUtils.formatDateFromString(dateResultsDue, "yyyy-MM-dd"));
				htsProficiencyTesting.setTestingPoint(testingPoint);
				htsProficiencyTesting.setCreator(authenticatedUser);
			}
			ObjectMapper mapper = new ObjectMapper();
			if (StringUtils.isNotBlank(specimenData)) {
				JsonNode jsonNode = mapper.readTree(specimenData);
				List<SimpleObject> tests = mapper.readValue(jsonNode, new TypeReference<List<SimpleObject>>() {});
				htsProficiencyTesting.setHtsProficiencyTestingTest(getProficiencyTestingTests(tests, htsProficiencyTesting));
			}
			if (StringUtils.isNotBlank(dtsBuffersData)) {
				JsonNode node = mapper.readTree(dtsBuffersData);
				List<SimpleObject> buffers = mapper.readValue(node, new TypeReference<List<SimpleObject>>() {});
				htsProficiencyTesting
				        .setDtsProficiencyTestingBuffer(getProficiencyTestingBuffers(buffers, htsProficiencyTesting));
			}
			if (StringUtils.isNotEmpty(reportData)) {
				JsonNode jsonNode = mapper.readTree(reportData);
				SimpleObject report = mapper.readValue(jsonNode, new TypeReference<SimpleObject>() {});
				htsProficiencyTesting.setDateTested(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(report.get("dateTested")), "yyyy-MM-dd"));
				htsProficiencyTesting.setResultsReceivedBy(
				    Context.getPersonService().getPersonByUuid(String.valueOf(report.get("resultsReceivedBy"))));
				htsProficiencyTesting
				        .setPerformanceScore(Double.parseDouble(String.valueOf(report.get("performanceScore"))));
				htsProficiencyTesting.setPercentageScore(Double.parseDouble(String.valueOf(report.get("percentageScore"))));
				htsProficiencyTesting.setComment(String.valueOf(report.get("overallComment")));
				htsProficiencyTesting.setReportReviewedBy(
				    Context.getPersonService().getPersonByUuid(String.valueOf(report.get("reportReviewedBy"))));
				htsProficiencyTesting.setDateReviewed(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(report.get("dateReviewed")), "yyyy-MM-dd"));
			}
			htsProficiencyTestingService.saveHtsProficiencyTesting(htsProficiencyTesting);
		}
	}
	
	private Set<HtsProficiencyTestingTest> getProficiencyTestingTests(List<SimpleObject> tests,
	        HtsProficiencyTesting htsProficiencyTesting) throws ParseException {
		Set<HtsProficiencyTestingTest> existingHtsProficiencyTestingTests = htsProficiencyTesting
		        .getHtsProficiencyTestingTest();
		Set<HtsProficiencyTestingTest> updatedHtsProficiencyTestingTests = new HashSet<>();
		for (SimpleObject test : tests) {
			String proficiencyTestingTestId = String.valueOf(test.get("proficiencyTestingTestId"));
			Optional<HtsProficiencyTestingTest> htsProficiencyTestingTest = existingHtsProficiencyTestingTests.stream()
			        .filter(ptt -> ptt.getId().equals(Integer.valueOf(proficiencyTestingTestId))).distinct().findFirst();
			if (htsProficiencyTestingTest.isPresent()) {
				HtsProficiencyTestingTest proficiencyTestingTest = htsProficiencyTestingTest.get();
				proficiencyTestingTest.setId(Integer.valueOf(proficiencyTestingTestId));
				proficiencyTestingTest.setSpecimenId(String.valueOf(test.get("ptSpecimenId")));
				proficiencyTestingTest.setLotNumber(String.valueOf(test.get("ptSpecimenLotNumber")));
				proficiencyTestingTest.setExpiryDate(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(test.get("ptSpecimenExpiryDate")), "yyyy-MM-dd"));
				proficiencyTestingTest.setConditionOfSpecimen(String.valueOf(test.get("ptSpecimenConditions")));
				proficiencyTestingTest
				        .setReasonForUnacceptableSpecimen(String.valueOf(test.get("reasonForUnacceptablePtSpecimen")));
				proficiencyTestingTest.setCorrectiveActions(String.valueOf(test.get("correctiveActions")));
				proficiencyTestingTest.setHtsProficiencyTesting(htsProficiencyTesting);
				htsProficiencyTestingService.saveOrUpdateHtsProficiencyTestingTest(proficiencyTestingTest);
				
				Set<HtsSpecimenResult> htsSpecimenResults = getSpecimenResults(
				    (List<SimpleObject>) test.get("specimenResults"), proficiencyTestingTest);
				proficiencyTestingTest.getSpecimenResults().clear();
				proficiencyTestingTest.setSpecimenResults(htsSpecimenResults);
				
				updatedHtsProficiencyTestingTests.add(proficiencyTestingTest);
			}
		}
		return updatedHtsProficiencyTestingTests;
	}
	
	private Set<HtsSpecimenResult> getSpecimenResults(List<SimpleObject> specimenResults,
	        HtsProficiencyTestingTest htsProficiencyTestingTest) {
		Set<HtsSpecimenResult> existingSpecimenResults = htsProficiencyTestingTest.getSpecimenResults();
		Set<HtsSpecimenResult> updatedSpecimenResults = new HashSet<>();
		
		for (LinkedHashMap specimenResult : specimenResults) {
			String specimenResultId = String.valueOf(specimenResult.get("id"));
			HtsSpecimenResult updatedSpecimenResult;
			if (StringUtils.isNotEmpty(specimenResultId)) {
				Optional<HtsSpecimenResult> htsSpecimenResult = existingSpecimenResults.stream()
				        .filter(spr -> spr.getId().equals(Integer.valueOf(specimenResultId))).distinct().findFirst();
				
				updatedSpecimenResult = htsSpecimenResult.orElseGet(HtsSpecimenResult::new);
			} else {
				updatedSpecimenResult = new HtsSpecimenResult();
			}
			
			updatedSpecimenResult.setInterpretation(String.valueOf(specimenResult.get("interpretation")));
			updatedSpecimenResult.setComment(String.valueOf(specimenResult.get("comment")));
			
			Set<HtsTestResult> testResultSet = getTestResults((List<SimpleObject>) specimenResult.get("results"),
			    updatedSpecimenResult);
			updatedSpecimenResult.getResults().clear();
			updatedSpecimenResult.setResults(testResultSet);
			
			updatedSpecimenResults.add(updatedSpecimenResult);
		}
		
		return updatedSpecimenResults;
	}
	
	private Set<HtsTestResult> getTestResults(List<SimpleObject> testResults, HtsSpecimenResult htsSpecimenResult) {
		Set<HtsTestResult> existingTestResults = htsSpecimenResult.getResults();
		Set<HtsTestResult> updatedTestResults = new HashSet<>();
		
		for (LinkedHashMap testResult : testResults) {
			String testResultId = String.valueOf(testResult.get("id"));
			
			HtsTestResult updatedTestResult;
			if (StringUtils.isNotEmpty(testResultId)) {
				Optional<HtsTestResult> htsTestResult = existingTestResults.stream()
				        .filter(tr -> tr.getId().equals(Integer.valueOf(testResultId))).distinct().findFirst();
				updatedTestResult = htsTestResult.orElseGet(HtsTestResult::new);
			} else {
				updatedTestResult = new HtsTestResult();
			}
			
			try {
				updatedTestResult.setExpiryDate(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(testResult.get("expiryDate")), "yyyy-MM-dd"));
			}
			catch (ParseException e) {
				e.printStackTrace();
			}
			updatedTestResult.setKitResults(String.valueOf(testResult.get("kitResults")));
			updatedTestResult.setResultNumber(String.valueOf(testResult.get("resultNumber")));
			updatedTestResult.setLotNumber(String.valueOf(testResult.get("lotNo")));
			updatedTestResult.setKitName(String.valueOf(testResult.get("kitName")));
			
			updatedTestResults.add(updatedTestResult);
			
		}
		
		return updatedTestResults;
	}
	
	private Set<DtsProficiencyTestingBuffer> getProficiencyTestingBuffers(List<SimpleObject> buffers,
	        HtsProficiencyTesting htsProficiencyTesting) throws ParseException {
		Set<DtsProficiencyTestingBuffer> existingDtsProficiencyTestingBuffers = htsProficiencyTesting
		        .getDtsProficiencyTestingBuffer();
		Set<DtsProficiencyTestingBuffer> updatedDtsProficiencyTestingBuffers = new HashSet<>();
		for (SimpleObject buffer : buffers) {
			String proficiencyTestingBufferId = String.valueOf(buffer.get("proficiencyTestingBufferId"));
			Optional<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffer = existingDtsProficiencyTestingBuffers.stream()
			        .filter(ptb -> ptb.getId().equals(Integer.valueOf(proficiencyTestingBufferId))).distinct().findFirst();
			if (dtsProficiencyTestingBuffer.isPresent()) {
				DtsProficiencyTestingBuffer proficiencyTestingBuffer = dtsProficiencyTestingBuffer.get();
				proficiencyTestingBuffer.setId(Integer.valueOf(proficiencyTestingBufferId));
				proficiencyTestingBuffer.setDtsBufferId(String.valueOf(buffer.get("dtsBufferId")));
				proficiencyTestingBuffer.setDtsLotNumber(String.valueOf(buffer.get("dtsLotNumber")));
				proficiencyTestingBuffer.setDtsExpiryDate(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(buffer.get("dtsExpiryDate")), "yyyy-MM-dd"));
				proficiencyTestingBuffer.setDtsConditionOfSpecimen(String.valueOf(buffer.get("dtsConditionOfBuffer")));
				proficiencyTestingBuffer
				        .setDtsReasonForUnacceptableSpecimen(String.valueOf(buffer.get("dtsReasonForUnacceptableBuffer")));
				proficiencyTestingBuffer.setDtsCorrectiveActions(String.valueOf(buffer.get("dtsCorrectiveActions")));
				proficiencyTestingBuffer.setDtsProficiencyTesting(htsProficiencyTesting);
				htsProficiencyTestingService.saveOrUpdateDtsProficiencyTestingBuffer(proficiencyTestingBuffer);
				updatedDtsProficiencyTestingBuffers.add(proficiencyTestingBuffer);
			}
		}
		return updatedDtsProficiencyTestingBuffers;
	}
	
	public List<SimplifiedHtsProficiencyTestingTest> getSimplifiedProficiencyTestingTests(
	        Set<HtsProficiencyTestingTest> htsProficiencyTestingTests) {
		List<SimplifiedHtsProficiencyTestingTest> simplifiedHtsProficiencyTestingTests = new ArrayList<>();
		for (HtsProficiencyTestingTest htsProficiencyTestingTest : htsProficiencyTestingTests) {
			SimplifiedHtsProficiencyTestingTest simplifiedHtsProficiencyTestingTest = new SimplifiedHtsProficiencyTestingTest();
			simplifiedHtsProficiencyTestingTest.setProficiencyTestingTestId(htsProficiencyTestingTest.getId());
			simplifiedHtsProficiencyTestingTest.setId(htsProficiencyTestingTest.getId());
			simplifiedHtsProficiencyTestingTest.setSpecimenId(htsProficiencyTestingTest.getSpecimenId());
			simplifiedHtsProficiencyTestingTest.setLotNumber(htsProficiencyTestingTest.getLotNumber());
			simplifiedHtsProficiencyTestingTest.setExpiryDate(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTestingTest.getExpiryDate(), "yyyy-MM-dd"));
			simplifiedHtsProficiencyTestingTest.setConditionOfSpecimen(htsProficiencyTestingTest.getConditionOfSpecimen());
			simplifiedHtsProficiencyTestingTest
			        .setReasonForUnacceptableSpecimen(htsProficiencyTestingTest.getReasonForUnacceptableSpecimen());
			simplifiedHtsProficiencyTestingTest.setCorrectiveActions(htsProficiencyTestingTest.getCorrectiveActions());
			simplifiedHtsProficiencyTestingTests.add(simplifiedHtsProficiencyTestingTest);
		}
		return simplifiedHtsProficiencyTestingTests;
	}
	
	public List<SimplifiedDtsProficiencyTestingBuffer> getSimplifiedProficiencyTestingBuffers(
	        Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffers) {
		List<SimplifiedDtsProficiencyTestingBuffer> simplifiedDtsProficiencyTestingBuffers = new ArrayList<>();
		for (DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer : dtsProficiencyTestingBuffers) {
			SimplifiedDtsProficiencyTestingBuffer simplifiedDtsProficiencyTestingBuffer = new SimplifiedDtsProficiencyTestingBuffer();
			simplifiedDtsProficiencyTestingBuffer.setProficiencyTestingBufferId(dtsProficiencyTestingBuffer.getId());
			simplifiedDtsProficiencyTestingBuffer.setDtsBufferId(dtsProficiencyTestingBuffer.getDtsBufferId());
			simplifiedDtsProficiencyTestingBuffer.setDtsLotNumber(dtsProficiencyTestingBuffer.getDtsLotNumber());
			simplifiedDtsProficiencyTestingBuffer.setDtsExpiryDate(
			    BotswanaEmrUtils.formatDateWithoutTime(dtsProficiencyTestingBuffer.getDtsExpiryDate(), "yyyy-MM-dd"));
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsConditionOfBuffer(dtsProficiencyTestingBuffer.getDtsConditionOfSpecimen());
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsReasonForUnacceptableBuffer(dtsProficiencyTestingBuffer.getDtsReasonForUnacceptableSpecimen());
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsCorrectiveActions(dtsProficiencyTestingBuffer.getDtsCorrectiveActions());
			simplifiedDtsProficiencyTestingBuffers.add(simplifiedDtsProficiencyTestingBuffer);
		}
		return simplifiedDtsProficiencyTestingBuffers;
	}
}
