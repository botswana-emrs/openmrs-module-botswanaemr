/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID;

import java.util.ArrayList;

import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttribute;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class AddStockRoomsPageController {
	
	private final IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void controller(@RequestParam(value = "stockRoomId", required = false) Integer stockRoomId, PageModel pageModel,
	        UiSessionContext uiSessionContext) {
		ArrayList<String> stockroomTypes = new ArrayList<>();
		stockroomTypes.add("Main Bulk");
		stockroomTypes.add("Dispensing");
		stockroomTypes.add("Hts Bulk");
		stockroomTypes.add("Hts Distribution");
		stockroomTypes.add("Pmtct Bulk");
		stockroomTypes.add("IDCC Bulk");
		pageModel.addAttribute("stockroomTypes", stockroomTypes);
		
		if (stockRoomId != null && uiSessionContext.getCurrentUser() != null) {
			Stockroom stockroom = iStockroomDataService.getById(stockRoomId);
			
			StockRoomAttribute stockRoomTypeAttribute = stockroom.getActiveAttributes().stream()
			        .filter(s -> s.getAttributeType().getUuid().equals(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID)).findFirst()
			        .orElse(null);
			String stockroomType = stockRoomTypeAttribute == null ? "" : stockRoomTypeAttribute.getValue();
			pageModel.addAttribute("stockRoom", stockroom);
			pageModel.addAttribute("stockRoomId", stockRoomId);
			pageModel.addAttribute("stockRoomType", stockroomType);
			
		} else {
			pageModel.addAttribute("stockRoom", null);
			pageModel.addAttribute("stockRoomId", null);
		}
	}
	
}
