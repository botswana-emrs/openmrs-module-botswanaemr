/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.Location;
import org.openmrs.Person;
import org.openmrs.PersonName;
import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.LocationService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.adminui.account.Account;
import org.openmrs.module.adminui.account.AccountService;
import org.openmrs.module.providermanagement.Provider;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FORCE_PASSWORD_CHANGE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;
import static org.openmrs.module.locationbasedaccess.LocationBasedAccessConstants.LOCATION_USER_PROPERTY_NAME;

public class ManageAccountsFragmentController {
	
	protected final Log log = LogFactory.getLog(getClass());
	
	private final UserService userService = Context.getService(UserService.class);
	
	/**
	 * @param model
	 * @param accountService
	 */
	public void get(FragmentModel model, @SpringBean("adminAccountService") AccountService accountService,
	        @SpringBean("adminService") AdministrationService adminService) {
		
		List<Account> accounts = accountService.getAllAccounts();
		model.addAttribute("accounts", accounts);
		Map<User, Boolean> userRetiredStatusMap = new HashMap<User, Boolean>();
		Map<org.openmrs.Provider, String> providerNameMap = new HashMap<org.openmrs.Provider, String>();
		for (Account a : accountService.getAllAccounts()) {
			//There seems to be some sort of bug in hbm mappings for provider
			//in the provider management module where when a provider
			//account is not linked to the user the name field never gets
			//populated, this is a hack to fetch the name via raw SQL
			for (org.openmrs.Provider p : a.getProviderAccounts()) {
				if (p.getPerson() == null) {
					List<List<Object>> rows = adminService
					        .executeSQL("select name from provider where provider_id=" + p.getProviderId(), true);
					
					providerNameMap.put(p, (String) rows.get(0).get(0));
				}
			}
			for (org.openmrs.User user : a.getUserAccounts()) {
				userRetiredStatusMap.put(user, user.getRetired());
			}
		}
		model.addAttribute("providerNameMap", providerNameMap);
		model.addAttribute("userRetiredStatusMap", userRetiredStatusMap);
	}
	
	public List<SimpleObject> getAllActiveParentFacilities(UiUtils ui,
	        @SpringBean("locationService") LocationService locationService,
	        @RequestParam(value = "personId", required = false) Person person) {
		
		List<Location> parentLocations = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		
		return SimpleObject.fromCollection(parentLocations, ui, "uuid", "name");
		
	}
	
	public List<SimpleObject> getUserAssignedFacilities(UiUtils ui,
	        @SpringBean("locationService") LocationService locationService,
	        @RequestParam(value = "personId", required = false) Person person) {
		List<Location> allUserLocations = new ArrayList<>();
		Account account = new Account(person);
		//TODO find out if one user can still have more than one account
		User user = account.getUserAccounts().get(0);
		if (user != null) {
			Object userProperty = user.getUserProperty(LOCATION_USER_PROPERTY_NAME);
			if (userProperty != null) {
				String[] locationUuids = userProperty.toString().split(",");
				for (String locationUuid : locationUuids) {
					allUserLocations.add(locationService.getLocationByUuid(locationUuid));
				}
			}
		}
		
		//TODO check if this needs to be replaced by filtering based on Location TAG instead of whether a location in child or parent
		List<Location> childLocations = new ArrayList<>();
		List<Location> parentLocations = new ArrayList<>();
		for (Location location : allUserLocations) {
			if (location != null) {
				if (location.getParentLocation() != null) {
					childLocations.add(location);
				} else {
					parentLocations.add(location);
				}
			}
		}
		return SimpleObject.fromCollection(parentLocations, ui, "uuid", "name");
		
	}
	
	public SimpleObject getPersonDetails(@RequestParam(value = "personId", required = false) Person person, UiUtils uu) {
		
		Account account;
		if (person == null) {
			account = new Account(new Person());
		} else {
			account = new Account(person);
		}
		
		SimpleObject so = new SimpleObject();
		ObjectMapper mapper = new ObjectMapper();
		SimpleObject simplePerson = new SimpleObject();
		simplePerson.put("familyName", uu.encodeHtml(account.getFamilyName()));
		simplePerson.put("middleName", uu.encodeHtml(account.getPerson().getMiddleName()));
		simplePerson.put("givenName", uu.encodeHtml(account.getGivenName()));
		simplePerson.put("gender", uu.encodeHtml(account.getGender()) != null ? uu.encodeHtml(account.getGender()) : "");
		
		so.put("simplePerson", simplePerson);
		
		for (User user : account.getUserAccounts()) {
			SimpleObject simpleUser = new SimpleObject();
			simpleUser.put("username", user.getUsername());
			simpleUser.put("systemId", user.getSystemId());
			so.put("userDetails", simpleUser);
			List<Role> userRol = user.getAllRoles().stream()
			        .filter(role -> role.getName().startsWith("Org") || role.getName().equals("Provider"))
			        .collect(Collectors.toList());
			List<Role> allSystemRoles = Context.getService(UserService.class).getAllRoles().stream()
			        .filter(role -> role.getName().startsWith("Org") || role.getName().equals("Provider"))
			        .collect(Collectors.toList());
			List<SimpleObject> roleObjects = new ArrayList<>();
			for (Role role : allSystemRoles) {
				
				SimpleObject roleObject = new SimpleObject();
				roleObject.put("value", role.getUuid());
				String[] split = role.getName().split(":");
				roleObject.put("label", split.length > 1 ? split[1] : split[0]);
				if (userRol.contains(role)) {
					roleObject.put("selected", true);
				} else {
					roleObject.put("selected", false);
				}
				roleObjects.add(roleObject);
			}
			so.put("roleOptions", roleObjects);
		}
		
		return so;
		
	}
	
	public void retireUser(@RequestParam(value = "personUuid", required = false) String personUuid,
	        @SpringBean("userService") UserService userService,
	        @RequestParam(value = "userRetireReason") String userRetireReason) {
		Person person = Context.getPersonService().getPersonByUuid(personUuid);
		List<User> users = userService.getUsersByPerson(person, false);
		
		for (User user : users) {
			if (user.getPerson().equals(person)) {
				userService.retireUser(user, userRetireReason);
			}
		}
	}
	
	public void unRetireUser(@RequestParam(value = "personUuid", required = false) String personUuid,
	        @SpringBean("userService") UserService userService) {
		Person person = Context.getPersonService().getPersonByUuid(personUuid);
		List<User> users = userService.getUsersByPerson(person, true);
		
		for (User user : users) {
			if (user.getPerson().equals(person)) {
				userService.unretireUser(user);
			}
		}
	}
	
	public SimpleObject deleteUser(@RequestParam(value = "personUuid", required = false) String personUuid,
	        @SpringBean("userService") UserService userService) {
		SimpleObject simpleObject = new SimpleObject();
		boolean hasException = false;
		Person person = Context.getPersonService().getPersonByUuid(personUuid);
		List<User> users = userService.getUsersByPerson(person, true);
		for (User user : users) {
			if (user.getPerson().equals(person)) {
				try {
					userService.purgeUser(user);
				}
				catch (Exception e) {
					hasException = true;
				}
			}
		}
		simpleObject.put("hasException", hasException);
		return simpleObject;
	}
	
	//only userRoles and login facilities are editable from the admin interface.
	public String post(@RequestParam("userName") String userName, @RequestParam("personId") Person personId,
	        @RequestParam(value = "password", required = false) String password,
	        @RequestParam(value = "userRole") String userRoles,
	        @RequestParam(required = false, value = "forcePassword") Boolean forcePassword,
	        @RequestParam(value = "userLoginFacility", required = false) String userLoginFacility) {
		Account account = new Account(personId);
		User user = account.getUserAccounts().get(0);
		if (userRoles != null) {
			String[] userRoleUuids = userRoles.split(",");
			for (Role uRole : user.getAllRoles()) {
				user.removeRole(uRole);
			}
			for (String uuid : userRoleUuids) {
				user.addRole(userService.getRoleByUuid(uuid));
			}
		}
		if (!password.isEmpty()) {
			userService.changePassword(user, password);
		}
		
		if (forcePassword != null) {
			user.setUserProperty(FORCE_PASSWORD_CHANGE, Boolean.TRUE.toString());
		}
		
		//update user properties -- facility locations assigned to a user
		user.setUserProperty(LOCATION_USER_PROPERTY_NAME, userLoginFacility);
		Context.getUserService().saveUser(user);
		Context.getService(AccountService.class).saveAccount(account);
		
		return null;
	}
	
	public String createNewAccount(@RequestParam(value = "personGivenName", required = false) String personGivenName,
	        @RequestParam(value = "personMiddleName", required = false) String personMiddleName,
	        @RequestParam("personFamilyName") String personFamilyName,
	        @RequestParam(value = "personGender", required = false) String personGender,
	        @RequestParam(value = "userName") String userName,
	        @RequestParam(value = "password", required = false) String password,
	        @RequestParam(value = "userRole", required = false) String userRoles,
	        @RequestParam(value = "userLoginFacility", required = false) String userLoginFacility,
	        @SpringBean("adminAccountService") AccountService accountService) {
		
		User currentlyAuthenticatedUser = Context.getAuthenticatedUser();
		PersonName personName = new PersonName();
		personName.setGivenName(personGivenName);
		personName.setMiddleName(personMiddleName);
		personName.setFamilyName(personFamilyName);
		
		Person person = new Person();
		person.setGender(personGender);
		person.addName(personName);
		
		Person savedPerson = null;
		
		try {
			savedPerson = Context.getPersonService().savePerson(person);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		List<User> users = userService.getAllUsers();
		if (!users.isEmpty() && users.stream().noneMatch(user -> user.getUsername().equals(userName))) {
			User user = new User(savedPerson);
			user.setCreator(Context.getAuthenticatedUser());
			user.setUserProperty(LOCATION_USER_PROPERTY_NAME, userLoginFacility);
			user.setUsername(userName);
			
			if (userRoles != null) {
				String[] userRoleUuids = userRoles.split(",");
				for (String uuid : userRoleUuids) {
					user.addRole(userService.getRoleByUuid(uuid));
				}
			}
			
			User newUser = null;
			try {
				newUser = Context.getUserService().createUser(user, password);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			Provider provider = new Provider();
			provider.setIdentifier(String.valueOf(UUID.randomUUID()));
			
			Account account = new Account(savedPerson);
			account.addUserAccount(newUser);
			account.setFamilyName(savedPerson != null ? savedPerson.getFamilyName() : null);
			account.setGivenName(savedPerson != null ? savedPerson.getGivenName() : null);
			account.setGender(savedPerson != null ? savedPerson.getGender() : null);
			account.addProviderAccount(provider);
			try {
				account.setPassword(currentlyAuthenticatedUser, password);
				accountService.saveAccount(account);
				
				return null;
			}
			catch (Exception e) {
				return null;
			}
		}
		return null;
	}
}
