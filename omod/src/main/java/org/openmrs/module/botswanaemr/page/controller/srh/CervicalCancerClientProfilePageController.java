/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CervicalCancerClientProfilePageController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui, final PageModel pageModel,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("formService") FormService formService, UiSessionContext sessionContext) {
		
		Visit currentPatientVisit = visit == null
		        ? BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false)
		        : visit;
		
		boolean hasActiveVisit = currentPatientVisit != null;
		
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("patient", patient);
		
		if (currentPatientVisit != null) {
			pageModel.addAttribute("visit", currentPatientVisit);
			pageModel.addAttribute("visitId", currentPatientVisit.getVisitId());
		} else {
			pageModel.addAttribute("visit", null);
			pageModel.addAttribute("visitId", "");
		}
		
		boolean resultInterpretationRecorded = currentPatientVisit.getEncounters().stream()
		        .filter(encounter -> encounter.getEncounterType().getUuid()
		                .equals(BotswanaEmrConstants.HPV_REQUEST_ENCOUNTER_TYPE_UUID))
		        .flatMap(encounter -> encounter.getObs().stream())
		        .filter(obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.RESULT_INTERPRESENTATION_CONCEPT_UUID))
		        .anyMatch(obs -> {
			        List<String> screeningUuids = Arrays.asList(BotswanaEmrConstants.POSITIVE_HIGH_HPV_CONCEPT_UUID,
			            BotswanaEmrConstants.HUMAN_PAPILLOMAVIRUS_SUBTYPE_16_CONCEPT_UUID,
			            BotswanaEmrConstants.HUMAN_PAPILLOMAVIRUS_SUBTYPE_18_CONCEPT_UUID,
			            BotswanaEmrConstants.HUMAN_PAPILLOMAVIRUS_SUBTYPE_45_CONCEPT_UUID);
			        return screeningUuids.contains(obs.getValueCoded().getUuid());
		        });
		
		// if VIA outcome is +ve and does not qualify for treatment or Negative or Clinical Suspicion of Cancer then Display Colposcopy/LEEP Form
		List<Obs> viaScreeningObs = currentPatientVisit.getEncounters().stream()
		        .filter(encounter -> encounter.getEncounterType().getUuid()
		                .equals(BotswanaEmrConstants.VIA_SCREENING_ENCOUNTER_TYPE_UUID))
		        .flatMap(encounter -> encounter.getObs().stream())
		        .filter(obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.SCREENING_RESULT_CONCEPT_UUID))
		        .collect(Collectors.toList()).stream()
		        .filter(obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.SCREENING_RESULT_CONCEPT_UUID))
		        .collect(Collectors.toList());
		Obs viaScreeningOutcome = null;
		if (viaScreeningObs != null && !viaScreeningObs.isEmpty()) {
			viaScreeningOutcome = viaScreeningObs.get(0);
		}
		//if via outcome is positive and does not qualify for treatment or (negative or clinical suspicion of cancer)
		boolean viaOutcomePositive = viaScreeningOutcome != null
		        && viaScreeningOutcome.getValueCoded().getUuid().equals(BotswanaEmrConstants.POSITIVE);
		
		boolean viaOutcomeNegative = viaScreeningOutcome != null
		        && viaScreeningOutcome.getValueCoded().getUuid().equals(BotswanaEmrConstants.NEGATIVE);
		
		boolean viaOutcomeClinicalSuspicionOfCancer = viaScreeningOutcome != null && viaScreeningOutcome.getValueCoded()
		        .getUuid().equals(BotswanaEmrConstants.CLINICAL_SUSPICION_OF_CANCER_CONCEPT_UUID);
		boolean displayColposcopyLeepForm = false;
		if (viaOutcomeNegative || viaOutcomeClinicalSuspicionOfCancer) {
			displayColposcopyLeepForm = true;
		}
		
		pageModel.addAttribute("resultInterpretationRecorded", resultInterpretationRecorded);
		pageModel.addAttribute("displayColposcopyLeepForm", displayColposcopyLeepForm);
		pageModel.addAttribute("currentVisit", currentPatientVisit);
		pageModel.addAttribute("returnUrl", HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "srh/cervicalCancerClientProfile", patient, currentPatientVisit, ui));
		
		pageModel.addAttribute("vatFormUuid", BotswanaEmrConstants.VAT_CLIENT_SCREENING_FORM_UUID);
		pageModel.addAttribute("viaFormUuid", BotswanaEmrConstants.CERVICAL_CANCER_SCREENING_FORM_UUID);
		pageModel.addAttribute("gynaecologyFormUuid", BotswanaEmrConstants.GYNAECOLOGY_CYTOLOGY_FORM_UUID);
		pageModel.addAttribute("hpvRequestFormUuid", BotswanaEmrConstants.HPV_REQUEST_FORM_UUID);
		pageModel.addAttribute("colposcopyLeepFormUuid", BotswanaEmrConstants.COLPOSCOPY_LEEP_SCREENING_FORM_UUID);
		
		List<Form> forms = new ArrayList<>();
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.HPV_REQUEST_FORM_UUID));
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.VAT_CLIENT_SCREENING_FORM_UUID));
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.CERVICAL_CANCER_SCREENING_FORM_UUID));
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.COLPOSCOPY_LEEP_SCREENING_FORM_UUID));
		forms.add(formService.getFormByUuid(BotswanaEmrConstants.GYNAECOLOGY_CYTOLOGY_FORM_UUID));
		
		forms.removeAll(Collections.singleton(null));
		
		List<Encounter> ccsEncounters = new ArrayList<>();
		ccsEncounters = encounterService.getEncounters(
		    new EncounterSearchCriteria(patient, null, null, null, null, forms, null, null, null, null, false));
		pageModel.addAttribute("ccsEncounters", ccsEncounters);
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("visit", currentPatientVisit);
	}
}
