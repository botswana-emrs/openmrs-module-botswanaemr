/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openmrs.Location;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedItemStockDetail;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.page.PageModel;

public class ExpiredItemsPageController {
	
	public static void controller(PageModel model, UiSessionContext sessionContext) {
		
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		Date today = DateUtil.getStartOfDay(new Date());
		
		List<SimplifiedItemStockDetail> simplifiedItemStockDetails = StockUtils.getAllItemsInLocation(location);
		
		List<SimplifiedItemStockDetail> expiredItems = StockUtils.filterByExpiryDate(simplifiedItemStockDetails, today);
		model.addAttribute("expiredItems", expiredItems);
		
	}
	
}
