/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttribute;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttributeType;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID;

@Slf4j
public class AddStockRoomsFormFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null) {
			fragmentModel.addAttribute("hasMainRoom",
			    StockUtils.getMainStockroom(uiSessionContext.getSessionLocation()) != null);
		} else {
			fragmentModel.addAttribute("hasMainRoom", false);
		}
		
	}
	
	public void saveCreatedStockRoom(@RequestParam("name") String name, @RequestParam("description") String description,
	        @RequestParam("stockRoomType") String stockRoomType, UiSessionContext uiSessionContext) {
		
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		
		Stockroom stockroom = new Stockroom();
		
		stockroom.setName(name);
		stockroom.setDescription(description);
		stockroom.setDateCreated(new Date());
		stockroom.setCreator(Context.getAuthenticatedUser());
		stockroom.setLocation(uiSessionContext.getSessionLocation());
		
		StockRoomAttribute stockRoomTypeAttribute = new StockRoomAttribute();
		stockRoomTypeAttribute.setAttributeType(getStockRoomType());
		stockRoomTypeAttribute.setCreator(Context.getAuthenticatedUser());
		stockRoomTypeAttribute.setValue(stockRoomType);
		stockRoomTypeAttribute.setName(stockRoomType);
		stockRoomTypeAttribute.setDateCreated(new Date());
		
		try {
			Stockroom newlyCreatedStockRoom = iStockroomDataService.save(stockroom);
			stockRoomTypeAttribute.setOwner(newlyCreatedStockRoom);
			
			//Add the attribute
			BotswanaInventoryContext.getStockRoomAttributeDataService().save(stockRoomTypeAttribute);
			
			log.info("\nSaved new Stockroom\n" + stockroom);
		}
		catch (Exception e) {
			log.warn("\nAn error occurred while saving stock room\n", e);
			throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
			        "An error occurred while adding new stock room: " + e.getMessage());
		}
	}
	
	public void saveEditedStockRoom(@RequestParam(value = "stockRoomId", required = false) Integer stockRoomId,
	        @RequestParam("name") String name, @RequestParam("description") String description,
	        @RequestParam("stockRoomType") String stockRoomType,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService) throws ParseException {
		
		Stockroom stockroom;
		
		if (stockRoomId != null) {
			stockroom = iStockroomDataService.getById(stockRoomId);
			
			stockroom.setName(name);
			stockroom.setDescription(description);
			
			StockRoomAttribute stockRoomTypeAttribute = stockroom.getActiveAttributes().stream()
			        .filter(s -> s.getAttributeType().getUuid().equals(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID)).findFirst()
			        .orElse(null);
			try {
				String previousStockroomType = stockRoomTypeAttribute != null ? stockRoomTypeAttribute.getValue() : null;
				if (!stockRoomType.equals(previousStockroomType)) {
					stockroom.getAttributes().clear();
					stockRoomTypeAttribute = new StockRoomAttribute();
					stockRoomTypeAttribute.setAttributeType(getStockRoomType());
					stockRoomTypeAttribute.setCreator(Context.getAuthenticatedUser());
					stockRoomTypeAttribute.setValue(stockRoomType);
					stockRoomTypeAttribute.setName(stockRoomType);
					stockRoomTypeAttribute.setOwner(stockroom);
					stockroom.addAttribute(stockRoomTypeAttribute);
				}
				
				stockroom = iStockroomDataService.save(stockroom);
				
				log.info("\nSaved Stock room\n" + stockroom);
			}
			catch (Exception e) {
				log.warn("\nAn error occurred while saving stock room\n", e);
				throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
				        "An error occurred while saving stock room: " + e.getMessage());
			}
		}
	}
	
	private StockRoomAttributeType getStockRoomType() {
		return BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
	}
	
}
