/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.mapper;

public class PatientsLinkagePoolMapper {
	
	private String patientId;
	
	private String patientNames;
	
	private String gender;
	
	private String age;
	
	private String facility;
	
	private String status;
	
	private String artEncounterId;
	
	private String visitNumber;
	
	private String pin;
	
	private Boolean artEnrolled;
	
	public Boolean getArtEnrolled() {
		return artEnrolled;
	}
	
	public void setArtEnrolled(Boolean artEnrolled) {
		this.artEnrolled = artEnrolled;
	}
	
	public String getPatientId() {
		return patientId;
	}
	
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	public String getPatientNames() {
		return patientNames;
	}
	
	public void setPatientNames(String patientNames) {
		this.patientNames = patientNames;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getAge() {
		return age;
	}
	
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getFacility() {
		return facility;
	}
	
	public void setFacility(String facility) {
		this.facility = facility;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getArtEncounterId() {
		return artEncounterId;
	}
	
	public void setArtEncounterId(String artEncounterId) {
		this.artEncounterId = artEncounterId;
	}
	
	public String getVisitNumber() {
		return visitNumber;
	}
	
	public void setVisitNumber(String visitNumber) {
		this.visitNumber = visitNumber;
	}
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
}
