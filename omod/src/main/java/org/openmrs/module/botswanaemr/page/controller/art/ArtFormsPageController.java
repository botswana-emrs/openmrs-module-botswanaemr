package org.openmrs.module.botswanaemr.page.controller.art;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.FormService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ART_TRANSFER_OUT_FORM_UUID;

public class ArtFormsPageController {
    public void controller(UiUtils ui, UiSessionContext sessionContext,
                           @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
                           @SpringBean("formService") FormService formService,
                           PageModel model,
                           @RequestParam("patientId") Patient patient,
                           @RequestParam(value = "returnUrl", required = false) String returnUrl,
                           @RequestParam(value = "selectedFormUuid", required = false) String selectedFormUuid,
                           @RequestParam(value = "encounterId", required = false) Encounter encounter,
                           @RequestParam(value = "visitId", required = false) Visit visit) {
        model.addAttribute("patient", patient);
        model.addAttribute("visit", visit);
        model.addAttribute("defaultEncounterDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
        model.addAttribute("encounter", encounter);

        List<SimpleObject> forms = new ArrayList<>();
        if (selectedFormUuid.equals(BotswanaEmrConstants.ART_PREP_INITIATION_FORM_UUID)) {
            forms.add(getFormData("Prep Initiation", BotswanaEmrConstants.ART_PREP_INITIATION_FORM_UUID,
                    BotswanaEmrConstants.ART_PREP_INITIATION_FORM_ENCOUNTER_TYPE_UUID));
        }
        if (selectedFormUuid.equals(BotswanaEmrConstants.PEP_INITIATION_FORM_UUID)) {
            forms.add(getFormData("Pep Initiation", BotswanaEmrConstants.PEP_INITIATION_FORM_UUID,
                    BotswanaEmrConstants.ART_PEP_FORM_ENCOUNTER_TYPE_UUID));
        }
        if (selectedFormUuid.equals(BotswanaEmrConstants.ART_PrEP_FOLLOW_UP_FORM_UUID)) {
            forms.add(getFormData("Prep Follow-up", BotswanaEmrConstants.ART_PrEP_FOLLOW_UP_FORM_UUID,
                    BotswanaEmrConstants.ART_PREP_FOLLOW_UP_FORM_ENCOUNTER_TYPE_UUID));
        }
        if (selectedFormUuid.equals(BotswanaEmrConstants.ART_PEP_FOLLOW_UP_FORM_UUID)) {
            forms.add(getFormData("Pep Follow-up", BotswanaEmrConstants.ART_PEP_FOLLOW_UP_FORM_UUID,
                    BotswanaEmrConstants.ART_PEP_FOLLOW_UP_FORM_ENCOUNTER_TYPE_UUID));
        }
        HtmlForm form = htmlFormEntryService
                .getHtmlFormByForm(formService.getFormByUuid(selectedFormUuid));
        model.addAttribute("formName", form.getName());

        forms.add(getFormData("TB Form", BotswanaEmrConstants.TB_SCREENING_FORM_UUID,
                BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID));

        model.addAttribute("forms", forms);
        JsonArray results = (JsonArray) new Gson().toJsonTree(forms, new TypeToken<List<SimpleObject>>() {}.getType());
        model.addAttribute("results", results);
        model.addAttribute("selected", StringUtils.isNotEmpty(selectedFormUuid) ? selectedFormUuid : "");
        model.addAttribute("returnUrl", ui.encodeForSafeURL(
                HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "srh/artDashboard", patient, visit, ui)));

    }
    private SimpleObject getFormData(String title, String formUuid, String encounterTypeUuid) {
        SimpleObject simpleObject = new SimpleObject();
        simpleObject.put("title", title);
        simpleObject.put("form", formUuid);
        simpleObject.put("encounterType", encounterTypeUuid);

        return simpleObject;
    }
}
