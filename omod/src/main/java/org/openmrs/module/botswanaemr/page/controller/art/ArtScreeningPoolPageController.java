/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.art;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.VisitType;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;

public class ArtScreeningPoolPageController extends QueuePatientFragmentController {
	
	private final List<String> artServiceTypes = new ArrayList<String>() {
		
		{
			add(BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PREP_FOLLOW_UP_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.PEP_FOLLOW_UP_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.ART_INITIATION_VISIT_TYPE_UUID);
			add(BotswanaEmrConstants.ART_FOLLOW_UP_VISIT_TYPE_UUID);
		}
	};
	
	public void controller(@SpringBean PageModel pageModel, UiSessionContext uiSessionContext) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		
		pageModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		pageModel.addAttribute("providerList", providerList);
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program artProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.ART_PROGRAM_UUID);
		pageModel.addAttribute("artProgramId", artProgram.getUuid());
		pageModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		Set<VisitType> visitTypes = new LinkedHashSet<>();
		VisitService visitService = Context.getVisitService();
		
		for (String visitTypeUuid : artServiceTypes) {
			visitTypes.add(visitService.getVisitTypeByUuid(visitTypeUuid));
		}
		visitTypes.remove(null);
		pageModel.addAttribute("visitTypes", visitTypes);
	}
}
