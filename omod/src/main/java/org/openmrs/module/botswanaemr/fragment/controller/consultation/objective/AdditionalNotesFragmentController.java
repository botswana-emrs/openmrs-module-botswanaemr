/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.objective;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

public class AdditionalNotesFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam(value = "visitId") Visit visit,
	        @RequestParam("patientId") Patient person, UiSessionContext uiSessionContext) {
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		List<Obs> additionalNotesObs = botswanaEmrService.getObservation(person, visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.OBJECTIVE_ADDITIONAL_NOTES_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		fragmentModel.addAttribute("additionalNotes", additionalNotesObs);
		fragmentModel.addAttribute("lastAdditionalNotes", additionalNotesObs.size() > 1 ? additionalNotesObs.get(0) : null);
		fragmentModel.addAttribute("patient", person);
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
	}
	
	public void addAdditionalNotes(@RequestParam("patientId") Patient patient,
	        @RequestParam("additionalNotes") String additionalNotes,
	        @RequestParam(value = "visitId", required = false) Visit visit, @RequestParam("locationId") Location location) {
		EncounterType consultationEncounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE);
		Encounter consultationsEncounter = BotswanaEmrUtils.createEncounter(patient, consultationEncounterType, location,
		    visit);
		if (StringUtils.isNotBlank(additionalNotes)) {
			Obs additionalNotesObs = BotswanaEmrUtils.creatObs(consultationsEncounter,
			    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.OBJECTIVE_ADDITIONAL_NOTES_CONCEPT_UUID));
			additionalNotesObs.setValueText(additionalNotes);
			
			//add this observation to the encounter
			consultationsEncounter.addObs(additionalNotesObs);
			Context.getEncounterService().saveEncounter(consultationsEncounter);
		}
	}
	
	public void editAdditionalNotes(@RequestParam("lastAdditionalObsId") Integer lastAdditionalObsId,
	        @RequestParam("editAdditionalNotes") String editAdditionalNotes) {
		ObsService obsService = Context.getObsService();
		if (lastAdditionalObsId != null && StringUtils.isNotBlank(editAdditionalNotes)) {
			Obs obs = obsService.getObs(lastAdditionalObsId);
			obs.setValueText(editAdditionalNotes);
			
			//commit the edited obs in the database
			obsService.saveObs(obs, "Edited Additional notes");
			
		}
	}
	
	public List<SimpleObject> fetchAdditionalNotes(@RequestParam(value = "visitId") Integer visitId,
			@RequestParam("patientId") Integer patientId,
			UiSessionContext uiSessionContext, @SpringBean("patientService") PatientService patientService,
			@SpringBean("visitService") VisitService visitService) {
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		
		Patient person = patientService.getPatient(patientId);
		Visit visit = visitService.getVisit(visitId);
		
		List<Obs> additionalNotesObs = botswanaEmrService.getObservation(person, visit,
				BotswanaEmrUtils.getConcept(BotswanaEmrConstants.OBJECTIVE_ADDITIONAL_NOTES_CONCEPT_UUID),
				uiSessionContext.getSessionLocation(), limitFetch);
		
		return additionalNotesObs.stream().map(obs -> {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("obsId", obs.getObsId());
			simpleObject.put("value", obs.getValueAsString(Context.getLocale()));
			simpleObject.put("dateCreated", obs.getDateCreated());
			return simpleObject;
		}).collect(Collectors.toList());
	}
}
