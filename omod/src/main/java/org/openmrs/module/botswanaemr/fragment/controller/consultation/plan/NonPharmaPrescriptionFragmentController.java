/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.plan;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

@Slf4j
public class NonPharmaPrescriptionFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiSessionContext uiSessionContext) {
		
		Obs nonPharmaPrescriptionObs = null;
		for (Encounter encounter : visit.getNonVoidedEncounters()) {
			if (encounter.getEncounterType().getUuid().equals(BotswanaEmrConstants.NON_PHARMA_PLAN_ENCOUNTER_TYPE_UUID)) {
				nonPharmaPrescriptionObs = encounter.getAllObs(false).stream()
				        .filter(e -> e.getConcept().getUuid().equals(BotswanaEmrConstants.NON_PHARMA_PLAN_CONCEPT_UUID))
				        .findAny().orElse(null);
			}
		}
		fragmentModel.addAttribute("visit", visit);
		fragmentModel.addAttribute("nonPharmaPrescriptionObs", nonPharmaPrescriptionObs);
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
		fragmentModel.addAttribute("patient", patient.getUuid());
		fragmentModel.addAttribute("patientId", patient.getPatientId());
	}
	
	public void createNonPharmaPrescription(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "nonPharmaPrescription") String nonPharmaPrescriptionPlan,
	        @RequestParam(value = "visitId") Visit visit, UiSessionContext sessionContext) throws IOException {
		
		EncounterType nonPharmaEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.NON_PHARMA_PLAN_ENCOUNTER_TYPE_UUID);
		Encounter nonPharmaPrescriptionPlanEncounter = BotswanaEmrUtils.createEncounter(patient, nonPharmaEncounterType,
		    sessionContext.getSessionLocation(), visit);
		
		Concept nonPharmaPrescriptionPlanConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.NON_PHARMA_PLAN_CONCEPT_UUID);
		
		Obs nonPharmaPrescriptionPlanObs = BotswanaEmrUtils.creatObs(nonPharmaPrescriptionPlanEncounter,
		    nonPharmaPrescriptionPlanConcept);
		nonPharmaPrescriptionPlanObs.setValueText(nonPharmaPrescriptionPlan);
		nonPharmaPrescriptionPlanEncounter.addObs(nonPharmaPrescriptionPlanObs);
		Context.getEncounterService().saveEncounter(nonPharmaPrescriptionPlanEncounter);
	}
	
	public void editNonPharmaPrescriptionPlan(
	        @RequestParam(value = "nonPharmaPrescriptionObsUuid", required = false) String nonPharmaPrescriptionObsUuid,
	        @RequestParam(value = "editableNonPharmaPrescriptionPlan") String editableNonPharmaPrescriptionPlan) {
		
		ObsService obsService = Context.getObsService();
		
		if (nonPharmaPrescriptionObsUuid != null && StringUtils.isNotBlank(editableNonPharmaPrescriptionPlan)) {
			Obs obs = obsService.getObsByUuid(nonPharmaPrescriptionObsUuid);
			obs.setValueText(editableNonPharmaPrescriptionPlan);
			obsService.saveObs(obs, "Edited NonPharmaPrescription plan successfully");
		}
	}
	
	public List<SimpleObject> fetchNonPharmaPrescription(@RequestParam(value = "visitId") Integer visitId,
			@RequestParam("patientId") Integer patientId,
			UiSessionContext uiSessionContext,
			@SpringBean("patientService") PatientService patientService,
			@SpringBean("visitService") VisitService visitService) {
		
		Patient patient = patientService.getPatient(patientId);
		Visit visit = visitService.getVisit(visitId);
		
		Obs nonPharmaPrescriptionObs = null;
		for (Encounter encounter : visit.getNonVoidedEncounters()) {
			if (encounter.getEncounterType().getUuid().equals(BotswanaEmrConstants.NON_PHARMA_PLAN_ENCOUNTER_TYPE_UUID)) {
				nonPharmaPrescriptionObs = encounter.getAllObs(false).stream()
						.filter(e -> e.getConcept().getUuid().equals(BotswanaEmrConstants.NON_PHARMA_PLAN_CONCEPT_UUID))
						.findAny().orElse(null);
			}
		}
		
		SimpleObject response = new SimpleObject();
		response.put("visitUuid", visit.getUuid());
		response.put("visitDate", visit.getDateCreated());
		response.put("visitLocation", uiSessionContext.getSessionLocation().getName());
		if (nonPharmaPrescriptionObs != null) {
			response.put("nonPharmaPrescriptionObs", nonPharmaPrescriptionObs.getValueText());
		}
		response.put("patientUuid", patient.getUuid());
		response.put("patientId", patient.getPatientId());
		return Collections.singletonList(response);
	}
}
