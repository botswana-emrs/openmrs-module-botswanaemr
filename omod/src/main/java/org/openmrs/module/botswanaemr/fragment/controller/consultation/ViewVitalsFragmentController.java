/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ViewVitalsFragmentController {
	
	public void controller(UiSessionContext uiSessionContext, @SpringBean FragmentModel model,
	        @FragmentParam("patientId") Patient patient, @FragmentParam(value = "visitId", required = false) Visit visit,
	        @FragmentParam(value = "returnUrl", required = false) String returnUrl, UiUtils ui) {
		model.addAttribute("patient", patient);
		model.addAttribute("activePatientAge", patient.getAge());
		model.addAttribute("visit", visit);
		model.addAttribute("returnUrl", ui.pageLink("botswanaemr", "consultation/nursesExaminationPatientPool",
		    SimpleObject.create("patientId", patient.getId(), "visitId", visit.getId(), "returnUrl", returnUrl)));
		
		List<Visit> visits = Context.getVisitService().getVisitsByPatient(patient, true, false);
		List<Visit> sortedVisits = visits.stream().sorted(Comparator.comparing(Visit::getDateCreated).reversed()).limit(4)
		        .collect(Collectors.toList());
		List<SimpleObject> vitalsData = new ArrayList<>();
		List<Encounter> encounterList = new ArrayList<>();
		
		for (Visit visit1 : sortedVisits) {
			List<Encounter> encounters = visit1.getNonVoidedEncounters().stream().filter(encounter -> encounter
			        .getEncounterType().getUuid().equals(BotswanaEmrConstants.DOCTORS_CONSULTATION_ENCOUNTER_TYPE)
			        || encounter.getEncounterType().getUuid()
			                .equals(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID)
			        || encounter.getEncounterType().getUuid().equals(BotswanaEmrConstants.VITALS_ENCOUNTER_TYPE_UUID))
			        .sorted(Comparator.comparing(Encounter::getDateCreated).reversed()).collect(Collectors.toList());
			encounterList.addAll(encounters);
		}
		Map<Date, List<Encounter>> groupedByDate = encounterList.stream()
		        .collect(Collectors.groupingBy(encounter -> normalizeDate(encounter.getEncounterDatetime()),
		            () -> new TreeMap<>((d1, d2) -> d2.compareTo(d1)), Collectors.toList()));
		
		groupedByDate.forEach((date, groupedEncounterList) -> {
			List<SimpleObject> simpleObjectList = getVitalsPerDay(groupedEncounterList);
			vitalsData.add(SimpleObject.create("day", BotswanaEmrUtils.formatDateWithoutTime(date, "dd MMMM yyyy"), "data",
			    simpleObjectList));
		});
		
		model.addAttribute("vitalsData", vitalsData);
		
	}
	
	private static Date normalizeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	private List<SimpleObject> getVitalsPerDay(List<Encounter> encounters) {
		List<SimpleObject> simpleObjects = new ArrayList<>();
		for (Encounter encounter : encounters) {
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("time", BotswanaEmrUtils.formatDateWithoutTime(encounter.getEncounterDatetime(), "HH:mm"));
			for (Obs obs : encounter.getAllObs(false)) {
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.TEMPERATURE)) {
					simpleObject.put("temp", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.SYSTOLIC_BP)) {
					simpleObject.put("sys", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.DIASTOLIC_BP)) {
					simpleObject.put("dia", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.WEIGHT_CONCEPT_UUID)) {
					simpleObject.put("weight", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HEIGHT)) {
					simpleObject.put("height", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.BMI)) {
					simpleObject.put("bmi", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.BSA)) {
					simpleObject.put("bsa", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.RESPIRATORY_RATE)) {
					simpleObject.put("rr", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.PULSE)) {
					simpleObject.put("pulse", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HEAD_CIRCUMFERENCE)) {
					simpleObject.put("head", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.GLUCOSE_LEVEL)) {
					simpleObject.put("glucose", obs);
				}
				if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.OXYGEN_SATURATION)) {
					simpleObject.put("oxygen", obs);
				}
			}
			simpleObjects.add(simpleObject);
		}
		
		return simpleObjects;
	}
}
