/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utils.UuidGenerator;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

public class PaymentMethodFragmentController {
	
	public void get(PageModel model) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		model.addAttribute("paymentMethods", service.getAllPaymentMethods(false));
		
	}
	
	public String registerPaymentMethod(@RequestParam(value = "methodName") String name,
	        @RequestParam(value = "description") String description, UiUtils uiUtils) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		PaymentMethod paymentMethod;
		if (StringUtils.isNotEmpty(name)) {
			paymentMethod = service.getAllPaymentMethods(true).stream().filter(p -> p.getName().equals(name)).findFirst()
			        .orElse(null);
			if (paymentMethod == null) {
				paymentMethod = new PaymentMethod();
				paymentMethod.setName(name);
				paymentMethod.setDateCreated(new Date());
				paymentMethod.setCreator(Context.getAuthenticatedUser());
				paymentMethod.setUuid(UuidGenerator.getNextUuid());
			} else {
				paymentMethod.setRetired(false);
				paymentMethod.setDateRetired(null);
			}
			paymentMethod.setDescription(description);
			
			//save the payment method
			service.savePaymentMethod(paymentMethod);
		}
		
		return null;
	}
	
}
