/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.discussion;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.openmrs.PersonName;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class MessageFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "message") Message message) {
		User authoredBy = message.getAuthoredBy();
		String time = BotswanaEmrUtils.getDurationSince(message.getDate());
		fragmentModel.addAttribute("message", message);
		fragmentModel.addAttribute("who", authoredBy.equals(Context.getAuthenticatedUser()) ? "me" : "them");
		fragmentModel.addAttribute("author", formatPersonName(authoredBy.getPersonName()));
		fragmentModel.addAttribute("time", time);
	}
}
