/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.DailyRegisteredListFragmentController;
import org.openmrs.module.botswanaemr.model.SimplifiedPatient;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GraphPatientViewPageController extends DailyRegisteredListFragmentController {
	
	public void controller(PageModel pageModel, UiSessionContext sessionContext, UiUtils ui,
	        @RequestParam(value = "type", required = false) BotswanaEmrConstants.summaryType typeOfSummary,
	        @RequestParam(value = "encounterType", required = false) String encounterTypeUuid,
	        @RequestParam(value = "day") String day) throws ParseException {
		Date today = DateUtil.getStartOfDay(new Date());
		String sessionLocationName = "";
		Location sessionLocation = sessionContext.getSessionLocation();
		if (sessionLocation != null) {
			sessionLocationName = sessionLocation.getName();
		}
		encounterService = Context.getEncounterService();
		
		pageModel.addAttribute("sessionLocation", sessionLocationName);
		String title = "";
		
		List<SimplifiedPatient> simplifiedPatients = new ArrayList<>();
		if (typeOfSummary != null && StringUtils.isNotEmpty(day)) {
			Date date = getDate(day);
			
			if (typeOfSummary == BotswanaEmrConstants.summaryType.REGISTRATION) {
				List<Registration> paymentList = BotswanaEmrUtils.getPatientsSeenOnDay(date, sessionLocation);
				simplifiedPatients = getSimplifiedPatientRegistrationDetails(paymentList, ui);
				title = "Registered Patients";
			} else if (typeOfSummary == BotswanaEmrConstants.summaryType.SCREENING) {
				if (StringUtils.isEmpty(encounterTypeUuid)) {
					encounterTypeUuid = BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID;
				}
				
				EncounterType encounterType = encounterService.getEncounterTypeByUuid(encounterTypeUuid);
				List<Patient> patientList = BotswanaEmrUtils.getPatientsScreenedOnDay(date, encounterType, sessionLocation);
				simplifiedPatients = getSimplifiedPatients(patientList);
				title = "Screened Patients";
			} else if (typeOfSummary == BotswanaEmrConstants.summaryType.CONSULTATION) {
				List<Patient> patientList = BotswanaEmrUtils.getPatientsConsultedOnDay(date, sessionLocation);
				simplifiedPatients = getSimplifiedPatients(patientList);
				title = "Treated Patients";
			} else if (typeOfSummary == BotswanaEmrConstants.summaryType.PROGRAM) {
				
			} else if (typeOfSummary == BotswanaEmrConstants.summaryType.ENROLLMENT) {
				Location location = null;
				if (org.apache.commons.lang.StringUtils.isNotEmpty(encounterTypeUuid)) {
					location = Context.getLocationService().getLocationByUuid(encounterTypeUuid);
				} else {
					location = Context.getLocationService()
					        .getLocationByUuid(BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
				}
				List<Patient> patientList = BotswanaEmrUtils.getPatientsEnrolledOnDay(DateUtils.getDateStartOfDay(date),
				    DateUtils.getDateEndOfDay(date), location, sessionLocation);
				simplifiedPatients = getSimplifiedPatients(patientList);
				title = "Enrolled Patients";
				
			} else {
				simplifiedPatients = getSimplifiedPatientRegistrationDetails(Context.getService(BotswanaEmrService.class)
				        .getPatientsRegisteredOnDate(null, today, sessionLocation, null),
				    ui);
			}
		} else {
			simplifiedPatients = getSimplifiedPatientRegistrationDetails(
			    Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(null, today, sessionLocation, null),
			    ui);
		}
		pageModel.addAttribute("pastRegisteredPatientsSummary", simplifiedPatients);
		pageModel.addAttribute("title", title);
	}
	
	private Date getDate(String day) {
		LocalDate today = new LocalDate();
		LocalDate date = today.dayOfWeek().withMinimumValue();
		if (day.equalsIgnoreCase("mon")) {
			date = date.withDayOfWeek(DateTimeConstants.MONDAY);
		} else if (day.equalsIgnoreCase("tue")) {
			date = date.withDayOfWeek(DateTimeConstants.TUESDAY);
		} else if (day.equalsIgnoreCase("wed")) {
			date = date.withDayOfWeek(DateTimeConstants.WEDNESDAY);
		} else if (day.equalsIgnoreCase("thur")) {
			date = date.withDayOfWeek(DateTimeConstants.THURSDAY);
		} else if (day.equalsIgnoreCase("fri")) {
			date = date.withDayOfWeek(DateTimeConstants.FRIDAY);
		} else if (day.equalsIgnoreCase("sat")) {
			date = date.withDayOfWeek(DateTimeConstants.SATURDAY);
		} else if (day.equalsIgnoreCase("sun")) {
			date = date.withDayOfWeek(DateTimeConstants.SUNDAY);
		} else if (day.equalsIgnoreCase("all")) {
			return null;
		}
		return date.toDate();
	}
}
