/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockTake;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.*;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

public class StockAdjustmentPageController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void get(@RequestParam(value = "stockOperationId", required = false) Integer stockOperationId, PageModel model,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) {
		if (uiSessionContext.getCurrentUser() != null && stockOperationId != null) {
			Set<SimplifiedStockTake> simplifiedStockAdjustmentList = new HashSet<>();
			StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
			        .getById(stockOperationId);
			
			for (StockOperationItem operationItem : stockOperation.getItems()) {
				ItemStock itemStock = stockroomDataService.getItem(stockOperation.getSource(), operationItem.getItem());
				
				SimplifiedStockTake stockTake = new SimplifiedStockTake();
				stockTake.setId(stockOperationId);
				stockTake.setItemCode(
				    itemStock.getItem().getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
				stockTake.setItemName(itemStock.getItem().getName());
				stockTake.setExpiryDate(BotswanaEmrUtils.formatDateWithoutTime(operationItem.getExpiration(), "yyyy-MM-dd"));
				stockTake.setTodayDate(BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
				stockTake.setItemStockUuid(itemStock.getUuid());
				int qnty = itemStock.getQuantity() - operationItem.getQuantity();
				stockTake.setItemStockQuantity(qnty);
				stockTake.setEnteredQuantityInStore(itemStock.getQuantity());
				stockTake.setBatchNumber(operationItem.getOperation().getOperationNumber());
				
				simplifiedStockAdjustmentList.add(stockTake);
			}
			
			model.addAttribute("simplifiedStockAdjustmentList", simplifiedStockAdjustmentList);
			model.addAttribute("selectedStockRoomId", stockOperation.getSource().getId());
			model.addAttribute("stockOperationStatus", stockOperation.getStatus().toString());
		} else {
			model.addAttribute("simplifiedStockAdjustmentList", "");
		}
	}
}
