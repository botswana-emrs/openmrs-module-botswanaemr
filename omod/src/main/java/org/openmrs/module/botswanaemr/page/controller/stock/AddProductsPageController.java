/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import org.openmrs.Drug;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.InventoryItemSimplifier;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Objects;
import java.util.Set;

public class AddProductsPageController {
	
	private final IItemDataService itemDataService = BotswanaInventoryContext.getItemDataService();
	
	private final IItemAttributeDataService itemAttributeDataService = BotswanaInventoryContext
	        .getItemAttributeDataService();
	
	private final IItemAttributeTypeDataService itemAttributeTypeDataService = BotswanaInventoryContext
	        .getItemAttributeTypeDataService();
	
	public void controller(@RequestParam(value = "itemId", required = false) Integer itemId, PageModel pageModel,
	        UiSessionContext uiSessionContext) {
		if (uiSessionContext.getCurrentUser() != null && itemId != null) {
			Item inventoryItem = itemDataService.getById(itemId);
			InventoryItemSimplifier simplifiedItem = InventoryItemSimplifier.simplify(inventoryItem);
			
			pageModel.addAttribute("inventoryItem", simplifiedItem);
			pageModel.addAttribute("itemId", itemId);
		} else {
			pageModel.addAttribute("inventoryItem", "");
			pageModel.addAttribute("itemId", null);
		}
	}
	
	private String getItemAttributeValue(Set<ItemAttribute> itemAttributes, ItemAttributeType itemAttributeType) {
		for (ItemAttribute itemAttribute : itemAttributes) {
			if (Objects.equals(itemAttribute.getAttributeType(), itemAttributeType)) {
				return itemAttribute.getValue();
			}
		}
		return null;
	}
}
