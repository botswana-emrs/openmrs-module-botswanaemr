/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.springframework.web.bind.annotation.RequestParam;

public class ModifyUserPasswordFragmentController {
	
	protected final Log log = LogFactory.getLog(ModifyUserPasswordFragmentController.class);
	
	public void controller() {
		
	}
	
	public void post(@RequestParam("currentPassword") String currentPassword,
	        @RequestParam("newPassword") String newPassword) {
		
		User user = Context.getAuthenticatedUser();
		
		try {
			Context.getUserService().changePassword(user, currentPassword, newPassword);
		}
		catch (Exception e) {
			log.error("Error occurred while trying to change user password", e);
			
		}
	}
	
}
