/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_CONTRACEPTIVE_HISTORY_ENCOUNTER_TYPE_UUID;

public class ContraceptiveHistoryFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam("patientId") Patient patient) {
		fragmentModel.addAttribute("contraceptiveFormUuid", BotswanaEmrConstants.SRH_CONTRACEPTIVE_HISTORY_FORM_UUID);
		EncounterType contraceptiveHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_CONTRACEPTIVE_HISTORY_ENCOUNTER_TYPE_UUID);
		List<Encounter> contraceptiveHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    contraceptiveHistoryEncounterType, null, null, null);
		fragmentModel.addAttribute("contraceptiveHistoryEncounters", contraceptiveHistoryEncounters);
	}
}
