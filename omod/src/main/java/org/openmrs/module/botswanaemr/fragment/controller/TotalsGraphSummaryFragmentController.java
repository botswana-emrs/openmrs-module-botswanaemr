/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_PROGRAM_UUID;

public class TotalsGraphSummaryFragmentController {
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	public void controller(FragmentModel model,
	        @FragmentParam(value = "type", required = false) BotswanaEmrConstants.summaryType typeOfSummary,
	        @FragmentParam(value = "encounterType", required = false) String encounterTypeUuid,
	        @FragmentParam(value = "programType", required = false) String programType, UiSessionContext uiSessionContext)
	        throws ParseException {
		
		List<String> days = getDate();
		String MON = days.get(0);
		String TUE = days.get(1);
		String WED = days.get(2);
		String THUR = days.get(3);
		String FRI = days.get(4);
		String SAT = days.get(5);
		String SUN = days.get(6);
		
		Location sessionLocation = uiSessionContext.getSessionLocation();
		EncounterType encounterType = encounterService.getEncounterTypeByUuid(encounterTypeUuid);
		
		if (typeOfSummary == null) {
			typeOfSummary = BotswanaEmrConstants.summaryType.REGISTRATION;
		}
		BotswanaEmrConstants.summaryType selectedGraphSummaryType;
		switch (typeOfSummary) {
			case SCREENING:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.SCREENING;
				break;
			case PHARMACY:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.PHARMACY;
				break;
			case PROGRAM:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.PROGRAM;
				break;
			case CONSULTATION:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.CONSULTATION;
				break;
			case ENROLLMENT:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.ENROLLMENT;
				break;
			default:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.REGISTRATION;
				break;
		}
		
		model.addAttribute("selectedGraphSummaryType", selectedGraphSummaryType);
		model.addAttribute("encounterTypeUuid", encounterTypeUuid);
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.CONSULTATION)) {
			model.addAttribute("sun", getPatientsConsultedOnDay(SUN, sessionLocation));
			model.addAttribute("mon", getPatientsConsultedOnDay(MON, sessionLocation));
			model.addAttribute("tue", getPatientsConsultedOnDay(TUE, sessionLocation));
			model.addAttribute("wed", getPatientsConsultedOnDay(WED, sessionLocation));
			model.addAttribute("thur", getPatientsConsultedOnDay(THUR, sessionLocation));
			model.addAttribute("fri", getPatientsConsultedOnDay(FRI, sessionLocation));
			model.addAttribute("sat", getPatientsConsultedOnDay(SAT, sessionLocation));
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.SCREENING)) {
			model.addAttribute("sun", getPatientsScreenedOnDay(SUN, encounterType, sessionLocation));
			model.addAttribute("mon", getPatientsScreenedOnDay(MON, encounterType, sessionLocation));
			model.addAttribute("tue", getPatientsScreenedOnDay(TUE, encounterType, sessionLocation));
			model.addAttribute("wed", getPatientsScreenedOnDay(WED, encounterType, sessionLocation));
			model.addAttribute("thur", getPatientsScreenedOnDay(THUR, encounterType, sessionLocation));
			model.addAttribute("fri", getPatientsScreenedOnDay(FRI, encounterType, sessionLocation));
			model.addAttribute("sat", getPatientsScreenedOnDay(SAT, encounterType, sessionLocation));
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.REGISTRATION)) {
			model.addAttribute("sun", getPatientsSeenOnDay(SUN, sessionLocation));
			model.addAttribute("mon", getPatientsSeenOnDay(MON, sessionLocation));
			model.addAttribute("tue", getPatientsSeenOnDay(TUE, sessionLocation));
			model.addAttribute("wed", getPatientsSeenOnDay(WED, sessionLocation));
			model.addAttribute("thur", getPatientsSeenOnDay(THUR, sessionLocation));
			model.addAttribute("fri", getPatientsSeenOnDay(FRI, sessionLocation));
			model.addAttribute("sat", getPatientsSeenOnDay(SAT, sessionLocation));
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.ENROLLMENT)) {
			Location location = null;
			if (StringUtils.isNotEmpty(encounterTypeUuid)) {
				location = Context.getLocationService().getLocationByUuid(encounterTypeUuid);
			} else {
				location = Context.getLocationService()
				        .getLocationByUuid(BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
			}
			
			Location currentLocation = uiSessionContext.getSessionLocation();
			
			model.addAttribute("sun", getPatientsEnrolledOnLocation(SUN, location, currentLocation));
			model.addAttribute("mon", getPatientsEnrolledOnLocation(MON, location, currentLocation));
			model.addAttribute("tue", getPatientsEnrolledOnLocation(TUE, location, currentLocation));
			model.addAttribute("wed", getPatientsEnrolledOnLocation(WED, location, currentLocation));
			model.addAttribute("thur", getPatientsEnrolledOnLocation(THUR, location, currentLocation));
			model.addAttribute("fri", getPatientsEnrolledOnLocation(FRI, location, currentLocation));
			model.addAttribute("sat", getPatientsEnrolledOnLocation(SAT, location, currentLocation));
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.PROGRAM)) {
			List<PatientProgram> patientPrograms = new ArrayList<>();
			DateTime startDate = new DateTime(new Date()).withDayOfWeek(DateTimeConstants.MONDAY);
			DateTime endDate = new DateTime(new Date()).withDayOfWeek(DateTimeConstants.SUNDAY);
			Program program = Context.getService(ProgramWorkflowService.class).getProgramByUuid(TB_PROGRAM_UUID);
			if (program != null) {
				patientPrograms = Context.getService(BotswanaEmrService.class).getPatientProgramByProgramAndDate(program,
				    startDate.toDate(), endDate.toDate(), false);
			}
			getPatientProgramEnrollmentByDay(model, patientPrograms);
		}
	}
	
	private void getPatientProgramEnrollmentByDay(FragmentModel model, List<PatientProgram> patientPrograms) {
		for (int i = 1; i <= 7; i++) {
			int finalI = i;
			int size = (int) patientPrograms.stream().filter(p -> new DateTime(p.getDateEnrolled()).getDayOfWeek() == finalI)
			        .count();
			switch (i) {
				case 1:
					model.addAttribute("mon", size);
					break;
				case 2:
					model.addAttribute("tue", size);
					break;
				case 3:
					model.addAttribute("wed", size);
					break;
				case 4:
					model.addAttribute("thur", size);
					break;
				case 5:
					model.addAttribute("fri", size);
					break;
				case 6:
					model.addAttribute("sat", size);
					break;
				case 7:
					model.addAttribute("sun", size);
					break;
			}
			
		}
	}
	
	private Object getPatientsScreenedOnDay(String dateString, EncounterType encounterType, Location location)
	        throws ParseException {
		int value = 0;
		List<Encounter> encountersList = Context.getService(BotswanaEmrService.class).getScreeningsDoneOnDate(
		    getDateFromString(dateString), getDateFromString(dateString), encounterType, null, location);
		if (encountersList != null) {
			encountersList = encountersList.stream().filter(e -> e.getForm() != null).collect(Collectors.toList());
			value = encountersList.size();
		}
		return value;
	}
	
	private Object getPatientsConsultedOnDay(String dateString, Location location) throws ParseException {
		int value = 0;
		List<Encounter> consultationEncountersList = BotswanaEmrUtils.getAllConsultations(getDateFromString(dateString),
		    getDateFromString(dateString), null, location);
		if (consultationEncountersList != null && consultationEncountersList.size() > 0) {
			value = consultationEncountersList.size();
		}
		return value;
	}
	
	private Integer getPatientsSeenOnDay(String dateString, Location location) throws ParseException {
		int value = 0;
		List<Registration> paymentList = Context.getService(BotswanaEmrService.class)
		        .getPatientsRegisteredOnDate(getDateFromString(dateString), getDateFromString(dateString), location, null);
		if (paymentList != null) {
			
			value = paymentList.size();
		}
		return value;
	}
	
	private Integer getPatientsEnrolledOnLocation(String dateString, Location location, Location currentLocation)
	        throws ParseException {
		
		int value = 0;
		List<PatientQueue> patientQueueList = new ArrayList<>();
		patientQueueList = patientQueueingService.getPatientQueueList(null, fromDate(getDateFromString(dateString)),
		    toDate(getDateFromString(dateString)), location, currentLocation, null, null);
		
		return patientQueueList.size();
	}
	
	private List<String> getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		List<String> days = new ArrayList<>();
		
		LocalDate today = new LocalDate();
		LocalDate weekStart = today.dayOfWeek().withMinimumValue();
		for (int i = 0; i < 7; i++) {
			days.add(format.format(weekStart.toDate()));
			weekStart = weekStart.plusDays(1);
		}
		return days;
	}
	
	private Date getDateFromString(String dateString) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
	}
	
	private Date fromDate(Date startDate) {
		return new DateTime(startDate).withTimeAtStartOfDay().toDate();
	}
	
	private Date toDate(Date endDate) {
		return new DateTime(endDate).withTime(23, 59, 59, 999).toDate();
	}
}
