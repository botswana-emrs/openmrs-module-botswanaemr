/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs.tb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.EncounterProvider;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.TbTreatmentService;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.botswanaemr.utilities.NameUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class DotFragmentController {
	
	public void controller(@SpringBean FragmentModel model, UiUtils ui, @RequestParam("patientId") Patient patient,
	        @SpringBean("obsService") ObsService obsService) {
		
		//todo find a better way of handling this.
		model.addAttribute("days", getDays());
		model.addAttribute("mOne", getMonthEvents(getMappedEvents(patient, obsService), 0));
		model.addAttribute("mTwo", getMonthEvents(getMappedEvents(patient, obsService), 1));
		model.addAttribute("mThree", getMonthEvents(getMappedEvents(patient, obsService), 2));
	}
	
	protected List<String> getDays() {
		List<String> days = new ArrayList<>();
		for (int i = 0; i <= 31; i++) {
			days.add(String.valueOf(i));
		}
		return days;
	}
	
	protected Map<Integer, List<Event>> getMappedEvents(@NotNull Patient patient, @NotNull ObsService obsService) {
		Concept administeredByConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.ADMINISTERED_CONCEPT_UUID);
		Concept dateAdministeredConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.DATE_ADMINISTERED_CONCEPT_UUID);
		
		Date enrollmentDate = null;
		Date completionDate = null;
		Optional<PatientProgram> patientProgram = Context
		        .getRegisteredComponent("botswana.emr.tbTreatmentService", TbTreatmentService.class)
		        .getTBEnrollment(patient);
		if (patientProgram.isPresent()) {
			enrollmentDate = patientProgram.get().getDateEnrolled();
			completionDate = patientProgram.get().getDateCompleted();
		}
		
		List<Obs> observations = obsService.getObservations(Collections.singletonList(patient), null,
		    Collections.singletonList(administeredByConcept), null, null, null, null, null, null, enrollmentDate,
		    completionDate, false);
		
		List<Obs> dateObservations = obsService.getObservations(Collections.singletonList(patient), null,
		    Collections.singletonList(dateAdministeredConcept), null, null, null, null, null, null, enrollmentDate,
		    completionDate, false);
		
		List<Event> events = new ArrayList<>();
		//find a better way
		if (enrollmentDate != null) {
			for (Obs obs : dateObservations) {
				Date dateAdminister = obs.getValueDatetime();
				events.add(new Event(getEncounterProviderInitials(obs), DateUtils.getStringDate(dateAdminister, "dd-mm-yyy"),
				        dateAdminister, whichMonth(enrollmentDate, dateAdminister)));
			}
		}
		
		//maintains order of insertion
		//At this point make it work!
		return events.stream().collect(Collectors.groupingBy(Event::getMonth));
	}
	
	protected List<String> getMonthEvents(Map<Integer, List<Event>> mappedEvents, int index) {
		List<String> events = new ArrayList<>();
		if (mappedEvents.size() > index) {
			events.addAll(handleEvents(mappedEvents.get(index)));
		}
		return events;
	}
	
	private List<String> handleEvents(List<Event> events) {
		List<String> monthEvents = new ArrayList<>();
		for (int i = 0; i <= 31; i++) {
			if (events != null) {
				Event event = getEventInMonth(events, i);
				if (event == null) {
					if (i <= DateUtils.getDateOfTheMonth(new Date())) {
						monthEvents.add("X");
					} else {
						monthEvents.add(" ");
					}
				} else {
					monthEvents.add(event.getTitle());
				}
			}
		}
		return monthEvents;
	}
	
	private Event getEventInMonth(List<Event> events, Integer index) {
		for (Event event : events) {
			if (DateUtils.getDateOfTheMonth(event.getDate()) == index) {
				return event;
			}
		}
		return null;
	}
	
	private int whichMonth(Date enroll, Date currentDate) {
		return DateUtils.getMonth(currentDate).getValue() - DateUtils.getMonth(enroll).getValue();
	}
	
	private Date getDateAdministeredBy(List<Obs> dateObs, Obs obs) {
		for (Obs dateObservation : dateObs) {
			//same encounter
			if (dateObservation.getEncounter().getUuid().equals(obs.getEncounter().getUuid())) {
				return dateObservation.getValueDate();
			}
		}
		return null;
	}
	
	private String getEncounterProviderInitials(@NotNull Obs obs) {
		//Find a better way
		List<EncounterProvider> encounterProviderSet = new ArrayList<>(obs.getEncounter().getEncounterProviders());
		String fullName = "";
		if (!encounterProviderSet.isEmpty()) {
			fullName = encounterProviderSet.get(0).getProvider().getPerson().getPersonName().getFullName();
		}
		
		return fullName.isEmpty() ? obs.getValueCoded().getDisplayString() : NameUtils.getInitials(fullName);
	}
	
	private boolean isPhysician(@NotNull Concept concept) {
		return concept.getUuid().equals(BotswanaEmrConstants.PHYSICIAN_CONCEPT_UUID);
	}
	
	@Data
	@AllArgsConstructor
	public static class Event implements Serializable {
		
		private String title;
		
		private String start;
		
		private Date date;
		
		int month;
	}
}
