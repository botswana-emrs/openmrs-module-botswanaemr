/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.util.LocaleUtility;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLabOrderResultsIfCompleteSet;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getLabResultsKeyMap;

public class LabHistoryFragmentController {
	
	public void controller(FragmentModel model, @SpringBean("labService") LabService labService,
	        @RequestParam("patientId") Patient patient, UiUtils ui) {
		model.addAttribute("labTests", getPatientLabTest(patient, ui, labService));
		int labId;
		List<SimplifiedLabTest> labResults = new ArrayList<SimplifiedLabTest>();
		List<LabTest> allPatientLabTests = getAllPatientLabTest(patient, labService);
		if (allPatientLabTests != null && allPatientLabTests.size() > 0) {
			for (LabTest labTest : allPatientLabTests) {
				labId = labTest.getLabTestId();
				labResults.addAll(getLabSummary(labId, labService));
			}
		}
		model.addAttribute("labResults", labResults);
	}
	
	public List<SimplifiedLabTest> getLabSummary(Integer id, @SpringBean("labService") LabService labService) {
		LabTest labTest = labService.getLabTestById(id);
		Encounter encounter = labTest.getEncounter();
		Set<Obs> labTestObsSet = encounter.getAllObs();
		String results = "NA";
		Obs retreatmentObs = null;
		SimplifiedLabTest simplifiedLabTest = null;
		List<SimplifiedLabTest> allLabTest = new ArrayList<SimplifiedLabTest>();
		if (labTestObsSet != null) {
			for (Obs labTestObs : labTestObsSet) {
				if (labTestObs.getConcept() != null && labTestObs.getValueCoded() != null
				        && labTestObs.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.TB_TEST_TYPE))) {
					if (labTestObs.getValueCoded().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.RETREATMENT))) {
						for (Obs obs1 : labTestObsSet) {
							if (obs1.getConcept().equals(BotswanaEmrUtils.getConcept(BotswanaEmrConstants.RETREATMENT))) {
								retreatmentObs = obs1;
							}
						}
					}
					
					if (labTestObs.getValueCoded().equals(labTest.getConcept())) {
						results = labTestObs.getValueCoded().getDisplayString();
						if (retreatmentObs != null) {
							results += ">>Re-treatment>> " + retreatmentObs.getValueCoded().getDisplayString();
						}
						simplifiedLabTest = new SimplifiedLabTest();
						simplifiedLabTest.setResults(results);
						simplifiedLabTest.setTestName(labTestObs.getConcept().getDisplayString());
						simplifiedLabTest.setStatus(labTest.getStatus());
						allLabTest.add(simplifiedLabTest);
					}
				} else {
					if (labTestObs.getConcept() != null && labTestObs.getValueCoded() != null) {
						results = getLabResults(labTest, labTestObs);
						simplifiedLabTest = new SimplifiedLabTest();
						simplifiedLabTest.setResults(results);
						simplifiedLabTest.setTestName(labTestObs.getValueCoded().getDisplayString());
						simplifiedLabTest.setStatus(labTest.getStatus());
						simplifiedLabTest
						        .setResultsList(getLabResultsKeyMap(getLabOrderResultsIfCompleteSet(id, labService)));
						
						allLabTest.add(simplifiedLabTest);
					}
				}
			}
		}
		return allLabTest;
	}
	
	private String getLabResults(LabTest labTest, Obs obs) {
		String results = "NA";
		Encounter labTestEncounterResult = labTest.getEncounterResult();
		if (labTestEncounterResult != null) {
			Set<Obs> testResultsObsSet = labTestEncounterResult.getObs();
			for (Obs testResultsObs : testResultsObsSet) {
				Concept obsConcept = testResultsObs.getConcept();
				if (obsConcept.equals(obs.getValueCoded())) {
					results = testResultsObs.getValueAsString(LocaleUtility.getDefaultLocale());
					break;
				}
			}
		}
		return results;
	}
	
	public List<SimpleObject> getPatientLabTest(@RequestParam("patientId") Patient patient, UiUtils ui,
	        LabService labService) {
		List<LabTest> labTests = labService.getLaboratoryTestsByPatient(patient);
		Collections.sort(labTests, new Comparator<LabTest>() {
			
			@Override
			public int compare(LabTest prevEntry, LabTest newEntry) {
				return newEntry.getAcceptDate().compareTo(prevEntry.getAcceptDate());
			}
		});
		return SimpleObject.fromCollection(labTests, ui, "labTestId", "acceptDate");
		
	}
	
	public List<LabTest> getAllPatientLabTest(@RequestParam("patientId") Patient patient, LabService labService) {
		List<LabTest> labTests = labService.getLaboratoryTestsByPatient(patient);
		Collections.sort(labTests, new Comparator<LabTest>() {
			
			@Override
			public int compare(LabTest prevEntry, LabTest newEntry) {
				return newEntry.getAcceptDate().compareTo(prevEntry.getAcceptDate());
			}
		});
		return labTests;
		
	}
}
