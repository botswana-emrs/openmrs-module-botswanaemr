/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.page.controller.art.ArtScreeningAndTestingPageController.getHivEncounters;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.PersonAttributeType;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.PatientService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.fragment.controller.consultation.plan.prescription.AddPrescriptionFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ArtProgramFragmentController {
	
	public static final String HIV_ADULT_INITIAL_ENCOUNTER_TYPE = "6406d528-cbd8-11ec-9d64-0242ac120002";
	
	private static final String HIV_PAEDS_INITIAL_ENCOUNTER_TYPE = "0077726a-c4a3-11ec-9d64-0242ac12000";
	
	public static final String HIV_ADULT_FOLLOW_UP_FORM_UUID = "3A3173A3-E403-4510-9994-EB7ED9B2126C";

	public static final String HIV_ADULT_COUNSELLING_FORM_UUID = "4310ea10-d084-11ec-9d64-0242ac120002";
	
	public static final String HIV_PAEDS_FOLLOW_UP_FORM_UUID = "767BE697-C7F4-416D-B8F4-9BD163BD95A7";
	
	private Visit currentPatientVisit;
	
	private boolean hasActiveVisit;
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, @SpringBean("visitService") VisitService visitService,
	        @FragmentParam(value = "visit", required = false) Visit visit,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "form", required = false) Form form,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, UiSessionContext sessionContext) {
		
		Form eligibilityForm = formService.getFormByUuid(BotswanaEmrConstants.ELIGIBILITY_AND_INITIATION_FORM_UUID);
		Form artProgramEnrolment = formService.getFormByUuid(BotswanaEmrConstants.ART_PROGRAM_ENROLMENT_FORM_UUID);
		HtmlForm artEligibilityForm = htmlFormEntryService.getHtmlFormByForm(eligibilityForm);
		HtmlForm artProgramEnrolmentForm = htmlFormEntryService.getHtmlFormByForm(artProgramEnrolment);
		
		currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		hasActiveVisit = currentPatientVisit != null;
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		
		Program artProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.ART_PROGRAM_UUID);
		
		EncounterType htsVerificationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_VERIFICATION_ENCOUNTER_TYPE_UUID);
		EncounterType htsDocumentPriorTestEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.HTS_PRIOR_TEST_ENCOUNTER_TYPE_UUID);
		
		List<EncounterType> htsVerificationTestEncounterTypes = new ArrayList<>();
		htsVerificationTestEncounterTypes.add(htsVerificationEncounterType);
		htsVerificationTestEncounterTypes.add(htsDocumentPriorTestEncounterType);
		
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, artProgram, null, null,
		    null, null, false);
		List<PatientProgram> allPatientPrograms = programWorkflowService.getPatientPrograms(patient, null, null, null, null,
		    null, false);
		List<PatientProgram> activeProgramEnrollmentForPatients = new ArrayList<PatientProgram>();
		boolean enrolled = false;
		
		for (PatientProgram patientProgram : patientProgramList) {
			if (patientProgram.getDateCompleted() == null) {
				activeProgramEnrollmentForPatients.add(patientProgram);
			}
		}
		
		enrolled = !activeProgramEnrollmentForPatients.isEmpty();
		
		List<String> preEnrolmentForms = Arrays.asList(BotswanaEmrConstants.LAB_REQUEST_FORM_UUID,
		    BotswanaEmrConstants.LAB_RESULTS_FORM_UUID);
		
		List<String> postEnrolmentForms = Arrays.asList(HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID,
		    HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID);
		
		HtmlForm transferOutForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(ART_TRANSFER_OUT_FORM_UUID));
		
		HtmlForm tptRegisterForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(ART_TPT_REGISTER_FORM_UUID));
		
		HtmlForm ltcOutcomeForm = htmlFormEntryService.getHtmlFormByForm(formService.getFormByUuid(LTC_OUTCOME_FORM_UUID));
		
		HtmlForm vitalsForm = htmlFormEntryService.getHtmlFormByForm(formService.getFormByUuid(VITALS_FORM_UUID));
		HtmlForm tbScreeningForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid("5514e068-00b9-427d-9210-8071fd456808"));
		HtmlForm pepForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.ART_PEP_FORM_UUID));
		HtmlForm prepFollowupForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.ART_PrEP_FOLLOW_UP_FORM_UUID));
		HtmlForm prepInitiationForm = htmlFormEntryService
				.getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.ART_PREP_INITIATION_FORM_UUID));
		HtmlForm pepInitiationForm = htmlFormEntryService
				.getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.ART_PEP_INITIATION_FORM_UUID));
		HtmlForm pepFollowUpForm = htmlFormEntryService
				.getHtmlFormByForm(formService.getFormByUuid(ART_PEP_FOLLOW_UP_FORM_UUID));
		HtmlForm artFollowUpForm = htmlFormEntryService
				.getHtmlFormByForm(formService.getFormByUuid(ART_FOLLOW_UP_FORM_UUID));
		
		List<HtmlForm> hivEncounterForms = getHivEncounterForms(patient, htmlFormEntryService, formService);
		
		List<HtmlForm> newHivEncounterForms = new ArrayList<>(hivEncounterForms);
		newHivEncounterForms.add(tptRegisterForm);
		newHivEncounterForms.add(ltcOutcomeForm);
		newHivEncounterForms.add(vitalsForm);
		newHivEncounterForms.add(tbScreeningForm);
		newHivEncounterForms.add(pepForm);
		newHivEncounterForms.add(prepFollowupForm);
		newHivEncounterForms.add(prepInitiationForm);
		newHivEncounterForms.add(pepInitiationForm);
		newHivEncounterForms.add(pepFollowUpForm);
		newHivEncounterForms.add(artFollowUpForm);
		//TODO: Confirm if this below line breaks logic
		hivEncounterForms.addAll(newHivEncounterForms);
		
		List<Encounter> filledEncounterForPatientForVisit = getHivEncounters(patient, encounterService,
		    Collections.singletonList(currentPatientVisit), hivEncounterForms);
		
		model.addAttribute("encounters", getHivEncounters(patient, encounterService, hivEncounterForms));
		
		Optional<Encounter> transferOutFormEncounter = Optional.empty();
		if (!filledEncounterForPatientForVisit.isEmpty()) {
			transferOutFormEncounter = filledEncounterForPatientForVisit.stream()
			        .filter(encounter -> encounter.getForm().equals(transferOutForm.getForm())).findFirst();
		}
		if (transferOutFormEncounter.isPresent()) {
			model.addAttribute("transferOutFormEncounter", transferOutFormEncounter.get());
		} else {
			model.addAttribute("transferOutFormEncounter", "");
		}
		List<HtmlForm> tmpAvailableForms;
		if (filledEncounterForPatientForVisit.isEmpty()) {
			tmpAvailableForms = hivEncounterForms;
		} else {
			List<Form> hivForms = filledEncounterForPatientForVisit.stream().map(Encounter::getForm)
			        .collect(Collectors.toList());
			tmpAvailableForms = hivEncounterForms.stream().filter(f -> !hivForms.contains(f.getForm()))
			        .collect(Collectors.toList());
		}
		Obs prepEligible = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
				BotswanaEmrConstants.ART_PREP_ELIGIBLE_CONCEPT_UUID, null, null);
				
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("hasVisit", hasActiveVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("programs", programWorkflowService.getAllPrograms());
		model.addAttribute("enrolled", enrolled);
		model.addAttribute("visitTypes", visitService.getAllVisitTypes());
		model.addAttribute("activePrograms", activeProgramEnrollmentForPatients);
		model.addAttribute("hivOutComes", Programs.getArtTreatmentOutcomes());
		model.addAttribute("eligible",
		    Programs.eligibleForProgramEnrollment(patient,
		        getEncounterTypeSearchCriteria(patient, htsVerificationTestEncounterTypes),
		        getLastDiscontinuedPatientProgram(patient, artProgram), artProgram));
		model.addAttribute("onArt", onArt(patient));
		model.addAttribute("artStatus", artStatus(patient));
		model.addAttribute("artEligibilityForm", artEligibilityForm);
		model.addAttribute("artProgramEnrolmentForm", artProgramEnrolmentForm);
		Concept hivStatusConcept = BotswanaEmrUtils.getPatientHivStatus(patient);
		model.addAttribute("hivStatus", hivStatusConcept == null ? "Unknown"
		        : hivStatusConcept.getDisplayString().toLowerCase().contains("positive") ? "+ve" : "-ve");
		
		// TODO: Determine ART Readiness
		
		model.addAttribute("artReady", isPatientArtReady(patient));
		
		model.addAttribute("onCtx", isPatientOnCtx(patient));
		
		model.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "programs/programs", patient, currentPatientVisit, ui)));
		model.addAttribute("stopArtForm", BotswanaEmrConstants.STOP_ART_FORM_UUID);
		model.addAttribute("artProgramId", BotswanaEmrConstants.ART_PROGRAM_UUID);
		model.addAttribute("changeRegimenForm", BotswanaEmrConstants.ART_REGIMEN_CHANGE_FORM_UUID);
		List<Obs> regimenObs = getObservations(patient, getConcept(REGIMEN_CONCEPT_UUID));
		List<Obs> regimenLineObs = getObservations(patient, getConcept(REGIMEN_LINE_CONCEPT_ID));
		//get the latest regimen from the regimenObs and assign to a currentRegimen variable and ensure regimenObs is not empty and not null
		Obs currentRegimen = regimenObs.isEmpty() ? null
		        : regimenObs.stream().max(Comparator.comparing(Obs::getObsDatetime)).get();
		Obs currentRegimenLine = regimenLineObs.isEmpty() ? null
		        : regimenLineObs.stream().max(Comparator.comparing(Obs::getObsDatetime)).get();
		
		model.addAttribute("currentRegimen",
		    currentRegimen == null ? "Regimen not set" : currentRegimen.getValueCoded().getName().getName());
		model.addAttribute("currentRegimenLine",
		    currentRegimenLine == null ? "Regimen line not set" : currentRegimenLine.getValueCoded().getName().getName());
		
		HtmlForm adherenceForm;
		adherenceForm = tmpAvailableForms.stream().filter(f -> f.getForm().getUuid().equals(ADHERENCE_FORM_UUID)).findFirst()
		        .orElse(null);
		model.addAttribute("adherenceForm", adherenceForm);
		
		// Check if patient has initial HIV encounter to determine whether to load initial of followup form
		EncounterSearchCriteriaBuilder encounterSearchCriteria = new EncounterSearchCriteriaBuilder();
		encounterSearchCriteria
		        .setEncounterTypes(Arrays.asList(encounterService.getEncounterTypeByUuid(HIV_ADULT_INITIAL_ENCOUNTER_TYPE),
		            encounterService.getEncounterTypeByUuid(HIV_PAEDS_INITIAL_ENCOUNTER_TYPE)));
		encounterSearchCriteria.setPatient(patient);
		HtmlForm consultationForm;
		if (encounterService.getEncounters(encounterSearchCriteria.createEncounterSearchCriteria()).isEmpty()) {
			consultationForm = tmpAvailableForms.stream()
			        .filter(f -> f.getForm().getUuid().equals(HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID)
			                || f.getForm().getUuid().equals(HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID))
			        .findFirst().orElse(null);
		} else {
			consultationForm = tmpAvailableForms.stream()
			        .filter(f -> f.getForm().getUuid().equals(HIV_ADULT_FOLLOW_UP_FORM_UUID)
			                || f.getForm().getUuid().equals(HIV_PAEDS_FOLLOW_UP_FORM_UUID))
			        .findFirst().orElse(null);
		}
		
		Boolean isPepVisitType = BotswanaEmrUtils.getIsPepVisitType(currentPatientVisit);
		// if ispep visit type, only retain the vitals form and the PEP form
		if (isPepVisitType) {
			tmpAvailableForms = tmpAvailableForms.stream().filter(
			    f -> f.getForm().getUuid().equals(VITALS_FORM_UUID) || f.getForm().getUuid().equals(ART_PEP_FORM_UUID) || f.getForm().getUuid().equals(ART_PEP_INITIATION_FORM_UUID))
			        .collect(Collectors.toList());
		}
		Boolean isPrepVisitType = BotswanaEmrUtils.getIsPrepVisitType(currentPatientVisit);
		// if isprep visit type, only retain vitals form and pre-exposure prophylaxis form
		if (isPrepVisitType) {
			if (currentPatientVisit.getVisitType().getUuid().equals(BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID)) {
				tmpAvailableForms = tmpAvailableForms.stream().filter(
				    f -> f.getForm().getUuid().equals(VITALS_FORM_UUID) || f.getForm().getUuid().equals(ART_PREP_FORM_UUID) || f.getForm().getUuid().equals(ART_PREP_INITIATION_FORM_UUID))
				        .collect(Collectors.toList());
				if( prepEligible == null || prepEligible.getValueCoded().getUuid().equals(NO_CONCEPT_UUID)) {
					tmpAvailableForms.removeIf(f -> f.getForm().getUuid().equals(ART_PREP_INITIATION_FORM_UUID));
				}
			} else {
				tmpAvailableForms = tmpAvailableForms.stream().filter(f -> f.getForm().getUuid().equals(VITALS_FORM_UUID)
				        || f.getForm().getUuid().equals(ART_PrEP_FOLLOW_UP_FORM_UUID)).collect(Collectors.toList());
			}
		}
		model.addAttribute("pepOrPrep", isPrepVisitType || isPepVisitType);
		
		//new tmpAvailableForms to add the tptRegisterForm
		//List<HtmlForm> newTmpAvailableForms = new ArrayList<>(tmpAvailableForms);
		//newTmpAvailableForms.add(tptRegisterForm);
		//newTmpAvailableForms.add(ltcOutcomeForm);
		//tmpAvailableForms = newTmpAvailableForms;
		// Exclude ART consultation form from available forms
		List<HtmlForm> artForms = getArtForms(currentPatientVisit.getVisitType(), tmpAvailableForms);
		//TODO: Confirm logic for post art forms
		model.addAttribute("availablePreArtForms",
				artForms != null ? artForms.stream()
						.filter(f -> f != null
								&& f.getForm() != null
								&& f.getForm().getEncounterType() != null
								&& f.getForm().getEncounterType().getUuid() != null
								&& !f.getForm().getEncounterType().getUuid().equals(HIV_ADULT_INITIAL_ENCOUNTER_TYPE)
								&& !f.getForm().getEncounterType().getUuid().equals(HIV_PAEDS_INITIAL_ENCOUNTER_TYPE)
								&& !f.getForm().getEncounterType().getUuid().equals(HIV_ADULT_FOLLOW_UP_ENCOUNTER_TYPE)
								&& !f.getForm().getEncounterType().getUuid().equals(HIV_PAEDS_FOLLOW_UP_ENCOUNTER_TYPE))
						.distinct()
						.collect(Collectors.toList()) : Collections.emptyList());

		model.addAttribute("consultationForm", consultationForm);
		boolean needsTesting = false;
		boolean needsVerification = false;
		boolean needsSupplementaryTestResults = false;
		Form htsForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID);
		Form htsVerificationForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID);
		Form htsSupplementaryTestResultsForm = formService
		        .getFormByUuid(BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID);
		
		List<Encounter> htsTestingEncounters = getEncounters(patient, encounterService, htsForm);
		if (htsTestingEncounters != null && !htsTestingEncounters.isEmpty()) {
			Encounter encounter = htsTestingEncounters.get(0);
			if (DateUtils.getMonthsInBetween(encounter.getEncounterDatetime(), new Date()) > 3) {
				needsTesting = true;
			} else {
				needsVerification = needsSupplementaryTestResults = true;
				
				List<Encounter> htsVerificationEncounters = getEncounters(patient, encounterService, htsVerificationForm);
				if (htsVerificationEncounters != null && !htsVerificationEncounters.isEmpty()) {
					needsVerification = htsVerificationEncounters.get(0).getEncounterDatetime()
					        .before(encounter.getEncounterDatetime());
				}
				
				List<Encounter> htsSupplementaryTestEncounters = getEncounters(patient, encounterService,
				    htsSupplementaryTestResultsForm);
				if (htsSupplementaryTestEncounters != null && !htsSupplementaryTestEncounters.isEmpty()) {
					needsSupplementaryTestResults = htsSupplementaryTestEncounters.get(0).getEncounterDatetime()
					        .before(encounter.getEncounterDatetime());
				}
			}
		}
		model.addAttribute("needsTesting", needsTesting);
		model.addAttribute("needsVerification", needsVerification);
		model.addAttribute("needsSupplementaryTestResults", needsSupplementaryTestResults);
		model.addAttribute("newTestFormUuid", BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID);
		model.addAttribute("verificationFormUuid", BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID);
		model.addAttribute("supplementaryTestingFormUuid", BotswanaEmrConstants.HTS_SUPPLEMENTARY_TEST_FORM_UUID);
		
		Obs hivStatusObs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), null,
		    BotswanaEmrConstants.HIV_STATUS_RESULT_CONCEPT_UUID, null, null);
		String hivStatus = "Unknown";
		if (hivStatusObs != null) {
			if (hivStatusObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.POSITIVE)) {
				hivStatus = "Positive";
			} else if (hivStatusObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.NEGATIVE)) {
				hivStatus = "Negative "
				        + BotswanaEmrUtils.formatDateWithoutTime(hivStatusObs.getObsDatetime(), "yyyy-MM-dd");
				;
			}
		}
		model.addAttribute("hivStatus", hivStatus);

		List<String> formsWithTbSection = Arrays.asList(
				PEP_INITIATION_FORM_UUID,
				ART_PEP_FOLLOW_UP_FORM_UUID,
				ART_PREP_INITIATION_FORM_UUID,
				ART_PrEP_FOLLOW_UP_FORM_UUID,
				HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID,
				ADULT_FOLLOW_UP_CONSULTATION_FORM_UUID,
				BotswanaEmrConstants.ART_FOLLOW_UP_FORM_UUID,
				HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID,
				PAEDS_FOLLOW_UP_CONSULTATION_FORM_UUID,
				ART_PROGRAM_ENROLMENT_FORM_UUID);

		model.addAttribute("formsWithTbSection", formsWithTbSection);
		
		BotswanaEmrUtils.getArtVisitTypes(model, currentPatientVisit);
		
	}
	
	private List<HtmlForm> getArtForms(VisitType visitType, List<HtmlForm> tempForms) {
		List<String> forms = new ArrayList<>();
		if (visitType.getUuid().equals(PREP_INITIATION_VISIT_TYPE_UUID)) {
			forms.addAll(Arrays.asList(BotswanaEmrConstants.ART_PREP_FORM_UUID, BotswanaEmrConstants.PHYSICAL_EXAM_FORM_UUID,
			    BotswanaEmrConstants.VITALS_FORM_UUID, BotswanaEmrConstants.ART_PREP_INITIATION_FORM_UUID));
		}
		if (visitType.getUuid().equals(PEP_INITIATION_VISIT_TYPE_UUID)) {
			forms.addAll(
			    Arrays.asList(BotswanaEmrConstants.VITALS_FORM_UUID, BotswanaEmrConstants.ART_PEP_INITIATION_FORM_UUID));
		}

		if (visitType.getUuid().equals(PEP_FOLLOW_UP_VISIT_TYPE_UUID)) {
			forms.addAll(
					Arrays.asList(BotswanaEmrConstants.VITALS_FORM_UUID, BotswanaEmrConstants.ART_PEP_FOLLOW_UP_FORM_UUID));
		}
		if (visitType.getUuid().equals(ART_FOLLOW_UP_VISIT_TYPE_UUID)) {
			forms.addAll(
					Arrays.asList(HIV_ADULT_COUNSELLING_FORM_UUID,
							HIV_ADULT_FOLLOW_UP_FORM_UUID,
							BotswanaEmrConstants.VITALS_FORM_UUID,
							BotswanaEmrConstants.ART_TPT_REGISTER_FORM_UUID,
							BotswanaEmrConstants.LTC_OUTCOME_FORM_UUID,
							BotswanaEmrConstants.ART_FOLLOW_UP_FORM_UUID,
							BotswanaEmrConstants.ART_TRANSFER_OUT_FORM_UUID));
		}
		if (visitType.getUuid().equals(ART_INITIATION_VISIT_TYPE_UUID)) {
			forms.addAll(
					Arrays.asList(HIV_ADULT_COUNSELLING_FORM_UUID,
							BotswanaEmrConstants.ART_TRANSFER_OUT_FORM_UUID,
							BotswanaEmrConstants.LTC_OUTCOME_FORM_UUID,
							BotswanaEmrConstants.VITALS_FORM_UUID,
							TB_SCREENING_FORM_UUID,
							ART_TPT_REGISTER_FORM_UUID));
		}
		
		if (!forms.isEmpty()) {
			return tempForms.stream().filter(f -> forms.contains(f.getForm().getUuid())).collect(Collectors.toList());
		}
		return tempForms;
	}
	
	private static List<Encounter> getEncounters(Patient patient, EncounterService encounterService, Form form) {
		EncounterSearchCriteria criteria = new EncounterSearchCriteria(patient, null, null, null, null,
		        Collections.singletonList(form), null, null, null, null, false);
		return encounterService.getEncounters(criteria);
	}
	
	private boolean isPatientArtReady(Patient patient) {
		boolean ready = false;
		EncounterType artAdherenceAssesmentEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ART_ADHERENCE_ASSESMENT_ENCOUNTER_TYPE_UUID);
		EncounterType artPaedsAdherenceAssesmentEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ART_PAEDS_ADHERENCE_ASSESMENT_ENCOUNTER_TYPE_UUID);
		
		EncounterType artinitialConsultationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ADULT_INITIAL_CONSULTATION_ENCOUNTER_TYPE_UUID);
		
		List<EncounterType> encs = new ArrayList<>();
		encs.add(artAdherenceAssesmentEncounterType);
		encs.add(artinitialConsultationEncounterType);
		encs.add(artPaedsAdherenceAssesmentEncounterType);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, null, null, null, null,
		        encs, null, null, null, false);
		List<Encounter> artReadyEncounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		if (artReadyEncounters.size() > 0) {
			ready = true;
		}
		return ready;
	}
	
	private String artStatus(Patient patient) {
		PersonAttributeType currentRegimen;
		currentRegimen = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.ART_STATUS_ATTRIBUTE_TYPE_UUID);
		return currentRegimen != null && patient.getAttribute(currentRegimen) != null
		        ? patient.getAttribute(currentRegimen).getValue()
		        : "";
	}
	
	// Check it patient is on CTX
	private boolean isPatientOnCtx(Patient patient) {
		Drug ctxDrug = Context.getConceptService().getDrugByUuid(BotswanaEmrConstants.COTRIMOXAZOLE_DRUG_UUID);
		Concept ctxConcept = ctxDrug.getConcept();
		List<Order> ctxOrders = Context.getOrderService().getOrderHistoryByConcept(patient, ctxConcept);
		//TODO: Filter out all orders other than drug orders
		return ctxOrders.size() > 0;
	}
	
	private List<HtmlForm> completedHtmlForms(Patient patient, @SpringBean("visitService") VisitService visitService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService) {
		List<HtmlForm> completedHtmlForms = new ArrayList<>();
		for (Form form : completedForms(patient, visitService)) {
			completedHtmlForms.add(htmlFormEntryService.getHtmlFormByForm(form));
		}
		return completedHtmlForms;
	}
	
	private List<Form> completedForms(Patient patient, @SpringBean("visitService") VisitService visitService) {
		List<Form> completedForms = new ArrayList<Form>();
		
		if (currentPatientVisit != null) {
			List<Encounter> encounterList = currentPatientVisit.getNonVoidedEncounters();
			for (Encounter encounter : encounterList) {
				completedForms.add(encounter.getForm());
			}
		}
		return completedForms;
	}
}
