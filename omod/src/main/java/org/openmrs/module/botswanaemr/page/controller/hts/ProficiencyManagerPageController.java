/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.hts;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedProficiencyTesting;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ProficiencyManagerPageController {
	
	private final HtsProficiencyTestingService htsProficiencyTestingService = Context
	        .getService(HtsProficiencyTestingService.class);
	
	public void controller(@SpringBean PageModel pageModel, UiUtils ui,
	        @RequestParam(value = "proficiencyTestingId") Integer proficiencyTestingId) {
		HtsProficiencyTesting htsProficiencyTesting = htsProficiencyTestingService
		        .getHtsProficiencyTesting(proficiencyTestingId);
		if (htsProficiencyTesting != null) {
			pageModel.addAttribute("htsProficiencyTesting",
			    SimpleObject.fromObject(BotswanaEmrUtils.getSimplifiedProficiencyTesting(htsProficiencyTesting), ui,
			        "proficiencyTestingId", "panelId", "receivedBy", "datePanelReceived", "testingPoint", "resultsDueDate"));
		}
	}
}
