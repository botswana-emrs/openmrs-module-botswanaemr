/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.vmmc;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.*;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDurationSince;

public class VmmcDashboardPageController {
	
	public void get(PageModel model, UiSessionContext sessionContext) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date yesterday = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);
		
		//Check for possibility of null pointer to some results
		int allVmmcCircumcisionsCount = 0;
		int allWeeklyVmmcCircumcisionsCount = 0;
		int yesterdayVmmcCircumcisionsCount = 0;
		
		Location location = null;
		if (sessionContext.getSessionLocation() != null) {
			location = sessionContext.getSessionLocation();
		}
		List<Encounter> allVmmcCircumcisionsList = getAllVmmcCircumcisions(null, null, null, location);
		if (allVmmcCircumcisionsList != null && allVmmcCircumcisionsList.size() > 0) {
			allVmmcCircumcisionsCount = allVmmcCircumcisionsList.size();
		}
		
		List<Encounter> allWeeklyVmmcCircumcisionsList = getAllWeeklyVmmcCircumcisions(location);
		if (allWeeklyVmmcCircumcisionsList != null && allWeeklyVmmcCircumcisionsList.size() > 0) {
			allWeeklyVmmcCircumcisionsCount = allWeeklyVmmcCircumcisionsList.size();
		}
		
		List<Encounter> yesterdayVmmcCircumcisionsList = getAllVmmcCircumcisions(yesterday, yesterday, null, location);
		if (yesterdayVmmcCircumcisionsList != null && yesterdayVmmcCircumcisionsList.size() > 0) {
			yesterdayVmmcCircumcisionsCount = yesterdayVmmcCircumcisionsList.size();
		}
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		model.addAttribute("allVmmcCircumcisionsCount", allVmmcCircumcisionsCount);
		model.addAttribute("allWeeklyVmmcCircumcisionsCount", allWeeklyVmmcCircumcisionsCount);
		model.addAttribute("yesterdayVmmcCircumcisionsCount", yesterdayVmmcCircumcisionsCount);
		model.addAttribute("dailyAverageVmmcCircumcisions", getDailyAverageVmmcCircumcisions(location));
		model.addAttribute("todayVmmcActivitiesList",
		    patientObjects(getAllVmmcActivities(null, null, limitFetch, location)));
		model.addAttribute("todayVmmcActivitiesList",
		    patientObjects(getAllVmmcActivities(null, null, limitFetch, location)));
	}
	
	private List<Encounter> getAllVmmcCircumcisions(Date start, Date end, Integer limit, Location location) {
		EncounterType vmmcOperationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE);
		List<EncounterType> vmmcEncounterType = new ArrayList<>();
		vmmcEncounterType.add(vmmcOperationEncounterType);
		List<Encounter> vmmcEncounters = Context.getService(BotswanaEmrService.class).getEncountersList(start, end,
		    vmmcEncounterType, limit, location);
		if (vmmcEncounters != null && !vmmcEncounters.isEmpty()) {
			vmmcEncounters = vmmcEncounters.stream().filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(location.getUuid()))
			        .collect(Collectors.toList());
		}
		return vmmcEncounters;
	}
	
	private List<Encounter> getAllWeeklyVmmcCircumcisions(Location location) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date lastSevenDays = DateUtil.adjustDate(today, -7, DurationUnit.DAYS);
		return getAllVmmcCircumcisions(lastSevenDays, today, null, location);
	}
	
	private Integer getDailyAverageVmmcCircumcisions(Location location) {
		int totalWeeklyVmmcCircumcisions = 0;
		final double WEEK_DAYS = 7;
		
		List<Encounter> allWeeklyVmmcConsultations = getAllWeeklyVmmcCircumcisions(location);
		
		if (allWeeklyVmmcConsultations != null && allWeeklyVmmcConsultations.size() > 0) {
			totalWeeklyVmmcCircumcisions = allWeeklyVmmcConsultations.size();
		}
		
		return (int) Math.ceil(totalWeeklyVmmcCircumcisions / WEEK_DAYS);
	}
	
	private List<GeneralPatientObject> patientObjects(List<Encounter> encounterList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (encounterList != null) {
			for (Encounter encounter : encounterList) {
				generalPatientObject = new GeneralPatientObject();
				Patient patient = encounter.getPatient();
				generalPatientObject.setName(formatPersonName(patient.getPersonName()));
				generalPatientObject.setGender(formatGender(patient));
				generalPatientObject.setCreator(formatPersonCreator(
				    new ArrayList<>(encounter.getEncounterProviders()).get(0).getProvider().getPerson()));
				generalPatientObject.setDuration(getDurationSince(encounter.getDateCreated()));
				generalPatientObject.setAssociatedEncounter(encounter.getEncounterType().getName());
				
				allItems.add(generalPatientObject);
			}
		}
		return allItems;
	}
	
	private List<Encounter> getAllVmmcActivities(Date start, Date end, Integer limit, Location location) {
		EncounterType vmmcOperationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE);
		EncounterType vmmcPostOperationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE);
		EncounterType vmmcConsentEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.VMMC_CONSENT_ENCOUNTER_TYPE);
		List<EncounterType> vmmcEncounterType = new ArrayList<>();
		vmmcEncounterType.add(vmmcPostOperationEncounterType);
		vmmcEncounterType.add(vmmcConsentEncounterType);
		vmmcEncounterType.add(vmmcOperationEncounterType);
		List<Encounter> vmmcEncounters = Context.getService(BotswanaEmrService.class).getEncountersList(start, end,
		    vmmcEncounterType, limit, location);
		if (vmmcEncounters != null && !vmmcEncounters.isEmpty()) {
			vmmcEncounters = vmmcEncounters.stream().filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(location.getUuid()))
			        .collect(Collectors.toList());
		}
		return vmmcEncounters;
	}
}
