/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.ui.framework.annotation.BindParams;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.validator.PatientValidator;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_EMAIL_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_FULLNAME_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_ID_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID;

public class NextOfKinInformationFragmentController {
	
	protected final Log log = LogFactory.getLog(NextOfKinInformationFragmentController.class);
	
	public void get(FragmentModel fragmentModel) {
	}
	
	public String post(@RequestParam(value = "patientId", required = false) @BindParams Patient patient,
	        @RequestParam(value = "personAttributeId", required = false) String personAttributeId,
	        @RequestParam(value = "nokIdType") String nokIdType, @RequestParam(value = "nokIdNumber") String nokIdNumber,
	        @RequestParam(value = "nokFullName") String nokFullName,
	        @RequestParam(value = "nokRelationship") String nokRelationship,
	        @RequestParam(value = "nokPhoneNumber") String nokPhoneNumber,
	        @RequestParam(value = "nokEmail", required = false) String nokEmail,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("patientValidator") PatientValidator patientValidator) {
		
		Set<PersonAttribute> allAttributes = patient.getAttributes();
		Set<PersonAttribute> nextOfKinAttributes = allAttributes.stream()
		        .filter(a -> a.getAttributeType().getName().contains("NOK")).collect(Collectors.toSet());
		
		if (nextOfKinAttributes.isEmpty()) {
			createPersonAttribute(patient, nokIdType, NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nokIdNumber, NOK_ID_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nokFullName, NOK_FULLNAME_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nokRelationship, NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nokPhoneNumber, NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
			createPersonAttribute(patient, nokEmail, NOK_EMAIL_ATTRIBUTE_TYPE_UUID);
		} else {
			if (StringUtils.isNotBlank(personAttributeId)) {
				for (PersonAttribute nextOfKinAttribute : nextOfKinAttributes) {
					updateNextOfKinAttribute(nextOfKinAttribute, nokIdType, NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID);
					updateNextOfKinAttribute(nextOfKinAttribute, nokIdNumber, NOK_ID_ATTRIBUTE_TYPE_UUID);
					updateNextOfKinAttribute(nextOfKinAttribute, nokFullName, NOK_FULLNAME_ATTRIBUTE_TYPE_UUID);
					updateNextOfKinAttribute(nextOfKinAttribute, nokRelationship, NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID);
					updateNextOfKinAttribute(nextOfKinAttribute, nokPhoneNumber, NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID);
					updateNextOfKinAttribute(nextOfKinAttribute, nokEmail, NOK_EMAIL_ATTRIBUTE_TYPE_UUID);
				}
			}
			patient.setAttributes(nextOfKinAttributes);
		}
		
		try {
			patientService.savePatient(patient);
			return null;
		}
		catch (Exception e) {
			log.warn("Error occurred while saving patient's details", e);
		}
		return null;
	}
	
	//Create new Attribute
	private void createPersonAttribute(Patient patient, String attributeValue, String attributeUuid) {
		PersonAttribute attribute = setPersonAttributeValue(attributeValue, attributeUuid);
		patient.addAttribute(attribute);
	}
	
	// Updates an already existing next of kin attribute
	private void updateNextOfKinAttribute(PersonAttribute nextOfKinAttribute, String attributeValue, String attributeUuid) {
		if (getPersonAttributeTypeUuid(nextOfKinAttribute).equals(attributeUuid)) {
			nextOfKinAttribute.setValue(attributeValue);
		}
	}
	
	//Returns a PersonAttributeType uuid given a personAttribute
	private String getPersonAttributeTypeUuid(PersonAttribute personAttribute) {
		return personAttribute.getAttributeType().getUuid();
	}
	
	// Only used to set new person Attributes when the
	// Attributes <Set> is Empty: Emergency patient UseCase
	private PersonAttribute setPersonAttributeValue(String personAttributeValue, String personAttributeTypeUuid) {
		PersonAttributeType personAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(personAttributeTypeUuid);
		PersonAttribute personAttribute = new PersonAttribute();
		personAttribute.setAttributeType(personAttributeType);
		personAttribute.setValue(personAttributeValue);
		return personAttribute;
	}
}
