/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.htmlformentry;

import org.openmrs.Encounter;
import org.openmrs.TestOrder;
import org.openmrs.api.APIException;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabOrderService;
import org.openmrs.module.htmlformentry.CustomFormSubmissionAction;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provide flexibility for creation of lab orders for patients after submission of the Lab request
 * form, all the requested tests, must be represented as lab orders
 */
public class BotswanaEmrLabOrderAction implements CustomFormSubmissionAction {
	
	Logger logger = LoggerFactory.getLogger(BotswanaEmrLabOrderAction.class);
	
	@Override
	public void applyAction(FormEntrySession formEntrySession) {
		FormEntryContext.Mode mode = formEntrySession.getContext().getMode();
		if (!(mode.equals(FormEntryContext.Mode.ENTER) || mode.equals(FormEntryContext.Mode.EDIT))) {
			return;
		}
		
		EncounterService encounterService = Context.getEncounterService();
		Encounter entrySessionEncounter = formEntrySession.getEncounter();
		if (entrySessionEncounter.getEncounterType()
		        .equals(encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE))
		        && entrySessionEncounter.getOrders().isEmpty()) {
			
			try {
				LabOrderService labOrderService = Context.getRegisteredComponent("botswana.emr.labOrderService",
				    LabOrderService.class);
				//add order here
				TestOrder order = labOrderService.createOrder(entrySessionEncounter);
				//add the lab test order
				labOrderService.createLabTest(order);
				
			}
			catch (APIException e) {
				logger.debug(e.getMessage());
				throw new APIException(e);
			}
		}
	}
}
