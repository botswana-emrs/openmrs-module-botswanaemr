/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class RegisterPatientPageController {
	
	public void get(@RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "action", required = false) String action, PageModel model,
	        @SpringBean("locationService") LocationService locationService, UiSessionContext uiSessionContext) {
		String currentServiceDeliveryPointUuid = String
		        .valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location currentServiceDeliveryPoint = locationService.getLocationByUuid(currentServiceDeliveryPointUuid);
		Location sessionLocation = uiSessionContext.getSessionLocation();
		model.addAttribute("location", sessionLocation);
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("currentServiceDeliveryPoint", currentServiceDeliveryPoint);
		model.addAttribute("isDirectServiceDelivery", action.equals("quick"));
	}
}
