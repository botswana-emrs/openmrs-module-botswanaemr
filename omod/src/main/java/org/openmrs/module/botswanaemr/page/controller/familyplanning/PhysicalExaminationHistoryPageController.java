/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.Model;
import org.openmrs.ui.framework.UiUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_PHYSICAL_EXAM_HISTORY_ENCOUNTER_TYPE_UUID;

public class PhysicalExaminationHistoryPageController {
	
	public void controller(UiUtils ui, Model model, UiSessionContext sessionContext,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(required = false, value = "encounterId") Encounter encounter,
	        @RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl) {
		
		final String definitionUiResource = "botswanaemr:htmlforms/srh-physical-exam-history-form.xml";
		
		boolean hasEncounter = false;
		
		EncounterType physicalExamHistoryEncounterType = BotswanaEmrUtils
		        .getEncounterType(SRH_PHYSICAL_EXAM_HISTORY_ENCOUNTER_TYPE_UUID);
		if (encounter != null) {
			hasEncounter = true;
			model.addAttribute("encounter", encounter);
		} else {
			model.addAttribute("encounter", "");
		}
		model.addAttribute("hasEncounter", hasEncounter);
		
		model.addAttribute("definitionUiResource", definitionUiResource);
		model.addAttribute("formUuid", BotswanaEmrConstants.SRH_PHYSICAL_EXAM_HISTORY_FORM_UUID);
		model.addAttribute("modalTitle", BotswanaEmrConstants.SRH_PHYSICAL_EXAM_HISTORY_FORM_MODAL_TITLE);
		model.addAttribute("currentPatientVisit", visit);
		model.addAttribute("defaultEncounterDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
		model.addAttribute("currentPatient", patient);
		List<Encounter> physicalExamHistoryEncounters = BotswanaEmrUtils.getEncountersByPatient(patient,
		    physicalExamHistoryEncounterType, null, null, null);
		model.addAttribute("physicalExamHistoryEncounters", physicalExamHistoryEncounters);
		model.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "familyplanning/familyPlanningCard", patient, visit, ui)));
	}
}
