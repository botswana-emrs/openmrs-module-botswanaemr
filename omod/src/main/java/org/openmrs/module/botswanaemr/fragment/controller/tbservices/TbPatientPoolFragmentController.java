/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.tbservices;

import org.joda.time.DateTime;
import org.openmrs.Location;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.SimplifiedPatientProgram;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TbPatientPoolFragmentController {
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		
		fragmentModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		fragmentModel.addAttribute("providerList", providerList);
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location location = Context.getLocationService().getLocationByUuid(BotswanaEmrConstants.TB_SERVICES_PORTAL_UUID);
		if (location != null) {
			fragmentModel.addAttribute("tbServicePoint", location.getUuid());
		}
	}
	
	public List<SimpleObject> getDotPatientsInProgram(UiSessionContext uiSessionContext, UiUtils uiUtils) {
		List<PatientProgram> patientPrograms = new ArrayList<>();
		
		DateTime startDate = new DateTime(new Date());
		DateTime endDate = new DateTime(new Date()).minusDays(14);
		
		Program program = Context.getService(ProgramWorkflowService.class)
		        .getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		if (program != null) {
			patientPrograms = Context.getService(BotswanaEmrService.class).getPatientProgramByProgramAndDate(program,
			    startDate.toDate(), endDate.toDate(), true);
		}
		
		List<SimplifiedPatientProgram> simplifiedPrograms = getSimplifiedPatients(patientPrograms);
		
		return SimpleObject.fromCollection(simplifiedPrograms, uiUtils, "identifier", "name", "gender", "dateOfBirth", "age",
		    "registeredDate", "status", "registeredBy", "patientId", "visitNumber");
	}
	
	public List<SimpleObject> getActivePatientsInProgram(UiSessionContext uiSessionContext, UiUtils uiUtils) {
		List<PatientProgram> patientPrograms = new ArrayList<>();
		Program program = Context.getService(ProgramWorkflowService.class)
		        .getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		if (program != null) {
			patientPrograms = Context.getService(BotswanaEmrService.class).getActivePatientProgramsByProgram(program, false);
		}
		
		List<SimplifiedPatientProgram> simplifiedPrograms = getSimplifiedPatients(patientPrograms);
		
		return SimpleObject.fromCollection(simplifiedPrograms, uiUtils, "identifier", "name", "gender", "dateOfBirth", "age",
		    "registeredDate", "status", "registeredBy", "patientId", "visitNumber");
	}
	
	protected List<SimplifiedPatientProgram> getSimplifiedPatients(List<PatientProgram> patientPrograms) {
		List<SimplifiedPatientProgram> allPatients = new ArrayList<SimplifiedPatientProgram>();
		SimplifiedPatientProgram simplifiedProgram = null;
		
		Set<Patient> patientList = new HashSet<>();
		
		if (patientPrograms != null) {
			for (PatientProgram patientProgram : patientPrograms) {
				Patient patient = patientProgram.getPatient();
				if (!patientList.contains(patient)) {
					simplifiedProgram = new SimplifiedPatientProgram();
					simplifiedProgram.setIdentifier(
					    patient.getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier()); // BotswanaEmrUtils.formatPatientIdentifier(patient));
					simplifiedProgram.setPatientId(patient.getPatientId());
					simplifiedProgram.setName(BotswanaEmrUtils.formatPersonName(patient.getPersonName()));
					simplifiedProgram.setGender(patient.getPerson().getGender());
					simplifiedProgram.setAge(patient.getAge() == null ? "" : patient.getAge().toString());
					simplifiedProgram
					        .setRegisteredBy(BotswanaEmrUtils.formatPersonCreator(patientProgram.getCreator().getPerson()));
					simplifiedProgram.setRegisteredDate(
					    BotswanaEmrUtils.formatDateWithoutTime(patient.getDateCreated(), "dd-MM-yyyy"));
					simplifiedProgram.setDateOfBirth(patient.getBirthdate() == null ? ""
					        : BotswanaEmrUtils.formatDateWithoutTime(patient.getBirthdate(), "dd-MM-yyyy"));
					
					if (patient.getDead()) {
						simplifiedProgram.setStatus("Expired");
					} else {
						simplifiedProgram.setStatus("Active");
					}
					allPatients.add(simplifiedProgram);
					patientList.add(patient);
				}
			}
		}
		return allPatients;
	}
}
