/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

public class PatientComplaintFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit) {
		fragmentModel.addAttribute("patient", patient);
		fragmentModel.addAttribute("visit", visit);
		
	}
	
	public void addPatientComplaints(@RequestParam("patientId") Patient patient, @RequestParam("complaint") String complaint,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext) {
		EncounterType encounterType = BotswanaEmrUtils
		        .getEncounterType(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		Encounter encounter = BotswanaEmrUtils.createEncounter(patient, encounterType, sessionContext.getSessionLocation(),
		    visit);
		
		ConceptService cs = Context.getConceptService();
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_COMPLAINT_GROUPING_CONCEPT_UUID);
		Concept compolaintConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_COMPLAINT_CONCEPT_UUID);
		
		if (StringUtils.isNotBlank(complaint)) {
			Obs groupingObs = BotswanaEmrUtils.creatObs(encounter, groupingConcept);
			Obs complaintObs = BotswanaEmrUtils.creatObs(encounter, compolaintConcept);
			complaintObs.setValueText(complaint);
			
			groupingObs.addGroupMember(complaintObs);
			
			//add this observation to the encounter
			encounter.addObs(groupingObs);
			Context.getEncounterService().saveEncounter(encounter);
		}
	}
	
	public void editPatientComplaint(@RequestParam("lastComplaintObsUuid") String lastComplaintObsUuid,
	        @RequestParam("editComplaint") String editComplaint) {
		ObsService obsService = Context.getObsService();
		if (StringUtils.isNotEmpty(lastComplaintObsUuid) && StringUtils.isNotBlank(editComplaint)) {
			Obs obs = obsService.getObsByUuid(lastComplaintObsUuid);
			obs.setValueText(editComplaint);
			
			//commit the edited obs in the database
			obsService.saveObs(obs, "Edited patient complaint");
			
		}
	}
}
