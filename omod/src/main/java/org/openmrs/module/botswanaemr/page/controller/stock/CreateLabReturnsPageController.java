/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import com.google.common.collect.Iterators;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedLabReturns;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.*;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;

public class CreateLabReturnsPageController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void get(@RequestParam(value = "stockOperationId", required = false) Integer stockOperationId, PageModel model,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) {
		if (uiSessionContext.getCurrentUser() != null && stockOperationId != null) {
			Set<SimplifiedLabReturns> simplifiedLabReturnsList = new HashSet<>();
			StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
			        .getById(stockOperationId);
			
			if (stockOperation != null) {
				for (StockOperationItem operationItem : stockOperation.getItems()) {
					ItemStock itemStock = stockroomDataService.getItem(stockOperation.getSource(), operationItem.getItem());
					if (itemStock == null) {
						continue;
					}
					SimplifiedLabReturns labReturns = new SimplifiedLabReturns();
					labReturns.setId(stockOperationId);
					labReturns.setItemCode(
					    itemStock.getItem().getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
					labReturns.setItemName(itemStock.getItem().getName());
					labReturns.setExpiryDate(
					    BotswanaEmrUtils.formatDateWithoutTime(operationItem.getExpiration(), "yyyy-MM-dd"));
					labReturns.setTodayDate(BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
					labReturns.setItemStockUuid(itemStock.getUuid());
					labReturns.setItemStockQuantity(operationItem.getQuantity());
					labReturns.setReason(operationItem.getBatchOperation().getDescription());
					labReturns.setComments(""); //TODO Populate comments from the DB
					
					labReturns.setBatchNumber(""); //Set default value
					// TODO: Explore use of the `itemStockDetail.CalculatedBatch` attribute to determine which batches are
					// associated with stock takes and therefore filter them out.
					if (itemStock.hasDetails()) {
						List<ItemStockDetail> itemStockDetailList = itemStock.getDetails().stream()
						        .filter(
						            i -> i.getBatchOperation() != null && i.getBatchOperation().getOperationNumber() != null
						                    && !i.getBatchOperation().getOperationNumber().contains("STOCK_TAKE"))
						        .collect(Collectors.toList());
						if (!itemStockDetailList.isEmpty()) {
							labReturns.setBatchNumber(
							    Iterators.getLast(itemStockDetailList.iterator()).getBatchOperation().getOperationNumber());
						}
					}
					simplifiedLabReturnsList.add(labReturns);
				}
			}
			
			model.addAttribute("simplifiedLabReturnsList", simplifiedLabReturnsList);
			model.addAttribute("selectedStockRoomId", stockOperation.getSource().getId());
			model.addAttribute("stockOperationStatus", stockOperation.getStatus().toString());
		} else {
			model.addAttribute("simplifiedLabReturnsList", "");
		}
	}
}
