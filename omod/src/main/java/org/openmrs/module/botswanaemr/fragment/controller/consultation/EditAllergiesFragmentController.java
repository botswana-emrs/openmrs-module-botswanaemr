/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.*;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;
import org.openmrs.ui.framework.session.Session;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EditAllergiesFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient, UiUtils ui,
	        @FragmentParam(value = "allergies", required = true) Allergies allergies) {
		
		model.addAttribute("allergies", allergies);
		Set<Concept> allergiesConcepts = new HashSet<>();
		for (String conceptSet : BotswanaEmrConstants.ALLERGIES_CONCEPT_SETS) {
			Concept concept = Context.getConceptService().getConceptByUuid(conceptSet);
			allergiesConcepts.addAll(concept.getSetMembers());
		}
		model.addAttribute("allergens", allergiesConcepts);
	}
	
	/**
	 * Searches for allergies
	 * 
	 * @param searchTerm the search text
	 * @param ui uiUtils
	 * @return A SimpleObject list of concepts
	 */
	public List<SimpleObject> searchAllergies(@RequestParam(value = "searchTerm") String searchTerm, UiUtils ui) {
		Set<Concept> allergiesConcepts = new HashSet<>();
		for (String conceptSet : BotswanaEmrConstants.ALLERGIES_CONCEPT_SETS) {
			Concept concept = Context.getConceptService().getConceptByUuid(conceptSet);
			allergiesConcepts.addAll(concept.getSetMembers());
		}
		List<Concept> concepts = new ArrayList<>();
		for (Concept concept : allergiesConcepts) {
			if (StringUtils.containsIgnoreCase(concept.getName().getName(), searchTerm)) {
				concepts.add(concept);
			}
		}
		return SimpleObject.fromCollection(concepts, ui, "id", "name", "uuid");
	}
	
	/**
	 * Fragment Action for updating and saving new patient allergies
	 */
	public void updateAllergies(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data) throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		PatientService ps = Context.getPatientService();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<String> deletedAllergies = mapper.readValue(arrayNode, new TypeReference<List<String>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedAllergies = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newAllergies = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		for (String uuid : deletedAllergies) {
			Allergy allergy = Context.getPatientService().getAllergyByUuid(uuid);
			if (allergy != null) {
				allergy.setVoided(true);
				ps.saveAllergy(allergy);
			}
		}
		
		for (SimpleObject simpleObject : editedAllergies) {
			Allergy allergy = ps.getAllergyByUuid((String) simpleObject.get("uuid"));
			if (allergy != null) {
				String newConceptUuid = (String) simpleObject.get("allergen");
				if (allergy.getAllergen().getCodedAllergen() != null
				        && allergy.getAllergen().getCodedAllergen().getUuid().equals(newConceptUuid)) {
					continue;
				} else if (StringUtils.isNotEmpty(allergy.getAllergen().getNonCodedAllergen())
				        && allergy.getAllergen().getNonCodedAllergen().equals(newConceptUuid)) {
					continue;
				}
				Concept concept = Context.getConceptService().getConceptByUuid(newConceptUuid);
				if (concept != null) {
					allergy.getAllergen().setCodedAllergen(concept);
				} else {
					Concept otherCodedConcept = Context.getConceptService()
					        .getConceptByUuid(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID);
					if (otherCodedConcept != null) {
						allergy.getAllergen().setNonCodedAllergen((String) simpleObject.get("allergen"));
						allergy.getAllergen().setCodedAllergen(otherCodedConcept);
					}
				}
				allergy.setComments((String) simpleObject.get("comment"));
				ps.saveAllergy(allergy);
			} else {
				// Save new
				newAllergies.add(simpleObject);
			}
		}
		
		for (SimpleObject simpleObject : newAllergies) {
			saveNewAllergy(simpleObject, patient);
		}
	}
	
	private void saveNewAllergy(SimpleObject allergyObject, Patient patient) {
		Allergy allergy = new Allergy();
		allergy.setPatient(patient);
		
		Allergen allergen = new Allergen();
		Concept concept = Context.getConceptService().getConceptByUuid((String) allergyObject.get("allergen"));
		if (concept != null) {
			allergen = new Allergen(AllergenType.OTHER, concept, "");
			allergen.setCodedAllergen(concept);
		} else {
			Concept otherCodedConcept = Context.getConceptService()
			        .getConceptByUuid(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID);
			if (otherCodedConcept != null) {
				allergen = new Allergen(AllergenType.OTHER, otherCodedConcept, (String) allergyObject.get("allergen"));
			}
		}
		
		// TODO : Set correct allergen type
		//        allergen.setAllergenType(AllergenType.OTHER);
		allergy.setAllergen(allergen);
		allergy.setComment((String) allergyObject.get("comment"));
		if (Context.getPatientService().getAllergies(patient).containsAllergen(allergy)) {
			return;
		}
		
		Context.getPatientService().saveAllergy(allergy);
	}
	
	/**
	 * Searches for concepts
	 * 
	 * @param searchTerm the search text
	 * @param ui uiUtils
	 * @return A SimpleObject list of concepts
	 */
	public List<SimpleObject> searchSymptoms(@RequestParam(value = "searchTerm") String searchTerm, UiUtils ui) {
		List<Concept> concepts = Context.getService(BotswanaEmrService.class).searchConcept(searchTerm, null);
		return SimpleObject.fromCollection(concepts, ui, "id", "name", "uuid");
	}
}
