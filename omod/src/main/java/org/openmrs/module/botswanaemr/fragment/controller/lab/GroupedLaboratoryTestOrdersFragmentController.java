/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.lab;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.api.EncounterService;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabOrder;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GroupedLaboratoryTestOrdersFragmentController {
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	private final LocationService locationService = Context.getService(LocationService.class);
	
	private static final LabService labService = Context.getService(LabService.class);
	
	protected Log log = LogFactory.getLog(GroupedLaboratoryTestOrdersFragmentController.class);
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location location = Context.getLocationService().getLocationByUuid(
		    String.valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT)));
		if (location != null) {
			fragmentModel.addAttribute("currentServiceDeliveryPoint", location.getUuid());
		}
	}
	
	public List<SimpleObject> getLaboratoryTestOrders(UiUtils ui, UiSessionContext uiSessionContext) {
		String facility = Context.getAuthenticatedUser()
		        .getUserProperty(BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY);
		Location location = locationService.getLocation(facility);
		EncounterType labEncounterType = encounterService
		        .getEncounterTypeByUuid(BotswanaEmrConstants.LAB_REQUEST_ENCOUNTER_TYPE);
		List<Encounter> encounters = Context.getService(BotswanaEmrService.class)
		        .getEncountersList(new Date(), new Date(), labEncounterType, null, location).stream()
		        .filter(encounter -> !encounter.getOrders().isEmpty()).collect(Collectors.toList());
		List<SimplifiedLabOrder> orders = getSimplifiedLabOrders(encounters);
		
		return SimpleObject.fromCollection(orders, ui, "pin", "patientNames", "status", "requestedBy", "patient",
		    "encounterId");
	}
	
	public static List<SimplifiedLabOrder> getSimplifiedLabOrders(List<Encounter> labEncounters) {
		List<SimplifiedLabOrder> laboratoryOrders = new ArrayList<>();
		SimplifiedLabOrder simplifiedLabOrder;
		if (!labEncounters.isEmpty()) {
			for (Encounter encounter : labEncounters) {
				simplifiedLabOrder = new SimplifiedLabOrder();
				simplifiedLabOrder.setPin(BotswanaEmrUtils.getOpenMrsId(encounter.getPatient()));
				simplifiedLabOrder
				        .setPatientNames(BotswanaEmrUtils.formatPersonName(encounter.getPatient().getPersonName()));
				List<LabTest> labTestOrders = labService.getLaboratoryTestListByEncounter(encounter);
				if (labTestOrders != null) {
					SimplifiedLabOrder finalSimplifiedLabOrder = simplifiedLabOrder;
					labTestOrders.forEach(order -> {
						if (order != null) {
							finalSimplifiedLabOrder.setStatus(order.getStatus());
						}
					});
				}
				simplifiedLabOrder.setRequestedBy(BotswanaEmrUtils.formatPersonName(encounter.getCreator().getPersonName()));
				simplifiedLabOrder.setPatient(encounter.getPatient());
				simplifiedLabOrder.setEncounterId(encounter.getEncounterId());
				laboratoryOrders.add(simplifiedLabOrder);
			}
		}
		return laboratoryOrders;
	}
	
	public void cancelLabTestOrder(String encounterId) {
		Encounter labOrderEncounter = encounterService.getEncounter(Integer.parseInt(encounterId));
		if (labOrderEncounter != null) {
			labOrderEncounter.setVoided(true);
			labOrderEncounter.setVoidedBy(Context.getAuthenticatedUser());
			labOrderEncounter.setVoidReason("Cancelled Laboratory Order");
			encounterService.saveEncounter(labOrderEncounter);
		}
	}
}
