/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.webservices.rest.web.ConversionUtil;
import org.openmrs.module.webservices.rest.web.representation.CustomRepresentation;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PAST_OPERATION_CONCEPT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PAST_OPERATION_NOTES_CONCEPT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PAST_OPERATION_YEAR_CONCEPT;

@Slf4j
public class PastOperationsFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "visit", required = false) Visit visit) {
		fragmentModel.addAttribute("visit", visit);
		Set<Obs> groupingObs = BotswanaEmrUtils.getPastOperationsObs(visit);
		List<SimpleObject> pastOperationsObjects = BotswanaEmrUtils.getSimplePastOperationsObjects(groupingObs);
		fragmentModel.addAttribute("pastOperationObjects", pastOperationsObjects);
	}
	
	public List<SimpleObject> getPastOperations(@RequestParam(value = "visitUuid") String visitUuid,
	        @SpringBean("visitService") VisitService visitService, UiUtils ui) {
		Visit visit = visitService.getVisitByUuid(visitUuid);
		Set<Obs> groupingObs = BotswanaEmrUtils.getPastOperationsObs(visit);
		List<SimpleObject> pastOperationsObjects = getPastOperationsDetails(groupingObs);
		return pastOperationsObjects;
	}
	
	public static List<SimpleObject> getPastOperationsDetails(Set<Obs> groupingObs) {
		List<SimpleObject> pastOperationsDetails = new ArrayList<>();
		
		for (Obs obs : groupingObs) {
			SimpleObject operationDetail = new SimpleObject();
			
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(PAST_OPERATION_NOTES_CONCEPT)) {
					operationDetail.put("comment", groupMember.getValueText());
				} else if (groupMember.getConcept().getUuid().equals(PAST_OPERATION_YEAR_CONCEPT)) {
					operationDetail.put("year", groupMember.getValueNumeric());
				} else if (groupMember.getConcept().getUuid().equals(PAST_OPERATION_CONCEPT)) {
					if (groupMember.getValueCoded() != null
					        && !groupMember.getValueCoded().getName().getName().equals("Other")) {
						operationDetail.put("pastOperation", groupMember.getValueCoded().getName().getName());
					} else {
						operationDetail.put("pastOperation", groupMember.getValueText());
					}
				}
			}
			
			if (operationDetail.get("year") != null || operationDetail.get("pastOperation") != null
			        || operationDetail.get("comment") != null) {
				pastOperationsDetails.add(operationDetail);
			}
		}
		
		return pastOperationsDetails;
	}
}
