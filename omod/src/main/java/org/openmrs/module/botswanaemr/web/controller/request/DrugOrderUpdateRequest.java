/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.web.controller.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * const payload = { drugUuid: drug, dosingType: "${dosingType}", dose: dosage, doseUnits: doseUnit,
 * frequency: frequency, numRefills: refillRepeat, quantity: quantity, quantityUnits: quantityUnit,
 * duration: refillDuration, durationUnits: "${durationUnits}", route: route, comments: comments,
 * instructions: instructions }
 */
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class DrugOrderUpdateRequest implements Serializable {
	
	@JsonProperty("drugUuid")
	String drugUuid;
	
	String dosingType;
	
	double dosage;
	
	String doseUnits;
	
	String frequency;
	
	int numRefills;
	
	double quantity;
	
	String quantityUnits;
	
	int duration;
	
	String durationUnits;
	
	String route;
	
	String comments;
	
	String instructions;
	
}
