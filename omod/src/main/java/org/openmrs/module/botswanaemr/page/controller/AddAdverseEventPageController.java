/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.coreapps.CoreAppsProperties;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class AddAdverseEventPageController {
	
	public void controller(@RequestParam(value = "formUuid", required = false) String formUuid,
	        @RequestParam("returnUrl") String returnUrl, PageModel model,
	        @RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @SpringBean("coreAppsProperties") CoreAppsProperties coreAppsProperties, UiSessionContext sessionContext) {
		model.addAttribute("formUuid", formUuid);
		
		try {
			returnUrl = URLDecoder.decode(returnUrl, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			
		}
		
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("currentPatient", patient);
		Visit currentVisit = visit != null ? visit
		        : BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), true);
		model.addAttribute("currentVisit", currentVisit);
		model.addAttribute("baseDashboardUrl", coreAppsProperties.getDashboardUrl());
		model.addAttribute("returnUrl", returnUrl);
		model.addAttribute("breadCrumbsDetails", getBreadCrumbsDetails(patient));
		model.addAttribute("breadCrumbsFormatters",
		    Context.getAdministrationService().getGlobalProperty("breadCrumbs.formatters", "(;, ;)").split(";"));
	}
	
	public List<PersonAttribute> getBreadCrumbsDetails(Patient patient) {
		String breadCrumbsDetailsAttrTypeUuids = Context.getAdministrationService()
		        .getGlobalProperty("breadCrumbs.details.personAttr.uuids");
		List<PersonAttribute> personNamePersonAttrs = new ArrayList<PersonAttribute>();
		if (StringUtils.isNotBlank(breadCrumbsDetailsAttrTypeUuids)) {
			for (String breadCrumbDetailAttrTypeUuid : breadCrumbsDetailsAttrTypeUuids.split(",")) {
				PersonAttributeType pat = Context.getPersonService()
				        .getPersonAttributeTypeByUuid(breadCrumbDetailAttrTypeUuid);
				PersonAttribute pa = patient.getAttribute(pat);
				if (pa != null) {
					personNamePersonAttrs.add(pa);
				}
			}
		}
		return CollectionUtils.isNotEmpty(personNamePersonAttrs) ? personNamePersonAttrs : null;
	}
}
