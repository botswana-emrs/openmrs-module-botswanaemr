/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Concept;
import org.openmrs.Location;
import org.openmrs.LocationTag;
import org.openmrs.RelationshipType;
import org.openmrs.api.ConceptService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class RegularRegistrationPageController {
	
	public void controller(PageModel model, UiSessionContext sessionContext) {
		
		model.addAttribute("educationLevels", BotswanaEmrUtils.getRequiredEducationLevels());
		
		List<RelationshipType> relationshipTypes = Context.getPersonService().getAllRelationshipTypes();
		HashSet<String> hashSet = new HashSet<>();
		for (RelationshipType relationshipType : relationshipTypes) {
			hashSet.add(relationshipType.getaIsToB());
			hashSet.add(relationshipType.getbIsToA());
		}
		List<String> relationships = new ArrayList<>(hashSet);
		Collections.sort(relationships);
		model.addAttribute("relationships", relationships);
		
		ConceptService cs = Context.getConceptService();
		List<Concept> employmentSectors = new ArrayList<>();
		for (String uuid : BotswanaEmrConstants.EMPLOYMENT_SECTOR_CONCEPTS) {
			Concept concept = cs.getConceptByUuid(uuid);
			if (concept != null) {
				employmentSectors.add(concept);
			}
		}
		model.addAttribute("employmentSectors", employmentSectors);
		boolean isBf = sessionContext.getSessionLocation().getTags().stream()
		        .anyMatch(locationTag -> locationTag.getName().equals(BotswanaEmrConstants.BDF_FACILITY_LOCATION_TAG_NAME));
		model.addAttribute("isBf", isBf);
		model.addAttribute("mpiSearchEnabled",
		    Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.GP_MPI_SEARCH_ENABLED));
	}
}
