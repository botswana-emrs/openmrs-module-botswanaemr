/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.Concept;
import org.openmrs.ConceptAnswer;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EditSymptomsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiUtils ui, UiSessionContext uiSessionContext) {
		model.addAttribute("visit", visit);
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		EncounterService encounterService = Context.getEncounterService();
		List<Obs> dietGroupingObs = botswanaEmrService.getObservation(patient,
		    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.SYMPTOMS_GROUPING_CONCEPT_UUID),
		    uiSessionContext.getSessionLocation(), limitFetch);
		Set<Obs> groupingObs = new HashSet<Obs>(dietGroupingObs);
		List<SimpleObject> symptomObjects = BotswanaEmrUtils.getSimplifiedSymptomObjects(groupingObs);
		model.addAttribute("symptomObjects", symptomObjects);
		
		Concept durationConcept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.DURATION_UNITS_CONCEPT);
		List<ConceptAnswer> durationAnswers = new ArrayList<>();
		if (durationConcept != null) {
			durationAnswers = BotswanaEmrUtils.getConceptAnswers(durationConcept);
		}
		model.addAttribute("durationUnits", durationAnswers);
	}
	
	EncounterSearchCriteria getEncounterSearchCriteria(Patient patient, List<EncounterType> types) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		
	}
	
	/**
	 * Fragment Action for updating and saving new patient symptoms
	 */
	public void updateSymptoms(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<SimpleObject> deletedSymptoms = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedSymptoms = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newSymptoms = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		saveNewSymptoms(patient, newSymptoms, sessionContext.getSessionLocation(), visit);
		
		ObsService os = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		for (SimpleObject simpleObject : deletedSymptoms) {
			if (simpleObject.get("symptom") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("symptom"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("comment") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("comment"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("duration") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("duration"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("durationUnit") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("durationUnit"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
		}
		
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_NOTE_CONCEPT);
		Concept durationConcept = cs.getConceptByUuid(BotswanaEmrConstants.SYMPTOM_DURATION_CONCEPT);
		Concept durationUnitConcept = cs.getConceptByUuid(BotswanaEmrConstants.DURATION_UNITS_CONCEPT);
		// Edit existing symptoms
		for (SimpleObject simpleObject : editedSymptoms) {
			if (simpleObject.get("uuid") != null && simpleObject.get("symptom") != null) {
				Obs groupingObs = null;
				Obs obs = os.getObsByUuid((String) simpleObject.get("uuid"));
				String symptomVal = (String) simpleObject.get("symptom");
				Concept obsValueCoded = cs.getConceptByUuid(symptomVal);
				if (obsValueCoded != null && obs.getValueCoded() != obsValueCoded) {
					obs.setValueCoded(obsValueCoded);
					os.saveObs(obs, "Edit symptom");
				} else {
					obs.setValueText(symptomVal);
					os.saveObs(obs, "Edit symptom");
				}
				groupingObs = obs.getObsGroup();
				
				// Edit comment
				if (simpleObject.get("commentUuid") != null && simpleObject.get("comment") != null) {
					Obs notesObs = os.getObsByUuid((String) simpleObject.get("commentUuid"));
					if (notesObs != null) {
						if (!notesObs.getValueText().equals(simpleObject.get("comment"))) {
							notesObs.setValueText((String) simpleObject.get("comment"));
							os.saveObs(notesObs, "Edit symptom");
						}
					} else {
						//New note
						Obs newObs = createNewObsFromGroup(groupingObs, notesConcept);
						if (newObs != null) {
							newObs.setValueText((String) simpleObject.get("comment"));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Operation note");
						}
					}
				}
				
				// Edit duration
				if (simpleObject.get("durationUuid") != null && simpleObject.get("duration") != null) {
					Obs durationObs = os.getObsByUuid((String) simpleObject.get("durationUuid"));
					if (durationObs != null) {
						if (!durationObs.getValueNumeric().equals(simpleObject.get("duration"))) {
							durationObs.setValueNumeric(Double.valueOf((String) simpleObject.get("duration")));
							os.saveObs(durationObs, "Edit symptom duration");
						}
					} else {
						//New duration
						Obs newObs = createNewObsFromGroup(groupingObs, durationConcept);
						if (newObs != null) {
							newObs.setValueNumeric(Double.valueOf((String) simpleObject.get("duration")));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Symptom duration");
						}
					}
				}
				
				// Edit duration unit
				if (simpleObject.get("durationUnitUuid") != null && simpleObject.get("durationUnit") != null) {
					Obs durationUnitObs = os.getObsByUuid((String) simpleObject.get("durationUnitUuid"));
					Concept concept = cs.getConceptByUuid(String.valueOf(simpleObject.get("durationUnit")));
					
					if (durationUnitObs != null) {
						if (durationUnitObs.getValueCoded() != concept && concept != null) {
							durationUnitObs.setValueCoded(concept);
							os.saveObs(durationUnitObs, "Edit symptom duration unit");
						}
					} else {
						//New duration unit
						Obs newObs = createNewObsFromGroup(groupingObs, durationUnitConcept);
						if (newObs != null && concept != null) {
							newObs.setValueCoded(concept);
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Symptom duration unit");
						}
					}
				}
			}
		}
	}
	
	private Obs createNewObsFromGroup(Obs groupingObs, Concept concept) {
		if (groupingObs != null) {
			return BotswanaEmrUtils.creatObs(groupingObs.getEncounter(), concept);
		}
		return null;
	}
	
	private void saveNewSymptoms(Patient patient, List<SimpleObject> newSymptoms, Location sessionLocation, Visit visit) {
		EncounterType vitalsEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		Encounter vitalEncounter = BotswanaEmrUtils.createEncounter(patient, vitalsEncounterType, sessionLocation, visit);
		
		ConceptService cs = Context.getConceptService();
		
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.SYMPTOMS_GROUPING_CONCEPT_UUID);
		Concept symptomConcept = cs.getConceptByUuid(BotswanaEmrConstants.SYMPTOMS_CONCEPT);
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PATIENT_NOTE_CONCEPT);
		Concept durationConcept = cs.getConceptByUuid(BotswanaEmrConstants.SYMPTOM_DURATION_CONCEPT);
		Concept durationUnitConcept = cs.getConceptByUuid(BotswanaEmrConstants.DURATION_UNITS_CONCEPT);
		
		for (SimpleObject simpleObject : newSymptoms) {
			Obs groupingObs = BotswanaEmrUtils.creatObs(vitalEncounter, groupingConcept);
			
			if (simpleObject.get("symptom") != null) {
				String symptomVal = (String) simpleObject.get("symptom");
				Concept obsValueCoded = cs.getConceptByUuid(symptomVal);
				Obs symptom = BotswanaEmrUtils.creatObs(vitalEncounter, symptomConcept);
				if (obsValueCoded != null) {
					symptom.setValueCoded(obsValueCoded);
				} else {
					symptom.setValueText(symptomVal);
				}
				groupingObs.addGroupMember(symptom);
				
				if (simpleObject.get("comment") != null) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, notesConcept);
					noteObs.setValueText((String) simpleObject.get("comment"));
					groupingObs.addGroupMember(noteObs);
				}
				
				if (simpleObject.get("duration") != null) {
					Obs durationObs = BotswanaEmrUtils.creatObs(vitalEncounter, durationConcept);
					durationObs.setValueNumeric(Double.valueOf((String) simpleObject.get("duration")));
					groupingObs.addGroupMember(durationObs);
				}
				
				if (simpleObject.get("durationUnit") != null) {
					Obs durationUnitObs = BotswanaEmrUtils.creatObs(vitalEncounter, durationUnitConcept);
					Concept concept = cs.getConceptByUuid(String.valueOf(simpleObject.get("durationUnit")));
					if (concept != null) {
						durationUnitObs.setValueCoded(concept);
						groupingObs.addGroupMember(durationUnitObs);
					}
				}
				vitalEncounter.addObs(groupingObs);
			}
		}
		
		//Save an encounter
		if (vitalEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(vitalEncounter);
		}
	}
	
	/**
	 * Searches for diagnosis concepts
	 * 
	 * @param searchTerm the diagnosis text
	 * @param ui uiUtils
	 * @return A SimpleObject list of diagnosis
	 */
	public List<SimpleObject> searchSymptoms(@RequestParam(value = "searchTerm") String searchTerm, UiUtils ui) {
		List<Concept> concepts = Context.getService(BotswanaEmrService.class).searchConcept(searchTerm,
		    BotswanaEmrConstants.DIAGNOSIS_CONCEPT_CLASS);
		return SimpleObject.fromCollection(concepts, ui, "id", "name", "uuid");
	}
}
