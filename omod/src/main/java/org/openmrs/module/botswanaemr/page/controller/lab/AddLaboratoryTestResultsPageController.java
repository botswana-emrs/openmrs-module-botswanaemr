/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.lab;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Order;
import org.openmrs.OrderAttribute;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class AddLaboratoryTestResultsPageController {
	
	protected final Log log = LogFactory.getLog(AddLaboratoryTestResultsPageController.class);
	
	public void controller(@RequestParam("patientId") Patient patient, @RequestParam("labTestId") String labTestId,
	        @SpringBean("labService") LabService labService, UiSessionContext uiSessionContext, final PageModel pageModel) {
		LabTest laboratoryTest = labService.getLabTestById(NumberUtils.createInteger(labTestId));
		String taskId = ""; // Initialize taskId with a default value
		String orderId = ""; // Initialize orderId with a default value
		if (laboratoryTest != null) {
			SimplifiedLabTest simplifiedLabTest = BotswanaEmrUtils.getSingleSimplifiedLabTest(laboratoryTest, labService);
			if (simplifiedLabTest != null) {
				pageModel.addAttribute("labTest", simplifiedLabTest);
			} else {
				pageModel.addAttribute("labTest", "");
			}
			
			// get Order from laboratoryTest object and fetch associated fhir task
			Order order = laboratoryTest.getOrder();
			if (order != null) {
				// Recursive call to fetch task id from the order attributes. If missing in the current order, fetch from the parent order
				taskId = getTaskIdFromOrder(order);
				
				OrderAttribute orderAttribute = order.getAttributes().stream()
				        .filter(attr -> attr.getAttributeType().getUuid().equals("0bb2965f-2bb2-440c-9ae3-c86d63ad9171"))
				        .findFirst().orElse(null);
				if (orderAttribute != null) {
					taskId = orderAttribute.getValueReference();
				}
				orderId = order.getUuid();
			}
			
			if (taskId == null || taskId.isEmpty()) {
				taskId = Context.getAdministrationService().getGlobalProperty("botswanaemr.defaultTaskId");
			}
			
		}
		
		pageModel.addAttribute("taskId", taskId);
		pageModel.addAttribute("orderId", orderId);
		pageModel.addAttribute("location", uiSessionContext.getSessionLocation());
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
	}
	
	private String getTaskIdFromOrder(Order order) {
		String taskId = "";
		OrderAttribute orderAttribute = order.getAttributes().stream()
		        .filter(attr -> attr.getAttributeType().getUuid().equals("0bb2965f-2bb2-440c-9ae3-c86d63ad9171")).findFirst()
		        .orElse(null);
		if (orderAttribute != null) {
			taskId = orderAttribute.getValueReference();
		} else {
			Order childOrder = Context.getOrderService().getRevisionOrder(order);
			if (childOrder != null) {
				return getTaskIdFromOrder(childOrder);
			}
		}
		
		return taskId;
	}
}
