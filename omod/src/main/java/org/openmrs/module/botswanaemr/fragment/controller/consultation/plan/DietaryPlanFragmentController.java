/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.plan;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class DietaryPlanFragmentController {
	
	public void controller(FragmentModel fragmentModel, @RequestParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit, UiSessionContext uiSessionContext) {
		
		List<Obs> obsList = new ArrayList<>();
		Obs dietaryObs = null;
		for (Encounter encounter : visit.getNonVoidedEncounters()) {
			if (encounter.getEncounterType().getUuid().equals(BotswanaEmrConstants.DIETARY_PLAN_ENCOUNTER_TYPE_UUID)) {
				obsList = encounter.getAllObs(false).stream()
				        .filter(e -> e.getConcept().getUuid().equals(BotswanaEmrConstants.DIETARY_PLAN_CONCEPT_UUID))
				        .collect(Collectors.toList());
			}
		}
		
		if (CollectionUtils.isNotEmpty(obsList)) {
			dietaryObs = obsList.get(0);
		}
		
		fragmentModel.addAttribute("additionalDietaryPlans", obsList);
		fragmentModel.addAttribute("visit", visit);
		fragmentModel.addAttribute("dietaryObs", dietaryObs);
		fragmentModel.addAttribute("location", uiSessionContext.getSessionLocation());
		fragmentModel.addAttribute("patient", patient.getUuid());
		fragmentModel.addAttribute("patientId", patient.getPatientId());
	}
	
	public void createDietaryPlan(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "dietaryPlan") String dietaryPlan, @RequestParam(value = "visitId") Visit visit,
	        UiSessionContext sessionContext) throws IOException {
		
		EncounterType dietEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.DIETARY_PLAN_ENCOUNTER_TYPE_UUID);
		Encounter dietaryPlanEncounter = BotswanaEmrUtils.createEncounter(patient, dietEncounterType,
		    sessionContext.getSessionLocation(), visit);
		
		Concept dietaryPlanConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.DIETARY_PLAN_CONCEPT_UUID);
		
		Obs dietaryPlanObs = BotswanaEmrUtils.creatObs(dietaryPlanEncounter, dietaryPlanConcept);
		dietaryPlanObs.setValueText(dietaryPlan);
		dietaryPlanEncounter.addObs(dietaryPlanObs);
		Context.getEncounterService().saveEncounter(dietaryPlanEncounter);
	}
	
	public void editDietaryPlan(@RequestParam(value = "dietaryObsUuid", required = false) String dietaryObsUuid,
	        @RequestParam(value = "editableDietaryPlan") String editableDietaryPlan) {
		
		ObsService obsService = Context.getObsService();
		
		if (dietaryObsUuid != null && StringUtils.isNotBlank(editableDietaryPlan)) {
			Obs obs = obsService.getObsByUuid(dietaryObsUuid);
			obs.setValueText(editableDietaryPlan);
			obsService.saveObs(obs, "Edited Dietary plan successfully");
		}
	}
	
	public List<SimpleObject> getDietaryPlans(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "visitId", required = false) Visit visit) {
		
		List<SimpleObject> dietaryPlans = new ArrayList<>();
		
		if (visit != null) {
			dietaryPlans = visit.getNonVoidedEncounters().stream()
			        .filter(encounter -> encounter.getEncounterType().getUuid()
			                .equals(BotswanaEmrConstants.DIETARY_PLAN_ENCOUNTER_TYPE_UUID))
			        .flatMap(encounter -> encounter.getAllObs(false).stream())
			        .filter(obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.DIETARY_PLAN_CONCEPT_UUID))
			        .map(obs -> SimpleObject.create("uuid", obs.getUuid(), "valueText", obs.getValueText()))
			        .collect(Collectors.toList());
		}
		
		return dietaryPlans;
	}
}
