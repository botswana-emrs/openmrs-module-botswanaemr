/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import com.google.common.collect.Iterators;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedStockTake;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.*;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;

public class CreateStockTakePageController {
	
	private final IStockroomDataService stockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void get(@RequestParam(value = "stockOperationId", required = false) Integer stockOperationId, PageModel model,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) {
		if (uiSessionContext.getCurrentUser() != null && stockOperationId != null) {
			Set<SimplifiedStockTake> simplifiedStockTakeList = new HashSet<>();
			StockOperation stockOperation = BotswanaInventoryContext.getStockOperationDataService()
			        .getById(stockOperationId);
			
			if (stockOperation != null) {
				for (StockOperationItem operationItem : stockOperation.getItems()) {
					ItemStock itemStock = stockroomDataService.getItem(stockOperation.getSource(), operationItem.getItem());
					ItemStockDetail itemStockDetail = itemStock.getDetails().stream()
					        .filter(i -> i.getBatchOperation() != null
					                && i.getBatchOperation().equals(operationItem.getBatchOperation()))
					        .findFirst().orElse(null);
					
					if (itemStockDetail == null) {
						continue;
					}
					SimplifiedStockTake stockTake = new SimplifiedStockTake();
					stockTake.setId(stockOperationId);
					Item item = itemStock.getItem();
					if (item == null) {
						continue;
					}
					if (!item.getCodes().isEmpty()) {
						stockTake.setItemCode(item.getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
					}
					stockTake.setItemName(item.getName());
					stockTake.setExpiryDate(
					    BotswanaEmrUtils.formatDateWithoutTime(operationItem.getExpiration(), "yyyy-MM-dd"));
					stockTake.setTodayDate(BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
					stockTake.setItemStockUuid(itemStock.getUuid());
					Integer preAdjustmentInteger = operationItem.getPreAdjustmentQuantity();
					if (preAdjustmentInteger == null) {
						preAdjustmentInteger = itemStockDetail.getQuantity() - operationItem.getQuantity();
					}
					int qnty = preAdjustmentInteger + operationItem.getQuantity();
					stockTake.setItemStockQuantity(preAdjustmentInteger);
					stockTake.setEnteredQuantityInStore(qnty);
					
					// Get the stock take attributes and set them to the stock take object
					String comments = operationItem.getComment();
					Integer variance = operationItem.getVariance();
					String varianceReason = operationItem.getVarianceReason();
					String varianceString = variance != null ? String.format("%+d", variance) : "";
					stockTake.setComments(comments);
					stockTake.setVariance(varianceString);
					stockTake.setVarianceReason(varianceReason);
					
					stockTake.setBatchNumber(""); //Set default value
					// TODO: Explore use of the `itemStockDetail.CalculatedBatch` attribute to determine which batches are
					// associated with stock takes and therefore filter them out.
					if (itemStock.hasDetails()) {
						List<ItemStockDetail> itemStockDetailList = itemStock.getDetails().stream()
						        .filter(
						            i -> i.getBatchOperation() != null && i.getBatchOperation().getOperationNumber() != null
						                    && !i.getBatchOperation().getOperationNumber().contains("STOCK_TAKE"))
						        .collect(Collectors.toList());
						if (!itemStockDetailList.isEmpty()) {
							stockTake.setBatchNumber(
							    Iterators.getLast(itemStockDetailList.iterator()).getBatchOperation().getOperationNumber());
						}
					}
					simplifiedStockTakeList.add(stockTake);
				}
			}
			
			model.addAttribute("simplifiedStockTakeList", simplifiedStockTakeList);
			model.addAttribute("selectedStockRoomId", stockOperation.getSource().getId());
			model.addAttribute("stockOperationStatus", stockOperation.getStatus().toString());
		} else {
			model.addAttribute("simplifiedStockTakeList", "");
		}
	}
}
