/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.fragment.controller.PatientQueueListFragmentController;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatientQueuePageController {
	
	public void controller(PageModel model, @RequestParam("location") Location location,
	        @RequestParam(value = "queueRoom", required = false) Location queueRoom) {
		
		model.addAttribute("location", location);
		model.addAttribute("locationList", Context.getLocationService().getAllLocations());
	}
	
}
