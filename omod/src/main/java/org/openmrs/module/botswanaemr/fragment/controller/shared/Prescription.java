/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.shared;

import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.DrugOrder;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.Model;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Prescription {
	
	public static void addPrescription(Model model, Patient patient) {
		
		//OutPatient Care setting
		model.addAttribute("careSettingUuid", BotswanaEmrConstants.CARE_SETTING_UUID);
		
		model.addAttribute("dosingType", "org.openmrs.SimpleDosingInstructions");
		model.addAttribute("orderType", BotswanaEmrConstants.DRUG_ORDER_TYPE_UUID);
		
		//Set to Days
		model.addAttribute("durationUnits", BotswanaEmrConstants.DAYS_DURATION_UNIT);
		model.addAttribute("type", BotswanaEmrConstants.ORDER_TYPE_TEXT);
		
		//Orderer
		model.addAttribute("orderer", BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()).getUuid());
		
		//Medication routes
		//TODO concept routes concepts
		List<Concept> administrationRouteConcepts = BotswanaEmrUtils.getAdministrationRouteConcepts();
		
		model.addAttribute("routes", administrationRouteConcepts);
		model.addAttribute("dosageUnits", BotswanaEmrUtils.getStrengthUnits());
		model.addAttribute("refillDurationUnits", Context.getOrderService().getDurationUnits());
		model.addAttribute("dispensingUnits", Context.getOrderService().getDrugDispensingUnits());
		model.addAttribute("quantityUnits", BotswanaEmrUtils.getQuantityUnits());
		model.addAttribute("quantityUnit", BotswanaEmrConstants.EACH_CONCEPT_UUID);
		
		//Better version: Implement lookup field
		List<Drug> drugs = Context.getConceptService().getAllDrugs().stream().filter(d -> d.getConcept() != null)
		        .sorted(Comparator.comparing(Drug::getDisplayName)).collect(Collectors.toList());
		removeAlreadyPrescribedDrugs(patient, drugs);
		
		model.addAttribute("drugs", drugs);
		model.addAttribute("frequencies", Context.getOrderService().getOrderFrequencies(false));
	}
	
	private static void removeAlreadyPrescribedDrugs(Patient patient, List<Drug> drugs) {
		OrderService orderService = Context.getOrderService();
		OrderType drugOrderType = orderService.getOrderTypeByUuid(BotswanaEmrConstants.DRUG_ORDER_TYPE_UUID);
		List<Order> orders = orderService.getActiveOrders(patient, drugOrderType, null, null);
		for (Order order : orders) {
			
			if (order instanceof DrugOrder) {
				DrugOrder drugOrder = (DrugOrder) order;
				drugs.remove(drugOrder.getDrug());
			}
		}
	}
}
