/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Form;
import org.openmrs.api.FormService;
import org.openmrs.module.htmlformentry.FormEntryContext;
import org.openmrs.module.htmlformentry.FormEntrySession;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentConfiguration;
import org.openmrs.ui.framework.fragment.FragmentModel;

import javax.servlet.http.HttpServletRequest;

public class EnterHtmlFormSectionFragmentController {
	
	public void controller(FragmentConfiguration config,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService,
	        @FragmentParam(value = "formUuid", required = false) String formUuid,
	        @FragmentParam(value = "definitionUiResource", required = false) String definitionUiResource,
	        @FragmentParam(value = "returnUrl", required = false) String returnUrl, FragmentModel fragmentModel,
	        HttpServletRequest request) throws Exception {
		
		config.require("formUuid | definitionUiResource");
		
		HtmlForm htmlForm = null;
		if (StringUtils.isNotBlank(formUuid)) {
			Form form = formService.getFormByUuid(formUuid);
			if (form != null) {
				htmlForm = htmlFormEntryService.getHtmlFormByForm(form);
			}
		}
		if (htmlForm != null) {
			FormEntrySession fes = new FormEntrySession(null, htmlForm, FormEntryContext.Mode.ENTER, request.getSession());
			fragmentModel.addAttribute("command", fes);
			fragmentModel.addAttribute("htmlForm", htmlForm);
		}
		fragmentModel.addAttribute("formUuid", formUuid);
		fragmentModel.addAttribute("definitionUiResource", definitionUiResource);
		fragmentModel.addAttribute("returnUrl", returnUrl);
	}
}
