/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class GenReportsPageController {
	
	public void controller(PageModel fragmentModel, @SpringBean("locationService") LocationService locationService,
	        UiSessionContext uiSessionContext) {
		
		StockOperationTemplate stockOperationTemplate = new StockOperationTemplate();
		stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getExternalRequisition());
		
		stockOperationTemplate.setStatus(StockOperationStatus.PENDING);
		
		// Get my main stockrooms
		List<Stockroom> myMainStockrooms = new ArrayList<>();
		myMainStockrooms.add(StockUtils.getMainStockroom(uiSessionContext.getSessionLocation()));
		// Get all main stockrooms in all facilities
		List<Stockroom> allMainStockrooms = BotswanaInventoryContext.getStockRoomDataService().getAll(false);
		// Keep stockrooms from other facilities
		for (Stockroom stockroom : myMainStockrooms) {
			allMainStockrooms.remove(stockroom);
		}
		
		StockOperationSearch stockOperationsSearch = new StockOperationSearch();
		
		List<StockOperation> stockOperationsReports = new ArrayList<>();
		
		for (Stockroom stockroom : myMainStockrooms) {
			// stockOperationTemplate.setDestination(BotswanaInventoryContext.getStockRoomDataService().getById(5));
			// stockOperationTemplate.setDestination(null);
			// stockOperationTemplate.setSource(stockroom);
			stockOperationTemplate.setStockroom(stockroom);
			
			stockOperationTemplate.setStatus(StockOperationStatus.COMPLETED);
			stockOperationTemplate.setInstanceType(WellKnownOperationTypes.getDistribution());
			
			stockOperationsSearch.setTemplate(stockOperationTemplate);
			List<StockOperation> stockOperations = stockroom == null ? new ArrayList<>()
			        : BotswanaInventoryContext.getStockRoomDataService().getOperations(stockroom, stockOperationsSearch,
			            null);
			stockOperationsReports.addAll(stockOperations);
			
		}
		List<Location> locations = new ArrayList<>();
		
		locations = Context.getLocationService()
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		locations.remove(uiSessionContext.getSessionLocation());
		List<Stockroom> sourceStockrooms = new ArrayList<>();
		sourceStockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(uiSessionContext.getSessionLocation(), false);
		Stockroom mainStockroom = StockUtils.getMainStockroom(uiSessionContext.getSessionLocation());
		List<Stockroom> destinationStockrooms = new ArrayList<>();
		
		fragmentModel.addAttribute("stockOperationsReports", stockOperationsReports);
		fragmentModel.addAttribute("myStockRooms", myMainStockrooms);
		fragmentModel.addAttribute("locations", locations);
		fragmentModel.addAttribute("sourceStockrooms", sourceStockrooms);
		fragmentModel.addAttribute("destinationStockrooms", destinationStockrooms);
		
	}
}
