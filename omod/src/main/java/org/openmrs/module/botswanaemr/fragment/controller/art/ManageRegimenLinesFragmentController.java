/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.Concept;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaRegimenService;
import org.openmrs.module.botswanaemr.model.regimen.*;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import java.util.stream.Collectors;

public class ManageRegimenLinesFragmentController {
	
	public enum Frequency {
		OD,
		BD,
		TDS,
		QID
	}
	
	private BotswanaRegimenService getRegimenService() {
		return Context.getService(BotswanaRegimenService.class);
	}
	
	public void controller(FragmentModel fragmentModel,
	        @FragmentParam(value = "regimenCategory", required = false) String regimenCategory) {
		List<RegimenLine> regimenLines = getRegimenService().getAllRegimenLine();
		if (regimenCategory != null) {
			regimenLines = regimenLines.stream().filter(r -> r.getUuid().equals(regimenCategory))
			        .collect(Collectors.toList());
		}
		fragmentModel.addAttribute("lines", regimenLines);
		fragmentModel.addAttribute("drugFrequencies", getValidDrugFrequencies());
	}
	
	@NotNull
	private EnumSet<Frequency> getValidDrugFrequencies() {
		return EnumSet.of(Frequency.OD, Frequency.BD, Frequency.TDS, Frequency.QID);
	}
	
	public String saveRegimenLines(@RequestParam(value = "regimenLineName", required = false) String regimenLineName,
	        @RequestParam(value = "regimenLineCode", required = false) String regimenLineCode,
	        @RequestParam(value = "regimenLineDescription", required = false) String regimenLineDescription) {
		BotswanaRegimenService botswanaRegimenService = Context.getService(BotswanaRegimenService.class);
		if (StringUtils.isNotBlank(regimenLineName) && StringUtils.isNotBlank(regimenLineCode)) {
			RegimenLine regimenLine = new RegimenLine();
			regimenLine.setName(regimenLineName);
			regimenLine.setCode(regimenLineCode);
			regimenLine.setCreator(Context.getAuthenticatedUser());
			regimenLine.setDateCreated(new Date());
			regimenLine.setRetired(false);
			if (StringUtils.isNotBlank(regimenLineDescription)) {
				regimenLine.setDescription(regimenLineDescription);
			}
			botswanaRegimenService.saveRegimenLine(regimenLine);
		}
		return "Regimen Line Saved";
	}
	
	public String saveRegimen(@RequestParam(value = "regimenName", required = false) String regimenName,
	        @RequestParam(value = "regimenLine", required = false) Integer regimenLine,
	        @RequestParam(value = "regimenConcept", required = false) String regimenConceptUuid) {
		BotswanaRegimenService botswanaRegimenService = Context.getService(BotswanaRegimenService.class);
		Concept concept = Context.getConceptService().getConceptByName(regimenConceptUuid);
		if (StringUtils.isNotBlank(regimenName) && regimenLine != null && concept != null) {
			Regimen regimen = new Regimen();
			regimen.setRegimenName(regimenName);
			regimen.setRegimenLine(botswanaRegimenService.getRegimenLine(regimenLine));
			regimen.setCreator(Context.getAuthenticatedUser());
			regimen.setDateCreated(new Date());
			regimen.setConceptRef(concept);
			regimen.setVoided(false);
			try {
				botswanaRegimenService.saveRegimen(regimen);
			}
			catch (APIException e) {
				throw new APIException("Failed to save regimen", e);
			}
		}
		return "Regimen Saved";
	}
	
	public List<SimpleObject> getRegimenConcepts(String regimenConcept, UiUtils ui) {
		List<Concept> conceptsList = Context.getConceptService().getConceptsByName(regimenConcept, Locale.getDefault(),
		    false);
		return SimpleObject.fromCollection(conceptsList, ui, "conceptId", "names.name", "uuid", "displayString");
	}
	
	public String saveRegimenComponent(
	        @RequestParam(value = "regimenComponentId", required = false) String regimenComponentId,
	        @RequestParam(value = "regimen") Integer regimenId,
	        @RequestParam(value = "regimenComponentLabel") String regimenComponentLabel,
	        @RequestParam(value = "drugs", required = false) String regimenComponentDrugsString)
	        throws JSONException, APIException {
		if (StringUtils.isNotEmpty(regimenComponentId)) {
			RegimenComponent regimenComponent = getRegimenService()
			        .getRegimenComponentById(Integer.parseInt(regimenComponentId));
			if (regimenComponent != null) {
				regimenComponent.setVoided(true);
				regimenComponent.setVoidedBy(Context.getAuthenticatedUser());
				regimenComponent.setDateVoided(new Date());
				regimenComponent.setVoidReason("Voided by user");
				getRegimenService().saveRegimenComponent(regimenComponent);
			}
		}
		Regimen regimen = getRegimenService().getRegimen(regimenId);
		if (regimenComponentLabel != null && regimen != null && regimenComponentDrugsString != null) {
			
			JSONObject componentDrugsJsonObject = new JSONObject(regimenComponentDrugsString);
			List<RegimenComponentDrug> regimenComponentDrugs = new ArrayList<>();
			Iterator<String> keys = componentDrugsJsonObject.keys();
			
			while (keys.hasNext()) {
				String key = keys.next();
				JSONObject drugObject = componentDrugsJsonObject.getJSONObject(key);
				RegimenComponentDrug regimenComponentDrug = new RegimenComponentDrug();
				regimenComponentDrug.setRegimen(regimen);
				regimenComponentDrug.setDateCreated(new Date());
				regimenComponentDrug.setCreator(Context.getAuthenticatedUser());
				regimenComponentDrug.setVoided(false);
				regimenComponentDrug.setDrug(Context.getConceptService().getDrugByUuid(drugObject.getString("drug")));
				String dosageStrength = drugObject.getString("strength");
				
				regimenComponentDrug.setStrength(dosageStrength);
				
				// split by " " to isolate the frequency from the dosage strength
				String[] dosageStrengthSplit = dosageStrength.split(" ");
				String dosage = dosageStrengthSplit[0];
				
				if (dosageStrengthSplit.length < 2) {
					extractDosageAndUnits(regimenComponentDrug, dosage);
				} else {
					String frequency = dosageStrengthSplit[1];
					
					extractDosageAndUnits(regimenComponentDrug, dosage);
					
					EnumSet<Frequency> validDrugFrequencies = getValidDrugFrequencies();
					
					if (!validDrugFrequencies.contains(Frequency.valueOf(frequency))) {
						regimenComponentDrug.setFrequency(Context.getConceptService().getConceptByName("Once a day"));
					} else {
						regimenComponentDrug.setFrequency(Context.getConceptService().getConceptByName(frequency));
					}
				}
				regimenComponentDrugs.add(regimenComponentDrug);
				
				// Save the regimen component drug
				regimenComponentDrugs.add(regimenComponentDrug);
			}
			
			RegimenComponent regimenComponent = new RegimenComponent(regimen, regimenComponentLabel, regimenComponentDrugs);
			regimenComponent.setCreator(Context.getAuthenticatedUser());
			regimenComponent.setDateCreated(new Date());
			regimenComponent.setVoided(false);
			
			try {
				getRegimenService().saveRegimenComponent(regimenComponent);
			}
			catch (APIException e) {
				throw new APIException("Failed to save regimen component", e);
			}
			
		}
		return "Regimen Component  Saved";
	}
	
	private void extractDosageAndUnits(RegimenComponentDrug regimenComponentDrug, String dosage) {
		// Retrieve the dosage and units and ignore the frequency if the strength is in the format "300mg". Validate via regex
		if (dosage.matches(".*\\d.*")) {
			regimenComponentDrug.setDose(Double.parseDouble(dosage.replaceAll("[^0-9]", "")));
			String dosageUnits = dosage.replaceAll("[^a-zA-Z]", "");
			Concept units = Context.getConceptService().getConceptByName(dosageUnits);
			if (units != null) {
				regimenComponentDrug.setUnits(units);
			}
		}
	}
	
	public SimpleObject getRegimenComponent(@RequestParam(value = "regimenComponentId") Integer regimenComponentId,
	        UiUtils ui) {
		RegimenComponent regimenComponent = getRegimenService().getRegimenComponentById(regimenComponentId);
		SimpleObject simpleObject = SimpleObject.fromObject(regimenComponent, ui, "uuid", "regimenComponentId",
		    "componentLabel", "creator", "dateCreated");
		simpleObject.putAll(SimpleObject.fromObject(regimenComponent.getRegimen(), ui, "regimenId", "regimenName",
		    "regimenLine", "conceptRef"));
		simpleObject.put("componentDrugs", SimpleObject.fromCollection(regimenComponent.getRegimenComponentDrugs(), ui,
		    "drug", "dose", "units", "frequency", "strength"));
		
		SimpleObject simpleObject1 = new SimpleObject();
		simpleObject1.put("data", simpleObject);
		simpleObject1.put("status", "success");
		return simpleObject1;
	}
}
