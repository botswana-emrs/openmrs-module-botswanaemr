/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.converter.simplifier;

import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.simplifier.BotswanaEmrAbstractSimplifier;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Converts a {@link Discussion} to a {@link SimpleObject}.
 */
@Component
public class BotswanaEmrDiscussionSimplifier extends BotswanaEmrAbstractSimplifier<Discussion> {
	
	/**
	 * @see BotswanaEmrAbstractSimplifier#simplify(Object)
	 */
	
	private final BotswanaEmrService botswanaEmrService;
	
	public BotswanaEmrDiscussionSimplifier(BotswanaEmrService botswanaEmrService) {
		this.botswanaEmrService = botswanaEmrService;
	}
	
	@Override
	protected SimpleObject simplify(Discussion discussion) {
		SimpleObject ret = new SimpleObject();
		ret.put("discussionId", discussion.getDiscussionId());
		ret.put("title", discussion.getTitle());
		ret.put("dateStarted", discussion.getDateStarted());
		ret.put("closed", discussion.getClosed());
		ret.put("userId", discussion.getStartedBy().getUserId());
		ret.put("mine", discussion.getStartedBy().equals(Context.getAuthenticatedUser()));
		ret.put("participationAllowed", participationAllowed(discussion));
		ret.put("hasParticipants", discussion.getParticipants() != null && !discussion.getParticipants().isEmpty());
		return ret;
	}
	
	private boolean participationAllowed(Discussion discussion) {
		List<User> participants = botswanaEmrService.getDiscussionParticipantsByDiscussion(discussion).stream()
		        .map(dp -> dp.getUser()).collect(Collectors.toList());
		return participants.contains(Context.getAuthenticatedUser())
		        || (discussion != null && discussion.getStartedBy().equals(Context.getAuthenticatedUser()));
	}
}
