/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.lab;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DATE_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAB_RESULTS_TEXT;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAB_TEST_INTERPRETATION;

public class LaboratoryTestOrdersFragmentController {
	
	private final ObsService obsService = Context.getService(ObsService.class);
	
	private final LabService labService = Context.getService(LabService.class);
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	protected final Log log = LogFactory.getLog(LaboratoryTestOrdersFragmentController.class);
	
	public void controller(@RequestParam(value = "encounterId", required = false) String encounterId,
	        @SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		Location location = Context.getLocationService().getLocationByUuid(
		    String.valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT)));
		if (location != null) {
			fragmentModel.addAttribute("currentServiceDeliveryPoint", location.getUuid());
		}
		if (encounterId != null) {
			fragmentModel.addAttribute("encounterId", encounterId);
		} else {
			fragmentModel.addAttribute("encounterId", "");
		}
	}
	
	public List<SimpleObject> getLaboratoryTests(UiUtils ui,
	        @RequestParam(value = "encounterId", required = false) String encounterId) {
		LabService labService = Context.getService(LabService.class);
		List<LabTest> labTests = labService.getAllLabTests().stream()
		        .filter(labTest -> labTest.getAcceptDate().getTime() >= BotswanaEmrUtils.getStartOfDay().getTime())
		        .filter(labTest -> labTest.getEncounter().getEncounterId().equals(Integer.parseInt(encounterId)))
		        .collect(Collectors.toList());
		List<SimplifiedLabTest> tests = BotswanaEmrUtils.getSimplifiedLabTests(labTests, labService);
		return SimpleObject.fromCollection(tests, ui, "testId", "testName", "status", "patientId", "patient", "patientNames",
		    "dateAdded", "requestedBy", "resultsList");
	}
	
	public void removeLabTest(Integer testId, LabService labService) {
		LabTest labTest = labService.getLabTestById(testId);
		if (labTest != null) {
			labService.deleteLabTest(labTest);
		}
	}
	
	public void addTestResults(@RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "labTestId") String labTestId,
	        @RequestParam(value = "labTestResults") String labTestResults,
	        // @RequestParam(value = "observationResults", required = false) Map<String,String> observationResults,
	        @RequestParam(value = "labResultsInterpretation", required = false) String labResultsInterpretation,
	        @RequestParam(value = "labTestSampleCollectionDate", required = false) String labTestSampleCollectionDate) {
		
		Integer integerLabTestId = NumberUtils.createInteger(labTestId);
		LabTest labTest = labService.getLabTestById(integerLabTestId);
		Concept labTestResultsConcept = conceptService.getConceptByUuid(LAB_RESULTS_TEXT);
		Concept labTestResultsInterpretationConcept = conceptService.getConceptByUuid(LAB_TEST_INTERPRETATION);
		Concept labTestSampleCollectionDateConcept = conceptService.getConceptByUuid(DATE_CONCEPT_UUID);
		
		if (labTest != null) {
			labTest.setStatus("COMPLETED");
			Order testOrder = labTest.getOrder();
			Order workingTestOrder = testOrder;
			if (workingTestOrder != null) {
				if (!workingTestOrder.isActive()) {
					// iteratively look for the active child order
					Order childTestOrder = Context.getOrderService().getRevisionOrder(workingTestOrder);
					while (childTestOrder != null) {
						if (childTestOrder.isActive()) {
							testOrder = childTestOrder;
							break;
						}
						childTestOrder = Context.getOrderService().getRevisionOrder(childTestOrder);
					}
				}
				
				Encounter labOrderEncounter = testOrder.getEncounter();
				
				// if (observationResults != null) {
				// 	for (Map.Entry<String, String> entry : observationResults.entrySet()) {
				// 		Concept concept = conceptService.getConceptByUuid(entry.getKey());
				// 		createAndAddObs(labOrderEncounter, concept, entry.getValue(), "Laboratory Test Results");
				// 	}
				// }
				
				if (labTestResults != null) {
					createAndAddObs(labOrderEncounter, labTestResultsConcept, labTestResults, "Laboratory Test Results");
				}
				
				if (labResultsInterpretation != null) {
					createAndAddObs(labOrderEncounter, labTestResultsInterpretationConcept, labResultsInterpretation,
					    "Laboratory Test Results Interpretation");
				}
				
				if (labTestSampleCollectionDate != null) {
					createAndAddObs(labOrderEncounter, labTestSampleCollectionDateConcept, labTestSampleCollectionDate,
					    "Laboratory Sample Collection Date");
				}
				
				Order editedTestOrder = testOrder.cloneForRevision();
				editedTestOrder.setEncounter(labOrderEncounter);
				editedTestOrder.setPatient(patient);
				editedTestOrder.setOrderer(testOrder.getOrderer());
				orderService.saveOrder(editedTestOrder, null);
				log.info("\n Add Test Results \n" + editedTestOrder);
			}
			labService.saveLabTest(labTest);
		}
	}
	
	private void createAndAddObs(Encounter encounter, Concept concept, String valueText, String comment) {
		Obs obs = BotswanaEmrUtils.creatObs(encounter, concept);
		if (concept.getDatatype().isNumeric()) {
			obs.setValueNumeric(Double.parseDouble(valueText));
		}
		if (concept.getDatatype().isText()) {
			obs.setValueText(valueText);
		}
		if (concept.getDatatype().isCoded()) {
			obs.setValueCoded(conceptService.getConceptByUuid(valueText));
		}
		
		obs.setValueDatetime(new Date());
		obs.setComment(comment);
		obsService.saveObs(obs, comment);
		encounter.addObs(obs);
	}
}
