/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.openmrs.Location;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.GeneralPatientObject;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPerson;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class RegistrationAdminDashboardPageController {
	
	private static final String GET_LOCATIONS = "Get Locations";
	
	private static final String VIEW_LOCATIONS = "View Locations";
	
	public void get(PageModel model, UiSessionContext sessionContext,
	        @SpringBean("locationService") LocationService locationService) {
		Date today = DateUtil.getStartOfDay(new Date());
		Date yesterDay = DateUtil.adjustDate(today, -1, DurationUnit.DAYS);
		
		//Check for possibility of null pointer to some results
		int allPatientListSize = 0;
		int todayRegistration = 0;
		int yesterdayPatients = 0;
		
		Location sessionLocation = sessionContext.getSessionLocation();
		String locationUuid = sessionLocation == null ? "" : sessionLocation.getUuid();
		
		allPatientListSize = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDateCountOnly(null, null,
		    null, sessionLocation);
		todayRegistration = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDateCountOnly(today, today,
		    null, sessionLocation);
		yesterdayPatients = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDateCountOnly(yesterDay,
		    yesterDay, null, sessionLocation);
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		model.addAttribute("allRegisteredPatients", allPatientListSize);
		model.addAttribute("todayRegisteredPatients", todayRegistration);
		model.addAttribute("yesterdayRegisteredPatients", yesterdayPatients);
		model.addAttribute("registeredPatientsDailyAverage", getDailyRegisteredAveragePatients(sessionLocation));
		model.addAttribute("todayRegisteredPatientsList",
		    patientObjects(getAllEverRegisteredPatientsWithLimits(today, today, limitFetch, sessionLocation)));
		
		boolean isRegistration;
		User authenticatedUser = sessionContext.getCurrentUser();
		isRegistration = authenticatedUser != null && BotswanaEmrUtils.isRegistrationClerk(authenticatedUser);
		model.addAttribute("isRegistration", isRegistration);
		
	}
	
	private List<Registration> getAllEverRegisteredPatientsWithLimits(Date start, Date end, Integer limit,
	        Location location) {
		List<Registration> registered = Context.getService(BotswanaEmrService.class).getPatientsCountRegisteredOnDate(start,
		    end, limit, location);
		
		return registered;
	}
	
	private int getAllEverRegisteredPatientsWithLimitsCount(Date start, Date end, Integer limit, Location location) {
		return Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDateCountOnly(start, end, limit,
		    location);
	}
	
	private Integer getDailyRegisteredAveragePatients(Location location) {
		
		int totalPatientsEverRegistered = 0;
		Registration firstPatient = null;
		Registration lastPatient = null;
		int daysBetweenDates = 0;
		
		if (getAllEverRegisteredPatientsWithLimits(null, null, null, location) != null
		        && getAllEverRegisteredPatientsWithLimitsCount(null, null, null, location) > 0) {
			totalPatientsEverRegistered = getAllEverRegisteredPatientsWithLimitsCount(null, null, null, location);
			firstPatient = getAllEverRegisteredPatientsWithLimits(null, null, null, location).get(0);
			lastPatient = getAllEverRegisteredPatientsWithLimits(null, null, null, location)
			        .get(getAllEverRegisteredPatientsWithLimitsCount(null, null, null, location) - 1);
		}
		if (firstPatient != null && lastPatient != null) {
			daysBetweenDates = BotswanaEmrUtils.unitsSince(lastPatient.getRegistrationDatetime(),
			    firstPatient.getRegistrationDatetime(), "days");
		}
		if (daysBetweenDates == 0) {
			daysBetweenDates = 1;
		}
		
		return totalPatientsEverRegistered / daysBetweenDates;
		
	}
	
	private List<GeneralPatientObject> patientObjects(List<Registration> paymentList) {
		List<GeneralPatientObject> allItems = new ArrayList<>();
		GeneralPatientObject generalPatientObject;
		if (paymentList != null) {
			for (Registration payment : paymentList) {
				generalPatientObject = new GeneralPatientObject();
				generalPatientObject.setName(formatPersonName(payment.getPatient().getPersonName()));
				generalPatientObject.setGender(formatGender(payment.getPatient()));
				generalPatientObject.setCreator(formatPerson(payment.getCreator().getPerson()));
				generalPatientObject.setDuration(formatDurationSinceRegistration(payment));
				
				allItems.add(generalPatientObject);
			}
		}
		
		return allItems;
	}
	
}
