/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.appointments;

import java.util.List;

import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class ViewAppointmentPageController {
	
	public void controller(PageModel model, UiSessionContext sessionContext,
	        @RequestParam(value = "appointmentId", required = true) String appointmentId,
	        @RequestParam("patientId") Patient patient) {
		Appointment appointment = Context.getService(AppointmentService.class)
		        .getAppointment(Integer.valueOf(appointmentId));
		model.addAttribute("appointment", appointment);
		List<Obs> clientCategoryObs = BotswanaEmrUtils.getObservations(appointment.getPatient(),
		    BotswanaEmrUtils.getConcept("164181AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"), appointment.getVisit());
		model.addAttribute("clientCategory",
		    clientCategoryObs.size() > 0 ? clientCategoryObs.get(0).getValueCoded().getName().getName() : ""); //TODO: retrieve client category
		model.addAttribute("patient", patient);
	}
}
