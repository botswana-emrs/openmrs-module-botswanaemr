/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.programs.tb;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.User;
import org.openmrs.api.LocationService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ART_SERVICES_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DOCTORS_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_TESTING_SERVICES_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.LAB_SERVICES_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NURSING_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PATIENT_REGISTRATION_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.PHARMACY_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_ART_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_DOCTOR;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_HOSPITAL_ADMIN;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_HTS_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_LAB_SERVICES;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_NURSE;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_PHARMACIST;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_REGISTRATION_CLERK;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_SRH_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_STOCK_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_SYSTEM_ADMIN;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_TB_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.ROLE_VMMC_MANAGER;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.STOCK_MANAGEMENT_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SERVICES_PORTAL_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VMMC_PORTAL_UUID;

@Slf4j
public class RetrospectiveDotBatchPageController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, PageModel pageModel,
	        UiSessionContext sessionContext) {
		
		LocationService locationService = Context.getLocationService();
		Date enrollmentDate = null;
		Date completionDate = null;
		
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program tbProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
		
		List<PatientProgram> patientProgramList = programWorkflowService.getPatientPrograms(patient, tbProgram, null, null,
		    null, null, false);
		List<PatientProgram> activePrograms = patientProgramList.stream().filter(PatientProgram::getActive)
		        .collect(Collectors.toList());
		
		if (activePrograms.size() > 0) {
			enrollmentDate = activePrograms.get(0).getDateEnrolled();
			completionDate = activePrograms.get(0).getDateCompleted();
		}
		
		pageModel.addAttribute("patient", patient);
		String enrollmentDateStr;
		if (enrollmentDate != null) {
			enrollmentDateStr = DateUtils.getStringDate(enrollmentDate, "yyyy-MM-dd");
		} else {
			enrollmentDateStr = "";
		}
		String completionDateStr;
		if (completionDate != null) {
			completionDateStr = DateUtils.getStringDate(completionDate, "yyyy-MM-dd");
		} else {
			completionDateStr = "";
		}
		pageModel.addAttribute("enrollmentDate", enrollmentDateStr);
		pageModel.addAttribute("completionDate", completionDateStr);
		pageModel.addAttribute("treatmentTypes", getTreatmentTypes());
		pageModel.addAttribute("administers", getAdministers());
		pageModel.addAttribute("providers", getProviderList());
		Double latestPatientWeight = getLatestPatientWeight(patient);
		pageModel.addAttribute("lastWeight", latestPatientWeight == null ? "" : latestPatientWeight);
		pageModel.addAttribute("patientAge", patient.getAge() != null ? patient.getAge() : "");
		pageModel.addAttribute("drugs", getFixedDoseDrugs());
		pageModel.addAttribute("currentProvider", sessionContext.getCurrentProvider());
		ArrayList<Location> facilitiesList = BotswanaEmrUtils.getFacilitiesList();
		pageModel.addAttribute("assignedlocations", facilitiesList);
	}
	
	protected static List<Provider> getProviderList() {
		return Context.getProviderService().getAllProviders(false);
	}
	
	protected static List<Concept> getTreatmentTypes() {
		Concept concept = new Concept();
		return new ArrayList<>(Arrays.asList(
		    Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.INITIAL_TREATMENT_PHASE_UUID),
		    Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.CONTINUATION_TREATMENT_PHASE_UUID)));
	}
	
	private List<Concept> getFixedDoseDrugs() {
		return Arrays.asList(getDrug("e0ae875d-15d4-4777-b303-026b48c8bf40"),
		    getDrug("fbb7afc3-6a1a-4b3b-9428-fb8edf628af5"), getDrug("6cd7233e-1105-466c-b3cf-4cf7cf0be81b"),
		    getDrug("d12de6ea-20dd-4f33-92a4-5fc0acd66825"), getDrug("038a6cc3-07a3-4396-a94e-ebdba61b6e28"),
		    getDrug("02a51202-8e00-493f-8cd7-db2eb98a39dc"));
	}
	
	private Concept getDrug(@NotNull String drug) {
		return Context.getConceptService().getConceptByUuid(drug);
	}
	
	protected static List<Concept> getAdministers() {
		return new ArrayList<>(
		        Arrays.asList(Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.PHYSICIAN_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.GUARDIAN_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.COMMUNITY_HEALTH_WORKER_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.MISSED_CONCEPT_UUID),
		            Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID)));
	}
	
	private Double getLatestPatientWeight(Patient patient) {
		Concept weightConcept = BotswanaEmrUtils.getConcept(BotswanaEmrConstants.WEIGHT_CONCEPT_UUID);
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		
		Obs lastWeightObs = Context.getObsService().getObservationsByPersonAndConcept(patient, weightConcept).stream()
		        .max(obsDateComparator).orElse(null);
		return lastWeightObs != null ? lastWeightObs.getValueNumeric() : null;
	}
	
}
