/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.ObsService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class EditPastOperationsFragmentController {
	
	public void controller(FragmentModel model, @FragmentParam("patientId") Patient patient,
	        @FragmentParam(value = "visit", required = false) Visit visit) {
		model.addAttribute("visit", visit);
		Set<Obs> groupingObs = BotswanaEmrUtils.getPastOperationsObs(visit);
		
		List<SimpleObject> pastOperationsObjects = BotswanaEmrUtils.getSimplePastOperationsObjects(groupingObs);
		model.addAttribute("patient", patient);
		model.addAttribute("pastOperationObjects", pastOperationsObjects);
	}
	
	/**
	 * Fragment Action for updating and saving new patient past operations
	 */
	public void updatePastOperations(UiUtils ui, @RequestParam("patientId") String patientId,
	        @RequestParam(value = "data", required = false) String data,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiSessionContext sessionContext)
	        throws IOException {
		Patient patient = Context.getPatientService().getPatient(Integer.valueOf(patientId));
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(data);
		
		ArrayNode arrayNode = (ArrayNode) jsonNode.get("deleted");
		List<SimpleObject> deletedOperations = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("edited");
		List<SimpleObject> editedOperations = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		arrayNode = (ArrayNode) jsonNode.get("new");
		List<SimpleObject> newOperations = mapper.readValue(arrayNode, new TypeReference<List<SimpleObject>>() {});
		
		ObsService os = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		for (SimpleObject simpleObject : deletedOperations) {
			if (simpleObject.get("pastOperation") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("pastOperation"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
				
			}
			if (simpleObject.get("comment") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("comment"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
			}
			if (simpleObject.get("year") != null) {
				Obs obs = os.getObsByUuid((String) simpleObject.get("year"));
				if (obs != null) {
					obs.setVoided(true);
					os.saveObs(obs, "deleted");
				}
				
			}
		}
		
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATION_NOTES_CONCEPT);
		Concept yearConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATION_YEAR_CONCEPT);
		Concept otherCodedConcept = cs.getConceptByUuid(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID);
		
		// Edit existing operations
		for (SimpleObject simpleObject : editedOperations) {
			
			if (simpleObject.get("pastOperationUuid") != null && simpleObject.get("pastOperation") != null) {
				Obs groupingObs = null;
				Obs obs = os.getObsByUuid((String) simpleObject.get("pastOperationUuid"));
				if (obs == null) {
					newOperations.add(simpleObject);
					break;
				}
				
				Concept obsValueCoded = cs.getConceptByUuid((String) simpleObject.get("pastOperation"));
				String pastOperation = String.valueOf(simpleObject.get("pastOperation"));
				if (obsValueCoded == null && (obs.getValueText() == null || !obs.getValueText().equals(pastOperation))) {
					if (otherCodedConcept != null) {
						obs.setValueCoded(otherCodedConcept);
						obs.setValueText((String) simpleObject.get("pastOperation"));
					}
					os.saveObs(obs, "Edit past operation");
				} else if (obs.getValueCoded() != obsValueCoded) {
					obs.setValueCoded(obsValueCoded);
					os.saveObs(obs, "Edit past operation");
				}
				groupingObs = obs.getObsGroup();
				
				if (simpleObject.get("commentUuid") != null && simpleObject.get("comment") != null) {
					Obs commentObs = os.getObsByUuid((String) simpleObject.get("commentUuid"));
					
					if (commentObs != null) {
						if (!commentObs.getValueText().equals(simpleObject.get("comment"))) {
							commentObs.setValueText((String) simpleObject.get("comment"));
							os.saveObs(commentObs, "Edit past operation notes");
						}
					} else {
						// New note
						Obs newObs = createNewObsFromGroup(groupingObs, notesConcept);
						if (newObs != null) {
							newObs.setValueText((String) simpleObject.get("comment"));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Operation note");
						}
					}
				}
				
				if (StringUtils.isNotEmpty((String) simpleObject.get("yearUuid"))
				        && StringUtils.isNotEmpty((String) simpleObject.get("year"))) {
					Obs yearObs = os.getObsByUuid((String) simpleObject.get("yearUuid"));
					if (yearObs != null) {
						if (!yearObs.getValueNumeric().equals(Double.valueOf((String) simpleObject.get("year")))) {
							yearObs.setValueNumeric(Double.valueOf((String) simpleObject.get("year")));
							os.saveObs(yearObs, "Edit past operation year");
						}
					} else {
						// New note
						Obs newObs = createNewObsFromGroup(groupingObs, yearConcept);
						if (newObs != null) {
							newObs.setValueNumeric(Double.parseDouble((String) simpleObject.get("year")));
							newObs.setObsGroup(groupingObs);
							newObs.setObsDatetime(new Date());
							os.saveObs(newObs, "Operation year");
						}
					}
				}
			}
			
		}
		
		saveNewPastOperations(patient, newOperations, sessionContext.getSessionLocation(), visit);
		
	}
	
	private Obs createNewObsFromGroup(Obs groupingObs, Concept concept) {
		if (groupingObs != null) {
			return BotswanaEmrUtils.creatObs(groupingObs.getEncounter(), concept);
		}
		return null;
	}
	
	private void saveNewPastOperations(Patient patient, List<SimpleObject> newPastOperations, Location sessionLocation,
	        Visit visit) {
		EncounterType vitalsEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		Encounter vitalEncounter = BotswanaEmrUtils.createEncounter(patient, vitalsEncounterType, sessionLocation, visit);
		
		ConceptService cs = Context.getConceptService();
		
		Concept groupingConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATIONS_GROUPING_CONCEPT);
		Concept pastOperationConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATION_CONCEPT);
		Concept notesConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATION_NOTES_CONCEPT);
		Concept yearConcept = cs.getConceptByUuid(BotswanaEmrConstants.PAST_OPERATION_YEAR_CONCEPT);
		
		for (SimpleObject simpleObject : newPastOperations) {
			Obs groupingObs = BotswanaEmrUtils.creatObs(vitalEncounter, groupingConcept);
			
			if (simpleObject.get("pastOperation") != null) {
				Concept obsValueCoded = cs.getConceptByUuid((String) simpleObject.get("pastOperation"));
				Obs pastOperation = BotswanaEmrUtils.creatObs(vitalEncounter, pastOperationConcept);
				if (obsValueCoded != null) {
					pastOperation.setValueCoded(obsValueCoded);
				} else {
					Concept otherCodedConcept = Context.getConceptService()
					        .getConceptByUuid(BotswanaEmrConstants.OTHER_CODED_CONCEPT_UUID);
					if (otherCodedConcept != null) {
						pastOperation.setValueCoded(otherCodedConcept);
						pastOperation.setValueText((String) simpleObject.get("pastOperation"));
					}
					
				}
				groupingObs.addGroupMember(pastOperation);
				
				if (StringUtils.isNotEmpty(String.valueOf(simpleObject.get("comment")))) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, notesConcept);
					noteObs.setValueText((String) simpleObject.get("comment"));
					groupingObs.addGroupMember(noteObs);
				}
				
				if (StringUtils.isNotEmpty((String) simpleObject.get("year"))) {
					Obs noteObs = BotswanaEmrUtils.creatObs(vitalEncounter, yearConcept);
					noteObs.setValueNumeric(Double.valueOf((String) simpleObject.get("year")));
					groupingObs.addGroupMember(noteObs);
				}
				vitalEncounter.addObs(groupingObs);
			}
		}
		
		//Save an encounter
		if (vitalEncounter.getAllObs() != null) {
			Context.getEncounterService().saveEncounter(vitalEncounter);
		}
	}
	
	EncounterSearchCriteria getEncounterSearchCriteria(Patient patient, List<EncounterType> types) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		
	}
	
	/**
	 * Searches for concepts
	 * 
	 * @param searchTerm the search text
	 * @param ui uiUtils
	 * @return A SimpleObject list of concepts
	 */
	public List<SimpleObject> searchPastOperations(@RequestParam(value = "searchTerm") String searchTerm, UiUtils ui) {
		List<Concept> concepts = Context.getService(BotswanaEmrService.class).searchConcept(searchTerm,
		    BotswanaEmrConstants.PROCEDURE_CONCEPT_CLASS);
		return SimpleObject.fromCollection(concepts, ui, "id", "name", "uuid");
	}
}
