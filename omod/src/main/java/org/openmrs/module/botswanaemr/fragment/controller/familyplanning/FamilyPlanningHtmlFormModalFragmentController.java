/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.familyplanning;

import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

public class FamilyPlanningHtmlFormModalFragmentController {
	
	public void controller(UiUtils ui, UiSessionContext sessionContext, FragmentModel fragmentModel,
	        @FragmentParam("formUuid") String formUuid, @FragmentParam("modalTitle") String modalTitle,
	        @FragmentParam("encounter") Encounter encounter, @FragmentParam("hasEncounter") Boolean hasEncounter,
	        @FragmentParam("definitionUiResource") String definitionUiResource, @RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(required = false, value = "visitId") Visit visit) {
		
		Visit currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
		    false);
		
		fragmentModel.addAttribute("encounter", encounter);
		fragmentModel.addAttribute("defaultEncounterDate", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
		fragmentModel.addAttribute("visitId", visit.getVisitId());
		fragmentModel.addAttribute("currentPatientVisit", currentPatientVisit);
		fragmentModel.addAttribute("currentPatient", patient);
		fragmentModel.addAttribute("formUuid", formUuid);
		fragmentModel.addAttribute("modalTitle", modalTitle);
		fragmentModel.addAttribute("hasEncounter", hasEncounter);
		fragmentModel.addAttribute("definitionUiResource", definitionUiResource);
		fragmentModel.addAttribute("returnUrl", ui.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr",
		    "familyplanning/familyPlanningCard", patient, currentPatientVisit, ui)));
	}
}
