/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.srh;

import org.apache.commons.lang3.ObjectUtils;
import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.SRH_ENROLMENT_FORM_UUID;

public class SrhClientProfilePageController {
	
	private final VisitService visitService = Context.getVisitService();
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @SpringBean("formService") FormService formService,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "visitId", required = false) Visit visit, UiUtils ui, final PageModel pageModel,
	        UiSessionContext sessionContext) {
		Date today = DateUtil.getStartOfDay(new Date());
		Visit currentPatientVisit = visit;
		if (visit == null) {
			currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(),
			    false);
		}
		boolean hasActiveVisit = currentPatientVisit != null;
		boolean hasFilledSrhEnrolmentForm = false;
		pageModel.addAttribute("pin", BotswanaEmrUtils.getOpenMrsId(patient));
		BotswanaEmrUtils.setPatientIdentifierAttributes(pageModel, patient);
		pageModel.addAttribute("hasVisit", hasActiveVisit);
		pageModel.addAttribute("patient", patient);
		
		Form srhEnrolmentForm = formService.getFormByUuid(SRH_ENROLMENT_FORM_UUID);
		
		List<Encounter> filledSrhEnrolmentEncounter = encounterService.getEncounters(new EncounterSearchCriteria(patient,
		        null, today, null, null, Arrays.asList(srhEnrolmentForm), null, null, null, null, false));
		
		if (!filledSrhEnrolmentEncounter.isEmpty()) {
			hasFilledSrhEnrolmentForm = true;
		}
		
		if (currentPatientVisit != null) {
			pageModel.addAttribute("visitId", currentPatientVisit.getVisitId());
			pageModel.addAttribute("currentVisit", currentPatientVisit);
		} else {
			pageModel.addAttribute("visitId", "");
		}
		
		Set<VisitType> visitTypes = new HashSet<>();
		
		VisitType ancVisitType = visitService.getVisitTypeByUuid("bcfcd1f8-7a12-4469-9d68-691a7f8569cf");
		VisitType maternityVisitTypeByUuid = visitService.getVisitTypeByUuid("99dde0fe-8138-4c32-a6c9-2269e7877626");
		
		visitTypes.add(ancVisitType);
		visitTypes.add(maternityVisitTypeByUuid);
		
		pageModel.addAttribute("srhEnrolmentForm", srhEnrolmentForm);
		pageModel.addAttribute("hasFilledSrhEnrolmentForm", hasFilledSrhEnrolmentForm);
		pageModel.addAttribute("visitTypes", visitTypes);
		pageModel.addAttribute("returnUrl", HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", "hts/htsClientProfile",
		    patient, currentPatientVisit, ui));
	}
}
