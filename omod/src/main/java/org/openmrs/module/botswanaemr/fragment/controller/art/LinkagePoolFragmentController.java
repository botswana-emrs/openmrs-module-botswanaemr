/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.Concept;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.OrderService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.mapper.PatientsLinkagePoolMapper;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.OrderSearchCriteria;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.HIV_CARE_REFERRAL_ORDER_REASON_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;

public class LinkagePoolFragmentController {
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	public void controller(@SpringBean FragmentModel fragmentModel, UiSessionContext uiSessionContext) {
		List<Provider> providerList = Context.getProviderService().getAllProviders();
		fragmentModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		fragmentModel.addAttribute("providerList", providerList);
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		ProgramWorkflowService programWorkflowService = Context.getService(ProgramWorkflowService.class);
		Program artProgram = programWorkflowService.getProgramByUuid(BotswanaEmrConstants.ART_PROGRAM_UUID);
		fragmentModel.addAttribute("artProgramId", artProgram.getUuid());
	}
	
	public List<SimpleObject> getLinkagePool(UiUtils uiUtils, UiSessionContext uiSessionContext) {
		List<SimpleObject> simpleObjects = new ArrayList<>();
		OrderType referralOrder = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		Concept hivCareReferralConcept = conceptService.getConceptByUuid(HIV_CARE_REFERRAL_ORDER_REASON_CONCEPT_UUID);
		
		if (referralOrder != null) {
			OrderSearchCriteria orderSearchCriteria = BotswanaEmrUtils.getOrderSearchCriteria(referralOrder);
			List<Order> hivCareReferralOrders = orderService.getOrders(orderSearchCriteria).stream()
			        .filter(hivCareReferralOrder -> hivCareReferralOrder.getOrderReason().equals(hivCareReferralConcept))
			        .filter(Order::isActive).distinct().collect(Collectors.toList());
			List<PatientsLinkagePoolMapper> patientsLinkagePoolMappers = mapPatientPoolToMapper(hivCareReferralOrders,
			    uiSessionContext);
			simpleObjects = SimpleObject.fromCollection(patientsLinkagePoolMappers, uiUtils, "pin", "patientId",
			    "patientNames", "gender", "age", "facility", "status", "artEnrolled", "visitNumber");
		}
		return simpleObjects;
	}
	
	public List<PatientsLinkagePoolMapper> mapPatientPoolToMapper(List<Order> hivCareReferralOrders,
	        UiSessionContext uiSessionContext) {
		List<PatientsLinkagePoolMapper> linkagePoolMappers = new ArrayList<>();
		String openmrsId = BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME;
		for (Order order : hivCareReferralOrders) {
			boolean artEnrolled = BotswanaEmrUtils.isEnrolledInProgram(order.getPatient(),
			    BotswanaEmrConstants.ART_PROGRAM_UUID);
			String names = order.getPatient().getFamilyName() + " " + order.getPatient().getGivenName() + " "
			        + order.getPatient().getMiddleName();
			PatientsLinkagePoolMapper linkagePoolMapper = new PatientsLinkagePoolMapper();
			linkagePoolMapper.setPin(order.getPatient().getPatientIdentifier(openmrsId).getIdentifier());
			linkagePoolMapper.setPatientId(order.getPatient().getUuid());
			linkagePoolMapper.setPatientNames(names.replace("null", ""));
			linkagePoolMapper.setGender(order.getPatient().getGender());
			linkagePoolMapper.setAge(String.valueOf(order.getPatient().getAge()));
			linkagePoolMapper.setFacility(order.getEncounter().getLocation().getName());
			linkagePoolMapper.setStatus(order.getFulfillerStatus().name());
			Visit currentActiveVisit = BotswanaEmrUtils.getPatientActiveVisit(order.getPatient(),
			    uiSessionContext.getSessionLocation(), true);
			linkagePoolMapper.setVisitNumber(currentActiveVisit.getUuid());
			linkagePoolMapper.setArtEnrolled(artEnrolled);
			linkagePoolMappers.add(linkagePoolMapper);
		}
		return linkagePoolMappers;
	}
}
