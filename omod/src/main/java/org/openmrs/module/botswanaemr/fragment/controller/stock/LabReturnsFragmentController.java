/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.stock;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.model.SimplifiedLabReturns;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LabReturnsFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @SpringBean("bemrs.stockOperationDataService") IStockOperationDataService iStockOperationDataService,
	        UiSessionContext uiSessionContext) {
		
		List<SimplifiedLabReturns> simplifiedLabReturnsList = new ArrayList<>();
		List<StockOperation> stockOperations = new ArrayList<>();
		List<Stockroom> stockrooms = StockUtils.getHtsDistributionStockrooms(uiSessionContext.getSessionLocation());
		if (uiSessionContext.getCurrentUser() != null) {
			for (Stockroom stockroom : stockrooms) {
				List<StockOperation> stockroomOperations = iStockOperationDataService.getOperationsByRoom(stockroom, null);
				if (!stockroomOperations.isEmpty()) {
					stockroomOperations = stockroomOperations.stream()
					        .filter(e -> e.getInstanceType().equals(WellKnownOperationTypes.getTransfer()))
					        .collect(Collectors.toList());
					stockOperations.addAll(stockroomOperations);
				}
			}
		}
		
		for (StockOperation stockOperation : stockOperations) {
			SimplifiedLabReturns labReturns = new SimplifiedLabReturns();
			labReturns.setId(stockOperation.getId());
			labReturns.setOperationDate(
			    BotswanaEmrUtils.formatDateWithoutTime(stockOperation.getOperationDate(), "MMM dd yyyy"));
			labReturns.setOfficer(String.valueOf(stockOperation.getCreator()));
			labReturns
			        .setReason(StringUtils.isEmpty(stockOperation.getDescription()) ? "" : stockOperation.getDescription());
			labReturns.setStatus(String.valueOf(stockOperation.getStatus()));
			
			simplifiedLabReturnsList.add(labReturns);
		}
		
		fragmentModel.addAttribute("allLabReturns", simplifiedLabReturnsList);
		
	}
}
