/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.pharmacy;

import org.openmrs.Location;
import org.openmrs.Order;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.QueuePatientFragmentController;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.module.reporting.common.DurationUnit;
import org.openmrs.ui.framework.page.PageModel;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PharmacyDashboardPageController extends QueuePatientFragmentController {
	
	public void get(PageModel model, UiSessionContext sessionContext) throws ParseException {
		Date currentDate = new Date();
		Location location = sessionContext.getSessionLocation();
		
		// Get start of year minus 1 week
		Date startOfYear = DateUtil.getStartOfYear(currentDate);
		Date startDate = DateUtil.adjustDate(startOfYear, -7, DurationUnit.DAYS);
		Date endDate = DateUtil.getEndOfYear(currentDate);
		
		// Fetch all drug orders for the year
		List<Order> yearlyDrugOrders = BotswanaEmrUtils.getDrugOrders(startDate, endDate, null, location);
		
		// Calculate various date ranges
		Date startOfDayToday = DateUtil.getStartOfDay(currentDate);
		Date endOfDayToday = DateUtil.getEndOfDay(currentDate);
		Date yesterDay = DateUtil.adjustDate(currentDate, -1, DurationUnit.DAYS);
		Date startOfDayYesterday = DateUtil.getStartOfDay(yesterDay);
		Date endOfDayYesterday = DateUtil.getEndOfDay(yesterDay);
		
		// Populate model attributes using the cached yearly drug orders
		model.addAttribute("todaysPrescriptionUploaded",
		    countOrdersInDateRange(yearlyDrugOrders, startOfDayToday, endOfDayToday, null));
		model.addAttribute("dailyAveragePrescription", calculateDailyAveragePrescription(yearlyDrugOrders));
		model.addAttribute("todaysPrescriptionFilled",
		    countOrdersInDateRange(yearlyDrugOrders, startOfDayToday, endOfDayToday, Order.FulfillerStatus.COMPLETED));
		model.addAttribute("yesterdaysPrescriptionUploaded",
		    countOrdersInDateRange(yearlyDrugOrders, startOfDayYesterday, endOfDayYesterday, null));
		model.addAttribute("activeFilledOrders",
		    getOrdersInDateRange(yearlyDrugOrders, startOfDayYesterday, endOfDayYesterday, Order.FulfillerStatus.COMPLETED));
		
		// Add date-related attributes (unchanged)
		addDateAttributes(model, currentDate);
	}
	
	private int countOrdersInDateRange(List<Order> orders, Date startDate, Date endDate, Order.FulfillerStatus status) {
		return (int) orders.stream().filter(order -> isOrderInDateRange(order, startDate, endDate, status)).count();
	}
	
	private List<Order> getOrdersInDateRange(List<Order> orders, Date startDate, Date endDate,
	        Order.FulfillerStatus status) {
		return orders.stream().filter(order -> isOrderInDateRange(order, startDate, endDate, status))
		        .collect(Collectors.toList());
	}
	
	private boolean isOrderInDateRange(Order order, Date startDate, Date endDate, Order.FulfillerStatus status) {
		Date orderDate = order.getDateActivated();
		boolean inDateRange = orderDate.compareTo(startDate) >= 0 && orderDate.compareTo(endDate) <= 0;
		return inDateRange && (status == null || order.getFulfillerStatus() == status);
	}
	
	private int calculateDailyAveragePrescription(List<Order> orders) {
		if (orders.isEmpty()) {
			return 0;
		}
		
		Order firstOrder = orders.get(0);
		Order lastOrder = orders.get(orders.size() - 1);
		int daysBetween = BotswanaEmrUtils.unitsSince(lastOrder.getDateActivated(), firstOrder.getDateActivated(), "days");
		
		return daysBetween == 0 ? 0 : orders.size() / daysBetween;
	}
	
	private void addDateAttributes(PageModel model, Date currentDate) {
		model.addAttribute("startOfDay",
		    BotswanaEmrUtils.formatDateWithoutTime(DateUtil.getStartOfDay(currentDate), "dd/MM/yyyy"));
		model.addAttribute("endOfDay",
		    BotswanaEmrUtils.formatDateWithoutTime(DateUtil.getEndOfDay(currentDate), "dd/MM/yyyy"));
		model.addAttribute("startOfDayOfThisWeek", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getStartOfWeek(currentDate)), "dd/MM/yyyy"));
		model.addAttribute("endOfLastDayOfThisWeek", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getEndOfWeek(currentDate)), "dd/MM/yyyy"));
		model.addAttribute("startOfDayOfThisMonth", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getStartOfMonth(currentDate)), "dd/MM/yyyy"));
		model.addAttribute("endOfLastDayOfThisMonth", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getEndOfMonth(currentDate)), "dd/MM/yyyy"));
		model.addAttribute("startOfDayOfTheFirstMonthInThisYear", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getStartOfYear(currentDate)), "dd/MM/yyyy"));
		model.addAttribute("endOfLastDayOfTheLastMonthThisYear", BotswanaEmrUtils
		        .formatDateWithoutTime(DateUtil.getStartOfDay(DateUtil.getEndOfYear(currentDate)), "dd/MM/yyyy"));
	}
}
