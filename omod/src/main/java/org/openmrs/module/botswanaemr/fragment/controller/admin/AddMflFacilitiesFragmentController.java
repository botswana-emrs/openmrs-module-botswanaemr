/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openmrs.Location;
import org.openmrs.LocationAttribute;
import org.openmrs.LocationAttributeType;
import org.openmrs.LocationTag;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.ui.framework.SimpleObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

public class AddMflFacilitiesFragmentController {
	
	private static final String PROP_MFL_API_AUTHORIZATION_TOKEN = "botswanaemr.mflApiAuthorizationToken";
	
	private static final String PROP_MFL_API_BASE_URL = "botswanaemr.mflApiBaseUrl";
	
	protected final Logger log = LoggerFactory.getLogger(getClass());
	
	public void controller() throws IOException {
		
	}
	
	public List<SimpleObject> getMflFacilities() throws IOException {
		List<SimpleObject> facilitiesObjects = new ArrayList<>();
		OkHttpClient client = new OkHttpClient();
		
		String mflApiBaseUrl = Context.getAdministrationService().getGlobalProperty(PROP_MFL_API_BASE_URL);
		String authorizationToken = Context.getAdministrationService().getGlobalProperty(PROP_MFL_API_AUTHORIZATION_TOKEN);
		Request request = new Request.Builder().url(mflApiBaseUrl + "/mfl/facility/all").get()
		        .addHeader("Authorization", authorizationToken).addHeader("cache-control", "no-cache")
		        .addHeader("Content-Type", "application/json").build();
		
		Response response;
		try {
			response = client.newCall(request).execute();
		}
		catch (IOException e) {
			log.error("Error connecting to API: " + mflApiBaseUrl + "/mfl/facility/all", e);
			return facilitiesObjects;
		}
		ResponseBody responseBody;
		try {
			responseBody = response.body();
		}
		catch (NullPointerException e) {
			log.error("Response body not found", e);
			return facilitiesObjects;
		}
		InputStream outputStream;
		try {
			outputStream = responseBody.byteStream();
		}
		catch (NullPointerException e) {
			log.error("Input stream not found", e);
			return facilitiesObjects;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(outputStream));
		StringBuilder responseBuffer = new StringBuilder();
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			responseBuffer.append(inputLine);
		}
		in.close();
		
		String stringResponse = responseBuffer.toString();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNodes;
		try {
			jsonNodes = mapper.readTree(stringResponse);
		}
		catch (JsonProcessingException e) {
			log.error("Error processing JSON response", e);
			return facilitiesObjects;
		}
		for (JsonNode node : jsonNodes) {
			SimpleObject so = new SimpleObject();
			so.put("newFacilityCode", node.get("newFacilityCode").getTextValue());
			so.put("facilityName", node.get("facilityName").getTextValue());
			so.put("status", node.get("status").getTextValue());
			facilitiesObjects.add(so);
		}
		
		response.close();
		
		return facilitiesObjects;
	}
	
	public String post(@RequestParam(value = "mflFacilityCode") String mflFacilityCode) {
		String[] mflFacilityCodes = mflFacilityCode.split(",");
		
		for (String facility : mflFacilityCodes) {
			String facilityName = facility.split("\\|")[1];
			String facilityMflCode = facility.split("\\|")[0];
			try {
				Location location = getLocationUsingMflCode(facilityMflCode, facilityName);
				if (location != null) {
					Context.getLocationService().saveLocation(location);
				}
			}
			catch (IOException e) {
				log.debug("Error saving location", e);
			}
		}
		
		return null;
	}
	
	public Location getLocationUsingMflCode(String facilityMflCode, String facilityName) throws IOException {
		LocationService ls = Context.getLocationService();
		
		Location location;
		
		// Get location by MFL Code
		LocationAttributeType codeAttrType = null;
		codeAttrType = ls.getLocationAttributeTypeByUuid(BotswanaEmrConstants.MASTER_FACILITY_CODE_UUID);
		List<Location> matchingLocations = ls.getLocations(null, null,
		    Collections.singletonMap(codeAttrType, facilityMflCode), false, null, 1);
		if (!matchingLocations.isEmpty()) {
			location = matchingLocations.get(0);
		} else {
			location = ls.getLocation(facilityName);
		}
		
		if (location == null) {
			location = new Location();
			location.setCreator(Context.getAuthenticatedUser());
			location.setDateCreated(new Date());
			location.setName(facilityName);
		}
		
		LocationTag visitLocationTag = ls.getLocationTagByName(VISIT_LOCATION_TAG_NAME);
		if (location.getTags() == null || !location.getTags().contains(visitLocationTag)) {
			location.addTag(visitLocationTag);
		}
		
		LocationAttributeType finalCodeAttrType = codeAttrType;
		Set<LocationAttribute> locationAttributeSet = location.getAttributes();
		if (locationAttributeSet.stream().noneMatch(la -> la.getAttributeType().equals(finalCodeAttrType))) {
			setAsAttribute(location, codeAttrType, facilityMflCode);
		}
		
		LocationAttributeType statusAttributeType = ls
		        .getLocationAttributeTypeByUuid(BotswanaEmrConstants.MASTER_FACILITY_STATUS_UUID);
		if (locationAttributeSet.stream().noneMatch(la -> la.getAttributeType().equals(statusAttributeType))) {
			setAsAttribute(location, statusAttributeType, "Active");
		}
		
		return location;
		
	}
	
	protected void setAsAttribute(Location location, LocationAttributeType type, String value) {
		if (StringUtils.isNotBlank(value)) {
			LocationAttribute attr = new LocationAttribute();
			attr.setAttributeType(type);
			attr.setValue(value.trim());
			attr.setOwner(location);
			location.setAttribute(attr);
		}
	}
}
