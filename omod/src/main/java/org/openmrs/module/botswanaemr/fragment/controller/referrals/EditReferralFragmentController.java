/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.referrals;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.ConceptService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.OrderService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.RECEIVING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_DESTINATION_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_DEPARTMENT_CONCEPT_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRING_LOCATION_TAG_NAME;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.USER_PROPERTY_CURRENTLY_LOGGED_FACILITY;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

public class EditReferralFragmentController {
	
	protected final Log log = LogFactory.getLog(EditReferralFragmentController.class);
	
	private final ConceptService conceptService = Context.getService(ConceptService.class);
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	private final LocationService locationService = Context.getService(LocationService.class);
	
	private final ObsService obsService = Context.getService(ObsService.class);
	
	private final ProviderService providerService = Context.getService(ProviderService.class);
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	public void controller(FragmentModel fragmentModel, UiSessionContext uiSessionContext,
	        @RequestParam(value = "patientId", required = false) Patient patient) {
		String currentServicePointUuid = String
		        .valueOf(uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		fragmentModel.addAttribute("currentServicePointUuid", currentServicePointUuid);
		
		String hospital = Context.getAuthenticatedUser().getUserProperty(USER_PROPERTY_CURRENTLY_LOGGED_FACILITY,
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.hospital"));
		
		fragmentModel.addAttribute("hospital", hospital);
		
		Visit activeVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, uiSessionContext.getSessionLocation(), true);
		
		List<Location> referringDepartments = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(REFERRING_LOCATION_TAG_NAME));
		if (!referringDepartments.isEmpty()) {
			fragmentModel.addAttribute("referringDepartments", referringDepartments);
		} else {
			fragmentModel.addAttribute("referringDepartments", Collections.EMPTY_LIST);
		}
		
		List<Location> receivingFacilities = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		if (!receivingFacilities.isEmpty()) {
			fragmentModel.addAttribute("receivingFacilities", receivingFacilities);
		} else {
			fragmentModel.addAttribute("receivingFacilities", Collections.EMPTY_LIST);
		}
		
		List<Location> receivingLocations = locationService
		        .getLocationsByTag(locationService.getLocationTagByName(REFERRAL_DESTINATION_LOCATION_TAG_NAME));
		List<Location> receivingDepartments = new ArrayList<>();
		if (!receivingLocations.isEmpty() && !referringDepartments.isEmpty()) {
			receivingDepartments.addAll(receivingLocations);
			receivingDepartments.addAll(referringDepartments);
			fragmentModel.addAttribute("receivingDepartments", receivingDepartments);
		} else {
			fragmentModel.addAttribute("receivingDepartments", Collections.EMPTY_LIST);
		}
		BotswanaEmrUtils.setSingleActiveReferralOrder(fragmentModel, patient, activeVisit);
	}
	
	public void editPatientReferral(@RequestParam(value = "patientId", required = false) Patient patient,
	        @RequestParam(value = "referralId", required = false) String referralId,
	        @RequestParam(value = "providerId", required = false) Integer providerId,
	        @RequestParam(value = "referralEncounterUuid", required = false) String referralEncounterUuid,
	        @RequestParam(value = "referralNotes", required = false) String referralNotes,
	        @RequestParam(value = "status", required = false) String status,
	        @RequestParam(value = "referringDepartmentName") String referringDepartmentName,
	        @RequestParam(value = "receivingFacilityName") String receivingFacilityName,
	        @RequestParam(value = "receivingDepartmentName") String receivingDepartmentName,
	        @RequestParam(value = "reason") String reason) {
		
		Location referringDepartment = locationService.getLocation(referringDepartmentName);
		Location receivingDepartment = locationService.getLocation(receivingDepartmentName);
		Location receivingFacility = locationService.getLocation(receivingFacilityName);
		
		Provider provider = BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson());
		if (providerId != null) {
			provider = providerService.getProvider(providerId);
		}
		
		Concept referringDepartmentConcept = conceptService.getConceptByUuid(REFERRING_DEPARTMENT_CONCEPT_UUID);
		Concept receivingDepartmentConcept = conceptService.getConceptByUuid(RECEIVING_DEPARTMENT_CONCEPT_UUID);
		Order.FulfillerStatus referralStatus = Enum.valueOf(Order.FulfillerStatus.class, status);
		Concept reasonConcept = conceptService.getConceptByName(reason);
		
		Encounter encounter = encounterService.getEncounterByUuid(referralEncounterUuid);
		if (encounter != null) {
			Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getDateCreated);
			Integer integerReferralId = NumberUtils.createInteger(referralId);
			Order referral = orderService.getOrder(integerReferralId).cloneForRevision();
			Optional<Obs> referringDepartmentObs = getClonedObs(obsDateComparator, patient,
			    REFERRING_DEPARTMENT_CONCEPT_UUID);
			if (referringDepartmentObs.isPresent()) {
				referringDepartmentObs.get().setObsDatetime(new Date());
				referringDepartmentObs.get().setValueText(referringDepartment.getName());
				referringDepartmentObs.get().setValueCoded(referringDepartmentConcept);
				obsService.saveObs(referringDepartmentObs.get(), "Department referring");
				encounter.addObs(referringDepartmentObs.get());
			}
			
			Optional<Obs> receivingDepartmentObs = getClonedObs(obsDateComparator, patient,
			    RECEIVING_DEPARTMENT_CONCEPT_UUID);
			if (receivingDepartmentObs.isPresent()) {
				receivingDepartmentObs.get().setObsDatetime(new Date());
				receivingDepartmentObs.get().setValueText(receivingDepartment.getName());
				receivingDepartmentObs.get().setComment(receivingFacility.getName());
				receivingDepartmentObs.get().setValueCoded(receivingDepartmentConcept);
				obsService.saveObs(receivingDepartmentObs.get(), "Department receiving");
				encounter.addObs(receivingDepartmentObs.get());
			}
			encounter.setLocation(receivingFacility);
			referral.setEncounter(encounter);
			referral.setPatient(patient);
			referral.setOrderer(provider);
			if (StringUtils.isNotBlank(reason)) {
				if (reasonConcept != null) {
					referral.setOrderReason(reasonConcept);
				} else {
					referral.setOrderReasonNonCoded(reason);
				}
			}
			if (StringUtils.isNotBlank(status)) {
				referral.setFulfillerStatus(referralStatus);
			}
			if (StringUtils.isNotBlank(referralNotes)) {
				referral.setCommentToFulfiller(referralNotes);
			}
			orderService.saveOrder(referral, null);
			encounter.addOrder(referral);
			log.info("\n Created referral order \n" + referral);
		}
	}
	
	private Provider getProvider(String providerName, List<Provider> providers) {
		Provider medicalProvider = null;
		for (Provider provider : providers) {
			if (BotswanaEmrUtils.formatPersonName(provider.getPerson().getPersonName())
			        .contains(providerName.substring(0, providerName.indexOf(' ')))) {
				medicalProvider = provider;
			}
		}
		return medicalProvider;
	}
	
	private Optional<Obs> getClonedObs(Comparator<Obs> obsDateComparator, Patient patient, String obsUuid) {
		return obsService.getObservationsByPerson(patient).stream().filter(obs -> obs.getConcept().getUuid().equals(obsUuid))
		        .max(obsDateComparator);
	}
}
