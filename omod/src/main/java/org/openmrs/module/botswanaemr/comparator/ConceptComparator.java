/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.comparator;

import java.util.Comparator;

import org.openmrs.Concept;

/**
 * Sorts allergies
 */
public class ConceptComparator implements Comparator<Concept> {
	
	private Comparator comparator;
	
	public ConceptComparator(Comparator comparator) {
		this.comparator = comparator;
	}
	
	@Override
	public int compare(Concept concept1, Concept concept2) {
		Object obj1 = concept1.getName();
		
		Object obj2 = concept2.getName();
		
		//sort non coded allergen at the bottom
		if (obj1 instanceof String && obj2 instanceof String) {
			return obj1.toString().compareToIgnoreCase(obj2.toString());
		}
		
		return comparator.compare(obj1, obj2);
	}
}
