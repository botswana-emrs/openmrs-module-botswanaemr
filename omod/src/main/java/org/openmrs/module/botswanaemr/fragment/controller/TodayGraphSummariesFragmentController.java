/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

public class TodayGraphSummariesFragmentController {
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	private final HashMap<String, List<Registration>> registeredPatients = new HashMap<>();
	
	private final HashMap<String, List<Encounter>> screenedPatients = new HashMap<>();
	
	private final String format = "yyyy-MM-dd";
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	public void controller(FragmentModel model,
	        @FragmentParam(value = "type", required = false) BotswanaEmrConstants.summaryType typeOfSummary,
	        @FragmentParam(value = "encounterType", required = false) String encounterTypeUuid,
	        UiSessionContext uiSessionContext) {
		EncounterType encounterType = encounterService.getEncounterTypeByUuid(encounterTypeUuid);
		Location sessionLocation = uiSessionContext.getSessionLocation();
		if (typeOfSummary == null) {
			typeOfSummary = BotswanaEmrConstants.summaryType.REGISTRATION;
		}
		BotswanaEmrConstants.summaryType selectedGraphSummaryType;
		switch (typeOfSummary) {
			case SCREENING:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.SCREENING;
				break;
			case PHARMACY:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.PHARMACY;
				break;
			case PROGRAM:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.PROGRAM;
				break;
			case CONSULTATION:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.CONSULTATION;
				break;
			case ENROLLMENT:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.ENROLLMENT;
				break;
			default:
				selectedGraphSummaryType = BotswanaEmrConstants.summaryType.REGISTRATION;
				break;
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.REGISTRATION)) {
			getPatientsSeenPerHourGiven(new Date(), sessionLocation, model);
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.ENROLLMENT)) {
			Location location = null;
			if (StringUtils.isNotEmpty(encounterTypeUuid)) {
				location = Context.getLocationService().getLocationByUuid(encounterTypeUuid);
			} else {
				location = Context.getLocationService()
				        .getLocationByUuid(BotswanaEmrConstants.SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
			}
			
			Location currentLocation = uiSessionContext.getSessionLocation();
			
			getPatientsEnrolledLocationHourGiven(new Date(), location, currentLocation, model);
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.SCREENING)) {
			for (int i = 0; i < 24; i++) {
				String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
				try {
					model.addAttribute("t" + hourString,
					    getScreeningsDonePerHourGiven(new Date(), hourString, encounterType, sessionLocation));
				}
				catch (ParseException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.CONSULTATION)) {
			List<Encounter> consultationEncountersList = BotswanaEmrUtils.getAllConsultations(new Date(), new Date(), null,
			    sessionLocation);
			if (consultationEncountersList != null) {
				for (int i = 0; i < 24; i++) {
					int finalI = i;
					int size = (int) consultationEncountersList.stream()
					        .filter(
					            consultation -> new DateTime(consultation.getEncounterDatetime()).getHourOfDay() == finalI)
					        .count();
					String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
					model.addAttribute("t" + hourString, size);
				}
				model.addAttribute("graphedPatientConsultations", consultationEncountersList);
			} else {
				for (int i = 0; i < 24; i++) {
					String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
					model.addAttribute("t" + hourString, 0);
				}
			}
		}
		
		if (selectedGraphSummaryType.equals(BotswanaEmrConstants.summaryType.PROGRAM)) {
			List<PatientProgram> patientPrograms = new ArrayList<>();
			Program program = Context.getService(ProgramWorkflowService.class)
			        .getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID);
			if (program != null) {
				patientPrograms = Context.getService(BotswanaEmrService.class).getPatientProgramByProgram(program);
			}
			
			for (int i = 0; i < 24; i++) {
				int finalI = i;
				int size = (int) patientPrograms.stream()
				        .filter(p -> new DateTime(p.getDateEnrolled()).getHourOfDay() == finalI).count();
				String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
				model.addAttribute("t" + hourString, size);
			}
			model.addAttribute("patientPrograms", patientPrograms);
		}
	}
	
	private Integer getScreeningsDonePerHourGiven(Date date, String hour, EncounterType encounterType, Location location)
	        throws ParseException {
		String startFromDateTime = getStartFromDateTime(date, hour);
		String endToDateTime = getEndToDateTime(date, hour);
		
		String dateTimeFormat = "yyyy-MM-dd hh:mm:ss";
		Date startTime = BotswanaEmrUtils.formatDateFromString(startFromDateTime, dateTimeFormat);
		Date endTime = BotswanaEmrUtils.formatDateFromString(endToDateTime, dateTimeFormat);
		
		int value = 0;
		
		List<Encounter> encountersList = getScreeningsDoneOnDate(date, encounterType, location);
		if (encountersList != null) {
			value = (int) encountersList.stream().filter(
			    p -> p.getEncounterDatetime().compareTo(startTime) >= 0 && p.getEncounterDatetime().compareTo(endTime) <= 0)
			        .count();
		}
		
		return value;
	}
	
	private List<Encounter> getScreeningsDoneOnDate(Date date, EncounterType encounterType, Location location) {
		String dateString = BotswanaEmrUtils.formatDateWithoutTime(date, format);
		if (!screenedPatients.containsKey(dateString)) {
			List<Encounter> encountersList = Context.getService(BotswanaEmrService.class).getScreeningsDoneOnDate(date, date,
			    encounterType, 1000, location);
			if (encountersList != null && !encountersList.isEmpty()) {
				encountersList = encountersList.stream().filter(e -> e.getForm() != null).collect(Collectors.toList());
			}
			screenedPatients.put(dateString, encountersList);
		}
		
		return screenedPatients.get(dateString);
	}
	
	private void getPatientsSeenPerHourGiven(Date date, Location location, FragmentModel model) {
		List<Registration> paymentList = getPatientsSeenOnDate(date, location);
		for (int i = 0; i < 24; i++) {
			int finalI = i;
			String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
			int size = 0;
			if (paymentList != null) {
				size = (int) paymentList.stream()
				        .filter(payment -> new DateTime(payment.getDateCreated()).getHourOfDay() == finalI).count();
			}
			model.addAttribute("t" + hourString, size);
		}
	}
	
	private void getPatientsEnrolledLocationHourGiven(Date date, Location location, Location currentLocation,
	        FragmentModel model) {
		List<PatientQueue> patientQueueList = new ArrayList<>();
		patientQueueList = patientQueueingService.getPatientQueueList(null, fromDate(date), toDate(date), location,
		    currentLocation, null, null);
		
		for (int i = 0; i < 24; i++) {
			int finalI = i;
			String hourString = StringUtils.leftPad(String.valueOf(i), 2, "0");
			int size = 0;
			if (patientQueueList != null) {
				size = (int) patientQueueList.stream()
				        .filter(queue -> new DateTime(queue.getDateCreated()).getHourOfDay() == finalI).count();
			}
			model.addAttribute("t" + hourString, size);
		}
	}
	
	private Integer getPatientsSeenPerHourGiven(Date date, String hour, Location location) throws ParseException {
		
		String startFromDateTime = getStartFromDateTime(date, hour);
		String endToDateTime = getEndToDateTime(date, hour);
		
		Date startTime = BotswanaEmrUtils.formatDateFromString(startFromDateTime, format);
		Date endTime = BotswanaEmrUtils.formatDateFromString(endToDateTime, format);
		
		int value = 0;
		List<Registration> paymentList = getPatientsSeenOnDate(date, location);
		if (paymentList != null) {
			value = (int) paymentList.stream()
			        .filter(p -> p.getDateCreated().compareTo(startTime) >= 0 && p.getDateCreated().compareTo(endTime) <= 0)
			        .count();
			
		}
		return value;
	}
	
	private String getEndToDateTime(Date date, String hour) {
		return BotswanaEmrUtils.formatDateWithoutTime(date, format) + " " + hour + ":59:59";
	}
	
	private String getStartFromDateTime(Date date, String hour) {
		return BotswanaEmrUtils.formatDateWithoutTime(date, format) + " " + hour + ":00:00";
	}
	
	private List<Registration> getPatientsSeenOnDate(Date date, Location location) {
		String dateString = BotswanaEmrUtils.formatDateWithoutTime(date, format);
		
		if (!registeredPatients.containsKey(dateString)) {
			List<Registration> paymentList = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(date,
			    date, location, 1000);
			registeredPatients.put(dateString, paymentList);
		}
		
		return registeredPatients.get(dateString);
	}
	
	private Date fromDate(Date startDate) {
		return new DateTime(startDate).withTimeAtStartOfDay().toDate();
	}
	
	private Date toDate(Date endDate) {
		return new DateTime(endDate).withTime(23, 59, 59, 999).toDate();
	}
}
