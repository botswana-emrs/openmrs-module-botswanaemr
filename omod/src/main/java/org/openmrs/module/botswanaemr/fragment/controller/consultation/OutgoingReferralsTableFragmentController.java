/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.api.OrderService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.OrderSearchCriteria;
import org.openmrs.parameter.OrderSearchCriteriaBuilder;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.REFERRAL_ORDER_TYPE_UUID;

public class OutgoingReferralsTableFragmentController {
	
	public void controller(FragmentModel fragmentModel, UiSessionContext sessionContext,
	        @SpringBean("orderService") OrderService orderService) {
		fragmentModel.addAttribute("openmrsId", BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME);
		OrderType referralOrderType = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		OrderSearchCriteria orderSearchCriteria = getOrderSearchCriteriaForPendingReferrals(referralOrderType);
		List<Order> referrals = orderService.getOrders(orderSearchCriteria);
		
		// Filter referrals targeted to the current facility
		List<Order> myReferrals = new ArrayList<>();
		for (Order order : referrals) {
			if (order.getEncounter() != null) {
				if (order.getEncounter().getObs() != null) {
					if (order.getEncounter().getObs().stream().anyMatch(
					    obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.REFERRING_DEPARTMENT_CONCEPT_UUID)
					            && obs.getComment().equals(sessionContext.getSessionLocation().getName()))) {
						myReferrals.add(order);
					}
				}
			}
		}
		
		if (!referrals.isEmpty()) {
			fragmentModel.addAttribute("outgoingReferral", myReferrals);
		} else {
			fragmentModel.addAttribute("outgoingReferral", "");
		}
	}
	
	public static OrderSearchCriteria getOrderSearchCriteriaForPendingReferrals(OrderType orderType) {
		OrderSearchCriteriaBuilder orderSearchCriteriaBuilder = new OrderSearchCriteriaBuilder();
		return orderSearchCriteriaBuilder.setOrderTypes(Collections.singletonList(orderType))
		        .setActivatedOnOrAfterDate(BotswanaEmrUtils.getDateToday())
		        .setFulfillerStatus(Order.FulfillerStatus.RECEIVED).build();
	}
}
