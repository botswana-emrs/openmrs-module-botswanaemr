/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.http.HttpStatus;
import org.openmrs.User;
import org.openmrs.api.APIAuthenticationException;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.webservices.rest.SimpleObject;
import org.springframework.web.bind.annotation.RequestParam;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.FORCE_PASSWORD_CHANGE;

public class ForceChangePasswordFragmentController {
	
	public Object changePassword(@RequestParam("newPassword") String newPassword,
	        @RequestParam("oldPassword") String oldPassword) throws Exception {
		if (!Context.isAuthenticated()) {
			throw new APIAuthenticationException("Must be authenticated to change your own password");
		} else {
			SimpleObject simpleObject = new SimpleObject();
			try {
				User user = Context.getAuthenticatedUser();
				user.setUserProperty(FORCE_PASSWORD_CHANGE, Boolean.FALSE.toString());
				Context.getUserService().saveUser(user);
				Context.getUserService().changePassword(user, oldPassword, newPassword);
				simpleObject.put("status", HttpStatus.SC_OK);
				simpleObject.put("response", "Success");
			}
			catch (APIException ex) {
				simpleObject.put("status", HttpStatus.SC_INTERNAL_SERVER_ERROR);
				simpleObject.put("response", "Failed: " + ex.getMessage());
			}
			return simpleObject;
		}
	}
}
