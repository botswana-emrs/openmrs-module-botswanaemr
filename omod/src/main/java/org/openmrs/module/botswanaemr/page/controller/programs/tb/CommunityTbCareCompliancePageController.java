/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.programs.tb;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.NOK_FULLNAME_ATTRIBUTE_TYPE_UUID;

@Slf4j
public class CommunityTbCareCompliancePageController {
	
	public void controller(@RequestParam("patientId") Patient patient, PageModel pageModel,
	        UiSessionContext sessionContext) {
		
		pageModel.addAttribute("communityTbCareCompliance", "");
		
		PersonAttributeType nokAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(NOK_FULLNAME_ATTRIBUTE_TYPE_UUID);
		//PersonAttributeType nationalIdNumber = Context.getPersonService().getPersonAttributeTypeByUuid(USER_NATIONAL_ID_ATTRIBUTE_TYPE_UUID);
		
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("nokName", getPatientAttributeValue(patient, nokAttributeType));
		pageModel.addAttribute("IdNumber", BotswanaEmrUtils.getOpenMrsId(patient));
		
		StringBuilder address = new StringBuilder();
		if (patient.getPersonAddress() != null) {
			String cityVillage = patient.getPersonAddress().getCityVillage();
			String countyDistrict = patient.getPersonAddress().getCountyDistrict();
			String stateProvince = patient.getPersonAddress().getStateProvince();
			String country = patient.getPersonAddress().getCountry();
			
			address.append(cityVillage != null ? cityVillage : "").append(" ")
			        .append(countyDistrict != null ? countyDistrict : "").append(" ")
			        .append(stateProvince != null ? stateProvince : "").append(" ").append(country != null ? country : "");
		}
		pageModel.addAttribute("address", address.toString());
		
	}
	
	private boolean isCommunityCareEnrolled(@NotNull Patient patient) {
		//163777AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		return false;
	}
	
	private String getPatientAttributeValue(@NotNull Patient patient, @NotNull PersonAttributeType attributeType) {
		List<PersonAttribute> attributes = patient.getAttributes(attributeType);
		List<String> attributesList = new ArrayList<>();
		attributes.forEach(att -> attributesList.add(att.getValue()));
		
		return attributesList.isEmpty() ? "" : attributesList.get(0).replaceAll("null", "");
	}
}
