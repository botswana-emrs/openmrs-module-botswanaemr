/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.jetbrains.annotations.NotNull;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestParam;

public class EndConsultationFragmentController {
	
	@Autowired
	private AdtService adtService;
	
	private PatientQueueingService patientQueueingService;
	
	public void controller(FragmentModel fragmentModel, @RequestParam("patientId") Patient patient,
	        @RequestParam(required = false, value = "patientQueueId") PatientQueue patientQueue,
	        @RequestParam(required = false, value = "referralId") Order referral, @RequestParam("visitId") Visit visit) {
		fragmentModel.addAttribute("name", patient.getPersonName().getFullName());
		fragmentModel.addAttribute("patientId", patient.getId());
		fragmentModel.addAttribute("referralId", referral == null ? null : referral.getOrderId());
		
		fragmentModel.addAttribute("patientQueueId", patientQueue == null ? "" : patientQueue.getId());
		if (visit != null) {
			fragmentModel.addAttribute("visitId", visit.getId());
		} else {
			fragmentModel.addAttribute("visitId", null);
		}
		
	}
	
	public Object closeConsultation(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "patientQueueId", required = false) PatientQueue patientQueue,
	        @RequestParam(required = false, value = "referralId") Order referral, @RequestParam("visitId") Visit visit) {
		patientQueueingService = Context.getService(PatientQueueingService.class);
		
		if (patientQueue == null) {
			patientQueue = patientQueueingService.getMostRecentQueue(patient);
		}
		
		SimpleObject simpleObject = close(patientQueue, visit, referral);
		
		return simpleObject;
		
		// return "redirect:" + uiUtils.pageLink("botswanaemr", "consultation/doctorsPatientPoolDashboard");
		
	}
	
	private SimpleObject close(PatientQueue patientQueue, Visit visit, Order referral) {
		SimpleObject simpleObject = new SimpleObject();
		patientQueueingService = Context.getService(PatientQueueingService.class);
		try {
			Order revisedOrder;
			// PatientQueue patientQueue = patientQueueingService.getIncompletePatientQueue(patient,uiSessionContext.getSessionLocation());
			if (patientQueue != null) {
				patientQueueingService.completePatientQueue(patientQueue);
			}
			// Visit visit = adtService.getActiveVisit(patient, uiSessionContext.getSessionLocation()).getVisit();
			//			adtService.closeAndSaveVisit(visit);
			if (referral != null) {
				//revisedOrder = referral.cloneForRevision();
				//revisedOrder.setAutoExpireDate(new Date());
				Context.getOrderService().updateOrderFulfillerStatus(referral, Order.FulfillerStatus.COMPLETED,
				    "Referral completed");
				//Context.getOrderService().saveOrder(referral, null);
			}
			simpleObject.put("status", HttpStatus.SC_OK);
			simpleObject.put("response", "Success");
			
		}
		catch (Exception ex) {
			simpleObject.put("status", HttpStatus.SC_INTERNAL_SERVER_ERROR);
			simpleObject.put("response", "Failed: " + ex.getMessage());
		}
		return simpleObject;
	}
	
}
