/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.MedicalHistorySimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDurationSinceRegistration;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatEncounterTypeText;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatGender;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

public class PatientMedicalHistoryFragmentController {
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl, PageModel model, UiUtils ui,
	        @SpringBean("encounterService") EncounterService encounterService) {
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		List<MedicalHistorySimplifier> simplifiedMedicalHistory = BotswanaEmrUtils
		        .simplifiedMedicalHistory(BotswanaEmrUtils.getConsultationPatientEncounters(patient, limitFetch));
		
		model.addAttribute("patient", patient);
		model.addAttribute("hasGetEncountersPrivilege",
		    Context.getAuthenticatedUser().hasPrivilege(BotswanaEmrConstants.PRIVILEGE_GET_ENCOUNTERS));
		model.addAttribute("encounters", simplifiedMedicalHistory);
		
		if (StringUtils.isBlank(returnUrl)) {
			returnUrl = ui.pageLink("botswanaemr", "patientProfile",
			    Collections.singletonMap("patientId", (Object) patient.getUuid()));
		}
		model.addAttribute("returnUrl", returnUrl);
	}
}
