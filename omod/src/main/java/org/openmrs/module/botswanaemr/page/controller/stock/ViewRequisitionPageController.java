/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.stock;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME;

public class ViewRequisitionPageController {
	
	public void controller(PageModel model, @RequestParam("id") StockOperation stockOperation,
	        @RequestParam(value = "requestType", required = false) String requestType,
	        @RequestParam(value = "action", required = false) String action,
	        @SpringBean("locationService") LocationService locationService,
	        @SpringBean("bemrs.stockroomDataService") IStockroomDataService iStockroomDataService,
	        UiSessionContext uiSessionContext) {
		Location externalLocation = stockOperation.getSource().getLocation();
		List<Location> allLocations = Context.getLocationService()
		        .getLocationsByTag(locationService.getLocationTagByName(VISIT_LOCATION_TAG_NAME));
		
		allLocations.remove(uiSessionContext.getSessionLocation());
		allLocations.remove(externalLocation);
		
		List<Stockroom> sourceStockrooms = new ArrayList<>();
		List<Stockroom> destinationStockrooms = new ArrayList<>();
		
		if (requestType.equals("external")) {
			sourceStockrooms.add(StockUtils.getMainStockroom(stockOperation.getSource().getLocation()));
			destinationStockrooms.add(StockUtils.getMainStockroom(uiSessionContext.getSessionLocation()));
		} else {
			sourceStockrooms.add(StockUtils.getMainStockroom(uiSessionContext.getSessionLocation()));
			destinationStockrooms.addAll(StockUtils.getDispensingStockrooms(uiSessionContext.getSessionLocation()));
		}
		sourceStockrooms.remove(stockOperation.getSource());
		destinationStockrooms.remove(stockOperation.getDestination());
		
		model.addAttribute("requestType", requestType);
		model.addAttribute("action", action);
		model.addAttribute("externalLocation", externalLocation);
		model.addAttribute("locations", allLocations);
		model.addAttribute("stockrooms", sourceStockrooms);
		model.addAttribute("destinationStockrooms", destinationStockrooms);
		model.addAttribute("items", stockOperation.getItems());
		model.addAttribute("stockOperation", stockOperation);
		
	}
}
