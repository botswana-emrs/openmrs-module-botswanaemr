/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.api.FormService;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.ui.framework.resource.ResourceFactory;
import org.springframework.web.bind.annotation.RequestParam;

public class EditEncounterPageController {
	
	public void get(@RequestParam("encounterId") Encounter encounter, @RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "returnProvider", required = false) String returnProvider,
	        @RequestParam(value = "returnPage", required = false) String returnPage,
	        @RequestParam(value = "returnLabel", required = false) String returnLabel,
	        @RequestParam(value = "breadcrumbOverride", required = false) String breadcrumbOverride,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService,
	        @RequestParam(value = "definitionUiResource", required = false) String definitionUiResource,
	        @SpringBean("coreResourceFactory") ResourceFactory resourceFactory, UiUtils ui, PageModel pageModel)
	        throws Exception {
		
		if (!encounter.getPatient().equals(patient)) {
			throw new IllegalArgumentException("encounter.patient != patient");
		}
		
		HtmlForm htmlForm = null;
		
		if (StringUtils.isNotBlank(definitionUiResource)) {
			htmlForm = HtmlFormUtil.getHtmlFormFromUiResource(resourceFactory, formService, htmlFormEntryService,
			    definitionUiResource, encounter.toString());
			if (htmlForm == null) {
				throw new IllegalArgumentException("No form found for resource " + definitionUiResource);
			}
		} else {
			if (encounter.getForm() == null) {
				throw new IllegalArgumentException("encounter.form is null");
			}
			htmlForm = htmlFormEntryService.getHtmlFormByForm(encounter.getForm());
		}
		if (htmlForm == null) {
			throw new IllegalArgumentException("encounter.form is not an HTML Form: " + encounter.getForm());
		}
		returnUrl = ui.encodeForSafeURL(
		    HtmlFormUtil.determineReturnUrl(returnUrl, returnProvider, returnPage, patient, encounter.getVisit(), ui));
		returnLabel = HtmlFormUtil.determineReturnLabel(returnLabel, patient, ui);
		pageModel.addAttribute("encounter", encounter);
		pageModel.addAttribute("patient", patient);
		pageModel.addAttribute("htmlForm", htmlForm);
		pageModel.addAttribute("returnUrl", returnUrl);
		pageModel.addAttribute("returnLabel", returnLabel);
		pageModel.addAttribute("definitionUiResource", definitionUiResource);
		pageModel.addAttribute("breadcrumbOverride", breadcrumbOverride);
	}
}
