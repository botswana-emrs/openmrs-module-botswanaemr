/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.simplifier;

import org.openmrs.ui.framework.SimpleObject;
import org.springframework.core.convert.converter.Converter;

/**
 * Abstract converter to a simple object
 */
public abstract class BotswanaEmrAbstractSimplifier<T> implements Converter<T, SimpleObject> {
	
	/**
	 * @see org.springframework.core.convert.converter.Converter#convert(Object)
	 */
	@Override
	public SimpleObject convert(T obj) {
		return simplify(obj);
	}
	
	/**
	 * Simplify the given object
	 * 
	 * @param obj the object
	 * @return the simple object
	 */
	protected abstract SimpleObject simplify(T obj);
}
