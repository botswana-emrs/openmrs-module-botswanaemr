/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.hts;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.model.hts.DtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedDtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedHtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedHtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedLabQualityControlTest;
import org.openmrs.module.botswanaemr.model.hts.SpecimenResultMapper;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class ProficiencyTestingFormFragmentController {
	
	public static final String HIV_TEST_RESULTS_FORM_UUID = "0a876084-d640-4e36-9924-1f961ad75f60";
	
	public static final String DEFINITION_UI_RESOURCE = "botswanaemr:htmlforms/hts-proficiency-testing-hiv-results-form.xml";
	
	private final HtsProficiencyTestingService htsProficiencyTestingService = Context
	        .getService(HtsProficiencyTestingService.class);
	
	IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
	
	IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
	
	IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
	
	IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
	
	public void controller(@RequestParam(value = "proficiencyTestingId", required = false) Integer proficiencyTestingId,
	        FragmentModel fragmentModel, UiSessionContext uiSessionContext, UiUtils ui) {
		fragmentModel.addAttribute("proficiencyTestingId", proficiencyTestingId);
		fragmentModel.addAttribute("formUuid", HIV_TEST_RESULTS_FORM_UUID);
		fragmentModel.addAttribute("definitionUiResource", DEFINITION_UI_RESOURCE);
		fragmentModel.addAttribute("returnUrl", ui.pageLink("botswanaemr", "hts/proficiencyTestsForm"));
		fragmentModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
		
		HashMap<String, String> kitNames = new LinkedHashMap<>();
		kitNames.put("Determine", "c2ce43cc-0345-40c0-91b2-24155f458d4e");
		kitNames.put("Bioline", "166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitNames.put("KHB", "c53f44de-5df2-4810-b99b-7f75e80d1a05");
		kitNames.put("ICT", "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		HashMap<String, String> secondTestKitNames = new LinkedHashMap<>();
		secondTestKitNames.put("Unigold", "c2acb8a1-32f6-456a-b322-d31a1d478c01");
		secondTestKitNames.put("1st Response", "570a1f68-0bea-4980-ba9d-d0d7d21e4be9");
		secondTestKitNames.put("Bio KIt", "c203885b-7bae-4318-8bb8-4aee96f973cd");
		secondTestKitNames.put("Immunoflow", "7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f");
		
		HashMap<String, String> kitResults = new LinkedHashMap<>();
		kitResults.put("Reactive", "1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitResults.put("Non Reactive", "1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		kitResults.put("Invalid", "163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		HashMap<String, String> hivStatus = new LinkedHashMap<>();
		hivStatus.put("Negative", "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		hivStatus.put("Positive", "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		hivStatus.put("Indeterminate", "1138AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		
		fragmentModel.addAttribute("kitNames", kitNames);
		fragmentModel.addAttribute("kitResults", kitResults);
		fragmentModel.addAttribute("hivStatus", hivStatus);
		fragmentModel.addAttribute("secondTestKitNames", secondTestKitNames);
		
		List<SimplifiedHtsSpecimenResult> specimenResults = new ArrayList<>();
		JsonArray result = new JsonArray();
		JsonArray htsTestingTests = new JsonArray();
		JsonArray simplifiedHtsProficiencyTestingBuffers = new JsonArray();
		SimpleObject htsProficiencyTestingObject = new SimpleObject();
		if (proficiencyTestingId != null) {
			HtsProficiencyTesting htsProficiencyTesting = htsProficiencyTestingService
			        .getHtsProficiencyTesting(proficiencyTestingId);
			List<HtsProficiencyTestingTest> htsProficiencyTestingTests = htsProficiencyTesting.getHtsProficiencyTestingTest()
			        .stream().sorted(Comparator.comparing(HtsProficiencyTestingTest::getId)).collect(Collectors.toList());
			htsTestingTests = (JsonArray) new Gson().toJsonTree(
			    SpecimenResultMapper.getSimplifiedProficiencyTestingTests(htsProficiencyTestingTests),
			    new TypeToken<List<SimplifiedHtsProficiencyTestingTest>>() {}.getType());
			specimenResults = SpecimenResultMapper.simplifySpecimenResults(htsProficiencyTestingTests);
			
			result = (JsonArray) new Gson().toJsonTree(specimenResults,
			    new TypeToken<List<SimplifiedHtsSpecimenResult>>() {}.getType());
			
			simplifiedHtsProficiencyTestingBuffers = (JsonArray) new Gson().toJsonTree(
			    getSimplifiedProficiencyTestingBuffers(htsProficiencyTesting.getDtsProficiencyTestingBuffer()),
			    new TypeToken<List<SimplifiedDtsProficiencyTestingBuffer>>() {}.getType());
			
			htsProficiencyTestingObject = SimpleObject.fromObject(
			    BotswanaEmrUtils.getSimplifiedProficiencyTesting(htsProficiencyTesting), ui, "proficiencyTestingId",
			    "panelId", "receivedBy", "receivedByName", "datePanelReceived", "testingPoint", "resultsDueDate",
			    "dateTested", "resultsReceivedBy", "resultsReceivedByName", "performanceScore", "percentageScore", "comment",
			    "reportReviewedBy", "reportReviewedByName", "dateReviewed", "published");
		}
		fragmentModel.addAttribute("htsProficiencyTesting", htsProficiencyTestingObject);
		fragmentModel.addAttribute("result", result);
		fragmentModel.addAttribute("htsTestingTests", htsTestingTests);
		fragmentModel.addAttribute("simplifiedHtsProficiencyTestingBuffers", simplifiedHtsProficiencyTestingBuffers);
	}
	
	private List<SimplifiedHtsProficiencyTestingTest> getSimplifiedProficiencyTestingTests(
	        List<HtsProficiencyTestingTest> htsProficiencyTestingTests) {
		List<SimplifiedHtsProficiencyTestingTest> simplifiedHtsProficiencyTestingTests = new ArrayList<>();
		for (HtsProficiencyTestingTest htsProficiencyTestingTest : htsProficiencyTestingTests) {
			SimplifiedHtsProficiencyTestingTest simplifiedHtsProficiencyTestingTest = new SimplifiedHtsProficiencyTestingTest();
			simplifiedHtsProficiencyTestingTest.setProficiencyTestingTestId(htsProficiencyTestingTest.getId());
			simplifiedHtsProficiencyTestingTest.setId(htsProficiencyTestingTest.getId());
			simplifiedHtsProficiencyTestingTest.setSpecimenId(htsProficiencyTestingTest.getSpecimenId());
			simplifiedHtsProficiencyTestingTest.setLotNumber(htsProficiencyTestingTest.getLotNumber());
			if (htsProficiencyTestingTest.getExpiryDate() != null) {
				simplifiedHtsProficiencyTestingTest.setExpiryDate(
				    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTestingTest.getExpiryDate(), "yyyy-MM-dd"));
			}
			simplifiedHtsProficiencyTestingTest.setConditionOfSpecimen(htsProficiencyTestingTest.getConditionOfSpecimen());
			simplifiedHtsProficiencyTestingTest
			        .setReasonForUnacceptableSpecimen(htsProficiencyTestingTest.getReasonForUnacceptableSpecimen());
			simplifiedHtsProficiencyTestingTest.setCorrectiveActions(htsProficiencyTestingTest.getCorrectiveActions());
			simplifiedHtsProficiencyTestingTests.add(simplifiedHtsProficiencyTestingTest);
		}
		return simplifiedHtsProficiencyTestingTests;
	}
	
	private List<SimplifiedDtsProficiencyTestingBuffer> getSimplifiedProficiencyTestingBuffers(
	        Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffers) {
		List<SimplifiedDtsProficiencyTestingBuffer> simplifiedDtsProficiencyTestingBuffers = new ArrayList<>();
		for (DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer : dtsProficiencyTestingBuffers) {
			SimplifiedDtsProficiencyTestingBuffer simplifiedDtsProficiencyTestingBuffer = new SimplifiedDtsProficiencyTestingBuffer();
			simplifiedDtsProficiencyTestingBuffer.setProficiencyTestingBufferId(dtsProficiencyTestingBuffer.getId());
			simplifiedDtsProficiencyTestingBuffer.setDtsBufferId(dtsProficiencyTestingBuffer.getDtsBufferId());
			simplifiedDtsProficiencyTestingBuffer.setDtsLotNumber(dtsProficiencyTestingBuffer.getDtsLotNumber());
			if (dtsProficiencyTestingBuffer.getDtsExpiryDate() != null) {
				simplifiedDtsProficiencyTestingBuffer.setDtsExpiryDate(
				    BotswanaEmrUtils.formatDateWithoutTime(dtsProficiencyTestingBuffer.getDtsExpiryDate(), "yyyy-MM-dd"));
			}
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsConditionOfBuffer(dtsProficiencyTestingBuffer.getDtsConditionOfSpecimen());
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsReasonForUnacceptableBuffer(dtsProficiencyTestingBuffer.getDtsReasonForUnacceptableSpecimen());
			simplifiedDtsProficiencyTestingBuffer
			        .setDtsCorrectiveActions(dtsProficiencyTestingBuffer.getDtsCorrectiveActions());
			simplifiedDtsProficiencyTestingBuffers.add(simplifiedDtsProficiencyTestingBuffer);
		}
		return simplifiedDtsProficiencyTestingBuffers;
	}
	
	public void saveProficiencyTestingForm(
	        @RequestParam(value = "proficiencyTestingId", required = false) String proficiencyTestingId,
	        @RequestParam(value = "panelId", required = false) String panelId,
	        @RequestParam(value = "datePanelReceived", required = false) String datePanelReceived,
	        @RequestParam(value = "receivedBy", required = false) String receivedBy,
	        @RequestParam(value = "dateResultsDue", required = false) String dateResultsDue,
	        @RequestParam(value = "testingPoint", required = false) String testingPoint,
	        @RequestParam(value = "published", required = false, defaultValue = "false") boolean published,
	        @RequestParam(value = "specimenData", required = false) String specimenData,
	        @RequestParam(value = "dtsBufferData", required = false) String dtsBufferData,
	        @RequestParam(value = "stockRoomUuid", required = false) String stockRoomUuid,
	        @RequestParam(value = "reportData", required = false) String reportData, UiSessionContext uiSessionContext)
	        throws ParseException, IOException {
		HtsProficiencyTesting htsProficiencyTesting = new HtsProficiencyTesting();
		if (StringUtils.isNotEmpty(proficiencyTestingId)) {
			htsProficiencyTesting = htsProficiencyTestingService
			        .getHtsProficiencyTesting(Integer.parseInt(proficiencyTestingId));
		}
		User authenticatedUser = Context.getAuthenticatedUser();
		
		if (StringUtils.isNotBlank(panelId))
			htsProficiencyTesting.setPanelId(panelId);
		if (StringUtils.isNotBlank(datePanelReceived))
			htsProficiencyTesting
			        .setDatePanelReceived(BotswanaEmrUtils.formatDateFromString(datePanelReceived, "yyyy-MM-dd"));
		if (StringUtils.isNotEmpty(receivedBy))
			htsProficiencyTesting.setReceivedBy(Context.getPersonService().getPersonByUuid(receivedBy));
		if (StringUtils.isNotBlank(dateResultsDue))
			htsProficiencyTesting.setResultsDueDate(BotswanaEmrUtils.formatDateFromString(dateResultsDue, "yyyy-MM-dd"));
		if (StringUtils.isNotBlank(testingPoint))
			htsProficiencyTesting.setTestingPoint(testingPoint);
		htsProficiencyTesting.setCreator(authenticatedUser);
		htsProficiencyTesting.setPublished(published);
		htsProficiencyTesting.setDateCreated(new Date());
		
		Set<HtsProficiencyTestingTest> proficiencyTestingTests = new HashSet<>();
		ObjectMapper mapper = new ObjectMapper();
		if (StringUtils.isNotBlank(specimenData)) {
			JsonNode jsonNode = mapper.readTree(specimenData);
			List<SimpleObject> ptSpecimens = mapper.readValue(jsonNode, new TypeReference<List<SimpleObject>>() {});
			proficiencyTestingTests = getPtSpecimens(ptSpecimens);
		}
		if (!proficiencyTestingTests.isEmpty()) {
			htsProficiencyTesting.setHtsProficiencyTestingTest(proficiencyTestingTests);
		}
		Set<DtsProficiencyTestingBuffer> proficiencyTestingBuffers = new HashSet<>();
		if (StringUtils.isNotBlank(dtsBufferData)) {
			JsonNode node = mapper.readTree(dtsBufferData);
			List<SimpleObject> dtsBuffers = mapper.readValue(node, new TypeReference<List<SimpleObject>>() {});
			proficiencyTestingBuffers = StringUtils.isNotEmpty(proficiencyTestingId)
			        ? getDtsBuffers(dtsBuffers, htsProficiencyTesting)
			        : getDtsBuffers(dtsBuffers);
		}
		if (!proficiencyTestingBuffers.isEmpty()) {
			htsProficiencyTesting.setDtsProficiencyTestingBuffer(proficiencyTestingBuffers);
		}
		if (StringUtils.isNotEmpty(reportData)) {
			JsonNode jsonNode = mapper.readTree(reportData);
			SimpleObject report = mapper.readValue(jsonNode, new TypeReference<SimpleObject>() {});
			htsProficiencyTesting.setDateTested(
			    BotswanaEmrUtils.formatDateFromString(String.valueOf(report.get("dateTested")), "yyyy-MM-dd"));
			htsProficiencyTesting.setResultsReceivedBy(
			    Context.getPersonService().getPersonByUuid(String.valueOf(report.get("resultsReceivedBy"))));
			htsProficiencyTesting.setPerformanceScore(Double.parseDouble(String.valueOf(report.get("performanceScore"))));
			htsProficiencyTesting.setPercentageScore(Double.parseDouble(String.valueOf(report.get("percentageScore"))));
			htsProficiencyTesting.setComment(String.valueOf(report.get("overallComment")));
			htsProficiencyTesting.setReportReviewedBy(
			    Context.getPersonService().getPersonByUuid(String.valueOf(report.get("reportReviewedBy"))));
			htsProficiencyTesting.setDateReviewed(
			    BotswanaEmrUtils.formatDateFromString(String.valueOf(report.get("dateReviewed")), "yyyy-MM-dd"));
		}
		htsProficiencyTestingService.saveHtsProficiencyTesting(htsProficiencyTesting);
		
		// TODO : Correct this implementation
		if (published) {
			for (HtsProficiencyTestingTest testingTest : htsProficiencyTesting.getHtsProficiencyTestingTest()) {
				if (testingTest.getStockroom() != null) {
					StockOperation stockOperation = new StockOperation();
					stockOperation.setOperationDate(new Date());
					stockOperation.setSource(testingTest.getStockroom());
					stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
					stockOperation.setDescription("Kits used for proficiency testing the patient");
					stockOperation.setOperationNumber(UUID.randomUUID().toString());
					stockOperation.setStatus(StockOperationStatus.NEW);
					
					for (HtsSpecimenResult specimenResult : testingTest.getSpecimenResults()) {
						for (HtsTestResult testResult : specimenResult.getResults()) {
							Item item = iItemDataService
							        .getItemsByConcept(Context.getConceptService().getConceptByUuid(testResult.getKitName()))
							        .get(0);
							StockOperation batchOperation = iStockOperationDataService
							        .getOperationByNumber(testResult.getLotNumber());
							Date expirationDate = testResult.getExpiryDate();
							
							stockOperation.addItem(item, 1, expirationDate, batchOperation);
						}
					}
					iStockOperationService.submitOperation(stockOperation);
					stockOperation.setStatus(StockOperationStatus.COMPLETED);
					iStockOperationService.submitOperation(stockOperation);
				}
			}
		}
	}
	
	private Set<HtsProficiencyTestingTest> getPtSpecimens(List<SimpleObject> ptSpecimens) throws ParseException {
		Set<HtsProficiencyTestingTest> htsProficiencyTestingTests = new HashSet<>();
		for (SimpleObject ptSpecimen : ptSpecimens) {
			HtsProficiencyTestingTest htsProficiencyTestingTest = new HtsProficiencyTestingTest();
			if (ptSpecimen.containsKey("id") && StringUtils.isNotEmpty(String.valueOf(ptSpecimen.get("id")))) {
				htsProficiencyTestingTest = (HtsProficiencyTestingTest) htsProficiencyTestingService.getById(
				    Integer.parseInt(String.valueOf(ptSpecimen.get("id"))), HtsProficiencyTestingTest.class.getName());
			}
			htsProficiencyTestingTest.setSpecimenId(String.valueOf(ptSpecimen.get("ptSpecimenId")));
			htsProficiencyTestingTest.setLotNumber(String.valueOf(ptSpecimen.get("ptSpecimenLotNumber")));
			
			if (StringUtils.isNotEmpty(String.valueOf(ptSpecimen.get("ptSpecimenExpiryDate")))) {
				htsProficiencyTestingTest.setExpiryDate(BotswanaEmrUtils
				        .formatDateFromString(String.valueOf(ptSpecimen.get("ptSpecimenExpiryDate")), "yyyy-MM-dd"));
			}
			htsProficiencyTestingTest.setConditionOfSpecimen(String.valueOf(ptSpecimen.get("ptSpecimenConditions")));
			htsProficiencyTestingTest
			        .setReasonForUnacceptableSpecimen(String.valueOf(ptSpecimen.get("reasonForUnacceptablePtSpecimen")));
			htsProficiencyTestingTest.setCorrectiveActions(String.valueOf(ptSpecimen.get("correctiveActions")));
			if (ptSpecimen.get("ptSpecimenId") != null) {
				String stockRoomId = getStockRoomId((List<SimpleObject>) ptSpecimen.get("specimenResults"));
				htsProficiencyTestingTest.setStockroom(StockUtils.getStockroom(stockRoomId));
			}
			htsProficiencyTestingTest.setSpecimenResults(
			    getSpecimenResults((List<SimpleObject>) ptSpecimen.get("specimenResults"), htsProficiencyTestingTest));
			
			htsProficiencyTestingTests.add(htsProficiencyTestingTest);
		}
		return htsProficiencyTestingTests;
	}
	
	private String getStockRoomId(List<SimpleObject> specimenResults) {
		for (LinkedHashMap result : specimenResults) {
			if (result.containsKey("stockRoomId")) {
				return String.valueOf(result.get("stockRoomId"));
			}
		}
		return null;
	}
	
	private Set<HtsSpecimenResult> getSpecimenResults(List<SimpleObject> specimenResults,
	        HtsProficiencyTestingTest htsProficiencyTestingTest) {
		Set<HtsSpecimenResult> specimenResultSet = new HashSet<>();
		for (LinkedHashMap specimenResult : specimenResults) {
			HtsSpecimenResult htsSpecimenResult = new HtsSpecimenResult();
			if (specimenResult.containsKey("id") && StringUtils.isNotEmpty(String.valueOf(specimenResult.get("id")))) {
				htsSpecimenResult = (HtsSpecimenResult) htsProficiencyTestingService.getById(
				    Integer.parseInt(String.valueOf(specimenResult.get("id"))), HtsSpecimenResult.class.getName());
			}
			if (specimenResult.get("interpretation") != null) {
				htsSpecimenResult.setInterpretation(String.valueOf(specimenResult.get("interpretation")));
				htsSpecimenResult.setComment(String.valueOf(specimenResult.get("comment")));
				if (specimenResult.containsKey("stockRoomId"))
					htsSpecimenResult
					        .setStockroom(StockUtils.getStockroom(String.valueOf(specimenResult.get("stockRoomId"))));
				htsSpecimenResult
				        .setResults(getTestResults((List<SimpleObject>) specimenResult.get("results"), htsSpecimenResult));
				
				specimenResultSet.add(htsSpecimenResult);
			}
		}
		
		return specimenResultSet;
	}
	
	private Set<HtsTestResult> getTestResults(List<SimpleObject> testResults, HtsSpecimenResult htsSpecimenResult) {
		Set<HtsTestResult> testResultSet = new HashSet<>();
		
		for (LinkedHashMap testResult : testResults) {
			Item item = null;
			StockOperation batchOperation = null;
			Date expirationDate = null;
			HtsTestResult htsTestResult = new HtsTestResult();
			if (testResult.containsKey("id") && StringUtils.isNotEmpty(String.valueOf(testResult.get("id")))) {
				htsTestResult = (HtsTestResult) htsProficiencyTestingService
				        .getById(Integer.parseInt(String.valueOf(testResult.get("id"))), HtsTestResult.class.getName());
			}
			try {
				
				if (StringUtils.isNotEmpty(String.valueOf(testResult.get("expiryDate")))) {
					htsTestResult.setExpiryDate(
					    BotswanaEmrUtils.formatDateFromString(String.valueOf(testResult.get("expiryDate")), "yyyy-MM-dd"));
				}
			}
			catch (ParseException e) {
				e.printStackTrace();
			}
			htsTestResult.setKitResults(String.valueOf(testResult.get("kitResults")));
			htsTestResult.setResultNumber(String.valueOf(testResult.get("resultNumber")));
			htsTestResult.setLotNumber(String.valueOf(testResult.get("lotNo")));
			htsTestResult.setKitName(String.valueOf(testResult.get("kitName")));
			
			testResultSet.add(htsTestResult);
			
		}
		return testResultSet;
	}
	
	private Set<DtsProficiencyTestingBuffer> getDtsBuffers(List<SimpleObject> dtsBuffers) throws ParseException {
		Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffers = new HashSet<>();
		for (SimpleObject dtsBuffer : dtsBuffers) {
			DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer = new DtsProficiencyTestingBuffer();
			dtsProficiencyTestingBuffer.setDtsBufferId(String.valueOf(dtsBuffer.get("dtsBufferId")));
			dtsProficiencyTestingBuffer.setDtsLotNumber(String.valueOf(dtsBuffer.get("dtsLotNumber")));
			if (StringUtils.isNotEmpty(String.valueOf(dtsBuffer.get("dtsExpiryDate")))) {
				dtsProficiencyTestingBuffer.setDtsExpiryDate(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(dtsBuffer.get("dtsExpiryDate")), "yyyy-MM-dd"));
			}
			dtsProficiencyTestingBuffer.setDtsConditionOfSpecimen(String.valueOf(dtsBuffer.get("dtsBufferConditions")));
			dtsProficiencyTestingBuffer
			        .setDtsReasonForUnacceptableSpecimen(String.valueOf(dtsBuffer.get("dtsReasonForUnacceptableBuffer")));
			dtsProficiencyTestingBuffer.setDtsCorrectiveActions(String.valueOf(dtsBuffer.get("dtsCorrectiveActions")));
			dtsProficiencyTestingBuffers.add(dtsProficiencyTestingBuffer);
		}
		return dtsProficiencyTestingBuffers;
	}
	
	private Set<DtsProficiencyTestingBuffer> getDtsBuffers(List<SimpleObject> buffers,
	        HtsProficiencyTesting htsProficiencyTesting) throws ParseException {
		Set<DtsProficiencyTestingBuffer> existingDtsProficiencyTestingBuffers = htsProficiencyTesting
		        .getDtsProficiencyTestingBuffer() != null ? htsProficiencyTesting.getDtsProficiencyTestingBuffer()
		                : new HashSet<>();
		Set<DtsProficiencyTestingBuffer> updatedDtsProficiencyTestingBuffers = new HashSet<>();
		for (SimpleObject buffer : buffers) {
			String proficiencyTestingBufferId = String.valueOf(buffer.get("id"));
			Optional<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffer = existingDtsProficiencyTestingBuffers.stream()
			        .filter(ptb -> ptb.getId().equals(Integer.valueOf(proficiencyTestingBufferId))).distinct().findFirst();
			DtsProficiencyTestingBuffer proficiencyTestingBuffer;
			proficiencyTestingBuffer = dtsProficiencyTestingBuffer.orElseGet(DtsProficiencyTestingBuffer::new);
			proficiencyTestingBuffer.setDtsBufferId(String.valueOf(buffer.get("dtsBufferId")));
			proficiencyTestingBuffer.setDtsLotNumber(String.valueOf(buffer.get("dtsLotNumber")));
			if (StringUtils.isNotEmpty(String.valueOf(buffer.get("dtsExpiryDate")))) {
				proficiencyTestingBuffer.setDtsExpiryDate(
				    BotswanaEmrUtils.formatDateFromString(String.valueOf(buffer.get("dtsExpiryDate")), "yyyy-MM-dd"));
			}
			proficiencyTestingBuffer.setDtsConditionOfSpecimen(String.valueOf(buffer.get("dtsConditionOfBuffer")));
			proficiencyTestingBuffer
			        .setDtsReasonForUnacceptableSpecimen(String.valueOf(buffer.get("dtsReasonForUnacceptableBuffer")));
			proficiencyTestingBuffer.setDtsCorrectiveActions(String.valueOf(buffer.get("dtsCorrectiveActions")));
			proficiencyTestingBuffer.setDtsProficiencyTesting(htsProficiencyTesting);
			htsProficiencyTestingService.saveOrUpdateDtsProficiencyTestingBuffer(proficiencyTestingBuffer);
			updatedDtsProficiencyTestingBuffers.add(proficiencyTestingBuffer);
		}
		return updatedDtsProficiencyTestingBuffers;
	}
}
