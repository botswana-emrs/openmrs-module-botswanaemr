/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.programs.tb;

import org.openmrs.Patient;
import org.openmrs.api.ObsService;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

@SuppressWarnings("unused")
public class ContinuationDotFragmentController extends DotFragmentController {
	
	public void controller(@SpringBean FragmentModel model, UiUtils ui, @RequestParam("patientId") Patient patient,
	        @SpringBean("obsService") ObsService obsService) {
		
		//todo find a better way
		model.addAttribute("days", getDays());
		model.addAttribute("mFour", getMonthEvents(getMappedEvents(patient, obsService), 3));
		model.addAttribute("mFive", getMonthEvents(getMappedEvents(patient, obsService), 4));
		model.addAttribute("mSix", getMonthEvents(getMappedEvents(patient, obsService), 5));
		model.addAttribute("mSeven", getMonthEvents(getMappedEvents(patient, obsService), 6));
		model.addAttribute("mEight", getMonthEvents(getMappedEvents(patient, obsService), 7));
		model.addAttribute("mNine", getMonthEvents(getMappedEvents(patient, obsService), 8));
	}
}
