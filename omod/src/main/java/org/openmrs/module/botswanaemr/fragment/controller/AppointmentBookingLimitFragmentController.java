/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.openmrs.Location;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.api.AppointmentBookingService;
import org.openmrs.module.botswanaemr.model.appointment.AppointmentBookingLimit;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AppointmentBookingLimitFragmentController {
	
	AppointmentBookingService appointmentBookingService = Context.getService(AppointmentBookingService.class);
	
	public void controller(FragmentModel model, @RequestParam(value = "facilityId", required = false) Location facility,
	        @SpringBean("locationService") LocationService locationService) {
		List<AppointmentBookingLimit> appointmentBookingLimits = appointmentBookingService
		        .getAppointmentBookingLimits(facility);
		List<SimpleObject> appointmentBookingLimitsObject = new ArrayList<>();
		for (AppointmentBookingLimit appointmentBookingLimit : appointmentBookingLimits) {
			SimpleObject object = new SimpleObject();
			object.put("day", appointmentBookingLimit.getDay());
			object.put("limit", appointmentBookingLimit.getLimit());
			
			appointmentBookingLimitsObject.add(object);
		}
		model.addAttribute("appointmentBookingLimits", appointmentBookingLimitsObject);
		model.addAttribute("facilityId", facility.getId());
	}
	
	public String post(UiUtils uiUtils, @RequestParam("facilityId") Location facility,
	        @RequestParam(value = "bookingLimits", required = true) String bookingLimits, UiSessionContext sessionContext)
	        throws JSONException {
		
		List<AppointmentBookingLimit> appointmentBookingLimits = appointmentBookingService
		        .getAppointmentBookingLimits(facility);
		
		JSONObject jsonObject = new JSONObject(bookingLimits);
		Iterator<String> keys = jsonObject.keys();
		Date today = new Date();
		
		while (keys.hasNext()) {
			String day = keys.next();
			String limitString = jsonObject.optString(day, "").trim();
			if (!limitString.isEmpty()) {
				try {
					int limit = Integer.parseInt(limitString);
					// If no existing booking limits, create new ones
					if (appointmentBookingLimits.isEmpty()) {
						AppointmentBookingLimit appointmentBookingLimit = new AppointmentBookingLimit();
						appointmentBookingLimit.setDay(day);
						appointmentBookingLimit.setLimit(limit);
						appointmentBookingLimit.setLocation(facility);
						appointmentBookingLimit.setComment("");
						appointmentBookingLimit.setCreator(sessionContext.getCurrentUser());
						appointmentBookingLimit.setDateCreated(today);
						
						// Save the appointment booking limit
						appointmentBookingService.saveAppointmentBookingLimit(appointmentBookingLimit);
					} else {
						for (AppointmentBookingLimit appointmentBookingLimit : appointmentBookingLimits) {
							if (day.equals(appointmentBookingLimit.getDay())) {
								appointmentBookingLimit.setDay(day);
								appointmentBookingLimit.setLimit(limit);
								appointmentBookingLimit.setLocation(facility);
								appointmentBookingLimit.setCreator(sessionContext.getCurrentUser());
								
								//update an existing appointment booking limit
								appointmentBookingService.updateAppointmentBookingLimit(appointmentBookingLimit);
							}
						}
					}
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		return "success";
	}
	
}
