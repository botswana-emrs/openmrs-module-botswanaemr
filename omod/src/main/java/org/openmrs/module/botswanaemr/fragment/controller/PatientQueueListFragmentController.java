/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Provider;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.LocationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.mapper.BotswanaPatientQueueMapper;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.emrapi.visit.VisitDomainWrapper;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.htmlformentryui.HtmlFormUtil;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_SCREENING_ENCOUNTER_TYPE_UUID;

public class PatientQueueListFragmentController {
	
	private final LocationService locationService = Context.getService(LocationService.class);
	
	private final PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
	
	private final EncounterService encounterService = Context.getService(EncounterService.class);
	
	private final HtmlFormEntryService htmlFormEntryService = Context.getService(HtmlFormEntryService.class);
	
	private final FormService formService = Context.getService(FormService.class);
	
	private final BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
	
	public void controller(@SpringBean FragmentModel pageModel, UiSessionContext uiSessionContext) {
		pageModel.addAttribute("locationList", Context.getLocationService().getAllLocations());
		pageModel.addAttribute("currentServicePoint",
		    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
	}
	
	public List<SimpleObject> getPatientQueueListByQueueRoom(UiSessionContext uiSessionContext, UiUtils uiUtils,
	        @SpringBean("adtService") AdtService adtService,
	        @RequestParam(value = "servicePoint", required = false) String servicePointUuid,
	        @RequestParam(value = "sourcePageName", required = false) String sourcePageName) {
		Location queueRoom;
		String currentServicePointUuid = null;
		if (StringUtils.isNotBlank(servicePointUuid)) {
			queueRoom = locationService.getLocationByUuid(servicePointUuid);
		} else {
			currentServicePointUuid = String.valueOf(
			    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
			queueRoom = locationService.getLocationByUuid(currentServicePointUuid);
		}
		
		List<SimpleObject> simpleObjects = new ArrayList<>();
		if (queueRoom == null) {
			return simpleObjects;
		}
		
		Date startOfDay = DateUtil.getStartOfDay(new Date());
		Date endOfDay = DateUtil.getEndOfDay(new Date());
		Location finalQueueRoom = queueRoom;
		List<PatientQueue> patientQueueList = patientQueueingService
		        .getPatientQueueListBySearchParams(null, startOfDay, endOfDay, null, null, null, queueRoom).stream()
		        .filter(q -> q.getLocationTo().equals(finalQueueRoom))
		        .filter(q -> q.getPatient().getAttributes("LocationAttribute").stream()
		                .anyMatch(a -> a.getValue().equals(uiSessionContext.getSessionLocation().getUuid())))
		        .distinct().collect(Collectors.toList());
		
		if (queueRoom.getUuid().equals(BotswanaEmrConstants.VMMC_PORTAL_UUID)) {
			for (PatientQueue patientQueue : patientQueueList) {
				VisitDomainWrapper visitWrapper = adtService.getActiveVisit(patientQueue.getPatient(),
				    patientQueue.getLocationFrom());
				if (visitWrapper != null) {
					Visit activeVisit = visitWrapper.getVisit();
					for (Encounter enc : activeVisit.getEncounters()) {
						if (enc.getEncounterType().getUuid()
						        .equals(BotswanaEmrConstants.VMMC_INITIAL_SCREENING_ENCOUNTER_TYPE)
						        || enc.getEncounterType().getUuid()
						                .equals(BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE)
						        || enc.getEncounterType().getUuid()
						                .equals(BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE)
						        || enc.getEncounterType().getUuid()
						                .equals(BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE)
						        || enc.getEncounterType().getUuid()
						                .equals(BotswanaEmrConstants.VMMC_CONSENT_ENCOUNTER_TYPE)) {
							patientQueue.setStatus(PatientQueue.Status.PICKED);
						}
					}
				}
			}
		}
		
		List<BotswanaPatientQueueMapper> patientQueueMappers = mapPatientQueueToMapper(patientQueueList, uiUtils,
		    servicePointUuid, sourcePageName);
		simpleObjects = SimpleObject.fromCollection(patientQueueMappers, uiUtils, "pin", "patientId", "dateTimeRegistered",
		    "durationOfWait", "recentQueueLocation", "treatmentStartTime", "patientNames", "gender", "age", "locationFrom",
		    "providerNames", "status", "priorityComment", "patientQueueId", "visitNumber", "presumptive", "returnUrl",
		    "hasTbScreeningEncounter", "queueEncounterId", "encounterIdentifier", "dateCreated", "hivStatus", "hivPositive");
		return simpleObjects;
	}
	
	public SimpleObject getPatientQueueListByQueueRoomWithExtendedSearch(UiSessionContext uiSessionContext, UiUtils uiUtils,
	        @SpringBean("adtService") AdtService adtService, @RequestParam(value = "draw", required = false) Integer draw,
	        @RequestParam(value = "start", required = false) Integer page,
	        @RequestParam(value = "length", required = false) Integer pageSize,
	        @RequestParam(value = "search[value]", required = false) String searchValue,
	        @RequestParam(value = "search[date]", required = false) Date searchDate,
	        @RequestParam(value = "servicePoint", required = false) String servicePointUuid,
	        @RequestParam(value = "extendedSearch", required = false) String extendedSearch,
	        @RequestParam(value = "sourcePageName", required = false) String sourcePageName) {
		
		SimpleObject simpleObject = new SimpleObject();
		Location queueRoom;
		String currentServicePointUuid = null;
		if (StringUtils.isNotBlank(servicePointUuid)) {
			queueRoom = locationService.getLocationByUuid(servicePointUuid);
		} else {
			currentServicePointUuid = String.valueOf(
			    uiSessionContext.getSession().getAttribute(BotswanaEmrConstants.CURRENT_SERVICE_DELIVERY_POINT));
			queueRoom = locationService.getLocationByUuid(currentServicePointUuid);
		}
		
		List<SimpleObject> simpleObjects = new ArrayList<>();
		if (queueRoom == null) {
			return simpleObject;
		}
		Date startOfDay;
		Date endOfDay;
		if (searchDate != null) {
			startOfDay = DateUtil.getStartOfDay(searchDate);
			endOfDay = DateUtil.getStartOfDay(searchDate);
		} else {
			startOfDay = DateUtil.getStartOfDay(new Date());
			endOfDay = DateUtil.getEndOfDay(new Date());
		}
		Location finalQueueRoom = queueRoom;
		List<PatientQueue> patientQueueList = patientQueueingService
		        .getPatientQueueListBySearchParams(searchValue, startOfDay, endOfDay, null, null, null, queueRoom).stream()
		        .filter(q -> q.getLocationTo().equals(finalQueueRoom))
		        .filter(q -> q.getPatient().getAttributes("LocationAttribute").stream()
		                .anyMatch(a -> a.getValue().equals(uiSessionContext.getSessionLocation().getUuid())))
		        .distinct().collect(Collectors.toList());
		
		if (queueRoom.getUuid().equals(BotswanaEmrConstants.VMMC_PORTAL_UUID)) {
			for (PatientQueue patientQueue : patientQueueList) {
				Visit activeVisit = adtService.getActiveVisit(patientQueue.getPatient(), patientQueue.getLocationFrom())
				        .getVisit();
				for (Encounter enc : activeVisit.getEncounters()) {
					if (enc.getEncounterType().getUuid().equals(BotswanaEmrConstants.VMMC_INITIAL_SCREENING_ENCOUNTER_TYPE)
					        || enc.getEncounterType().getUuid().equals(BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE)
					        || enc.getEncounterType().getUuid().equals(BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE)
					        || enc.getEncounterType().getUuid()
					                .equals(BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE)
					        || enc.getEncounterType().getUuid().equals(BotswanaEmrConstants.VMMC_CONSENT_ENCOUNTER_TYPE)) {
						patientQueue.setStatus(PatientQueue.Status.PICKED);
					}
				}
			}
		}
		
		List<BotswanaPatientQueueMapper> patientQueueMappers = mapPatientQueueToMapper(patientQueueList, uiUtils,
		    servicePointUuid, sourcePageName);
		simpleObjects = SimpleObject.fromCollection(patientQueueMappers, uiUtils, "pin", "patientId", "patientNames",
		    "gender", "age", "locationFrom", "providerNames", "status", "priorityComment", "patientQueueId", "visitNumber",
		    "presumptive", "returnUrl", "hasTbScreeningEncounter", "queueEncounterId", "encounterIdentifier", "dateCreated","hivPositive");
		simpleObject.put("draw", draw);
		int totalDisplayRecords = patientQueueList.size();
		simpleObject.put("iTotalDisplayRecords", totalDisplayRecords);
		simpleObject.put("iTotalRecords", totalDisplayRecords);
		simpleObject.put("aaData", simpleObjects);
		
		return simpleObject;
	}
	
	public List<SimpleObject> getPatientQueueList(
	        @RequestParam(value = "searchPhrase", required = false) String searchPhrase,
	        @RequestParam(value = "provider", required = false) final Provider provider,
	        @RequestParam(value = "queueRoom", required = false) Location queueRoom,
	        @RequestParam(value = "status", required = false) final PatientQueue.Status status,
	        @RequestParam(value = "sourcePageName", required = false) final String sourcePageName,
	        UiSessionContext uiSessionContext, UiUtils uiUtils) throws IOException {
		
		List<SimpleObject> simpleObjects;
		
		Date startOfDay = DateUtil.getStartOfDay(new Date());
		Date endOfDay = DateUtil.getEndOfDay(new Date());
		List<PatientQueue> patientQueueList;
		
		if (queueRoom == null) {
			queueRoom = uiSessionContext.getSessionLocation();
		}
		
		patientQueueList = patientQueueingService.getPatientQueueListBySearchParams(searchPhrase, startOfDay, endOfDay,
		    queueRoom, uiSessionContext.getSessionLocation(), status, queueRoom);
		
		if (!patientQueueList.isEmpty()) {
			Location sessionLocation = uiSessionContext.getSessionLocation();
			String locationUuid = null;
			if (sessionLocation != null) {
				locationUuid = sessionLocation.getUuid();
			}
			
			String finalLocationUuid = locationUuid;
			patientQueueList = patientQueueList.stream()
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(finalLocationUuid))
			        .collect(Collectors.toList());
		}
		
		if (provider != null) {
			List<PatientQueue> list = new ArrayList<>();
			for (PatientQueue p : patientQueueList) {
				if (p.getProvider() != null) {
					if (p.getProvider().getUuid().equals(provider.getUuid())) {
						list.add(p);
					}
				}
			}
			patientQueueList = list;
		}
		List<BotswanaPatientQueueMapper> patientQueueMappers = mapPatientQueueToMapper(patientQueueList, uiUtils,
		    uiSessionContext.getSessionLocation().getUuid(), sourcePageName);
		simpleObjects = SimpleObject.fromCollection(patientQueueMappers, uiUtils, "pin", "patientId", "patientNames",
		    "gender", "age", "locationFrom", "providerNames", "status", "priorityComment", "patientQueueId", "visitNumber");
		return simpleObjects;
	}
	
	public List<BotswanaPatientQueueMapper> mapPatientQueueToMapper(List<PatientQueue> patientQueueList, UiUtils uiUtils,
	        String servicePointUuid, String sourcePageName) {
		List<BotswanaPatientQueueMapper> patientQueueMappers = new ArrayList<>();
		String returnUrl = "";
		EncounterType tbScreeningEncounterType = encounterService.getEncounterTypeByUuid(TB_SCREENING_ENCOUNTER_TYPE_UUID);
		for (PatientQueue patientQueue : patientQueueList) {
			String names = patientQueue.getPatient().getFamilyName() + " " + patientQueue.getPatient().getGivenName() + " "
			        + patientQueue.getPatient().getMiddleName();
			BotswanaPatientQueueMapper patientQueueMapper = new BotswanaPatientQueueMapper();
			patientQueueMapper.setId(patientQueue.getId());
			patientQueueMapper.setPatientNames(names.replace("null", ""));
			patientQueueMapper.setPatientId(patientQueue.getPatient().getPatientId());
			patientQueueMapper.setLocationFrom(patientQueue.getLocationFrom().getName());
			patientQueueMapper.setLocationTo(patientQueue.getLocationTo().getName());
			patientQueueMapper.setGender(patientQueue.getPatient().getGender());
			patientQueueMapper.setPin(patientQueue.getPatient()
			        .getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME).getIdentifier());
			Visit lastPatientVisit = patientQueue.getEncounter() != null ? patientQueue.getEncounter().getVisit()
			        : BotswanaEmrUtils.getPatientActiveVisit(patientQueue.getPatient(), patientQueue.getLocationFrom(),
			            true);
			String visitNumber = lastPatientVisit != null ? lastPatientVisit.getVisitId().toString() : "";
			patientQueueMapper.setVisitNumber(visitNumber);

			boolean isHivPositive = BotswanaEmrUtils.isHivPositive(patientQueue.getPatient());
			String hivStatus = BotswanaEmrUtils.getHivStatus(patientQueue.getPatient());
			patientQueueMapper.setHivStatus(hivStatus);
			patientQueueMapper.setHivPositive(isHivPositive);
			
			Date encounterDateTime;
			String treatmentStartTime = null;
			
			if (patientQueue.getEncounter() != null) {
				// retrieve the encounter date and time -> treatment start time
				encounterDateTime = patientQueue.getEncounter().getEncounterDatetime();
				Instant instant = encounterDateTime.toInstant();
				LocalDateTime treatmentStartDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
				DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
				treatmentStartTime = treatmentStartDateTime.format(timeFormatter);
				patientQueueMapper.setTreatmentStartTime(treatmentStartTime);
			}
			
			if (patientQueue.getProvider() != null) {
				patientQueueMapper.setProviderNames(patientQueue.getProvider().getName());
			}
			
			patientQueueMapper.setAge(String.valueOf(patientQueue.getPatient().getAge()));
			patientQueueMapper.setDateCreated(uiUtils.formatDatetimePretty(patientQueue.getDateCreated()));
			patientQueueMapper.setPriorityComment(patientQueue.getPriorityComment());
			
			Registration latestPayment = botswanaEmrService.getLatestPaymentEntry(patientQueue.getPatient(),
			    patientQueue.getLocationFrom());
			
			if (latestPayment != null) {
				Date paymentDate = latestPayment.getRegistrationDatetime();
				
				Instant instant = paymentDate.toInstant();
				
				//set the registrationDatetime according to the time zone accuracy
				LocalDateTime registrationDatetime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
				
				// Format the payment datetime to display the time only
				DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm");
				String formattedTime = registrationDatetime.format(dateTimeFormatter);
				
				//set to patientQueueMapper property
				patientQueueMapper.setDateTimeRegistered(formattedTime);
				
				// Compute the duration of wait, check if treatment started
				if (treatmentStartTime == null) {
					LocalDateTime now = LocalDateTime.now();
					Duration duration = Duration.between(registrationDatetime, now);
					
					long hours = duration.toHours();
					long minutes = duration.toMinutes() % 60;
					
					String durationInHoursMin = String.format("%02d hours %02d minutes", hours, minutes);
					patientQueueMapper.setDurationOfWait(durationInHoursMin);
				} else {
					//pause the duration of wait
					patientQueueMapper.setDurationOfWait("Paused");
				}
				
				if (treatmentStartTime == null) {
					LocalDateTime now = LocalDateTime.now();
					Duration duration = Duration.between(registrationDatetime, now);
					
					long hours = duration.toHours();
					long minutes = duration.toMinutes() % 60;
					
					String durationInHoursMin = String.format("%02d hours %02d minutes", hours, minutes);
					patientQueueMapper.setDurationOfWait(durationInHoursMin);
				} else {
					try {
						DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
						LocalTime treatmentStartLocalTime = LocalTime.parse(treatmentStartTime, timeFormatter);
						LocalDateTime treatmentStartDateTime = registrationDatetime.toLocalDate()
						        .atTime(treatmentStartLocalTime);
						Duration frozenDuration = Duration.between(registrationDatetime, treatmentStartDateTime);
						
						long hours = frozenDuration.toHours();
						long minutes = frozenDuration.toMinutes() % 60;
						
						String frozenDurationInHoursMin = String.format("%02d hours %02d minutes", hours, minutes);
						patientQueueMapper.setDurationOfWait(frozenDurationInHoursMin);
					}
					catch (DateTimeParseException e) {
						patientQueueMapper.setDurationOfWait("Invalid treatment start time");
					}
				}
			} else {
				patientQueueMapper.setDateTimeRegistered("");
				patientQueueMapper.setDurationOfWait("");
			}
			
			//most recent queueing location
			PatientQueue queue = patientQueueingService.getMostRecentQueue(patientQueue.getPatient());
			
			if (queue != null) {
				Location currentQueueLocation = queue.getQueueRoom();
				patientQueueMapper.setRecentQueueLocation(currentQueueLocation.toString());
			} else {
				patientQueueMapper.setRecentQueueLocation("");
			}
			
			if (lastPatientVisit != null) {
				List<HtmlForm> completedHtmlForms = BotswanaEmrUtils.completedHtmlForms(lastPatientVisit,
				    htmlFormEntryService);
				
				if (servicePointUuid != null) {
					switch (servicePointUuid) {
						case BotswanaEmrConstants.HIV_TESTING_SERVICES_PORTAL_UUID:
							setHtsStatus(patientQueueMapper, completedHtmlForms);
							break;
						case BotswanaEmrConstants.TB_SERVICES_PORTAL_UUID:
							patientQueueMapper.setStatus(getTbsStatus(completedHtmlForms));
							break;
						case BotswanaEmrConstants.PHARMACY_PORTAL_UUID:
							patientQueueMapper.setStatus(getPharmacyStatus(patientQueue.getEncounter()));
							break;
						default:
							patientQueueMapper.setStatus(patientQueue.getStatus().name());
					}
				}
			}
			
			boolean isPresumptive = BotswanaEmrUtils.isTbPresumptive(patientQueue.getPatient(), lastPatientVisit);
			
			patientQueueMapper.setPresumptive(isPresumptive);
			String returnPageName;
			if (sourcePageName != null) {
				returnPageName = sourcePageName;
			} else {
				returnPageName = "consultation/screeningAndTriage";
			}
			returnUrl = uiUtils.encodeForSafeURL(HtmlFormUtil.determineReturnUrl(returnUrl, "botswanaemr", returnPageName,
			    patientQueue.getPatient(), lastPatientVisit, uiUtils));
			patientQueueMapper.setReturnUrl(returnUrl);
			
			boolean hasTbScreeningEncounter = false;
			if (lastPatientVisit != null) {
				Set<Encounter> encounters = lastPatientVisit.getEncounters();
				Comparator<Encounter> encounterDateTimeComparator = Comparator.comparing(Encounter::getEncounterDatetime);
				Optional<Encounter> tbScreeningEncounter = encounters.stream()
				        .filter(encounter -> encounter.getEncounterType().equals(tbScreeningEncounterType))
				        .filter(
				            encounter -> encounter.getForm().getUuid().equals(BotswanaEmrConstants.TB_SCREENING_FORM_UUID))
				        .max(encounterDateTimeComparator);
				
				if (tbScreeningEncounter.isPresent()) {
					hasTbScreeningEncounter = true;
					patientQueueMapper.setEncounterIdentifier(tbScreeningEncounter.get().getEncounterId());
				}
				patientQueueMapper.setHasTbScreeningEncounter(hasTbScreeningEncounter);
			}
			
			if (patientQueue.getEncounter() != null) {
				patientQueueMapper.setQueueEncounterId(patientQueue.getEncounter().getEncounterId());
			}
			patientQueueMappers.add(patientQueueMapper);
		}
		return patientQueueMappers;
	}
	
	private String getPharmacyStatus(Encounter enc) {
		String patientQueueStatus = "";
		
		if (enc != null) {
			Set<Order> encounterOrders = enc.getOrders();
			String prescriptionStatus = BotswanaEmrUtils.determinePrescriptionStatus(encounterOrders);
			
			if (prescriptionStatus.equals("Awaiting")) {
				patientQueueStatus = "PENDING";
			} else if (prescriptionStatus.equals("Completed")) {
				patientQueueStatus = "COMPLETED";
			} else if (prescriptionStatus.equals("In progress") || prescriptionStatus.equals("Partially completed")
			        || prescriptionStatus.equals("Partially filled")) {
				patientQueueStatus = "IN_PROGRESS";
			} else if (prescriptionStatus.equals("Not prescribed")) {
				patientQueueStatus = "COMPLETED";
			}
		}
		
		return patientQueueStatus;
	}
	
	private String getTbsStatus(List<HtmlForm> completedHtmlForms) {
		List<HtmlForm> tbsRequiredHtmlForms = new ArrayList<>();
		HtmlForm tbScreeningForOpdNurseForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_SCREENING_FORM_UUID));
		tbsRequiredHtmlForms.add(tbScreeningForOpdNurseForm);
		HtmlForm tbScreeningGenForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_SCREENING_GEN_FORM_UUID));
		tbsRequiredHtmlForms.add(tbScreeningGenForm);
		HtmlForm tbLabRequestForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_LAB_REQUEST_FORM_UUID));
		tbsRequiredHtmlForms.add(tbLabRequestForm);
		HtmlForm tbLabResultsForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.LAB_RESULTS_FORM_UUID));
		tbsRequiredHtmlForms.add(tbLabResultsForm);
		HtmlForm tbXrayResultsForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_XRAY_RESULTS_FORM_UUID));
		tbsRequiredHtmlForms.add(tbXrayResultsForm);
		HtmlForm tbContactListingForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_CONTACT_LISTING_FORM_UUID));
		tbsRequiredHtmlForms.add(tbContactListingForm);
		HtmlForm tbDotForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.TB_DOT_FORM_UUID));
		tbsRequiredHtmlForms.add(tbDotForm);
		
		String tbTreatmentStatus;
		
		if (hasFilledAllForms(tbsRequiredHtmlForms, completedHtmlForms)) {
			tbTreatmentStatus = "COMPLETED";
		} else if (!hasFilledAllForms(tbsRequiredHtmlForms, completedHtmlForms)
		        && hasFilledSomeForms(tbsRequiredHtmlForms, completedHtmlForms)) {
			tbTreatmentStatus = "IN_PROGRESS";
		} else {
			tbTreatmentStatus = "PENDING";
		}
		
		return tbTreatmentStatus;
	}
	
	private void setHtsStatus(BotswanaPatientQueueMapper patientQueueMapper, List<HtmlForm> completedHtmlForms) {
		List<HtmlForm> htsRequiredHtmlForms = new ArrayList<>();
		HtmlForm htsRegisterForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID));
		htsRequiredHtmlForms.add(htsRegisterForm);
		HtmlForm htsVerificationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID));
		htsRequiredHtmlForms.add(htsVerificationForm);
		HtmlForm htsPnsIndexForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_PNS_INDEX_FORM_UUID));
		htsRequiredHtmlForms.add(htsPnsIndexForm);
		HtmlForm htsChildrenIndexInformationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID));
		htsRequiredHtmlForms.add(htsChildrenIndexInformationForm);
		HtmlForm htsSexContactInformationForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_IPV_SCREENING_FORM_UUID));
		htsRequiredHtmlForms.add(htsSexContactInformationForm);
		HtmlForm htsPostTestForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_POST_TEST_FORM_UUID));
		htsRequiredHtmlForms.add(htsSexContactInformationForm);
		HtmlForm htsPriorForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(BotswanaEmrConstants.HTS_DOCUMENT_PRIOR_TEST_FORM_UUID));
		/* 
		List<HtmlForm> htsFilledTestHtmlForms = new ArrayList<>();
		htsFilledTestHtmlForms.add(htsRegisterForm);
		htsFilledTestHtmlForms.add(htsVerificationForm);
		htsFilledTestHtmlForms.add(htsPostTestForm);
		
		       if (hasFilledAllForms(htsRequiredHtmlForms, completedHtmlForms)) {
		    patientQueueMapper.setStatus("COMPLETED");
		       } else if (hasFilledAllForms(htsFilledTestHtmlForms, completedHtmlForms)) {
		   patientQueueMapper.setStatus("TEST_COMPLETED");
		       } else if (!hasFilledAllForms(htsRequiredHtmlForms, completedHtmlForms) && hasFilledSomeForms(htsRequiredHtmlForms, completedHtmlForms)) {
		    patientQueueMapper.setStatus("IN_PROGRESS");
		} else {
		    patientQueueMapper.setStatus("PENDING");
		}
		
		 */
		// Get last hts encounter
		Collection<Form> htsForms = new ArrayList<>();
		htsForms.add(htsRegisterForm.getForm());
		htsForms.add(htsVerificationForm.getForm());
		htsForms.add(htsPriorForm.getForm());
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteriaBuilder()
		        .setPatient(Context.getPatientService().getPatient(patientQueueMapper.getPatientId()))
		        .setEnteredViaForms(htsForms).createEncounterSearchCriteria();
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		// if not empty order by date and get the last encounter
		if (!encounters.isEmpty()) {
			encounters.sort(Comparator.comparing(Encounter::getEncounterDatetime));
			Encounter lastHtsEncounter = encounters.get(encounters.size() - 1);
			// if last encounter is diagnostics test, get the testing results from the associated obs
			if (lastHtsEncounter.getForm().getUuid().equals(BotswanaEmrConstants.HTS_DIAGNOSTICS_TEST_FORM_UUID)) {
				List<Obs> htsResultsObs = lastHtsEncounter.getAllObs(true).stream().filter(
				    obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.HIV_TEST_DIAGNOSIS_RESULT_CONCEPT_UUID))
				        .collect(Collectors.toList());
				if (!htsResultsObs.isEmpty()) {
					Obs htsResultObs = htsResultsObs.get(0);
					Concept htsResult = htsResultObs.getValueCoded();
					// Set status to "Diagnostics Test Completed" if results are found
					if (htsResult != null) {
						// If results are indeterminant, set status to "Awaiting Lab test"
						if (htsResult.getUuid().equals(BotswanaEmrConstants.INDETERMINATE_CONCEPT_UUID)) {
							patientQueueMapper.setStatus("AWAITING_LAB_TEST");
						} else {
							patientQueueMapper.setStatus("DIAGNOTICS_TEST_COMPLETED");
						}
					} else {
						// Set status to "Diagnostics Test Ongoing" if no results are found
						patientQueueMapper.setStatus("DIAGNOTICS_TEST_ONGOING");
					}
				} else {
					// Set status to "Diagnostics Test Ongoing" if no results are found
					patientQueueMapper.setStatus("DIAGNOTICS_TEST_ONGOING");
				}
			}
			// if last encounter is verification test, get the testing results from the associated obs
			if (lastHtsEncounter.getForm().getUuid().equals(BotswanaEmrConstants.HTS_VERIFICATION_FORM_UUID)) {
				List<Obs> htsResultsObs = lastHtsEncounter.getAllObs(true).stream().filter(
				    obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID))
				        .collect(Collectors.toList());
				if (!htsResultsObs.isEmpty()) {
					Obs htsResultObs = htsResultsObs.get(0);
					Concept htsResult = htsResultObs.getValueCoded();
					// Set status to "Verification Test Completed" if results are found
					if (htsResult != null) {
						// If results are indeterminant, set status to "Awaiting Lab test"
						if (htsResult.getUuid().equals(BotswanaEmrConstants.INDETERMINATE_CONCEPT_UUID)) {
							patientQueueMapper.setStatus("AWAITING_LAB_TEST");
						} else {
							patientQueueMapper.setStatus("VERIFICATION_TEST_COMPLETED");
						}
					} else {
						// Set status to "Verification Test Ongoing" if no results are found
						patientQueueMapper.setStatus("VERIFICATION_TEST_ONGOING");
					}
				} else {
					// Set status to "Verification Test Ongoing" if no results are found
					patientQueueMapper.setStatus("VERIFICATION_TEST_ONGOING");
				}
			}
			// if last encounter is document prior test, get the testing results from the associated obs
			if (lastHtsEncounter.getForm().getUuid().equals(BotswanaEmrConstants.HTS_DOCUMENT_PRIOR_TEST_FORM_UUID)) {
				List<Obs> htsResultsObs = lastHtsEncounter.getAllObs(true).stream().filter(
				    obs -> obs.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_PRIOR_TEST_STATUS_CONCEPT_UUID))
				        .collect(Collectors.toList());
				if (!htsResultsObs.isEmpty()) {
					Obs htsResultObs = htsResultsObs.get(0);
					Concept htsResult = htsResultObs.getValueCoded();
					// Set status to "Document Prior Test Completed" if results are found
					if (htsResult != null) {
						// if prior test status is Selft test positive, set status to "Awaiting Diagnostic Test"
						if (htsResult.getUuid().equals(BotswanaEmrConstants.SELF_TESTING_POSITIVE_CONCEPT_UUID)) {
							patientQueueMapper.setStatus("AWAITING_DIAGNOSTIC_TEST");
						} else {
							patientQueueMapper.setStatus("PRIOR_TEST_COMPLETED");
						}
					} else {
						// Set status to "Document Prior Test Ongoing" if no results are found
						patientQueueMapper.setStatus("PRIOR_TEST_ONGOING");
					}
				} else {
					// Set status to "Document Prior Test Ongoing" if no results are found
					patientQueueMapper.setStatus("PRIOR_TEST_ONGOING");
				}
			}
			
		} else {
			// Set status to "Pending" if no encounters are found
			patientQueueMapper.setStatus("PENDING");
		}
		
	}
	
	private boolean hasFilledAllForms(List<HtmlForm> requiredHtmlForms, List<HtmlForm> completedHtmlForms) {
		return completedHtmlForms.containsAll(requiredHtmlForms);
	}
	
	private boolean hasFilledSomeForms(List<HtmlForm> requiredHtmlForms, List<HtmlForm> completedHtmlForms) {
		return requiredHtmlForms.stream().anyMatch(completedHtmlForms::contains);
	}
}
