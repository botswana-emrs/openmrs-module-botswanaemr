/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.consultation.assessment;

import org.openmrs.ConditionVerificationStatus;
import org.openmrs.Diagnosis;
import org.openmrs.DiagnosisAttribute;
import org.openmrs.EncounterType;
import org.openmrs.Visit;
import org.openmrs.api.DiagnosisService;
import org.openmrs.api.VisitService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.openmrs.ui.framework.SimpleObject;
import org.springframework.web.bind.annotation.RequestParam;

public class DiagnosisFragmentController {
	
	public void controller(FragmentModel fragmentModel, @FragmentParam(value = "visit", required = false) Visit visit,
	        @SpringBean("diagnosisService") DiagnosisService diagnosisService) {
		fragmentModel.addAttribute("visit", visit);
		
		// Get diagnosis
		List<Diagnosis> diagnoses = diagnosisService.getDiagnosesByVisit(visit, false, false);
		diagnoses = diagnoses.stream().filter(d -> d.getVoided().equals(false)).collect(Collectors.toList());
		
		// Exclude diagnoses entered during the consultation encounters
		List<EncounterType> consultationEncounterTypes = BotswanaEmrUtils.getConsultationEncounterTypes();
		diagnoses = diagnoses.stream().filter(d -> !consultationEncounterTypes.contains(d.getEncounter().getEncounterType()))
		        .collect(Collectors.toList());
		fragmentModel.addAttribute("hasDiagnoses", diagnoses.size() > 0);
		fragmentModel.addAttribute("diagnoses", diagnoses);
		fragmentModel.addAttribute("provisionalDiagnoses", diagnoses.stream()
		        .filter(d -> d.getCertainty().equals(ConditionVerificationStatus.PROVISIONAL)).collect(Collectors.toList()));
		fragmentModel.addAttribute("confirmedDiagnoses", diagnoses.stream()
		        .filter(d -> d.getCertainty().equals(ConditionVerificationStatus.CONFIRMED)).collect(Collectors.toList()));
		fragmentModel.addAttribute("visit", visit);
		HashMap<Integer, String> diagnosisAttributes = new HashMap<Integer, String>();
		for (Diagnosis diagnosis : diagnoses) {
			String diagnosisValue = diagnosis.getDiagnosis().getCoded() != null
			        ? diagnosis.getDiagnosis().getCoded().getUuid()
			        : diagnosis.getDiagnosis().getNonCoded();
			
			for (DiagnosisAttribute attribute : diagnosis.getActiveAttributes()) {
				if (attribute.getAttributeType().getUuid()
				        .equals(BotswanaEmrConstants.DIAGNOSIS_TYPE_DIAGNOSIS_ATTRIBUTE_TYPE)) {
					diagnosisAttributes.put(diagnosis.getId(), String.valueOf(attribute.getValueReference()));
					break;
				}
			}
		}
		fragmentModel.addAttribute("diagnosisAttributes", diagnosisAttributes);
	}
	
	public SimpleObject fetchDiagnosisData(@RequestParam(value = "visitId", required = false) Integer visitId,
	        @SpringBean("diagnosisService") DiagnosisService diagnosisService,
	        @SpringBean("visitService") VisitService visitService) {
		
		SimpleObject response = new SimpleObject();
		Visit visit = visitService.getVisit(visitId);
		response.put("visit", visit != null ? visit.getVisitId() : null);
		
		List<Diagnosis> diagnoses = diagnosisService.getDiagnosesByVisit(visit, false, false).stream()
		        .filter(d -> !d.getVoided()).collect(Collectors.toList());
		
		List<EncounterType> consultationEncounterTypes = BotswanaEmrUtils.getConsultationEncounterTypes();
		diagnoses = diagnoses.stream().filter(d -> !consultationEncounterTypes.contains(d.getEncounter().getEncounterType()))
		        .collect(Collectors.toList());
		
		List<Diagnosis> diagnosis1048 = diagnoses.stream()
		        .filter(d -> d.getEncounter().getEncounterType().getUuid().equals("c5ce65c6-263f-4eeb-a623-e60129e50be7"))
		        .collect(Collectors.toList());
		
		HashMap<Integer, String> diagnosisAttributes = new HashMap<>();
		for (Diagnosis diagnosis : diagnoses) {
			for (DiagnosisAttribute attribute : diagnosis.getActiveAttributes()) {
				if (attribute.getAttributeType().getUuid()
						.equals(BotswanaEmrConstants.DIAGNOSIS_TYPE_DIAGNOSIS_ATTRIBUTE_TYPE)) {
					diagnosisAttributes.put(diagnosis.getId(), String.valueOf(attribute.getValueReference()));
					break;
				}
			}
		}
		
		List<SimpleObject> diagnosisList = diagnoses.stream()
				.map(diagnosis -> mapDiagnosisToSimpleObject(diagnosis, diagnosisAttributes))
				.collect(Collectors.toList());
		
		List<SimpleObject> diagnosis1048List = diagnosis1048.stream()
				.map(diagnosis -> mapDiagnosisToSimpleObject(diagnosis, diagnosisAttributes))
				.collect(Collectors.toList());
		
		List<SimpleObject> provisionalDiagnoses = diagnosisList.stream()
		        .filter(d -> d.get("certainty").equals(ConditionVerificationStatus.PROVISIONAL.toString()))
		        .collect(Collectors.toList());
		
		List<SimpleObject> confirmedDiagnoses = diagnosisList.stream()
		        .filter(d -> d.get("certainty").equals(ConditionVerificationStatus.CONFIRMED.toString()))
		        .collect(Collectors.toList());
		
		response.put("hasDiagnoses", !diagnoses.isEmpty());
		response.put("diagnoses", diagnosisList);
		response.put("provisionalDiagnoses", provisionalDiagnoses);
		response.put("confirmedDiagnoses", confirmedDiagnoses);
		response.put("diagnosisAttributes", diagnosisAttributes);
		response.put("diagnosis1048", diagnosis1048List);
		return response;
	}
	
	private SimpleObject mapDiagnosisToSimpleObject(Diagnosis diagnosis, Map<Integer, String> diagnosisAttributes) {
		SimpleObject diagnosisObj = new SimpleObject();
		diagnosisObj.put("id", diagnosis.getId());
		diagnosisObj.put("certainty", diagnosis.getCertainty().toString());
		diagnosisObj.put("diagnosisValue",
		    diagnosis.getDiagnosis().getCoded() != null ? diagnosis.getDiagnosis().getCoded().getUuid()
		            : diagnosis.getDiagnosis().getNonCoded());
		diagnosisObj.put("encounterType", diagnosis.getEncounter().getEncounterType().getName());
		
		if (diagnosisAttributes.containsKey(diagnosis.getId())) {
			diagnosisObj.put("attributeValue", diagnosisAttributes.get(diagnosis.getId()));
		}
		return diagnosisObj;
	}
}
