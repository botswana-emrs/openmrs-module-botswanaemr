/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import org.openmrs.Location;
import org.openmrs.Order;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.text.ParseException;
import java.util.List;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getDate;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.parseDateFromString;

public class TotalsPharmacyGraphSummaryFragmentController {
	
	public void controller(FragmentModel model, UiSessionContext sessionContext) throws ParseException {
		
		String MON = getDate().get(0);
		String TUE = getDate().get(1);
		String WED = getDate().get(2);
		String THUR = getDate().get(3);
		String FRI = getDate().get(4);
		String SAT = getDate().get(5);
		String SUN = getDate().get(6);
		
		Location location = sessionContext.getSessionLocation();
		model.addAttribute("sun", getPatientsSeenOnDay(SUN, location));
		model.addAttribute("mon", getPatientsSeenOnDay(MON, location));
		model.addAttribute("tue", getPatientsSeenOnDay(TUE, location));
		model.addAttribute("wed", getPatientsSeenOnDay(WED, location));
		model.addAttribute("thur", getPatientsSeenOnDay(THUR, location));
		model.addAttribute("fri", getPatientsSeenOnDay(FRI, location));
		model.addAttribute("sat", getPatientsSeenOnDay(SAT, location));
	}
	
	private Integer getPatientsSeenOnDay(String dateString, Location location) throws ParseException {
		int value = 0;
		List<Order> filledPrescription = BotswanaEmrUtils.getDrugOrders(parseDateFromString(dateString),
		    parseDateFromString(dateString), Order.FulfillerStatus.COMPLETED, location);
		if (!filledPrescription.isEmpty()) {
			value = filledPrescription.size();
		}
		
		return value;
	}
}
