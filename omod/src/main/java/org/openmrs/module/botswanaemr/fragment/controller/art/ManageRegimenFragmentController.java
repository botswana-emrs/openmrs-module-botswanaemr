/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.art;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaRegimenService;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.SimplifiedRegimen;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.List;

public class ManageRegimenFragmentController {
	
	public void controller(FragmentModel fragmentModel,
	        @FragmentParam(value = "regimenCategory", required = false) String regimenCategory, UiUtils ui) {
		
		BotswanaRegimenService regimenService = Context.getService(BotswanaRegimenService.class);
		List<SimplifiedRegimen> simplifiedRegimenList = new ArrayList<>();
		SimplifiedRegimen simplifiedRegimen = null;
		List<Regimen> allRegimen = regimenService.getAllRegimen(false);
		
		if (regimenCategory != null) {
			allRegimen = allRegimen.stream().filter(r -> r.getRegimenLine().getUuid().equals(regimenCategory))
			        .collect(java.util.stream.Collectors.toList());
		}
		
		if (allRegimen != null) {
			for (Regimen regimen : allRegimen) {
				simplifiedRegimen = new SimplifiedRegimen();
				simplifiedRegimen.setRegimenId(regimen.getRegimenId());
				simplifiedRegimen.setRegimenName(regimen.getRegimenName());
				simplifiedRegimen
				        .setRegimenLine(regimen.getRegimenLine().getCode() + "-" + regimen.getRegimenLine().getName());
				simplifiedRegimen.setConceptsName(regimen.getConceptRef().getDisplayString());
				
				simplifiedRegimenList.add(simplifiedRegimen);
			}
		}
		
		fragmentModel.addAttribute("regimen", simplifiedRegimenList);
	}
	
	// Get all TB regimens and return them as a list of SimplifiedRegimen
	public List<SimpleObject> getTbRegimens(UiUtils ui) {
		BotswanaRegimenService regimenService = Context.getService(BotswanaRegimenService.class);
		List<Regimen> allRegimen = regimenService.getAllRegimen(true);
		allRegimen = allRegimen.stream()
		        .filter(r -> r.getRegimenLine().getUuid().equals("75b76eae-7d8e-451b-871f-7718579a505f"))
		        .collect(java.util.stream.Collectors.toList());
		List<SimpleObject> simpleObjects = new ArrayList<>();
		for (Regimen regimen : allRegimen) {
			RegimenComponent regimenComponent = regimenService.getRegimenComponentByRegimen(regimen);
			if (regimenComponent != null) {
				List<RegimenComponentDrug> regimenComponentDrugs = regimenService
				        .getRegimenComponentDrugsByRegimenComponent(regimenComponent);
				if (regimenComponentDrugs != null && !regimenComponentDrugs.isEmpty()) {
					for (RegimenComponentDrug regimenComponentDrug : regimenComponentDrugs) {
						SimpleObject simpleObject = new SimpleObject();
						simpleObject.put("uuid", regimenComponentDrug.getDrug().getUuid());
						simpleObject.put("regimenName", regimenComponentDrug.getDrug().getConcept().getDisplayString() + " ("
						        + regimenComponentDrug.getStrength() + ")");
						simpleObjects.add(simpleObject);
					}
				}
			}
			
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("uuid", regimen.getRegimenId());
			simpleObject.put("regimenName", regimen.getRegimenName());
			simpleObjects.add(simpleObject);
			
		}
		
		return simpleObjects;
	}
}
