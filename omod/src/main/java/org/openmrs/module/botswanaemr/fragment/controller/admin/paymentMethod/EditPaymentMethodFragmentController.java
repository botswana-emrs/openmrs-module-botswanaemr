/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.admin.paymentMethod;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.fragment.controller.admin.PaymentMethodFragmentController;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class EditPaymentMethodFragmentController extends PaymentMethodFragmentController {
	
	public void get(PageModel model, @FragmentParam(value = "paymentMethodId", required = false) String paymentMethodId) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		PaymentMethod paymentMethod = service.getPaymentMethod(Integer.valueOf(paymentMethodId));
		if (paymentMethod == null) {
			paymentMethod = new PaymentMethod();
		}
		model.addAttribute("paymentMethod", paymentMethod);
	}
	
	public SimpleObject getPaymentMethod(FragmentModel model,
	        @RequestParam(value = "paymentMethodId") Integer paymentMethodId) {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		
		PaymentMethod paymentMethod = service.getPaymentMethod(paymentMethodId);
		if (paymentMethod == null) {
			paymentMethod = new PaymentMethod();
		}
		
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("id", paymentMethod.getPaymentMethodId());
		simpleObject.put("name", paymentMethod.getName());
		simpleObject.put("description", paymentMethod.getDescription());
		return simpleObject;
	}
	
	public void editPaymentMethod(@RequestParam(value = "editPaymentMethodId") Integer paymentMethodId,
	        @RequestParam(value = "editPaymentMethodName") String name,
	        @RequestParam(value = "editDescription") String description, UiUtils uiUtils) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		PaymentMethod paymentMethod = service.getPaymentMethod(paymentMethodId);
		if (paymentMethod != null && StringUtils.isNotEmpty(name)) {
			paymentMethod.setName(name);
			paymentMethod.setDescription(description);
			
			//save the service paymentMethod
			service.savePaymentMethod(paymentMethod);
		}
		
	}
	
	public String voidPaymentMethod(@RequestParam(value = "paymentMethodId") Integer methodId) {
		
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		PaymentMethod paymentMethod = service.getPaymentMethod(methodId);
		if (paymentMethod != null) {
			paymentMethod.setRetired(true);
			paymentMethod.setRetireReason("deleted");
			paymentMethod.setDateRetired(new Date());
			
			//save the payment method
			service.savePaymentMethod(paymentMethod);
		}
		
		return null;
	}
	
}
