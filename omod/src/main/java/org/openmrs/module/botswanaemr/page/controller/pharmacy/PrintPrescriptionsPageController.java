/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.pharmacy;

import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttribute;
import org.openmrs.User;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.DrugOrderSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatDateWithoutTime;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.formatPersonName;

@Slf4j
public class PrintPrescriptionsPageController {
		
	private final OrderService orderService = Context.getService(OrderService.class);
	
	public void controller(PageModel pageModel, @RequestParam(value = "patientId") Patient patient,
	        @RequestParam(value = "encounterId") Encounter encounter, UiUtils ui) {
		
		OrderType drugOrderType = orderService.getOrderTypeByUuid(BotswanaEmrConstants.DRUG_ORDER_TYPE_UUID);
		
		List<DrugOrderSimplifier> patientDrugOrders = new ArrayList<>();
		Set<Order> drugOrders = encounter.getOrders().stream().filter(order -> order.getPatient().equals(patient))
		        .filter(order -> order.getOrderType().equals(drugOrderType))
				.filter(order -> order.getAction().equals(Order.Action.NEW))
				.filter(order -> order.getPreviousOrder() == null)
		        .filter(order -> order.getFulfillerStatus() == null)
				.collect(Collectors.toSet());
		
		Order orderDetails = drugOrders.stream().findFirst().orElse(null);
		
		if (orderDetails != null) {
			User userDetails = Context.getUserService().getUsersByPerson(orderDetails.getOrderer().getPerson(), false)
			        .get(0);
			pageModel.addAttribute("creatorNames", formatPersonName(orderDetails.getOrderer().getPerson().getPersonName()));
			pageModel.addAttribute("creatorEmail", userDetails.getEmail());
			PersonAttribute creatorPhoneNumber = userDetails.getPerson().getAttribute("Telephone Number");
			if (creatorPhoneNumber != null) {
				pageModel.addAttribute("creatorPhoneNumber", creatorPhoneNumber);
			} else {
				pageModel.addAttribute("creatorPhoneNumber", "");
			}
			PersonAddress creatorAddress = getPersonAddress(userDetails.getPerson().getAddresses());
			if (creatorAddress != null) {
				pageModel.addAttribute("creatorAddress", creatorAddress);
			} else {
				pageModel.addAttribute("creatorAddress", "");
			}
		} else {
			pageModel.addAttribute("creatorNames", "");
			pageModel.addAttribute("creatorEmail", "");
			pageModel.addAttribute("creatorPhoneNumber", "");
			pageModel.addAttribute("creatorAddress", "");
		}
		
		pageModel.addAttribute("facility", encounter.getLocation().getName());
		pageModel.addAttribute("datePrescribed", formatDateWithoutTime(encounter.getEncounterDatetime(), "dd-MMM-yyyy"));
		pageModel.addAttribute("patientNames", formatPersonName(patient.getPersonName()));
		pageModel.addAttribute("patientAge", patient.getAge());
		pageModel.addAttribute("patientGender", patient.getGender());
		PersonAddress patientAddress = getPersonAddress(patient.getAddresses());
		if (patientAddress != null) {
			pageModel.addAttribute("patientAddress",
			    SimpleObject.fromObject(patientAddress, ui, "address2", "cityVillage:message", "address4", "address1"));
		} else {
			pageModel.addAttribute("patientAddress", "");
		}
		
		DrugOrder drugOrder;
		for (Order order : drugOrders) {
			if (order instanceof DrugOrder) {
				drugOrder = (DrugOrder) order;
				try {
					DrugOrderSimplifier drugOrderSimplifier = DrugOrderSimplifier.simplify(drugOrder);
					if (drugOrder.getDrug() !=null) {
						drugOrderSimplifier.setDrug(BotswanaEmrUtils.fetchAndformatDrugName(drugOrder.getDrug()));
					} else {
						drugOrderSimplifier.setDrug(drugOrder.getDrugNonCoded());
					}
					patientDrugOrders.add(drugOrderSimplifier);

				}
				catch (Exception e) {
					log.error("Error parsing dosing instructions", e);
				}
			}
		}
		pageModel.addAttribute("patientDrugOrders", patientDrugOrders);
	}
	
	private PersonAddress getPersonAddress(Set<PersonAddress> personAddresses) {
		return personAddresses.stream().findFirst().orElse(null);
	}
}
