/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.openmrs.Encounter;
import org.openmrs.Form;
import org.openmrs.Patient;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.VisitService;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LoadAllFormsFragmentController {
	
	public static final String VITALS_FORM_UUID = "a000cb34-9ec1-4344-a1c8-f692232f6edd";
	
	public static final String VISIT_NOTE_FORM_UUID = "c75f120a-04ec-11e3-8780-2b40bef9a44b";
	
	public static final String ADMISSION_SIMPLE_FORM_UUID = "d2c7532c-fb01-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String DISCHARGE_FORM_UUID = "b5f8ffd8-fbde-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String TRANSFER_WITHIN_HOSPITAL_FORM_UUID = "a007bbfe-fbe5-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String TRIAGE_SCREENING_FORM_UUID = "07fada0b-8c38-44df-bf3f-f1d4120ed697";
	
	private Visit currentPatientVisit;
	
	public void controller(FragmentModel model, UiUtils ui, @RequestParam("patientId") Patient patient,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService, @SpringBean("visitService") VisitService visitService,
	        @FragmentParam(value = "visit", required = false) Visit visit,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @RequestParam(value = "form", required = false) Form form, UiSessionContext sessionContext) {
		
		List<HtmlForm> allHtmlForms = htmlFormEntryService.getAllHtmlForms();
		boolean hasVisit = false;
		currentPatientVisit = BotswanaEmrUtils.getPatientActiveVisit(patient, sessionContext.getSessionLocation(), false);
		if (currentPatientVisit != null) {
			hasVisit = true;
		}
		
		//filter out the default form brought in by reffApp
		String vitalsForm = VITALS_FORM_UUID;
		String visitNoteForm = VISIT_NOTE_FORM_UUID;
		String admissionSimpleForm = ADMISSION_SIMPLE_FORM_UUID;
		String dischargeForm = DISCHARGE_FORM_UUID;
		String transferWithinHospitalForm = TRANSFER_WITHIN_HOSPITAL_FORM_UUID;
		String triageScreeningForm = TRIAGE_SCREENING_FORM_UUID;
		
		//form that will be clicked
		
		List<String> listOfFormsToExcludeUuids = Arrays.asList(vitalsForm, visitNoteForm, admissionSimpleForm, dischargeForm,
		    transferWithinHospitalForm, triageScreeningForm);
		//Only retain the forms created by the botswanaemr provider
		allHtmlForms.removeAll(excludedForms(listOfFormsToExcludeUuids, htmlFormEntryService, formService));
		
		Set<HtmlForm> completedHtmlForms = completedHtmlForms(patient, visitService, htmlFormEntryService);
		allHtmlForms.removeAll(completedHtmlForms);
		List<HtmlForm> list = new ArrayList<HtmlForm>();
		for (HtmlForm f : allHtmlForms) {
			if (f.getForm().getRetired() == false) {
				list.add(f);
			}
		}
		allHtmlForms = list;
		
		model.addAttribute("availbleForms", allHtmlForms);
		model.addAttribute("completedForms", completedHtmlForms);
		model.addAttribute("currentVisit", currentPatientVisit);
		model.addAttribute("patient", patient);
		model.addAttribute("hasVisit", hasVisit);
		model.addAttribute("encounters", getFilledEncounterForPatientPerTheCurrentVisit(patient, encounterService,
		    currentPatientVisit, listOfFormsToExcludeUuids));
		
	}
	
	private List<HtmlForm> excludedForms(List<String> uuids,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService,
	        @SpringBean("formService") FormService formService) {
		List<HtmlForm> allFormsToExclude = new ArrayList<HtmlForm>();
		for (String uuid : uuids) {
			Form form = formService.getFormByUuid(uuid);
			if (form != null) {
				HtmlForm htmlForm = htmlFormEntryService.getHtmlFormByForm(form);
				if (htmlForm != null) {
					allFormsToExclude.add(htmlForm);
				}
			}
		}
		return allFormsToExclude;
	}
	
	private List<Form> completedForms(Patient patient, @SpringBean("visitService") VisitService visitService) {
		List<Form> completedForms = new ArrayList<Form>();
		
		if (currentPatientVisit != null) {
			List<Encounter> encounterList = currentPatientVisit.getNonVoidedEncounters();
			for (Encounter encounter : encounterList) {
				completedForms.add(encounter.getForm());
			}
		}
		return completedForms;
	}
	
	private Set<HtmlForm> completedHtmlForms(Patient patient, @SpringBean("visitService") VisitService visitService,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService) {
		Set<HtmlForm> completedHtmlForms = new HashSet<HtmlForm>();
		for (Form form : completedForms(patient, visitService)) {
			completedHtmlForms.add(htmlFormEntryService.getHtmlFormByForm(form));
		}
		return completedHtmlForms;
	}
	
	private List<Encounter> getFilledEncounterForPatientPerTheCurrentVisit(Patient patient,
	        @SpringBean("encounterService") EncounterService encounterService, Visit currentVisit,
	        List<String> listOfFormsToExcludeUuids) {
		List<Encounter> encounterList = encounterService.getEncountersByPatient(patient);
		List<Encounter> neededEncounters = new ArrayList<>();
		for (Encounter encounter : encounterList) {
			if (currentVisit != null && encounter.getVisit().equals(currentVisit)
			        && !listOfFormsToExcludeUuids.contains(encounter.getForm().getUuid())) {
				neededEncounters.add(encounter);
			}
			
		}
		return neededEncounters;
	}
	
}
