/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller.pharmacy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.DrugOrder;
import org.openmrs.Order;
import org.openmrs.OrderAttribute;
import org.openmrs.OrderAttributeType;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.PrescriptionLabelSimplifier;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.StockUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class PrintPrescriptionLabelDetailsFragmentController {
	
	private final OrderService orderService = Context.getService(OrderService.class);
	
	public void controller(@FragmentParam("orderId") Integer orderId, UiUtils uiUtils, FragmentModel fragmentModel) {
		
		List<SimpleObject> simplifiedPrescriptionLabel;
		simplifiedPrescriptionLabel = SimpleObject.fromCollection(getSimplifiedLabel(orderId), uiUtils, "drug", "facility",
		    "frequency", "lotNumber", "expiryDate", "effectiveStartDate", "dispensedBy", "prescribedBy", "patientNames",
		    "dispensedQuantity", "dosingInstructions", "expiryDate", "duration", "drugOrderId", "dosage",
		    "quantityDispensed", "visitNumber");
		fragmentModel.addAttribute("prescriptionLabels", simplifiedPrescriptionLabel);
	}
	
	private List<PrescriptionLabelSimplifier> getSimplifiedLabel(Integer orderId) {
		List<PrescriptionLabelSimplifier> simplifiedPrescriptionLabels = new ArrayList<>();
		PrescriptionLabelSimplifier prescriptionLabelSimplifier;
		OrderAttributeType orderAttributeType = orderService
		        .getOrderAttributeTypeByUuid(BotswanaEmrConstants.ORDER_FULFILLER_NAME_ATTRIBUTE_TYPE_UUID);
		OrderAttributeType orderBatchNumberAttributeType = orderService
		        .getOrderAttributeTypeByUuid(BotswanaEmrConstants.ORDER_BATCH_NUMBER_ATTRIBUTE_TYPE_UUID);
		OrderAttributeType orderExpiryDateAttributeType = orderService
		        .getOrderAttributeTypeByUuid(BotswanaEmrConstants.ORDER_EXPIRY_DATE_ATTRIBUTE_TYPE_UUID);
		
		if (orderId != null) {
			Order order = orderService.getOrder(orderId);
			prescriptionLabelSimplifier = new PrescriptionLabelSimplifier();
			DrugOrder drugOrder;
			if (order instanceof DrugOrder) {
				drugOrder = (DrugOrder) order;
				String visitNumber = "";
				if (drugOrder.getEncounter() != null && drugOrder.getEncounter().getVisit() != null) {
					visitNumber = drugOrder.getEncounter().getVisit().getVisitId().toString();
				}
				visitNumber = drugOrder.getEncounter().getVisit().getVisitId().toString();
				prescriptionLabelSimplifier.setVisitNumber(visitNumber);
				prescriptionLabelSimplifier.setDrug(drugOrder.getDrug().getDisplayName());
				// check if the drug has a dosage form
				if (drugOrder.getDrug().getDosageForm() != null) {
					prescriptionLabelSimplifier.setDispensedUnits(drugOrder.getDrug().getDosageForm().getDisplayString());
				}
				if (drugOrder.getFrequency() != null) {
					prescriptionLabelSimplifier.setFrequency(drugOrder.getFrequency().getName());
				} else {
					prescriptionLabelSimplifier.setFrequency("");
				}
				Optional<OrderAttribute> orderBatchNumberAttribute = drugOrder.getActiveAttributes().stream()
				        .filter(attribute -> attribute.getAttributeType().equals(orderBatchNumberAttributeType)).findFirst();
				orderBatchNumberAttribute
				        .ifPresent(attribute -> prescriptionLabelSimplifier.setLotNumber(attribute.getValueReference()));
				Optional<OrderAttribute> orderExpiryDateAttribute = drugOrder.getActiveAttributes().stream()
				        .filter(attribute -> attribute.getAttributeType().equals(orderExpiryDateAttributeType)).findFirst();
				orderExpiryDateAttribute.ifPresent(attribute -> {
					try {
						Date expiryDate = BotswanaEmrUtils.formatFullDateFromStringWithTime(attribute.getValueReference());
						prescriptionLabelSimplifier
						        .setExpiryDate(BotswanaEmrUtils.formatDateWithoutTime(expiryDate, "dd/MM/yyyy"));
					}
					catch (ParseException e) {
						prescriptionLabelSimplifier.setExpiryDate("");
					}
				});
				String effectiveStartDate = BotswanaEmrUtils.formatDateWithTime(drugOrder.getEffectiveStartDate(),
				    "dd/MM/yyyy HH:mm:ss");
				prescriptionLabelSimplifier.setEffectiveStartDate(effectiveStartDate);
				Optional<OrderAttribute> orderAttribute = drugOrder.getActiveAttributes().stream()
				        .filter(attribute -> attribute.getAttributeType().equals(orderAttributeType)).findFirst();
				orderAttribute
				        .ifPresent(attribute -> prescriptionLabelSimplifier.setDispensedBy(attribute.getValueReference()));
				
				if (drugOrder.getOrderer() != null && drugOrder.getOrderer().getPerson() != null) {
					prescriptionLabelSimplifier
					        .setPrescribedBy(drugOrder.getOrderer().getPerson().getPersonName().getFullName());
				} else {
					prescriptionLabelSimplifier.setPrescribedBy("");
				}
				if (drugOrder.getDuration() != null) {
					prescriptionLabelSimplifier
					        .setDuration(drugOrder.getDuration() + " " + drugOrder.getDurationUnits().getDisplayString());
					prescriptionLabelSimplifier
					        .setQuantityDispensed(drugOrder.getQuantity());
				} else {
					prescriptionLabelSimplifier.setDuration("");
					prescriptionLabelSimplifier.setQuantityDispensed(0.0);
				}
				if (drugOrder.getQuantity() != null) {
					prescriptionLabelSimplifier.setQuantityDispensed(drugOrder.getQuantity());
				} else {
					prescriptionLabelSimplifier.setQuantityDispensed(0.0);
				}
				prescriptionLabelSimplifier.setDrugOrderId(drugOrder.getOrderId());
				if (drugOrder.getFrequency() != null) {
					prescriptionLabelSimplifier.setDosage(drugOrder.getFrequency().getName());
				} else {
					prescriptionLabelSimplifier.setDosage("");
				}
				if (drugOrder.getInstructions() != null) {
					ObjectMapper mapper = new ObjectMapper();
					try {
						PrescriptionDosingInstruction prescriptionDosingInstruction = mapper
						        .readValue(drugOrder.getInstructions(), PrescriptionDosingInstruction.class);
						if (StringUtils.isNotBlank(prescriptionDosingInstruction.getDosingInstructions())) {
							prescriptionLabelSimplifier
							        .setDosingInstructions(prescriptionDosingInstruction.getDosingInstructions());
						} else {
							prescriptionLabelSimplifier.setDosingInstructions("");
						}
					}
					catch (JsonProcessingException e) {
						prescriptionLabelSimplifier.setDosingInstructions("");
					}
				} else {
					prescriptionLabelSimplifier.setDosingInstructions("");
				}
				prescriptionLabelSimplifier.setFacility(drugOrder.getEncounter().getLocation().getName());
				prescriptionLabelSimplifier.setPatientNames(
				    BotswanaEmrUtils.formatPersonName(drugOrder.getEncounter().getPatient().getPersonName()));
				Integer dispensedQuantity = getDispensedQuantity(drugOrder, prescriptionLabelSimplifier.getLotNumber());
				prescriptionLabelSimplifier.setDispensedQuantity(dispensedQuantity.toString());
				simplifiedPrescriptionLabels.add(prescriptionLabelSimplifier);
			}
		}
		return simplifiedPrescriptionLabels;
	}
	
	private Integer getDispensedQuantity(DrugOrder drugOrder, String batchNumber) {
		Integer[] dispensedQuantity = new Integer[1];
		dispensedQuantity[0] = 0;
		IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
		
		Item item = null;
		OrderAttributeType orderItemCodeAttributeType = Context.getOrderService()
		        .getOrderAttributeTypeByUuid(BotswanaEmrConstants.ORDER_ITEM_CODE_ATTRIBUTE_TYPE_UUID);
		OrderAttribute orderItemCodeAttribute = drugOrder.getActiveAttributes().stream()
		        .filter(attribute -> attribute.getAttributeType().equals(orderItemCodeAttributeType)).findFirst()
		        .orElse(null);
		String itemCode = null;
		if (orderItemCodeAttribute != null) {
			itemCode = orderItemCodeAttribute.getValueReference();
		}
		if (itemCode != null) {
			IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
			item = iItemDataService.getItemByCode(itemCode);
		}
		
		// if that fails, get the item from the drug
		if (item == null) {
			item = StockUtils.getItemFromDrug(drugOrder.getDrug());
		}
		if (item != null) {
			StockOperationSearch stockOperationSearch = new StockOperationSearch();
			stockOperationSearch.setOperationNumberComparisonType(BaseObjectTemplateSearch.StringComparisonType.EQUAL);
			stockOperationSearch.getTemplate().setOperationNumber(batchNumber);
			stockOperationSearch.getTemplate().setItem(item);
			stockOperationSearch.getTemplate().setPatient(drugOrder.getPatient());
			
			StockOperation stockOperation = iStockOperationDataService.getOperations(stockOperationSearch).stream()
			        .findFirst().orElse(null);
			if (stockOperation != null) {
				final Item finalItem = item;
				stockOperation.getItems().forEach(stockOperationItem -> {
					if (stockOperationItem.getItem().equals(finalItem)) {
						dispensedQuantity[0] += stockOperationItem.getQuantity();
					}
				});
			}
		}
		return dispensedQuantity[0];
	}
	
	static class PrescriptionDosingInstruction {
		
		@JsonProperty("data")
		private List<Data> data;
		
		@JsonProperty("dosingInstructions")
		private String dosingInstructions;
		
		// Getters and Setters
		public List<Data> getData() {
			return data;
		}
		
		public void setData(List<Data> data) {
			this.data = data;
		}
		
		public String getDosingInstructions() {
			return dosingInstructions;
		}
		
		public void setDosingInstructions(String dosingInstructions) {
			this.dosingInstructions = dosingInstructions;
		}
	}
	
	static class Data {
		
		@JsonProperty("frequency")
		private String frequency;
		
		@JsonProperty("frequencyDisplay")
		private String frequencyDisplay;
		
		@JsonProperty("toDate")
		private String toDate;
		
		@JsonProperty("fromDate")
		private String fromDate;
		
		@JsonProperty("duration")
		private String duration;
		
		// Getters and Setters
		public String getFrequency() {
			return frequency;
		}
		
		public void setFrequency(String frequency) {
			this.frequency = frequency;
		}
		
		public String getFrequencyDisplay() {
			return frequencyDisplay;
		}
		
		public void setFrequencyDisplay(String frequencyDisplay) {
			this.frequencyDisplay = frequencyDisplay;
		}
		
		public String getToDate() {
			return toDate;
		}
		
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		
		public String getFromDate() {
			return fromDate;
		}
		
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
		
		public String getDuration() {
			return duration;
		}
		
		public void setDuration(String duration) {
			this.duration = duration;
		}
		
	}
}
