/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.fragment.controller;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.SeriesDataObject;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.fragment.FragmentModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getEncounters;

public class MonthlyGraphSummariesFragmentController {
	
	public void controller(FragmentModel model,
	        @FragmentParam(value = "type", required = false) BotswanaEmrConstants.summaryType typeOfSummary,
	        @FragmentParam(value = "encounterType", required = false) String encounterTypeUuid,
	        UiSessionContext uiSessionContext) {
		
		if (typeOfSummary == null) {
			typeOfSummary = BotswanaEmrConstants.summaryType.REGISTRATION;
		} else if (typeOfSummary != BotswanaEmrConstants.summaryType.PROGRAM) {
			typeOfSummary = BotswanaEmrConstants.summaryType.SCREENING;
		}
		
		Location sessionLocation = uiSessionContext.getSessionLocation();
		
		if (typeOfSummary.equals(BotswanaEmrConstants.summaryType.PROGRAM)) {
			List<Encounter> enrollmentEncounters = new ArrayList<>();
			List<SeriesDataObject> seriesData = new ArrayList<>();
			
			EncounterType encounterType = Context.getEncounterService()
			        .getEncounterTypeByUuid(BotswanaEmrConstants.TB_ENROLMENT_ENCOUNTER_TYPE_UUID);
			if (encounterType != null) {
				DateTime startDate = new DateTime(new Date());
				int monthOfYear = startDate.getMonthOfYear();
				startDate = new DateTime().withMonthOfYear(monthOfYear).withTimeAtStartOfDay();
				
				int firstWeekInMonth = startDate.withDayOfMonth(1).getWeekOfWeekyear();
				int lastWeekInMonth = startDate.dayOfMonth().withMaximumValue().getWeekOfWeekyear();
				
				while (firstWeekInMonth <= lastWeekInMonth) {
					
					DateTime firstDay = new DateTime().withWeekOfWeekyear(firstWeekInMonth)
					        .withDayOfWeek(DateTimeConstants.MONDAY).withTimeAtStartOfDay();
					DateTime lastDay = new DateTime().withWeekOfWeekyear(firstWeekInMonth)
					        .withDayOfWeek(DateTimeConstants.SUNDAY).withTimeAtStartOfDay().plusDays(1).minusMinutes(1);
					if (firstWeekInMonth == lastWeekInMonth) {
						lastDay = new DateTime().dayOfMonth().withMaximumValue().withTimeAtStartOfDay().plusDays(1)
						        .minusMinutes(1);
					}
					
					String name = "" + firstDay.dayOfMonth().getAsShortText() + " - " + lastDay.getDayOfMonth();
					
					enrollmentEncounters = getEncountersByDates(encounterType, sessionLocation, firstDay.toDate(),
					    lastDay.toDate());
					
					SeriesDataObject dataObject = new SeriesDataObject(name, enrollmentEncounters.size());
					seriesData.add(dataObject);
					
					firstWeekInMonth++;
				}
			}
			
			SimpleObject simpleObject = new SimpleObject();
			simpleObject.put("seriesData", seriesData);
			
			model.addAttribute("seriesData", simpleObject.toJson());
		}
	}
	
	private List<Encounter> getEncountersByDates(EncounterType encounterType, Location location, Date fromDate,
	        Date toDate) {
		return getEncounters(encounterType, location, fromDate, toDate);
	}
}
