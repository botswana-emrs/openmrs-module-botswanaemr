/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.familyplanning;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import java.util.ArrayList;
import java.util.List;

import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientIdentifierType;
import org.openmrs.Relationship;
import org.openmrs.Visit;
import org.openmrs.api.EncounterService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.page.PageModel;
import org.springframework.web.bind.annotation.RequestParam;

public class FamilyPlanningCardPageController {
	
	public Object controller(@RequestParam("patientId") Patient patient, final PageModel model,
	        @RequestParam(required = false, value = "visitId") Visit visit,
	        @RequestParam(required = false, value = "returnUrl") String returnUrl,
	        @SpringBean("patientService") PatientService patientService,
	        @SpringBean("encounterService") EncounterService encounterService) {
		
		List<String> identifierUuids = new ArrayList<>();
		identifierUuids.add(NATIONAL_ID_IDENTIFIER_TYPE);
		identifierUuids.add(BIRTH_CERT_IDENTIFIER_TYPE);
		identifierUuids.add(PASSPORT_IDENTIFIER_TYPE);
		identifierUuids.add(OPENMRS_ID_IDENTIFIER_TYPE);
		
		List<Relationship> relationships = Context.getPersonService().getRelationships(patient, null, null);
		List<Object> relations = new ArrayList<>();
		PatientIdentifier identifier = null;
		for (Relationship relationship : relationships) {
			relationship.getRelationshipType();
			relationship.getPersonB();
			Patient pat = patientService.getPatientByUuid(relationship.getPersonB().getUuid());
			if (pat != null) {
				identifier = pat.getPatientIdentifier(getCustomIdentificationType(identifierUuids));
				if (identifier == null) {
					identifier = pat.getPatientIdentifier();
				}
			}
			relations.add(relationship);
		}
		
		model.addAttribute("patient", patient);
		model.addAttribute("visit", visit);
		model.addAttribute("relations", relations);
		model.addAttribute("idValue", identifier);
		model.addAttribute("idType", getCustomIdentificationType(identifierUuids));
		model.addAttribute("returnUrl", returnUrl);
		
		return null;
	}
	
	private PatientIdentifierType getCustomIdentificationType(List<String> uuids) {
		for (String uuid : uuids) {
			PatientIdentifierType identifierType = Context.getPatientService().getPatientIdentifierTypeByUuid(uuid);
			if (identifierType != null) {
				return identifierType;
			}
		}
		return null;
	}
}
