/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.page.controller.consultation;

import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.module.appui.UiSessionContext;
import org.openmrs.module.botswanaemr.fragment.controller.shared.Prescription;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.ui.framework.UiUtils;
import org.openmrs.ui.framework.page.PageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

public class DrugPrescriptionPageController {
	
	public static final Logger logger = LoggerFactory.getLogger(DrugPrescriptionPageController.class);
	
	public void controller(@RequestParam("patientId") Patient patient,
	        @RequestParam(value = "returnUrl", required = false) String returnUrl,
	        @RequestParam(value = "visitId", required = false) Visit visit,
	        @RequestParam(value = "encounterId", required = false) Integer encounterId,
	        @RequestParam(value = "patientQueueId", required = false) Integer patientQueueId, PageModel pageModel,
	        UiSessionContext sessionContext, UiUtils uiUtils) {
		
		Encounter encounter = BotswanaEmrUtils.getDrugOrderEncounter(patient, visit, sessionContext.getSessionLocation());
		User currentUser = Context.getAuthenticatedUser();
		
		pageModel.addAttribute("patientUUID", patient.getUuid());
		pageModel.addAttribute("patient", patient);
		
		pageModel.addAttribute("encounter", encounter != null ? encounter.getUuid() : null);
		pageModel.addAttribute("patientVisitUUID", encounter != null ? encounter.getVisit().getUuid() : null);
		
		pageModel.addAttribute("patientQueueId", patientQueueId);
		pageModel.addAttribute("returnUrl", returnUrl);
		pageModel.addAttribute("location", sessionContext.getSessionLocation().getName());
		
		pageModel.addAttribute("drugOrder", BotswanaEmrUtils.getActivePatientDrugOrders(patient));
		
		pageModel.addAttribute("today", BotswanaEmrUtils.formatDateWithoutTime(new Date(), "yyyy-MM-dd"));
		
		if (currentUser != null) {
			pageModel.addAttribute("currentUser", currentUser.getUsername());
		}
		Prescription.addPrescription(pageModel, patient);
	}
	
}
