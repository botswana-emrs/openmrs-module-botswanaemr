<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.message("Quick Patient Registration") }",
            link: "${ ui.pageLink("botswanaemr", "quickRegistration") }"
        }
    ]));
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <h5>Patient registration form</h5>
                    <p>Complete the form to perform a quick patient registration</p>
                </div>

                <div class="col-3">
                    ${ui.includeFragment("botswanaemr", "widget/cancelAndGoBack")}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="center-container">
                <form id="quickPatientRegistrationForm">
                    <div class="card-title">
                        <h3 class="text-dark">Quick patient registration</h3>
                    </div>
                    <br/>
                    <h4 class="text-dark">Patient details</h4>
                    <br/>

                    <h3 class="text-black-50 body"></h3>

                    <p>
                        The quick registration process will collect basic information about a patient to allow for the dispensing of drugs prescribed at other facilities
                    </p>

                    <br/>
                    <span style="display: none">
                        ${ui.includeFragment("botswanaemr", "widget/facilityLocation")}
                    </span>

                    <div class="form-group">
                        <label for="name" class="text-dark">Full name</label>
                        <input type="text" class="form-control" id="name" aria-describedby="nameHelp"
                               name="fullName"
                               required placeholder="Enter the patient's full name">
                        <small id="nameHelp" class="form-text text-muted">Enter the patient's full name.</small>
                        <label id="nameError" class="error" for="name">This field is required.</label>
                    </div>

                    <div class="form-group">
                        <label for="dateOfBirth">Date Of Birth
                            <span class="required-label">*</span>
                        </label>
                        <input class="form-control birth-date" id="dateOfBirth"
                               name="dateOfBirth"
                               required placeholder="Patient's date of birth">
                        <label id="dateOfBirthError" class="error" for="dateOfBirth">This field is required.</label>
                    </div>

                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value="" id="dobEstimate">
                        <label class="form-check-label" for="dobEstimate"
                               style="margin-top: auto">
                            Date of birth estimated?
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="age">Age
                            <span class="required-label">*</span>
                        </label>
                        <input type="number" class="form-control" id="age" name="age" min="0"
                               required placeholder="Enter the patient's age">
                    </div>

                    <div class="form-group">
                        <label for="gender">Gender
                            <span class="required-label">*</span>
                        </label>
                        <select class="form-control custom-select" id="gender" name="gender" required>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <br/>
                    <input type="hidden" name="returnUrl" id="returnUrl" value="${returnUrl}">
                    <div>
                        <p style="display: inline">
                            <button id="next" type="submit" class=" btn btn-primary right ">${ui.message("Complete")}
                            </button>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
