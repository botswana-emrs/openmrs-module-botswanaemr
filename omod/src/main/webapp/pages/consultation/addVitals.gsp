<%
if (sessionContext.authenticated && !sessionContext.currentProvider) {
throw new IllegalStateException("Logged-in user is not a Provider")
}
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<%
ui.includeJavascript("botswanaemr", "utils/vitals-utils.js")

%>
<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard'
        },
        {label: "Add Vitals"}
    ];
    let activePatientAge = "${activePatient.person.age}";
</script>
<% ui.includeJavascript("botswanaemr", "vitalsValidation.js") %>

<script type="text/javascript">
    jq(function () {

        let newChecksForm = \$("#newChecksForm");
        if (newChecksForm.length > 0) {
            newChecksForm.validate({
                rules: {
                    systolic: {
                        digits: true,
                        max: 250,
                        min: 0
                    },
                    diastolic: {
                        digits: true,
                        max: 150,
                        min: 0
                    },
                    temp: {
                        number: true,
                        max: 43,
                        min: 25
                    },
                    weight: {
                        number: true,
                        max: 250,
                        min: 0
                    },
                    height: {
                        number: true,
                        max: 272,
                        min: 10
                    },
                    bmi: {
                        number: true
                    },
                    bsa: {
                        number: true
                    },
                    respiratory: {
                        number: true,
                        max: 99,
                        min: 0
                    },
                    pulse: {
                        number: true,
                        max: 230,
                        min: 0
                    },
                    hcircumference: {
                        number: true
                    },
                    glucoseLevel: {
                        number: true
                    },
                    oxygenSaturation: {
                        number: true,
                        max: 100,
                        min: 50
                    },
                }
            });
        }

        function updateBmi() {
            let wt = jq("#weight").val();
            let ht = jq("#height").val();

            let bmi = calculateBmi(wt, ht);

            if (bmi != null && !isNaN(bmi)) {
                jq("#bmi").val(bmi.toFixed(1));
            } else {
                jq("#bmi").val('');
            }
        }

        function updateBsa() {
            let wt = jq("#weight").val();
            let ht = jq("#height").val();

            let bsa = calculateBsa(wt, ht);

            if (bsa != null && !isNaN(bsa)) {
                jq("#bsa").val(bsa.toFixed(2));
            } else {
                jq("#bsa").val('');
            }
        }

        jq("#weight, #height").change(function (e) {
            updateBmi();
            updateBsa()
        });

        jq("#newChecksForm").submit(function (e) {
            e.preventDefault();
            let valid = \$("#newChecksForm").valid();
            if (!valid) {
                return false;
            }
    
            // Check if all text fields are empty
            let allEmpty = true;
            jq("#newChecksForm input[type='text']").each(function() {
                if (jq(this).val().trim() !== "") {
                    allEmpty = false;
                    return false; // break the loop
                }
            });

            if (allEmpty) {
                jq().toastmessage('showErrorToast', 'All fields are empty. Please fill in at least one field.');
                return false;
            }    

            // if encounter date is missing pop up toaast message
            if (jq("encounterDate").val() == "") {
                jq().toastmessage('showErrorToast', 'Encounter date is missing.');
                return false;
            }         

            var vitals = {
                'patientId': ${activePatient.patientId},
                'patientQueueId': "",
                'temp': jq('#temperature').val(),
                'sbp': jq('#systolic').val(),
                'dbp': jq('#diastolic').val(),
                'weight': jq('#weight').val(),
                'height': jq('#height').val(),
                'bmi': jq('#bmi').val(),
                'bsa': jq('#bsa').val(),
                'rRate': jq('#respiratory').val(),
                'pulse': jq('#pulse').val(),
                'hCircumference': jq('#hCircumference').val(),
                'bsa': jq('#glucoseLevel').val(),
                'oxygenSaturation': jq('#oxygenSaturation').val(),
                'lmnp': jq('#lmnp').val(),
                'pregnancyKnown': jq('#pregnancyKnown').val(),
                'conscious': jq('#conscious').val(),
                'responsive': jq('#responsive').val(),
                'vitals': true
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "newChecks", "captureVitals")}', vitals)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The vitals have been captured");
                    history.back();
                })

        });

        jq("#encounterDate").datepicker({
            setDate: new Date(),
            dateFormat: "yy-mm-dd",
                yearRange: "-150:+0",
                maxDate: 0,
            "autoclose": true,
        });
    });
</script>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        <div class="card pr-0 pl-0">
            <div class="row mb-4">
                <div class="col pr-0 pl-0">
                    <h6 class="mb-2 mt-2 pl-4 pr-4">Add Vitals</h6>
                    <hr class="divider pt-1"/>
                </div>
            </div>

            <div class="container-fluid">
                <div class="pr-4 pl-4">
                    <form method="post" id="newChecksForm">
                        <div class="row">
                            <div class="form-group col col-6 mb-6">
                                <label for="encounterDate"> Date:
                                <span class="text-danger">*</span>
                                </label>
                                <span class="input-group">
                                <input type="text"
                                    class="form-control py-2"
                                    value="" id="encounterDate"
                                    name="encounterDate" placeholder="Date vitals taken"/>
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group vital"">
                                    <label for="temp">Temperature (Degree Celcius):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="temp"
                                           name="temp" placeholder="Temperature (Degree Celcius)"/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col">
                                    <label>Blood pressure (mmHg)</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 mr-0 pr-0">
                                        <div class="form-group vital">
                                            <label for="diastolic" class="hidden">systolic</label>
                                            <input
                                                    id="systolic"
                                                    name="systolic"
                                                    type="text"
                                                    value=""
                                                    digits
                                                    class="form-control"/>
                                        </div>
                                    </div>

                                    <div class="text-separator ml-1 mr-1">
                                        <span style="font-size: 2rem">/</span>
                                    </div>

                                    <div class="col-md-3 ml-0 pl-0 vital">
                                        <label for="diastolic" class="hidden">diastolic</label>
                                        <input
                                                id="diastolic"
                                                name="diastolic"
                                                type="text"
                                                value=""
                                                digits
                                                class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group edit-section vital">
                                    <label for="pulse">Pulse(b/m):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="pulse"
                                           name="pulse" placeholder="Pulse"/>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group vital">
                                    <label for="weight">Weight(Kg):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="weight"
                                           name="weight" placeholder="Weight"/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group edit-section vital">
                                    <label for="height">Height(cm):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="height"
                                           name="height" placeholder="Height"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="bmi">BMI(Kg/M2):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="bmi"
                                           name="bmi" placeholder="BMI"/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group edit-section vital">
                                    <label for="bsa">BSA(M2):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="bsa"
                                           name="bsa" placeholder="BSA"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group vital">
                                    <label for="respiratory">Respiratory Rate(b/m):
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="respiratory"
                                           name="respiratory" placeholder="RR"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group edit-section vital">
                                    <label for="glucoseLevel">RBS:
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="glucoseLevel"
                                           name="glucoseLevel" placeholder="Glucose Level(mm/gl)"/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group vital">
                                    <label for="oxygenSaturation">Oxygen Saturation:
                                        
                                    </label>
                                    <input type="text" digits class="form-control border" id="oxygenSaturation"
                                           name="oxygenSaturation" placeholder="Oxygen Level"/>
                                </div>
                            </div>
                        </div>

                        <div class="border-0 mt-5">
                            <div class="row actions px-0">
                                <div class="col-12 px-0" id="buttonSection">
                                    <button type="button" id="cancel" class="btn btn-md btn-dark bg-dark float-right ml-1 pl-5 pr-5" onclick="history.back();">
                                        Back
                                    </button>
                                    <button type="submit" id="saveVitals"
                                            class="btn btn-md btn-primary float-right mr-0 pl-5 pr-5">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
