<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
    throw new IllegalStateException("Logged-in user is not a Provider")
}
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'
        },
        {label: "Print Vitals"}
    ];

    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div class="row">
    <div class="col-md-9 pl-3">
        <button id="print" onclick="printContent('#prescriptionsCard');" class="btn btn-primary float-left">
            <i class="fa fa-print fa-1x"></i> Print Vitals
        </button>
    </div>
    <div class="col-md-3 justify-content-end">
        ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
    </div>
</div>

<div class="row">
    <div class="col">
        <div id="prescriptionsCard" class="card">
            <h2 class="text-primary text-center">${facility}</h2>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <ul class="list-unstyled pl-5">
                        <li>${creatorNames}</li>
                        <li>${creatorAddresses}</li>
                    </ul>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>PH: <u style="text-decoration:underline dotted">${creatorPhoneNumber}</u></li>
                        <li>EM: ${creatorEmail}</li>
                    </ul>
                </div>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <ul class="list-unstyled pl-5">
                        <li>Patient Name: ${patientNames}</li>
                        <li>Address: ${patientAddresses}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>Age: ${patientAge}</li>
                        <li>Date: ${visitDate}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>Sex: ${patientGender}</li>
                    </ul>
                </div>
            </div>
            <hr class="divider bg-primary"/>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <h5 class='text-primary'>Patient's Vitals</h5>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row p-0">
                                        <div class="col p-0">Temperature(C): ${temperature}</div>
                                        <div class="col p-0">Weight(Kg): ${weight}</div>
                                        <div class="col p-0">Height(cm): ${height}</div>
                                        <div class="col p-0">Blood pressure(mmHg): ${systolicBp}&nbsp;/&nbsp;${diastolicBp}</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row p-0">
                                        <div class="col p-0">BMI(Kg/M2): ${bmi}</div>
                                        <div class="col p-0">BSA(M2): ${bsa}</div>
                                        <div class="col p-0">Pulse(b/m): ${pRate}</div>
                                        <div class="col p-0">Respiratory Rate(b/m): ${rRate}</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row p-0">
                                        <div class="col p-0">Glucose Level(mm/gl) ${glucoseLevel}</div>
                                        <% def gender = patient.gender %>
                                        <% if (gender.equals('F')) {%>
                                        <div class="col p-0">Last Menstrual Period: ${lmp}</div>
                                        <% } else { %>
                                        <div class="col p-0">&nbsp;</div>
                                        <% } %>
                                    </div>
                                </li>
                                <hr/>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row justify-content-center mt-4">
                <div class="col-md-3">
                    <ul class="list-unstyled pl-5">
                        <li>Health Practitioner's Signature:</li>
                    </ul>
                </div>
                <div class="col-md-9 pl-0 pr-0">
                    <hr style="border-bottom: 1px dotted #eee;"/>
                </div>
            </div>
        </div>
    </div>
</div>
