<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard'},
        { label: "Dashboard"}
    ];

    jq(document).ready(function() {
        toggleGraph();
    });
</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text">
                       <i class="fa fa-user-plus color-success border-success"></i> Total patients screened
                    </div>
                    <div class="stat-digit text-success">${allScreeningsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "SCREENING", encounterType: "7A4C7F96-F49D-43BA-BB51-D34C0B4C2F6B"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Screenings
                            <span class="text-danger">${dailyAverageScreenings}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
<div class="row collapsible">
    <div class="col collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's Screenings</div>
                    <div class="stat-digit text-success">${todayScreeningsCount}</div>
                        <div class="fullwidth">
                            ${ ui.includeFragment("botswanaemr", "todayGraphSummaries", [type: "SCREENING", encounterType: "7A4C7F96-F49D-43BA-BB51-D34C0B4C2F6B"]) }
                        </div>
                        <div class="clearfix">
                            <small class="pl-0">
                                Yesterday
                                <span class="text-danger">${yesterdayScreeningsCount}</span>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="col">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                    <div class="card mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                                <h4 class="pl-0"> Screening and triage patient pool</h4>
                            </div>
                        </div>
                        <div id="screeningPool" class="table-responsive">
                            ${ui.includeFragment("botswanaemr", "screeningAndTriagePatientPool")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
