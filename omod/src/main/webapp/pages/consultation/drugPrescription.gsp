<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "Drug Prescription",
            link: '${ui.pageLink("botswanaemr", "/consultation/drugPrescription", [patientId: patient.uuid])}'
        }
    ];

</script>

<script type="text/javascript">

    jQuery(document).ready(function () {
        let location = `${location}`;
        let currentUser = `${currentUser}`

        // show loading overlay if an ajax request is made
        jQuery.ajaxSetup({
            beforeSend: function() {
                showLoadingOverlay();
            },
            complete: function() {
                hideLoadingOverlay();
            }
        });

        // set facility with current location
        jq("#facility").val(location);
        jq("#prescriber").val(currentUser);


        fetchFacilities();

        function fetchFacilities() {
            let parameter = {};
            jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchFacilities.action', parameter)
            .done(function(data) {
                if (data.status === 'success') {
                    if (data.locations) {
                        var facilities = data.locations;
                        jq("#facility").autocomplete({
                            source: facilities,
                            change: function(event, ui) {
                                if (!ui.item) {
                                    jq("#facility").val("");
                                }
                            }
                        });
                    }
                }
            });
        }

        let simplifiedDrugs = [];
        const drugMap = new Map();
        //getSimplifiedDrugs
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','consultation/drugPrescription','getSimplifiedDrugs')}",
            dataType: "json",
            success: (result) => {
                result.data.forEach((simplifiedDrug) => {
                    simplifiedDrugs.push(simplifiedDrug.name);
                    drugMap.set(simplifiedDrug.name, simplifiedDrug.uuid);
                });
            }
        });

        let simplifiedNonDrugs = [];
        const nonDrugMap = new Map();
        //getSimplifiedDrugs
        /*jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','stock/allProducts','getAllProducts')}",
            data: {start: 1, length: 100},
            dataType: "json",
            success: (result) => {
                result.aaData.forEach((simplifiedNonDrug) => {
                    simplifiedNonDrugs.push(simplifiedNonDrug.name);
                    drugMap.set(simplifiedNonDrug.name, simplifiedNonDrug.code);
                });
            }
        });*/


        jq('#drug').autocomplete({
            source : simplifiedDrugs
        })

        jq('#non-drug').autocomplete({
            source: function(request, response) {
                jq.getJSON('${ ui.actionLink("botswanaemr", 'stock/allProducts','getAllProducts') }', {
                    'search[value]': request.term, start: 1, length: 10
                }).success(function(payload) {
                    // console.log(data.aaData);
                    let results = [];
                    payload.aaData.forEach((item) => {
                        let result = {
                            label: item.name + ' (' + item.code + ')',
                            value: item.id // Explore using Concept Id instead
                        };
                        results.push(result);
                    });
                    response(results);
                });
            },
            minLength: 3,
            select: function(event, ui) {
                event.preventDefault();
                jq('#non-drug').val(ui.item.label);
                jq('#non-drug').attr('data-overlay',ui.item.value);
            },
        })


        const key = "orderBasket";

        const orderBasketStore = () => {
            // Fetch the item from the localStorage and filter by the patient.uuid parameter
            let basket = JSON.parse(window.localStorage.getItem(key));
            if (basket) {
                return basket.filter(item => item.patient.uuid == '${patientUUID}')
            }

            return basket;
        }

        const addToBasket = (value) => {
            let basket = orderBasketStore();
            if (basket) {
                basket.push(value);
            } else {
                basket = [value];
            }
            window.localStorage.setItem(key, JSON.stringify(basket));
        }

        function removeFromOrderBasket(value) {
            let basket = orderBasketStore();
            if (basket) {
                basket = basket.filter(item => item !== value);
                window.localStorage.setItem(key, JSON.stringify(basket));
            }
        }

        function removeFromOrderBasketAtIndex(index) {
            let basket = orderBasketStore();
            if (basket) {
                let value = basket[index];
                basket = basket.filter(item => item !== value);
                window.localStorage.setItem(key, JSON.stringify(basket));
            }            
        }

        function getOrderBasket() {
            return orderBasketStore();
        }

        function clearOrderBasket() {
            window.localStorage.removeItem(key);
        }

        function removeFromBasket(value) {
            const basket = orderBasketStore();
            if (basket) {
                const index = basket.indexOf(value);
                if (index > -1) {
                    basket.splice(index, 1);
                }
            }
            window.localStorage.setItem(key, JSON.stringify(basket));
        }

        const acc = document.getElementsByClassName("accordion");
        let i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight) {
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

        jQuery(document).on('click','#btnAddRow', function () {
            let lastRow  = jQuery('#drug-prescription-form tbody:visible>tr:last');
            lastRow
                .clone(true)
                .insertAfter(lastRow).hide().fadeIn().find('input').each(function () {
                jQuery(this).val('');
            });
        });

        const numberOfDays = (date1, date2) => {
            const Difference_In_Time = date2.getTime() - date1.getTime();

            return Difference_In_Time / (1000 * 3600 * 24);
        }

        const calculateDuration = (fromDate, toDate) => {
            const fromDateObject = new Date(fromDate);
            const toDateObject = new Date(toDate);
            if (fromDateObject != null && toDateObject != null) {
                if (fromDateObject.getTime() > toDateObject.getTime()) {
                    return 0;
                } else {
                    return numberOfDays(fromDateObject, toDateObject);
                }
            } else {
                return 0;
            }
        }

        const getInputValues = (name) => {
            const values = [];
            jQuery("#drug-prescription-form input[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const getSelectValues = (name) => {
            const values = [];
            jQuery("#drug-prescription-form select[name='" + name + "[]']:enabled").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const getSelectedDisplay = (name) => {
            const displays = [];
            jQuery("#drug-prescription-form select[name='" + name + "[]']:enabled option:selected").each(function () {
                displays.push(jQuery(this).text().trim());
            });
            return displays;
        }

        const validateFields = (data) => {
            const nullFields = [];
            for (const value of data) {
                if (!value) {
                    nullFields.push(value);
                }
            }
            return nullFields;
        }

        const goBack = (event) => {
            event.preventDefault();
            window.history.back();
        }

        const orderBasket = orderBasketStore();

        const displayEmptyState = function() {
            const emptyState = '<div class="empty-state" style="margin: 12px;">' +
                '<div class="empty-state-icon">' +
                '<i class="fas fa-shopping-cart"></i>' +
                '</div>' +
                '<div class="empty-state-title">' +
                '<h4>No drug orders in basket</h4>' +
                '</div>' +
                '<div class="empty-state-message">' +
                '<p>Add drug orders to basket to continue</p>' +
                '</div>' +
                '</div>';
            jQuery("#order-basket").append(emptyState);            
        }

        const getDrugTitle = function() {
            return jq("#drug").val() + ' (' + jq("#dosage").val() + ' ' + jq("#doseUnit option:selected").text() + ')';
        }

        window.onload = function () {
            if (orderBasket) {
                document.getElementById("order-count").innerHTML = orderBasket.length;
            }
        }

        const updateOrderBasket = () => {
            const orderBasket = orderBasketStore();
            jQuery("#order-basket").empty();

            if (!orderBasket || orderBasket.length == 0) {
                displayEmptyState();
            }

            if (orderBasket) {
                let section = "";
                let index = 0;
                orderBasket.forEach(function (order) {
                    delete order.instructions;

                    const item = order;

                    if (Object.keys(order.drug).length !== 0) {
                        const dosingInstructions = order.dosingInstructions;

                        const heading = "<h6 class='card-title'> Additional Dosing instructions </h6>";
                        let instructions = heading + "<ul style='list-style-type:circle; margin: 4px;'>";

                        dosingInstructions.data.forEach(function (instruction) {
                            let singularDuration = 0;
                            if (instruction.fromDate && instruction.toDate) {
                                calculateDuration(instruction.fromDate, instruction.toDate);
                            } else {
                                singularDuration = instruction.duration;
                            }
                            instructions += "<li> <i>" + instruction.frequencyDisplay + " for " + singularDuration + " days </i></li>";
                        });

                        if (dosingInstructions.dosingInstructions) {
                            instructions += "<li> <i>" + dosingInstructions.dosingInstructions + "</i></li>";
                        }
                        instructions += '</ul>';

                        section = "<ul style='list-style-type:square; margin: 10px;'>"
                            + "<li>"
                            + order.rawData.drug
                            + ", " + order.rawData.dosage
                            + " " + order.rawData.doseUnit
                            + " - " + order.rawData.route
                            + instructions
                            + "</li>"
                            + ' <button id="btn-remove-basket" class="btn btn-sm btn-danger" data-index="' + index + '" data-druguuid="' + order.drug.uuid + '">'
                            + '<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>'
                            + '</button>'
                            + "</ul>";
                    } else {
                        const nonDrugInstructions = order.nonDrugInstructions;
                        const heading = "<h6 class='card-title'> Non-Drug instructions </h6>";
                        let instructions = heading + "<ul style='list-style-type:circle; margin: 4px;'><li> <i>" + nonDrugInstructions + "</i></li></ul>";

                        section = "<ul style='list-style-type:square; margin: 10px;'>"
                            + "<li>"
                            + order.rawData.drug
                            + instructions
                            + "</li>"
                            + ' <button id="btn-remove-basket" class="btn btn-sm btn-danger" data-index="' + index + '" data-druguuid="' + order.drugNonCoded.id + '">'
                            + '<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>'
                            + '</button>'
                            + "</ul>";
                    }
                    jQuery("#order-basket").append(section);
                    index++;
                });
                document.getElementById("order-count").innerHTML = orderBasket.length;
            }
        }

        jQuery("#btn-back").click(function (event) {
            goBack(event);
        });

        jQuery(document).on("click", "#btn-remove-basket", function(event) {
            const drugUuid = jQuery(this).data("druguuid");
            const index = jQuery(this).data("index");
            removeFromOrderBasketAtIndex(index);
            updateOrderBasket();
            console.log("id", drugUuid);
        });


        jQuery("#btn-clear").click(function () {
                clearOrderBasket();
                jQuery("#order-basket").empty();
                document.getElementById("order-count").innerHTML = 0;
            }
        );

        jQuery('#btnAddToBasket').click(() => {
            const dosages = getInputValues('dosage');
            const toDates = getInputValues('toDate');
            const fromDates = getInputValues('fromDate');
            const startDates = getInputValues('startDate');

            const drugs = getInputValues('drug');
            const nonDrugId = jq('#non-drug').attr('data-overlay');
            const refillRepeats = getInputValues('refillRepeat');
            const doseUnits = getSelectValues('doseUnit');
            const frequencies = getSelectValues('frequency');
            const frequencyDisplays = getSelectedDisplay('frequency');
            const routes = getSelectValues('route');
            const durations = getInputValues('duration');
            const nonDrugInstructions = jQuery('#instructions').val();
            const quantity = jQuery('#quantity')
            const quantityUnits = jQuery('#quantityUnit')

            const errors = [];
            if (!jQuery("#choose-non-drug").is(':checked')) {
                errors.push(...validateFields(dosages),...validateFields(frequencies),
                    ...validateFields(drugs), ...validateFields(refillRepeats),
                    ...validateFields(doseUnits), ...validateFields(routes));

                if (jQuery("#choose-frequency").is(':checked')) {
                    errors.push(...validateFields(toDates), ...validateFields(fromDates));
                } else {
                    errors.push(...validateFields(durations), ...validateFields(startDates));
                }
            } else {
                errors.push(...validateFields(nonDrugId), ...validateFields(quantity), ...validateFields(quantityUnits));
            }

            if (errors.length > 0) {
                jq().toastmessage('showErrorToast', "Null Fields: Make sure you fill all the fields");
            } else {
                let payload = {};
                if (!jQuery("#choose-non-drug").is(':checked')) {
                    const data = [];
                    for (let i = 0; i < frequencies.length; i++) {
                        data.push({
                            frequency: frequencies[i],
                            frequencyDisplay: frequencyDisplays[i],
                            toDate: toDates[i],
                            fromDate: fromDates[i],
                            duration: durations[i],
                            startDate: startDates[i]
                        });
                    }

                    let duration = 0;
                    data.forEach(drug => {
                        if (jq("#choose-frequency").is(':checked')) {
                            duration += calculateDuration(new Date(drug.fromDate), new Date(drug.toDate));
                        } else {
                            duration += parseInt(drug.duration);
                        }
                    });

                    let frequency = '';
                    if (jq("#choose-frequency").is(':checked')) {
                        frequency = jq('#frequency-dates').val();
                    } else {
                        frequency = jq('#frequency-duration').val();
                    }

                    const instructions = {
                        data: data,
                        dosingInstructions: jQuery('#dosageInstructions').val()
                    }

                    payload = {
                        patient: {
                            uuid: "${patientUUID}"
                        },
                        careSetting: {
                            uuid: "${careSettingUuid}"
                        },
                        encounter: {
                            uuid: "${encounter}"
                        },
                        orderer: {
                            uuid: "${orderer}"
                        },
                        orderType: {
                            uuid: "${orderType}"
                        },
                        drug: {
                            uuid: drugMap.get(jq("#drug").val())
                        },
                        drugNonCoded: getDrugTitle(),
                        dosingType: "${dosingType}",
                        dose: jq("#dosage").val(),
                        doseUnits: {
                            uuid: jq("#doseUnit").val()
                        },
                        frequency: {
                            uuid: frequency
                        },
                        numRefills: jq("#refillRepeat").val(),
                        quantity: "0",
                        quantityUnits: {
                            uuid: "${quantityUnit}"
                        },
                        duration: duration,
                        durationUnits: {
                            uuid: "${durationUnits}"
                        },
                        route: {
                            uuid: jq("#route").val()
                        },
                        type: "${type}",
                        dosingInstructions: instructions,
                        instructions: JSON.stringify(instructions),
                        rawData: {
                            data: data,
                            drug: jq("#drug").val(),
                            route: jq("#route option:selected").text().trim(),
                            dosage: jq("#dosage").val(),
                            doseUnit: jq("#doseUnit option:selected").text().trim()
                        }
                    }
                } else {
                    payload = {
                        patient: {
                            uuid: "${patientUUID}"
                        },
                        careSetting: {
                            uuid: "${careSettingUuid}"
                        },
                        encounter: {
                            uuid: "${encounter}"
                        },
                        orderer: {
                            uuid: "${orderer}"
                        },
                        orderType: {
                            uuid: "${orderType}"
                        },
                        concept: {
                            uuid: "163101AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                        },
                        drug:{},
                        drugNonCoded: jq("#non-drug").val(),
                        dosingType: "org.openmrs.FreeTextDosingInstructions",
                        dose: "0",
                        doseUnits: {
                            uuid: "162381AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                        },
                        frequency: {
                            uuid: "162135AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                        },
                        numRefills: "0",
                        quantity: "0",
                        quantityUnits: {
                            uuid: "162399AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                        },
                        duration: "0",
                        durationUnits: {
                            uuid: "162582AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                        },
                        route: null,
                        type: "${type}",
                        dosingInstructions: nonDrugInstructions,
                        nonDrugInstructions: nonDrugInstructions,
                        rawData: {
                            drug: jq("#non-drug").val(),
                        }
                    }
                }

                showLoadingOverlay();

                // Add to basket
                addToBasket(payload);
                updateOrderBasket();
                window.location.reload();
            }
        });

        // Add to basket
        updateOrderBasket();

        jQuery("#choose-non-drug").change(function() {
            if (jq(this).is(":checked")) {
                jq("#non-drug-prescription-form").show();
                jq("#drug-prescription-form").hide();
                jq("prescription-title").text("Non-drug prescription form");
            } else {
                jq("#non-drug-prescription-form").hide();
                jq("#drug-prescription-form").show();
                jq("prescription-title").text("Drug prescription form");
            }
        });

        jQuery("#btn-reset").click(function () {
                window.history.back();
            }
        );

        jQuery("#btn-save-orders").click(function () {
                let promises = [];

                orderBasket.forEach(drugOrder => {
                    const drugOrderItem = { ...drugOrder};
                    const instructions = drugOrder.dosingInstructions;

                    //remove rawData from drugOrder
                    delete drugOrderItem.rawData;
                    delete drugOrderItem.nonDrugInstructions;

                    // If handling a non-drug prescription, remove the drug object from the payload
                    if (drugOrderItem.drug.uuid === undefined) {
                        delete drugOrderItem.drug;
                    } else {
                        delete drugOrderItem.dosingInstructions;
                    }

                    console.log("payload", drugOrderItem)
                    showLoadingOverlay();
                    promises.push(new Promise((resolve, reject) => {
                        jq.ajax({
                            type: "POST",
                            url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/order",
                            dataType: "json",
                            data: JSON.stringify(drugOrderItem),
                            contentType: "application/json",
                            success: function (data) {
                                let orderUuid = data.uuid;
                                jq.getJSON('${ui.actionLink("botswanaemr", "search", "closePatientQueue")}&patientQueueId=' + ${patientQueueId} + '&encounterId=' + drugOrder.encounter.uuid
                                ).success(function (data) {
                                    let params = {
                                        orderUuid: orderUuid,
                                        facility: jq("#facility").val(),
                                        prescriber: jq("#prescriber").val()
                                    }
                                    jq.getJSON('${ui.actionLink('botswanaemr','search','updateOrderAttributes')}', params)
                                        .done(function (data) {
                                            resolve(orderBasket.indexOf(drugOrder));
                                            // jq().toastmessage('showSuccessToast', "Prescriptions added successfully");
                                        }).fail(function() {
                                            reject("An error occurred while adding Prescriptions!");
                                            // jq().toastmessage('showErrorToast', "An error occurred while adding Prescriptions!");
                                    })
                                }).fail(function(e) {
                                    reject(e);
                                    // jq().toastmessage('showErrorToast',  "Error");
                                });

                            },
                            error: (error) => {
                                let errorMessage = '';
                                if (error.responseJSON == undefined) {
                                    errorMessage = 'Something went wrong while posting the prescription';
                                } else {
                                    errorMessage = error.responseJSON.message;
                                }
                                if (errorMessage === 'Order.cannot.have.more.than.one') {
                                    errorMessage = "Duplicate prescription"
                                }
                                reject(errorMessage);
                                // jq().toastmessage('showErrorToast', "Error: " + errorMessage);
                            }
                        });
                    }));
                })
                Promise.allSettled(promises)
                    .then((results) => {
                        let allSuccessful = true;
                        let errors = [];
                        results.forEach((result) => {
                           if (result.status === 'rejected') {
                                allSuccessful = false;
                                errors.push(result.reason);
                           } else {
                             // clear item from basket
                             removeFromOrderBasketAtIndex(result.value);
                           }
                        });

                        if (allSuccessful) {
                            jq().toastmessage('showSuccessToast', "Prescriptions added successfully");
                            clearOrderBasket();
                            urlUtils.redirectToReturnUrl();
                        } else {
                            let errorMessage = errors.join('</br>');
                            jq().toastmessage('showErrorToast', errorMessage);
                            window.location.reload();
                        }
                    });
            }
        );

        // Toggle frequency and duration
        jQuery('#choose-frequency').change(function() {
            let chooseDuration = !jQuery(this).is(':checked'); 
            jQuery("#frequency-with-dates").toggleClass('hidden');
            jQuery("#frequency-with-dates").find('input,select').attr('disabled', chooseDuration);
            jQuery("#frequency-simple").toggleClass('hidden');
            jQuery("#frequency-simple").find('input,select').attr('disabled', !chooseDuration);
        });

        jQuery('#choose-frequency').attr('checked', false);
        jQuery("#frequency-with-dates").find('input,select').attr('disabled',true);

        // Toggle visibility if the numRefills section based on whether or not the confirmRefill checkbox has been checked
        jQuery('#confirmRefill').change(function() {
            let isChecked = jQuery(this).is(':checked');
            jQuery("#numRefills").toggleClass('hidden', !isChecked);
        });

    });
</script>

<style>
.icon-badge-group {

}

.icon-badge-group .icon-badge-container {
    display: inline-block;
    margin-left: 15px;
}

.icon-badge-group .icon-badge-container:first-child {
    margin-left: 0
}

.icon-badge-container {
    margin-top: 20px;
    position: relative;
}

.icon-badge-icon {
    font-size: 30px;
    position: relative;
}

.icon-badge {
    background-color: red;
    font-size: 12px;
    color: white;
    margin: 10px;
    text-align: center;
    width: 20px;
    height: 20px;
    border-radius: 35%;
    position: absolute; /* changed */
    top: -5px; /* changed */
    left: 18px; /* changed */
}

.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.accordion:after {
    content: '+';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "-";
}

.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
}
</style>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient])}
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input id="choose-non-drug" name="choose-non-drug" type="checkbox" class="form-check-input">
                        <label for="choose-non-drug" class="form-check-label">Non-drug prescription?</label>
                    </div>
                </div>
            </div>

            <div class="card-header bg-primary m-b-16" id="prescription-title">
                <h4 class="card-title text-white">Drug Prescription Form</h4>
            </div>

            <div class="card-body" id="non-drug-prescription-form" style="display: none">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="drug">Item</label>
                            <input id="non-drug" name="non-drug[]" type="text" placeholder=""
                                   class="form-control input-lg">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="quantity">Quantity</label>
                            <input id="quantity" name="quantity[]" type="number" placeholder="" min="1" max="500"
                                   class="form-control input-lg">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="quantityUnit">Units</label>
                            <select name="quantityUnit[]" id="quantityUnit" class="form-control input-lg">
                                <option value="">select unit</option>
                                <% quantityUnits.each { %>
                                <option value="${it.getUuid()}">${it.getDisplayString()}</option>
                                <% } %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin: 10px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="instructions">Instructions</label>
                            <textarea id="instructions" name="instructions[]" class="form-control"
                                      rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body" id="drug-prescription-form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="drug">Drug</label>
                            <input id="drug" name="drug[]" type="text" placeholder=""
                                   class="form-control input-lg">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="route">Route</label>
                            <select name="route[]" id="route" class="form-control input-lg">
                                <option value="">select route</option>
                                <% routes.each { %>
                                <option value="${it.getUuid()}">
                                    ${it.getDisplayString()}
                                </option>
                                <% } %>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosage">Dose</label>
                            <input id="dosage" name="dosage[]" type="number" placeholder=""
                                   class="form-control input-lg">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="doseUnit">Units</label>
                            <select name="doseUnit[]" id="doseUnit" class="form-control input-lg">
                                <option value="">select unit</option>
                                <% dosageUnits.each { %>
                                <option value="${it.getUuid()}">${it.getDisplayString()}</option>
                                <% } %>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="facility">Source facility</label>
                            <input id="facility" name="facility[]" type="text" placeholder=""
                                   class="form-control input-lg"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="facility">Prescriber</label>
                            <input id="prescriber" name="prescriber[]" type="text" placeholder=""
                                   class="form-control input-lg" readonly/>
                        </div>
                    </div>
                </div>

                <div class="row">
                <!-- Checkbox to confirm refill-->
                    <div class="col-md-6">
                        <div class="form-group pl-4">
                            <input id="confirmRefill" name="confirmRefill[]" type="checkbox" class="form-check-input" value="0" min="0" max="6">
                            <label for="confirmRefill" class="form-check-label">Refill?</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group hidden" id="numRefills">
                            <label for="refillRepeat">Number of Refills</label>
                            <input id="refillRepeat" name="refillRepeat[]" type="number" class="form-control" value="0" min="0" max="6">
                        </div>
                    </div>
                </div>

                <div class="card-header bg-primary m-b-16">
                    <h5 class="card-title text-white">Dosage Instructions</h5>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-check">
                            <input id="choose-frequency" name="choose-frequency" type="checkbox" class="form-check-input">
                            <label for="choose-frequency" class="form-check-label">Frequency and dates</label>
                        </div>
                    </div>
                </div>
                <div class="row m-b-24 hidden" id="frequency-with-dates">
                    <table class="table table-bordered table-hover w-auto">
                        <thead>
                        <tr>
                            <th class="text-center">
                                Frequency
                            </th>
                            <th class="text-center">
                                From
                            </th>
                            <th class="text-center">
                                To
                            </th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <tr id='row'>
                            <td>
                                <select name="frequency[]" id="frequency-dates" class="form-control input-md">
                                    <option value="">select frequency</option>
                                    <% frequencies.each { %>
                                    <option value="${it.getUuid()}">
                                        ${it.getName()}
                                    </option>
                                    <% } %>
                                </select>
                            </td>
                            <td>
                                <label>
                                    <input id="fromDate" name="fromDate[]" type="date"
                                           class="form-control input-md">
                                </label>
                            </td>
                            <td>
                                <label>
                                    <input id="toDate" name="toDate[]" type="date"
                                           class="form-control input-md">
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <div class="col-md-12 pl-0 pr-0">
                        <button class="dashed-button rounded" id="btnAddRow">+ Add row</button>
                    </div>
                </div>
                <div class="row m-b-24" id="frequency-simple">
                    <table class="table table-bordered table-hover w-auto">
                        <thead>
                        <tr>
                            <th class="text-center">
                                Frequency
                            </th>
                            <th class="text-center">
                                Start Date
                            </th>
                            <th class="text-center">
                                Duration (Days)
                            </th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <tr id='row'>
                            <td>
                                <select name="frequency[]" id="frequency-duration" class="form-control input-md">
                                    <option value="">select frequency</option>
                                    <% frequencies.each { %>
                                    <option value="${it.getUuid()}">
                                        ${it.getName()}
                                    </option>
                                    <% } %>
                                </select>
                            </td>
                            <td>
                                <label>
                                    <input id="startDate" name="startdate[]" type="date" value="${today}"
                                           class="form-control input-md">
                                </label>
                            </td>
                            <td>
                                <label>
                                    <input id="duration" name="duration[]" type="number" max="90" min="0"
                                           class="form-control input-md">
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <div class="col-md-12 pl-0 pr-0">
                        <button class="dashed-button rounded" id="btnAddRow">+ Add row</button>
                    </div>
                </div>
                <div class="row" style="margin: 10px;">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosageInstructions">Dosage Instructions (optional)</label>
                            <textarea id="dosageInstructions" name="dosageInstructions" class="form-control" placeholder="Additional dosing instructions (e.g. take with food, once a day at 8am)"
                                      rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row card-footer pl-0 pr-0">
                <div class="col-md-6">
                    <button id="btn-reset" type="button" class="btn btn-dark bg-dark float-left ml-0">Discard</button>
                </div>

                <div class="col-md-6">
                    <button id="btnAddToBasket" type="button" class="btn btn-primary float-right mr-0">Add to basket</button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <% if (drugOrder.size > 0 ) { %>
        <div class="card">
            <div class="card-header bg-primary m-b-16">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="card-title text-white">Active Prescriptions</h3>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div id="active-prescriptions">
                    <% drugOrder.each { %>
                    <div class="mb-2">
                        <strong>${it.drug}</strong>
                        <br/>
                        ${it.dosage} , ${it.frequency}, ${it.route} 
                    </div>
                    <hr>
                    <% } %>
                </div>
            </div>
        </div>
        <% } %>

        <div class="card">
            <div class="card-header bg-primary m-b-16">
                <div class="row">
                    <div class="col-md-6">
                        <div class="icon-badge-container">
                            <i class="fa fa-shopping-basket icon-badge-icon"></i>

                            <div class="icon-badge" id="order-count">0</div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3 class="card-title text-white">Order Basket</h3>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div id="order-basket">
                </div>
            </div>

            <div class="row justify-content-center m-t-24">
                <div class="col-md-6 justify-content-xxl-start">
                    <button id="btn-clear" class="btn btn-dark bg-dark">Clear All</button>
                </div>

                <div class="col-md-6 justify-content-xxl-end">
                    <button id="btn-save-orders" class="btn btn-primary float-right">Save Order</button>
                </div>
            </div>
        </div>

    </div>
</div>
