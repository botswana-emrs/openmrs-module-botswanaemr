<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/consultation/doctorDashboard.page"},
        {label: "${ui.message('Referrals')}"}
    ];

    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {

            let matchingOptions = [];

            if (oSettings.nTable.getAttribute('id') === 'incomingReferralsTable') {
                if (jq('#searchPhrase').val() !== '') {
                    let pin = aData[1].trim();
                    let name = aData[2].trim();

                    //Exact matching for pin & partial matching for names
                    matchingOptions.push(
                        pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                    );
                }

                if (jq('#filterReferralReason').val() !== '') {
                    let referralReason = aData[4].trim();
                    matchingOptions.push(referralReason === jq('#filterReferralReason').val());

                }

                if (jq('#inReferralDate').val() !== '') {
                    let referralDate = aData[5].trim();
                    matchingOptions.push(referralDate === jq('#inReferralDate').val());

                }

                if (jq('#inReferralStatus').val() !== '') {
                    let status = aData[6].trim();
                    matchingOptions.push(status === jq('#inReferralStatus').val());
                }
            } else if (oSettings.nTable.getAttribute('id') === 'outgoingReferralsTable') {
                if (jq('#outSearchPhrase').val() !== '') {
                    let pin = aData[1].trim();
                    let name = aData[2].trim();

                    //Exact matching for pin & partial matching for names
                    matchingOptions.push(
                        pin === jq('#outSearchPhrase').val() || name.toLowerCase().includes(jq('#outSearchPhrase').val().toLowerCase())
                    );
                }

                if (jq('#filterReferralOutReason').val() !== '') {
                    let referralReason = aData[4].trim();
                    matchingOptions.push(referralReason === jq('#filterReferralOutReason').val());

                }

                if (jq('#outReferralDate').val() !== '') {
                    let referralDate = aData[5].trim();
                    matchingOptions.push(referralDate === jq('#outReferralDate').val());

                }

                if (jq('#outReferralStatus').val() !== '') {
                    let status = aData[6].trim();
                    matchingOptions.push(status === jq('#outReferralStatus').val());
                }
            }

            // handling reset
            if (matchingOptions.length === 0) {
                return  true;
            }

            // Check all statuses
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });

            return matchingStatus;
        }
    );

</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a  class="nav-link" id="referred-to-me-tab"
                        data-toggle="pill" href="#referred-to-me"
                        role="tab" aria-controls="referred-to-me"
                        aria-selected="false"> All Referrals (Incoming)
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="my-referrals-tab"
                       data-toggle="pill" href="#my-referrals"
                       role="tab" aria-controls="my-referrals"
                       aria-selected="true"> My Referrals (Outgoing)
                    </a>
                </li>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3">
                <!-- 1st tab -->
                <div class="tab-pane fade" id="referred-to-me"
                     role="tabpanel" aria-labelledby="referred-to-me-tab">
                    ${ ui.includeFragment("botswanaemr", "consultation/incomingReferralsTable") }
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade show active" id="my-referrals"
                     role="tabpanel" aria-labelledby="my-referrals-tab">
                    ${ ui.includeFragment("botswanaemr", "consultation/outgoingReferralsTable") }
                </div>
            </div>
        </div>
    </div>
</div>
