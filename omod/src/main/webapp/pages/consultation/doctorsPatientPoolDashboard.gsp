<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/doctorsPatientPoolDashboard.page'},
        { label: "Patient Pool"}
    ];

    jq(document).ready(function() {
        toggleGraph();
    });
</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Total consultations completed</div>
                    <div class="stat-digit text-success">${allConsultationsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "CONSULTATION"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Consultations
                            <span class="text-danger">${dailyAverageConsultations}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's Completed Consultations</div>
                    <div class="stat-digit text-success">${todayConsultationsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "todayGraphSummaries", [type: "CONSULTATION"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Yesterday
                            <span class="text-danger">${yesterdayConsultationsCount}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pr-0">
                            <h4 class="pl-0"> Today's Patient Pool</h4>
                        </div>
                    </div>
                    <div id="doctorsPatientPool" class="table-responsive">
                        ${ui.includeFragment("botswanaemr", "consultation/doctorsPatientPool")}
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    <h5>Patient Pool Activity</h5>
                    <% todayDoctorsConsultationActivitiesList.each { %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>${it.creator}</span> accepted to treat
                                <span class="text-success">${it.name}</span>
                                <span class="text-muted">${it.duration} </span>
                            </small>
                        </li>
                    </ul>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>
