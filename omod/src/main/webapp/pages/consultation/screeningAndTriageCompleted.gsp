<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "screening.js")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex { attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size() - 1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }
    ui.includeCss("botswanaemr", "screening.css")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard'
        },
        {
            label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }",
            link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'
        }
    ];
    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
    breadcrumbs.push({label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>
    jq(function () {
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function () {
            window.location.reload();
        });

        jq("#st-back-to-patient-pool").click(function(e) {
            e.preventDefault();
            showOverlay();
            window.location.href = this.href;
        });
    });
    var patient = {id: ${ patient.id }};

</script>

<!-- Loading overlay -->
<div class="card-overlay" id="saveOverlay" style="display: none;">
    <img class="spinner-img"
         src="${ui.resourceLink('botswanaemr', 'images/loading-spinner.gif')}"
         alt="loading..."/>
</div>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="container-fluid center-content-container content-section">
                    <span style=" color: #008000b5;">
                        <i class="fa fa-check-circle  fa-4x"></i>
                    </span>
                    <h6 class="h5 content-section">Screening and Triage completed</h6>
                    <hr/>
                    <h6 class="h6 content-section">Assign to patient pool</h6>
                    ${ui.includeFragment("botswanaemr", "queuePatient", [patientId: patient.patient.patientId, returnUrl: ui.pageLink("botswanaemr", "consultation/auxilliaryNurseDashboard"), isScreeningCompleted: true])}
            </div>
        </div>
    </div>
</div>
