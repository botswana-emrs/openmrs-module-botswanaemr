<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [ isScreeningPage: ui.message("true") ])
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.includeJavascript("botswanaemr", "screening.js")
    ui.includeJavascript("botswanaemr", "utils.js")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex { attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size() - 1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }
    ui.includeCss("botswanaemr", "screening.css")

    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>

<script type="text/javascript">
    let activePatient = "${patient}";
    let activePatientAge = "${patient.patient.person.age}";
    let icd11SearchUrl = "${ ui.actionLink("botswanaemr", "search", "searchIcd11") }";
    let hasVitalsPrivilege = ${hasVitalsPrivilege};
    let hasMedicalDetailsPrivilege = ${hasMedicalDetailsPrivilege};
    let hasSymptomsPrivilege = ${hasSymptomsPrivilege};
    
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard'
        },
        {
            label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }",
            link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'
        }
    ];
    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
    breadcrumbs.push({label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>
    jq(function () {
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function () {
            window.location.reload();
        });
    });
    var patient = {id: ${ patient.id }};
</script>

<% if (includeFragments) {
    includeFragments.each {
        def configs = [:];
        if (it.extensionParams.fragmentConfig != null) {
            configs.putAll(it.extensionParams.fragmentConfig);
        }
        configs.patient = patient; %>
${ui.includeFragment(it.extensionParams.provider, it.extensionParams.fragment, configs)}
<% }
}
%>
<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>

<div>
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.patient.uuid, visit: activeVisit])}
</div>

<div class="row">
    <div class="col">
        <p style="margin:10px;" class="right">
            <strong>TB Presumptive:</strong> <span class="${presumptive ? 'text-danger' : ''}">${presumptive ? 'Yes' : ''}</span>
        </p>
        <button style="margin:10px;" type="button" class="btn btn-success right" data-toggle="modal"
                data-target="#addPrescriptionModal">Prescribe Drug</button>
        <% if (hasTbScreeningEncounter) {%>
        <button style="margin:10px;" type="button" class="btn btn-success right">
            <a href="${ui.pageLink('botswanaemr', 'editEncounter', [
                    patientId: patient.patient.id,
                    visitId  : activeVisit.uuid,
                    encounterId: tbScreeningEncounterId,
                    returnUrl: returnUrl])}">
                TB Screening
            </a>
        </button>
        <% } else { %>
        <button style="margin:10px;" type="button" class="btn btn-success right">
            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                    patientId: patient.patient.id,
                    formUuid : tbScreeningFormUuid,
                    visitId  : activeVisit.uuid,
                    returnUrl: returnUrl])}">
                TB Screening
            </a>
        </button>
        <% } %>

        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-patient-info-tab"
                       data-toggle="pill" href="#pills-patient-info"
                       role="tab" aria-controls="pills-patient-info"
                       aria-selected="true">Patient Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-screening-process-tab"
                       data-toggle="pill" href="#pills-screening-process"
                       role="tab" aria-controls="pills-screening-process"
                       aria-selected="false">Screening Process
                    </a>
                </li>
                <%  if (hasMedicalDetailsPrivilege) { %>
                <li class="nav-item">
                    <a class="nav-link" id="basic-medical-details-tab"
                       data-toggle="pill" href="#basic-medical-details"
                       role="tab" aria-controls="basic-medical-details"
                       aria-selected="false">Basic Medical Details
                    </a>
                </li>
                <% } %>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3" ng-app="triageDataApp" ng-controller="TriageDataController">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-patient-info"
                     role="tabpanel" aria-labelledby="pills-patient-info-tab">
                    ${ui.includeFragment("botswanaemr", "patientInformation", [patientId: patient.patient.uuid])}
                    ${ui.includeFragment("botswanaemr", "nextOfKinInformation")}
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-screening-process"
                     role="tabpanel" aria-labelledby="pills-screening-process-tab">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <% if (hasVisit) { %>
                                <div style="text-align:center;margin-top:40px;" class="row pl-3 pr-3">
                                    <ul class="nav step-nav">
                                        <% if (hasVitalsPrivilege) { %>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="step-1">
                                                <span class="badge badge-pill badge-primary">1</span>
                                                <small class="text-center text-danger">
                                                    Capture vitals
                                                </small>
                                            </a>
                                        </li>
                                        <% } %>                                        
                                        <% if (hasMedicalDetailsPrivilege) { %>
                                        <li class="nav-item">
                                            <a class="nav-link" id="step-2">
                                                <span class="badge badge-pill badge-primary">2</span>
                                                <small class="text-center text-danger">
                                                    Basic medical details
                                                </small>
                                            </a>
                                        </li>
                                        <% } %>
                                        <% if (hasSymptomsPrivilege) { %>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#step-3" id="step-3">
                                                <span class="badge badge-pill badge-primary">3</span>
                                                <small class="text-center text-danger">
                                                    Capture symptoms
                                                </small>
                                            </a>
                                        </li>
                                        <% } %>
                                    </ul>
                                </div>
                                <% if (screeningAndTriageEncounter == null) { %>
                                ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
                                        patient  : formPatient,
                                        htmlForm : htmlForm,
                                        visit    : currentVisit,
                                        returnUrl: returnUrl
                                ])}
                                <% } else { %>
                                ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
                                        visit               : screeningAndTriageEncounter.visit,
                                        encounter           : screeningAndTriageEncounter,
                                        patient             : formPatient,
                                        returnUrl           : returnUrl,
                                        definitionUiResource: "botswanaemr:htmlforms/triage-screening-form.xml"

                                ])}
                                <% } %>
                                <% } else { %>
                                <h6>Please check-in patient to start the screening process</h6>
                                <%
                                        ui.includeFragment("botswanaemr", "visit/patientVisit")
                                    }
                                %>
                                <div class="form-tab">
                                    <div class="col text-center">
                                        <% if (hasVisit) { %>

                                        <h5 class="h5 content-section">Confirm details</h5>

                                        <div class="col-8 text-left center-section">
                                            <p>Please confirm the patient's details below based on their screening.</p>
                                        </div>

                                        <div class="col-12">
                                            <div class="bg-light mb-3 content-section">
                                                <div class="col-8 text-left center-section">

                                                    <div id="confirmDetails"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <% } %>
                                    </div>
                                </div>
                                <% if (hasVisit) { %>
                                <div class="button-section pl-3 pr-3">
                                    <button id="prevBtn" class="btn btn-dark bg-dark horizontal-button"
                                            onclick="nextPrev(-1)">Previous</button>
                                    <button id="nextBtn" class=" btn btn-primary horizontal-button"
                                            onclick="nextPrev(1)">Next Step</button>
                                </div>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
                <%  if (hasMedicalDetailsPrivilege) { %>
                <div class="tab-pane fade" id="basic-medical-details"
                     role="tabpanel" aria-labelledby="basic-medical-details-tab">
                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="card bg-light mb-3">
                                ${ui.includeFragment("botswanaemr", "allergies", [patientId: patient.patient.uuid])}
                            </div>
                        </div>

                        <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="card bg-light mb-3">
                                ${ui.includeFragment("botswanaemr", "conditions", [patientId: patient.patient.uuid])}
                            </div>
                        </div>

                        <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="card bg-light mb-3">
                                ${ui.includeFragment("botswanaemr", "vitalsTrends", [patientId: patient.patient.uuid])}
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>                
            </div>
        </div>
    </div>
</div>
