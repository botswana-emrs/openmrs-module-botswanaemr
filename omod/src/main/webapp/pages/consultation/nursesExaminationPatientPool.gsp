<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex { attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size()-1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }
%>

<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "jquery.validate.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/consultation/auxilliaryNurseDashboard"},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }" ,
        link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'}
    ];
    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
        breadcrumbs.push({ label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>
    jq(function(){
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function() {
            window.location.reload();
        });
    });
    var patient = { id: ${ patient.id } };
</script>

<%  if(includeFragments){
    includeFragments.each {
        def configs = [:];
        if(it.extensionParams.fragmentConfig != null){
            configs.putAll(it.extensionParams.fragmentConfig);
        }
        configs.patient = patient; %>
        ${ui.includeFragment(it.extensionParams.provider, it.extensionParams.fragment, configs)}
    <%}
}
%>
${ ui.includeFragment("botswanaemr", "consultation/beginConsultation") }
${ ui.includeFragment("botswanaemr", "consultation/endConsultation") }
<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <div class="col">
            <a href="${ui.pageLink('botswanaemr','consultation/' + patientPoolPage)}"> Patient pool / </a>
            <a href="${ui.pageLink('botswanaemr','consultation/nursesExaminationPatientPool', [patientId: patient.patient.id, visitId: visitId])}">Nurses examination</a>
        </div>
    </div>
    <div class="col-5">
        <% if (isDoctorsPortal) { %>
            <a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${patient.patient.id}"
               class="btn btn-sm bg-white text-primary btn-outline-info mb-3">
               <i class="fa fa-file-text-o"></i> View Patient Profile
            </a>
            <% if (referralId != "") {%>
            <a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=${patient.patient.id}&visitId=${activeVisit.id}" target="blank"
            class="btn btn-sm bg-orange text-primary btn-outline-info float-left mb-3 ml-2 mr-2">
                <i class="fa fa-folder-o"></i> Referral
            </a>
            <%}%>

            <% if(isPendingState) {%>
                <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#beginConsultationModal" id="beginConsulations">
                    <i class="fa fa-handshake-o"></i> Begin Consultation
                </button>
            <%}%>
            <% if(isPickedState) {%>
        <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#endConsultationModal">
            <i class="fa fa-handshake-o"></i> End Consultation
        </button>
        <%}%>
        <% } %>
    </div>
</div>

<div>
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.patient.uuid, visit: activeVisit])}
</div>


<div class="row">
    <div class="col">
        <div class="card px-0">
            <div class="row">
                <div class="col">
                    <span class="text-primary">
                        <i class="fa fa-user"></i> Patient:
                    </span>
                    ${patient.patient.person.personName.givenName}&nbsp;${patient.patient.person.personName.familyName}
                </div>
                <div class="col">
                    <span class="text-primary">
                        <i class="fa fa-id-badge"></i> Patient ID:
                    </span>
                    ${pin}
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col">
                            <span>${identifierType}:</span>
                        </div>
                        <div class="col">
                            <span class="text-danger">${identifierValue}</span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <span> Severity:
                        <p class="text-danger inline">${severity}</p>
                    </span>
                </div>
                <div class="col">
                    <span class="float-right"> Status:
                        <p class="text-warning inline">${queueStatus}</p>
                    </span>
                </div>
                <!-- Add back button widget-->
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>

        <button style="margin:10px;" type="button" class="btn btn-success right" data-toggle="modal" data-target="#addPrescriptionModal">Prescribe Drug</button>

        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a  class="nav-link" id="pills-vitals-tab"
                        data-toggle="pill" href="#pills-vitals"
                        role="tab" aria-controls="pills-vitals"
                        aria-selected="true"> Vitals
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link active" id="pills-medical-info-tab"
                        data-toggle="pill" href="#pills-medical-info"
                        role="tab" aria-controls="pills-medical-info"
                        aria-selected="false"> Basic Medical Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-screening-and-triage-exam-tab"
                        data-toggle="pill" href="#pills-screening-and-triage-exam"
                        role="tab" aria-controls="pills-screening-and-triage-exam"
                        aria-selected="false"> Screening And Triage Examination
                    </a>
                </li>
            </ul>
            <div class="row pt-0">
                <div class="col col-sm-12 col-md-12 col-lg-9">
                    <!-- content -->
                    <div class="tab-content" id="pills-tabContent" ng-app="triageDataApp" ng-controller="TriageDataController" ng-init='init("${ patient.patient.uuid }", "${ activePatientVisit?.uuid }")'>
                        <!-- 1st tab -->
                        <div class="tab-pane fade show active" id="pills-vitals"
                             role="tabpanel" aria-labelledby="pills-vitals-tab">
                            <div class="row pt-0">
                                <div class="col col-sm-12 col-md-12 col-lg-12">
                                    <div class="card mb-3">
                                        ${ ui.includeFragment("botswanaemr", "consultation/viewVitals", [ patientId: patient.patient.uuid, visitId: visitId, returnUrl: returnUrl ]) }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 2nd tab -->
                        <div class="tab-pane fade show active" id="pills-medical-info"
                         role="tabpanel" aria-labelledby="pills-medical-info-tab">
                             <div class="row pt-0">
                                 <div class="col col-sm-12 col-md-12 col-lg-12">
                                     <div class="card mb-3">
                                         ${ ui.includeFragment("botswanaemr", "conditions", [ patientId: patient.patient.uuid ]) }
                                     </div>
                                     <div class="card mb-3">
                                         ${ ui.includeFragment("botswanaemr", "allergies", [ patientId: patient.patient.uuid ]) }
                                     </div>
                                     <div class="card mb-3">
                                         ${ ui.includeFragment("botswanaemr", "pastOperations", [ patientId: patient.patient.uuid, visit: activePatientVisit ]) }
                                     </div>
                                     <div class="card mb-3">
                                         ${ ui.includeFragment("botswanaemr", "lifeStyle", [ patientId: patient.patient.uuid, visit: activePatientVisit]) }
                                     </div>
                                 </div>
                             </div>
                        </div>
                        <!-- 3rd tab -->
                        <div class="tab-pane fade" id="pills-screening-and-triage-exam"
                             role="tabpanel" aria-labelledby="pills-screening-and-triage-exam-tab">
                            <div class="row pt-0">
                                <div class="col col-sm-12 col-md-12 col-lg-12">
                                    <div class="container-fluid card shadow d-flex justify-content-center">
                                        <!-- nav options -->
                                        <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab-two" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="subjective-tab"
                                                   data-toggle="pill" href="#subjective"
                                                   role="tab" aria-controls="subjective"
                                                   aria-selected="true">Subjective
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="objective-tab"
                                                   data-toggle="pill" href="#objective"
                                                   role="tab" aria-controls="objective"
                                                   aria-selected="false">Objective
                                                </a>
                                            </li>
                                        </ul>

                                        <!-- content -->
                                        <div class="tab-content" id="pills-tabContent-two">
                                            <!-- 1st tab -->
                                            <div class="tab-pane fade show active" id="subjective"
                                                 role="tabpanel" aria-labelledby="subjective-tab">
                                                ${ui.includeFragment("botswanaemr", "consultation/subjective", [patientId: patient.patient.uuid, visit: activePatientVisit])}
                                            </div>
                                            <!-- 2nd tab -->
                                            <div class="tab-pane fade" id="objective"
                                                 role="tabpanel" aria-labelledby="objective-tab">
                                                ${ui.includeFragment("botswanaemr", "consultation/objective", [patientId: patient.patient.uuid, visit: activePatientVisit])}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Activity Panel -->
                <div class="col col-sm-12 col-md-12 col-lg-3">
                    <% if (!isScreening) { %>
                        ${ ui.includeFragment("botswanaemr", "consultation/consultationHistory", [ patientId: patient.patient.uuid ]) }
                    <% } %>
                    <div class="card">
                        <h5>Activity Trail</h5>
                        <% consultationEncounters.each { %>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <small>
                                    <span>${it.creator}</span> ${it.typeText}
                                    <span class="text-success">${it.name}</span>
                                    <span class="text-muted">${it.duration}</span>
                                </small>
                            </li>
                        </ul>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
