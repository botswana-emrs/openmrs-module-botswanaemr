<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/doctorDashboard.page'},
        { label: "Consultations"}
    ];
</script>

<div id="consultationList">
    ${ui.includeFragment("botswanaemr", "consultation/list/consultationList")}
</div>
