<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
    throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex {attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size()-1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }" ,
        link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'}
    ];
    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
        breadcrumbs.push({ label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>
    jq(function(){
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function() {
            window.location.reload();
        });
    });
    var patient = { id: ${ patient.id } };
</script>

<% if(includeFragments){
    includeFragments.each {
    def configs = [:];
        if(it.extensionParams.fragmentConfig != null){
            configs.putAll(it.extensionParams.fragmentConfig);
        }
        configs.patient = patient; %>
        ${ui.includeFragment(it.extensionParams.provider, it.extensionParams.fragment, configs)}
    <%}
}
%>
${ ui.includeFragment("botswanaemr", "consultation/beginConsultation") }
${ ui.includeFragment("botswanaemr", "consultation/endConsultation") }
<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <div class="col">
            <a href="${ui.pageLink('botswanaemr','consultation/doctorsPatientPoolDashboard')}"> Patient pool / </a>
            <a href="${ui.pageLink('botswanaemr','consultation/nursesExaminationPatientPool', [patientId: patient.patient.id])}">Nurses examination / </a>
            <a href="${ui.pageLink('botswanaemr','consultation/patientInformationDoctorsPortal', [patientId: patient.patient.id])}">Patient information</a>
        </div>
    </div>
    <div class="col-5">
        <!-- TODO: Create and insert a begin consultation widget here -->
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card px-0">
            <div class="row">
                <div class="col col-sm-12 col-md-12 col-lg-3 pl-1">
                    <span class="text-primary">
                        <i class="fa fa-user"></i> NAME:
                            ${patient.patient.person.personName.givenName}&nbsp;${patient.patient.person.personName.familyName}
                    </span>
                </div>
                <div class="col col-sm-12 col-md-12 col-lg-2 pl-0">
                    <span class="text-success">
                        <i class="fa fa-id-badge"></i> Patient ID:
                            ${pin}
                    </span>
                </div>
                <div class="col col-sm-12 col-md-12 col-lg-3 pl-0">
                    <span>
                        ${identifierType}:&nbsp;<span class="text-danger">${identifierValue}</span>
                    </span>
                </div>
                <div class="col col-sm-12 col-md-12 col-lg-4">
                    <div class="row">
                        <span class="text-primary inline">
                            <% if (allergies) { %>
                            Allergy:
                            <h6 class="inline text-primary">
                                <span class="text-danger">
                                    <% allergies.each { %>
                                    ${it.allergen},
                                    <% } %>
                                </span>
                            </h6>
                            <br/>
                            <h6 class="inline text-primary">
                                Severity: <span class="text-warning"><% if (severity) { %> ${
                                    ui.encodeHtmlContent(ui.format(severity))} <% } %></span>
                            </h6>
                            <br/>
                            <h6 class="inline text-primary">
                                Status: <span class="text-success">Status</span>
                            </h6>

                            <div class="clearfix"></div>
                            <br/>
                            <% } else { %>
                            <span class="inline">
                                Allergies: <span class="text-danger">No Known Allergies!</span>
                            </span>
                            <% } %>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-patient-info-tab"
                       data-toggle="pill" href="#pills-patient-info"
                       role="tab" aria-controls="pills-patient-info"
                       aria-selected="true"> Patient Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-info-tab"
                        data-toggle="pill" href="#pills-medical-info"
                        role="tab" aria-controls="pills-medical-info"
                        aria-selected="false"> Basic Medical Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-history-tab"
                        data-toggle="pill" href="#pills-medical-history"
                        role="tab" aria-controls="pills-medical-history"
                        aria-selected="false"> Medical History
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-payment-history-tab"
                        data-toggle="pill" href="#pills-payment-history"
                        role="tab" aria-controls="pills-payment-history"
                        aria-selected="false"> Payment History
                    </a>
                </li>
            <li class="nav-item">
                <a  class="nav-link" id="pills-lab-history-tab"
                    data-toggle="pill" href="#pills-lab-history"
                    role="tab" aria-controls="pills-lab-history"
                    aria-selected="false"> Lab History
                </a>
            </li>
        </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-patient-info"
                     role="tabpanel" aria-labelledby="pills-patient-info-tab">
                    ${ ui.includeFragment("botswanaemr", "patientInformation", [ patientId: patient.patient.id ]) }
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-medical-info"
                     role="tabpanel" aria-labelledby="pills-medical-info-tab">
                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="card">
                                ${ ui.includeFragment("botswanaemr", "allergies", [ patientId: patient.patient.id ]) }
                            </div>
                        </div>
                        <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="card">
                                ${ ui.includeFragment("botswanaemr", "conditions", [ patientId: patient.patient.id ]) }
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 3rd tab -->
                <div class="tab-pane fade" id="pills-medical-history"
                     role="tabpanel" aria-labelledby="pills-medical-history-tab">
                    ${ui.includeFragment("botswanaemr", "patientMedicalHistory")}
                </div>
                <!-- 4th tab -->
                <div class="tab-pane fade" id="pills-payment-history"
                     role="tabpanel" aria-labelledby="pills-payment-history-tab">
                    ${ui.includeFragment("botswanaemr", "admin/paymentHistory")}
                </div>
            <!-- 5th tab -->
            <div class="tab-pane fade" id="pills-lab-history"
                 role="tabpanel" aria-labelledby="pills-lab-history-tab">
                ${ui.includeFragment("botswanaemr", "consultation/labHistory")}
            </div>
            </div>
        </div>
    </div>
</div>
