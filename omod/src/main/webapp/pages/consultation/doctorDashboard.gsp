<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/consultation/doctorDashboard.page"},
        {label: "${ui.message('Dashboard')}"}
    ];

    jq(document).ready(function() {
        var status = jq("#status").val();
        jq('#status').trigger('click');
        jq('#status').click(function() {
            if(status == "Completed") {
                jq('#flag').addClass('text-success');
            } else {
                jq('#flag').addClass('text-warning');
            }
        })
    });
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pr-0">
                            <h4 class="pl-0"> Today's upcoming appointments</h4>
                        </div>
                    </div>
                    <div class="table-responsive">
                        ${ui.includeFragment("botswanaemr", "consultation/appointments")}
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    ${ui.includeFragment("botswanaemr", "dashboardReferrals")}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card mt-3">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 pr-0">
                    <h4 class="pl-0"> Lab Results</h4>
                </div>
            </div>
            <div class="table-responsive">
                ${ui.includeFragment("botswanaemr", "consultation/labResults")}
            </div>
        </div>
    </div>
</div>
