<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex {attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size()-1 != index) {
            formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }

    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>

<% if(includeFragments){
    includeFragments.each {
        def configs = [:];
        if(it.extensionParams.fragmentConfig != null){
            configs.putAll(it.extensionParams.fragmentConfig);
        }
        configs.patient = patient; %>
        ${ui.includeFragment(it.extensionParams.provider, it.extensionParams.fragment, configs)}
    <%}
}
%>

<script type="text/javascript">
    let breadcrumbs = [
        {icon: "icon-home", link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/consultation/doctorDashboard.page"},
        {
            label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }",
            link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'
        }
    ];
    jQuery(function(){
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function() {
            window.location.reload();
        });
      //jq("#caseToast").toast('show');

        var diagnosisRecorded = <%= diagnosisRecorded %>;
        function togglePlanTab(isDiagnosisRecorded) {
            if (isDiagnosisRecorded === false) {
                jq('#plan-tab').addClass('disabled');
                jq('#plan-tab').attr('aria-disabled', 'true');
                jq('#plan').removeClass('nursing-diagnosis');
                jq('#plan-tab').on('click', function (e) {
                    e.preventDefault();
                    alert('Record at least one diagnosis to proceed!');
                    return false;
                });
                jq('#plan').removeClass('show active');
            } else {
                jq('#plan-tab').removeClass('disabled');
                jq('#plan-tab').attr('aria-disabled', 'false');
                jq('#plan-tab').off('click');
                jq('#plan').addClass('show nursing-diagnosis');
            }
        }

        togglePlanTab(diagnosisRecorded);

        jq(document).on('nursingDiagnosisUpdated', function (event, data) {
            if (data.diagnosisRecorded !== undefined) {
                diagnosisRecorded = data.diagnosisRecorded;
                togglePlanTab(diagnosisRecorded);
            }
        });

    });
    var patient = { id: ${ patient.id } };
</script>
<style type="text/css">
.toast {
  left: 50%;
  position: fixed;
  transform: translate(-50%, 0px);
  z-index: 9999;
  border: 2px solid #006400;
  background: #90EE90;
}
</style>
${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}
${ ui.includeFragment("botswanaemr", "consultation/beginConsultation") }
${ ui.includeFragment("botswanaemr", "consultation/endConsultation") }
<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-5">
        <div class="col">
            <a href="${ui.pageLink('botswanaemr','consultation/consultation', [patientId: patient.patient.id, visitId: activeVisit?.id])}">
                Consultations / ${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }
                consultation details
            </a>
        </div>
    </div>
    <div class="col-7 text-center">
        <a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${patient.patient.id}"
           class="btn btn-sm bg-white text-primary btn-outline-info float-left mb-3">
            <i class="fa fa-file-text-o"></i> View Patient Profile
        </a>
        <% if (referralId != "") {%>
        <a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=${patient.patient.id}&visitId=${activeVisit.id}"
           class="btn btn-sm bg-white text-primary btn-outline-info float-left mb-3">
            <i class="fa fa-file-text-o"></i> Referral
        </a>
        <%}%>

        <% if (!activeVisit.encounters.isEmpty() && activeVisit != "") { %>
            <a  href="/${ui.contextPath()}/botswanaemr/printActiveVisitSummary.page?patientId=${patient.patient.patientId}&visitId=${activeVisit.id}"
                class="btn btn-dark bg-dark btn-sm text-white">
                <i class="fa fa-print text-white"></i> Print Visit Summary
            </a>
        <% } %>

        <% if (isDoctorsPortal) { %>
        <% if (isPickedState && isCurrentProvider) { %>
        <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#endConsultationModal">
            <i class="fa fa-handshake-o"></i> End Consultation
        </button>
        <%} else if (isPendingState) {%>
        <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#beginConsultationModal">
            <i class="fa fa-handshake-o"></i> Begin Consultation
        </button>
        <% } } %>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card px-0">
            <div class="row">
                <div class="col">
                    <span class="text-primary"><i class="fa fa-user"></i> Patient:</span>
                    ${patient.patient.person.personName.givenName}&nbsp;${patient.patient.person.personName.familyName}
                </div>
                <div class="col">
                    <ul class="list-unstyled">
                        <li class="text-primary">
                            <i class="fa fa-id-badge"></i> Patient ID:
                            <span class="text-dark">${pin}</span>
                        </li>
                        <% if (identifierType != "") { %>
                            <li class="text-primary">
                                <i class="fa fa-id-badge"></i> ${identifierType}:
                                <span class="text-dark">${identifierValue}</span>
                            </li>
                        <% } %>
                    </ul>
                </div>
                <div class="col">
                   <span class="text-dark">${provider}</span>
                </div>
                <div class="col">
                    <div class="row">
                        <ul class="list-unstyled text-primary">
                            <% if(allergies) {%>
                                <li> Allergy:
                                    <% allergies.each { %>
                                    <span class="text-danger">${it.allergen}<% if (allergies.size() > 1) { %>,<% } %></span>
                                    <% } %>
                                </li>
                                <li>Severity:
                                    <span class="text-warning"><% if (severity) { %> ${ ui.encodeHtmlContent(ui.format(severity)) } <% } %></span>
                                </li>
                                <li>Queue Status:
                                    <span class="text-success">${queueStatus}</span>
                                </li>
                            <%}else{%>
                                <span class="inline">
                                   Allergies: <span class="text-danger"> Unknown!</span>
                                </span>
                            <%}%>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <span>Date:
                        <span class="text-danger">${consultationStartdate}</span>
                    </span>
                </div>
                <div class="col">
                    <span>Time:
                        <span class="text-danger">${consultationStarttime}</span>
                        (Started by: ${consultationProvider})
                    </span>
                </div>
                <div class="col">
                <!--
                    <div class="row">
                        <div class="col">
                            <span>Case:</span>
                        </div>
                        <div class="col pl-0 pr-5">
                            <span class="text-dark">${caseId}</span>
                        </div>
                    </div>
                -->
                </div>
                <div class="col"></div>
                <div class="col"></div>
            </div>
        </div>
        <!--
        <% if(caseState) {%>
        <div class="toast" data-autohide="false" id="caseToast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="mr-auto text-primary">&nbsp;&nbsp;&nbsp;</strong>
                <small class="text-muted">&nbsp;&nbsp;&nbsp;</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body">
                <% if(caseState == "new") {%>
                <i class="fas fa-check-circle"></i>
                <b>Case created. Consultation begun!</b>
                <p>
                   You have created a new case for this consultation and started the consultation with this patient.
                </p>
                <%} else if(caseState == "existing"){%>
                <i class="fas fa-check-circle"></i>
                <b>Case linked. Consultation begun!</b>
                <p>
                    You have linked this consultation to an existing one and started the consultation with this patient.
                </p>
                <%}%>
            </div>
        </div>
        <%}%>
        -->
        <div class="row pt-0">
            <div class="col col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="container-fluid card shadow d-flex justify-content-center pl-0 pr-0">
                    <!-- nav options -->
                    <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-doctors-assessment-tab"
                               data-toggle="pill" href="#pills-doctors-assessment"
                               role="tab" aria-controls="pills-doctors-assessment"
                               aria-selected="true">Clinical Assessment
                            </a>
                        </li>
                        <% patientActivePrograms.each { %>
                        <li class="nav-item">
                            <a class="nav-link" id="program-tab-${it.id}"
                               href="${ui.pageLink('botswanaemr', 'programs/programs', [patientId: patient.patient.uuid, programId: it.uuid, visitId: activeVisit?.id])}">${it.description}
                            </a>
                        </li>
                        <% } %>
                        <li class="nav-item">
                            <a class="nav-link" id="discussion-tab"
                               data-toggle="pill" href="#discussion"
                               role="tab" aria-controls="discussion"
                               aria-selected="false">Discussion
                            </a>
                        </li>
                        <li style="position: absolute; right: 1%">
                            <div class="btn-group float-right">
                                <button type="button"
                                        class="btn btn-sm btn-primary dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false">
                                    <i class="fa fa-external-link"></i> Patient programs
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <% programList.each { %>
                                    <a href="${ui.pageLink('botswanaemr','programs/programs',[patientId: patient.patient.uuid, currentServicePoint: currentServicePoint, programId: it.uuid])}" class="dropdown-item">${it.description}</a>
                                    <% } %>
                                    <a href="${ui.pageLink('botswanaemr','hts/htsClientProfile',[patientId: patient.patient.uuid])}" class="dropdown-item">HTS module</a>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="row">
                        <div class="col col-md-3"></div>
                        <div class="col col-md-3"></div>
                        <div class="col col-md-3 pr-0"></div>
                        <div class="col col-md-3 pr-0 float-right">
                        </div>
                    </div>
                    <!-- content -->
                    <div class="tab-content" id="pills-tabContent" ng-app="triageDataApp"
                         ng-controller="TriageDataController"
                         ng-init='init("${patient.patient.uuid}", "${activeVisit?.uuid}")'>
                        <!-- 1st tab -->
                        <div class="tab-pane fade show active" id="pills-doctors-assessment"
                             role="tabpanel" aria-labelledby="pills-doctors-assessment-tab">
                            <div class="container-fluid card shadow d-flex justify-content-center">
                                <!-- nav options -->
                                <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab-two" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="subjective-tab"
                                           data-toggle="pill" href="#subjective"
                                           role="tab" aria-controls="subjective"
                                           aria-selected="true">Subjective
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="objective-tab"
                                           data-toggle="pill" href="#objective"
                                           role="tab" aria-controls="objective"
                                           aria-selected="false">Objective
                                        </a>
                                    </li>
                                    <% if (!isNursingPortal) { %>
                                    <li class="nav-item">
                                        <a class="nav-link" id="assessment-tab"
                                           data-toggle="pill" href="#assessment"
                                           role="tab" aria-controls="assessment"
                                           aria-selected="false">Assessment
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="plan-tab"
                                           data-toggle="pill" href="#plan"
                                           role="tab" aria-controls="plan"
                                           aria-selected="false">Plan
                                        </a>
                                    </li>
                                    <% } %>
                                </ul>
                                <!-- content -->
                                <div class="tab-content" id="pills-tabContent-two">
                                    <!-- 1st tab -->
                                    <div class="tab-pane fade show active" id="subjective"
                                         role="tabpanel" aria-labelledby="subjective-tab">
                                        ${ui.includeFragment("botswanaemr", "consultation/subjective", [patientId: patient.patient.uuid, visit: activeVisit])}
                                    </div>
                                    <!-- 2nd tab -->
                                    <div class="tab-pane fade" id="objective"
                                         role="tabpanel" aria-labelledby="objective-tab">
                                        ${ui.includeFragment("botswanaemr", "consultation/objective", [patientId: patient.patient.uuid, visit: activeVisit])}
                                    </div>
                                    <% if (!isNursingPortal) { %>
                                        <!-- 3rd tab -->
                                        <div class="tab-pane fade" id="assessment"
                                             role="tabpanel" aria-labelledby="assessment-tab">
                                            ${ui.includeFragment("botswanaemr", "consultation/assessment", [patientId: patient.patient.uuid, visit: activeVisit])}
                                        </div>
                                        <!-- 4th tab -->
                                        <div class="tab-pane fade" id="plan"
                                             role="tabpanel" aria-labelledby="plan-tab">
                                            ${ui.includeFragment("botswanaemr", "consultation/plan", [patientId: patient.patient.uuid, visit: activeVisit])}
                                        </div>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                        <!-- 2nd tab -->
                        <div class="tab-pane fade grey-background" id="discussion"
                             role="tabpanel" aria-labelledby="discussion-tab">
                            ${ui.includeFragment("botswanaemr", "consultation/discussion", [patientId: patient.patient.uuid, visit: activeVisit])}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Activity Panel -->
            <div class="col col-sm-12 col-md-12 col-lg-3 pr-0">
                <div class="card">
                    <h5>Activity Trail</h5>
                    <!-- 
                    <% consultationEncounters.each { %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>${it.creator}</span> ${it.typeText}
                                <span class="text-success">${it.name}</span>
                                <span class="text-muted">${it.duration}</span>
                            </small>
                        </li>
                    </ul>
                    <% } %>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>


