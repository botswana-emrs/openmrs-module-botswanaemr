<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "payment.js")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.message("Assign Patient To Another Queue") }",
            link: "${ ui.pageLink("botswanaemr", "regularRegistration") }"
        }
    ]));

    jq(document).ready(function(){
		jQuery("#reassignPatientModal").modal('show');
	});
</script>

<div class="row">
    <div class="col">
        <div class="card">
            ${ui.includeFragment("botswanaemr", "reassignPatientToQueue")}
        </div>
    </div>
</div>
