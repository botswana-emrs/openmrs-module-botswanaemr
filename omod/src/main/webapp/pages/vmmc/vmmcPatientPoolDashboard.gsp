<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/vmmc/vmmcDashboard.page'},
        { label: "Patient Pool"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div id="vmmcPatientPool" class="col">
                    ${ui.includeFragment("botswanaemr", "vmmc/vmmcPatientPool")}
                </div>
            </div>
        </div>
    </div>
</div>
