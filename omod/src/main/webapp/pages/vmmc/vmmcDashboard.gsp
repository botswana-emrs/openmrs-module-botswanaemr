<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/vmmc/vmmcDashboard.page'},
        { label: "Dashboard"}
    ];

    function printRecentAdmissionsTableView(el) {

        jq.getJSON('${ui.actionLink("botswanaemr", "vmmc/vmmcRecentAdmissions", "getLetterHeadDiv")}'
        ).success(function (data) {
            var header = '<div class="row">' +
                '<div class="col">' +
                '<h4 class="text-left p-l-5">VMMC Recent Admissions</h4>' +
                '</div>' +
                '</div>';
            var printContent = jq(el).clone();
            printContent.find('th:last-child, td:last-child').remove();
            jq('body').empty().html(data?data.printHeader:'</p').append(header).append(printContent);
            window.print();
            location.reload();

        });
    }

    jq(document).ready(function() {
        toggleGraph();
    })

</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col collapsible-content">
        <div class="card bg-primary">
            <div class="row">
                <div class="col pl-0 pr-0">
                    <div class="display-4">${yesterdayVmmcCircumcisionsCount}</div>
                </div>
                <div class="col pl-0 pr-0">
                    <p class="text-white">Yesterday's</p>
                    <p class="text-white">Circumcision visits</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col collapsible-content">
        <div class="card bg-primary">
            <div class="row">
                <div class="col pl-0 pr-0">
                    <div class="display-4">${dailyAverageVmmcCircumcisions}</div>
                </div>
                <div class="col pl-0 pr-0">
                    <p class="text-white">Daily</p>
                    <p class="text-white">Average</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col collapsible-content">
        <div class="col">
            <div class="card bg-primary">
                <div class="row">
                    <div class="col pl-0 pr-0">
                        <div class="display-4">${allVmmcCircumcisionsCount}</div>
                    </div>
                    <div class="col pl-0 pr-0">
                        <p class="text-white">Total</p>
                        <p class="text-white">Circumcisions</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row collapsible">
    <div class="col-md-12 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Weekly visits </div>
                    <div class="stat-digit text-success">${allWeeklyVmmcCircumcisionsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "SCREENING", encounterType: "B444FBD1-EA53-4FDB-BC52-EE90E93B4E1F"]) }
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-10 col-md-10 col-lg-10 pr-0">
                                <h4 class="pl-0"> Recent Admissions</h4>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2 pr-0">
                                <button type="button" class="btn btn-primary px-3 float-right" >
                                    <i class="fa fa-print" aria-hidden="true" onclick="printRecentAdmissionsTableView('#vmmcRecentAdmissionsTable')"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="vmmcRecentAdmissions" class="table-responsive">
                        ${ui.includeFragment("botswanaemr", "vmmc/vmmcRecentAdmissions")}
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <% todayVmmcActivitiesList.each { %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                 <span>${it.creator}</span> completed the
                                 <span>${it.associatedEncounter}</span> encounter of
                                 <span class="text-success">${it.name}</span>
                                 <span class="text-danger">${it.gender}</span>
                                 <span class="text-muted">${it.duration} </span>
                            </small>
                        </li>
                    </ul>
                     <%}%>
                </div>
            </div>
        </div>
    </div>
</div>
