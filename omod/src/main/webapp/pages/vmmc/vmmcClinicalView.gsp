<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/vmmc/vmmcDashboard.page'},
        { label: "VMMC Forms"}
    ];
</script>
<div class="col col-md-12">
    ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
</div>
<div class="col col-md-12">
    ${ui.includeFragment("botswanaemr", "patient/patientHeader", [patient: patient, widgets: "1,2,3,4,5"])}
</div>
<% if (filledInitialScreeningForm && !eligibleForVmmc) { %>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong>
                <i class="fa fa-warning"></i> Client is not eligible to proceed based on the screening outcome. Cannot proceed beyond screening! <%if (screeningHardStopConditions.size()>0) {%> The stop conditions include ${screeningHardStopConditions} <%}%>
            </strong>
        </div>
    </div>
</div>
<% } %>
<% if (hasDisagreedOrDeferred) { %>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong>
                <i class="fa fa-warning"></i> Unable to proceed because the counselling outcome is ${vmmcCounsellingOutcomeLastestObs}
            </strong>
        </div>
    </div>
</div>
<% } %>
<% if (patient.person.gender == 'M') { %>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-md-6">
                    <div class="list-group">
                        <a href="" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                        <% if (hasVisit) { %>
                            <a href="${ui.pageLink('botswanaemr', 'hts/htsClientProfile', [patientId: patient.id, returnUrl: returnUrl])}" class="list-group-item list-group-item-action" target="_blank">HIV Testing Services (HTS)</a>

                            <% availableVmmcForms.each { %>
                                <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                        patientId: patient.id,
                                        formUuid : it.form.uuid,
                                        visitId  : currentVisit.uuid,
                                        returnUrl: returnUrl])}"
                                   class="list-group-item list-group-item-action">${it.form.name}
                                </a>
                            <% } %>

                        <% } else { %>
                            <div class="alert alert-warning text-danger" role="alert">
                                <strong>
                                    <i class="fa fa-warning"></i> Please start a patient visit to proceed!
                                </strong>
                            </div>
                        <% } %>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action bg-success">Completed forms</a>
                        <% if (vmmcEncounters.size() == 0) { %>
                            No past filled forms to show
                        <% } %>
                        <% vmmcEncounters.each { %>
                            <span class="list-group-item list-group-item-action">
                                ${it.form.name} (${it.encounterDatetime})
                                <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                                    onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                                </i>
                            </span>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>
<!-- Alert that this component is only available to male clients -->
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong>
                <i class="fa fa-warning"></i> This component is only available to male clients!
            </strong>
        </div>
    </div>
</div>
<% } %>
