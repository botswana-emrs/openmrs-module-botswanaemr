<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    def appFrameworkService = context.getService(context.loadClass("org.openmrs.module.appframework.service.AppFrameworkService"))
    def monthly = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.monthly")
    def registers = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.registers")
    def quarterly = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.quarterly")
    def contextModel = [:]
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {label: "Reports"}
    ];
    jq(document).ready(function() {
        jq('#reportId').on('change', function() {
            var selectedReport = jq(this).find('option:selected');
            jq('input[name="reportName"]').val(selectedReport.text().trim());
        });
    });


</script>

<div class="row">
    <div class="col">

        <% if (moduleStatus) { %>
        <div class="card">
            ${ui.includeFragment("botswanaemrreports", "vmmcReports")}
        </div>
        <% } else { %>
        <div class="card">
            <p>BotswanaEMR reporting module NOT started. Please load and start the module</p>
        </div>
        <% } %>
    </div>
</div>

