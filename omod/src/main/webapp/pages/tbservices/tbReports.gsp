<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/tbservices/tbServicesDashboard.page'},
        { label: "TB Reports"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <% if(moduleStatus) {%>
            ${ui.includeFragment("botswanaemrreports", "tbReports")}
            <%} else {%>
            <p>BotswanaEMR reporting module NOT started. Please load and start the module</p>
            <%}%>
        </div>
    </div>
</div>
