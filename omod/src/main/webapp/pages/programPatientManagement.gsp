<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {label: "All Patients list"}
    ];

    var jq = jQuery
    
    jq(document).ready(function () {
        jq('#resetBtn').on('click', function () {
            jq('#patientSearch').val('')
            jq('#filterGender').val('')
            jq('#filterDateRegistered').val('')
            jq('#filterStatus').val('')

            updateTable();
        });

        jq('#inquireBtn').on('click', function () {
            updateTable();
        });

        updateTable()
    });

    var updateTable = function () {
        getPatientsByAdvancedSearch(jq('#filterDateRegistered').val(),
            jq('#patientSearch').val(),
            jq('#filterGender').val(),
            jq('#filterStatus').val()
        );
    }

    function initializePatientsTable() {
        var table = jq('#patientsTable').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'className': 'select-checkbox',
                    'checkboxes': {
                        'selectRow': true,
                        'select': true
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'dom': 'lBrtip',
            'buttons': [
                {
                    'extend': 'collection',
                    'text': 'More operations',
                    'className': 'btn btn-sm p-1 ',
                    'exportOptions': {
                        'modifier': {
                            'selected': true
                        }
                    },
                    'buttons': [
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        },
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        }
                    ],
                    'dropup': false
                }
            ],
            'searching': true,

            "pagingType": 'simple_numbers',

            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                }
            },
            'order': [[1, 'asc']]
        });
        jq('#selectedRowsAlertDiv').hide();

        table.on('select', function (e, dt, type, indexes) {
            if (table.rows({selected: true}).count() === 1) {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " item selected");
                jq('#selectedRowsAlertDiv').show();
            } else {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                jq('#selectedRowsAlertDiv').show();
            }
        })
            .on('deselect', function (e, dt, type, indexes) {
                if (table.rows({selected: true}).count() === 0) {
                    jq('#selectedRowsAlertDiv').hide();
                } else {
                    jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                    jq('#selectedRowsAlertDiv').show();
                }

            });

        table.buttons().container().appendTo(jq('#other '));

        jq('.card-link').click(() => {
            if (jq('#genderDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });
    }

    function populateTbody(data) {
        jq('#patientsTable').DataTable().buttons().destroy();
        jq('#patientsTable').DataTable().clear().destroy();

        data.map((item) => {
            let returnUrl =  urlUtils.encodeUrl("/${ui.contextPath()}/botswanaemr/programPatientManagement.page?programId=${programId}");
            var url = "href = /${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=" + item.patientId + "&returnUrl=" + returnUrl;
            var treatmentUrl = "href = /${ui.contextPath()}/botswanaemr/programs/programs.page?patientId=" + item.patientId + "&ensureActiveVisit=false&programId=${programId}&returnUrl=" + returnUrl;
            jq("#patientDataTbody").append(`
            <tr>
                <td></td>
                <td>\${item.identifier}</td> 
                <td>\${item.name}</td>
                <td>\${item.gender}</td> 
                <td>\${item.dateOfBirth}</td>
                <td>\${item.age}</td>
                <td>\${item.registeredDate}</td>
                <td>\${item.status}</td>
                <td>
                    <a \${treatmentUrl} class='color-primary'>Treat | </a>
                    <a \${url} class='color-primary'>View profile</a>
                </td> 
            </tr>`);
        });

        initializePatientsTable();
    }

    function getPatientsByAdvancedSearch(dateRegistered, nameOrUniqueId, gender, status) {
        var searchResult;
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","getPatientsByAdvancedSearch")}',
            dataType: "json",
            global: false,
            async: true,
            data: {
                dateRegistered: dateRegistered,
                nameOrUniqueId: nameOrUniqueId,
                gender: gender,
                status: status,
                programId: '${programId}'
            },
            success: function (data) {

                searchResult = data;
                populateTbody(searchResult)

            }
        });
    }

    function actionRedirect(patientId) {
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patientId=" + patientId;
    }
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-horizontal form-group">
                        <label for="patientSearch">Search:</label>
                        <input type="text" class="form-control" id="patientSearch"
                               name="patientSearch" placeholder="Search by Patient ID, or name">
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse" id="genderDiv">
                        <label for="filterGender">Sex:</label>
                        <select class="custom-select form-control" id="filterGender">
                            <option selected disabled>Select Sex</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-horizontal form-group">
                        <label for="filterStatus">Status:</label>
                        <select class="custom-select form-control" id="filterStatus">
                            <option selected disabled>Select status:</option>
                            <option value="Active">Active</option>
                            <option value="Expired">Expired</option>
                        </select>
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse" id="dateDiv">
                        <label for="filterDateRegistered">Date:
                        </label>
                        <input type="text" 
                               class="form-control datepicker"
                               id="filterDateRegistered"
                               name="filterDateRegistered"
                               placeholder="Select date registered"
                        />
                        <script type="text/javascript">
                            jq('#filterDateRegistered').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                "setDate": new Date(),
                                dateFormat: "dd-mm-yy",
                                yearRange: "-150:+0",
                                maxDate: 0,
                                "autoclose": true,
                                onSelect: function (data) {
                                    jq("#filterDateRegistered").trigger('change');
                                }
                            });
                        </script>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
                                ${ui.message("Search")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <button type="submit" id="resetBtn" class="btn btn-sm btn-dark bg-dark mb-3">
                                ${ui.message("Reset")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <a id="collapseBtn" class="card-link btn btn-sm btn-warning bg-white color-primary border-0"
                               data-target=".multi-collapse"
                               aria-expanded="false"
                               aria-controls="dateDiv genderDiv">
                               <span>${ui.message("Expand")}
                                    <i id="collapse-icon" class="icon-chevron-down"></i>
                               </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <%
                        //TODO: Make this check configurable
                    %>
                    <% if (sessionLocation == "Registration Desk") { %>
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" onclick="redirectToNewRegistrationPage()"
                                    class="btn btn-sm btn-primary">
                                <i class="icon-plus"></i>   Register New Patient
                            </button>
                            <script type="text/javascript">
                                let redirectToNewRegistrationPage = () => {
                                    window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/startRegistration.page";
                                };
                            </script>
                        </div>

                        <div id="other" class="col-auto"></div>
                    </div>
                    <% } %>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div id="selectedRowsAlertDiv" class="alert alert-info color-info p-0" role="alert">
                                <label class="icon-info-sign color-primary" for="selectedRowsCount"></label>
                                <label id="selectedRowsCount"></label>
                            </div>

                            <div class="table-responsive">
                                <table id="patientsTable"
                                       class="table table-sm table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-left">Patient ID</th>
                                        <th class="text-left">Name</th>
                                        <th class="text-left">Sex</th>
                                        <th class="text-left">Date of birth</th>
                                        <th class="text-left">Age</th>
                                        <th class="text-left">Registered on</th>
                                        <th class="text-left">Status</th>
                                        <th class="text-left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="patientDataTbody">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
