<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Proficiency Tests"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col pl-0">
                        <h5 class="text-primary text-uppercase">Proficiency Tests Listing</h5>
                    </div>
                </div>
            </div>
            ${ui.includeFragment("botswanaemr", "hts/proficiencyTestsListing")}
        </div>
    </div>
</div>