<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Client Profile"}
    ];
    jQuery(document).ready(function () {
        console.log("js loaded ..: ...");
        let hasCompletedHtsDailyForm = jQuery("#hasCompletedHtsDailyForm").val();
        if (hasCompletedHtsDailyForm === false) {
            verificationForm.attr('href', '');
            verificationForm.click(function(e) {
                jq().toastmessage('showErrorToast', "First Complete the New Diagnostic test form to Continue!!");
                e.preventDefault();
            });
        }
    });
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span>
                </div>
                <div class="col-3">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0 hidden">
                        <div class="row">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="col">Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
                        <div class="row">
                            <div class="col-12 pl-0 pr-0">
                                <h5 class="text-primary text-left mt-3 pl-3">PNS FORMS</h5>
                                <hr class="divider pt-1 bg-primary ml-3"/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <table id="availableForms" class="table table-bordered mt-2">
                                            <thead>
                                                <tr>
                                                    <th scope="col" class="bg-pale">
                                                        <h6 class="text-dark text-center">Available Forms:</h6>
                                                        <input type="hidden" id="hasCompletedHtsDailyForm" value="${hasCompletedHtsDailyForm}"/>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% if (hasVisit) { %>
                                                    <% contacts.each { %>
                                                    <tr>
                                                        <td class="text-primary text-center text-uppercase">
                                                           <div class="row">
                                                               <div class="col">
                                                                   ${it.name ?: ""}
                                                               </div>
                                                               <% if (it.relation == sexualPartnerConceptId || it.relation == ivduConceptId) {%>
                                                                  <% if (it.hasIpvScreeningResult) { %>
                                                                      <% if (it.ipvScreeningResult == 'Positive') { %>
                                                                       <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'editEncounter', [
                                                                                   encounterId: it.ipvScreeningEncounter.uuid,
                                                                                   patientId: patient.id,
                                                                                   formUuid : ipvScreeningForm.form.uuid,
                                                                                   visitId  : currentVisit.uuid,
                                                                                   pnsContactRecordId: it.pnsContactRecordId,
                                                                                   pnsContactName: it.name?.replace(' ','__'),
                                                                                   pnsContactAgeCategory: it.ageCategory,
                                                                                   returnUrl: returnUrl])}">
                                                                               <small><i>(${ipvScreeningForm.form.name})</i></small>
                                                                           </a>
                                                                       </div>
                                                                        <% } %>
                                                                        <% if (it.ipvScreeningResult == 'Negative') { %>
                                                                       <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                   patientId: patient.id,
                                                                                   formUuid : partnerCharacteristicAndHivForm.form.uuid,
                                                                                   visitId  : currentVisit.uuid,
                                                                                   pnsContactName: it.name?.replace(' ','__'),
                                                                                   returnUrl: returnUrl])}">
                                                                               <small><i>(${partnerCharacteristicAndHivForm.form.name})</i></small>
                                                                           </a>
                                                                       </div>
                                                                       <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                   patientId: patient.id,
                                                                                   formUuid : contactsForm.form.uuid,
                                                                                   visitId  : currentVisit.uuid,
                                                                                   pnsContactName: it.name?.replace(' ','__'),
                                                                                   returnUrl: returnUrl])}">
                                                                               <small><i>(${contactsForm.form.name})</i></small>
                                                                           </a>
                                                                       </div>
                                                                       <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                   patientId: patient.id,
                                                                                   formUuid : referralsAndOutcomeForm.form.uuid,
                                                                                   visitId  : currentVisit.uuid,
                                                                                   pnsContactName: it.name?.replace(' ','__'),
                                                                                   returnUrl: returnUrl])}">
                                                                               <small><i>(${referralsAndOutcomeForm.form.name})</i></small>
                                                                           </a>
                                                                       </div>
                                                                        <% } %>
                                                                   <% } else { %>
                                                                       <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                   patientId: patient.id,
                                                                                   formUuid : ipvScreeningForm.form.uuid,
                                                                                   visitId  : currentVisit.uuid,
                                                                                   pnsContactRecordId: it.pnsContactRecordId,
                                                                                   pnsContactName: it.name?.replace(' ','__'),
                                                                                   pnsContactAgeCategory: it.ageCategory,
                                                                                   returnUrl: returnUrl])}">
                                                                               <small><i>(${ipvScreeningForm.form.name})</i></small>
                                                                           </a>
                                                                       </div>
                                                                   <% } %>
                                                               <% } else { %>
                                                                    <% if (it.hasFilledChildrenIndexForm) { %>
                                                                        <div class="col">
                                                                           <a href="${ui.pageLink('botswanaemr', 'editEncounter', [
                                                                                encounterId: it.childrenOfIndexInformationEncounter.encounterUuid,
                                                                                patientId: patient.id,
                                                                                formUuid : childrenOfIndexInformationForm.form.uuid,
                                                                                visitId  : currentVisit.uuid,
                                                                                pnsContactName: it.name?.replace(' ','__'),
                                                                                returnUrl: returnUrl])}">
                                                                               <u>FORM 3 <br/>
                                                                                   <small><i>(${childrenOfIndexInformationForm.form.name})</i></small>
                                                                               </u>
                                                                           </a>
                                                                        </div>
                                                                    <% } else { %>
                                                                     <div class="col">
                                                                         <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                            patientId: patient.id,
                                                                            formUuid : childrenOfIndexInformationForm.form.uuid,
                                                                            visitId  : currentVisit.uuid,
                                                                            pnsContactName: it.name?.replace(' ','__'),
                                                                            returnUrl: returnUrl])}">
                                                                                <u>FORM 3 <br/>
                                                                                <small><i>(${childrenOfIndexInformationForm.form.name})</i></small>
                                                                            </u>
                                                                         </a>
                                                                     </div>
                                                                     <% } %>
                                                               <% } %>
                                                           </div>
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                <% } else { %>
                                                    <tr>
                                                        <td class="text-center">
                                                            <div class="alert alert-warning text-danger" role="alert">
                                                                <strong><i class="fa fa-warning"></i>
                                                                    Please start a patient visit to proceed!
                                                                </strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <% } %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 mr-0 pr-0">
                                        <div class="table-responsive">
                                                <table class="table table-bordered mt-2">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">Completed Forms:</h6></th>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">Partner:</h6></th>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">Date:</h6></th>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">User:</h6></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <% encounters.each { %>
                                                        <tr>
                                                            <td class="text-primary text-uppercase">
                                                                <u>${it.form.name}</u>
                                                                <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                                                                   onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.encounterUuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                                                                </i>
                                                            </td>
                                                            <td>${it.partnerName}</td>
                                                            <td>${it.encounterDatetime.format("yyyy-MM-dd")}</td>
                                                            <td>${it.creator.personName.fullName}</td>
                                                        </tr>
                                                    <% } %>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
