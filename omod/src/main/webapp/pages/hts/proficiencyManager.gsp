<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Screening / HTS: Proficiency Testing Form"}
    ];

    jq(document).ready(function(){
		jQuery("#deleteProficiencyTestingModal").modal('show');
	});
</script>

<div class="row">
    <div class="col">
        <div class="card">
            ${ui.includeFragment("botswanaemr", "hts/deleteProficiencyTesting")}
        </div>
    </div>
</div>
