<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Add Self Test Kit"}
    ];
  

jQuery(function() {
    jq('#submitRequisitionBtn').on('click', (e) => {
        e.preventDefault();
        let date = jQuery('#date').val();

        let mainStockRoom = jQuery('#mainStockRoom').val().trim();
                let purpose = jQuery('#purpose').val().trim();

        let lotNumber = jQuery('#lotNumber').val().trim();
        let expiryDate = jQuery('#expiryDate').val();
        let quantity = jQuery('#quantity').val();
        let remarks = jQuery('#remarks').val();
                let distributor = jQuery('#distributor').val();


        if (date.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select stocking date to proceed");
            return;
        }

        if ( mainStockRoom.length === 0) {
            jq().toastmessage('showErrorToast', "Select Stock room to proceed");
            return;
        }

        if ( purpose.length === 0) {
            jq().toastmessage('showErrorToast', "Select purpose to proceed");
            return;
        }

        if (lotNumber.length === 0) {
            jq().toastmessage('showErrorToast', "Enter lot number to proceed");
            return;
        }

        if (expiryDate.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select expiry date to proceed");
            return;
        }


        if (quantity.length === 0) {
            jq().toastmessage('showErrorToast', "Enter quantity to proceed");
            return;
        }

        if (distributor.length === 0) {
            jq().toastmessage('showErrorToast', "Enter Distributor's name to proceed");
            return;
        }

        if (remarks.length === 0) {
            jq().toastmessage('showErrorToast', "Enter remarks to proceed");
            return;
        }

        saveSelfTestKit(date, mainStockRoom, purpose, lotNumber, expiryDate, quantity, distributor, remarks);
    });


    jq('#date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        "autoclose": true
    });
    jq("#date").datepicker("setDate", new Date());



    jq('#expiryDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        minDate: 0,
        "autoclose": true
    });
    jq("#expiryDate").datepicker("setDate", new Date());

    // Handle removal of items
    jq("#productsTable tbody").on("click", "a", function () {
        jq(this).parents("tr").remove();
    });
});

function saveSelfTestKit(date, mainStockRoom, purpose, lotNumber, expiryDate, quantity, distributor, remarks) {          
    var searchResult;
    jQuery.ajax({
        url: '${ui.actionLink("botswanaemr","hts/HtsAction","saveSelfTestKit")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemId: 2,
            date: date,
            purpose: purpose,
            lotNumber: lotNumber,
            expiryDate: expiryDate,
            mainStockRoom: mainStockRoom,
            quantity: quantity,
            distributor: distributor,
            remarks: remarks

        },
        success: function(data) {
            jq().toastmessage('showNoticeToast', "Operation completed successfully");
            jQuery('#mainStockRoom').val('');
            jQuery('#remarks').val('');
            jQuery('#distributor').val('')
            jQuery('#purpose').val('');
            location.href = '${ui.pageLink("botswanaemr", "hts/selfTestKit")}';
            
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
}
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Self test kit details</h5>
                                <p>Capture the lot number of the self test kit & add the required fields</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-4 pr-3">
                                    <label for="date">Date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="date" name="date" class="form-control" placeholder="Enter stock date">
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="mainStockRoom"> Main Stock room:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="mainStockRoom" class="form-control">
                                        <option value="" selected>Select Source stock room</option>
                                        <% mainStockRooms.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="purpose"> Purpose:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="purpose" class="form-control">
                                        <option value="Primary" >Pr - Primary</option>
                                        <option value="Secondary" >Se - Secondary</option>
                                        <option value="Both" >Bo - Both</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6 pr-3">
                                    <label for="lotNumber">Lot Number:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="lotNumber" name="date" class="form-control" placeholder="Enter Lot number">
                                </div>
                                <div class="form-group col-md-6 pr-3">
                                    <label for="expiryDate">Expiry Date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="expiryDate" name="expiryDate" class="form-control" placeholder="Enter expiry date">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6 pr-3">
                                    <label for="quantity">Quantity:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" id="quantity" name="date" class="form-control" placeholder="Enter quantity">
                                </div>
                                <div class="form-group col-md-6 pr-3">
                                    <label for="distributor">Distributor Name:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="distributor" name="distributor" class="form-control" placeholder="Enter Distributor name">
                                </div>
                            </div>



                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="remarks">Remarks:
                                        <span class="text-danger"></span>
                                    </label>
                                    <textarea class="form-control" rows="2"  id="remarks" name="remarks" placeholder="Enter remarks"></textarea>
                                </div>
                                <div class="form-group col-md-6">
                                        <div class="col pr-0">
                                            <button id="submitRequisitionBtn" type="button" class="btn btn-success float-right mr-0">Issue Requisition</button>
                                        </div>
                                </div>
                            </div>

                            <h5 class="text-primary">PRODUCT DETAILS</h5>

                            <hr class="divider pt-1 bg-primary"/>


                            <div class="table-responsive">
                                <table id="productsTable"
                                       class="table table-striped table-sm table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Purpose</th>
                                            <th>Lot Number</th>
                                            <th>Expiry Date</th>
                                            <th>Quantity</th>
                                            <th>Remarks</th>
                                            <th>Distributor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                    <% stockOperations.each { %>
                                                        <tr>
                                                            <td>${it.id}</td>
                                                            <td>${ui.formatDatePretty(it.operationDate)}</td>
                                                            <td>
                                                            <%
                                                                for (attribute in it.activeAttributes) {
                                                                    if(attribute.attributeType.name == 'Purpose'){
                                                                    println attribute.value

                                                                    }
                                                                }
                                                                %>
                                                            
                                                            </td>
                                                            <td>${it.operationNumber}</td>
                                                            <td>${it.items[0]?.expiration ? ui.formatDatePretty(it.items[0].expiration) : ''}</td>
                                                            <td>${it.items[0].quantity}</td>
                                                            <td>${it.description}</td>

                                                            <td> 
                                                            <%
                                                                for (attribute in it.activeAttributes) {
                                                                    if(attribute.attributeType.name == 'Distributor name'){
                                                                    println attribute.value

                                                                    }
                                                                }
                                                                %>
                                                            
                                                            </td>                                                            
                                                        
                                                        </tr>
                                                    <% } %>
                                    </tbody>
                                </table>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
