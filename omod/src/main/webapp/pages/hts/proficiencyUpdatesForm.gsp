<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Screening / HTS: Proficiency Testing Form"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col pl-0">
                        <h5 class="text-primary text-uppercase">Proficiency Testing Form</h5>
                    </div>
                </div>
            </div>
            ${ui.includeFragment("botswanaemr", "hts/updateProficiencyTestingForm")}
        </div>
    </div>
</div>