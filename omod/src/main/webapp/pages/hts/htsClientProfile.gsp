<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Client Profile"}
    ];
    jQuery(document).ready(function () {
        console.log("js loaded ..: ...");
        let hasCompletedDiagnosticsTesting = ${hasCompletedDiagnosticsTesting} // jQuery("#hasCompletedDiagnosticsTesting").val();
        let verificationForm = jQuery("#availableForms tbody tr:first-child").find("ul li").filter(function() {
            return jQuery(this).text().trim() === "New Verification Test";
        });

        if (hasCompletedDiagnosticsTesting === false) {
            verificationForm.attr('href', '');
            verificationForm.attr('style', 'color: #c7c7c7;');
            verificationForm.click(function(e) {
                jq().toastmessage('showErrorToast', "First Complete a Positive New Diagnostic Test Form or Document Prior Test Form to Continue!!");
                e.preventDefault();
            });
        }

        jQuery("#completedFormDate").on('change', function() {
            let value = jQuery(this).val().toLowerCase();
            let rows =  jQuery("#completedFormsTable tbody tr");
            rows.each(function() {
                if (!value) {
                    jQuery(this).show();
                    return;
                }
                let firstVal = jQuery(this).find("td:nth-child(2)");
                if (firstVal && firstVal.html() !== value) {
                    jQuery(this).hide();
                } else {
                    jQuery(this).show();
                }
            });
        });

        let todayDate = new Date().toISOString().split('T')[0];
        jQuery("#completedFormDate").val(todayDate).trigger('change');

        jQuery('#resetCompletedBtn').on('click', () => {
            fieldHelper.clearAllFields(jQuery('#completedForms'));
        });

        // For screening (Self-Test), both positive and negative, if selected, have a pop-up alert to refer to a diagnostic test. 
        <%  
        if (!hasCompletedDiagnosticsTesting && clientSelftested) {
        %>
            // jq().toastmessage('showWarningToast', 'Client underwent self-test screening. Refer to diagnostic test');
            jq().toastmessage('showToast', {
                text     : 'Client underwent self-test screening. Refer to diagnostic test',
                sticky   : true,
                position : 'top-right',
                type     : 'warning',
                close    : function () {console.log("toast is closed ...");}
            });
        <%
        }
        %>
    });
</script>
<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, showHivStatus: true])}
    </div>
</div>
<% if (hasIndeterminateOutcome || isDueForLabTesting) { %>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <button style="margin:10px;" type="button" class="btn btn-outline-danger right">
                        <% if (labOrderEncounterStatus == "PENDING") { %>
                            <a href="/${ui.contextPath()}/botswanaemr/lab/laboratoryOrders.page?encounterId=${labOrderEncounterId}" class="">
                                <i class="fa fa-folder-o"></i> Waiting for lab results
                            </a>
                        <% } else if (labOrderEncounterStatus != "COMPLETED") { %>
                            <a href="${ui.pageLink('botswanaemr', 'enterForm', [ patientId: patient.id,
                                formUuid : hivLabTestRequestFormUuid,
                                visitId  : currentVisit.uuid,
                                returnUrl: returnUrl])}">
                                <i class="fa fa-folder-o"></i> Patient needs to do a lab order. click to submit lab request
                            </a>
                        <% } %>
                    </button>
                </div>
                <div class="col-3">
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0 hidden">
                        <div class="row">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="col">Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                <% if (indeterminate) { %>
                    <div class="card-body">
                        <div class="row">
                                <div class="col">
                                <i class="fa fa-bell text-warning pl-2"></i>
                                    <span class="text-warning text-left">Patient has indeterminate HIV results</span>
                                </div>
                        </div>
                    </div>
                <% } %>
                 <% if (noResultsObtained) { %>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <i class="fa fa-bell text-warning pl-2"></i>
                                    <span class="text-warning text-left">Pending Results</span>
                            </div>
                        </div>
                    </div>
                 <% } %>
                    <div class="card-body">
                        ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
                        ${ ui.includeFragment("botswanaemr", "hts/htsPastTests", [patientId: patient.uuid])}
                        <div class="row">
                            <div class="col-12 pl-0 pr-0">
                                <h5 class="text-primary text-left mt-3 pl-3">HTS FORMS</h5>
                                <hr class="divider pt-1 bg-primary ml-3"/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <table id="availableForms" class="table table-bordered mt-2">
                                            <thead>
                                                <tr>
                                                    <th scope="col" class="bg-pale">
                                                        <h6 class="text-dark text-center">Available Forms:</h6>
                                                        <input type="hidden" id="hasCompletedDiagnosticsTesting" value="${hasCompletedDiagnosticsTesting}"/>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% if (hasVisit) { %>
                                                    <tr>
                                                        <td style="text-align:left" class="text-capitalize">
                                                            <div class="row">
                                                                <div class="col font-weight-bold">HIV Testing and Counselling</div>
                                                            </div>
                                                            <% if (!isHivPositive) { %>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <ul>
                                                                    <% hivTestingAndCounsellingForms.each { %>
                                                                    <li class="text-capitalize">
                                                                        <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                patientId: patient.id,
                                                                                formUuid : it.form.uuid,
                                                                                visitId  : currentVisit.uuid,
                                                                                returnUrl: returnUrl])}">
                                                                            <u>${it.form.name}</u>
                                                                        </a>
                                                                    </li>
                                                                    <% } %>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <% } %>
                                                    <div class="row mt-1">
                                                        <div class="col font-weight-bold">Self Testing</div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col">

                                                            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                    patientId: patient.id,
                                                                    formUuid : selftestkit.form.uuid,
                                                                    visitId  : currentVisit.uuid,
                                                                    returnUrl: returnUrl])}">
                                                                <u>${selftestkit.form.name}</u>
                                                            </a>
                                                    </div>
                                                    </div>
                                                    <% if (htsReferral != null && isHivPositive) {%>
                                                            <div class="row mt-1">
                                                                <div class="col font-weight-bold">Referral</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                            patientId: patient.id,
                                                                            formUuid : htsReferral.form.uuid,
                                                                            visitId  : currentVisit.uuid,
                                                                            returnUrl: returnUrl])}">
                                                                        <u>${htsReferral.form.name}</u>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <% } %>
                                                            <% if (isHivPositive || hasCompletedDiagnosticsTesting) {%>
                                                            <div class="row mt-1">
                                                                <div class="col font-weight-bold">Partner notification</div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <ul>
                                                                        <% if (indexCaseInterview != null) { %>
                                                                        <li>
                                                                            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                                    patientId: patient.id,
                                                                                    formUuid : indexCaseInterview.form.uuid,
                                                                                    visitId  : currentVisit.uuid,
                                                                                    returnUrl: returnUrl])}">
                                                                                <u>${indexCaseInterview.form.name}</u>
                                                                            </a>
                                                                        </li>
                                                                        <%}%>
                                                                        <li>
                                                                            <a href="/${ui.contextPath()}/botswanaemr/hts/pnsClientProfile.page?patientId=${patient.id}" id="view"><u>PNS Contacts & children</u></a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <% } %>
                                                <% } else { %>
                                                    <tr>
                                                        <td class="text-center">
                                                            <div class="alert alert-warning text-danger" role="alert">
                                                                <strong><i class="fa fa-warning"></i>
                                                                    Please start a patient visit to proceed!
                                                                </strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <% } %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6 mr-0 pr-0" id="completedForms">
                                        <div class="table-responsive">
                                            <div class="row">
                                                <div class="col-4">
                                                    <div>
                                                        <input type="text"
                                                               class="form-control py-2"
                                                               id="completedFormDate"
                                                               name="completedFormDate"
                                                               placeholder="YYYY-MM-DD">
                                                        <script type="text/javascript">
                                                            jQuery('#completedFormDate').datepicker({
                                                                changeMonth: true,
                                                                changeYear: true,
                                                                showButtonPanel: true,
                                                                "setDate": new Date(),
                                                                dateFormat: "yy-mm-dd",
                                                                yearRange: "-150:+0",
                                                                maxDate: 0,
                                                                "autoclose": true,
                                                                onSelect: function (data) {
                                                                    jQuery("#completedFormDate").trigger('change');
                                                                }
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="col-2 pl-0 pr-0">
                                                    <button type="submit" class="btn btn-outline btn-secondary float-right" id="resetCompletedBtn" name="resetCompletedBtn">
                                                        <i class="fa fa-undo fa-1x"></i>
                                                    </button>
                                                </div>
                                            </div>

                                            <table class="table table-bordered mt-2" id="completedFormsTable">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" class="bg-pale"><h6 class="text-dark">Completed Forms:</h6></th>
                                                        <th scope="col" class="bg-pale"><h6 class="text-dark">Date:</h6></th>
                                                        <th scope="col" class="bg-pale"><h6 class="text-dark">User:</h6></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <% encounters.each { %>
                                                    <tr>
                                                        <td class="text-primary text-uppercase">
                                                            ${it.form.name} <span class="sub-text">(${it.encounterDatetime.format('yyyy-MM-dd hh:mm a')})</span>
                                                            <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                                                               onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                                                            </i>
                                                        </td>
                                                        <td>${it.encounterDatetime.format("yyyy-MM-dd")}</td>
                                                        <td>${it.creator.personName.fullName}</td>
                                                    </tr>
                                                <% } %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
