<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "HTS"}
    ];
</script>
<style>
    .toast {
        width: 400px;
        max-width: 100%;
        font-size: .875rem;
        pointer-events: auto;
        background-color: rgba(255, 255, 255, .85);
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .1);
        box-shadow: 0 0.5rem 1rem rgba(0,0,0,.15);
        border-radius: 0.25rem;
    }
    .toast-header {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding: 0.75rem;
        color: #6c757d;
        background-color: rgba(255, 255, 255, .85);
        background-clip: padding-box;
        border-bottom: 2px solid rgba(0, 0, 0, .1);
    }
    .close {
        float: right;
        font-size: 1.5rem;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .5;
    }
    .end-0 {
        right: 0;
    }
    .top-10 {
        top: 15%;
    }
</style>
<script>
    jq(document).ready(function() {
        let qcCompleted = ${qcCompleted};
        let missingTestingPoints = ${missingTestingPoints};

        try {
            if (!qcCompleted) {
                let message = jq("#toast-body").html() + '<b>' + missingTestingPoints.toString() + '</b>';
                jq("#toast-body").html(message);
                jq("#liveToast").toast({
                    'autohide': false
                });
                jq("#liveToast").toast('show');
            }
        } catch (error) {
            console.error('Toast error:', error);
        }

        // toggle statistics
        jq('.collapsible-button').click(function() {
            let collapsible = jq('.collapsible');
            if (collapsible.length > 0) {
                collapsible.slideToggle();
            }
        });

    toggleGraph();
    });
</script>
<div class="position-fixed bottom-0 end-0 top-10 mr-2 p-3" style="z-index: 11">
  <div id="liveToast" class="toast fade hide" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <svg class="bd-placeholder-img rounded me-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"><rect width="100%" height="100%" fill="#dc3545"></rect></svg>

      <strong class="ml-1 mr-auto">Missing Lab Quality Control</strong>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true" style="color: #000;">x</span>
      </button>
    </div>
    <div id="toast-body" class="toast-body">
      Hello, weekly lab quality control not done for:
    </div>
  </div>
</div>

<div id="toastContainer" class="toast-container"></div>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible" style="display:none;">
    <div class="col collapsible-content">
        <div class="card">
            <div class="row text-primary">
                <div class="col-12 pl-0 pr-0">
                <a href="${ui.pageLink('botswanaemr', 'graphPatientView', [type: "SCREENING" , encounterType: "f90231be-7ea1-11ed-a1eb-0242ac120002", day: "all"])}">
                    Total Tested: <span class="text-success">$totalTestedPatientCount</span>
                    </a>
                    <div class="px-0">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "SCREENING", encounterType: "f90231be-7ea1-11ed-a1eb-0242ac120002"]) }
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col pl-0">TESTED TODAY:
                    <span>$totalTestedPatientCountToday</span>
                </div>
                <div class="col pl-0">TESTED YESTERDAY:
                    <span>$totalTestedPatientCountYesterday</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col hidden">
        <div class="card">
            <div class="row text-primary">
                <div class="col">
                    <div class="card bg-pale">
                        <h5 class="small text-dark">AVAILABLE DETERMINE TEST KITS</h5>
                        <span class="display-4 bg-pale">${determineQuantity}</span>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-pale">
                        <h5 class="small text-dark">AVAILABLE UNIGOLD TEST KITS</h5>
                        <span class="display-4 bg-pale">${unigoldQuantity}</span>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-pale">
                        <h5 class="small text-dark">KIT AVAILABILITY RATIO</h5>
                        <span class="display-4 bg-pale">34%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-9">
                                <h5>Screening Pool</h5>
                            </div>
                            <div class="col pr-0">
                                <a href="${ui.pageLink('botswanaemr', 'startRegistration')}" class="btn btn-primary text-white mr-0 float-right">
                                   <i class="fa fa-user-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="htsPatientPool" class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "hts/htsPatientPool")}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="activity-trail">
                <div class="card mt-4">
                    <h4>Activity Trail</h4>
                    <% todayHTSList.each { %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>${it.creator}</span> completed the screening of patient
                                <span class="text-success">${it.name}</span>
                                <span class="text-muted">${it.duration}</span>
                            </small>
                        </li>
                    </ul>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>
