<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.includeJavascript("botswanaemr", "utils.js")
    ui.includeJavascript("botswanaemr", "payment-history.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<script type="text/javascript">
    let breadcrumbs = _.compact(_.flatten([
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.message("Start Registration") }",
            link: "${ ui.pageLink("botswanaemr", "startRegistration") }"
        },
        {
            label: "Confirm Details"
        }
    ]));

    jq(function() {
       jq("#person-confirmation").submit(function(e) {
           e.preventDefault();
           let action = jq(document.activeElement).text().trim(); // Get the value of the button that triggered the action
           <% if (action == null ||  action == "") {%>
           if (action.indexOf('Proceed') > -1) {
               jq("#actionPage").val("capturePayment");
           } else if (action.indexOf('Profile') > -1) {
               jq("#actionPage").val("profile");
           }
           <% } else {%>
           if (action.indexOf('Proceed') > -1) {
               jq("#actionPage").val("captureDelayedPayment");
           } else if (action.indexOf('Profile') >-1) {
               jq("#actionPage").val("profile");
           }
           <%}%>
           e.target.submit();
           return false;
       });

        function getOpenCRQueryParams(name) {
            let urlParams = new URLSearchParams(window.location.search);
            return urlParams.get(name);
        }

        function getUrlParameter(name) {
            const urlParams = new URLSearchParams(window.location.search);
            return urlParams.get(name);
        }

        jq(document).on('click', '#register', function() {
            showLoadingOverlay();
            jq('#person-confirmation').submit();        
        });

        let checkRecordInClientRegistry = function() {
            let patientId = "$patientUuid";   
            let idNumber = "$identifierValue";
            let idType = "$identifierType";

            let payload = {
                idNumber: idNumber,
                identifierType: idType
            };
            
            // Async call to OpenCR
            jq.ajax({
                url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/registry/getPatientByAdvancedSearch",
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(payload),
                success: function (response) {
                    let hospital = "${location}";

                    let results = response;

                    if (results.status === '404 NOT_FOUND') {
                        jq('#register').text('Post to Registry and Proceed');
                        jq('#register').attr('id', 'postToRegistry');
                        jq('.bg-info').text('Registered at this Facility but missing in Client Registry');

                        jq(document).on('click', '#postToRegistry', function() {
                            let patientUuid = "$patientUuid";
                            let location = "$locationName";

                            let postData = {
                                patientUuid: patientUuid,
                                location: location
                            };

                            let openCrPostUrl = '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/registry/savePatientInRegistry";
                            showLoadingOverlay();
                            jq.ajax({
                                url: openCrPostUrl,
                                type: 'POST',
                                contentType: 'application/json',
                                data: JSON.stringify(postData),
                                success: function(data, textStatus, jqXHR) {
                                    if (data.status === '201 CREATED') {
                                        // console.log('Patient was successfully created or updated in Client Registry:', data);
                                        //window.location.href = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard';
                                    } else {
                                        // console.error('Failed to create patient in client Registry:', data);
                                        // alert('Failed to create patient in Client Registry. Please try again.');
                                        jq().toastmessage('showWarningToast', 'Failed to create patient in Client Registry');
                                        //TODO: Queue this record for future attempts @nkimaiga@intellisoftkenya.com
                                        //window.location.href = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard';
                                    }
                                    jq('#person-confirmation').submit();
                                },
                                error: function(xhr, status, error) {
                                    console.error('Error creating or updating patient in Client Registry:', error);
                                    jq('#person-confirmation').submit();
                                }
                            });

                        });
                    }
                },
                error: function () {
                    console.warn('Failed to communicate with client registry');
                    //window.location.href = proceedUrl;
                }
            });
        }

        <% if (mpiSearchEnabled) { %>
            checkRecordInClientRegistry();
        <% } %>
   });

</script>

<div class="row">
    <div class="col">
        <div class="center-container">
            <div class="row">
                <div class="col-md-12 card">
                    <form id="person-confirmation" method="post">
                        <p class="card-title">Person details :</p>

                        <div class="row p-0">
                            <div class="p-0 form-group col-sm-4">
                                <label class="form-label"><span
                                        class="text-dark">Name:</span> ${result.person.preferredName.display}
                                </label>
                            </div>

                            <div class="p-0 form-group col-sm-4">
                                <% def gen = result.person.gender %>
                                <% if (gen.equals('M')) { %>
                                <label class="form-label"><span class="text-dark">Sex:</span> Male</label>
                                <% } else if (gen.equals('F')) { %>
                                <label class="form-label"><span class="text-dark">Sex:</span> Female</label>
                                <% } else { %>
                                <label class="form-label"><span class="text-dark">Sex:</span> Other</label>
                                <% } %>
                            </div>

                            <div class="p-0 form-group  col-sm-4">
                                <label class="form-label"><span
                                        class="text-dark">Age: </span>${result.person.age}
                                </label>
                            </div>
                        </div>

                        <div class="form-label center ">
                            <p><strong>Info!</strong><span class="bg-info"> ${infoNote}</span>
                            </p>
                            <input name="patientUuid" value="${result.uuid}" style="display: none">
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-4">
                                <button id="back" type="reset" onclick="urlUtils.redirectToReturnUrl()"
                                        class="btn btn-sm  btn-block mb-3 btn-danger left">
                                    ${ui.message(" Back ")}
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button id="patientProfile" type="submit"
                                        class="btn btn-sm btn-primary mb-3 btn-block"><i
                                        class="icon-eye-open"></i>${ui.message("Profile")}
                                </button>
                                <script type="text/javascript">
                                    let redirectProfilePage = (patientId) => {
                                        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patientId=" + patientId
                                    };
                                </script>
                            </div>

                            <div class="col-md-4">
                                <input type="hidden" name="actionPage" id="actionPage"/>
                                <button id="register" type="button"
                                        class="btn btn-sm btn-primary btn-block mb-3 float-right">${ui.message("Proceed ")}<i
                                        class="icon-double-angle-right"></i>
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
