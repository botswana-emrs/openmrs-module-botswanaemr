<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "${ ui.message("Emergency Registration Form") }", link: "${ ui.pageLink("botswanaemr", "emergencyRegistration") }" }
    ]));
</script>

<script type="text/javascript">
    jq(document).ready(function () {
        function autoSelectGender() {
            var patientId = jq('#patientId').val();
            var fifthDigit = patientId.charAt(4);
            var genderVal = parseInt(fifthDigit);
            var gender = jq('#emergencyGender').val();
            var genderValid = true;

            if (genderVal < 1 || genderVal > 2) {
                jq("#genderError").text("Invalid ID number. The fifth number ID should be 1 or 2").show();
                jq("#patientIdError").text("Invalid ID number. The fifth number ID should be 1 or 2").show();
                genderValid = false;
            } else {
                var autoGender = (genderVal === 1) ? 'Male' : 'Female';
                jq('#emergencyGender').val(autoGender);
                if (genderVal === 1 && gender !== "Male") {
                    jq("#genderError").text("Gender should be Male. Please check if ID number is correct.").show();
                    genderValid = false;
                } else if (genderVal === 2 && gender !== "Female") {
                    jq("#genderError").text("Gender should be Female. Please check if ID number is correct.").show();
                    genderValid = false;
                } else {
                    genderValid = true;
                    jq("#genderError").hide();
                    jq("#patientIdError").hide();
                }
            }
        }

        jq('#patientId').on('input', autoSelectGender);
    });
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <h5>Patient registration form</h5>
                    <p>Complete the form to register an emergency patient</p>
                </div>
                <div class="col-3">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>
<div class="row">
  <div class="col">
      <div class="card">
          <div class="center-container">
            <form id="emergencyPatientRegistrationForm">
              <div class="card-title">
                  <h3 class="text-dark">Emergency patient</h3>
              </div>
              <br/>
              <h4 class="text-dark">Patient details</h4>
              <br/>
              <h3 class="text-black-50 body">Emergency patient</h3>
              <p>
                  An emergency patient will have a Patient ID created for them without any information inputted which can be filled out at a later stage.
              </p>

              <br/>
                <span style="display: none">
                    ${ui.includeFragment("botswanaemr", "widget/facilityLocation")}
                </span>

                <div class="form-group">
                    <label for="citizenType">Citizen Type :</label>
                    <div class="form-check form-check-inline" id="citizenType">
                        <input class="form-check-input" type="radio" name="citizenType" id="citizen" value="Citizen" data-attr="Citizen" checked>
                        <label class="form-check-label radio-label" for="citizen">Citizen</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="citizenType" id="nonCitizen" value="Non-citizen" data-attr="Non-citizen">
                        <label class="form-check-label radio-label" for="nonCitizen">Non-citizen</label>
                    </div>
                </div>

                <div class="form-group">
                    <label>Select identification type</label>

                    <div class="btn-group inline-button-group" role="group" >
                        <button type="button" id="btnId"
                                class="btn btn-outline-secondary outline-button default-input"
                                onclick="selectIdTypeIdentification(this.id)">ID</button>
                        <button type="button" id="btnPassport"
                                class="btn btn-outline-secondary outline-button default-input"
                                onclick="selectIdTypeIdentification(this.id)">Passport</button>
                        <button type="button" id="btnBirthCertificate"
                                class="btn btn-outline-secondary outline-button default-input"
                                onclick="selectIdTypeIdentification(this.id)">Birth Certificate</button>
                        <button type="button" id="noIdentification"
                                class="btn btn-outline-secondary outline-button default-input"
                                onclick="selectIdTypeIdentification(this.id)">No Identification</button>
                    </div>
                    <label id="idTypeError"
                           class="form-text text-danger">Patient identification type is required.</label>
                </div>

                <div class="form-group">
                    <label for="patientId"><span class="black-text" id="idNumberLabel">ID number</span>
                        <span class="required-label temp-label">*</span>
                    </label>

                    <div class="form-group is-loading">
                        <input type="number" class="form-control spinner-text default-input" id="patientId"
                               name="idNumber" required
                               placeholder="Enter the patient's ID number" minlength="9" maxlength="9">

                        <div class="spinner-border spinner-section" role="status" id="patientSpinner">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                    <label id="patientIdError"
                           class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                    <a class="badge badge-primary badge-pill text-white px-1 py-1" href="{{= url }}" id="existing">${ui.message("Register as an existing patient >>")}</a>
                </div>

                <div class="form-group">
                    <label for="name" class="text-dark">Full name</label>
                    <input type="text" class="form-control" id="name" aria-describedby="nameHelp"
                           name="fullName"
                           required placeholder="Enter the patient's full name">
                    <small id="nameHelp" class="form-text text-muted">Enter the patient's full name.</small>
                    <label id="nameError" class="error" for="name">This field is required.</label>
                </div>

                <div class="form-group">
                    <label>Patient Type :</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="patientType" id="adult" value="Unknown Adult" data-attr="Unknown Adult" >
                        <label class="form-check-label radio-label" for="adult" required>Unknown Adult</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="patientType" id="child" value="Unknown Child" data-attr="Unknown Child" >
                        <label class="form-check-label radio-label" for="child">Unknown Child</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="emergencyGender">Gender
                        <span class="required-label">*</span>
                    </label>
                    <select class="form-control custom-select" id="emergencyGender" name="emergencyGender" required>
                        <option disabled>Select the patient's sex</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                    <label id="genderError" class="form-text text-danger">Wrong gender selected.</label>
                </div>

              <br/>
              <br/>
              <br>
              <div>
                  <p style="display: inline">
                      <button id="next" type="submit" class=" btn btn-primary right "> ${ui.message("Complete")}
                      </button>

                  </p>
              </div>
            </form>
          </div>
      </div>
  </div>
</div>
