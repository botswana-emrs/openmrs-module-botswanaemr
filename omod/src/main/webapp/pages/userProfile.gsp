<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [title: ui.message("User profile")])
    ui.includeCss("adminui", "systemadmin/accounts.css")
    ui.includeCss("botswanaemr", "choices.min.css")
    ui.includeCss("botswanaemr", "base.min.css")
    ui.includeJavascript("botswanaemr", "choices.min.js")
%>
<script type="text/javascript">
    var breadcrumbs = [
        {label: "${ ui.message("User profile") }"}
    ];
</script>

<div class="col">
    <div class="row">
        <div class="col-sm-2">
            <!-- Tab navs -->
            <div

                    class="nav flex-column nav-pills text-left"
                    id="v-pills-tab"
                    role="tablist"
                    aria-orientation="vertical">
                <a
                        class="nav-link active"
                        id="v-pills-basic-settings-tab"
                        data-toggle="pill"
                        href="#v-pills-basic-settings"
                        role="tab"
                        aria-controls="v-pills-basic-settings"
                        aria-selected="true">Basic Settings</a>
                <a
                        class="nav-link"
                        id="v-pills-security-settings-tab"
                        data-toggle="pill"
                        href="#v-pills-security-settings"
                        role="tab"
                        aria-controls="v-pills-security-settings"
                        aria-selected="false">Security Settings</a>
                <a
                        class="nav-link"
                        id="v-pills-notification-settings-tab"
                        data-toggle="pill"
                        href="#v-pills-notification-settings"
                        role="tab"
                        aria-controls="v-pills-notification-settings"
                        aria-selected="false">Notification Settings</a>
            </div>
            <!-- Tab navs -->
        </div>

        <div class="col-9">
            <!-- Tab content -->
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active"
                     id="v-pills-basic-settings"
                     role="tabpanel"
                     aria-labelledby="v-pills-basic-settings-tab">
                    <form
                            id="basicDetailsForm" method="POST">
                        <div class="row">
                            <div class="col">
                                <div class="d-flex justify-content-between align-items-center mb-3">
                                    <h4 class="text-right">Basic Settings</h4>
                                </div>

                                <div class="col-md-12">
                                    <label class="labels">Name</label>
                                    <input type="text" class="form-control" name="givenName"
                                           placeholder="first name"
                                           value="${user.getGivenName() ?: ''}">
                                </div>

                                <div class="col-md-12">
                                    <label class="labels">Surname</label>
                                    <input type="text" class="form-control" name="familyName"
                                           placeholder="familyName"
                                           value="${user.getFamilyName() ?: ''}">
                                </div>

                                <div class="col-md-12">
                                    <label class="labels">ID</label>
                                    <input type="text"
                                           class="form-control spinner-text"
                                           id="userNationalId"
                                           name="userNationalId"
                                           value="${user.getPerson().getAttribute('USER-National-ID-Number') ?: ''}"
                                           placeholder="User's ID number" minlength="9">
                                </div>

                                <div class="col-md-12">
                                    <label for="email">Email ID</label>
                                    <input id="email"
                                           name="email"
                                           type="text"
                                           maxlength="250"
                                           class="form-control"
                                           placeholder="User's email address"
                                           value="${user.getEmail() ?: ''}">
                                </div>

                                <div class="col-md-12">
                                    <label for="phoneNumber">Phone Number
                                    </label>
                                    <input
                                            id="phoneNumber"
                                            name="telephoneNumber"
                                            type="tel"
                                            minlength="7"
                                            maxlength="8"
                                            class="form-control"
                                            placeholder="User's phone number"
                                            value="${user.getPerson().getAttribute('USER-Telephone-Contact') ?: ''}"/>
                                </div>

                                <div class="col-md-12">
                                    <label class="labels">Country</label>
                                    <input type="text" class="form-control" name="country"
                                           placeholder="country"
                                           value="${user.getPerson().getAttribute('USER-Country') ?: ''}">
                                </div>

                                <div class="col-md-12">
                                    <label class="labels">Street Address</label>
                                    <input type="text" class="form-control" name="streetAddress"
                                           placeholder="Street address"
                                           value="${user.getPerson().getAttribute('USER-Street-Address') ?: ''}">
                                </div>

                                <div class="col-md-12" disabled="true">
                                    <label class="labels">User type</label>
                                    <input type="text" name="userType" class="form-control" value="${roles}">
                                </div>

                                <div class="col-md-12" disabled="true">
                                    <label class="labels">Unit</label>
                                    <input type="text" name="unit" class="form-control" value="">
                                </div>
                            </div>

                            <div class="col">
                                <div class="col-md-3">
                                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                        <img class="rounded-circle mt-5" width="150px"
                                             src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="actions pr-5 pl-3">
                            <div class="col-md-6 mt-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Update Information
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

                <div
                        class="tab-pane fade"
                        id="v-pills-security-settings"
                        role="tabpanel"
                        aria-labelledby="v-pills-security-settings-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Security Settings</h4>
                    </div>
                    ${ui.includeFragment("botswanaemr", "admin/securitySettings")}

                </div>

                <div
                        class="tab-pane fade"
                        id="v-pills-notification-settings"
                        role="tabpanel"
                        aria-labelledby="v-pills-security-settings-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h4 class="text-right">Notification Settings content</h4>
                    </div>
                    ${ui.includeFragment("botswanaemr", "admin/notificationSettings")}
                </div>
            </div>
            <!-- Tab content -->
        </div>
    </div>
</div>
