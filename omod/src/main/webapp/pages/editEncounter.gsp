<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.message("Consultation"))) }" ,
            link: '${ returnUrl }'},
        {label: "${ ui.message("Edit Form") }"}
    ];

    //align the radio buttons well on EDIT Mode as in the ENTER mode
    jq(function() {
        jq('input[type=radio]').parents('span').addClass('form-check-inline left');
    });
</script>

${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<div class="row">
   <div class="col col-sm-12 col-md-12 col-lg-12">
       ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient])}
   </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            ${ ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
                    visit: encounter.visit,
                    encounter: encounter,
                    patient: patient,
                    returnUrl: returnUrl,
                    definitionUiResource: definitionUiResource ?: ""
            ]) }
        </div>
    </div>
</div>
