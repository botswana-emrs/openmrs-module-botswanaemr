<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/doctorDashboard.page?appId=botswanaemr.doctorDashboard'
        },
        {label: "Patient Followup"}
    ];
    var jq = jQuery;
    jq(document).ready(function () {
        jq('#missedAppointmentsTable').DataTable({
            searchPanes: true,
            searching: true,
            dom: 'rtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });
</script>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        <div class="hpanel">
            <div class="alert alert-info">
                <center>* You are expected to make followup on clients in the present view.</center>
                <center style="display: none">- First select the action you wish to perform</center>
                <center style="display: none">- Follow the dialog to send the respective action's notification to the client</center>
            </div>

            <div class="panel-body">
                <p>Scheduled Appointments</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "appointments/missedAppointments")}
    </div>
</div>

<div>
    <div class="col col-sm-12 col-md-12 col-lg-12">
    </div>
</div>

