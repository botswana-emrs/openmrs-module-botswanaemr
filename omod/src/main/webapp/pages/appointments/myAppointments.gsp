<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/doctorDashboard.page?appId=botswanaemr.doctorDashboard'
        },
        {label: "My Appointments"}
    ];
</script>

<!--<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "consultation/appointments", [allAppointments: "true"])}
    </div>
</div>-->

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "appointments/myAppointments")}
    </div>
</div>
