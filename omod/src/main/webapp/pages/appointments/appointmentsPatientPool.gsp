<%
if (sessionContext.authenticated && !sessionContext.currentProvider) {
throw new IllegalStateException("Logged-in user is not a Provider")
}
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/appointments/appointmentsManagement.page'
        },
        {label: "Appointments Patient Pool"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div id="appointmentsPatientPool" class="col">
                    ${ui.includeFragment("botswanaemr", "appointments/appointmentsPatientPool")}
                </div>
            </div>
        </div>
    </div>
</div>
