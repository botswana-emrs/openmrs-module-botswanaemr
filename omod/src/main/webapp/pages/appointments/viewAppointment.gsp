<%
if (sessionContext.authenticated && !sessionContext.currentProvider) {
throw new IllegalStateException("Logged-in user is not a Provider")
}
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/appointments/appointmentsManagement.page'
        },
        {label: "View Appointment"}
    ];
</script>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "appointments/viewAppointment")}
    </div>
</div>
