<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    const breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '${ui.pageLink("botswanaemr", "registrationAdminDashboard", [patient: patient.uuid])}'},
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "Consultation",
            link: '${ui.pageLink("botswanaemr", "consultation/consultation", [patientId: patient.uuid])}'
        },
        { label: "Appointment Scheduling"}
    ];
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#cancelAndGoBackToConsultation').click(function(e){
            e.preventDefault();
                window.history.back();
            });
        });

</script>
<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient])}
    </div>
</div>

<div class="row">
    <div class="col col-md-8">
        <h5>Schedule follow up consultation</h5>
    </div>
    <div class="col-md-4">
        <button class="btn btn-outline border-0 text-danger float-right" id="cancelAndGoBackToConsultation">
            <i class="fa fa-close text-dark"></i> Go back
        </button>
    </div>

</div>

<div class="row" style="margin: 10px; padding: 10px;">
    <div class="col-sm-12 card">
        <div class="fullwidth">
            <div> ${ui.includeFragment("botswanaemr", "appointments/schedule")}</div>
        </div>
    </div>
</div>
