<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    let title = "All " + '${title}';
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {label: title}
    ];

    var jq = jQuery
    let table;

    jq(document).ready(function () {

        initializePatientsTable();

        jq('#resetBtn').on('click', function () {
            jq('#patientSearch').val('')
            jq('#filterGender').val('')
            jq('#filterDateRegistered').val('')
            jq('#filterStatus').val('')
            jq('#patientsTable').DataTable().search('').draw();
        });

        jq('#inquireBtn').on('click', function () {
            jq('#patientsTable').DataTable().search(jq('#patientSearch').val).draw();
        });
        jq('#patientSearch').on('keyup', function() {
            var searchTerm = jq(this).val();
            jq('#patientsTable').DataTable().search(searchTerm).draw();
        });

        jq('#filterGender').on('change', function () {
            jq('#patientsTable').DataTable().column(3).search(jq(this).val()).draw();
        });
        jq('#filterStatus').on('change', function () {
            jq('#patientsTable').DataTable().column(7).search(jq(this).val()).draw();
        });
        jq('#filterDateRegistered').on('change', function () {
            jq('#patientsTable').DataTable().column(6).search(jq(this).val()).draw();
        });

        function dateRange(date) {
            var date1 = jq.datepicker.parseDate("dd-mm-yy", jq("#startDateRange").text());
            var date2 = jq.datepicker.parseDate("dd-mm-yy", jq("#stopDateRange").text());
            var isHighlight = date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2));

            return [true, isHighlight ? "dp-highlight" : ""];
        }

        function DRonSelect(dateText) {
            var date1 = jq.datepicker.parseDate("dd-mm-yy", jq("#startDateRange").text());
            var date2 = jq.datepicker.parseDate("dd-mm-yy", jq("#stopDateRange").text());

            if (!date1 || date2) {
                jq("#startDateRange").text(dateText);
                jq("#stopDateRange").text("");
                jq("#Datepicker").datepicker();
            } else {
                if (jq.datepicker.parseDate("dd-mm-yy", jq("#startDateRange").text()) >=
                    jq.datepicker.parseDate("dd-mm-yy", dateText)) {
                    jq("#startDateRange").text(dateText);
                    jq("#stopDateRange").text("");
                    jq("#Datepicker").datepicker();
                } else {
                    jq("#stopDateRange").text(dateText);
                    jq("#Datepicker").datepicker();
                    jq("#Datepicker .ui-datepicker").hide();
                    let dateVal = jq("#startDateRange").text() + " - " + jq("#stopDateRange").text();
                    jq("#searchDateRange").val(dateVal);
                    table.draw();
                }
            }
        }
        function datePicker() {
            jq(document).ready(function () {
                jq("#Datepicker").datepicker({
                    dateFormat: "dd-mm-yy",
                    minDate: null,
                    maxDate: "+3M +0D",
                    beforeShowDay: dateRange,
                    onSelect: DRonSelect
                });
            });
        }

        jq("#searchDateRange").on("click", function () {
            datePicker();
            jq("#Datepicker .ui-datepicker").show();
        });
    });

    function minDate() {
        return jq.datepicker.parseDate("dd-mm-yy", jq('#startDateRange').html())
    }

    function maxDate() {
        return jq.datepicker.parseDate("dd-mm-yy", jq('#stopDateRange').html())
    }

    function initializePatientsTable() {
        table = jq('#patientsTable').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'className': 'select-checkbox',
                    'checkboxes': {
                        'selectRow': true,
                        'select': true
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'dom': 'lBrtip',
            'buttons': [
                {
                    'extend': 'collection',
                    'text': 'More operations',
                    'className': 'btn btn-sm p-1 ',
                    'exportOptions': {
                        'modifier': {
                            'selected': true
                        }
                    },
                    'buttons': [
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        },
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        }
                    ],
                    'dropup': false
                }
            ],
            'searching': true,

            "pagingType": 'simple_numbers',

            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                }
            },
            'order': [[1, 'asc']]
        });
        jq('#patientsTable').DataTable.ext.search.push(function (settings, data, dataIndex) {
            let min = minDate();
            let max = maxDate();
            let date = jq.datepicker.parseDate("dd-mm-yy", data[6]);

            return (min === null && max === null) ||
                (min === null && date <= max) ||
                (min <= date && max === null) ||
                (min <= date && date <= max);

        });
        jq('#selectedRowsAlertDiv').hide();

        table.on('select', function (e, dt, type, indexes) {
            if (table.rows({selected: true}).count() === 1) {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " item selected");
                jq('#selectedRowsAlertDiv').show();
            } else {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                jq('#selectedRowsAlertDiv').show();
            }
        })
            .on('deselect', function (e, dt, type, indexes) {
                if (table.rows({selected: true}).count() === 0) {
                    jq('#selectedRowsAlertDiv').hide();
                } else {
                    jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                    jq('#selectedRowsAlertDiv').show();
                }

            });

        table.buttons().container().appendTo(jq('#other '));

        jq('.card-link').click(() => {
            if (jq('#genderDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });


        return table;
    }


    function actionRedirect(patientId) {
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patientId=" + patientId;
    }
</script>
<style>
    .dp-highlight .ui-state-default {
        background: #484 !important;
        color: #FFF !important;
    }

    .ui-datepicker.ui-datepicker-multi {
        width: 100% !important;
    }

    .ui-datepicker-multi .ui-datepicker-group {
        float: none;
    }

    #datepicker {
        height: 300px;
        overflow-x: scroll;
    }

    .overflow-container {
        position: relative;
    }

    .element1,
    .element2 {
        position: absolute;
    }

    .element1 {
        top: 0; /* Adjust as needed */
        left: 0; /* Adjust as needed */
        z-index: 100000;
    }

    .element2 {
        top: 0; /* Align to the top */
        right: 0; /* Align to the right */
    }

    h6 {
        color: #363463;
    }

    .ui-widget {
        font-size: 100%
    }
</style>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-horizontal form-group">
                        <label for="patientSearch">Search:</label>
                        <input type="text" class="form-control" id="patientSearch"
                               name="patientSearch" placeholder="Search by Patient ID, or name">
                    </div>

                    <div class="col pl-0 pr-0">
                        <div class="form-horizontal form-group collapse multi-collapse" id="genderDiv">
                            <label for="filterGender">Sex:</label>
                            <select class="custom-select form-control" id="filterGender">
                                <option selected disabled>Select Sex</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                        </div>
                        <div class="form-horizontal form-group">
                            <div class="">
                                <label for="searchDateRange">Range:</label>
                                <input type="text" class="form-control border" id="searchDateRange"
                                       name="searchDateRange" placeholder="Date Range"/>
                                <div class="element1" id="Datepicker"></div>
                            </div>
                            <label id="startDateRange" hidden></label>
                            <label id="stopDateRange" hidden></label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-horizontal form-group">
                        <label for="filterStatus">Status:</label>
                        <select class="custom-select form-control" id="filterStatus">
                            <option selected disabled>Select status:</option>
                            <option value="Active">Active</option>
                            <option value="Expired">Expired</option>
                        </select>
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse" id="dateDiv">
                        <label for="filterDateRegistered">Date:
                        </label>
                        <input type="text"
                               class="form-control datepicker"
                               id="filterDateRegistered"
                               name="filterDateRegistered"
                               placeholder="Select date registered"
                        />
                        <script type="text/javascript">
                            jq('#filterDateRegistered').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                "setDate": new Date(),
                                dateFormat: "dd-mm-yy",
                                yearRange: "-150:+0",
                                maxDate: 0,
                                "autoclose": true,
                                onSelect: function (data) {
                                    jq("#filterDateRegistered").trigger('change');
                                }
                            });
                        </script>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
                                ${ui.message("Search")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <button type="submit" id="resetBtn" class="btn btn-sm btn-dark bg-dark mb-3">
                                ${ui.message("Reset")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <a id="collapseBtn" class="card-link btn btn-sm btn-warning bg-white color-primary border-0"
                               data-target=".multi-collapse"
                               aria-expanded="false"
                               aria-controls="dateDiv genderDiv">
                                <span>${ui.message("Expand")}
                                    <i id="collapse-icon" class="icon-chevron-down"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <%
                        //TODO: Make this check configurable
                    %>
                    <% if (sessionLocation == "Registration Desk") { %>
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" onclick="redirectToNewRegistrationPage()"
                                    class="btn btn-sm btn-primary">
                                <i class="icon-plus"></i>   Register New Patient
                            </button>
                            <script type="text/javascript">
                                let redirectToNewRegistrationPage = () => {
                                    window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/startRegistration.page";
                                };
                            </script>
                        </div>

                        <div id="other" class="col-auto"></div>
                    </div>
                    <% } %>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div id="selectedRowsAlertDiv" class="alert alert-info color-info p-0" role="alert">
                                <label class="icon-info-sign color-primary" for="selectedRowsCount"></label>
                                <label id="selectedRowsCount"></label>
                            </div>

                            <div class="table-responsive">
                                <table id="patientsTable"
                                       class="table table-sm table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-left">Patient ID</th>
                                        <th class="text-left">Name</th>
                                        <th class="text-left">Sex</th>
                                        <th class="text-left">Date of birth</th>
                                        <th class="text-left">Age</th>
                                        <th class="text-left">Registered on</th>
                                        <th class="text-left">Status</th>
                                        <th class="text-left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="patientsDataTbody">
                                        <% pastRegisteredPatientsSummary.each { %>
                                        <tr>
                                            <td></td>
                                            <td>${it.identifier}</td>
                                            <td>${it.name}</td>
                                            <td>${it.gender}</td>
                                            <td>${it.dateOfBirth}</td>
                                            <td>${it.age}</td>
                                            <td>${it.registeredDate}</td>
                                            <td>${it.status}</td>
                                            <td><a class="color-primary" href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${it.patientId}">View profile</a></td>
                                        </tr>
                                        <% } %>
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
