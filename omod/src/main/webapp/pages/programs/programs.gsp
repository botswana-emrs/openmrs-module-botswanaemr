<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/' + 'botswanaemr' + '/' + '${pageLink}' + '.page'},
        {label: "<%=programName  %>"},
        {label: "Consultation", link: "${ui.pageLink('botswanaemr', 'consultation/consultation', [patientId: patient.uuid, visitId: currentVisit.visitId])}"},
        {label: "${ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName)))}, ${ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName)))}"}
    ];
</script>

<div class="col col-md-12">
    ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, visitId: currentVisit?.id, showHivStatus: true])}
</div>

<script>
    jq(function () {
        var currentVisit;
        jq("#quick-program-creation-dialog").dialog({
            autoOpen: false,
            width: 'auto',
            position: ['center', 150],
            modal: true,
        });
        jq("#quick-program-discontinuation-dialog").dialog({
            autoOpen: false,
            width: 'auto',
            position: ['center', 150],
            modal: true,
        });
        // next add the onclick handler
        jq("#enrollInProgram").click(function () {
            jq("#quick-program-creation-dialog").dialog("open");
            return false;
        });

        jq(".ui-dialog-titlebar").hide();

        jq(document).on("submit", "#discontinue-program-dialog-form", function (e) {

            e.preventDefault();
            jq('#discontinue-program-dialog-form button.confirm').prop('disabled', true);
            jq('#discontinue-program-dialog-form button.confirm').addClass('disabled');
            jq('#discontinue-program-dialog-form button.cancel').addClass('disabled');
            jq('#discontinue-program-dialog-form button.cancel').prop('disabled', true);
            jq('#discontinue-program-dialog-form button.cancel').addClass('disabled');

            var params = {
                'patient': jq('#patient-to-discontinue').val(),
                'discontinuationDate': jq('#discontinuation-date').val(),
                'programOutcome': jq('#program-disc-outcome').val(),
                'programDisco': jq('#program-disc').val()
            };

            jq.ajax({
                url: '${ ui.actionLink("botswanaemr", "programs/program", "post")}',
                type: 'POST',
                data: params,
                dataType: 'json'
            })
                .done(function (data) {
                    // Code to execute on success
                    jq().toastmessage('showNoticeToast', "Patient successfully discontinued");
                    location.reload();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    // Code to execute on failure
                    console.error("AJAX request failed:", textStatus, errorThrown);

                    // Additional error handling if needed

                    // Display an error message to the user, for example:
                    jq().toastmessage('showErrorToast', "Failed to discontinue patient. Please try again.");
                })
                .always(function () {
                    jq('#discontinue-program-dialog-form button.confirm').prop('disabled', false);
                    jq('#discontinue-program-dialog-form button.confirm').removeClass('disabled');
                    jq('#discontinue-program-dialog-form button.cancel').removeClass('disabled');
                    jq('#discontinue-program-dialog-form button.cancel').prop('disabled', false);
                    jq('#discontinue-program-dialog-form button.cancel').removeClass('disabled');
                });
        });
    });
</script>

<% if (patient) { %>
<% if (hasVisit) { %>
<script>
    jQuery(function () {
        jQuery("#beginProgramModal").appendTo("body");

    });
</script>

<div class="modal fade" id="beginProgramModal" tabindex="-1"
     data-controls-modal="beginProgramModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="beginProgramModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="beginProgramModalTitle">Program enrollment</h4>
                <button type="button" id="enroll" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <input type="hidden" id="patient" name="patient" value="${patient.id}"/>
                <i>Are you sure you want to enroll <b>${names}</b> into TB program now?</i><br/>
                <br/>
                <hr/>

                <div id="tasks">
                    <button class="confirm right" onclick="
                        location.href = '${ ui.pageLink('botswanaemr', 'enterForm', [
                                      patientId: patient.uuid,
                                      formUuid: '2d21632a-9dac-11ec-9408-4fbe33f081cf',
                                      visitId: currentVisit.uuid,
                                      returnUrl: tbProfileUrl
                                ])}'">
                        ${ui.message("Fill TB enrollment form")}
                        <i class="icon-spinner icon-spin icon-2x" style="display: none; margin-left: 10px;"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#endProgramModal").appendTo("body");
    });
</script>

<div class="modal fade" id="endProgramModal" tabindex="-1"
     data-controls-modal="endProgramModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="endProgramModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="endProgramModalTitle">Program Discontinuation</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="dialog-content form">
                    <form id="discontinue-program-dialog-form" method="post1"
                          action="${ui.actionLink('botswanaemr', 'programs/program', 'post')}">
                        <input type="hidden" id="patient-to-discontinue" name="patient" value="${patient.id}"/>
                        <i>Are you sure you want to discontinue <b>${names}</b> from program now?</i><br/><br/>
                        Active programs are
                        <hr/>
                        <ul>
                            <% activePrograms.each { %>
                            <li>${it.program.name}-Enrolled on-${it?.dateEnrolled}-at-${it?.location?.name}</li>
                            <% } %>
                        </ul>
                        <hr/>
                        Discontinuation Date:
                        <input type="text" class="form-control datepicker" required id="discontinuation-date"
                               name="discontinuationDate" placeholder="yyyy-mm-dd"/> <br/>
                        <script type="text/javascript">
                            jq('#discontinuation-date').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                "setDate": new Date(),
                                dateFormat: "yy-mm-dd",
                                yearRange: "-150:+0",
                                maxDate: 0,
                                "autoclose": true,
                                onSelect: function (data) {
                                    jq("#discontinuation-date").trigger('change');
                                }
                            });
                        </script>
                        <select id="program-disc" name="programDisco" required>
                            <option class="dialog-drop-down small">${ui.message("Select program")}</option>
                            <% activePrograms.each { %>
                            <option class="dialog-drop-down small"
                                    value="${it.program.programId}">${ui.format(it.program.name)}</option>
                            <% } %>
                        </select>
                        <br/>
                        Outcome
                        <select id="program-disc-outcome" name="programOutcome" required>
                            <option class="dialog-drop-down small">${ui.message("Select outcome")}</option>
                            <% programOutComes.each { key, value -> %>
                            <option class="dialog-drop-down small" value="${key.conceptId}">${ui.format(value)}</option>
                            <% } %>
                        </select>
                        <br/>
                        <hr/>
                        <button class="cancel" data-dismiss="modal">${ui.message("Close")}</button>
                        <button class="confirm right" type="submit" id="discontinue">${ui.message("Discontinue")}
                            <i class="icon-spinner icon-spin icon-2x" style="display: none; margin-left: 10px;"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
<% } %>

${ui.includeFragment("botswanaemr", programPath, [patientId: patient.uuid, visitId: currentVisit?.id])}

