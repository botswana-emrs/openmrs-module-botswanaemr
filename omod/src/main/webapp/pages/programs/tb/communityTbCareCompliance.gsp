<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "TB Program",
            link: '${ui.pageLink("botswanaemr", "/programs/programs", [patientId: patient.uuid])}'
        },
        {label: "Community TB Care Treatment Compliance"}
    ];

    const printOrExportTbSummaryPdf = (summary) => {
        const contents = document.getElementById(summary).innerHTML;

        const originalContents = document.body.innerHTML;
        document.body.innerHTML = contents;

        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<div id="hidden-element" style="margin: auto; padding: 10px;">
    <button class="primary btn-success" onclick="printOrExportTbSummaryPdf('tbSummaryExport');"
            value="tbTreatmentSummary">Export/Print PDF</button>
</div>

<div id="tbSummaryExport">
    <div style="margin: 4px; padding: 10px;">
        <div style="border: #0099CC solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">COMMUNITY TB CARE TREATMENT COMPLIANCE</h5>
        </div>
    </div>

    <div class="row">
        <div class="col col-sm-12 col-md-12 col-lg-12">
            ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, showHivStatus: true])}
        </div>
    </div>

    <div style="margin: 10px; padding: 10px;">
        <div style="border: #2C3E50 solid 2px; margin:10px; padding:10px;">
            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">
                <div class="col-sm-12">
                    <div class="fullwidth">
                        <table id="cultureDstReport" class="table table-sm table-bordered" style="text-align: start">
                            <thead>
                            <tr>
                                <th>ID Number</th>
                                <th>Date of Registration on CTBC</th>
                                <th>Patient's Name</th>
                                <th>Age</th>
                                <th>Sex</th>
                                <th>CTBC Model</th>
                                <th>Physical Address</th>
                                <th>Next of kin</th>
                                <th>Supporter's name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>${IdNumber}</td>
                                <td></td>
                                <td>${patient.getPersonName()?.getFullName()}</td>
                                <td>${patient.getAge()}</td>
                                <td>${patient.getGender()}</td>
                                <td></td>
                                <td>${address}</td>
                                <td>${nokName}</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin: 10px; padding: 10px;">
        <div style="border: #0099CC solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">INITIAL PHASE</h5>
        </div>

        <div style="border: #2C3E50 solid 2px; margin: 4px; padding: 10px;">
            <div class="row" style="border: #2C3E50 solid 1px; margin: 10px; padding: 10px;">
                <div class="col-sm-12">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/dot", [patientId: patient])}
                    </div>
                </div>
            </div>
        </div>

        <div style="border: #0099CC solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">CONTINUATION PHASE</h5>
        </div>

        <div style="border: #2C3E50 solid 2px; margin: 4px; padding: 10px;">
            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">
                <div class="col-sm-12">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/continuationDot", [patientId: patient])}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
