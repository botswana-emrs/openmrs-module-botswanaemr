<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    //ui.includeJavascript("botswanaemr", "jspdf.min.js")
    //ui.includeJavascript("botswanaemr", "pdfmake.min.js")
%>

<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "TB Program",
            link: '${ui.pageLink("botswanaemr", "/programs/programs", [patientId: patient.uuid])}'
        },
        {label: "TB Treatment Summary"}
    ];

    const printOrExportTbSummaryPdf = (summary) => {
        const contents = document.getElementById(summary);

        const clonedContent = contents.cloneNode(true);

        // Remove the '.card' class from all elements that have it to avoid print conflicts
        clonedContent.querySelectorAll('.card').forEach((element) => {
           element.classList.remove('card');
        });

        const originalContents = document.body.innerHTML;

        document.body.innerHTML = clonedContent.innerHTML;
        window.print();

        document.body.innerHTML = originalContents;
    }
</script>

<div id="hidden-element" style="margin: auto; padding: 10px;">
    <button class="primary btn-success" onclick="printOrExportTbSummaryPdf('tbSummaryExport');" value="tbTreatmentSummary">Export/Print PDF</button>
</div>

<div id="tbSummaryExport">
    <div style="margin: 4px; padding: 10px;">
        <div style="border: #2C3E50 solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">TB TREATMENT SUMMARY</h5>
        </div>
    </div>

    <div class="row">
        <div class="col col-sm-12 col-md-12 col-lg-12">
            ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, showHivStatus: true])}
        </div>
    </div>

    <div style="margin: 10px; padding: 10px;">
        <div style="border: #2C3E50 solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">INITIAL PHASE</h5>
        </div>

        <div style="border: #2C3E50 solid 2px; margin: 4px; padding: 10px;">

            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">
                <div class="col-sm-6">
                    <div class="fullwidth">
                        <table class="table table-sm table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Patient Category</th>
                                <th>Treatment Group</th>
                                <th>Disease Classification</th>
                                <th>Regimen</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td rowspan="2">${tbEnrollmentDetails.PatientCategory ? tbEnrollmentDetails.PatientCategory : ''}</td>
                                <td rowspan="2">${tbEnrollmentDetails.TreatmentGroup ? tbEnrollmentDetails.TreatmentGroup : ''}</td>
                                <td rowspan="2">${tbEnrollmentDetails.TreatmentClassification ? tbEnrollmentDetails.TreatmentClassification : ''}</td>
                                <td rowspan="2">${regimen ? regimen.regimenName : ''}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--
            <div>
                <table>
                    <tr>
                        <th>Regimen</th>
                        <th>Regimen</th>
                    </tr>
                </table>
            </div>
-->
                <div class="col-sm-3">
                    <div class="fullwidth">
                        <table class="table table-sm table-bordered table-responsive" style="text-align: start">
                            <thead>
                            <tr>
                                <th colspan="2">Community TB Care</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>No. Screened</td>
                                <td>No. Expected</td>
                            </tr>
                            <tr>
                                <td>${tbEnrollmentDetails.noOfScreened ? tbEnrollmentDetails.noOfScreened : ''}</td>
                                <td>${tbEnrollmentDetails.noOfExpected ? tbEnrollmentDetails.noOfExpected : ''}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="fullwidth">
                        <table class="table table-sm table-bordered table-responsive" style="text-align: start">
                            <thead>
                            <tr>
                                <th colspan="2">Contact Screening</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Counselled</td>
                                <td>Enrolled</td>
                            </tr>
                            <tr>
                                <td>${tbEnrollmentDetails.counselled ? tbEnrollmentDetails.counselled : ''}</td>
                                <td>${tbEnrollmentDetails.enrolled ? tbEnrollmentDetails.enrolled : ''}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="border: #2C3E50 solid 1px; margin:10px; padding: 10px;">
                <div class="col-sm-8 col-md-8">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/tbLabResults", [patientId: patient])}
                    </div>
                </div>

                <div class="col-sm-4 col-md-4">
                    <div class="fullwidth">
                        <table class="table table-sm table-bordered table-responsive" style="text-align: start">
                            <thead>
                            <tr>
                                <th colspan="2">HIV Status and Interventions</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>HIV Test 1</td>
                                <td>${tbEnrollmentDetails.hivStatusTest ? tbEnrollmentDetails.hivStatusTest : ''}</td>
                                <td>${tbEnrollmentDetails.hivStatusTestDate ? tbEnrollmentDetails.hivStatusTestDate : ''}</td>
                            </tr>
                            <tr>
                                <td>ART</td>
                                <td>${tbEnrollmentDetails.ART ? tbEnrollmentDetails.ART : ''}</td>
                                <td>${tbEnrollmentDetails.ARTDate ? tbEnrollmentDetails.ARTDate : ''}</td>
                            </tr>
                            <tr>
                                <td>CPT</td>
                                <td>${tbEnrollmentDetails.CPT ? tbEnrollmentDetails.CPT : ''}</td>
                                <td>${tbEnrollmentDetails.CPTDate ? tbEnrollmentDetails.CPTDate : ''}</td>
                            </tr>
                            <tr>
                                <td>TPT</td>
                                <td>${tbEnrollmentDetails.TPT ? tbEnrollmentDetails.TPT : ''}</td>
                                <td>${tbEnrollmentDetails.TPTDate ? tbEnrollmentDetails.TPTDate : ''}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">

                <div class="col-sm">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/diabetesMellitus", [patientId: patient])}
                    </div>
                </div>

                <div class="col-sm">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "weightTracking", [patientId: patient])}
                    </div>
                </div>
            </div>

            <div class="row" style="border: #2C3E50 solid 1px; margin: 10px; padding: 10px;">
                <div class="col-sm-12">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/dot", [patientId: patient])}
                    </div>
                </div>
            </div>
        </div>

        <div style="border: #2C3E50 solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white">CONTINUATION PHASE</h5>
        </div>

        <div style="border: #2C3E50 solid 2px; margin: 4px; padding: 10px;">

            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">
                <div class="col-sm-7">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/chestXRayResult", [patientId: patient])}
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/cultureDstReport", [patientId: patient])}
                    </div>
                </div>
            </div>

            <div class="row" style="border: #2C3E50 solid 1px; margin:10px; padding: 10px;">
                <div class="col-sm-5">
                    <div class="fullwidth">
                        <table class="table table-sm table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th class="text-start">Treatment Outcomes</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-start">${regimen.treatmentProgram.outcome != null ? regimen.treatmentProgram.outcome.concept.name.name : 'On Treatment'}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-sm-7">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/adverseDrugReaction", [patientId: patient])}
                    </div>
                </div>
            </div>

            <div class="row" style="border: #2C3E50 solid 1px; padding: 10px;">
                <div class="col-sm-12">
                    <div class="fullwidth">
                        ${ui.includeFragment("botswanaemr", "programs/tb/continuationDot", [patientId: patient])}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
