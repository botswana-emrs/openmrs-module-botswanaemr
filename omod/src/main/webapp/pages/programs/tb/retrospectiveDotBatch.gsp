<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    //add FDC quantity formula
    ui.includeJavascript("botswanaemr", "utils.js")
    ui.includeJavascript("botswanaemr", "fdc-quantity-formula.js")
%>

<script type="text/javascript">
    const breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "TB Program",
            link: '${ui.pageLink("botswanaemr", "/programs/tb", [patientId: patient.uuid])}'
        },
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "Community TB care Compliance Batch Updates",
            link: '${ui.pageLink("botswanaemr", "/programs/tb/retrospectiveDotBatch", [patientId: patient.uuid])}'
        }
    ];

    const DOT_STORE_KEY = "dot-batch-updates-store";

    const dotBatchUpdatesStore = () => {
        return JSON.parse(window.localStorage.getItem(DOT_STORE_KEY));
    }

    const addToDotStore = (value) => {
        let batchUpdates = dotBatchUpdatesStore();
        if (batchUpdates) {
            batchUpdates.push(value);
        } else {
            batchUpdates = [value];
        }
        window.localStorage.setItem(DOT_STORE_KEY, JSON.stringify(batchUpdates));
    }

    function getDatesInRange(dotStartDate, dotEndDate) {
        let dotDate = new Date(dotStartDate);
        let dotDates = [];
        while (dotDate <= dotEndDate) {
            dotDates.push(new Date(dotDate));
            dotDate.setDate(dotDate.getDate() + 1);
        }
        return dotDates;
    }

    function padTo2Digits(num) {
        return num.toString().padStart(2, '0');
    }

    function formatDate(date) {
        return [
            date.getFullYear(),
            padTo2Digits(date.getMonth() + 1),
            padTo2Digits(date.getDate()),
        ].join('-');
    }

    function getValidStartDate(){
        var maxDate = new Date();
        maxDate.setDate(maxDate.getDate());
        jQuery("#date").attr({"max" : formatDate(maxDate) });
    }

    function getValidEndDate(){
        var maxDate = new Date();
        maxDate.setDate(maxDate.getDate());
        jQuery("#endDate").attr({"max" : formatDate(maxDate) });
    }

    function removeFromBatchUpdatesStoreByItem(value) {
        const arr = value.split("#");

        const batchUpdateStore = getDotBatchUpdatesStore();
        const tempStore = getDotBatchUpdatesStore();
        
        if (batchUpdateStore) {
            batchUpdateStore.forEach((item) => {
                if (item.drug === arr[0] && item.date === arr[1]) {
                    tempStore.splice(tempStore.indexOf(item), 1);
                }
            });

            window.localStorage.setItem(DOT_STORE_KEY, JSON.stringify(tempStore));
        }
    }

    function getDotBatchUpdatesStore() {
        return dotBatchUpdatesStore();
    }

    function clearOrderBatchUpdateStore() {
        window.localStorage.removeItem(DOT_STORE_KEY);
    }

    function removeFromBatchUpdateStore(value) {
        const batchUpdateStore = dotBatchUpdatesStore();
        if (batchUpdateStore) {
            const index = batchUpdateStore.indexOf(value);
            if (index > -1) {
                batchUpdateStore.splice(index, 1);
            }
        }
        window.localStorage.setItem(DOT_STORE_KEY, JSON.stringify(batchUpdateStore));
    }

    function removeDotItem(item) {
        removeFromBatchUpdateStore(item);
    }

    jQuery(document).ready(function () {
        getValidStartDate();
        getValidEndDate();
        
        let initialDrugs = [
            'Rifampicin 75 mg/Isoniazid 50mg/Pyrazinamide 150 mg (paediatric)',
            'Rifampicin 150mg/Isoniazid 75mg/Pyrazinamide 400mg/Ethambutol 275mg'];

        let continuationDrugs = [
            'Rifampicin 150 mg/Isoniazid 75 mg/ Ethambutol 275 mg',
            'Rifampicin 75mg/Isoniazid 50mg (paediatric)'];

        let adultDrugs = [
            'Rifampicin 150mg/Isoniazid 75mg/Pyrazinamide 400mg/Ethambutol 275mg',
            'Rifampicin 150 mg/Isoniazid 75 mg/ Ethambutol 275 mg'];

        let childDrugs = [
            'Rifampicin 75 mg/Isoniazid 50mg/Pyrazinamide 150 mg (paediatric)',
            'Rifampicin 75mg/Isoniazid 50mg (paediatric)'];

        let initialAdultDrugs = initialDrugs.filter(function (drug) {
            return adultDrugs.includes(drug);
        });

        let initialChildDrugs = initialDrugs.filter(function (drug) {
            return childDrugs.includes(drug);
        });

        let continuationAdultDrugs = continuationDrugs.filter(function (drug) {
            return adultDrugs.includes(drug);
        });

        let continuationChildDrugs = continuationDrugs.filter(function (drug) {
            return childDrugs.includes(drug);
        });

        const lastWeight = '${lastWeight}';
        let weight = '${lastWeight}';
        const age = '${patientAge}';

        const drugField = jQuery('#drug');
        const weightField = jQuery('#weight');

        //default to patient last weight
        weightField.val(lastWeight);

        const getSelectedTreatmentType = () => {
            return jQuery('#treatmentType').find('option:selected').val();
        }

        function getActiveDrugOrders() {
            jq.getJSON('${ ui.actionLink("botswanaemr", "search", "getActiveDrugOrdersByPatientAndDrugConcept")}', {
                'patientId': ${patient.id},
                'drugConceptUuid':  drugField.find('option:selected').val()
            }).success(function (data) {
                // Check if data contains any items
                if (data.length > 0) {
                    let display = "";

                    // Iterate over each item in the data
                    data.forEach(function (item) {
                        display += "<br/><strong>Date: </strong>" + item.date + " </br> " +
                                   "<strong>Ordered by: </strong>" + item.orderer + " </br>";
                    });

                    jq("#pending-lab-order").html(display);
                    //allow form submission
                    jq('#btn-process-batch-dot').show();
                } else {
                    // No active drug orders found
                    jq("#pending-lab-order").html("<br/><i class='fa fa-circle text-warning'></i><span class='text-muted px-0' style='background: none;'> No pending prescriptions for this drug, add a prescription first</span>");
                    //prevent form submission
                    jq('#btn-process-batch-dot').hide();
                }
            }).error(function (data) {
                jq().toastmessage('showErrorToast', "An error occurred while submitting DOT batch updates");
            });
        }

        drugField.change(function () {
           getActiveDrugOrders();
        });

        let treatmentDrugField = jq("#drug");

        function enableAppropriateDrug() {
            if (weightField.val() !== '' && !isNaN(weightField.val())) {
                weight = weightField.val();
            }
            let treatmentType = getSelectedTreatmentType();
            let selectedTreatmentPhase = '';
            if (treatmentType === INITIAL_TB_TREATMENT_PHASE) {
                selectedTreatmentPhase = 'Initial';
            } else if (treatmentType === CONTINUATION_TREATMENT_PHASE) {
                selectedTreatmentType = 'Continuation';
            }

            tbUtils.chooseFdcDrugBasedOnAgeAndWeight(treatmentDrugField, selectedTreatmentPhase, age, weight);
        }

        const ETHAMBUTOL_DRUG_UUID = 'd12de6ea-20dd-4f33-92a4-5fc0acd66825';
        const INITIAL_TB_TREATMENT_PHASE = '159795AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
        const CONTINUATION_TREATMENT_PHASE = '159794AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';

        const calculateQuantity = () => {
            let selectWeight = weightField.val();
            if (!selectWeight) {
                selectWeight = weight;
            }

            if (getSelectedTreatmentType().trim() === INITIAL_TB_TREATMENT_PHASE
                && getSelectedDrug() === ETHAMBUTOL_DRUG_UUID) {
                if (age < 14) {
                    const quantity = calculateQuantityBasedOnWeight(selectWeight);
                    jQuery('#quantity').val(quantity);
                }
            } else {
                const quantity = calculateQuantityBasedOnWeightAndAge(selectWeight, age);
                jQuery('#quantity').val(quantity);
            }
        }

        window.onload = function () {
            const batchUpdates = getDotBatchUpdatesStore();

            if (batchUpdates) {
                const listPrefix = "<ol style='margin: 4px;'>";
                const listSuffix = "</ol>";
                let listContent = '';

                batchUpdates.forEach((dot) => {
                    let removeItemBtn = document.createElement("button");
                    removeItemBtn.id = dot.drug + '#' + dot.date;
                    removeItemBtn.innerHTML = '<i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>';
                    removeItemBtn.className = 'btn btn-sm btn-danger';
                    listContent += '<li><div class="row"><div class="col-md-10"><p>' + dot.display + '</p></div><div class="col-md-2">' + removeItemBtn.outerHTML + '</div></div></li>';
                })

                jQuery("#dot-batch-updates").append(listPrefix + listContent + listSuffix);

            } else {
                const emptyState = '<div class="empty-state" style="margin: 12px;">' +
                    '<div class="empty-state-icon">' +
                    '<i class="fas fa-shopping-cart"></i>' +
                    '</div>' +
                    '<div class="empty-state-title">' +
                    '<h4>No DOT batch updates available.</h4>' +
                    '</div>' +
                    '<div class="empty-state-message">' +
                    '<p>Fill out Community TB care Compliance Form</p>' +
                    '</div>' +
                    '</div>';
                jQuery("#dot-batch-updates").append(emptyState);
            }
        }

        jQuery('#btn-save-batch-dot').click((event) => {
            event.preventDefault();
        });

        jQuery('#dot-batch-updates').on('click', 'button', function() {
            const idString = jQuery(this).attr('id');
            removeFromBatchUpdatesStoreByItem(idString);
            location.reload();
        })

        jQuery('#specify-div').hide();

        jQuery('#administerBy').change(function () {
            const val = jQuery(this).val();
            if (val === '1574AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jQuery('#provider-div').show();
                jQuery('#provider').val('${currentProvider.uuid}')
            } else {
                jQuery('#provider-div').hide();
            }

            if (val === '5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' || val === '160639AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' || val === '1555AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jQuery('#specify-div').show();
            } else {
                jQuery('#specify-div').hide();
            }
        });

        jQuery('#treatmentType').change(function () {
            // const selectedTreatmentType = jQuery(this).val();
            // if (selectedTreatmentType === '159795AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
            //     drugField.find('option').each(function() {
            //         console.log("xx", jq(this).text().trim())
            //     });
            //}
            drugField.val('');
            enableAppropriateDrug();
            calculateQuantity();
        });

        const getSelectedDrug = () => {
            return drugField.find('option:selected').val();
        }

        weightField.change(function () {
            enableAppropriateDrug();
            calculateQuantity();
        });

        jQuery("#btn-back").click(function (event) {
            event.preventDefault();
            history.back();
        });

        jQuery('#btn-add-dot').click(function () {
            const treatmentType = jQuery('#treatmentType').find('option:selected').val();
            const drug = drugField.find('option:selected').val();
            const provider = jQuery('#provider').find('option:selected').val();
            const administer = jQuery('#administerBy').find('option:selected').val();
            const date = jQuery('#date').val();
            const endDate = jQuery('#endDate').val();
            const quantity = jQuery('#quantity').val();
            const remarks = jQuery('#remarks').val();
            const weight = weightField.val();
            const facility = jQuery('#facilityLocationField').find('option:selected').text();

            let errors = [];
            if (treatmentType === '') {
                errors.push("treatment phase")
            }
            if (drug === '') {
                errors.push("drug")
            }

            if (administer === '') {
                errors.push("administer by")
            }

            if (weight === '') {
                errors.push("weight")
            }

            if (quantity === '') {
                errors.push("quantity");
            }

            if (date === '') {
                errors.push("Date");
            }

            if (endDate === '') {
                errors.push("EndDate");
            }

            if (date > endDate) {
                errors.push("DateMismatch");
            }

            if (errors.length > 0) {
                let errorMessage = '';
                if(jQuery.inArray("DateMismatch", errors) !== -1) {
                    errorMessage = "End date cannot be before Start date ! ";
                } else if (errors.length === 0) {
                    errorMessage = "The field " + errors[0] + " is required";
                } else {
                    errorMessage = "The fields "
                    errors.forEach(function (msg) {
                        errorMessage += msg + ','
                    });
                    errorMessage += " are required!"
                }
                
                jq().toastmessage('showErrorToast', errorMessage);

            } else {
                let selectedAdministerBy = jQuery('#administerBy').find('option:selected').val();
                let providerDisplay;

                 if (selectedAdministerBy == '5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                    providerDisplay = jQuery('#specify').val();
                 } else {
                    providerDisplay = jQuery('#provider').find('option:selected').text();
                 }

                if (providerDisplay === 'select provider') {
                    providerDisplay = "Missed"
                }

                let treatmentTypeDisplay = '';
                
                if (getSelectedTreatmentType().trim() === '159795AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                    treatmentTypeDisplay = "Initial phase";
                } else {
                    treatmentTypeDisplay = "Continuation phase";
                }

                var sDate = new Date(date);
                var eDate = new Date(endDate);
                const allDotDates = getDatesInRange(sDate, eDate);
                let display = "";
                let payload = {};

                if (allDotDates.length > 1) { 
                    allDotDates.forEach(dDate => {
                        display = "<strong>" + formatDate(new Date(dDate)) + "</strong> - " + "</strong> - " + drugField.find('option:selected').text()
                                + " </br> <strong>Administered by: </strong>" + providerDisplay
                                + "</br> <strong>TreatmentType: </strong>" + treatmentTypeDisplay
                                + "</br> <strong>Quantity administered: </strong>" + quantity
                                + "</br><strong>Weight: </strong>" + weight
                                + "</br><strong>Facility: </strong>" + facility
                                + "</br><strong>Remarks: </strong>" + remarks;

                        payload = {
                            treatmentType: treatmentType,
                            drug: drug,
                            administerBy: administer,
                            date: formatDate(dDate),
                            endDate: formatDate(dDate),
                            remarks: remarks,
                            weight: weight,
                            quantity: quantity,
                            provider: provider,
                            display: display,
                            facility: facility
                        }

                        addToDotStore(payload);
                    });
                    
                } else {

                    display = "<strong>" + formatDate(new Date(date)) + "</strong> - " + "</strong> - " + drugField.find('option:selected').text()
                        + " </br> <strong>Administered by: </strong>" + providerDisplay
                        + "</br> <strong>TreatmentType: </strong>" + treatmentTypeDisplay
                        + "</br> <strong>Quantity administered: </strong>" + quantity
                        + "</br><strong>Weight: </strong>" + weight
                        + "</br><strong>Facility: </strong>" + facility
                        + "</br><strong>Remarks: </strong>" + remarks;

                    payload = {
                        treatmentType: treatmentType,
                        drug: drug,
                        administerBy: administer,
                        date: date,
                        endDate: endDate,
                        remarks: remarks,
                        weight: weight,
                        quantity: quantity,
                        provider: provider,
                        display: display,
                        facility: facility
                    }
                    
                    addToDotStore(payload);
                }
                window.location.reload();
            }
        });

        jQuery('#btn-clear-all').click(function () {
            clearOrderBatchUpdateStore();
            jQuery("#dot-batch-updates").empty();
            location.reload();
        });

        jQuery('#btn-process-batch-dot').click(function (event) {
            event.preventDefault();
            const data = getDotBatchUpdatesStore();
            if (data && data.length > 0) {
                const payload = [];
                data.forEach((item) => {
                    payload.push (
                        {
                            treatmentType: item.treatmentType,
                            drug: item.drug,
                            administerBy: item.administerBy,
                            date: item.date,
                            endDate: item.endDate,
                            remarks: item.remarks,
                            weight: item.weight,
                            quantity: item.quantity,
                            facility: item.facility,
                            provider: item.provider
                        }
                    )
                })

                jq.getJSON('${ ui.actionLink("botswanaemr", "programs/tb/dotBatchUpdates", "dotBatchUpdate")}', {
                    'patientId': ${patient.id},
                    'data': JSON.stringify(payload)
                }).success(function (data) {
                    jq().toastmessage('showNoticeToast', "DOT batch updates have been submitted successfully");
                    clearOrderBatchUpdateStore();

                    // return to patient consultation page
                    // let returnUrl = "${ui.pageLink("botswanaemr", "consultation/consultation", [patientId: patient.id, visitId: patient])}";
                    // window.location.href = returnUrl;
                    urlUtils.redirectToReturnUrl();
                }).error(function (data) {
                    jq().toastmessage('showErrorToast', "An error occurred while submitting DOT batch updates");
                });
            }
        });
    });
</script>

<style>
.form-group.required .control-label:after {
    content: "*";
    color: red;
}
</style>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, showHivStatus: true])}
    </div>
</div>

<div class="row">
    <div class="col-9">
        <p>Community TB care Compliance Batch Updates</p>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary m-b-16">
                <h4 class="card-title text-white">Community TB care Compliance Form</h4>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="date">Start Date</label>
                            <input type="date" id="date" name="date" required="required" data-provide="datepicker" data-date-end-date="0d"
                                   class="form-control input-lg" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="endDate">End Date</label>
                            <input type="date" id="endDate" name="endDate" required="required" data-provide="datepicker" data-date-end-date="0d"
                                   class="form-control input-lg" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required">
                            <label class="control-label" for="treatmentType">Treatment phase</label>
                            <select name="treatmentType" id="treatmentType" class="form-control input-lg">
                                <option value="">select treatment phase</option>
                                <% treatmentTypes.each { %>
                                <option value="${it.getUuid()}">
                                    ${it.getDisplayString()}
                                </option>
                                <% } %>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label" for="drug">Drug</label>
                            <select name="drug" id="drug" class="form-control input-lg">
                                <option value="">select drug</option>
                                <% drugs.each { %>
                                <option value="${it.getUuid()}">
                                    ${it.getDisplayString()}
                                </option>
                                <% } %>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group required">
                            <label class="control-label" for="weight">Weight</label>
                            <input id="weight" name="weight" type="number" placeholder=""
                                   class="form-control input-lg">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group required">
                            <label class="control-label" for="quantity">Quantity</label>
                            <input id="quantity" name="quantity" type="number" placeholder=""
                                   class="form-control input-lg">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group required">
                            <label class="control-label" for="administerBy">Administer By</label>
                            <select name="administerBy" id="administerBy" class="form-control input-lg">
                                <option value="">select administer by</option>
                                <% administers.each { %>
                                <option value="${it.getUuid()}">
                                    ${it.getDisplayString()}
                                </option>
                                <% } %>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div id="provider-div" class="form-group required">
                            <label class="control-label" for="provider">Provider</label>
                            <select name="provider" id="provider" class="form-control input-lg">
                                <option value="">select provider</option>
                                <% providers.each { %>
                                <option value="${it.getUuid()}">
                                    ${it.getName()}
                                </option>
                                <% } %>
                            </select>
                        </div>
                        <div id="specify-div" class="form-group required">
                            <label class="control-label" for="specify">Specify</label>
                            <input id="specify" name="specify" type="text" placeholder=""
                                class="form-control input-lg">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div id="facility-div"  class="form-group">
                            ${ ui.includeFragment("botswanaemr", "widget/facilityLocation") }
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="remarks">Remarks</label>
                            <textarea id="remarks" name="remarks" class="form-control"
                                      rows="3"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row card-footer p-3">
                    <div class="col-md-6 pl-0">
                        <button id="btn-reset" type="button" class="btn btn-dark bg-dark">Discard</button>
                    </div>

                    <div class="col-md-6 pr-0">
                        <button id="btn-add-dot" type="button" class="btn btn-primary float-right">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-primary">
                <h4 class="card-title text-white">Drug Orders</h4>
            </div>

            <div class="card-body">
                <div class="row">
                    <div id="pending-lab-order">
                        <br/>
                        <i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Select drug first!</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header bg-primary">
                <h4 class="card-title text-white">DOT Batch Updates</h4>
            </div>

            <div class="card-body">
                <div class="row">
                    <div id="dot-batch-updates">
                    </div>
                </div>
            </div>

            <div class="row card-footer mt-4 p-1">
                <div class="col-md-6 pl-0">
                    <button id="btn-clear-all" type="button" class="btn btn-dark bg-dark">Clear All</button>
                </div>

                <div class="col-md-6 pr-0">
                    <button id="btn-process-batch-dot" type="button" class="btn btn-primary float-right">Process DOT Updates</button>
                </div>
            </div>
        </div>
    </div>
</div>
