<%
    ui.includeFragment("appui", "standardEmrIncludes")
    ui.includeCss("appui", "bootstrap.min.css")
    ui.includeCss("botswanaemr", "font-awesome.min.css")
    ui.includeCss("botswanaemr", "sidebar.css")
    ui.includeCss("botswanaemr", "helper.css")
    ui.includeCss("botswanaemr", "style.css")

    ui.includeJavascript("appui", "jquery-3.4.1.min.js")
    ui.includeJavascript("appui", "bootstrap.min.js")
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${ui.message("referenceapplication.login.title")}</title>
        <link rel="shortcut icon" type="image/ico" href="/${ui.contextPath()}/images/openmrs-favicon.ico"/>
        <link rel="icon" type="image/png" href="/${ui.contextPath()}/images/openmrs-favicon.png"/>
        ${ui.resourceLinks()}
    </head>

    <body>
        <script type="text/javascript">
            var OPENMRS_CONTEXT_PATH = '${ ui.contextPath() }';
        </script>

        <script type="text/javascript">
            jQuery(function() {
                updateSelectedOption = function() {
                    jQuery('#sessionLocation li').removeClass('selected');
                    var sessionLocationVal = jQuery('#sessionLocationInput').val();
                };
                updateSelectedOption();
                jQuery('#sessionLocation li').click(function() {
                    jQuery('#sessionLocationInput').val(jQuery(this).attr("value"));
                    updateSelectedOption();
                });
                jQuery('#username').focus();

                var cannotLoginController = emr.setupConfirmationDialog({
                    selector: '#cannotLoginPopup',
                    actions: {
                        confirm: function() {
                            cannotLoginController.close();
                        }
                    }
                });
                jQuery('a#cantLogin').click(function() {
                    cannotLoginController.show();
                });
                jQuery('#closeBtn').click(function() {
                    jQuery('#cannotLoginPopup').hide();
                });
                pageReady = true;

                jQuery("#togglePassword").click(function(event) {
                    event.preventDefault();
                    togglePasswordVisibility();
                });

                function togglePasswordVisibility() {
                    var passwordField = document.getElementById("password");
                    var toggleIcon = document.getElementById("togglePassword");
                    if (passwordField.type === "password") {
                        passwordField.type = "text";
                        toggleIcon.classList.remove("fa-eye");
                        toggleIcon.classList.add("fa-eye-slash");
                    } else {
                        passwordField.type = "password";
                        toggleIcon.classList.remove("fa-eye-slash");
                        toggleIcon.classList.add("fa-eye");
                    }
                }
            });
        </script>

        <div class="container px-1 shadow-sm mb-1 bg-white rounded">
            <div class="row">
                <div class="col text-center justify-content-center">
                    <img src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}"
                         class="img-thumbnail rounded mx-auto d-block mt-2"
                         alt=""/>

                    <h4 class="text-center lead">BotswanaEMR - Hospital Services</h4>

                    <form id="login-form" class="login-form" method="post" autocomplete="off">
                        ${ui.includeFragment("referenceapplication", "infoAndErrorMessages")}
                        <div class="form-group">
                            <label for="username">
                                ${ui.message("referenceapplication.login.username")}
                            </label>

                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="icon-user" style="font-size: smaller;"></i>
                                    </span>
                                </div>
                                <input id="username"
                                       type="text"
                                       name="username"
                                       class="form-control icon-user"
                                       placeholder="${ui.message('referenceapplication.login.username.placeholder')}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password">
                                ${ui.message("referenceapplication.login.password")}:
                            </label>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="icon-key" style="font-size: small;"></i>
                                    </span>
                                </div>
                                <input id="password" 
                                    type="password" 
                                    name="password" 
                                    class="form-control icon-key" 
                                    placeholder="${ui.message('referenceapplication.login.password.placeholder')}"/>
                                <div class="input-group-append">
                                    <a href="javascript:void(0);" class="form-control input-group-text" id="togglePasswordLink">
                                        <i class="fa fa-eye" style="font-size: small;" id="togglePassword" style="cursor: pointer;" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col text-left pl-0">
                                <a id="cantLogin" class="text-primary" href="javascript:void(0)">
                                    <i class="icon-question-sign small"></i>
                                    <small class="text-primary">
                                        ${ui.message("Forgot password?")}
                                    </small>
                                </a>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit"
                                    class="btn btn-primary btn-block">
                                    ${ui.message("Sign in")}
                            </button>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="redirectUrl" value="${redirectUrl}"/>
                        </div>
                    </form>
                </div>

                <div class="col-7 d-none d-lg-block px-0">
                    <img class="object-fit-cover"
                         src="${ui.resourceLink('botswanaemr', 'images/bemr-login-banner.jpg')}"
                         width="100%" height="100%" alt=""/>
                </div>

                <div id="cannotLoginPopup" class="alert alert-info" style="display: none">
                    <div class="dialog-header">
                        <div class="row bg-primary">
                            <h3 class="text-white text-bolder">
                                <i class="fa fa-info-circle fa-1x pl-2"></i>
                                ${ui.message("referenceapplication.login.cannotLogin")}
                            </h3>
                        </div>
                    </div>
                    <div class="dialog-content">
                        <p class="dialog-instructions py-4">
                            ${ui.message("referenceapplication.login.cannotLoginInstructions")}
                        </p>
                        <button id="closeBtn" class="btn btn-dark bg-dark btn-block">${ui.message("Close")}</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
