<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    def appFrameworkService = context.getService(context.loadClass("org.openmrs.module.appframework.service.AppFrameworkService"))
    def overview = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.overview")
    def monthly = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.monthly")
    def registers = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.registers")
    def quarterly = appFrameworkService.getExtensionsForCurrentUser("org.openmrs.module.botswanaemr.reports.quarterly")
    def contextModel = [:]
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {label: "Reports"}
    ];
    jq(document).ready(function() {
        jq('#reportId').on('change', function() {
            var selectedReport = jq(this).find('option:selected');
            jq('input[name="reportName"]').val(selectedReport.text().trim());
        });
    });


</script>

<div class="row">
    <div class="col">

        <% if (moduleStatus) { %>
        <div class="card">
            ${ui.includeFragment("botswanaemrreports", "reports")}
        </div>
        <% if (context.getAuthenticatedUser().hasRole("Administrator")) { %>
        <div class="card">
            ${ui.includeFragment("botswanaemr", "reportSetup")}
        </div>
        <% } %>
        <% } else { %>
        <div class="card">
            <p>BotswanaEMR reporting module NOT started. Please load and start the module</p>
        </div>
        <% } %>
    </div>
</div>

<div class="modal fade" id="addReportPermissionModal" tabindex="-1"
     data-controls-modal="addReportPermissionModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addReportPermissionModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addReportPermissionModalTitle"><i><a
                        href="#">Add Entry</a></i>
                </h4>
                <i data-dismiss="modal" style="padding-right: 10px" class="icon-remove right "></i>
            </div>

            <div class="modal-body">
                <form action="${ui.actionLink('botswanaemr', 'reportSetup', 'saveMailTemplate')}">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Report</label>
                        <div class="col-sm-8">
                            <select class="form-control lockable custom-select" name="reportId"  required  id="reportId">
                                <option value="">Select a Report Type</option>
                                <% registers.each { %>
                                <option value=${it.id}>
                                ${ui.includeFragment("uicommons", "extension", [extension: it, contextModel: contextModel])}
                                </option>
                                <% } %>
                            </select>
                            <input name="reportName" style="display: none">
                        </div>
                    </div>
                    <fieldset class="form-group w-100">
                        <legend class="w-auto"><small>Recipients</small></legend>

                        <div class="form-group">
                            <label class="control-label">CC List (Comma Separated Email Addresses)</label>
                            <textarea class="form-control" rows="2" type="text" id="cccList" required name="cccList"></textarea>
                        </div>

                        <div class="form-group">
                            <label class="control-label">BCC List (Comma Separated Email Addresses)</label>
                            <input class="form-control" type="text" id="bccList" name="bccList">
                        </div>
                    </fieldset>
                    <div class="form-group" style="display: none">
                        <label for="mailFrom">From</label>
                        <input type="email" class="form-control" id="mailFrom" name="mailFrom" value="botswanaemr@gov.com">
                    </div>
                    <fieldset class="form-group w-100">
                        <legend class="w-auto"><small>Mail</small></legend>
                    <div class="form-group">
                        <label for="mailSubject">Subject</label>
                        <input type="text" class="form-control" name="mailSubject" id="mailSubject">
                    </div>
                    <div class="form-group">
                        <label for="mailContentText">Content :</label>
                        <textarea class="form-control" id="mailContentText" name="mailContentText" rows="3"></textarea>
                    </div>
                    </fieldset>

                    <div class="form-group">
                        <div class="col-sm">
                            <button class="btn horizontal-button btn-primary float-right" type="submit">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>