<%
    ui.includeJavascript("botswanaemr", "utils/countries.js")
    ui.includeJavascript("botswanaemr", "utils.js")
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var registrationType = getQueryParam('regType');
    
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.message("Regular Registration") }",
            link: "${ ui.pageLink("botswanaemr", "startRegistration") }"
        },
        { 
            label: registrationType === "routine" ? "${ ui.message('Routine Registration Form')}" : "${ ui.message('Emergency Registration Form')}" 
        }
    ]));
</script>

<script type="text/javascript">
    jQuery(function () {
        jQuery("#smartwizard").smartWizard({
            selected: 0,
            theme: 'dots',
            transition: {
                animation: 'none',
                speed: '400',
            },
            toolbarSettings: {
                showNextButton: true,
                showPreviousButton: true,
                overrideNextButton: true,
                overridePreviousButton: true,
                toolbarExtraButtons: [
                ]
            },
            keyboardSettings: {
                keyNavigation: false
            },
            lang: {
                next: 'Next',
                previous: 'Previous'
            },
            errorSteps: [],
        });
    });
</script>

<style>
.tab-full {
    height: fit-content !important;
}
.form-group .text-danger {
    color: red !important;
}
.error {
    color: red !important;
}
.iti--allow-dropdown {
    width: 100% !important;
}
</style>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <h5>Patient registration form</h5>
                    <p>Complete the form to register a patient</p>
                </div>
                <div class="col-3">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div id="smartwizard" class="px-5">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#step-1">
                            <span class="badge badge-pill badge-primary">1</span>
                            <small class="text-center text-danger">
                                Patient's Details
                            </small>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-2">
                            <span class="badge badge-pill badge-primary">2</span>
                            <small class="text-center text-danger">
                                Contact Person Details
                            </small>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#step-3">
                            <span class="badge badge-pill badge-primary">3</span>
                            <small class="text-center text-danger">
                                Confirm All Details
                            </small>
                        </a>
                    </li>
                </ul>
                <div class="container-fluid">
                    <div class="row hidden">
                        <button id="btnAdvancedSearch" class=" btn btn-sm btn-primary horizontal-button" data-toggle="modal" data-target="#clientRegistryModal">Advanced Client Registry Search</button> 
                    </div>
                
                    <!-- Search Modal -->
                    <div class="modal fade" id="clientRegistryModal" tabindex="-1"
                        data-controls-modal="clientRegistryModal" data-backdrop="static"
                        data-keyboard="false" role="dialog" aria-hidden="true"
                        aria-labelledby="clientRegistryModal">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                            <div class="modal-content" id="modalContent">
                                <div class="modal-header bg-primary">
                                    <h4 class="modal-title text-white" id="editPastOperationsModalTitle">Advanced Search</h4>
                                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col">
                                        <form id="advancedPatientSearch">
                                            <div class="form-group">
                                                <label>Select identification type</label>

                                                <div class="btn-group inline-button-group" role="group" style="padding-left: 5px">
                                                    <button type="button" id="btnAdvancedId"
                                                            class="btn btn-outline-secondary outline-button default-input"
                                                            onclick="selectAdvancedIdTypeIdentification(this.id)">ID</button>
                                                    <button type="button" id="btnAdvancedPassport"
                                                            class="btn btn-outline-secondary outline-button default-input"
                                                            onclick="selectAdvancedIdTypeIdentification(this.id)">Passport</button>
                                                    <button type="button" id="btnAdvancedBirthCertificate"
                                                            class="btn btn-outline-secondary outline-button default-input"
                                                            onclick="selectAdvancedIdTypeIdentification(this.id)">Birth Certificate</button>
                                                </div>
                                               
                                            </div>

                                            <div class="form-group">
                                                <label for="patientId"><span class="black-text" id="idNumberLabelAdvanced">ID number</span></label>

                                                <div class="form-group is-loading">
                                                    <input type="number" class="form-control spinner-text default-input" id="patientIdAdvanced" name="idNumber"
                                                        placeholder="Enter the patient's ID number" minlength="9" maxlength="9">

                                                    <div class="spinner-border spinner-section" role="status" id="advancedPatientSearchSpinner">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </div>
                                                <label id="patientIdErrorAdvanced"
                                                    class="form-text text-danger">Patient ID not found in registry. Register patient in the OpenCR.</label>
                                            </div>

                                            <div class="form-group">
                                               <div class="pl-0 pr-0">
                                                        <label for="givenName">First name</label>
                                                        <input type="text" class="form-control name-input" id="advancedGivenName" aria-describedby="nameHelp"
                                                            name="givenName" placeholder="Enter the first name">
                                                </div>
                                                <div class="pr-0 pl-0">
                                                        <label for="familyName">Last name</label>
                                                        <input type="text" class="form-control name-input" id="advancedFamilyName" aria-describedby="nameHelp" name="familyName" placeholder="Enter the last name">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="dateOfBirth">Date Of Birth</label>
                                                <input type="date" class="form-control birth-date" id="advancedDateOfBirth" name="dateOfBirth" placeholder="Patient's date of birth">
                                            </div>

                                            <div class="form-group">
                                                <label for="gender">Sex</label>
                                                <select class="form-control custom-select" id="advancedGender" name="gender" required>
                                                    <option disabled selected>Select the patient's sex</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col text-right">
                                                    <button id="cancelSearch" type="button" class="btn btn-outline-info cancel" data-dismiss="modal">Cancel</button>
                                                    <button id="btnAdvancedSearch" class="btn btn-primary" type="submit">Search</button>
                                                </div>    
                                            </div>
                                            
                                        </form>

                                        <div class="container-fluid" id="patientDataTableContainer" style="display: none;">
                                            <h5>Registry search results</h5>
                                            <table id="patientDataTable" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>ID Number</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Date of Birth</th>
                                                        <th>Sex</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
        
                <div class="container-fluid center-content-container">
                    <div class="tab-content tab-full">
                        <div id="step-1" class="tab-pane content-section text-left" role="tabpanel"
                             aria-labelledby="step-1">
                            <h5 class="h5 content-section">Patient's Details</h5>
                            <h6 class="h6">ID or passport numbers checkup</h6>

                            <p class="text-left content-section">By entering the patient's National Identity (ID) or passport number, <br/>the system will automatically lookup for them through the national database <br/>to complete their general details on this form
                            </p>

                            <form id="formPatientDetails" data-form-name="patientDetailsForm">
                                <div class="form-group">
                                    <label for="citizenType">Citizen Type :</label>
                                    <div class="form-check form-check-inline" id="citizenType">
                                        <input class="form-check-input" type="radio" name="citizenType" id="citizen" value="Citizen" data-attr="Citizen" checked>
                                        <label class="form-check-label radio-label" for="citizen">Citizen</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="citizenType" id="nonCitizen" value="Non-citizen" data-attr="Non-citizen">
                                        <label class="form-check-label radio-label" for="nonCitizen">Non-citizen</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Select identification type</label>

                                    <div class="btn-group inline-button-group" role="group" style="padding-left: 5px">
                                        <button type="button" id="btnId"
                                                class="btn btn-outline-secondary outline-button default-input"
                                                onclick="selectIdTypeIdentification(this.id)">ID</button>
                                        <button type="button" id="btnPassport"
                                                class="btn btn-outline-secondary outline-button default-input"
                                                onclick="selectIdTypeIdentification(this.id)">Passport</button>
                                        <button type="button" id="btnBirthCertificate"
                                                class="btn btn-outline-secondary outline-button default-input"
                                                onclick="selectIdTypeIdentification(this.id)">Birth Certificate</button>
                                        <button type="button" id="noIdentification"
                                                class="btn btn-outline-secondary outline-button default-input"
                                                onclick="selectIdTypeIdentification(this.id)">No Identification</button>
                                    </div>
                                    <label id="idTypeError"
                                           class="form-text text-danger">Patient identification type is required.</label>
                                </div>

                                <div class="form-group">
                                    <label for="patientId"><span class="black-text" id="idNumberLabel">ID number</span>
                                        <span class="required-label temp-label">*</span>
                                    </label>

                                    <div class="form-group is-loading text-right">
                                        <% if (mpiSearchEnabled) { %>
                                        <input type="number" class="form-control spinner-text default-input" id="patientId"
                                               name="idNumber" required
                                               placeholder="Enter the patient's ID number" minlength="9" maxlength="9">
                                               <a href="#"  data-toggle="modal" data-target="#clientRegistryModal"><span>Advanced Client Registry Demographics search</span></a>

                                        <div class="spinner-border spinner-section" role="status" id="patientSpinner">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                         <% } %>
                                    </div>
                                    <label id="patientIdError"
                                           class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                                    <a class="badge badge-primary badge-pill text-white px-1 py-1" href="{{= url }}" id="existing">${ui.message("Register as an existing patient >>")}</a>
                                </div>

                                <div class="form-group" id="fgCountry">
                                    <label for="country">Country
                                        <span class="required-label">*</span>
                                    </label>
                                    <input type="text" class="form-control" id="country" name="country"
                                        required placeholder="Please enter the patient's country">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col pl-0 pr-0">
                                            <label for="givenName">First name
                                                <span class="required-label">*</span>
                                            </label>
                                            <input type="text" class="form-control name-input" id="givenName" aria-describedby="nameHelp"
                                                   name="givenName"
                                                   required placeholder="Enter the first name">
                                        </div>
                                        <div class="col">
                                            <label for="middleName">Middle name
                                            </label>
                                            <input type="text" class="form-control name-input" id="middleName" aria-describedby="nameHelp"
                                                   name="middleName"
                                                   placeholder="Enter the middle name">
                                        </div>
                                        <div class="col pr-0 pl-0">
                                            <label for="familyName">Last name
                                                <span class="required-label">*</span>
                                            </label>
                                            <input type="text" class="form-control name-input" id="familyName" aria-describedby="nameHelp"
                                                   name="familyName"
                                                   required placeholder="Enter the last name">
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="gender">Sex
                                        <span class="required-label">*</span>
                                    </label>
                                    <select class="form-control custom-select" id="gender" name="gender" required>
                                        <option disabled selected>Select the patient's sex</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    <label id="genderError"
                                           class="form-text text-danger">Wrong gender selected.</label>
                                </div>

                                <div class="form-group">
                                    <label for="dateOfBirth">Date Of Birth
                                        <span class="required-label">*</span>
                                    </label>
                                    <input type="text" class="form-control birth-date" id="dateOfBirth"
                                           name="dateOfBirth"
                                           required placeholder="Patient's date of birth">
                                    <label id="dateOfBirthError" class="error" for="dateOfBirth">This field is required.</label>
                                </div>

                                <div class="form-group">
                                    <input class="form-check-input" type="checkbox" value="" id="dobEstimate">
                                    <label class="form-check-label" for="dobEstimate"
                                           style="margin-top: auto">
                                        Date of birth estimated?
                                    </label>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col pl-0 pr-0">
                                            <label for="age">Age
                                                <span class="required-label">*</span>
                                            </label>
                                            <input type="number" class="form-control" id="age" name="age" min="0"
                                                   required placeholder="Years">
                                        </div>
                                        <div class="col pl-0 pr-0 age-div" id="months">
                                            <label for="ageMonth">Months
                                            </label>
                                            <input type="number" class="form-control" id="ageMonth" name="ageMonth" min="0" max="11"
                                                   placeholder="Months">
                                        </div>
                                        <div class="col pl-0 pr-0 age-div" id="weeks">
                                            <label for="ageWeeks">Weeks
                                            </label>
                                            <input type="number" class="form-control" id="ageWeeks" name="ageWeeks" min="0"
                                                   placeholder="Weeks">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="maritalStatus">Marital Status
                                        <span class="required-label">*</span>
                                    </label>
                                    <select type="text" class="form-control custom-select" id="maritalStatus"
                                            name="maritalStatus">
                                        <option disabled selected>Select the patient's marital status</option>
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Divorced">Divorced</option>
                                        <option value="Widowed">Widowed</option>
                                        <option value="Separated">Separated</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="emailAddress"
                                           placeholder="Patient's email address">
                                    <label id="patientEmailError"
                                           class="form-text text-danger">Invalid email address</label>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <label for="phoneNumber">Contact number
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-3 pl-0 pr-0 type-col">
                                            <select class="form-control custom-select" id="numberType" name="numberType"
                                            style="min-width: fit-content;">
                                                <option>Mobile</option>
                                                <option>Telephone</option>
                                            </select>
                                        </div>
                                        <div class="col pl-0 pr-0">
                                            <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber"
                                                   placeholder="e.g 71 22 2341" minlength="7" maxlength="8" required>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <input class="form-check-input" type="checkbox" value="" id="phoneNumberNa">
                                    <label class="form-check-label" id="phoneNumberNaLabel" for="phoneNumberNa"
                                           style="margin-top: auto">
                                        Confirm the patient does not have a phone number
                                        <span class="required-label">*</span>
                                    </label>
                                </div>

                                <div class="contact-template">
                                    <div class="form-group">
                                        <div class="row">
                                            <label>Contact number</label>
                                        </div>
                                        <div class="row">
                                            <div class="col-3 pl-0 pr-0 type-col">
                                                <select class="form-control custom-select" id="extraNumberType" name="extraNumberType"
                                                        style="min-width: fit-content;">
                                                    <option>Mobile</option>
                                                    <option>Telephone</option>
                                                </select>
                                            </div>
                                            <div class="col pl-0 pr-0">
                                                <div class="text-icon-div">
                                                    <input type="tel" class="form-control" id="extraPhone" name="extraPhone"
                                                           placeholder="Patient's phone number" minlength="7" maxlength="8">
                                                    <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <label id="extraPhoneError" class="error" for="extraPhone"></label>
                                    </div>
                                </div>

                                <div id="extraContacts"></div>

                                <button class="dashed-button rounded"
                                        id="btnAddContact">+ Add another contact number</button>

                                <% if (isBf) { %>
                                <div class="form-group" id="fgAffiliation">
                                    <label for="affiliation">Patient Affiliation
                                    </label>
                                    <select type="text" class="form-control custom-select" id="affiliation"
                                            name="affiliation">
                                        <option disabled selected>Select the patient affiliation</option>
                                        <option value="Civilian">Civilian</option>
                                        <option value="Force Officer">Force Officer</option>
                                        <option value="Dependent to a force officer">Dependent to a force officer</option>
                                    </select>
                                </div>

                                <div class="form-group" id="fgForceNumber">
                                    <label for="forceNumber">Force Number</label>
                                    <input type="text" class="form-control" id="forceNumber" name="forceNumber"
                                           placeholder="Force Number">
                                </div>

                                <div class="form-group" id="fgRank">
                                    <label for="rank">Rank</label>
                                    <input type="text" class="form-control" id="rank" name="rank"
                                           placeholder="Rank">
                                </div>
                                <% } %>

                                <div class="form-group" id="fgEducation">
                                    <label for="education">Highest education Level
                                    </label>
                                    <select type="text" class="form-control custom-select" id="education"
                                            name="education">
                                        <option selected value="">Select patient's education level</option>
                                        <%  educationLevels.each {  key, val -> %>
                                        <option value="${val.answerConcept.id}">${key.name}</option>
                                        <% } %>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="occupation">Occupation</label>
                                    <select type="text" class="form-control custom-select" id="occupation"
                                            name="occupation">
                                    </select>
                                    <input type="text" class="form-control" id="otherOccupation" name="otherOccupation"
                                           placeholder="Enter the occupation" style="display: none;">
                                </div>


                                <div class="form-group" id="fgEmployer">
                                    <label for="employer">Name of employer
                                    </label>
                                    <input type="text" class="form-control" id="employer" name="employer"
                                           placeholder="Please enter the name of the employer">
                                </div>

                                <div class="form-group">
                                    <label for="employmentSector">Employment Sector
                                    </label>
                                    <select type="text" class="form-control custom-select" id="employmentSector"
                                            name="employmentSector">
                                        <option selected value="">Select patient's employment sector</option>
                                        <%  employmentSectors.each{ %>
                                        <option value="${it.id}">${it.name.name}</option>
                                        <% } %>
                                    </select>
                                </div>

                                <div class="form-group" id="otherEmploymentSectorGrp">
                                    <label for="otherEmploymentSector">Other employment sector
                                    </label>
                                    <input type="text" class="form-control" id="otherEmploymentSector" name="otherEmploymentSector"
                                           placeholder="Specify other employment sector">
                                </div>

                                <div class="row">
                                    <div class="col pl-0 pb-1">
                                        <h6 class="h6">Residential Address</h6>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="permanentResidentialAddress" name="permanentResidentialAddress">
                                            <label class="form-check-label" for="permanentResidentialAddress">
                                                Permanent Residential Address
                                            </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="currentResidentialAddress" name="currentResidentialAddress">
                                            <label class="form-check-label" for="currentResidentialAddress">
                                                Current Residential Address
                                            </label>
                                    </div>
                                </div>
                                <div class="form-group" id="permanentResidential" style="display: none;">
                                    <div class="row">
                                        <div class="col pl-0 pb-1">
                                            <h6 class="h6">Permanent Residential address</h6>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">District
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <select type="text" class="form-control custom-select" id="address2"
                                            name="district" required>
                                            <option disabled selected>Select the patient's district</option>
                                        </select>
                                        <input type="text" class="form-control" id="otherDistrict" name="otherDistrict"
                                            placeholder="Specify the district">
                                    </div>

                                    <div class="form-group">
                                        <label for="cityVillage">City/Town/Village
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                            <input type="text" class="form-control" id="cityVillage" name="city"
                                                required placeholder="Patient's city">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Ward/Locality
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address5" name="ward"
                                            placeholder="Patient's ward/locality">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">House/Unit/Apartment/Plot No
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address7" name="houseNo"
                                            placeholder="Patient's House/Unit/Apartment/Plot no.">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Other address information e.g Landmark
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address4" name="landmark"
                                            placeholder="Other address information e.g Landmark">
                                    </div>
                                    <div class="row hidden">
                                        <div class="col pl-0 pb-1">
                                            <h6 class="h6">Current residential address</h6>
                                        </div>
                                        <div class="col pl-0 pb-1">
                                            <button class="mini-button rounded btn-address mt-4 mb-1 float-right" id="btnCopyResAddress">Copy Permanent Address</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="currentResidential" style="display: none;">
                                    <div class="row">
                                        <div class="col pl-0 pb-1">
                                            <h6 class="h6">Current Residential address</h6>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address2">District
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <select type="text" class="form-control custom-select" id="currentDistrict"
                                            name="district" required>
                                            <option disabled selected>Select the patient's district</option>
                                        </select>
                                        <input type="text" class="form-control" id="otherDistrict2" name="otherDistrict2"
                                            placeholder="Specify the district">
                                    </div>

                                    <div class="form-group">
                                        <label for="cityVillage">City/Town/Village
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                            <input type="text" class="form-control" id="currentCityVillage" name="city"
                                                required placeholder="Patient's city">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Ward/Locality
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address5" name="ward"
                                            placeholder="Patient's ward/locality">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">House/Unit/Apartment/Plot No
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address7" name="houseNo"
                                            placeholder="Patient's House/Unit/Apartment/Plot no.">
                                    </div>
                                    <div class="form-group">
                                        <label for="address2">Other address information e.g Landmark
                                            <span class="required-label temp-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="address4" name="landmark"
                                            placeholder="Other address information e.g Landmark">
                                    </div>
                                    <div class="row hidden">
                                        <div class="col pl-0 pb-1">
                                            <h6 class="h6">Current residential address</h6>
                                        </div>
                                        <div class="col pl-0 pb-1">
                                            <button class="mini-button rounded btn-address mt-4 mb-1 float-right" id="btnCopyResAddress">Copy Permanent Address</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div id="step-2" class="tab-pane content-section" role="tabpanel" aria-labelledby="step-2">
                            <h5 class="text-left h5 content-section">Contact Person Details</h5>
                            <h6 class="text-left h6">ID or passport number check up</h6>

                            <p class="text-left">By entering the patient's National Identity (ID) or passport number, <br/>
                            the system will automatically lookup for them through the national database <br/> to complete their general details on this form
                            </p>

                            <form id="formNOK">
                                <div id="nexOfKin">
                                    <div class="form-group nokContactPersonType" id="nokContactPersonType">
                                        <label>Select Contact Person type <span class="required-label">*</span></label>
                                        <span class="form-check error-message" style="display: none;"></span>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="nextOfKinType" name="nokContactPersonType">
                                            <label class="form-check-label" for="nextOfKinType">
                                                Next of Kin : With decision-making rights on the health of the patient.
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="contactPersonType" name="nokContactPersonType">
                                            <label class="form-check-label" for="contactPersonType">
                                                Contact Person : A person to contact in relation with the patient
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="treatmentBuddyType" name="nokContactPersonType">
                                            <label class="form-check-label" for="treatmentBuddyType">
                                                Treatment Buddy : A person to be contacted for a particular program issue
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="unspecifiedPersonType" name="nokContactPersonType">
                                            <label class="form-check-label" for="unspecifiedPersonType">
                                                Unspecified : Unspecified contact person 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Select identification type</label>

                                        <div class="btn-group inline-button-group" role="group" >
                                            <button type="button" id="nokBtnId"
                                                    class="btn btn-outline-secondary outline-button "
                                                    onclick="selectNokIdTypeIdentification(this.id)">ID</button>
                                            <button type="button" id="nokBtnPassport"
                                                    class="btn btn-outline-secondary outline-button"
                                                    onclick="selectNokIdTypeIdentification(this.id)">Passport</button>
                                        </div>
                                        <label id="nokIdTypeError"
                                                   class="form-text text-danger">Identification type is required.</label>
                                    </div>

                                    <div class="form-group">
                                        <label for="nokIdNumber"><span class="black-text" id="nokIdNumberLabel">ID number</span>
                                        </label>

                                        <div class="form-group is-loading">
                                            <input type="number" class="form-control" id="nokIdNumber" name="idNumber"
                                                   minlength="9" placeholder="enter ID number">

                                            <div class="spinner-border spinner-section" role="status" id="nokSpinner">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                        <label id="nokIdError"
                                               class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                                    </div>

                                    <div class="form-group">
                                        <label for="nokFullName">Full Name
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="text" class="form-control" id="nokFullName" name="fullName"
                                               required
                                               placeholder="enter full name">
                                    </div>

                                    <div class="form-group">
                                        <label for="nokRelationship">Relationship
                                            <span class="required-label">*</span>
                                        </label>
                                        <select class="custom-select" id="nokRelationship" name="relationship" required>
                                            <option disabled selected>Select the relationship with the patient</option>
                                            <%  relationships.each{ %>
                                                <% if(!it.equals('Patient') && !it.equals('Other') ) { %>
                                                    <option value="${it}">${it}</option>
                                                <% } %>
                                            <% } %>
                                            <% if(relationships.contains('Other')) { %>
                                                <option value="Other">Other</option>
                                            <% } %>
                                        </select>
                                        <input type="text" class="form-control" id="otherNokRelationship" name="otherNokRelationship"
                                            placeholder="Specify the relationship">

                                    </div>

                                    <div class="form-group">
                                        <label for="nokContact">Contact number
                                            <span class="required-label">*</span>
                                        </label>
                                        <input type="tel" class="form-control" id="nokContact" name="contactNumber"
                                               required
                                               minlength="7"
                                               maxlength="8"
                                               placeholder="enter contact number">
                                    </div>

                                    <div id="extraNokContact" class="extra-nok-contact"></div>

                                    <button class="dashed-button rounded add-nok-contact mt-4"
                                            id="btnAddNokContact">+ Add another contact number</button>

                                    <div class="form-group">
                                        <label for="nokEmail">Email
                                        </label>
                                        <input type="email" class="form-control" id="nokEmail" name="emailAddress"
                                               placeholder="enter email address">
                                        <label id="nextOfKinEmailError"
                                               class="form-text text-danger">Invalid email address</label>
                                    </div>
                                    <div class="text-icon-div">
                                        <h6></h6>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col pl-0 pb-1">
                                                <label for="nokAddress2">District</label>
                                            </div>
                                            <div class="col pr-0">
                                                <button class="mini-button rounded btn-address mt-4 mb-1 float-right" id="btnAddress">Copy Patient Address</button>
                                            </div>
                                        </div>
                                        <select type="text" class="form-control custom-select" id="nokAddress2"
                                                name="nokAddress2">
                                            <option disabled selected>Select the District</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="nokCityVillage">City/Town/Village
                                        </label>
                                        <input type="text" class="form-control" id="nokCityVillage" name="nokCityVillage"
                                               placeholder="City">
                                    </div>

                                </div>

                                <div class="nok-contact-template">
                                    <div class="form-group">
                                        <label>Contact number
                                            <span class="required-label">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel" class="form-control" id="extraNokContact" name="extraNokContact"
                                                   required placeholder="Next of kin's phone number" minlength="7" maxlength="8">
                                            <i class="fa fa-trash-o  fa-2x delete-icon delete-extra-contact" id="extraNokContactDelete"></i>
                                        </div>
                                        <label id="extraNokContactError" class="error" for="extraNokContact"></label>
                                    </div>
                                </div>

                                <div id="extraNoks"></div>

                                <button class="dashed-button rounded"
                                        id="btnAddNok">+ Add another next of kin</button>
                            </form>
                        </div>

                        <div id="step-3" class="tab-pane content-section" role="tabpanel" aria-labelledby="step-3">
                            <h5 class="text-left h5 content-section">Confirm details</h5>
                            <h6 class="text-left h6">Please confirm the patient's details below before registering them.</h6>

                            <div class="details-card text-left">
                                <h6 class="h6"><strong>Patient general details</strong></h6>
                                <span style="display: none">
                                    ${ui.includeFragment("botswanaemr", "widget/facilityLocation")}
                                </span>
                                <div id="generalDetails">
                                </div>

                                <h6 class="h6 content-section"><strong>Contact Person details</strong></h6>

                                <div id="nokDetails">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="button-section">
                        <button id="btnPrevious" class="btn btn-dark bg-dark horizontal-button">Previous</button>
                        <button id="btnNext" class=" btn btn-primary horizontal-button">Next Step</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
