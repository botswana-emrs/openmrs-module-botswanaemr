<%
    ui.decorateWith("botswanaemr", "botswanaHeadlessPage")
%>
${ui.includeFragment("botswanaemr", "familyplanning/familyPlanningHtmlFormModal", [
        formUuid            : formUuid,
        modalTitle          : modalTitle,
        encounter           : encounter,
        hasEncounter        : hasEncounter,
        definitionUiResource: definitionUiResource]
)}
