<%
    ui.decorateWith("botswanaemr", "botswanaHeadlessPage")
%>
${ ui.includeFragment("botswanaemr", "familyplanning/familyPlanningHtmlFormModal", [
        formUuid: gynaecologicalFormUuid,
        modalTitle: modalTitle,
        encounter: gynaecologicalEncounter,
        hasEncounter: hasGynaecologicalEncounter,
        definitionUiResource: gynaecologicalDefinitionUiResource]
)}
