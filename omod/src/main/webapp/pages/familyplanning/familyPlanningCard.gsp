<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'},
        {
            label: "SRH Client Profile",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhClientProfile.page?patientId=${patient.uuid}&visitId=${visit.uuid}&returnUrl=${returnUrl}'
        },
        {
            label: "Family Planning Card",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/familyplanning/familyPlanningCard.page?patientId=${patient.uuid}&visitId=${visit.uuid}&returnUrl=${returnUrl}'
        },
        { label: "${patient.familyName} ${patient.givenName}"}
    ];
</script>

<script type="text/javascript">
    jq(document).ready(function () {
        function populateTable(data) {
            let tbody = jq('#tblActivityTrail tbody');
            tbody.empty();

            jq.each(data, function(index, item) {
                var row = jq('<tr></tr>');

                row.append('<td>' + (index + 1) + '</td>');
                row.append('<td>' + item.dateCreatedString + '</td>');
                row.append('<td>' + item.action + '</td>');
                row.append('<td>' + item.userDetails + '</td>');

                tbody.append(row);
            });
        }

        const AUDIT_LOG_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/auditLogs";
        function getAuditLogs(objectId, objectType, title) {
            jq.ajax({
                type: "GET",
                url: AUDIT_LOG_URL,
                dataType: "json",
                global: false,
                async: true,
                data: {
                    objectId: objectId,
                    objectType: objectType,
                },
                success: function (data) {
                    populateTable(data);
                    jq("#activityTrailModalLabel").text(title + " - Activity Trail")
                    jq('#activityTrailModal').modal("show");
                }
            });
        }

        jq(document).on("click", ".view-activity", function () {
            let objectId = jq(this).attr("objectId");
            let objetType = jq(this).attr("objectType");
            let title = jq(this).attr("title");
            getAuditLogs(objectId, objetType, title);
        });
    });
</script>

${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<div class="row">
    <div class="col">
        <div class="container-fluid card shadow d-flex justify-content-center">
            ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
            ${ui.includeFragment("botswanaemr", "hivStatusBar", [patientId: patient])}
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a  class="nav-link active" id="pills-family-planning-social-habits-tab"
                        data-toggle="pill" href="#pills-family-planning-social-habits"
                        role="tab" aria-controls="pills-family-planning-social-habits"
                        aria-selected="false"> SOCIAL HABITS
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-family-planning-medical-info-tab"
                        data-toggle="pill" href="#pills-family-planning-medical-info"
                        role="tab" aria-controls="pills-family-planning-medical-info"
                        aria-selected="false"> MEDICAL/SURGICAL
                    </a>
                </li>
                <% if (patient.person.gender == 'F') { %>
                    <li class="nav-item">
                        <a  class="nav-link" id="pills-family-planning-productive-info-tab"
                            data-toggle="pill" href="#pills-family-planning-productive-info"
                            role="tab" aria-controls="pills-family-planning-productive-info"
                            aria-selected="false"> GYNAECOLOGICAL
                        </a>
                    </li>
                <% } %>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-family-planning-sexual-info-tab"
                        data-toggle="pill" href="#pills-family-planning-sexual-info"
                        role="tab" aria-controls="pills-family-planning-sexual-info"
                        aria-selected="false"> SEXUAL/STI
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-family-planning-contraceptive-info-tab"
                        data-toggle="pill" href="#pills-family-planning-contraceptive-info"
                        role="tab" aria-controls="pills-family-planning-contraceptive-info"
                        aria-selected="false"> CONTRACEPTIVE
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-family-planning-physical-exam-info-tab"
                        data-toggle="pill" href="#pills-family-planning-physical-exam-info"
                        role="tab" aria-controls="pills-family-planning-physical-exam-info"
                        aria-selected="false"> PHYSICAL EXAMINATION
                    </a>
                </li>
                 <li class="nav-item">
                    <a  class="nav-link" id="pills-family-planning-plan-info-tab"
                        data-toggle="pill" href="#pills-family-planning-plan-info"
                        role="tab" aria-controls="pills-family-planning-plan-info"
                        aria-selected="false"> PLAN
                    </a>
                 </li>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-family-planning-social-habits"
                     role="tabpanel" aria-labelledby="pills-family-planning-social-habits-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/familyPlanningSocialHabits") }
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-family-planning-medical-info"
                     role="tabpanel" aria-labelledby="pills-family-planning-medical-info-tab">
                     ${ ui.includeFragment("botswanaemr", "familyplanning/familyPlanningMedicalHistory") }
                </div>
                <!-- 3rd tab -->
                <div class="tab-pane fade" id="pills-family-planning-productive-info"
                     role="tabpanel" aria-labelledby="pills-family-planning-productive-info-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/reproductiveGynaecological") }
                </div>
                <!-- 4th tab -->
                <div class="tab-pane fade" id="pills-family-planning-sexual-info"
                     role="tabpanel" aria-labelledby="pills-family-planning-sexual-info-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/sexualStiHistory") }
                </div>
                <!-- 5th tab -->
                <div class="tab-pane fade" id="pills-family-planning-contraceptive-info"
                     role="tabpanel" aria-labelledby="pills-family-planning-contraceptive-info-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/contraceptiveHistory") }
                </div>
                <!-- 6th tab -->
                <div class="tab-pane fade" id="pills-family-planning-physical-exam-info"
                     role="tabpanel" aria-labelledby="pills-family-planning-physical-exam-info-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/physicalExaminationFemaleHistory") }
                </div>
                 <!-- 7th tab -->
                 <div class="tab-pane fade" id="pills-family-planning-plan-info"
                    role="tabpanel" aria-labelledby="pills-family-planning-plan-info-tab">
                    ${ ui.includeFragment("botswanaemr", "familyplanning/planHistory") }
                 </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="activityTrailModal" tabindex="-1" aria-labelledby="activityTrailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="activityTrailModalLabel">Activity Trail</h5>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <table id="tblActivityTrail">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Action</th>
                        <th>By</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" style="background: #6c757d; border: 1px solid #6c757d;" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
