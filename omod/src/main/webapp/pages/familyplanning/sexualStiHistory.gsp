<%
    ui.decorateWith("botswanaemr", "botswanaHeadlessPage")
%>
${ ui.includeFragment("botswanaemr", "familyplanning/familyPlanningHtmlFormModal", [
        formUuid: sexualStiHistoryFormUuid,
        modalTitle: sexualStiHistoryModalTitle,
        encounter: sexualStiHistoryEncounter,
        hasEncounter: hasSexualStiEncounter,
        definitionUiResource: sexualStiFormDefinitionUiResource]
)}
