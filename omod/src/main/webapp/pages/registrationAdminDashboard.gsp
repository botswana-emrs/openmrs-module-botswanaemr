<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "Registration Admin Dashboard Portal"}
    ];

    jq(document).ready(function() {
        toggleGraph();
    })
</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Total Registrations Completed</div>
                    <div class="stat-digit text-success">${allRegisteredPatients}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "REGISTRATION"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Registrations
                            <span class="text-danger">${registeredPatientsDailyAverage}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's Registrations</div>
                    <div class="stat-digit text-success">${todayRegisteredPatients}</div>
                        <div class="fullwidth">
                            ${ ui.includeFragment("botswanaemr", "todayGraphSummaries") }
                        </div>
                        <div class="clearfix">
                            <small class="pl-0">
                                Yesterday
                                <span class="text-danger">${yesterdayRegisteredPatients}</span>
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                    <div class="card mt-4">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-9 pr-0">
                                <h4 class="pl-0"> Today's Registrations</h4>
                            </div>
                        </div>
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "dailyRegisteredList") }
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                    <div class="card mt-4">
                        <h4>Activity</h4>
                        <% todayRegisteredPatientsList.each { %>
                            <ul class="list-group">
                                <li class="list-group-item">
                                   <small>
                                       <span>${it.creator}</span> registered
                                       <span class="text-success">${it.name}</span>
                                       <span class="text-danger">${it.gender}</span>
                                       <span class="text-muted">${it.duration}</span>
                                   </small>
                                </li>
                            </ul>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
