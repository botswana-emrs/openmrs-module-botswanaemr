<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
	ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("uicommons", "ngDialog/ngDialog.js")
    ui.includeJavascript("uicommons", "ngDialog/ngDialog.js")
    ui.includeJavascript("uicommons", "services/conceptSearchService.js")
    ui.includeJavascript("uicommons", "directives/coded-or-free-text-answer.js")
    ui.includeCss("uicommons", "ngDialog/ngDialog.min.css")

    ui.includeCss("allergyui", "allergy.css")
    def isEdit = allergy.id != null;
    def title = isEdit ?
            ui.message("allergyui.editAllergy", ui.escapeJs(ui.encodeHtmlContent(ui.format(allergy.allergen.coded ? allergy.allergen.codedAllergen : allergy.allergen)))) :
            ui.message("allergyui.addNewAllergy");

    def allergensByType = [
        DRUG: drugAllergens,
        FOOD: foodAllergens,
        OTHER: otherAllergens
    ]
%>
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.givenName))) }" , link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'},
        { label: "${ ui.message("allergyui.allergies") }", link: '${ui.pageLink("botswanaemr", "allergies", [patientId: patient.uuid, returnUrl: returnUrl])}'},
        { label: "${ title }" }
    ];

</script>
<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        ${ ui.includeFragment("coreapps", "patientHeader", [ patient: patient ]) }
    </div>
</div>

${ ui.includeFragment("allergyui", "removeAllergyDialog") }

<% ui.includeJavascript("allergyui", "allergies.js") %>
<div class="row">
    <div class="col">
        <div class="card">
            <div id="allergy" ng-app="allergyApp" ng-controller="allergyController" ng-init="severity = ${ allergy?.severity?.id }; allergenType='${allergy.allergen == null ? "DRUG" : allergy.allergen.allergenType}'; otherConceptId=${otherNonCodedConcept.conceptId}">
                <div class="row justify-content-center">
                    <div class="col col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                        <h4 class="inline pr-5">${ title }</h4>
                        <div class="col-9 pt-3 pl-0">
                            <% if (isEdit) { %>
                                <button type="button" class="btn btn-sm btn-danger btn-block float-left" onclick="removeAllergy('${ ui.encodeJavaScriptAttribute(ui.format(allergy.allergen)) }', ${ allergy.id})">
                                     ${ ui.message("allergyui.removeAllergy") }
                                </button>
                            <% } %>
                        </div> 
                        <form method="post" id="allergy" action="${ ui.pageLink("botswanaemr", "allergy", [patientId: patient.uuid, returnUrl: returnUrl]) }">
                            <input type="hidden" name="allergenType" value="{{allergenType}}"/>
                            <% if (isEdit) { %>
                                <input type="hidden" name="allergyId" value="${allergy.id}" />
                            <% } %>
                            <div class="row">
                                <div id="allergens" class="col col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                                    <% if (!isEdit) { %>
                                        <div id="types" class="button-group horizontal">
                                            <% allergenTypes.each { category ->  if (category.name()!="ENVIRONMENT") { %>
                                            <label class="button small" ng-model="allergenType" btn-radio="'${ui.format(category)}'" ng-class="{ confirm: allergenType == '${ui.format(category)}' }">
                                                ${ category }
                                            </label>
                                            <% } } %>
                                        </div>
                                        <% allergensByType.each {
                                            def typeName = it.key
                                            def allergens = it.value
                                        %>
                                        <ul ng-show="allergenType == '${ typeName }'">
                                            <% allergens.each { allergen -> %>
                                            <li>
                                                <% if (allergen.id == otherNonCodedConcept.id) { %>
                                                    <input id="allergen-${allergen.id}" type="radio" name="codedAllergen" value="${allergen.id}" class="coded_allergens" ng-model="allergen" ng-click="checkOtherRadioButton()"
                                                        ${(allergy.allergen != null && allergen == allergy.allergen.codedAllergen) ? "checked=checked" : ""}/>
                                                <% } else { %>
                                                    <input id="allergen-${allergen.id}" type="radio" name="codedAllergen" value="${allergen.id}" class="coded_allergens" ng-model="allergen"
                                                        ${(allergy.allergen != null && allergen == allergy.allergen.codedAllergen) ? "checked=checked" : ""}/>
                                                <% } %>
                                                <label for="allergen-${allergen.id}" id="allergen-${allergen.id}-label" class="coded_allergens_label" ng-click="otherFieldFocus()">${ui.format(allergen)}</label>

                                                <% if (allergen.id == otherNonCodedConcept.id) { %>
                                                    <% if(typeName == 'DRUG') { %>
                                                        <input type="hidden" name="otherCodedAllergen" ng-value="otherCodedAllergen.concept ? 'CONCEPT:'+otherCodedAllergen.concept.uuid : 'NON_CODED:'+otherCodedAllergen.word">
                                                        <coded-or-free-text-answer id="${typeName}otherCodedAllergen" concept-classes="8d490dfc-c2cc-11de-8d13-0010c6dffd0f,b4535251-9183-4175-959e-9ee67dc71e78" ng-model="otherCodedAllergen" ng-click="otherFieldFocus()" />
                                                    <% } else {%>
                                                        <input type="text" class="form-control" id="${typeName}nonCodedAllergen" name="nonCodedAllergen" ng-model="nonCodedAllergen" ng-focus="otherFieldFocus()"/>
                                                    <% } %>
                                                <% } %>
                                            </li>
                                            <% } %>
                                        </ul>
                                        <% } %>
                                    <% } %>
                                </div>
                            </div>
                            <div class="row">
                                <div id="reactions" class="col col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                                    <label class="heading">${ ui.message("allergyui.reactionSelection") }:</label>
                                    <ul>
                                        <% reactionConcepts.each { reaction -> %>
                                            <li>
                                                <input ng-model="reaction${reaction.id}" type="checkbox" id="reaction-${reaction.id}" class="allergy-reaction" name="allergyReactionConcepts" value="${reaction.id}" ng-init="reaction${reaction.id} = ${ allergyReactionConcepts.contains(reaction) }" />
                                                <label for="reaction-${reaction.id}">${ui.format(reaction)}</label>
                                                <% if (reaction.id == otherNonCodedConcept.id) { %>
                                                    <input type="text" class="form-control" name="reactionNonCoded" ng-focus="otherReactionFocus(${reaction.id})" <% if (allergy.reactionNonCoded) { %> value="${allergy.reactionNonCoded}" <% } %>/>
                                                <% } %>
                                            </li>
                                        <% } %>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col col-sm-12 col-md-12 col-lg-12">
                                    <label class="heading">${ ui.message("allergyui.severity") }:</label>
                                    <div id="severities" class="form-inline pt-3">
                                        <% severities.each { severity -> %>
                                            <p>
                                                <input type="radio" id="severity-${severity.id}" class="allergy-severity" name="severity" value="${severity.id}" ${ severity == allergy.severity ? "checked=checked" : "" } ng-checked="severity == ${severity.id}" ng-model="severity"/>
                                                <label for="severity-${severity.id}">${ui.format(severity)}</label>
                                            </p>
                                        <% } %>
                                        <i class="icon-remove delete-item pt-0" title="${ ui.message("general.clear") }" ng-click="severity = null"></i>
                                    <div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="comment" class="col col-sm-12 col-md-12 col-lg-11 pl-0 pr-0">
                                    <label>${ ui.message("allergyui.comment") }:</label>
                                    <textarea id="allergy-comment" maxlength="1024" name="comment">${allergy.comment != null ? ui.encodeHtmlContent(allergy.comment) : ""}</textarea>
                                </div>
                            </div>

                            <div id="actions" class="row">
                                 <div class="col col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                                     <button type="button"
                                             class="btn btn-dark bg-dark"
                                             onclick="location.href='${ ui.pageLink("botswanaemr", "allergies", [patientId: patient.uuid, returnUrl: returnUrl ]) }'" />
                                             ${ ui.message("coreapps.cancel") }
                                    </button>
                                 </div>
                                 <div class="col col-sm-12 col-md-12 col-lg-3 pl-4 pr-2">
                                     <input type="submit" id="addAllergyBtn"
                                            class="btn btn-sm btn-block btn-success"
                                            value="${ ui.message("coreapps.save") }"
                                            <% if(!isEdit){ %> ng-disabled="!canSave()" <% } %>/>
                                 </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>