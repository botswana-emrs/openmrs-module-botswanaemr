<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/models/concept.model.js")
    ui.includeJavascript("coreapps", "conditionlist/models/condition.model.js")
    ui.includeJavascript("coreapps", "conditionlist/emr.messages.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("coreapps", "conditionlist/controllers/manageconditions.controller.js")

    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
%>

    <div class="row">
       <div class="col col-sm-12 col-md-12 col-lg-12">
           ${ui.includeFragment("coreapps", "patientHeader", [patient: patient])}
       </div>
    </div>

    <script type="text/javascript">
        var breadcrumbs = [
            { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
            {
                label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
                link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
            },
            {label: "${ ui.message("coreapps.conditionui.conditions") }"}
        ];
    </script>

    <script>
        jQuery.noConflict();
        jQuery(function () {
            jQuery("#tabs").tabs();
        });
    </script>

    <div class="row">
        <div class="col">
            <div id="condition" ng-app="manageConditionsApp" ng-controller="ManageConditionsController"
                 ng-init="conditionHistoryList = getConditions('${patient.uuid}')">
                <div id="tabs">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="tabs-height" href="#ACTIVE">
                                ${ui.message('coreapps.conditionui.active.label')}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="tabs-height" href="#INACTIVE">
                                ${ui.message('coreapps.conditionui.inactive.label')}
                            </a>
                        </li>
                    </ul>

                    <span ng-repeat="tab in tabs">
                        <div id="{{tab}}">
                            <div class="table-responsive">
                                <table class="conditions-table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>${ui.message("coreapps.conditionui.condition")}</th>
                                        <th>${ui.message("coreapps.conditionui.onsetdate")}</th>
                                        <th ng-if="tab==='INACTIVE'">${ui.message("coreapps.stopDate.label")}</th>
                                        <th ng-if="'${hasModifyConditionsPrivilege}'">${ui.message("coreapps.actions")}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-if="conditions.size() == 0">
                                        <td colspan="6" align="center">
                                            ${ui.message("coreapps.conditionui.noKnownConditions")}
                                        </td>
                                    </tr>
                                    <tbody ng-repeat="conditionHistory in conditionHistoryList">
                                    <tr class="clickable-tr" ng-init="condition = conditionHistory.conditions[conditionHistory.conditions.length-1]"
                                        ng-show="condition.status===tab && condition.voided === false">
                                        <td>{{condition.concept.name}}</td>
                                        <td>{{formatDate(condition.onSetDate)}}</td>
                                        <td ng-if="condition.status==='INACTIVE' && condition.voided===false" ng-style="strikeThrough(condition.voided)">{{formatDate(condition.endDate)}}</td>
                                        <td ng-if="'${hasModifyConditionsPrivilege}'">
                                            <button class="btn btn-sm btn-primary">
                                                <i class="fa fa-pencil" title="${ui.message("coreapps.conditionui.editCondition","")}"
                                                    ng-click="redirectToEditCondition('${ ui.pageLink("botswanaemr", "editCondition", [patientId: patient.uuid, returnUrl: returnUrl]) }',condition)"
                                                    ng-if="condition.voided===false">
                                                </i>
                                            </button>
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash" title="${ui.message("coreapps.delete")}"
                                                    ng-click="conditionConfirmation(conditionHistory.conditions)" ng-if="condition.voided===false">
                                                </i>
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </span>
                </div>

                <div id="remove-condition-dialog" class="dialog" style="display: none; position: absolute; left: 35%; top:30%;">
                    <div class="dialog-header">
                        <h3>${ ui.message("coreapps.conditionui.removeCondition") }</h3>
                    </div>
                    <div class="dialog-content">
                        <ul>
                            <li class="info">
                                <span id="removeConditionMessage">${ ui.message("coreapps.conditionui.removeCondition.message","")}</span>
                            </li>
                        </ul>
                        <button class="btn btn-sm btn-success right" type="submit" ng-click="removeCondition()">${ ui.message("general.yes") }</button>
                        <button class="btn btn-sm btn-danger" ng-click="cancelDeletion()">${ ui.message("general.no") }</button>
                    </div>
                </div>
                <div class="actions mt-5 mb-3">
                    <button class="btn btn-sm btn-danger"
                            onclick="location.href = '${ ui.encodeHtml(returnUrl) }'">${ui.message("coreapps.return")}</button>
                    <button id="conditionui-addNewCondition" class="btn btn-sm btn-success right pr-0"
                            onclick="location.href = '${ ui.pageLink("botswanaemr", "addCondition", [patientId: patient.uuid, returnUrl: returnUrl]) }'">
                            ${ui.message("coreapps.conditionui.addNewCondition")}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>