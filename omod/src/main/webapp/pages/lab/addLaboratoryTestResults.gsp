<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '#'},
        { label: "Add Lab Results"}
    ];
</script>

${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<script type="text/javascript">
    jq(function () {
        let postResults = function (params) {
            jq.getJSON('${ ui.actionLink("botswanaemr","lab/laboratoryTestOrders", "addTestResults")}', params)
                .success(function (data) {
                console.log(data);
                jq().toastmessage('showSuccessToast', "Patient's test results updated successfully");
                location.reload();
            })            
        }

        jq("#addLabResultsForm").submit(function(e){
            e.preventDefault();
            var params = {
                'patientId': jq('#patientId').val(),
                'labTestId': jq('#labTestId').val(),
                'labTestResults': jq('#labTestResults').val(),
                'labResultsInterpretation': jq('#labResultsInterpretation').val(),
                'labTestSampleCollectionDate': jq('#labTestSampleCollectionDate').val()
            };

            postResults(params);
        });

        jq(document).on('click', '#saveLabResults', function() {
             var params = {
                'patientId': jq('#patientId').val(),
                'labTestId': jq('#labTestId').val(),
                'labTestResults': jq('#pulledResult').text(),
                'labResultsInterpretation': jq('#labResultsInterpretation').val(),
                'labTestSampleCollectionDate': jq('#labTestSampleCollectionDate').val()
            };

            postResults(params);
        });

        let loadLabStatusModal = function(test, status, results) {
            let resultsHtml = results.map(result => `
                <div class="row">
                    <!-- <div class="col-md-6">\${result.testName}</div> -->
                    <div class="col-md-6">Result</div>
                    <div class="col-md-6" id="pulledResult">\${result.value}</div>
                </div>
            `).join('');

            let modalHtml = `
                <div class="modal fade" id="labStatusModal" tabindex="-1" role="dialog" aria-labelledby="labStatusModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="labStatusModalLabel">Lab Status</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6"><strong>Test:</strong> \${test}</div>
                                        <div class="col-md-6"><strong>Status:</strong> \${status}</div>
                                    </div>
                                    \${resultsHtml}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary" id="saveLabResults">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            `;

            if (jq('#labStatusModal').length) {
                jq('#labStatusModal .modal-body .container').html(`
                    <div class="row">
                        <div class="col-md-6"><strong>Test:</strong> \${test}</div>
                        <div class="col-md-6"><strong>Status:</strong> \${status}</div>
                    </div>
                    \${resultsHtml}
                `);
            } else {
                jq('body').append(modalHtml);
            }

            jQuery('#labStatusModal').modal('show');
        }

        jq("#check-results").click(function() {
            showLoadingOverlay();
            let taskId = jq(this).data('task-id');
            taskId = '${taskId}';
            // taskId = 'c463030f-235a-4182-8ca1-c4dfdb02b23f';
            jq.ajax({
                url: '${ ui.actionLink("botswanaemr", "search", "fetchLabResultsFromShr")}',
                type: 'GET',
                dataType: "json",
                global: false,
                data: {
                    taskId: taskId,
                },
                success: function(data) {
                    // Handle success response
                    console.log('Results fetched successfully:', data);
                    hideLoadingOverlay();
                    if (data.status.indexOf('200') > -1) {
                        if (data.response) {
                            /* Test data for modal */
                            /*
                            let test = 'CD4 Panel';
                            let status = 'Ready';
                            let results = [{'value': '1234'}];
                            loadLabStatusModal(test, status, results);
                            */

                            let test = jq('#testName').text();
                            let status = data.taskStatus;
                            let results = data.response;
                            loadLabStatusModal(test, status, results);                        

                        }
                    } else {
                        if (data.response) {
                            jq().toastmessage('showWarningToast', data.message);
                        } else {
                            jq().toastmessage('showErrorToast', "Error fetching results. Please try again later.");
                        }
                    }
                },
                error: function(xhr, status, error) {
                    hideLoadingOverlay();
                    jq().toastmessage('showErrorToast', "Error fetching results. Please try again later.");
                }
            });
        });

        jq().toastmessage({stayTime : 10000});
    });
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span>
                </div>
                <div class="col-3">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-1">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0 hidden">
                        <div class="row">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="col">Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
                        <div class="row">
                            <% if (labTest != null && labTest != "") { %>
                                <div class="col-12 pl-0 pr-0">
                                    <h5 class="text-primary text-left mt-3 pl-3">LAB TEST</h5>
                                    <hr class="divider pt-1 bg-primary ml-3"/>
                                    <div class="row">
                                        <% if (labTest.status != "COMPLETED") {%>
                                            <div class="col pr-0">
                                                <button type="button" class="btn btn-sm btn-success float-right" data-toggle="modal" data-target="#addLaboratoryResultsModal">Add Results</button>
                                            </div>
                                            <div class="col pr-0">
                                                <button type="button" class="btn btn-sm btn-info float-right" id="check-results" data-task-id="${taskId}" data-order-id="${orderId}"><i class="fa fa-refresh"></i> Check results online</button>
                                            </div>
                                        <% } %>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-bordered mt-2">
                                                <tbody>
                                                    <tr>
                                                        <td>Lab Test Name:</td>
                                                        <td class="text-left">
                                                            <span class="text-primary">
                                                                <u>
                                                                    <a href="#" data-toggle="tooltip" id="testName" title="Added on:&nbsp;${ui.format(labTest.dateAdded)}">
                                                                        ${labTest.testName}
                                                                    </a>
                                                                </u>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Test Status: </td>
                                                        <td class="text-left">
                                                            <% if(labTest.status == "COMPLETED") { %>
                                                                <i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Completed</span>
                                                            <% } else { %>
                                                                <i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>
                                                            <% } %>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6 mr-0 pr-0">
                                            <div class="table-responsive">
                                                <table class="table table-bordered mt-2">
                                                    <tbody>
                                                        <tr>
                                                            <td>Requested By:</td>
                                                            <td class="text-left">
                                                                <span class="text-primary">${labTest.requestedBy}</span>
                                                            </td>
                                                        </tr>
                                                        <tr class="hidden">
                                                            <td>Date Of Test: </td>
                                                            <td class="text-left">
                                                                <span>${labTest.dateAdded}</span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <% } else { %>
                                <div class="col">
                                    <table class="table table-bordered mt-4">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <h5 class="text-center text-warning">No test data captured!</h5>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <% } %>
                        </div>
                        
                        <div class="row">
                            <div class="col-12 pr-0">
                                <h5 class="text-primary text-left mt-3">RESULTS</h5>
                                <hr class="divider pt-1 bg-primary"/>
                                <div class="table-responsive mt-3">
                                    <table class="table table-bordered mt-2">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="bg-pale"><h6 class="text-dark">Test Name:</h6></th>
                                                <th scope="col" class="bg-pale"><h6 class="text-dark">Sample Collection Date:</h6></th>
                                                <th scope="col" class="bg-pale"><h6 class="text-dark">Results:</h6></th>
                                                <th scope="col" class="bg-pale"><h6 class="text-dark">Interpretation:</h6></th>
                                                <th scope="col" class="bg-pale"><h6 class="text-dark">Action:</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% if (!labTest.resultsList.isEmpty()) { %>
                                                <tr>
                                                    <td class="text-primary">
                                                        ${labTest.testName}
                                                    </td>
                                                    <% labTest.resultsList.each {key, value -> %>
                                                        <% if(key != null && key != "" && key == "Laboratory Sample Collection Date") { %>
                                                            <td>
                                                                <span class="text-muted">${value}</span>
                                                            </td>
                                                        <% } %>
                                                    <% } %>
                                                    <% labTest.resultsList.each {key, value -> %>
                                                        <% if (key != null && key != "" && key == "Laboratory Test Results") {%>
                                                            <td>
                                                                <span class="text-muted">${value}</span>
                                                            </td>
                                                        <% } %>
                                                    <% } %>
                                                    <% labTest.resultsList.each {key, value -> %>
                                                        <% if (key != null && key != "" && key == "Laboratory Test Results Interpretation") { %>
                                                            <td>
                                                                <span class="text-muted">${value}</span>
                                                            </td>
                                                        <% } %>
                                                    <% } %>
                                                </tr>
                                            <% } else { %>
                                                <tr>
                                                    <td colspan="5">
                                                        <h5 class="text-center text-warning">No test results data captured. Add data by saving results form!</h5>
                                                    </td>
                                                </tr>
                                            <% } %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Test Results Modal -->
<% if (labTest != null && labTest != "") { %>
    <div class="modal fade" id="addLaboratoryResultsModal" tabindex="-1"
         data-controls-modal="addLaboratoryResultsModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="addLaboratoryResultsModalTitle">

        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="addLaboratoryResultsModalTitle">Add Results</h4>
                    <button type="button" id="closeModal" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="addLabResultsForm">
                        <input type="hidden" id="patientId" name="patientId" value="${labTest.patientId}" class="form-control"/>
                        <input type="hidden" id="labTestId" name="labTestId" value="${labTest.testId}" class="form-control">
                        <table class="table table-bordered mt-2">
                            <tbody>
                            <tr>
                                <td class="pt-3">Lab Test Name:</td>
                                <td class="text-left">
                                    <input id="labTestName" style="border: 0;" name="labTestName" readonly="readonly" value="${labTest.testName}">
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="form-group">
                            <label>Sample Collection Date
                                <span class="text-danger">*</span>
                            </label>
                            <input type="date"
                                   class="form-control"
                                   id="labTestSampleCollectionDate"
                                   name="labTestSampleCollectionDate"
                                   required
                                   placeholder="Sample Collection Date"/>
                        </div>

                        <div class="form-group">
                            <label>Results
                                <span class="text-danger">*</span>
                            </label>
                            <input type="text"
                                   class="form-control"
                                   id="labTestResults"
                                   name="labTestResults"
                                   required
                                   placeholder="Results"/>
                        </div>

                        <div class="form-group hidden">
                            <label>Interpretation
                            </label>
                            <textarea id="labResultsInterpretation"
                                      class="form-control"
                                      name="labResultsInterpretation"></textarea>
                        </div>

                        <div class="row">
                            <div class="col pr-0">
                                <div class="col pr-0">
                                    <button type="submit"
                                            class="btn btn-sm btn-primary float-right ml-1">Save
                                    </button>
                                </div>
                                <button type="button"
                                        class="btn btn-sm btn-dark bg-dark float-right"
                                        data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<% } %>
