<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/lab/groupedLaboratoryOrders.page'
        },
        {label: "Lab Orders"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div id="laboratoryOrders" class="col">
                    ${ui.includeFragment("botswanaemr", "lab/groupedLaboratoryTestOrders")}
                </div>
            </div>
        </div>
    </div>
</div>
