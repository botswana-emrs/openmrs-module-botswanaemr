<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Lab Quality Control"}
    ];
</script>

<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#labQualityControlsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
            "oPaginate": {
                "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
            }
            },
            "order": [[0, "desc"]]
        });

        jq('#labQualityControlsTable').on('click', "a.delete", function (e) {
            e.preventDefault();
            let param = {
                'labQualityControlId': jQuery(this).attr('data-overlay')
            };
            jq('.dialog-instructions').text('Are you sure you want to delete this lab Quality Control?');
            const deleteQualityControlDialog = emr.setupConfirmationDialog({
                selector: '#confirm-delete-dialog',
                actions: {
                    confirm: function () {
                        jq.getJSON('${ui.actionLink("botswanaemr", "lab/laboratoryQualityControlForm", "retireLabQualityControl")}', param)
                            .success(function (data) {
                                emr.successMessage("Lab Quality Control deleted successfully!");
                                location.href = '${ui.pageLink("botswanaemr", "lab/labQualityControl")}';
                            })
                            .fail(function (err) {
                                    emr.errorMessage(err);
                                    console.log(err)
                                }
                            )
                        console.log("yes");
                    },
                    cancel: function () {
                        console.log("no");
                    }
                }
            });
            deleteQualityControlDialog.show();
        });
    });
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <h5 class="text-primary text-uppercase">Lab Quality Control</h5>
                    </div>
                    <div class="col pr-0 mr-0">
                        <a href="${ui.pageLink('botswanaemr', 'lab/labQualityControlForm')}" class="btn btn-sm btn-primary text-white float-right">
                            New Entry
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="labQualityControlsTable"
                       class="table table-striped table-sm table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Operator</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <% simplifiedLabQualityControlList.each { %>
                        <tr>
                            <td>${it.date}</td>
                            <td>${it.operator}</td>
                            <td>
                                <% if (it.published) { %>
                                    Published
                                <% } else { %>
                                    Draft
                                <% } %>
                            </td>
                            <td>
                                <a href="/${ui.contextPath()}/botswanaemr/lab/labQualityControlForm.page?labQualityControlId=${it.id}" class="color-primary">View</a> |
                                <% if (!it.published) { %>
                                    <a href="/${ui.contextPath()}/botswanaemr/lab/labQualityControlForm.page?labQualityControlId=${it.id}" class="color-primary">Edit</a> |
                                    <a href="#" class="color-primary delete" data-overlay="${it.id}">Delete</a>
                                <% } %>
                            </td>
                        </tr>
                    <% } %>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                
            </div>
        </div>
    </div>
</div>
<div id="confirm-delete-dialog" class="alert alert-info" role="alert" style="display: none">
    <div class="dialog-header">
        <div class="row bg-info">
            <h4 class="text-white text-bolder">
                <i class="fa fa-info-circle fa-1x pl-2"></i>
                ${ui.message("Delete confirmation")}
            </h4>
        </div>
    </div>
    <div class="dialog-content">
        <p class="dialog-instructions py-4">
            ${ui.message("Are you sure you want to delete this record?")}
        </p>
        <br/>
        <div class="buttons">
            <button type="button" class="btn btn-sm btn-dark bg-dark cancel" data-dismiss="modal">
                ${ui.message("Close")}
            </button>
            <button class="confirm btn btn-sm btn-primary right ml-1">${ui.message("Delete")}
            </button>
        </div>
    </div>
</div>