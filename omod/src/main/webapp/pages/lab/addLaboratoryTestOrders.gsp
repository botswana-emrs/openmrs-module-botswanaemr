<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '#'},
        { label: "Add Lab Orders"}
    ];
</script>

<script type="text/javascript">
    let params = new URLSearchParams(window.location.search);
    let patientId = params.get('patientId');
    jq(function() {

        let requiredTests = [];
        /* jq("#labOrder").autocomplete({
            source: function(request, response) {
                jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTests") }', {
                    query: request.term
                }).success(function(data) {
                    var results = [];
                    for (var i in data) {
                        var result = {
                            label: data[i].name,
                            value: data[i].id
                        };
                        results.push(result);
                    }
                    response(results);
                });
            },
            minLength: 3,
            select: function(event, ui) {
                event.preventDefault();
                jq(this).val(ui.item.label);
                jq("#list").append('<li id="' + ui.item.value + '">' + ui.item.label + '<button type="button" onclick="deleteItem(' + ui.item.value + ')" class="delete btn btn-sm btn-outline border-0 bg-transparent"><i class="fa fa-times text-danger"></i></button></li>');
                requiredTests.push(ui.item.value);
                jq('#labOrder').focus();
                jq('#labOrder').val('');
                jq('#task-tests').show();
            },
            open: function() {
                jq(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                jq(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
        */
        let isConsultation = `${isConsultation}`;

        jq("#addLabOrderForm").submit(function(e) {
            e.preventDefault();
            if (jq('#orderingSite').val() == '') {
                jq().toastmessage('showErrorToast', "Please select Lab site to proceed");
                jq('#orderingSite').focus();
                return;
            }
            // requiredTests is empty return with an alert message
            if (requiredTests.length == 0) {
                jq().toastmessage('showErrorToast', "Please select and add lab tests to proceed");
                return;
            }
            jq("#testList").val(requiredTests);
            var params = {
                'patientId': <%= patient.id %> ,
                'labList': jq('#testList').val(),
                'requestedBy': jq('requestedBy').val(),
                'dateOfLastTest': jq('#dateOfLastTest').val(),
                'orderingSiteId': jq('#orderingSite').val(),
                'locationId': ${location.id}
            };
            // add encounter id to the params if it exists
            <%
             if (encounter) {
            %>
                params.encounterId = ${encounter.id};
            <%
                }
             %>
            showLoadingOverlay();
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "addLabOrders")}', params)
                .done(function(data) {
                    jq().toastmessage('showNoticeToast', "Lab orders captured successfully");
                    if (isConsultation) {
                        setTimeout(() => {
                            getLabTestsForPatient();
                            hideLoadingOverlay();
                            history.back();
                        }, 1500);
                    } else {
                        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/lab/addLaboratoryTestOrdersPool.page";
                    }
                })
            .fail(function(xhr, status, error) {
                jq().toastmessage('showErrorToast', "Failed to capture lab orders. Please try again.");
                console.error("Error:", status, error);
            })
            .always(function() {
                hideLoadingOverlay();
            });
        });

        jq("#removeLabOrderForm").click(function(e) {
            var params = {
                'testId': jq('#testId').val()
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "lab/laboratoryTestOrders", "removeLabTest")}', params).success(function(data) {
                jq().toastmessage('showNoticeToast', "Lab order removed successfully!");
            })
        });

        const searchSystemUsers = function (request, response) {
            jq.getJSON(
                '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/user?v=full&q=' + request.term,
                function (data) {
                    let results = data.results.map(function (user) {
                        let label = user.person && user.person.display ? user.person.display : user.display;
                        let value = user.person.uuid;

                        return {
                            label: label,
                            value: value
                        };
                    });
                    response(results)
                });
        };
        const setupUserAutocomplete = function (fieldIdentifier) {
            let textField = jq(fieldIdentifier);

            textField.autocomplete({
                source: searchSystemUsers,
                // select: selectItem,
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(this).attr('data-overlay', ui.item.value);
                    jq(this).val(ui.item.label);
                }
            });
        }

        // Set autocomplete for system users
        setupUserAutocomplete('#requestedBy');

        let filterLabTests = function(filter) {
            jq("#lab-tests ul li").each(function() {
                if (!jq(this).find('input').attr('data-overlay')) {
                    return;
                }
                let test = jq(this).text().toLowerCase();
                jq(this).toggle(test.indexOf(filter) > -1);
            });
        }

        jq("#labTestFilter").keyup(function() {
            let filter = jq(this).val().toLowerCase();
            filterLabTests(filter);
        });

        let filterLabProfiles = function(filter) {
            jq("#lab-profile ul li").each(function() {
                let profileName = jq(this).text().toLowerCase();
                jq(this).toggle(profileName.indexOf(filter) > -1);
            });
        }

        jq("#labProfileFilter").keyup(function() {
            let filter = jq(this).val().toLowerCase();
            filterLabProfiles(filter);
        });

        // For each lab profile, set a click event listener to update the autocomplete source for the labOrder field the corresponding lab panels. Should support the ability to select multiple lab panels
        jq("#lab-profile input[type='checkbox']").click(function() {
            // get all selected lab panels and convert to comma separated string
            let labPanelIds = jq("#lab-profile input[type='checkbox']:checked").map(function() {
                return jq(this).attr('id');
            }).get().join(',');

            if (labPanelIds === '') {
                jq(".common-input").addClass('hidden');
            } else {
                jq(".common-input").removeClass('hidden');
            }

            // make ajax call to get lab tests for selected lab panels
            jq.ajax({
                type: 'GET',
                url: '${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTestsFromPanel") }',
                dataType: "json",
                data: {
                    query: '',
                    labPanelIds: labPanelIds
                },
                success: function(data) {
                    // For each lab test returned, create a list-item entry in the lab-tests div. Each entry is a clone of the first list-item entry
                    // jq("#lab-tests ul").empty();
                    data.forEach(function(test) {
                        // If lab test already exists in the list, do not add it again
                        if (jq("#lab-tests ul li input[id='" + test.uuid + "']").length > 0) {
                            return;
                        }
                        let listItem = jq("#lab-tests ul li.tmpl").clone();
                        listItem.removeClass('tmpl hidden');
                        listItem.find('input').attr('id', test.uuid);
                        listItem.find('input').attr('name', test.name);
                        listItem.find('input').attr('data-overlay', test.panelId);
                        listItem.find('label').attr('for', test.uuid);
                        listItem.find('label').text(test.name);
                        jq("#lab-tests ul").append(listItem);
                    });
                    // Empty the tests list for panels that have not been selected. This is to ensure that the tests list is updated when a panel is unchecked
                    jq("#lab-profile input[type='checkbox']").each(function() {
                        if (!jq(this).prop('checked')) {
                            // remove the parent list item if it is not hidden
                            let parent = jq("#lab-tests ul li input[data-overlay='" + jq(this).attr('id') + "']").parents('li');
                            if (!parent.hasClass('hidden')) {
                                parent.remove();
                            }
                        }
                    });
                    filterLabTests(jq("#labTestFilter").val().toLowerCase());
                }
            });
            // When select all is checked, select all lab tests and vice versa when unchecked
            jq("#select-all-check").click(function() {
                jq("#lab-tests ul li input").prop('checked', jq(this).prop('checked'));
            });

            jq("#labOrder").autocomplete({
                source: function (request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTestsFromPanel") }', {
                        query: request.term,
                        labPanelIds: labPanelIds
                    }).success(function (data) {
                        let results = [];
                        for (let i in data) {
                            let result = {
                                label: data[i].name,
                                value: data[i].id
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(this).val(ui.item.label);
                    jq("#list").append('<li id="' + ui.item.value + '">' + ui.item.label + '<button type="button" onclick="deleteItem("' + ui.item.value + '")" class="delete btn"><i class="fa fa-times text-danger"></i></button></li>');
                    requiredTests.push(ui.item.value);
                    jq('#labOrder').focus();
                    jq('#labOrder').val('');
                    jq('#task-tests').show();
                },
                open: function () {
                    jq(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                },
                close: function () {
                    jq(this).removeClass("ui-corner-top").addClass("ui-corner-all");
                }
            });

            jq("#addAllTests").click(function() {
                // if no tests are selected, display a toast message to the user and return
                if (jq("#lab-tests ul li input:checked").length === 0) {
                    jq().toastmessage('showErrorToast', "No tests selected. Please select tests to add");
                    return;
                }
                jq("#lab-tests ul li input:checked:visible").each(function() {
                    let testId = jq(this).attr('id');
                    let testName = jq(this).attr('name');
                    if (requiredTests.indexOf(testId) === -1) {
                        requiredTests.push(testId);
                        jq("#list").append('<li id="' + testId + '">' + testName + '<button type="button" onclick="deleteItem(' + testId + ')" class="delete btn"><i class="fa fa-times text-danger"></i></button></li>');
                        jq('#task-tests').show();
                    }
                });
            });

        });

        // Add event listener to remove test button from the lablist container
        jq("#task-tests").on('click','button',function() {
            let testId = jq(this).parent().attr('id');
            let index = requiredTests.indexOf(testId);
            if (index > -1) {
                requiredTests.splice(index, 1);
            }
            jq(this).parent().remove();
            if (requiredTests.length === 0) {
                jq('#task-tests').hide();
            }
        });

    });

    document.addEventListener("pageshow", function(event) {
        if (event.persisted) {
            getLabTestsForPatient();
        }
    });

    function getLabTestsForPatient() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTestsForPatient")}',
        { 'patientId': patientId})
        .done(function(data) {
            renderLabTests(data);
        }).fail(function(jqXHR) {
            console.error('Error loading lab order data');
        });
    } 

    function deleteItem(itemId) {
        let itemToRemove = document.getElementById(itemId);
        itemToRemove.parentNode.removeChild(itemToRemove);
    }

</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-6">
                    Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span>
                </div>
                <div class="col-6">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-1">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0 hidden">
                        <div class="row">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="col">Client Profile:<span class="text-primary bg-transparent"> ${patient.familyName}&nbsp;${patient.givenName}</span></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}
                        <form method="post" id="addLabOrderForm">
                            <div class="row">
                                <div class="col ml-0">
                                    <div class="form-group">
                                        <label>Lab Site:
                                                        <span class="text-danger">*</span>
                                        </label>
                                        <select class="form-control" id="orderingSite" name="orderingSite">
                                            <option value="">Select Lab Site</option>
                                            <% labOrderingSites.each { site -> %>
                                            <option value="${site.uuid}">${site.name}</option>
                                            <% } %>
                                        </select>                                        
                                    </div>
                                </div>
                                <div class="col ml-0">
                                    <div class="form-group">
                                        <label>Requested By:</label>
                                        <input class="form-control" id="requestedBy" value="${currentUser}"  name="requestedBy">
                                    </div>
                                </div>
                            </div>
                            <div class="row hidden">
                                <div class="col pr-0">
                                    <div class="form-group">
                                        <label>Date Of Test:</label>
                                        <input type="text"
                                               class="form-control py-2"
                                               id="dateOfLastTest"
                                               name="dateOfLastTest"
                                               placeholder="dd-mm-yyyy">
                                        <script type="text/javascript">
                                            jQuery('#dateOfLastTest').datepicker({
                                                changeMonth: true,
                                                changeYear: true,
                                                showButtonPanel: true,
                                                "setDate": new Date(),
                                                dateFormat: "dd-mm-yy",
                                                yearRange: "-150:+0",
                                                maxDate: 0,
                                                "autoclose": true,
                                                onSelect: function (data) {
                                                    jQuery("#dateOfLastTest").trigger('change');
                                                }
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col pr-0"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 pl-0 pr-0">
                                    <h5 class="text-primary text-left mt-3 pl-3">LAB PROFILE</h5>
                                    <hr class="divider pt-0"/>
                                    <div class="row">
                                        <div class="col-md-12" id="lab-profile">
                                            <div class="col-md-12 p-0">
                                                <div class="form-group row mb-0">
                                                    <label class="col-md-4 col-form-label" for="labOrder">Select Profile
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control mb-2" id="labProfileFilter" name="labProfileFilter" placeholder="Filter by profile name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <ul class="list-group list-group-light">
                                                    <%
                                                        labPanels.each { panel ->
                                                            def panelName = panel.name
                                                            def panelId = panel.uuid
                                                            def parentProfileId = panel.parentProfileId
                                                    %>
                                                    <li class="list-group-item p-0 p-l-4">
                                                        <div class="form-inline mr-1">
                                                            <input type="hidden" name="parentProfile" value="${parentProfileId}"/>
                                                            <input class="form-check-input form-check-inline" type="checkbox" value=""  id="${panelId}" name="${panelId}" data-overlay="${parentProfileId}" />
                                                            <label class="form-check-label pb-2" for="${panelId}">${panelName}</label>
                                                        </div>
                                                    </li>
                                                    <% } %>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- As section for loading lab tests corresponding to the selected profiles-->
                                <div class="col-lg-6 col-md-12 pl-0 pr-0">
                                    <h5 class="text-primary text-left mt-3 pl-3">LAB PANELS</h5>
                                    <hr class="divider pt-0"/>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-12" style="max-height: 400px; overflow-y: scroll">
                                                    <ul class="list-group list-group-light">
                                                        <li class="list-group item p-0 p-l-4 hidden common-input">
                                                            <div class="form-inline mr-1">
                                                                <label for="labTestFilter"></label>
                                                                <input type="text" class="form-control mb-2" id="labTestFilter" name="labTestFilter" placeholder="Filter by test name" />
                                                            </div>
                                                        </li>
                                                        <li class="list-group-item p-0 p-l-4 hidden common-input">
                                                            <div class="form-inline mr-1">
                                                                <input class="form-check-input form-check-inline lab-test" type="checkbox" value=""  id="select-all-check" name="select-all" />
                                                                <label class="form-check label pb-2" for="select-all-check"><span class="fw-bolder">Select all</span></label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-12" id="lab-tests" style="max-height: 400px; overflow-y: scroll">
                                                    <ul class="list-group list-group-light">
                                                        <li class="list-group-item p-0 p-l-4 tmpl hidden">
                                                            <div class="form-inline mr-1">
                                                                <input class="form-check-input form-check-inline lab-test" type="checkbox" value=""  id="lab-test" name="lab-test" />
                                                                <label class="form-check-label pb-2" for="lab-test">test</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-12">
                                                    <ul class="list-group list-group-light">
                                                        <li class="list-group-item p-0 p-l-4 hidden common-input">
                                                            <div class="form-inline mr-1">
                                                                <button type="button" class="btn btn-sm btn-outline bg-primary text-white border-0 float-end" id="addAllTests">Add all selected tests</button>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 pl-0 pr-0">
                                    <h5 class="text-primary text-left mt-3 pl-3">SELECTED LAB TESTS</h5>
                                    <hr class="divider pt-0 bg-primary ml-3"/>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="task-tests" class="tasks hidden">
                                                <fieldset class="col">
                                                    <legend  class="w-auto">Selected Lab Tests</legend>
                                                    <ul id="list" class="selected-lab-tests" name="labList"></ul>
                                                    <br />
                                                </fieldset>
                                            </div>
                                            <input type="hidden" id="testList" name="testTest" />
                                        </div>
                                        <div class="col-md-6 mr-0 pr-0">
                                            <div class="table-responsive">
                                                <table class="table table-bordered mt-2">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">Test Name:</h6></th>
                                                            <th scope="col" class="bg-pale"><h6 class="text-dark">Action:</h6></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <% if (labTests) { %>
                                                            <% labTests.each { %>
                                                                <tr>
                                                                    <td class="text-primary text-uppercase">
                                                                        <u>
                                                                            <a href="#" data-toggle="tooltip" title="Added on:&nbsp;${ui.format(it.dateAdded)}">
                                                                                ${it.testName}
                                                                            </a>
                                                                        </u>
                                                                    </td>
                                                                    <td class="text-danger">
                                                                        <form method="post" id="removeLabOrderForm">
                                                                           <input type="hidden" id="testId" value="${it.testId}" name="testId">
                                                                           <button id="removeTestOrderBtn" type="button" id="removeTestButton" class="btn btn-sm btn-outline bg-transparent text-danger border-0" name="removeTest">Remove</button>
                                                                        </form>
                                                                    </td>
                                                                </tr>
                                                            <% } %>
                                                        <% } else { %>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <h5 class="text-center text-warning">No data captured. Add data by saving tests form!</h5>
                                                                </td>
                                                            </tr>
                                                        <% } %>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer pl-0 pr-0">
                                <button type="submit" class="btn btn-md btn-block btn-primary" id="addLabOrderBtn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
