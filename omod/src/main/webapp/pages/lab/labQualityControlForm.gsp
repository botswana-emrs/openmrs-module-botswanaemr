<%
    ui.includeCss("botswanaemr", "bootstrapMultiselect.css")
    ui.includeJavascript("botswanaemr", "bootstrapMultiselect.js")
    ui.includeJavascript("botswanaemr", "stockutils.js")
%>
<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/hts/htsDashboard.page'},
        { label: "Lab Quality Control Form"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <h5 class="text-primary text-uppercase">Lab Quality Control Form</h5>
                    </div>
                </div>
            </div>
            ${ui.includeFragment("botswanaemr", "lab/laboratoryQualityControlForm")}
        </div>
    </div>
</div>