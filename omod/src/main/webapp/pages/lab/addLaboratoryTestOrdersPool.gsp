<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '#'},
        { label: "Add Laboratory Tests Pool"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-9">
                                <h5>Add Laboratory Tests Pool</h5>
                            </div>
                            <div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl, 'action': 'self'])}" class="btn btn-primary text-white mr-0 float-right">
                                    <i class="fa fa-user-plus"></i> Register patient
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="htsPatientPool" class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "lab/laboratoryTestOrdersPool")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
