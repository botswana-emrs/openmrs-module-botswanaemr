<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [ title: ui.message("Payments Portal") ])
    ui.includeJavascript("appui", "paymentSearch.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {label: "${ ui.message("Payments Portal") }"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-payment-history-tab"
                        data-toggle="pill" href="#pills-payment-history"
                        role="tab" aria-controls="pills-payment-history"
                        aria-selected="true">
                            <img src="${ui.resourceLink('botswanaemr', 'images/icons8-overview-32.png')}" /> Overview
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-delayed-payments-tab"
                        data-toggle="pill" href="#pills-delayed-payments"
                        role="tab" aria-controls="pills-delayed-payments"
                        aria-selected="false">
                            <img src="${ui.resourceLink('botswanaemr', 'images/icons8-payment-delay.png')}" /> Delayed payments
                    </a>
                </li>
                <% if (!isRegistration) { %>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-service-provider-tab"
                        data-toggle="pill" href="#pills-service-provider"
                        role="tab" aria-controls="pills-service-provider"
                        aria-selected="false">
                            <img src="${ui.resourceLink('botswanaemr', 'images/icons8-mobile-32.png')}" /> Service Provider
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-payment-methods-tab"
                        data-toggle="pill" href="#pills-payment-methods"
                        role="tab" aria-controls="pills-payment-methods"
                        aria-selected="false">
                            <img src="${ui.resourceLink('botswanaemr', 'images/icons8-card-security-32.png')}" /> Payment Methods
                    </a>
                </li>
                <% } %>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-payment-history"
                    role="tabpanel" aria-labelledby="pills-payment-history-tab">
                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-12">
                            ${ ui.includeFragment("botswanaemr", "admin/paymentHistory") }
                        </div>
                    </div>
                </div>
            <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-delayed-payments"
                    role="tabpanel" aria-labelledby="pills-delayed-payments-tab">
                    <div class="row justify-content-center">
                        <div class="col col-sm-12 col-md-12 col-lg-12">
                            ${ ui.includeFragment("botswanaemr", "admin/delayedPayment") }
                        </div>
                    </div>
                </div>
                <!-- 3rd tab -->
                <div class="tab-pane fade" id="pills-service-provider"
                    role="tabpanel" aria-labelledby="pills-service-provider-tab">
                    <div class="row justify-content-center">
                        <div class="col col-sm-12 col-md-12 col-lg-10">
                             ${ ui.includeFragment("botswanaemr", "admin/serviceProvider") }
                        </div>
                    </div>
                </div>
                <!-- 4th tab -->
                <div class="tab-pane fade" id="pills-payment-methods"
                    role="tabpanel" aria-labelledby="pills-payment-methods-tab">
                    <div class="row justify-content-center">
                        <div class="col col-sm-12 col-md-12 col-lg-10">
                            ${ ui.includeFragment("botswanaemr", "admin/paymentMethod") }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
