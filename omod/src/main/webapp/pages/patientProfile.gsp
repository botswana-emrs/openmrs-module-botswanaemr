<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("botswanaemr", "utils/countries.js")
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.includeJavascript("botswanaemr", "utils.js")
    ui.includeJavascript("botswanaemr", "payment-history.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex {attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size()-1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }
%>

<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
    
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }" ,
        link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'}
    ];
    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
        breadcrumbs.push({ label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>
    jq(function(){
        // Persist already set patient names
        namesSet = true;
        jq(".tabs").tabs();
        jq(document).on('sessionLocationChanged', function() {
            window.location.reload();
        });
    });
    var patient = { id: ${ patient.id } };

    jQuery.noConflict(false);

</script>

<% if(includeFragments){
    includeFragments.each {
        def configs = [:];
        if(it.extensionParams.fragmentConfig != null){
            configs.putAll(it.extensionParams.fragmentConfig);
        }
        configs.patient = patient; %>
        ${ui.includeFragment(it.extensionParams.provider, it.extensionParams.fragment, configs)}
    <%}
}
%>
<script>
  jq(function() {
      function getUrlParameter(param) {
          let pageUrl = window.location.search.substring(1);
          let urlVariables = pageUrl.split('&');
          for (let i = 0; i < urlVariables.length; i++) {
              let paramName = urlVariables[i].split('=');
              if (paramName[0] === param) {
                  return paramName[1];
              }
          }
      }

      if (!getUrlParameter("proceed")) {
          jq("#proceed").hide();
      }

    jq("#lab-test-left-menu").on("click", ".lab-summary", function () {
      jq("#lab-detail").empty();
      jq("#lab-detail").html("<i class=\"icon-spinner icon-spin icon-2x pull-left\"></i>")
      var acceptedDate = jq(this);
      var labIdnt = jq(acceptedDate).find(".lab-idnt").val();

      jq(".lab-summary").removeClass("selected");
      jq(acceptedDate).addClass("selected");

      jq.getJSON('${ui.actionLink("botswanaemr", "search", "getLabSummary")}',
          {'Id': labIdnt}
      ).success(function (data) {
        let labDetailPanel = jq("#lab-detail");
        labDetailPanel.html("");
        data.forEach(function(labDetail){
            for (var prop in labDetail) {
                labDetailPanel.append(jq("<P/>").append(stringUtils.toSentenceCase(prop) + ": "  +  labDetail[prop]));
            }
        });

        console.log(data);

      })
    });

    jq("#proceed").on("click", function (e) {
        e.preventDefault();
        let url = "${ui.pageLink("botswanaemr", "capturePayment", [patientId: patient.patient.id])}";
        window.location.href = url;
    })
    
    jq("#printRegistration").on("click", function (e) {
        e.preventDefault();
        let url = "${ui.pageLink("botswanaemr", "printRegistrationSummary", [patientId: patient.patient.id, visitId: activeVisit.id])}";
        window.location.href = url;
    })

  });
</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>

<% if(currentServicePoint.equals(doctorsPortal)){%>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="row">
                    <div class="col-9">
                        <p>Patient profile:</p>
                    </div>
                    <div class="col-3">
                        ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                    </div>
                </div>
            </div>
        </div>
    </div>
<% } %>

<div class="row">
    <div class="col">
        <div class="card px-0">
            <div class="row">
                <div class="col col-sm-12 col-md-12 col-lg-3 pl-2">
                    <span class="text-primary">
                        <i class="fa fa-user"></i> NAME:
                            ${patient.patient.person.personName.givenName}&nbsp;${patient.patient.person.personName.familyName}
                    </span>
                </div>
                <div class="col col-sm-12 col-md-12 col-lg-3 pl-0">
                    <span>
                        ${identifierType}<%if (identifierValue){%>:<%}%>&nbsp<span class="text-danger">${identifierValue}</span>
                    </span>
                </div>

                <div class="col col-sm-12 col-md-12 col-lg-3 pl-0">
                    <span class="text-success">
                        <i class="fa fa-id-badge"></i> Patient ID: ${pin}
                    </span>
                </div>

                <div class="col col-sm-12 col-md-12 col-lg-3 pl-0">
                    <button type="button" id="proceed" class="btn btn-sm btn-primary float-right">
                        Continue registration >>
                    </button>
                    <button type="button" id="printRegistration" class="btn btn-dark bg-dark btn-sm text-white" alt="Print Registration Summary">
                        <i class="fa fa-print text-white"></i>
                    </button>
                </div>


            </div>
        </div>
        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-patient-info-tab"
                        data-toggle="pill" href="#pills-patient-info"
                        role="tab" aria-controls="pills-patient-info"
                        aria-selected="true"> Patient Information
                    </a>
                </li>
                <% if(!isRegistration) { %>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-info-tab"
                        data-toggle="pill" href="#pills-medical-info"
                        role="tab" aria-controls="pills-medical-info"
                        aria-selected="false"> Basic Medical Information
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-medical-history-tab"
                        data-toggle="pill" href="#pills-medical-history"
                        role="tab" aria-controls="pills-medical-history"
                        aria-selected="false"> Medical History
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-payment-history-tab"
                        data-toggle="pill" href="#pills-payment-history"
                        role="tab" aria-controls="pills-payment-history"
                        aria-selected="false"> Payment History
                    </a>
                </li>
                <li class="nav-item">
                    <a  class="nav-link" id="pills-lab-history-tab"
                        data-toggle="pill" href="#pills-lab-history"
                        role="tab" aria-controls="pills-lab-history"
                        aria-selected="false"> Lab History
                    </a>
                </li>
                <% } %>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3" ng-app="triageDataApp" ng-controller="TriageDataController">
                <!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-patient-info"
                     role="tabpanel" aria-labelledby="pills-patient-info-tab">
                     ${ ui.includeFragment("botswanaemr", "patientInformation", [ patientId: patient.patient.uuid ]) }
                     ${ ui.includeFragment("botswanaemr", "nextOfKinInformation") }
                </div>
                <% if(!isRegistration) { %>
                    <!-- 2nd tab -->
                    <div class="tab-pane fade" id="pills-medical-info"
                        role="tabpanel" aria-labelledby="pills-medical-info-tab">
                        <div class="row">
                            <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                               <div class="card bg-light mb-3">
                                    ${ ui.includeFragment("botswanaemr", "allergies", [ patientId: patient.patient.uuid ]) }
                               </div>
                            </div>
                            <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                               <div class="card bg-light mb-3">
                                    ${ ui.includeFragment("botswanaemr", "conditions", [ patientId: patient.patient.uuid ]) }
                               </div>
                            </div>
                        </div>
                    </div>
                    <!-- 3rd tab -->
                    <div class="tab-pane fade" id="pills-medical-history"
                        role="tabpanel" aria-labelledby="pills-medical-history-tab">
                        ${ui.includeFragment("botswanaemr", "patientMedicalHistory")}
                    </div>
                    <!-- 4th tab -->
                    <div class="tab-pane fade" id="pills-payment-history"
                        role="tabpanel" aria-labelledby="pills-payment-history-tab">
                        ${ui.includeFragment("botswanaemr", "admin/paymentHistory")}
                    </div>
                    <!-- 5th tab -->
                    <div class="tab-pane fade" id="pills-lab-history"
                         role="tabpanel" aria-labelledby="pills-lab-history-tab">
                        ${ui.includeFragment("botswanaemr", "consultation/labHistory")}
                    </div>
                <% } %>
            </div>
        </div>
    </div>
</div>
