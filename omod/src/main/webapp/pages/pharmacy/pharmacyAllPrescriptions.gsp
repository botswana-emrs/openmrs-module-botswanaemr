<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'
        },
        {label: "Prescriptions"}
    ];
</script>

<div class="row">
    <div class="col">
        <div id="pharmacyPrescriptions" class="card">
            ${ ui.includeFragment("botswanaemr", "pharmacy/pharmacyPrescriptions")}
        </div>
    </div>
</div>
