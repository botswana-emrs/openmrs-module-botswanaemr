<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'
        },
        {label: "Print All Prescription Labels"}
    ];

    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div class="row">
    <div class="col-md-9 pl-3">
        <button id="print" onclick="printContent('#labelStrip');" class="btn btn-primary float-left">
            <i class="fa fa-print fa-1x"></i> Print Labels
        </button>
    </div>
    <div class="col-md-3 justify-content-end">
        ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
    </div>
</div>

<div class="row">
    ${ ui.includeFragment("botswanaemr", "pharmacy/printAllPrescriptionLabelDetails") }
</div>