<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
    throw new IllegalStateException("Logged-in user is not a Provider")
}
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'
        },
        {label: "Print Prescriptions"}
    ];

    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div class="row">
    <div class="col-md-9 pl-3">
        <button id="print" onclick="printContent('#prescriptionsCard');" class="btn btn-primary float-left">
            <i class="fa fa-print fa-1x"></i> Print Prescriptions
        </button>
    </div>
    <div class="col-md-3 justify-content-end">
        ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
    </div>
</div>

<div class="row">
    <div class="col">
        <div id="prescriptionsCard" class="card">
            <h2 class="text-primary text-center">${facility}</h2>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <ul class="list-unstyled pl-5">
                        <li>${creatorNames}</li>
                        <li>${creatorAddress}</li>
                    </ul>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>PH: <u style="text-decoration:underline dotted">${creatorPhoneNumber}</u></li>
                        <li>EM: ${creatorEmail}</li>
                    </ul>
                </div>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <ul class="list-unstyled pl-5">
                        <li>Patient Name: ${patientNames}</li>
                        <li>Address: ${patientAddress}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>Age: ${patientAge}</li>
                        <li>Date: ${datePrescribed}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-unstyled pr-5">
                        <li>Sex: ${patientGender}</li>
                    </ul>
                </div>
            </div>
            <hr class="divider bg-primary"/>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <ul class="list-unstyled pl-5">
                        <% patientDrugOrders.each{ %>
                            <li>
                                <div class="row">
                                    <div class="col pl-0">Drug: ${it.drug} </div>
                                    <div class="col pl-0">Refills: ${it.totalRefills}</div>
                                    <div class="col pl-0">Duration: ${it.refillDuration}</div>
                                    <div class="col pl-0">Dose: ${it.dosage}</div>
                                    <div class="col pl-0">Route: ${it.route}</div>
                                    <div class="col pl-0">Frequency: ${it.frequency}</div>
                                    <div class="col pl-0">Dosing Instructions: ${it.dosingInstructions ? it.dosingInstructions : 'No instructions provided'}</div>
                                </div>
                            </li>
                        <% } %>
                    </ul>
                </div>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row justify-content-center mt-4">
                <div class="col-md-3">
                    <ul class="list-unstyled pl-5">
                        <li>Health Practitioner's Signature:</li>
                    </ul>
                </div>
                <div class="col-md-9 pl-0 pr-0">
                    <hr style="border-bottom: 1px dotted #eee;"/>
                </div>
            </div>
        </div>
    </div>
</div>
