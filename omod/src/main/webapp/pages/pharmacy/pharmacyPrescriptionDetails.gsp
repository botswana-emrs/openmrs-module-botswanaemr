<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex {attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
                if (breadCrumbsDetails.size()-1 != index) {
                    formattedBreadCrumbs += breadCrumbsFormatters[1]
                }
            }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }

    %>

    <style type="text/css">
        .ui-autocomplete {
            z-index: 100000;
        }
    </style>


<script>
    var breadcrumbs = [{
            icon: "icon-home",
            link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/pharmacy/pharmacyAllPrescriptions.page"
        },
        {
            label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient))) }${ formattedBreadCrumbs }",
            link: '${ ui.urlBind("/" + contextPath + "pharmacy/pharmacyAllPrescriptions", [ patientId: patient.id ] ) }'
        }
    ];

    jQuery(function() {
        sendNotificationToProvider();
    });

    // autocomplete for recipient using the openmrs rest api for querying providers
    jQuery(function() {
        jQuery("#recipient").autocomplete({
            source: function(request, response) {
                jQuery.ajax({
                    url: "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/provider?q=" + request.term + "&v=custom:(uuid,display,person:(uuid,display))",
                    dataType: "json",
                    success: function(data) {
                        response(data.results.map(function(item) {
                            return {
                                label: stringUtils.toSentenceCase(item.person.display),
                                value: item.uuid
                            };
                        }));
                    },
                });
            },
            minLength: 3,
            select: function(event, ui) {
                jq("#recipient").attr("data-selected-provider", ui.item.value);
                jq("#recipient").val(ui.item.label);
            }
        });
    });

    function sendNotificationToProvider() {
        jQuery("#notifyDoctor").click(function(e) {
            e.preventDefault();
            var parameters = {
                'recipientName': jQuery("#recipient").val(),
                'messageForDoctor': jQuery("#message").val()
            };
            jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "sendNotificationToProvider")}', parameters)
                .done(function(data) {
                    jq().toastmessage('showNoticeToast', "Email send successfully");
                    location.reload();
                });
        });
    }
</script>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-7">
        <div class="col">
            <a href="/${ui.contextPath()}/botswanaemr/pharmacy/pharmacyAllPrescriptions.page">
                Prescriptions / Prescriptions < back to prescriptions
            </a>
        </div>
    </div>
    <div class="col-5">
        <button type="button"
                class="btn btn-sm bg-white text-primary btn-outline-info float-left mb-3"
                onclick="location.href='${ ui.pageLink('botswanaemr', 'patientProfile', [patientId: patient.id]) }'">
            <i class="fa fa-file-text-o"></i> View Patient Profile
        </button>
        <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#emailDoctorModal">
            <i class="fa fa-envelope"></i> Email Doctor
        </button>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card px-0">
            <div class="row">
                <div class="col">
                    <span class="text-primary"><i class="fa fa-user"></i> Patient:</span>
                    ${patient.person.personName.givenName}&nbsp;${patient.person.personName.familyName}
                </div>
                <div class="col">
                    <ul class="list-unstyled">
                        <li class="text-primary">
                            <i class="fa fa-id-badge"></i> Patient ID:
                            <span class="text-dark">${pin}</span>
                        </li>
                        <% if (identifierType != "") { %>
                        <li class="text-primary">
                            <i class="fa fa-id-badge"></i> ${identifierType}:
                            <span class="text-dark">${identifierValue}</span>
                        </li>
                        <% } %>
                    </ul>
                </div>
                <div class="col">
                    <span class="text-dark">${provider}</span>
                </div>
                <div class="col">
                    <div class="row">
                        <ul class="list-unstyled text-primary">
                            <% if(allergies) {%>
                            <li> Allergy:
                                <% allergies.each { %>
                                <span class="text-danger">${it.allergen}<% if (allergies.size() > 1) { %>,<% } %></span>
                                <% } %>
                            </li>
                            <li>Severity: <span class="text-warning"><% if (severity) { %> ${ ui.encodeHtmlContent(ui.format(severity)) } <% } %></span></li>
                            <li>Queue Status: <span class="text-success">${queueStatus}</span></li>
                            <li>Prescription Status: <span class="text-success">${prescriptionStatus}</span></li>
                            <%}else{%>
                            <span class="inline">
                                   Allergies: <span class="text-danger"> Unknown!</span>
                                </span>
                            <%}%>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <ul class="list-unstyled text-primary">
                            <li>
                                <% if (hasDiagnoses) { %>
                                <ul class="list-unstyled text-primary">Diagnoses:
                                    <% diagnoses.eachWithIndex { it, index, offset = index + 1 -> %>
                                    <li class="text-dark">${offset}.&nbsp;${it.diagnosis.specificName?.name ?: it.diagnosis.nonCoded}</li>
                                    <% } %>
                                </ul>
                                <% } %>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row pt-0">
            <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                ${ ui.includeFragment("botswanaemr", "pharmacy/pharmacyHoldRequestsTable")}
            </div>
            <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                ${ ui.includeFragment("botswanaemr", "pharmacy/pharmacyPrescriptionsDetailsTable")}
            </div>
            <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/editPrescription", [patientId: patient.uuid, visit: activeVisit])}
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="emailDoctorModal" tabindex="-1"
     data-controls-modal="emailDoctorModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="emailDoctorModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="emailDoctorModalTitle">Email Doctor</h4>
                    <button type="button" id="emailDoctor" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    To:
                    <input type="text" id="recipient"  name="recipientName" value=""/>
                    <br />
                    Message:<br />
                    <textarea cols="50" rows="5" id="message" name="messageForDoctor"></textarea>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary float-right ml-1" style="margin-left: 2rem" id="notifyDoctor">Send</button>
                </div>
            </div>
        </div>
</div>
<script>
  jQuery(function () {
    jQuery("#emailDoctorModal").appendTo("body");
  });
</script>



