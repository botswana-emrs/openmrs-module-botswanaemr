<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "moment.min.js")
    ui.includeJavascript("botswanaemr", "daterangepicker.min.js")
    ui.includeCss("botswanaemr", "daterangepicker.css")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'},
        { label: "Dashboard"}
    ];
    jQuery(function () {
      populatePopularDrug("${startOfDay}-${endOfDay}");
      jQuery("#today").on('click', function () {
        jQuery("#daily").show();
        jQuery("#monthly").hide();
        jQuery("#yearly").hide();
        jQuery("#weekly").hide();
        jQuery("#dateRange").val("${startOfDay}-${endOfDay}");
        populatePopularDrug("${startOfDay}-${endOfDay}");
      });
      jQuery("#week").on('click', function () {
        jQuery("#daily").hide();
        jQuery("#monthly").hide();
        jQuery("#yearly").hide();
        jQuery("#weekly").show();
        jQuery("#dateRange").val("${startOfDayOfThisWeek}-${endOfLastDayOfThisWeek}");
        populatePopularDrug("${startOfDayOfThisWeek}-${endOfLastDayOfThisWeek}");
      });
      jQuery("#month").on('click', function () {
        jQuery("#daily").hide();
        jQuery("#yearly").hide();
        jQuery("#weekly").hide();
        jQuery("#monthly").show();
        jQuery("#dateRange").val("${startOfDayOfThisMonth}-${endOfLastDayOfThisMonth}");
        populatePopularDrug("${startOfDayOfThisMonth}-${endOfLastDayOfThisMonth}");
      });
      jQuery("#year").on('click', function () {
        jQuery("#daily").hide();
        jQuery("#weekly").hide();
        jQuery("#monthly").hide();
        jQuery("#yearly").show();
        jQuery("#dateRange").val("${startOfDayOfTheFirstMonthInThisYear}-${endOfLastDayOfTheLastMonthThisYear}");
        populatePopularDrug("${startOfDayOfTheFirstMonthInThisYear}-${endOfLastDayOfTheLastMonthThisYear}");
      });
      
      jQuery('input[name="daterange"]').daterangepicker({
        opens: 'left'
      }, function (startDate, endDate) {
        jQuery("#dateRange").val(startDate+"-"+endDate);
      });
      

      jQuery("#dateRange").on('change keyup paste mouseup', function() {
        if (jQuery(this).val()) {
          populatePopularDrug(jQuery(this).val());
        }
      });
    });
    function populatePopularDrug(str){
      jq.getJSON('${ui.actionLink("botswanaemr", "pharmacy/pharmacyAction", "getListOfTheMostRankingDrugsOverPeriodOfTime")}',
          {
            "dateString" : str
          }
      ).success(function(data) {
          if (data && data.length > 0) {
              jQuery("#ranking").show();

              let rankContents = jq('#ranking');
              rankContents.html('<h6 class="p-3 text-dark">Most popular drugs ranking</h6>');

              let colRanking = jq('<div/>', {class: 'col-2'});
              colRanking.append(jq('<span/>', {class: '"position-absolute top-0 start-100 translate-middle badge rounded-pill bg-primary'}));

              let colName = jq('<div/>', {class: 'col pl-1'});
              colName.append(jq('<p/>'));
              jQuery.each(data, function (i, dt) {
                  let newCol = colRanking.clone();
                  newCol.find('span').text(dt["numberOfOccurences"]);

                  let newColName = colName.clone();
                  newColName.find('p').text(dt["drugName"]);

                  let row = jq('<div/>', {class: 'row'});

                  row.append(newCol);
                  row.append(newColName);
                  rankContents.append(row);
              });
          } else {
              jQuery("#ranking").show();
              let rankContents = jq('#ranking');
              rankContents.html('<h6 class="p-3 text-muted">No orders done in the selected period</h6>');
          }

      });
    }
</script>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i>Today's prescriptions filled</div>
                    <div class="stat-digit text-success">${todaysPrescriptionFilled}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "pharmacy/totalsPharmacyGraphSummary") }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average
                            <span class="text-danger">${dailyAveragePrescription}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i>Today's prescriptions uploaded</div>
                    <div class="stat-digit text-success">${todaysPrescriptionUploaded}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "pharmacy/todayPharmacyGraphSummaries") }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Yesterday
                            <span class="text-danger">${yesterdaysPrescriptionUploaded}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Disable this section and rework it for asynchronous data loading -->
<!--
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-2 pt-2">
                            <h5 class="pl-3">Prescriptions</h5>
                        </div>
                        <div class="col-7 pl-5 pr-3">
                            <ul class="list-inline pl-5">
                                <li class="list-inline-item pl-5">
                                    <a class="nav-link" href="#" id="today">Today</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="nav-link" href="#" id="week">This week</a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="nav-link" href="#" id="month">This month</a>
                                </li>
                                <li class="list-inline-item" id="year">
                                    <a class="nav-link" href="#">All year</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3" style="margin-left: -45px;">
                            <input type="text" class="form-control float-left" name="daterange" value="${startOfDay}-${endOfDay}" id="dateRange" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <h6 class="p-3 text-dark">Drugs Trend</h6>
                            <div class="fullwidth" id="daily">
                                ${ ui.includeFragment("botswanaemr", "pharmacy/dailyPharmacyGraphSummaries") }
                            </div>
                            <div class="fullwidth" id="weekly" style="display: none">
                                ${ ui.includeFragment("botswanaemr", "pharmacy/weeklyPharmacyGraphSummary") }
                            </div>
                            <div class="fullwidth" id="monthly" style="display: none">
                                ${ ui.includeFragment("botswanaemr", "pharmacy/monthlyPharmacyGraphSummaries") }
                            </div>
                            <div class="fullwidth" id="yearly" style="display: none">
                                ${ ui.includeFragment("botswanaemr", "pharmacy/yearlyPharmacyGraphSummaries") }
                            </div>
                        </div>
                        <div class="col-4 pl-5" id="ranking">
                            <ul class="list-group pl-5">
                                <li class="list-group-item border-0">
                                    <div class="row pb-0" id="content">

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
