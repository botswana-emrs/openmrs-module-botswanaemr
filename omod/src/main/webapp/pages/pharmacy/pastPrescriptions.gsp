<%
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'},
        { label: "Past Prescriptions"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div id="pharmacyPatientPool" class="col">
                    ${ui.includeFragment("botswanaemr", "pharmacy/pastPrescriptions" , [patientId: patient.id])}
                </div>
            </div>
        </div>
    </div>
</div>

