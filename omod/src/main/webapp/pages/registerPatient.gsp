<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "${ ui.message("registrationapp.registration.label") }", link: "${ ui.pageLink("botswanaemr", "registerPatient") }" }
    ]));

    let redirectToNextRegPage = () => {
        jq("input[type=radio]:checked").each(function() {
            let regType = this.id === "emergencyCheck" ? "emergency" : "routine";

            if (this.id === "emergencyCheck"){
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/emergencyRegistration.page?regType=" + regType + "&returnUrl=${returnUrl}&quick=${isDirectServiceDelivery}&currentServiceDeliveryPoint=${currentServiceDeliveryPoint?.uuid}";
            } else if (this.id === "regularCheck"){
                // window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page?returnUrl=${returnUrl}&quick=${isDirectServiceDelivery}&currentServiceDeliveryPoint=${currentServiceDeliveryPoint?.uuid}";
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/startRegistration.page?regType=" + regType + "&returnUrl=${returnUrl}&quick=${isDirectServiceDelivery}&location=${location}&currentServiceDeliveryPoint=${currentServiceDeliveryPoint?.uuid}";
            } else if (this.id === "quickCheck"){
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/quickRegistration.page?returnUrl=${returnUrl}&quick=${isDirectServiceDelivery}&currentServiceDeliveryPoint=${currentServiceDeliveryPoint?.uuid}";
            }
        });
    };
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <h5>Patient registration form</h5>
                    <p>Complete the form that follows to register a patient</p>
                </div>
                <div class="col-3">
                    ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validation-errors" class="note-container" style="display: none" >
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <form class="simple-form-ui" id="registration" action="javascript:redirectToNextRegPage()">
                    <div class="center-container">
                        <div class="card-title">
                            <h3 class="text-dark">Register a new patient</h3>
                        </div>
                        <h3 class="text-black-50 bold">Select patient type</h3>
                        <p>
                            Select whether the patient will be continue through a regular or emergency registration.
                        </p>
                        <br>
                        <em>
                            What type of registration is this?
                        </em>
                        <br>
                        <div class="radios">
                            <p>
                                <input id="regularCheck" checked="checked" name="radiogroup" type="radio"></input>
                                <label for="regularCheck">Routine</label>
                            </p>
                            <% if (isDirectServiceDelivery) { %>
                            <p>
                                <input name="radiogroup" id="quickCheck" type="radio" checked="checked"></input>
                                <label for="quickCheck">Quick (For patient's destined to the ${currentServiceDeliveryPoint?.name} Service delivery point)</label>
                            </p>
                            <% } else { %>
                            <p>
                                <input name="radiogroup" id="emergencyCheck" type="radio"></input>
                                <label for="emergencyCheck">Emergency</label>
                            </p>
                            <% } %>
                            <br/>
                            <br/>
                            <br>
                            <div>
                                <p style="display: inline">
                                    <button id="next" type="submit"  class=" btn btn-primary right">${ui.message("Next Step")}
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>

            </form>
        </div>
    </div>
</div>
