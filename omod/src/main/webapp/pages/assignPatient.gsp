<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "payment.js")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.message("registrationapp.registration.label") }",
            link: "${ ui.pageLink("botswanaemr", "regularRegistration") }"
        }
    ]));
</script>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="container-fluid center-content-container content-section">
                    <span style=" color: #008000b5;">
                        <i class="fa fa-check-circle  fa-4x"></i>
                    </span>
                    <h5 class="h5 content-section">Payment Captured</h5>
                    <hr/>
                    <h6 class="h6 content-section">Assign to patient pool</h6>

                    <p class="text-center">Please assign the patient to <br/>
                        - Screening patient pool (Screening and triage for a consultation) 
                    </p>
                    ${ui.includeFragment("botswanaemr", "queuePatient", ["startVisit": startVisit, "returnUrl": returnUrl, "action": action ])}
            </div>
        </div>
    </div>
</div>
