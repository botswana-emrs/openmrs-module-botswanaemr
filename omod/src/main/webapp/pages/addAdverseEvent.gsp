<script type="text/javascript">
    jQuery.noConflict(true);
</script>
<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    def defaultEncounterDate = currentVisit ? currentVisit.startDatetime : new Date()
%>

<script type="text/javascript">

    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/adverseEvents.page'},
    ];

    jq(function() {
        jq('input[type=radio]').parents('span').addClass('form-check-inline left');
    });

    let icd11SearchUrl = "${ ui.actionLink("botswanaemr", "search", "searchIcd11") }";
</script>

${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<div class="row">
    <div class="col">
        ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
            visit               : currentVisit,
            formUuid            : formUuid,
            patient             : currentPatient,
            returnUrl           : returnUrl,
            defaultEncounterDate: defaultEncounterDate,
        ])}
    </div>
</div>


