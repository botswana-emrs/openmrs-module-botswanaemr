<%
   if (sessionContext.authenticated && !sessionContext.currentProvider) {
       throw new IllegalStateException("Logged-in user is not a Provider")
   }
   ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
   %>
<script type="text/javascript">
   var breadcrumbs = [
       {
           icon: "icon-home",
           link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/adverseEvents.page'
       },
       {label: "All Patients list"}
   ];
   
   jq(document).ready(function () {
       function populateTbody(data) {
           var tbody = jq("#encountersTableBody");
           tbody.empty();
   
           data.forEach(function (event) {
               var row = "<tr>";
               row += "<td>" + formatDate(event.visitDate) + "</td>";
               row += "<td>" + (event.adverseReaction || "N/A") + "</td>";
               row += "<td>" + formatDate(event.onset) + "</td>";
               row += "<td>" + "" + "</td>";
               row += "<td>" + formatDate(event.dateReported) + "</td>";
               row += "<td><a href='" + generateEditLink(event.encounterId) + "'>Edit encounter</a></td>";
               
               row += "</tr>";
   
               tbody.append(row);
           });
       }
   
       function formatDate(dateStr) {
           if (!dateStr) return "N/A";
           var date = new Date(dateStr);
           return date.toISOString().split('T')[0];
       }
   
       var adverseEventsList = JSON.parse('<%= adverseEventsListJson %>');
       populateTbody(adverseEventsList);
   
       function generateEditLink(encounterId) {
           var patientId = '<%= patient.id %>';
   
           var editEncounterUrl = "${ui.pageLink('botswanaemr', 'editEncounter')}";    
           var returnUrl = '${ui.pageLink("botswanaemr", "adverseEvents")}';
   
           var editUrl = editEncounterUrl + "?patientId=" + patientId + "&encounterId=" + encounterId + "&returnUrl=" + encodeURIComponent(returnUrl);
           return editUrl;
       }
   });
</script>
<div class="row">
   <div class="col col-sm-12 col-md-12 col-lg-12">
      ${ui.includeFragment("botswanaemr", "clientProfileBioData")}
   </div>
</div>
<div class="row">
   <div class="col">
      <div class="card">
         <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
               <div class="table-responsive">
                  <table id="encountersTable" class="table table-sm table-bordered" style="width:100%">
                     <thead>
                        <tr>
                           <th class="text-left">Visit date</th>
                           <th class="text-left">Adverse Reactions</th>
                           <th class="text-left">Onset</th>
                           <th class="text-left">Suspect medicines</th>
                           <th class="text-left">Date Reported</th>
                           <th class="text-left">Actions</th>
                        </tr>
                     </thead>
                     <tbody id="encountersTableBody"></tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>