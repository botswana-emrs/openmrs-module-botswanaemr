<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "Go Back To Dashboard" }
    ];
</script>

<div class="row content-justified-center">
    <div class="col-12">
        <div class="alert alert-info text-center">
            Patient not found in the System! <button class="btn btn-sm btn-dark btn-outline" onclick="history.back()">Go Back To Previous Page</button>
        </div>
    </div>
</div>