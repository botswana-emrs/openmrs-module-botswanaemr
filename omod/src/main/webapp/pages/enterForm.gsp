<script type="text/javascript">
    //jQuery.noConflict(true);
</script>
<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    def defaultEncounterDate = currentVisit ? currentVisit.startDatetime : new Date()

%>

<script type="text/javascript">

    var breadcrumbs = [
        { icon: "icon-home", link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/consultation/doctorDashboard.page"},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient))) }" ,
            link: "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patientId=${patient.id}"},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.message("Consultation"))) }",
            link: '${ returnUrl }'},
        { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.message("Enter Form"))) }" ,
            link: '${ returnUrl }'}
    ];

    jq(function() {
        jq('input[type=radio]').parents('span').addClass('form-check-inline left');
    });

    let icd11SearchUrl = "${ ui.actionLink("botswanaemr", "search", "searchIcd11") }";
</script>

<% if (showHeader) { %>
<div class="row">
   <div class="col col-sm-12 col-md-12 col-lg-12">
       ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient, visitId: currentVisit?.id])}
   </div>
</div>
<% } %>

<div class="row">
    <div class="col">
        ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
            visit               : currentVisit,
            formUuid            : formUuid,
            patient             : currentPatient,
            defaultEncounterDate: defaultEncounterDate,
            returnUrl           : returnUrl
        ])}
    </div>
</div>


