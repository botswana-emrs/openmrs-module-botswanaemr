<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Stock Management"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card bg-pale">
            <div class="row text-primary">
                <div class="col-10 pl-0 pr-0">
                    <i class="fas fa-clipboard-list fa-1x"></i> &nbsp; Today's Orders
                    <div class="display-4">${todayOrders}</div>
                </div>
                <div class="col-2">
                    <i class="fa fa-arrow"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card bg-pale">

            <div class="row text-primary">
                <div class="col-10 pl-0 pr-0">
                    <i class="fa fa fa-clock-o fa-1x"></i> &nbsp; Items Close To Expiry
                    <div class="display-4"><a href="${ui.pageLink("botswanaemr", "stock/itemsCloseToExpiry")}">${closeToExpire}</a></div>
                </div>
                <div class="col-2">
                    <i class="fa fa-arrow"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="col ">
        <div class="card bg-pale">
            <div class="row text-danger">
                <div class="col-10 pl-0 pr-0">
                    <i class="fa fa-calendar fa-1x"></i> &nbsp; Expired Items
                    <div class="display-4"><a href="${ui.pageLink("botswanaemr", "stock/expiredItems")}">${expiredItems}</a></div>
                </div>
                <div class="col-2">
                    <i class="fa fa-arrow"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-9">
                                <h5>Available Products</h5>
                            </div>
                            <!--<div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'stock/addProducts')}" class="btn btn-primary text-white mr-0">
                                    + Add Item
                                </a>
                                <button type="button" class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </button>
                            </div>-->
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/availableProducts")}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <% stockActivities.each{ %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>${it.stockCreatorName}</span> completed <b>${it.instanceType}</b>
                                <br/>
                                <span class="text-muted">${it.duration}</span>
                            </small>
                        </li>
                    </ul>
                     <% } %>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">
                        <div class="row">
                            <div class="col-8">
                                <h5>Items Low In Stock</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/itemsLowInStock")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
