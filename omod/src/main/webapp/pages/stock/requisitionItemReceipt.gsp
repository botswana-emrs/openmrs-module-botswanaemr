<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Receipt Form"}
    ];

jQuery(function() {

    // Handle removal of items
    jq("#requisitionApplicationsTable tbody").on("click", ".text-danger", function() {
        jq(this).parents("tr").remove();
    });

    jq('#submitRequisitionBtn').on('click', (e) => {
        e.preventDefault();
        let sourceStockRoomId = jQuery('#sourceStockRoom').val().trim();
        let destinationStockRoomId = jQuery('#destinationStockRoom').val().trim();
        let itemId = jQuery('#items').val();
        let requisitionDate = jQuery('#requisitionDate').val();
        let description = jQuery('#description').val();

        if (requisitionDate.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select requisition date to proceed");
            return;
        }

        if (sourceStockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select source stock room to proceed");
            return;
        }
        if (destinationStockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select destination stock room to proceed");
            return;
        }

        if (sourceStockRoomId === destinationStockRoomId) {
            jq().toastmessage('showErrorToast', "The source and destination stock rooms can't be the same");
            return;
        }


        var tb = jq("#requisitionApplicationsTable tbody");
        jsonObj = [];

        var size = tb.find("tr").length;
        for (let i = 0; i < tb.find("tr").length; i++) {
            let item = {};

            item["uuid"] = jq(tb.find("tr")[i]).find("input[name^=uuid]").val();
            item["quantity"] = jq(tb.find("tr")[i]).find("input[name^=quantityReceived]").val();
            item["discrepancy"] = jq(tb.find("tr")[i]).find("input[name^=quantityDiscrepancy]").val();
            item["discrepancyReason"] = jq(tb.find("tr")[i]).find("input[name^=discrepancyReason]").val();
            item["batchNumber"] = jq(tb.find("tr")[i]).find("select[name^=batchNumber]").val();
            item["expiryDate"] = jq(tb.find("tr")[i]).find("input[name^=expiryDate]").val();

            if (jq(tb.find("tr")[i]).find("input[type=number]").val().length === 0) {
                jq().toastmessage('showErrorToast', "Allocate the quantity received");
                return;
            }
            if (parseInt(jq(tb.find("tr")[i]).find("div#quantity").text()) < parseInt(jq(tb.find("tr")[i]).find("input[type=number]").val())) {
                jq().toastmessage('showErrorToast', "Can not allocate a higher quantity than available");
                return;
            }
            if (jq(tb.find("tr")[i]).find("select[name^=batchNumber]").val().length === 0) {
                jq().toastmessage('showErrorToast', "Allocate the batch number");
                return;
            }
            if (jq(tb.find("tr")[i]).find("input[name^=expiryDate]").val().length === 0) {
                jq().toastmessage('showErrorToast', "Allocate the expiry date");
                return;
            }

            // if discrepancy > 0,  discrepancy reason must be provided
            if (parseInt(jq(tb.find("tr")[i]).find("input[name^=quantityDiscrepancy]").val()) > 0) {
                if (jq(tb.find("tr")[i]).find("input[name^=discrepancyReason]").val().length === 0) {
                    jq().toastmessage('showErrorToast', "Discrepancy reason must be provided");
                    return;
                }
            }

         jsonObj.push(item);
        }
        const newObs = Object.assign({}, jsonObj);
        saveRequisition(requisitionDate, JSON.stringify(newObs), sourceStockRoomId, destinationStockRoomId, description);
    });

    //Calculate the discrepancy
    jq("#requisitionApplicationsTable tbody").on("change", "input[name^=quantityReceived]", function() {
        let quantityOrdered = jq(this).parents("tr").find("input[name^=quantityOrdered]").val();
        let quantityReceived = parseInt(jq(this).val());
        let discrepancy = quantityOrdered - quantityReceived;
        if (discrepancy < 0) {
            discrepancy = 0;
        }

        //disbale discrepancy reason  is discrepancy is 0
        if (discrepancy == 0) {
            jq(this).parents("tr").find("input[name^=discrepancyReason]").val("");
            jq(this).parents("tr").find("input[name^=discrepancyReason]").prop("disabled", true);
        } else {
            jq(this).parents("tr").find("input[name^=discrepancyReason]").prop("disabled", false);
        }
        
        jq(this).parents("tr").find("input[name^=quantityDiscrepancy]").val(discrepancy);
    });

    // set min date for html5 expiry date to tomorrow
    jq('input[name^=expiryDate]').attr('min', new Date().toISOString().split('T')[0]);

    jq("#requisitionApplicationsTable tbody").on("change", "select[name^=batchNumber]", function() {
        let expiryDate = jq(this).find("option:selected").data("expiry-date");
        jq(this).parents("tr").find("input[name^=expiryDate]").val(expiryDate);
    });    

});


function saveRequisition(requisitionDate, jsonObj, sourceStockRoomId, destinationStockRoomId, description) {
        var sendURL;
        if (jQuery('#actionType').val() == "transfer" && jQuery('#requestType').val() == "external"){
            sendURL = '${ui.actionLink("botswanaemr","stock/incomingExternalRequisitions","issueRequisition")}';
        } else if (jQuery('#requestType').val() == "external"){
            sendURL = '${ui.actionLink("botswanaemr","stock/incomingExternalRequisitions","saveRequisition")}';
        } else if (jQuery('#requestType').val() == "externalReceive" && jQuery('#actionType').val() == "transfer") {
            sendURL = '${ui.actionLink("botswanaemr","stock/incomingExternalRequisitions","receiveRequisition")}';
        } else if (jQuery('#requestType').val() == "internalReceive") {
            sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","receiveRequisition")}';
        } else {
            sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","saveRequisition")}';
        }
    showLoadingOverlay();
    var searchResult;
    jQuery.ajax({
        type: 'POST',
        url: sendURL,
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemId: jsonObj,
            requisitionDate: requisitionDate,
            destinationStockRoomId: destinationStockRoomId,
            description: description,
            sourceStockRoomId: sourceStockRoomId,
            stockOperationUuid: jQuery('#stockOperationUuid').val().trim()
        },
        success: function(data) {
                jq().toastmessage('showNoticeToast', "Operation completed successfully");
                if (jQuery('#requestType').val() === "external" || jQuery('#actionType').val() === "transfer") {
                    location.href = '${ui.pageLink("botswanaemr", "stock/positiveAdjustmentsDashboard")}';
                } else {
                    location.href = '${ui.pageLink("botswanaemr", "stock/internalRequisitionsPg")}';
                }
            
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });



}
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-1 pr-1">
                        <div class="row">
                            <div class="col-9 pl-1">
                                <h5>RECEIPT FORM: ${stockOperation.id}</h5>
                                <input type="input" id="requestType" value="${requestType}" class="hidden">
                            <input type="text" id="actionType" name="actionType" value="${actionType}" class="hidden">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                        <input type="text" id="stockOperationUuid" name="stockOperationUuid" value="${stockOperation.uuid}" class="hidden">
                        <input type="text" id="requisitionDate" name="requisitionDate" value="${stockOperation.operationDate}" class="hidden">
                        <input type="text" id="sourceStockRoom" name="sourceStockRoom" value="${stockOperation.source.id}" class="hidden">
                        <input type="text" id="destinationStockRoom" name="destinationStockRoom" value="${stockOperation.destination.id}" class="hidden">
                        <input type="text" id="description" name="description" value="${stockOperation.description}" class="hidden">
                        <input type="text" id="actionType" name="actionType" value="${actionType}" class="hidden">


                       
                            <h5 class="text-primary">RECEIPT DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            <div class="row">
                                <div class="col pl-0">
                                    <div class="table-responsive">
                                        <table class="table mt-3">
                                            <tbody>
                                                <tr>
                                                    <th>Description</th>
                                                    <td>${stockOperation.description}</td>
                                                </tr>
                                                <tr>
                                                    <th>Requisition Date</th>
                                                    <td>${ui.formatDatePretty(stockOperation.operationDate)}</td>
                                                </tr>
                                                <tr>
                                                    <th>Location</th>
                                                    <td>${location.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Request From</th>
                                                    <td>${ui.format(stockOperation.creator)}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col pr-0">
                                    <div class="table-responsive">
                                        <table class="table mt-3">
                                            <tbody>
                                                <tr>
                                                    <th>Source Facility</th>
                                                    <td>${stockOperation.source.location.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Source Stockroom</th>
                                                    <td>${stockOperation.source.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Destination Stockroom</th>
                                                    <td>${stockOperation.destination.name}</td>
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    <td>${ui.format(stockOperation.status)}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-primary mt-5">PRODUCT DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            
                            <div class="table-responsive">
                                <table id="requisitionApplicationsTable"
                                       class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Product</th>
                                            <th>Quantity Ordered</th>
                                            <th>Quantity Issued</th>
                                            <th>Quantity Received</th>
                                            <th>Batch Number</th>
                                            <th>Expiry Date</th>
                                            <th>Discrepancy</th>
                                            <th>Discrepancy Reason</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <% items.each { %>
                                          <tr>
                                            <td>${it.item.item.codes.stream().findFirst().orElse(null).code}<input type='text' name='uuid[]' value=${it.item.item.uuid} class='hidden'/></td>
                                            <td>${it.item.item.name}</td>
                                            <td>${it.quantityOrdered}</td>
                                            <td>${it.item.quantity}
                                                <input type='text' name='quantityOrdered[]' value=${it.item.quantity} class='hidden'/>
                                            </td>                                            
                                            <td>
                                                <input type='number' class="form-control" id="quantityReceived" name="quantityReceived[]" min="0" max="${it.item.quantity}"/>
                                            </td>
                                            <td>
                                                <select class="form-control" id="batchNumber" name="batchNumber[]">
                                                    <!-- Add options here -->
                                                    <option value="">Select batch</option>
                                                     <% it.batchDetails.each { %>
                                                    <option value="${it.id}" data-expiry-date="${it.expiryDate}">${it.batchNumber} (${it.expiryDate})</option>
                                                     <% } %>
                                                </select>
                                                <!--<input type='text' class="form-control" id="batchNumber" name="batchNumber[]"/>-->
                                            </td>
                                            <td>
                                                <input type='date' class="form-control" id="expiryDate" name="expiryDate[]"/>
                                            </td>
                                            <td>
                                                <input type='number' class="form-control" id="quantityDiscrepancy" name="quantityDiscrepancy[]" min="0" max="${it.quantity}" value="0" readonly/>
                                            </td>
                                            <td>
                                                <input type='text' class="form-control" id="discrepancyReason" name="discrepancyReason[]"/>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col pr-1">
                                                        <a href="#" class="text-danger">Reject</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                            <% } %>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="card-footer text-muted mt-4 pr-1">
                                <div class="col">

                                    <% if(actionType == "transfer") { %>
                                    <button id="submitRequisitionBtn"  type="submit" class="btn btn-primary float-right mr-1">
                                        Receive Products
                                    </button>
                                    <% } else {%>
                                    <button id="submitRequisitionBtn"  type="submit" class="btn btn-primary float-right mr-1">
                                        Issue Requisition
                                    </button>
                                    <% } %>                                    
                                </div>                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

