<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "configuration", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/configuration.page'},
        { label: "Institutions"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">
                        <div class="col-8">
                            <h5>Manage Institutions</h5>
                        </div>
                        <div class="col">
                            <a href="${ui.pageLink('botswanaemr', 'stock/configuration/addInstitutionForm')}" class="btn btn-primary float-right">
                                <div class="text-white text-wrap"> + Add Institution</div>
                            </a>
                        </div>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/configuration/allInstitutions")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
