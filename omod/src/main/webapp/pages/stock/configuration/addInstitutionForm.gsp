<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/configuration.page', label: "Configuration"},
        { link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/configuration/institutions.page', label: "Institutions"},
        { label: "Add Institution"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                ${ ui.includeFragment("botswanaemr", "stock/configuration/addInstitutionForm")}
            </div>
        </div>
    </div>
</div>

