<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementLandingPg.page'},
        { label: "Products close to expire"}
    ];
  
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Your products close to expire</h5>
                                <p>View products close to expire</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-row">   
                            <div class="table-responsive">
                                <h3 class="body">Products</h3>

                                <table id="itemsToReceiveTable"
                                    class="table table-striped table-sm table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Stock Room</th>
                                        <th>Product</th>
                                        <th>Batch</th>
                                        <th>Quantity</th>
                                        <th>Expiry Date</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyId">
                                        <% closeToExpire.each { %>
                                        <tr>
                                            <td>${it.id}</td>
                                            <td>${it.stockRoom}</td>
                                            <td>${it.itemName}</td>
                                            <td>${it.batchNumber}</td>
                                            <td>${it.quantity}</td>
                                            <td>${ui.formatDatePretty(it.expiryDate)}</td>                    
                                        </tr>
                                    <% } %>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

