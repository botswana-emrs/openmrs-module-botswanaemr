<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Internal Requisitions"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">&nbsp;
                        <div class="row">
                            <div class="col-8">
                                <h5>Manage Internal Requisitions</h5>
                            </div>
                            <div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'stock/addRequisitions')}" class="btn btn-outline btn-primary">
                                    + Add Requisition
                                </a>
                                &nbsp;
                                <button type="button" class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <%   if(canViewStockOperations) { %>
                            <h3 class="body">New requisitions</h3>
                                ${ ui.includeFragment("botswanaemr", "stock/outgoingRequisitions")}
                            <% } %>
                            <%   if(canApproveStockOperations) { %>
                            <h3 class="body">Requisitions for Approval</h3>
                                ${ ui.includeFragment("botswanaemr", "stock/incomingInternalRequisitions", [action:"approve"])}
                            <% } %>
                            <%   if(canViewStockOperations) { %>
                            <h3 class="body">Confirm receipt</h3>
                                ${ ui.includeFragment("botswanaemr", "stock/incomingInternalRequisitions", [action:"receive"])}
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
