<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

<script type="text/javascript">
    let breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        {label: "Requisition Application"}
    ];

    jQuery(function () {

        // Handle removal of items
        jq("#requisitionApplicationsTable tbody").on("click", ".text-danger", function () {
            jq(this).parents("tr").remove();
        });


        jq('#submitRequisitionBtn').on('click', (e) => {
            e.preventDefault();
            let sourceStockRoomId = jQuery('#sourceStockRoom').val().trim();
            let destinationStockRoomId = jQuery('#destinationStockRoom').val().trim();
            let itemId = jQuery('#items').val();
            let requisitionDate = jQuery('#requisitionDate').val();
            let description = jQuery('#description').val();

            if (requisitionDate.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select requisition date to proceed");
                return;
            }

            if (sourceStockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select source stock room to proceed");
                return;
            }
            if (destinationStockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select destination stock room to proceed");
                return;
            }

            if (sourceStockRoomId === destinationStockRoomId) {
                jq().toastmessage('showErrorToast', "The source and destination stock rooms can't be the same");
                return;
            }


            let tb = jq("#requisitionApplicationsTable tbody");
            jsonObj = [];

            var size = tb.find("tr").length;
            for (let i = 0; i < tb.find("tr").length; i++) {
                let item = {};

                item["uuid"] = jq(tb.find("tr")[i]).find("input[type=text]").val();
                item["quantity"] = jq(tb.find("tr")[i]).find("input[type=number]").val();

                if (jq(tb.find("tr")[i]).find("input[type=number]").val().length === 0) {
                    jq().toastmessage('showErrorToast', "Allocate the quantity to transfer");
                    return;
                }
                if (parseInt(jq(tb.find("tr")[i]).find("div#quantity").text()) < parseInt(jq(tb.find("tr")[i]).find("input[type=number]").val())) {
                    jq().toastmessage('showErrorToast', "Can not allocate a higher quantity than available");
                    return;
                }

                jsonObj.push(item);
            }
            const newObs = Object.assign({}, jsonObj);
            saveRequisition(requisitionDate, JSON.stringify(newObs), sourceStockRoomId, destinationStockRoomId, description);
        });

        function saveRequisition(requisitionDate, jsonObj, sourceStockRoomId, destinationStockRoomId, description) {
            let sendURL;
            if (jQuery('#actionType').val() == "transfer" && jQuery('#requestType').val() == "external") {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingExternalRequisitions","issueRequisition")}';
            } else if (jQuery('#actionType').val() == "transfer") {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","transferRequisition")}';
            } else if (jQuery('#requestType').val() == "external") {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingExternalRequisitions","approveRequisition")}';
            } else if (jQuery('#actionType').val() == "approveInternal") {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","approveRequisition")}';
            } else if (jQuery('#actionType').val() == "receiveInternal") {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","receiveRequisition")}';
            } else {
                sendURL = '${ui.actionLink("botswanaemr","stock/incomingInternalRequisitions","saveRequisition")}';
            }
            showLoadingOverlay();
            let searchResult;

            jQuery.ajax({
                url: sendURL,
                dataType: "json",
                global: false,
                async: true,
                data: {
                    itemId: jsonObj,
                    requisitionDate: requisitionDate,
                    destinationStockRoomId: destinationStockRoomId,
                    description: description,
                    sourceStockRoomId: sourceStockRoomId,
                    stockOperationUuid: jQuery('#stockOperationUuid').val().trim()
                },
                beforeSend: function () {
                    jq("#saveOverlay").show();
                },
                success: function (data) {
                    jq().toastmessage('showNoticeToast', "Operation completed successfully");
                    if (jQuery('#requestType').val() === "external" || jQuery('#actionType').val() === "transfer") {
                        location.href = '${ui.pageLink("botswanaemr", "stock/positiveAdjustmentsDashboard")}';
                    } else {
                        location.href = '${ui.pageLink("botswanaemr", "stock/internalRequisitionsPg")}';
                    }

                    return data;
                },
                fail: function (err) {
                    jq().toastmessage('showErrorToast', "Operation Failed");
                },
                complete: function () {
                    jq("#saveOverlay").hide();
                }
            });
        }
    });
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-1 pr-1">
                        <div class="row">
                            <div class="col-9 pl-1">
                                <% if (requestType == "external") { %>
                                <h5>Positive and negative adjustments Application: ${stockOperation.id}</h5>
                                <input type="input" id="requestType" value=${requestType} class="hidden">
                                <% } else { %>
                                <h5>INTERNAL REQUISITION APPLICATION: ${stockOperation.id}</h5>
                                <% } %>
                            </div>

                            <div class="col">

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form>
                            <input type="text" id="stockOperationUuid" name="stockOperationUuid"
                                   value="${stockOperation.uuid}" class="hidden">
                            <input type="text" id="requisitionDate" name="requisitionDate"
                                   value="${stockOperation.operationDate}" class="hidden">
                            <input type="text" id="sourceStockRoom" name="sourceStockRoom"
                                   value="${stockOperation.source.id}" class="hidden">
                            <input type="text" id="destinationStockRoom" name="destinationStockRoom"
                                   value="${stockOperation.destination.id}" class="hidden">
                            <input type="text" id="description" name="description" value="${stockOperation.description}"
                                   class="hidden">
                            <input type="text" id="actionType" name="actionType" value="${actionType}" class="hidden">



                            <h5 class="text-primary">APPLICATION DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>

                            <div class="row">
                                <div class="col pl-0">
                                    <div class="table-responsive">
                                        <table class="table mt-3">
                                            <tbody>
                                            <tr>
                                                <th>Description</th>
                                                <td>${stockOperation.description}</td>
                                            </tr>
                                            <tr>
                                                <th>Requisition Date</th>
                                                <td>${ui.formatDatePretty(stockOperation.operationDate)}</td>
                                            </tr>
                                            <tr>
                                                <th>Location</th>
                                                <td>${location.name}</td>
                                            </tr>
                                            <tr>
                                                <th>Request From</th>
                                                <td>${ui.format(stockOperation.creator)}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col pr-0">
                                    <div class="table-responsive">
                                        <table class="table mt-3">
                                            <tbody>
                                            <tr>
                                                <th>Source Facility</th>
                                                <td>${stockOperation.source.location.name}</td>
                                            </tr>
                                            <tr>
                                                <th>Source Stockroom</th>
                                                <td>${stockOperation.source.name}</td>
                                            </tr>
                                            <tr>
                                                <th>Destination Stockroom</th>
                                                <td>${stockOperation.destination.name}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>${ui.format(stockOperation.status)}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <h5 class="text-primary mt-5">PRODUCT DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>

                            <div class="table-responsive">
                                <table id="requisitionApplicationsTable"
                                       class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product</th>
                                        <th>Quantity Requested</th>
                                        <th>Quantity Available</th>
                                        <th>Quantity Allocated</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <% items.each { %>
                                    <tr>
                                        <td>${it.item.codes.stream().findFirst().orElse(null).code}<input type='text'
                                                                                                          name='uuid[]'
                                                                                                          value=${it.item.uuid} class='hidden'/>
                                        </td>
                                        <td>${it.item.name}</td>
                                        <td>${it.quantity}</td>

                                        <td>${ui.includeFragment("botswanaemr", "stock/stockQuantity", [id: it.item.id, stockroomId: stockOperation.source.id])}</td>
                                        <td>
                                            <input type='number' class="form-control" id="quantityAllocated"
                                                   name="quantityAllocated"/>
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col pr-1">
                                                    <a href="#" class="text-danger">Reject</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <% } %>
                                    </tbody>
                                </table>
                            </div>

                            <div class="card-footer text-muted mt-4 pr-1">
                                <% if (actionType == "transfer") { %>
                                <button id="submitRequisitionBtn" type="submit"
                                        class="btn btn-primary float-right mr-1">
                                    Issue Requisition
                                </button>
                                <% } else { %>
                                <button id="submitRequisitionBtn" type="submit"
                                        class="btn btn-primary float-right mr-1">
                                    Approve Requisition
                                </button>
                                <% } %>                            
                            &nbsp;</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

