<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/labReturns.page', label: "Stock Returns"},
        { label: "Create Stock Returns"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
               ${ ui.includeFragment("botswanaemr", "stock/createLabReturns")}
            </div>
        </div>
    </div>
</div>
