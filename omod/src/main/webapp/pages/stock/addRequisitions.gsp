<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Add Requisitions"}
    ];
  

jQuery(function() {
jQuery('#facilityLocation').on('change', function() {
    jq.ajax({
        type: "GET",
        url: '${ui.actionLink("botswanaemr","search","getMainStockRoomsByLocation")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            id: this.value
        },
        success: function(data) {
            jq("#sourceStockRoom option").each(function() {
                jq(this).remove();
            });
            jq("#sourceStockRoom").append(`<option value='' selected>Select Source stock room</option>`);
            data.forEach(function(value) {
                jq("#sourceStockRoom").append(`<option value=` + value.id + `>` + value.name + `</option>`);
            });
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
});
    jq('#addProductBtn').on('click', (e) => {
        e.preventDefault();
        if (!jQuery('#productUuid').val()) {
            jq().toastmessage('showErrorToast', "Product entered is not in the main stock room");
            return
        }
        if (!jQuery('#quantityRequired').val()) {
            jq().toastmessage('showErrorToast', "Specify the product quantity");
            return
        }

        let productName = jQuery('#productName').val();
        let productUuid = jQuery('#productUuid').val();
        let productCode = jQuery('#productCode').val();
        let quantityRequired = jQuery('#quantityRequired').val();
        jq("#productsTable tbody").append(
            "<tr>" +
            "<td>" + productCode + "<input type='text' name='uuid[]' value='" + productUuid + "' class='hidden'/></td>" +
            "<td>" + productName + "</td>" +
            "<td>" + quantityRequired + "<input type='number' name='quantity[]' value='" + quantityRequired + "' class='hidden'/></td>" +
            "<td><a href='#' data-overlay='" + productUuid + "'>Remove</a></td>" +
            "</tr>");
        jQuery('#product').val('');
        jQuery('#productName').val('');
        jQuery('#productUuid').val('');
        jQuery('#productCode').val('');
        jQuery('#quantityRequired').val('');
        return;
    });

    jq('#submitRequisitionBtn').on('click', (e) => {
        e.preventDefault();
        let sourceStockRoomId = jQuery('#sourceStockRoom').val().trim();
        let destinationStockRoomId = jQuery('#destinationStockRoom').val().trim();
        let itemId = jQuery('#items').val();
        let requisitionDate = jQuery('#requisitionDate').val();
        let description = jQuery('#description').val();

        if ( jQuery('#requestType').val() == "external" && jQuery('#facilityLocation').val().trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select location to proceed");
            return;
        }
        if (requisitionDate.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select requisition date to proceed");
            return;
        }

        if (sourceStockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select source stock room to proceed");
            return;
        }
        if (destinationStockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select destination stock room to proceed");
            return;
        }

        if (sourceStockRoomId === destinationStockRoomId) {
            jq().toastmessage('showErrorToast', "The source and destination stock rooms can't be the same");
            return;
        }

        var tb = jq("#productsTable tbody");
        jsonObj = [];

        var size = tb.find("tr").length;
        tb.find("tr").each(function(index, element) {
            let item = {};

            item["uuid"] = jq(this).find("input[type=text]").val();;
            item["quantity"] = jq(this).find("input[type=number]").val();

            jsonObj.push(item);
        });

        if (jsonObj.length === 0) {
            jq().toastmessage('showErrorToast', "Add product(s) to the requisition to proceed");
            return;
        }

        const newObs = Object.assign({}, jsonObj);

        saveRequisition(requisitionDate, JSON.stringify(newObs), sourceStockRoomId, destinationStockRoomId, description);
    });

    jq("#product").autocomplete({
            source: function(request, response) {
                jq.ajax({
                    type: "GET",
                    url: '${ui.actionLink("botswanaemr","search","getAvailableItems")}',
                    dataType: "json",
                    global: false,
                    async: false,
                    data: {
                        name: jQuery('#product').val(),
                        stockRoomId: jQuery('#sourceStockRoom').val()
                    },
                    success: function(data) {
                        response(data);
                    },
                    fail: function(err) {
                        jq().toastmessage('showErrorToast', "Operation Failed");
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                jq("#product").val(ui.item.name + " [" + ui.item.code + "]");
                jq("#productUuid").val(ui.item.uuid);
                jq("#productName").val(ui.item.name);
                jq("#productCode").val(ui.item.code);
                return false;
            }
        })
        .data("ui-autocomplete")._renderItem = function(ul, item) {
            return jq("<li>")
                .append("<a>" + item.code + " <span class='text-decoration-underline'>" + item.name + "</span></a>")
                .appendTo(ul);
        };

    jq('#requisitionDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        "autoclose": true
    });
    jq("#requisitionDate").datepicker("setDate", new Date());

    // Handle removal of items
    jq("#productsTable tbody").on("click", "a", function () {
        jq(this).parents("tr").remove();
    });
});

function saveRequisition(requisitionDate, jsonObj, sourceStockRoomId, destinationStockRoomId, description) {
                        
    var searchResult;
    jQuery.ajax({
        type: 'POST',
        url: '${ui.actionLink("botswanaemr","stock/outgoingRequisitions","saveRequisition")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemId: jsonObj,
            requisitionDate: requisitionDate,
            destinationStockRoomId: destinationStockRoomId,
            description: description,
            sourceStockRoomId: sourceStockRoomId,
            requisitionType: jQuery('#requestType').val()

        },
        success: function(data) {
            jq().toastmessage('showNoticeToast', "Operation completed successfully");
            jq("#productsTable > tbody").empty();
            jQuery('#sourceStockRoom').val('');
            jQuery('#destinationStockRoom').val('');
            jQuery('#destinationStockRoom').val('')
            jQuery('#description').val('');
            if(jQuery('#requestType').val() == "external"){
            location.href = '${ui.pageLink("botswanaemr", "stock/positiveAdjustmentsDashboard")}';
            }else{
            location.href = '${ui.pageLink("botswanaemr", "stock/internalRequisitionsPg")}';
            }
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
}
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Your requisition details</h5>
                                <p>Capture the description of the requisition & add the products required</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-4 pr-3">
                                    <label for="requisitionDate">Requisition date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="requisitionDate" name="requisitionDate" class="form-control" placeholder="Enter requisition date">
                                </div>
                                <% if(requestType == "external") { %>
                                <input type="input" id="requestType" value=${requestType} class="hidden">
                                <div class="form-group col-md-4 pl-3">
                                    <label for="facilityLocation"> Facility location:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="facilityLocation" class="form-control">
                                        <option value="" selected>Select Facility location</option>
                                        <% locations.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                                <% } %>
                                 <div class="form-group col-md-4 pl-3">
                                    <label for="sourceStockRoom"> Source Stock room:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="sourceStockRoom" class="form-control">
                                        <option value="" selected>Select Source stock room</option>
                                        <% sourceStockrooms.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="destinationStockRoom"> Destination Stock room:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="destinationStockRoom" class="form-control">
                                        <option value="" selected>Select Destination stock room</option>
                                        <% destinationStockrooms.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="description">Description:
                                        <span class="text-danger"></span>
                                    </label>
                                    <textarea class="form-control" rows="2"  id="description" name="description" placeholder="Enter description"></textarea>
                                </div>
                            </div>

                            <h5 class="text-primary">PRODUCT DETAILS</h5>

                            <hr class="divider pt-1 bg-primary"/>

                            <div class="form-row">
                                <div class="form-group col-md-5 pr-3 ui-widget">
                                    <label for="product"> Product:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <div class="ui-widget">
                                    <input type="text" id="product" name="product" class="form-control" placeholder="Enter product">
                                        <input type="hidden" id="productUuid">
                                        <input type="hidden" id="productName">
                                        <input type="hidden" id="productCode">
                                    </div>
                                </div>
                                <div class="form-group col-md-5 pl-3">
                                    <label for="quantityRequired"> Quantity required:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="quantityRequired" name="quantityRequired" class="form-control" placeholder="Enter quantity required">
                                </div>
                                <div class="form-group col-md-2 pl-4">
                                    <label for="addProductBtn">&nbsp;</label>
                                    <button id="addProductBtn" class="btn btn-primary p-2 mt-1 float-right"> + Add Product</button>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table id="productsTable"
                                       class="table table-striped table-sm table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Product</th>
                                            <th>Quantity Required</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="card-footer text-muted mt-4 pr-1">
                                <div class="row">
                                    <div class="col pr-0">
                                        <button id="submitRequisitionBtn" type="button" class="btn btn-success float-right mr-0">Create Requisition</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

