<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "External Requisition"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Manage External Requisitions</h5>
                            </div>
                            <div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'stock/addExternalRequisitions',[requestType:'external'])}" class="btn btn-outline btn-primary">
                                    + Add External Requisition
                                </a>
                                &nbsp;
                                <a href="/${ui.contextPath()}/botswanaemr/stock/printRequisitions.page" class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/allExternalRequisition")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

