<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Positive adjustments"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Manage Positive adjustments</h5>
                            </div>
                            <div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'stock/addRequisitions',[requestType:'external'])}" class="btn btn-outline btn-primary">
                                    + Add Positive Adjustment
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/positiveAdjustments")}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-3 pr-0 hidden" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <!--<ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>Doctor</span> completed the screening of
                                <span class="text-success">Logan Paul</span>
                            </small>
                        </li>
                    </ul>-->
                </div>
            </div>

        </div>
    </div>
</div>

