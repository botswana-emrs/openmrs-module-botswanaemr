<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Gen 12 Reports"}
    ];
  

jQuery(function() {
    jQuery('#facilityLocation').on('change', function() {
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","getMainStockRoomsByLocation")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                id: this.value
            },
            success: function(data) {
                jq("#destinationStockRoom option").each(function() {
                    jq(this).remove();
                });
                jq("#destinationStockRoom").append(`<option value='' selected>Select destination stock room</option>`);
                data.forEach(function(value) {
                    jq("#destinationStockRoom").append(`<option value=` + value.id + `>` + value.name + `</option>`);
                });
            },
            fail: function(err) {
                jq().toastmessage('showErrorToast', "Operation Failed");
            }
        });
    });

    jQuery('#sourceStockRoom').on('change', function() {
    jq.ajax({
        type: "GET",
        url: '${ui.actionLink("botswanaemr","search","getDistributedStockOPerationsByDate")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            sourceStockRoomId: this.value,
            startDate: jq('#startDate').val(),
            endDate: jq('#endDate').val()
        },
        success: function(data) {
            jq("#tbodyId").empty();
            displayData(data);

        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
});

jQuery('#destinationStockRoom').on('change', function() {
    jq.ajax({
        type: "GET",
        url: '${ui.actionLink("botswanaemr","search","getDistributedStockOPerationsByDate")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            destinationStockRoomId: this.value,
            startDate: jq('#startDate').val(),
            endDate: jq('#endDate').val()
        },
        success: function(data) {
            jq("#tbodyId").empty();
            displayData(data);

        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
});
    
    jq('#startDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        "autoclose": true,
        "onSelect": function() {

    if (new Date(this.value) > new Date(jq('#endDate').val())) {
        jq().toastmessage('showErrorToast', "Start date should be lower than end date");
        return
    }
    jq.ajax({
        type: "GET",
        url: '${ui.actionLink("botswanaemr","search","getDistributedStockOPerationsByDate")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            startDate: this.value,
            endDate: jq('#endDate').val()
        },
        success: function(data) {
            jq("#tbodyId").empty();
            displayData(data);

        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
}
    });

    jq("#startDate").datepicker("setDate", new Date());

    jq('#endDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        "autoclose": true,
        "onSelect": function () {
        console.log("123333")

if(new Date(this.value) < new Date(jq('#startDate').val())){
            jq().toastmessage('showErrorToast', "End date should be higher than start date");
return
}
        jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","search","getDistributedStockOPerationsByDate")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    endDate: this.value,
                    startDate: jq('#startDate').val()
                },
                success: function(data) {
                            jq("#tbodyId").empty();
                    displayData(data);
                
                },
                fail: function(err) {
                    jq().toastmessage('showErrorToast', "Operation Failed");
                }
            });


        }
    });
    jq("#endDate").datepicker("setDate", new Date());

function displayData(data) {
     data.forEach(function(value) {
                console.log(value);
                        
                //alert(value);
                jq("#tbodyId").append(
            "<tr>" +
            "<td>" + value.id + "</td>" +
            "<td>" + value.destination + "</td>" +
            "<td>" + value.description + "</td>" +
            "<td>" + value.operationDate + "</td>" +

            "<td>" + value.creator + "</td>" +
            "<td> <i class='fa fa-circle text-success'></i>" + value.status + "</td>" +
            "<td> <a href='${ui.pageLink('botswanaemr', 'stock/printRequisitions', [id: " + value.id+ "])}'>View</a> </td>" +
            "</tr>");
            });
}
});


</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Your Gen 12 reports</h5>
                                <p>Select the Dispensing/Receiving stock room to filter</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-4 pr-3">
                                    <label for="startDate">Start date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="startDate" name="startDate" class="form-control" placeholder="Enter requisition date">
                                </div>
                                <div class="form-group col-md-4 pr-3">
                                    <label for="endDate">End date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="endDate" name="endDate" class="form-control" placeholder="Enter end date">
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="sourceStockRoom"> Distributing Stock room:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="sourceStockRoom" class="form-control">
                                        <option value="" selected>Select Distributing stock room</option>
                                        <% sourceStockrooms.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                            

                            <div class="table-responsive">
                                <h3 class="body">Gen 12 Forms</h3>

                                <table id="itemsToReceiveTable"
                                    class="table table-striped table-sm table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Request From</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Requestor</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyId">
                                        <% stockOperationsReports.each { %>
                                        <tr>
                                            <td>${it.id}</td>
                                            <td>${it.destination}</td>
                                            <td>${it.description}</td>
                                            <td>${ui.formatDatePretty(it.operationDate)}</td>
                                            <td>${ui.format(it.creator)}</td>
                                            <td>
                                                <i class="fa fa-circle text-success"></i> ${it.status}
                                            </td>
                                            <td>
                                                <a href="${ui.pageLink('botswanaemr', 'stock/printRequisitions', [id: it.id])}">View</a>
                                            </td>
                                        </tr>
                                    <% } %>
                                    </tbody>
                                </table>
                            </div>

                         
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

