<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Stock Returns"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">&nbsp;
                        <div class="row">
                            <div class="col-10">
                                <h5>Stock Returns</h5>
                            </div>
                            <div class="col pr-0">
                                <a href="${ui.pageLink('botswanaemr', 'stock/createLabReturns')}" class="btn btn-primary float-right">
                                   <div class="text-white text-wrap"> + Add New</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/labReturns")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
