<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Dashboard"}
    ];
</script>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Total active patients</div>
                    <div class="stat-digit text-success">11</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "SCREENING", encounterType: "0077726a-c4a3-11ec-9d64-0242ac120002"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Screenings
                            <span class="text-danger">42</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's total enrollment</div>
                    <div class="stat-digit text-success">13</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "todayGraphSummaries", [type: "SCREENING", encounterType: "0077726a-c4a3-11ec-9d64-0242ac120002"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Yesterday
                            <span class="text-danger">23</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col pl-0">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-1">
                        <div class="row">
                            <div class="col-10 pl-0">
                                <h5>Manage External / Internal Requisitions</h5>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/externalAndInternalRequisitions")}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>Doctor</span> completed the screening of
                                <span class="text-success">Logan Paul</span>
                            </small>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
