<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Products"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">&nbsp;
                        <div class="row">
                            <div class="col-8">
                                <h5>Manage Products</h5>
                            </div>
                            <div class="col">
                                <a href="${ui.pageLink('botswanaemr', 'stock/addProducts')}" class="btn btn-primary">
                                   <div class="text-white text-wrap"> + Add Product</div>
                                </a>
                                &nbsp;
                                <button type="button" class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "stock/allProducts")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
