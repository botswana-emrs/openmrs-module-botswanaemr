<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Batch History"}
    ];

jQuery(function() {
    
    jq('#searchBtn').on('click', (e) => {
        e.preventDefault();
        if (!jQuery('#productId').val()) {
            jq().toastmessage('showErrorToast', "Enter Product to add");
            return
        }
        if (!jQuery('#batchNumber').val()) {
            jq().toastmessage('showErrorToast', "Specify the batch number");
            return
        }
        fetchBatchHistory();
        return;
    });

    jq("#product").autocomplete({
            source: function(request, response) {
                jq.ajax({
                    type: "GET",
                    url: '${ui.actionLink("botswanaemr","search","getItems")}',
                    dataType: "json",
                    global: false,
                    async: false,
                    data: {
                        name: jQuery('#product').val()
                    },
                    success: function(data) {
                        response(data);
                    },
                    fail: function(err) {
                        jq().toastmessage('showErrorToast', "Operation Failed");
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                jq("#product").val(ui.item.name + " [" + ui.item.code + "]");
                jq("#productId").val(ui.item.uuid);
                jq("#productName").val(ui.item.name);
                jq("#productCode").val(ui.item.code);


                let itemCode = jq("#productCode").val();
                let selectedKitQuantity = jq("#quantityRequired");
                let batchIdInput = jq("#batchNumber");
                let expiryInput = jq("#expiryRequired");

                let stockroomInput = jq("#stockRoomId");

                // Fetch stock card details
                jq.ajax({
                    type: "GET",
                    url: '${ui.actionLink("botswanaemr","search","getStockCardDetails")}',
                    data: { itemCode: itemCode },
                    success: function(data) {
                        // Update static HTML with fetched data
                        jq("#stockCardContainer #hospital").text(data.hospital);
                        jq("#stockCardContainer #facilityCode").text(data.facilityCode);
                        jq("#stockCardContainer #itemId").val(data.itemId);

                        // Populate stock room options
                        let stockRoomSelect = jq("#stockCardContainer #stockRoomId");
                        stockRoomSelect.empty();
                        stockRoomSelect.append('<option value="">Select stock room</option>');
                        data.stockRooms.forEach(function(room) {
                            stockRoomSelect.append('<option value="' + room.id + '">' + room.name + '</option>');
                        });

                        jq("#stockCardContainer #commodityCode").text(data.commodityCode);
                        jq("#stockCardContainer #commodityName").text(data.commodityName);
                        jq("#stockCardContainer #unitOfIssue").text(data.unitOfIssue);
                    },
                    fail: function(err) {
                        jq().toastmessage('showErrorToast', "Failed to load stock card details");
                    }
                });

                fetchBatches(stockroomInput, itemCode, selectedKitQuantity, batchIdInput, expiryInput);
                return false;
            }
    })
    .data("ui-autocomplete")._renderItem = function(ul, item) {
        return jq("<li>")
            .append("<a>" + item.code + " <span class='text-decoration-underline'>" + item.name + "</span></a>")
            .appendTo(ul);
    };

    jq('#expiryRequired').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: "yy-mm-dd",
        minDate: 0,
        "autoclose": true
    });

    jq('#fromDate, #toDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: "yy-mm-dd",
        minDate: null,
        "autoclose": true
    });

    jq('body').on('change', '#stockRoomId', function() {
        let itemCode = jq("#productCode").val();
        let selectedKitQuantity = jq("#quantityRequired");
        let batchIdInput = jq("#batchNumber");
        let expiryInput = jq("#expiryRequired");
        let stockroomInput = jq(this);
        
        fetchBatches(stockroomInput, itemCode, selectedKitQuantity, batchIdInput, expiryInput);
    });

    function populateTransactionsTbody(retVal) {
        jq('#stockDetailsTbody').empty();
        retVal.forEach(function (data) {
            let transactionReference = data.transactionReference;
            let transactionDate = new Date(data.transactionDate).toDateInputValue();
            let facilityName = data.facilityName;
            let quantityReceived = data.quantityReceived;
            let quantityIssued = data.quantityIssued;
            let expiryDate = new Date(data.expiryDate).toDateInputValue();
            let batchNumber = data.batchNumber;
            let adjustment  = data.adjustment;
            let losses = data.losses;
            let quantityOnHand = data.quantityOnHand;
            let remarks = data.remarks;
            let initials = data.initials;
            jq('#stockDetailsTbody').append("<tr><td>" + transactionDate + "</td> <td>" + transactionReference + "</td> <td>" + facilityName + "</td> <td>" + quantityReceived + "</td> <td>" + quantityIssued + "</td> <td>" + adjustment + "</td> <td>" + losses + "</td> <td>" + quantityOnHand + "</td> <td>" + remarks + "</td> <td>" + initials + "</td></tr>");
        });
    }

    function fetchBatchHistory() {
        var result;
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr", "stock/viewStockAvailability", "getBatchHistory")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                stockRoomId: jq('#stockRoomId option:selected').val(),
                itemCode: jq('#productCode').val(),
                batchNumber: jq('#batchNumber option:selected').val()
            },
            success: function (data) {
                result = data;
            }
        });
        populateTransactionsTbody(result)
    }

    function populateBatchNumberSelect(data) {
        var batchSelect = jq('#batchNumber');
        batchSelect.empty();
        batchSelect.append('<option value="">Select batch number</option>');

        jq.each(data, function(index, batch) {
            let expiryDate = batch.expiryDate ? (new Date(batch.expiryDate).toLocaleDateString()) : '';
            batchSelect.append('<option value="' + batch.batchNumber + '">' + batch.batchNumber + ' (' + expiryDate + ')' + '</option>');
        });
    }

    function fetchBatches(stockroomInput, itemCode, selectedKitQuantity, batchIdInput, expiryInput) {
        if (jq('#productCode').val() == "") {
            return;
        }    
    
        let parameter = {
            'itemId': jq('#productCode').val()
        };
        // Add stockroom to the paramters if set
        let stockroomId = stockroomInput.val();
        if (stockroomId != '') {
            parameter.stockroomId = stockroomId;
        }

        jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchBatchesByItemCode.action', parameter)
            .done(function (data) {
                if (data.status === 'success') {
                    // Populate batch numbers into the select element
                    if (data.batches) {
                        populateBatchNumberSelect(data.batches);

                        // Update quantity and expiry based on the selected batch
                        batchIdInput.on('change', function() {
                            let selectedBatch = jq(this).val();
                            let quantity = batchNumberToQty[selectedBatch] || '';
                            let expiryDate = batchNumberToExpiry[selectedBatch] || '';

                            jq(selectedKitQuantity).val(quantity);
                            jq(expiryInput).datepicker('setDate', new Date(expiryDate));
                            jq(expiryInput).datepicker('option', 'minDate', new Date(expiryDate));
                            jq(expiryInput).datepicker('option', 'maxDate', new Date(expiryDate));
                        });
                    } else {
                        populateBatchNumberSelect([]); // Clear the select if no batches are found
                    }
                } else {
                    populateBatchNumberSelect([]); // Clear the select if the request fails
                }
            });
        }
    });

    function printContent() {
        window.print();
    }

</script>
<style>
    @media print {
        body * {
            visibility: hidden; /* Hide everything by default */
        }
        #batchHistoryCard, #batchHistoryCard * {
            visibility: visible; /* Only show the batch history card and its contents */
        }
        #formInput {
            display: none; /* Hide the form input during printing */
        }
        .btn {
            display: none; /* Hiding all buttons during printing */
        }
        #stockCardContainer {
            display: block !important; /* Ensure stock card container is displayed during printing */
        }
        #stockCardContainer, #stockCardContainer * {
            visibility: visible !important; /* Show stock card container and its contents */
        }
    }
</style>


<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4" id="batchHistoryCard">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>BATCH HISTORY</h5>
                            </div>
                             <div class="col">
                                <button type="button" onclick="printContent();"  class="btn btn-primary btn-outline p-2 float-right">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                </button>
                             </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="stockCardContainer" class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0" style="display: none;">
                            <h5 class="text-primary">STOCK CARD</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            <div>
                                <div>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <div class="table-responsive">
                                                <table class="table mt-3 table-info">
                                                    <tbody>
                                                        <tr>
                                                            <td>Facility:</td>
                                                            <td class="text-left"><span id="hospital"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Facility Code:</td>
                                                            <td class="text-danger text-left"><span id="facilityCode"></span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col pr-0">
                                            <div class="table-responsive">
                                                <table class="table mt-3 table-info">
                                                    <tbody>
                                                        <tr>
                                                            <th>Store Name:</th>
                                                            <td class="text-left pb-0">
                                                                <form id="changeStockRoomForm">
                                                                    <input type="hidden" class="form-control" id="itemId" name="itemId">
                                                                    <select class="border-0 bg-light p-0" name="stockRoomId" id="stockRoomId" style="select:focus{outline: none;}" placeholder="Select stock room">
                                                                        <option value="">Select stock room</option>
                                                                    </select>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row pt-1 pb-2">
                                        <div class="col pl-0 pr-0">
                                            <div class="table-responsive">
                                                <table id="commodityDetailsTable1" class="table mt-3 table-info">
                                                    <tbody id="commodityDetailsTBody1">
                                                        <tr>
                                                            <td class="d-md-table-cell">Commodity Code: <span id="commodityCode"></span></td>
                                                            <td colspan="3" class="text-left">Commodity Name: <span id="commodityName"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left d-md-table-cell">Unit of Issue: <span id="unitOfIssue"></span></td>
                                                            <td class="text-left">Mini. Stock Level: <span id="minStockLevel"></span></td>
                                                            <td class="text-left">Max. Stock Level: <span id="maxStockLevel"></span></td>
                                                            <td class="text-left">Emergency Order Point: <span id="emergencyOrderPoint"></span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form>
                            <div id="formInput">
                                <div class="form-row">
                                     <div class="form-group col pl-3">
                                         <label for="product"> Product Name:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="product" name="product" class="form-control" placeholder="Enter product name"/>
                                        <input type="hidden" id="productId"/>
                                        <input type="hidden" id="productName"/>
                                        <input type="hidden" id="productCode"/>
                                    </div>
                                    <div class="form-group col pl-3">
                                        <label for="location"> Location:
                                        </label>
                                        <select id="stockRoomId" class="form-control">
                                            <option value="" selected>Select Location</option>
                                            <% stockRooms.each { %>
                                                <option value= ${it.id}>${it.name}</option>
                                                <% } %>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col pl-3">
                                        <label for="batchNumber"> Batch:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select id="batchNumber" name="batchNumber" class="form-control">
                                            <option value="">Select batch number</option>
                                        </select>
                                    </div>
                                     <div class="form-group col pl-3 hidden">
                                        <label for="expiryRequired"> Expiry date:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="expiryRequired" name="expiryRequired" class="form-control" placeholder="Enter expiry date">
                                    </div>
                                </div>

                                <div class="form-row hidden">
                                    <div class="form-group col pl-3">
                                       <label for="quantityRequired"> Quantity Per Pack:
                                            <span class="text-danger">*</span>
                                        </label>
                                       <input type="text" id="quantityRequired" name="quantityRequired" class="form-control" placeholder="Enter quantity">
                                    </div>
                                     <div class="form-group col pl-3">
                                        <label for="manufacturer"> Manufacturer:</label>
                                        <input type="text" id="manufacturer" name="manufacturer" class="form-control" placeholder="Enter manufacturer">
                                    </div>
                                </div>

                                <div class="form-row hidden">
                                    <div class="form-group col pl-3">
                                        <label for="fromDate"> From:</label>
                                        <input type="text" id="fromDate" name="fromDate" class="form-control" placeholder="From">
                                    </div>
                                    <div class="form-group col pl-3">
                                         <label for="toDate"> To:</label>
                                         <input type="text" id="toDate" name="toDate" class="form-control" placeholder="to">
                                     </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="searchBtn">&nbsp;</label>
                                        <button id="searchBtn" class="btn btn-primary p-2 float-right">
                                            <i class="fa fa-search"></i> Search
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="openingBalance">
                                <h5 class="text-primary">OPENING BALANCE</h5>
                                <hr class="divider pt-1 bg-primary"/>

                               <div class="table-responsive">
                                    <table id="stockDataTable"
                                        class="table table-sm table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>REF No</th>
                                                <th>Facility/Dept</th>
                                                <th>Qty Received</th>
                                                <th>Qty issued</th>
                                                <th>Adjust</th>
                                                <th>Losses</th>
                                                <th>Qty On Hand</th>
                                                <th>Remarks</th>
                                                <th>Officer</th>
                                            </tr>
                                        </thead>
                                        <tbody id="stockDetailsTbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

