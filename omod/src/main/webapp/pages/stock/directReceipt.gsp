<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Direct Receipt"}
    ];
  

jQuery(function() {

    const getData = function (request, response) {
        jq.getJSON(
            '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/user?v=full&q=' + request.term,
            function (data) {
                let results = data.results.map(function (user) {
                    let label = user.person && user.person.display ? user.person.display : user.display;

                    return {
                        label: label,
                        value: user.uuid
                    };
                });
                response(results)
            });
    };

    const selectItem = function (event, ui) {
        jq("#receiver").val(ui.item.label);
        return false;
    }

    jq("#receiver").autocomplete({
        source: getData,
        select: selectItem,
        minLength: 4
    });
    
    jq('#addProductBtn').on('click', (e) => {
        e.preventDefault();
        if (!jQuery('#productUuid').val()) {
            jq().toastmessage('showErrorToast', "Enter Product to add");
            return
        }
        if (!jQuery('#quantityRequired').val()) {
            jq().toastmessage('showErrorToast', "Specify the product quantity");
            return
        }
        if (!jQuery('#batchRequired').val()) {
            jq().toastmessage('showErrorToast', "Specify the batch number");
            return
        }
        if (!jQuery('#expiryRequired').val()) {
            jq().toastmessage('showErrorToast', "Specify the expiry date");
            return
        }

        if (!jQuery('#discrepancyRequired').val()) {
            jq().toastmessage('showErrorToast', "Specify the discrepancy");
            return
        }

        if (!jQuery('#comment').val()) {
            jq().toastmessage('showErrorToast', "Specify the comment");
            return
        }

        let productName = jQuery('#productName').val();
        let productUuid = jQuery('#productUuid').val();
        let productCode = jQuery('#productCode').val();
        let quantityRequired = jQuery('#quantityRequired').val();
        let batchRequired = jQuery('#batchRequired').val();
        let expiryRequired = jQuery('#expiryRequired').val();
        let discrepancyRequired = jQuery('#discrepancyRequired').val();
        let comment = jQuery('#comment').val();

        jq("#productsTable tbody").append(
            "<tr>" +
            "<td>" + productCode + "<input type='text' name='uuid[]' value='" + productUuid + "' class='hidden'/></td>" +
            "<td>" + productName + "</td>" +
            "<td>" + quantityRequired + "<input type='number' name='quantity[]' value='" + quantityRequired + "' class='hidden'/></td>" +
            "<td>" + batchRequired + "<input type='text' name='batchRequired[]' value='" + batchRequired + "' class='hidden'/></td>" +
            "<td>" + expiryRequired + "<input type='text' name='expiryRequired[]' value='" + expiryRequired + "' class='hidden'/></td>" +
            "<td>" + discrepancyRequired + "<input type='text' name='discrepancyRequired[]' value='" + discrepancyRequired + "' class='hidden'/></td>" +
            "<td>" + comment + "<input type='text' name='comment[]' value='" + comment + "' class='hidden'/></td>" +
            "<td><a href='#' data-overlay='" + productUuid + "'>Remove</a></td>" +
            "</tr>");
        jQuery('#product').val('');
        jQuery('#productName').val('');
        jQuery('#productUuid').val('');
        jQuery('#productCode').val('');
        jQuery('#quantityRequired').val('');
        jQuery('#batchRequired').val('');
        jQuery('#expiryRequired').val('');
        jQuery('#discrepancyRequired').val('0');
        jQuery('#comment').val('');

        return;
    });

    jq('#submitDirectReceiptBtn').on('click', (e) => {
        e.preventDefault();
        let mainStockroomsId = jQuery('#mainStockrooms').val().trim();
        let institutionsId = jQuery('#institutions').val().trim();
        let itemId = jQuery('#items').val();
        let receiptDate = jQuery('#receiptDate').val();
        let description = jQuery('#description').val();
        let receiver = jQuery('#receiver').val();
        let orderNo = jQuery('#orderNo').val();
        let gen12No = jQuery('#gen12No').val();

        if (receiptDate.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select receipt date to proceed");
            return;
        }

        if (mainStockroomsId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select source stock room to proceed");
            return;
        }
        if (institutionsId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select source institution to proceed");
            return;
        }

        if (mainStockroomsId === institutionsId) {
            jq().toastmessage('showErrorToast', "The source and destination stock rooms can't be the same");
            return;
        }

        if (receiver.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Enter received by to proceed");
            return;
        }

        let tb = jq("#productsTable tbody");
        jsonObj = [];

        let size = tb.find("tr").length;
        tb.find("tr").each(function(index, element) {
            let item = {};

            item["uuid"] = jq(this).find("input[type=text]").val();
            item["quantity"] = jq(this).find( "input[type=number][name='quantity[]']").val();
            item["batch"] = jq(this).find( "input[type=text][name='batchRequired[]']").val();
            item["expiry"] = jq(this).find( "input[type=text][name='expiryRequired[]']").val();
            item["discrepancy"] = jq(this).find( "input[type=text][name='discrepancyRequired[]']").val();
            item["comment"] = jq(this).find( "input[type=text][name='comment[]']").val();

            jsonObj.push(item);
        });

        if (jsonObj.length === 0) {
            jq().toastmessage('showErrorToast', "Add product(s) to the stock operation to proceed");
            return;
        }

        const newObs = Object.assign({}, jsonObj);

        saveDirectReceipt(receiptDate, JSON.stringify(newObs), mainStockroomsId, institutionsId, description, receiver, orderNo, gen12No);
    });

    jq("#product").autocomplete({
            source: function(request, response) {
                jq.ajax({
                    type: "GET",
                    url: '${ui.actionLink("botswanaemr","search","getItems")}',
                    dataType: "json",
                    global: false,
                    async: false,
                    data: {
                        name: jQuery('#product').val()
                    },
                    success: function(data) {
                        response(data);
                    },
                    error: function(err) {
                        jq().toastmessage('showErrorToast', "Operation Failed");
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                jq("#product").val(ui.item.name + " [" + ui.item.code + "]");
                jq("#productUuid").val(ui.item.uuid);
                jq("#productName").val(ui.item.name);
                jq("#productCode").val(ui.item.code);
                return false;
            }
        })
        .data("ui-autocomplete")._renderItem = function(ul, item) {
            return jq("<li>")
                .append("<a>" + item.code + " <span class='text-decoration-underline'>" + item.name + "</span></a>")
                .appendTo(ul);
        };

    jq('#receiptDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        maxDate: 0,
        "autoclose": true
    });
    jq("#receiptDate").datepicker("setDate", new Date());

    jq('#expiryRequired').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "yy-mm-dd",
        minDate: 0,
        "autoclose": true
    });
    jq("#expiryRequired").datepicker("setDate", new Date());

    // Handle removal of items
    jq("#productsTable tbody").on("click", "a", function () {
        jq(this).parents("tr").remove();
    });
});

function saveDirectReceipt(receiptDate, jsonObj, mainStockroomsId, institutionsId, description, receiver, orderNo, gen12No) {
                        
    var searchResult;
    jQuery.ajax({
        url: '${ui.actionLink("botswanaemr","stock/directStock","saveDirectReceipt")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemId: jsonObj,
            receiptDate: receiptDate,
            institutionsId: institutionsId,
            description: description,
            receiver:receiver,
            mainStockroomsId: mainStockroomsId,
            orderNo: orderNo,
            gen12No: gen12No
        },
        success: function(data) {
            jq().toastmessage('showNoticeToast', "Operation completed successfully");
            jq("#productsTable > tbody").empty();
            jQuery('#mainStockrooms').val('');
            jQuery('#institutions').val('');
            jQuery('#institutions').val('')
            jQuery('#description').val('');
            location.href = '${ui.pageLink("botswanaemr", "stock/directReceipt")}';
            
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
}
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Your receipt details</h5>
                                <p>Capture the description of the receipt & add the products required</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-4 pr-3">
                                    <label for="receiptDate">Receipt date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="receiptDate" name="receiptDate" class="form-control" placeholder="Enter receipt date">
                                </div>
                                 <div class="form-group col-md-4 pl-3">
                                    <label for="mainStockrooms"> Destination stock room:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="mainStockrooms" class="form-control">
                                        <option value="" selected>Select Destination stock room</option>
                                        <% mainStockrooms.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="institutions"> Received from:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="institutions" class="form-control">
                                        <option value="" selected>Select Institutions</option>
                                        <% institutions.each { %>
                                            <option value= ${it.id}>${it.name}</option>
                                            <% } %>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="description">Comments/Notes:
                                        <span class="text-danger"></span>
                                    </label>
                                    <textarea class="form-control" rows="2"  id="description" name="description" placeholder="Enter description"></textarea>
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="receiver">Received by:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input class="form-control" id="receiver" name="receiver" placeholder="Enter Receiver"/>
                                </div>
                                <div class="form-group col-md-4 pl-3">
                                    <label for="orderNo">Order Number:
                                        <span class="text-danger"></span>
                                    </label>
                                    <input class="form-control" id="orderNo" name="orderNo" placeholder="Enter Order Number"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="gen12No">Gen 12 Number:
                                        <span class="text-danger"></span>
                                    </label>
                                    <input class="form-control" id="gen12No" name="gen12No" placeholder="Gen 12 Number"/>
                                </div>
                            </div>

                            <h5 class="text-primary">PRODUCT DETAILS</h5>

                            <hr class="divider pt-1 bg-primary"/>

                            <div class="form-row">
                                <div class="form-group col-md-3 ui-widget">
                                    <label for="product"> Product:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <div class="ui-widget">
                                    <input type="text" id="product" name="product" class="form-control" placeholder="Enter product">
                                        <input type="hidden" id="productUuid">
                                        <input type="hidden" id="productName">
                                        <input type="hidden" id="productCode">
                                    </div>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="quantityRequired"> Quantity:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="quantityRequired" name="quantityRequired" class="form-control" placeholder="Enter quantity">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="batchRequired"> Batch:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="batchRequired" name="batchRequired" class="form-control" placeholder="Enter batch number">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="expiryRequired"> Expiry date:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="expiryRequired" name="expiryRequired" class="form-control" placeholder="Enter expiry date">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="discrepancyRequired">Discrepancy:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" min="0" minlength="0" id="discrepancyRequired" name="discrepancyRequired" class="form-control" placeholder="Enter discrepancy between ordered and received" value="0">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="comment">Comment:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="comment" name="comment" class="form-control" placeholder="Enter comment">
                                </div>
                                <div class="form-group col">
                                    <label for="addProductBtn">&nbsp;</label>
                                    <button id="addProductBtn" class="btn btn-primary p-2 mt-1 float-right"> + Add Product</button>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table id="productsTable"
                                       class="table table-striped table-sm table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Batch Number</th>
                                            <th>Expiry Date</th>
                                            <th>Discrepancy</th>
                                            <th>Comment</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="card-footer text-muted mt-4 pr-1">
                                <div class="row">
                                    <div class="col pr-0">
                                        <button id="submitDirectReceiptBtn" type="button" class="btn btn-success float-right mr-0">Submit Direct Receipt</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

