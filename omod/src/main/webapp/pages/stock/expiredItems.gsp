<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementLandingPg.page'},
        {label: "Expired Products"}
    ];

    jQuery(function () {

        jq("#expiredItemsTable tbody").on("click", "button", function () {
            let comment = jq(this).parents("tr").find("input[name^=comment]").val();
            let id = jq(this).parents("tr").find(".id").html();
            let stockRoom = jq(this).parents("tr").find(".stockRoom").html();
            let stockRoomId = jq(this).parents("tr").find(".stockRoomId").html();

            let quantity = jq(this).parents("tr").find(".quantity").html();
            let batchNumber = jq(this).parents("tr").find(".batchNumber").html();

            let date = jq.datepicker.parseDate("dd.M.yy", jq(this).parents("tr").find(".expiration").html());
            let formattedDate = jq.datepicker.formatDate("dd/mm/yy", date);

            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","stock/createStockAdjustment","disposeExpiredProducts")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    comment: comment,
                    id: id,
                    stockRoom: stockRoom,
                    stockRoomId: stockRoomId,
                    quantity: quantity,
                    batchNumber: batchNumber,
                    expiration: formattedDate
                },
                success: function (data) {
                    jq().toastmessage('showNoticeToast', "Items disposed successfully");
                    jq("#expiredItemsTable").find('tr#' + id).remove();
                    if (jq("#expiredItemsTable #tbodyId").find('tr').length === 0) {
                        location.href = '${ui.pageLink("botswanaemr", "stock/stockManagementLandingPg")}';
                    }
                },
                fail: function (err) {
                    jq().toastmessage('showErrorToast', "Operation Failed");
                }
            });
        });


    });
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-0">
                        <div class="row">
                            <div class="col-8">
                                <h5>Your expired products</h5>

                                <p>View expired products</p>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form>
                            <div class="form-row">
                                <div class="table-responsive">
                                    <table id="expiredItemsTable"
                                           class="table table-striped table-sm table-bordered" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Stock Room</th>
                                            <th>Product</th>
                                            <th>Batch</th>
                                            <th>Quantity</th>
                                            <th>Expiry Date</th>
                                            <th>Comment</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyId">
                                        <% expiredItems.each { %>
                                        <tr id='${it.id}'>
                                            <td class='id'>${it.id}</td>
                                            <td class='stockRoom'>${it.stockRoom}</td>
                                            <td style='display:none' class='stockRoomId'>${it.stockRoomId}</td>
                                            <td class='itemName'>${it.itemName}</td>
                                            <td class='batchNumber'>${it.batchNumber}</td>
                                            <td class='quantity'>${it.quantity}</td>
                                            <td class='expiration'>${ui.formatDatePretty(it.expiryDate)}</td>
                                            <td class="comment">
                                                <input type='text' name='comment[]'
                                                       class='form-control form-control-sm'/>
                                            </td>
                                            <td>
                                                <button id="createStockAdjustmentBtn" type="button"
                                                        class="btn btn-success btn-block float-right">Dispose</button>
                                            </td>
                                        </tr>
                                        <% } %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

