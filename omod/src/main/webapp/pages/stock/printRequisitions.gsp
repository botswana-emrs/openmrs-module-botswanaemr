<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page'
        },
        {label: "Print Stock Requisitions"}
    ];

    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div class="row">
    <div class="col-md-9 pl-3">
        <button id="print" onclick="printContent('#requisitionsCard');" class="btn btn-primary float-left">
            <i class="fa fa-print fa-1x"></i> Print Stock Requisitions
        </button>
    </div>
    <div class="col-md-3 justify-content-end">
        ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
    </div>
</div>

<div class="row">
    <div class="col">
        <div id="requisitionsCard" class="card">
            <h2 class="text-primary text-center">
                COMBINED ISSUE AND RECEIPT VOUCHER
                <br/>
                (ALLOCATED ITEMS OF SUPPLIES)
            </h2>
            <div class="row">
                <ul class="list-unstyled pl-5 pr-3">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <span class="text-dark">To:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary"> ${stockOperation?.source?.location?.name}</span>
                                            </u>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <span class="text-dark">Please supply the undermentioned item to:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary"> ${stockOperation?.destination?.location?.name} - ${stockOperation.destination.name}</span>
                                            </u>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <span class="text-dark">Authorizing Officer's Name:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary">${ui.format(stockOperation.creator)}</span>
                                            </u>
                                        </div>
                                        <div class="col">
                                            &nbsp;
                                        </div>
                                        <div class="col pl-0">
                                            <span class="text-dark">Signature:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </u>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <span class="text-dark">Designation:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary">    </span>
                                            </u>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col pl-0">
                                            <span class="text-dark">Ministry:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary">      </span>
                                            </u>
                                        </div>
                                        <div class="col pl-0">
                                            <span class="text-dark">Department:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary"></span>
                                            </u>
                                        </div>
                                        <div class="col pl-0">
                                            <span class="text-dark">Date:</span>
                                            <u style="text-decoration:underline dotted">
                                                <span class="text-primary">${ui.format(stockOperation.operationDate)}</span>
                                            </u>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </ul>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row pl-5 pr-5">
                <table class="table table-bordered mx-3">
                    <thead>
                        <tr>
                            <th scope="col">Issuer's Ledger Folio No.</th>
                            <th colspan="4">Item</th>
                            <th scope="col">Unit</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Recipient Ledger Folio No.</th>
                        </tr>
                    </thead>
                    <tbody>
                    <% items.each { %>
                                          <tr>
                                            <th></th>
                                            <td colspan="4">${it.item.name}</td>
                                            <td>packs</td> 
                                            <td>${it.quantity}</td>
                                            <th></th>


                                              
                                        </tr>
                                            <% } %>
                    </tbody>
                </table>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <ul class="list-unstyled pl-5">
                        <li>
                            <div class="row">
                                <div class="col pl-0">Issued by:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">$context.authenticatedUser.personName.fullName</span>
                                    </u>
                                    <br/>
                                    <small class="text-muted">
                                        <em>Name to be Printed</em>
                                    </small>
                                </div>
                                <div class="col pl-0">Collected By:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">   </span>
                                    </u>
                                    <br/>
                                    <small class="text-muted">
                                        <em>Name to be Printed</em>
                                    </small>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <ul class="list-unstyled pl-5">
                        <li>
                            <div class="row">
                                <div class="col pl-0">Signature:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Date:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Signature:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Date:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col pl-0">Identity No:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                                <div class="col pl-0">BX:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-3">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                                <div class="col">
                                    &nbsp;
                                </div>
                                <div class="col pl-0">Identity No:
                                    <u style="text-decoration:underline dotted">
                                        <span class="text-primary pl-2">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </span>
                                    </u>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <hr class="divider pt-1 bg-primary"/>
            <div class="row justify-content-center mt-4">
                <div class="col-md-12">
                    <h5 class="text-muted text-center">ACKNOWLEDGEMENT</h5>
                    <p class="text-muted text-center">I acknowledge receipt of the above supplies and certify that they have taken on charge as shown</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-unstyled pl-5">
                        <li>
                            <div class="row">
                                <div class="col pl-0">Received by:
                                    <u style="text-decoration:underline dotted">
                                    <span class="text-primary pl-2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Signature:
                                    <u style="text-decoration:underline dotted">
                                    <span class="text-primary pl-2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Identity No:
                                    <u style="text-decoration:underline dotted">
                                    <span class="text-primary pl-2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                    </u>
                                </div>
                                <div class="col pl-0">Date:
                                    <u style="text-decoration:underline dotted">
                                    <span class="text-primary pl-2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                    </u>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
