<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/stock/stockManagementDashboard.page'},
        { label: "Configuration"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">&nbsp;</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="${ui.pageLink('botswanaemr', 'stock/stockRooms')}">Stock rooms</a>
                                </li>
                                 <li class="list-group-item">
                                    <a href="${ui.pageLink('botswanaemr', 'stock/configuration/institutions')}">Institutions</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="${ui.pageLink('botswanaemr', 'stock/unit')}">Units</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
