<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "Go Back To Dashboard" }
    ];
    jQuery(function () {
        jq("form#modifyPasswordForm").submit(function (e) {
            e.preventDefault();
            let form = \$(e.target);
            if (form.valid())
            {
                let data = {newPassword: jq("#newPassword"), oldPassword: jq("#oldPassword")};
                jQuery.post(form.attr('action'), form.serialize(), function(result) {
                    if (result.status === 200 || result.status.toString().indexOf('200') > -1) {
                        jq().toastmessage('showNoticeToast', "Password Changed successfully");
                        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/selectServicePoint.page";
                    } else {
                        jq().toastmessage('showErrorToast',  result.response);
                    }
                },'json')
                    .fail(function (err) {
                        console.log(err)
                        jq().toastmessage('showErrorToast', + err);
                    })
            }
        });
    });
</script>
<div class="row content-justified-center">
    <div class="col-12">
        <form id="modifyPasswordForm"
              action="${ui.actionLink("botswanaemr", "forceChangePassword", "changePassword")}" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="oldPassword">Enter old password
                            <span class="text-danger">*</span>
                        </label>
                        <input type="password" class="form-control"
                               id="oldPassword" required name="oldPassword" value="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="newPassword">Enter new password
                            <span class="text-danger">*</span>
                        </label>
                        <input type="password" class="form-control"
                               id="newPassword" required name="newPassword" value="" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="confirmPassword">Confirm new password
                            <span class="text-danger">*</span>
                        </label>
                        <input type="password" class="form-control"
                               id="confirmPassword" required name="confirmPassword" value="" data-rule-password="true" data-rule-equalTo="#newPassword" required>
                    </div>
                </div>
            </div>
            <div class="actions mt-5">
                <button class="btn btn-dark bg-dark float-right ml-1" data-dismiss="modal">
                    ${ui.message("Close")}
                </button>
                <button id="savePassword" class="btn btn-primary float-right"
                        type="submit">
                    ${ui.message("Save")}
                </button>
            </div>
        </form>
    </div>
</div>