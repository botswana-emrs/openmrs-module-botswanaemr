<%
    ui.decorateWith("appui", "standardEmrPage")
%>
<script type="text/javascript">
  var breadcrumbs = [
    { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
    { label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }"}
  ];
  jq(function(){
    jq(".tabs").tabs();
    // make sure we reload the page if the location is changes; this custom event is emitted by by the location selector in the header
    jq(document).on('sessionLocationChanged', function() {
      window.location.reload();
    });
  });
  var patient = { id: ${ patient.id } };
</script>
${ ui.includeFragment("coreapps", "patientHeader", [ patient: patient.patient, activeVisit: activeVisit, appContextModel: appContextModel ]) }


<div class="clear"></div>
<div class="dashboard clear row">
    <div class="col-12 col-lg-9">
    </div>
</div>