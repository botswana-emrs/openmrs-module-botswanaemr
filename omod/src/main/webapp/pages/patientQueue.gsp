<%
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<div class="row">
    <div class="col">
        <div class="card">
            <h5>Provider pool</h5>
            <div class="table-responsive">
                ${ui.includeFragment("botswanaemr", "patientQueueList")}
            </div>
        </div>
    </div>

</div>
