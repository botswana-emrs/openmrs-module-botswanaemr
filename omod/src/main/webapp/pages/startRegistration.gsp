<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [ isRegistrationPage: ui.message("true") ])
%>
<style type="text/css">
    .mt-40 {
        margin-top: 40px;
    }
    .mt-80 {
        margin-top: 80px;
    }
    .mt-150 {
        margin-top: 150px;
    }
</style>
<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {
            label: "${ ui.message("Start Registration") }",
            link: "${ ui.pageLink("botswanaemr", "startRegistration") }"
        }
    ]));

    let redirectToNextRegPage = () => {
        if (document.getElementById("newPatientCheck").checked) {
            window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
        } /*else if (document.getElementById("existingPatientCheck").checked) {
            window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page";
        }*/

    };

</script>

<script type="text/javascript">
    jq = jQuery;
    jq(function () {
        //  jq("#search-form-wrapper").addClass("open").addClass("mt-80");
        jq(".search-form-trigger").hide();
        jq("#search-form-wrapper").addClass("open").addClass("mt-150");
        jq("#option-separator").addClass("mt-40");
    });
</script>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <form class="simple-form-ui" id="registration">
            <div class="center-container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="font-weight-bolder text-dark">SEARCH PERSON DETAILS</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
