<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    def formattedBreadCrumbs = "";

    if (breadCrumbsDetails) {
        formattedBreadCrumbs += " " + breadCrumbsFormatters[0]
        breadCrumbsDetails.eachWithIndex { attr, index ->
            formattedBreadCrumbs += ui.escapeJs(ui.encodeHtmlContent(ui.format(attr)));
            if (breadCrumbsDetails.size() - 1 != index) {
                formattedBreadCrumbs += breadCrumbsFormatters[1]
            }
        }
        formattedBreadCrumbs += breadCrumbsFormatters[2]
    }

    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "registerPatient.js")
    ui.includeJavascript("botswanaemr", "utils.js")

    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")

%>

<script type="text/javascript">
    let activePatient = "${patient}";
    let activePatientAge = "${patient.patient.person.age}";
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/art/artDashboard'
        },
        {
            label: "${ ui.escapeJs(ui.encodeHtmlContent(ui.format(patient.patient))) }${ formattedBreadCrumbs }",
            link: '${ ui.urlBind("/" + contextPath + baseDashboardUrl, [ patientId: patient.patient.id ] ) }'
        }
    ];

    <% if (ui.message(dashboard + ".breadcrumb") != dashboard + ".breadcrumb") { %>
    breadcrumbs.push({label: "${ ui.message(dashboard + ".breadcrumb") }"})
    <% } %>

    var patient = {id: ${ patient.id }};

    jQuery.noConflict(false);

</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-9">
                    <h5>Screening and testing</h5>

                    <p>Complete the process to screen a patient and add them to the patient pool</p>
                </div>

                <div class="col-3">
                    ${ui.includeFragment("botswanaemr", "widget/cancelAndGoBack")}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text"></div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="container-fluid card shadow d-flex justify-content-center">
            <!-- nav options -->
            <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-patient-info-tab"
                       data-toggle="pill" href="#pills-patient-info"
                       role="tab" aria-controls="pills-patient-info"
                       aria-selected="true">Patient Information
                    </a>
                </li>
                <% if (patientPrograms.size() == 0) { %>
                <li class="nav-item">
                    <a class="nav-link" id="pills-screening-and-testing-tab"
                       data-toggle="pill" href="#pills-screening-and-testing"
                       role="tab" aria-controls="pills-screening-and-testing"
                       aria-selected="false">Testing
                    </a>
                </li>
                <% } %>
                <% if (patientPrograms.size() == 0) { %>
                <li class="nav-item">
                    <a class="nav-link" id="pills-hiv-enrollment-tab"
                       data-toggle="pill" href="#pills-hiv-enrollment"
                       role="tab" aria-controls="pills-hiv-enrollment"
                       aria-selected="false">ART enrollment
                    </a>
                </li>
                <% } %>
                <% if (patientPrograms.size() > 0) { %>
                <% patientPrograms.each{ %>
                <li class="nav-item">
                    <a  class="nav-link"
                        id="program-tab-${it.id}"
                        href="${ui.pageLink('botswanaemr', 'programs/programs', [patientId: patient.patient.uuid, programId: it.program.uuid])}">
                        ${it.program.description}
                    </a>
                </li>
            <% } %>
                <% } %>
            </ul>
            <!-- content -->
            <div class="tab-content" id="pills-tabContent p-3" ng-app="triageDataApp"
                 ng-controller="TriageDataController"><!-- 1st tab -->
                <div class="tab-pane fade show active" id="pills-patient-info"
                     role="tabpanel" aria-labelledby="pills-patient-info-tab">
                    ${ui.includeFragment("botswanaemr", "patientInformation", [patientId: patient.id])}
                    ${ui.includeFragment("botswanaemr", "nextOfKinInformation")}
                </div>
                <!-- 2nd tab -->
                <div class="tab-pane fade" id="pills-screening-and-testing"
                     role="tabpanel" aria-labelledby="pills-screening-and-testing-tab">
                    <div class="row">
                        <div class="col-md-12">
                            ${ ui.includeFragment("botswanaemr", "hts/htsPastTests", [patientId: patient.patient.uuid])}
                        </div>
                    </div>
                </div>
                <!-- 3rd tab -->
                <div class="tab-pane fade" id="pills-hiv-enrollment"  role="tabpanel" aria-labelledby="pills-hiv-enrollment-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="list-group">
                                <% if (hasVisit) { %>
                                    <% if (!enrolled) { %>
                                        <a href="#" class="list-group-item list-group-item-action bg-primary">Pre-enrolment</a>
                                        <% if (!eligible) { %>
                                            <span class="alert alert-sm alert-warning color-info" role="alert">
                                                <i class="fa fa-info-circle"></i> Not Eligible for enrolment!
                                            </span>
                                        <% } %>
                                        <% if (eligible) { %>
                                            <% if (!enrolled) { %>
                                                <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                    patientId: patient.id,
                                                    formUuid : artEnrolmentForm.form.uuid,
                                                    visitId  : currentVisit.uuid,
                                                    returnUrl: returnUrl])}"
                                                    class="list-group-item list-group-item-action">
                                                    <i class="fa fa-arrow-circle-up"></i> Enroll into ART program
                                                </a>
                                            <% } %>
                                        <% } %>
                                    <% } else { %>
                                        <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                                        <% availableArtForms.each { %>
                                        <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                                            patientId: patient.id,
                                            formUuid : it.form.uuid,
                                            visitId  : currentVisit.uuid,
                                            returnUrl: returnUrl])}"
                                            class="list-group-item list-group-item-action">
                                            ${it.form.name}
                                        </a>
                                        <% } %>
                                    <% } %>
                                <% } %>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item list-group-item-action bg-success">Completed forms</a>
                                <% hivTreatmentForms.each { %>
                                <span class="list-group-item list-group-item-action">
                                    ${it.form.name}
                                    <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                                       onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                                    </i>
                                </span>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
