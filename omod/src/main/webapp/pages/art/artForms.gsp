<%
if (sessionContext.authenticated && !sessionContext.currentProvider) {
throw new IllegalStateException("Logged-in user is not a Provider")
}
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<%
ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/art/artDashboard.page'
        },
        {label: "Enter Form"}
    ];
</script>
<script type="text/javascript">
    jq = jQuery;
    jQuery(function () {

        let patientId = '${patient.id}';
        let visitId = '${visit.id}';
        let forms = JSON.parse('${results}');
        let selected = '${selected}';

        jq("#next-tab").click(function(){
            console.log("clicked");
            jq("#pills-start-form-tab").tab("show");
        })

    });
</script>
<style>
    .tab-full {
        height: fit-content !important;
    }
    .error {
        color: orange !important;
    }
    .iti--allow-dropdown {
        width: 100% !important;
    }
    #iframe {
        width: 100%;
        border: none;
    }
</style>
<div class="container-fluid card shadow d-flex justify-content-center">
    <!-- nav options -->
    <ul class="nav nav-pills mb-3 shadow-sm" id="pills-tab" role="tablist">
    <li class="nav-item">
            <a class="nav-link" id="pills-tb-form-tab"
               data-toggle="pill" href="#pills-tb-form"
               role="tab" aria-controls="pills-tb-form"
               aria-selected="false">TB Form
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" id="pills-start-form-tab"
               data-toggle="pill" href="#pills-start-form"
               role="tab" aria-controls="pills-start-form"
               aria-selected="true">${formName}
            </a>
        </li>
    </ul>

        ${ui.includeFragment('htmlformentryui', 'htmlform/enterHtmlForm', [
            visit : visit,
            patient : patient,
            formUuid : selected,
            encounter: encounter,
            defaultEncounterDate : defaultEncounterDate,
            returnUrl : 'returnUrl']
        )}
</div>