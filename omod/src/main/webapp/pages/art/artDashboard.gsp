<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/art/artDashboard.page'},
        { label: "Dashboard"}
    ];

    jq(document).ready(function() {
        toggleGraph();
    });
</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Total active patients</div>
                    <div class="stat-digit text-success">${allScreeningsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "ENROLLMENT", encounterType: "E3708599-BD88-4130-8023-50DC9695AAAD"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Screenings
                            <span class="text-danger">${dailyAverageScreenings}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's total enrollment</div>
                    <div class="stat-digit text-success">${todayScreeningsCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "todayGraphSummaries", [type: "SCREENING", encounterType: "0077726a-c4a3-11ec-9d64-0242ac120002"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Yesterday
                            <span class="text-danger">${yesterdayScreeningsCount}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pr-0">
                            <h4 class="pl-0"> ART Patient Pool</h4>
                        </div>
                    </div>
                    <div id="artPatientPool" class="table-responsive">
                        ${ui.includeFragment("botswanaemr", "art/artPatientPool" ,[patientId: 20])}
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <% todayScreeningActivitiesList.each { %>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>${it.creator}</span> completed the screening of
                                <span class="text-success">${it.name}</span>
                                <span class="text-danger">${it.gender}</span>
                                <span class="text-muted">${it.duration}</span>
                            </small>
                        </li>
                    </ul>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
</div>
