<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [title: ui.message("Regimen Management")])
    def genderOptions = [ [label: ui.message("Person.gender.male"), value: 'M'],
                          [label: ui.message("Person.gender.female"), value: 'F']]
%>

<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page'},
        {
            label: "${ ui.message('Regimen Management')}",
            link: '${ui.pageLink("botswanaemr", "art/regimenManagement")}'
        }
    ];

    jQuery(document).ready(function() {
        jQuery("#addRegimenComponentsModal").appendTo('body');
        jQuery("#addRegimenModal").appendTo('body');
    });
</script>




<div class="card">
    <div class="row">
        <div class="col-sm-2">
            <!-- Tab navs -->
            <div class="nav flex-column nav-pills text-left"
                 id="v-pills-tab"
                 role="tablist"
                 aria-orientation="vertical">
                <a class="nav-link active"
                   id="v-pills-manage-regimen-lines-tab"
                   data-toggle="pill"
                   href="#v-pills-manage-regimen-lines"
                   role="tab"
                   aria-controls="v-pills-manage-regimen-lines"
                   aria-selected="true">Manage Regimen Categories</a>

                <a class="nav-link"
                   id="v-pills-manage-regimens-tab"
                   data-toggle="pill"
                   href="#v-pills-manage-regimens"
                   role="tab"
                   aria-controls="v-pills-manage-regimens"
                   aria-selected="false">Manage Regimens</a>

               <a class="nav-link"
                  id="v-pills-manage-regimen-components-tab"
                  data-toggle="pill"
                  href="#v-pills-manage-regimen-components"
                  role="tab"
                  aria-controls="v-pills-manage-regimen-components"
                  aria-selected="false">Manage Components</a>

            </div>
            <!-- Tab navs -->
        </div>

        <div class="col-9">
            <!-- Tab content -->
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade active show"
                     id="v-pills-manage-regimen-lines"
                     role="tabpanel"
                     aria-labelledby="v-pills-manage-regimen-lines-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Manage Regimen Categories</h5>
                    </div>
                    ${ui.includeFragment("botswanaemr", "art/manageRegimenLines")}
                </div>

                <div class="tab-pane fade"
                     id="v-pills-manage-regimens"
                     role="tabpanel"
                     aria-labelledby="v-pills-manage-regimens-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Manage Regimens</h5>
                    </div>
                    ${ui.includeFragment("botswanaemr", "art/manageRegimen", [regimenCategory: regimenCategory])}
                </div>
                <div class="tab-pane fade"
                     id="v-pills-manage-regimen-components"
                     role="tabpanel"
                     aria-labelledby="v-pills-manage-regimen-components-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Manage Regimen Components</h5>
                    </div>
                    ${ui.includeFragment("botswanaemr", "art/manageRegimenComponents")}
                </div>
            </div>
            <!-- Tab content -->

        </div>
    </div>
</div>

<div class="modal fade" id="addRegimenLineModal" tabindex="-1"
     data-controls-modal="addRegimenLineModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addRegimenLineModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addRegimenLineModalTitle">Add Regimen Category</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                ${ui.includeFragment("botswanaemr", "art/addRegimenLine", [regimenCategory:regimenCategory])}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addRegimenModal" tabindex="-1"
     data-controls-modal="addRegimenModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addRegimenModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addRegimenModalTitle">Add Regimen</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                ${ui.includeFragment("botswanaemr", "art/addRegimen", [regimenCategory:regimenCategory])}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addRegimenComponentsModal" tabindex="-1"
     data-controls-modal="addRegimenComponentsModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addRegimenComponentsModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addRegimenComponentsModalTitle">Add Regimen Components</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                ${ui.includeFragment("botswanaemr", "art/addRegimenComponents", [regimenCategory:regimenCategory])}
            </div>
        </div>
    </div>
</div>