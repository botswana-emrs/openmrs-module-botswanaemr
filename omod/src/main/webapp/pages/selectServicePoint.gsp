<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.includeJavascript("appui", "popper.min.js")
    ui.includeJavascript("appui", "bootstrap.min.js")
    ui.includeCss("appui", "bootstrap.min.css")
    ui.includeCss("botswanaemr", "font-awesome.min.css")
    ui.includeJavascript("botswanaemr", "jquery-3.6.0.js")
    ui.includeCss("botswanaemr", "style.css")
%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>BotswanaEMR</title>
        <link rel="shortcut icon" type="image/ico" href="/${ui.contextPath()}/images/openmrs-favicon.ico"/>
        <link rel="icon" type="image/png\" href="/${ui.contextPath()}/images/openmrs-favicon.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${ui.resourceLinks()}
        <script src="/${ui.contextPath()}/csrfguard" type="text/javascript"></script>
    </head>

    <body style="background-color:#ffffff">
        <div class="page-container">
            <main id="content" class="main">
                <div id="content-area" class="mt-0">
                    <div class="row d-flex justify-content-end mt-1">
                        <div class="col-1">
                            <a href="/${contextPath}/ms/logout" class="btn btn-primary btn-block" style="color: white;">
                                <i class="icon-signout small"></i> ${ui.message("emr.logout")}
                            </a>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-5 text-center">
                            <img class="mx-auto d-block mt-3" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}"
                                 width="80" height="80"/>
                            <span class="h4 text-primary text-bold pb-3">BotswanaEMR</span>
                            <img class="mx-auto d-block rounded-circle mt-3 mb-3"
                                 src="${ui.resourceLink('botswanaemr', 'images/avatar.png')}" width="80" height="80"/>

                            <div class="h6 text-muted mb-3">Welcome</div>
                            <br/>

                            <div class="h5 text-dark text-bold mb-1">${authenticatedUserName}</div>
                            <br/>
                            <span class="h6 text-muted">Please select the facility & service location  you would like to log into</span>
                            <br/>

                            <form method="post">
                                <div class="form-group">
                                    <label for="loggedInLocationId">Facility:</label>
                                    <select class="custom-select form-control" name="loggedInLocationId" id="loggedInLocationId" required>
                                        <option value="">Select Facility</option>
                                        <% assignedlocations.each { %>
                                        <option <% if (assignedlocations.size() == 1) { %> selected <% } %> value="${it.getLocationId()}">${it.name}</option>
                                        <% } %>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="sessionLocationId" class="mb-2">Service location:</label>
                                    <select class="custom-select form-control border form-inline" name="sessionLocationId"
                                            id="sessionLocationId" required>
                                        <option  <% if (locations.size() > 1) { %> selected <% } %> value="">Select service location to check in to</option>
                                        <% locations.each { %>
                                        <option <% if (locations.size() == 1) { %> selected <% } %> value="${it.locationId}">${it.name}</option>
                                        <% } %>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-check-circle small"> </i> Check in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
</html>
