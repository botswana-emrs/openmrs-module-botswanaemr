<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'
        },
        {label: "All Patients list"}
    ];

    var jq = jQuery
    
    jq(document).ready(function () {
        jq('#resetBtn').on('click', function () {
            jq('#patientSearch').val('')
            jq('#filterGender').val('')
            jq('#filterStartDateRegistered').val('')
            jq('#filterEndDateRegistered').val('')
            jq('#filterStatus').val('')
            jq('#ageCategory').val('')
            jq('#condition').val('')

            updateTable();
        });

        jq('#inquireBtn').on('click', function () {
            updateTable();
        });

        jq('#patientSearch').on('keyup', function() {
            var searchTerm = jq(this).val();
            jq('#patientsTable').DataTable().search(searchTerm).draw();
        });

        jq('#filterGender').on('change', function() {
            jq('#patientsTable').DataTable().column(3).search(jq(this).val()).draw();
        });

        jq('#filterStatus').on('change', function() {
            jq('#patientsTable').DataTable().column(7).search(jq(this).val()).draw();
        });
        jq('#filterStartDateRegistered,#filterEndDateRegistered').on('change', function() {
            jq('#patientsTable').DataTable().draw();
        });

        let ageCategoryCtl = jq('#ageCategory');
        DataTable.ext.search.push(function (settings, data, dataIndex) {
            let ageCategory = ageCategoryCtl.val();
            let age = data[5];
            if (
                (ageCategory === null) ||
                (ageCategory === '1-4' && age < 5) ||
                (ageCategory === '5-9' && age >= 5 && age < 10) ||
                (ageCategory === '10-14' && age >= 10 && age < 15) ||
                (ageCategory === '15-24' && age >= 15 && age < 25) ||
                (ageCategory === '25-54' && age >= 25 && age < 55) ||
                (ageCategory === '55-64' && age >= 55 && age < 65) ||
                (ageCategory === '65=>' && age >= 65)
            ) {
                return true;
            }

            return false;
        });

        let startDateRegisteredCtl = jq('#filterStartDateRegistered');
        let endDateRegisteredCtl = jq('#filterEndDateRegistered');
        DataTable.ext.search.push(function (settings, data, dataIndex) {
            let startDate = new Date(startDateRegisteredCtl.datepicker('getDate')).getTime();
            let endDate = new Date(endDateRegisteredCtl.datepicker('getDate')).getTime();
            let registrationDate = data[6];
            const [day, month, year] = registrationDate.split("-");
            const registrationTime = new Date(year, month - 1, day).getTime()

            if (isNaN(registrationTime)) return false;

            if (
                (startDate === 0 && endDate === 0) ||
                (startDate === 0 && registrationTime <= endDate) ||
                (startDate <= registrationTime && endDate === 0) ||
                (startDate <= registrationTime && registrationTime <= endDate)
            ) {
                return true;
            }

            return false;
        });

        jq('#ageCategory').on('change', function() {
            jq('#patientsTable').DataTable().draw();
        });
        updateTable()
    });

    var updateTable = function () {
        getPatientsByAdvancedSearch (
            jq('#filterStartDateRegistered').val(),
            jq('#filterEndDateRegistered').val(),
            jq('#patientSearch').val(),
            jq('#filterGender').val(),
            jq('#filterStatus').val(),
            jq('#ageCategory').val(),
            jq('#condition').val()
        );
    }

    function initializePatientsTable() {
        var table = jq('#patientsTable').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'className': 'select-checkbox',
                    'checkboxes': {
                        'selectRow': true,
                        'select': true
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'dom': 'lBrtip',
            'buttons': [
                {
                    'extend': 'collection',
                    'text': 'More operations',
                    'className': 'btn btn-sm p-1 ',
                    'exportOptions': {
                        'modifier': {
                            'selected': true
                        }
                    },
                    'buttons': [
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to CSV',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        },
                        {
                            'extend': 'pdfHtml5',
                            alignment: 'left',
                            orientation: 'portrait',
                            'text': 'Export to PDF',
                            exportOptions: {
                                columns: [0, 1, 2, 5, 4, 5, 6, 7 ]
                            },
                            pageSize: 'A4',
                            customize: function (doc) {
                                doc.defaultStyle.fontSize = 8;
                                doc.styles.tableHeader.fontSize = 10;
                                doc.content[1].table.widths = [ '10%',  '15%%', '15%%', '15%%', '15%%', '15%%', '15%%'];
                            }
                        }
                    ],
                    'dropup': false
                }
            ],
            'searching': true,

            "pagingType": 'simple_numbers',

            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                }
            },
            'order': [[1, 'desc']]
        });
        jq('#selectedRowsAlertDiv').hide();

        table.on('select', function (e, dt, type, indexes) {
            if (table.rows({selected: true}).count() === 1) {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " item selected");
                jq('#selectedRowsAlertDiv').show();
            } else {
                jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                jq('#selectedRowsAlertDiv').show();
            }
        })
            .on('deselect', function (e, dt, type, indexes) {
                if (table.rows({selected: true}).count() === 0) {
                    jq('#selectedRowsAlertDiv').hide();
                } else {
                    jq('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                    jq('#selectedRowsAlertDiv').show();
                }

            });

        table.buttons().container().appendTo(jq('#other '));

        jq('.card-link').click(() => {
            if (jq('#genderDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });
    }

    function populateTbody(data) {
        jq('#patientsTable').DataTable().buttons().destroy();
        jq('#patientsTable').DataTable().clear().destroy();

        data.map((item) => {
            var url = '/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=' + item.patientId;
            var labUrl = '/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestOrders.page?patientId=' + item.patientId;
            jq("#patientDataTbody").append("<tr><td>" + "</td><td>" + item.identifier + "</td> <td>" + item.name + "</td><td>" + item.gender + "</td> <td>" + item.dateOfBirth + "</td><td>" + item.age + "</td><td>" + item.registeredDate + "</td><td>" + item.status + "</td>" + 
            "<td>" +
            "<a href=" + url + " class='color-primary'>" + 'View profile' + "</a>" + 
            <% if (isLabServicesDeliveryPoint) { %>
            " | <a href="+ labUrl +">Add Lab Test</a>" + 
            <% } %>
            "</td> </tr>");
        });

        initializePatientsTable();
    }

    function getPatientsByAdvancedSearch(startDateRegistered, endDateRegistered, nameOrUniqueId, gender, status, ageCategory, condition) {
        var searchResult;
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","getPatientsByAdvancedSearch")}',
            dataType: "json",
            global: false,
            async: true,
            data: {
                startDateRegistered: startDateRegistered,
                endDateRegistered: endDateRegistered,
                nameOrUniqueId: nameOrUniqueId,
                gender: gender,
                status: status,
                condition: condition
            },
            success: function (data) {

                searchResult = data;
                populateTbody(searchResult)

            }
        });
    }

    function actionRedirect(patientId) {
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patientId=" + patientId;
    }
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-horizontal form-group">
                        <label for="patientSearch">Search:</label>
                        <input type="text" class="form-control input-sm" id="patientSearch"
                               name="patientSearch" placeholder="Search by Patient ID, or name">
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse gender" id="genderDiv">
                        <label for="filterGender">Sex:</label>
                        <select class="custom-select form-control input-sm" id="filterGender">
                            <option selected disabled>Select sex</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-horizontal form-group">
                        <label for="filterStatus">Status:</label>
                        <select class="custom-select form-control input-sm" id="filterStatus">
                            <option selected disabled>Select status:</option>
                            <option value="Active">Active</option>
                            <option value="Expired">Expired</option>
                        </select>
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse" id="dateDiv">
                        <label for="filterStartDateRegistered">Registration Date:
                        </label>
                        <input type="text"
                               class="form-control datepicker input-sm"
                               id="filterStartDateRegistered"
                               name="filterStartDateRegistered"
                               placeholder="Select start date"
                        />
                        <label for="filterEndDateRegistered">
                        </label>
                        <input type="text"
                               class="form-control datepicker input-sm"
                               id="filterEndDateRegistered"
                               name="filterEndDateRegistered"
                               placeholder="Select end date"
                        />
                        <script type="text/javascript">
                            jq('#filterStartDateRegistered,#filterEndDateRegistered').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                "setDate": new Date(),
                                dateFormat: "dd-mm-yy",
                                yearRange: "-150:+0",
                                maxDate: 0,
                                "autoclose": true,
                                onSelect: function (data) {
                                    jq("#filterStartDateRegistered,#filterEndDateRegistered").trigger('change');
                                }
                            });
                        </script>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-horizontal form-group multi-collapse">
                        <label for="ageCategory">Age Category:</label>
                        <select class="custom-select form-control input-sm" id="ageCategory">
                            <option selected disabled>Select Age Category</option>
                            <% ageCategories.each { ageCategory -> %>
                            <option value="<%=ageCategory.value%>"><%=ageCategory.label%></option>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-horizontal form-group collapse multi-collapse">
                        <label for="condition">Condition:</label>
                        <select class="custom-select form-control input-sm" id="condition" name="condition">
                            <option selected disabled>Select Condition</option>
                            <% conditions.each { key, value -> %>
                            <option value="${key}">${value}</option>
                            <% } %>

                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
                                ${ui.message("Search")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <button type="submit" id="resetBtn" class="btn btn-sm btn-dark bg-dark mb-3">
                                ${ui.message("Reset")}
                            </button>
                        </div>

                        <div class="col-auto">
                            <a id="collapseBtn" class="card-link btn btn-sm btn-warning bg-white color-primary border-0"
                               data-target=".multi-collapse"
                               aria-expanded="false"
                               aria-controls="dateDiv genderDiv">
                               <span>${ui.message("Expand")}
                                    <i id="collapse-icon" class="icon-chevron-down"></i>
                               </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <%
                        //TODO: Make this check configurable
                    %>
                    <% if (sessionLocation == "Registration Desk") { %>
                    <div class="row">
                        <div class="col-auto">
                            <button type="submit" onclick="redirectToNewRegistrationPage()"
                                    class="btn btn-sm btn-primary">
                                <i class="icon-plus"></i>   Register New Patient
                            </button>
                            <script type="text/javascript">
                                let redirectToNewRegistrationPage = () => {
                                    window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/startRegistration.page";
                                };
                            </script>
                        </div>

                        <div id="other" class="col-auto"></div>
                    </div>
                    <% } %>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div id="selectedRowsAlertDiv" class="alert alert-info color-info p-0" role="alert">
                                <label class="icon-info-sign color-primary" for="selectedRowsCount"></label>
                                <label id="selectedRowsCount"></label>
                            </div>

                            <div class="table-responsive">
                                <table id="patientsTable"
                                       class="table table-sm table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-left">Patient ID</th>
                                        <th class="text-left">Name</th>
                                        <th class="text-left">Sex</th>
                                        <th class="text-left">Date of birth</th>
                                        <th class="text-left">Age</th>
                                        <th class="text-left">Registered on</th>
                                        <th class="text-left">Status</th>
                                        <th class="text-left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="patientDataTbody">
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
