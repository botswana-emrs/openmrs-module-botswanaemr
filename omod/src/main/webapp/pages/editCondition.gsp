<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")

    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("uicommons", "angular-sanitize.min.js")
    ui.includeJavascript("uicommons", "ngDialog/ngDialog.js")
    ui.includeJavascript("uicommons", "ngDialog/ngDialog.js")
    ui.includeJavascript("uicommons", "services/conceptSearchService.js")
    ui.includeJavascript("uicommons", "directives/coded-or-free-text-answer.js")
    ui.includeFragment("coreapps", "patientHeader", [patient: patient])
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/models/concept.model.js")
    ui.includeJavascript("coreapps", "conditionlist/models/condition.model.js")
    ui.includeJavascript("coreapps", "conditionlist/emr.messages.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("botswanaemr", "editcondition.controller.js")
    ui.includeCss("uicommons", "ngDialog/ngDialog.min.css")
    ui.includeCss("coreapps", "conditionlist/conditions.css")
%>
<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.familyName))) }, ${ ui.encodeJavaScript(ui.encodeHtmlContent(ui.format(patient.givenName))) }",
            link: '${ui.pageLink("botswanaemr", "patientProfile", [patientId: patient.uuid])}'
        },
        {
            label: "${ ui.message("coreapps.conditionui.conditions") }",
            link: '${ui.pageLink("botswanaemr", "manageConditions", [patientId: patient.id, returnUrl: returnUrl])}'
        },
        {label: "${ui.message('coreapps.conditionui.editCondition',"")}"}
    ];
</script>

<div class="row">
    <div class="col-12 justify-content-center">
        <div class="card">
            <div id="condition" ng-app="conditionApp" ng-controller="ConditionController">
                <h4 class="inline">${ui.message('coreapps.conditionui.editCondition',"")}</h4><br/>
                <form>
                    <fieldset class="form-group row">
                        <div class="container-fluid">
                            <ul id="concept-and-date">
                                <li>
                                    <label>${ui.message('coreapps.conditionui.condition')} </label>
                                    <coded-or-free-text-answer id="conceptId" class="concept"
                                                               concept-classes="8d4918b0-c2cc-11de-8d13-0010c6dffd0f,8d492954-c2cc-11de-8d13-0010c6dffd0f,8d492b2a-c2cc-11de-8d13-0010c6dffd0f,8d491a9a-c2cc-11de-8d13-0010c6dffd0f"
                                                               ng-model="concept"/>

                                </li>
                                <label> ${ui.message('coreapps.conditionui.onsetdate')} </label>
                                    <input type="date" class="form-control birth-date" id="conditionStartDate"
                                           name="conditionStartDate"
                                           required>
                                </li>

                                <li id="status" class="group">
                                    <label>${ui.message('coreapps.stopDate.label')} </label>
                                    <input type="date" class="form-control birth-date" id="conditionEndDate"
                                                       name="conditionEndDate"
                                                       required>
                                </li>
                            </ul>
                            <div id="status" class="horizontal inline">
                                <ul>
                                    <li>
                                        <p>
                                            <input type="radio" id="status-1" class="condition-status" value="${ui.message('coreapps.conditionui.active.label')}" name="status" ng-model="condition.status" ng-change="showEndDate()"/>
                                            <label for="status-1">${ui.message('coreapps.conditionui.active.label')}</label>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <input type="radio" id="status-2" class="condition-status" value="${ui.message('coreapps.conditionui.inactive.label')}" name="status" ng-model="condition.status" ng-change="showEndDate()"/>
                                            <label for="status-2">${ui.message('coreapps.conditionui.inactive.label')}</label>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </fieldset>
                </form>

                <div id="actions">
                    <button type="submit" id="addConditionBtn" class="btn btn-sm btn-success mr-0 right"
                            ng-click="validateCondition()">
                            ${ui.message("coreapps.save")}
                    </button>

                    <button class="btn btn-sm btn-dark bg-dark"
                            onclick="location.href='${ui.pageLink("botswanaemr", "manageConditions", [patientId: patient.uuid, returnUrl: returnUrl])}'">
                            ${ ui.message("coreapps.cancel") }
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>