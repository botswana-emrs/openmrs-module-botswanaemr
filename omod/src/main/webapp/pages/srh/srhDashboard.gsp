<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "utils.js")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'},
        { label: "Dashboard"}
    ];

    jq(document).ready(function() {
        toggleGraph();
    })
</script>

<div class="row">
    <div class="col mb-0">
        <button class="btn tbn-small collapsible-button">Toggle Statistics</button>
    </div>
</div>
<div class="row collapsible">
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user-plus color-success border-success"></i> Total patient enrollments</div>
                    <div class="stat-digit text-success">${allEnrolledCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "totalsGraphSummary", [type: "ENROLLMENT", encounterType: "F1F50A54-4E86-4126-950D-8AF10563FEC7"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Daily Average Screenings
                            <span class="text-danger">${dailyAverageEnrollments}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 collapsible-content">
        <div class="card">
            <div class="stat-widget-one">
                <div class="col">
                    <div class="stat-text"><i class="fa fa-user color-primary border-primary"></i> Today's total enrollment</div>
                    <div class="stat-digit text-success">${todayEnrolledCount}</div>
                    <div class="fullwidth">
                        ${ ui.includeFragment("botswanaemr", "todayGraphSummaries", [type: "ENROLLMENT", encounterType: "F1F50A54-4E86-4126-950D-8AF10563FEC7"]) }
                    </div>
                    <div class="clearfix">
                        <small class="pl-0">
                            Yesterday
                            <span class="text-danger">${yesterdayEnrolledCount}</span>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col pl-0">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4 pl-1">
                        <div class="row">
                            <div class="col-10 pl-0">
                                <h5>SRH Patient Pool</h5>
                            </div>
                            <div class="col">
                                <a  href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl, 'action': 'self'])}" class="btn btn-primary p-2 float-right pr-0">
                                    <i class="fa fa-user-plus text-white" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="familyPlanningPool" class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "srh/srhPatientPool")}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3 pr-0 hidden" id="user-activity">
                <div class="card mt-4">
                    <h4>Activity</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <small>
                                <span>Doctor</span> completed the screening of
                                <span class="text-success">Logan Paul</span>
                                <span class="text-danger">1 week ago</span>
                            </small>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
