<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    jq = jQuery;
    jQuery(function () {
        jq(".sidebar").hide();
        jq(".header").hide();
        jq(".breadcrumbs-row").hide();
    });
</script>
<div class="row" id="mainPageSection">
${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}
    <div class="col">
        <div class="card">
            <% if (hasRegistryEncounter == true) { %>
                ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
                    visit : encounter?.visit,
                    encounter : encounter,
                    patient : patient,
                    formUuid : formUuid,
                    returnUrl : returnUrl,
                    definitionUiResource : definitionUiResource ?: ""]
                )}
            <% } else { %>
                ${ui.includeFragment('htmlformentryui', 'htmlform/enterHtmlForm', [
                    visit : visit,
                    patient : patient,
                    formUuid : formUuid,
                    defaultEncounterDate : defaultEncounterDate,
                    returnUrl : returnUrl]
                )}
            <% } %>
        </div>
    </div>
</div>
