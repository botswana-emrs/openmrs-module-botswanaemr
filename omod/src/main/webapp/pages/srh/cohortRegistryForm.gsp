<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'
        },
        {label: "Cohort Registry Form"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="card">
            <% if (hasRegistryEncounter == true) { %>
                ${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
                    visit : encounter?.visit,
                    encounter : encounter,
                    patient : patient,
                    returnUrl : returnUrl,
                    definitionUiResource : definitionUiResource ?: ""]
                )}
            <% } else { %>
                ${ui.includeFragment('htmlformentryui', 'htmlform/enterHtmlForm', [
                    visit : visit,
                    patient : patient,
                    formUuid : formUuid,
                    defaultEncounterDate : defaultEncounterDate,
                    returnUrl : returnUrl]
                )}
            <% } %>
        </div>
    </div>
</div>
