<%
   ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<script type="text/javascript">
   var breadcrumbs = [
       {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'},
       {
           label: "SRH Client Profile",
           link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhClientProfile.page?patientId=${patient.uuid}&visitId=${currentVisit.uuid}&returnUrl=${returnUrl}'
       },
       {label: "Obstetric Client Profile"}
   ];
</script>
<div class="row">
   <div class="col-12">
      <div class="card bg-pale pl-0 pr-0">
         <div class="card-body">
            <div class="row">
               <div class="col">
                  <h5 class="text-muted">Client Profile:
                     <span class="text-primary bg-transparent">${patient.familyName}&nbsp;${patient.givenName}</span>
                  </h5>
               </div>
               <div class="col">
                  ${ui.includeFragment("botswanaemr", "widget/cancelAndGoBack")}
               </div>
            </div>
         </div>
      </div>
      <div class="card px-0">
         <div class="row p-0">
            <% if (obstetricProfileRiskFactors.size() > 0) { %>
            <div class="col">
               <span class="badge badge-pill badge-danger">
                  <!-- ${obstetricProfileRiskFactors.size() > 0 ? "High Risk Client" + obstetricProfileRiskFactors.riskFactors.flatten().join(", ") : ""} </span>-->
                  High Risk Client<a href="#" data-toggle="modal" data-target="#highRiskFactorsModal"><i
                     class="fa fa-info-circle"></i></a>
               </span>
            </div>
            <% } %>
            <% if (positionOfUterusObs) { %>
            <div class="col">
               <span class="badge badge-pill badge-danger">
               Patient has a severe Vaginal bleeding <a href="#" data-toggle="modal" data-target="#highRiskFactorsModal"><i
                  class="fa fa-info-circle"></i></a>
               </span>
            </div>
            <% } %>
            <% if (bulkyConsistencyOfUterusObs) { %>
            <div class="col">
               <span class="badge badge-pill badge-danger">
               Uterus bulky <a href="#" data-toggle="modal" data-target="#highRiskFactorsModal"><i
                  class="fa fa-info-circle"></i></a>
               </span>
            </div>
            <% } %>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col">
      <div class="row">
         <div class="col-12 pl-0 pr-0">
            <div class="card mt-4">
               <div class="card-body">
                  ${ui.includeFragment("botswanaemr", "clientProfileBioData")}
                  <div class="row">
                     <div class="col-12 pl-0 pr-0">
                        <h5 class="text-primary text-left mt-3 pl-3">GESTATION TRENDS</h5>
                        <hr class="divider pt-1 bg-primary ml-3"/>
                        ${ui.includeFragment("botswanaemr", "srh/gestationTrends", [patientId: patient.uuid])}
                     </div>
                  </div>
                  <div class="row">
                     <div class="row col-12 pl-0 pr-0 mb-2">
                        <div class="col-12 pl-0 pr-0">
                           <h5 class="text-primary text-left mt-3 pl-3">STATUS BAR</h5>
                           <hr class="divider pt-1 bg-primary ml-3"/>
                           <table class="table table table-bordered mt-3 pr-0">
                              <thead>
                                 <tr class="bg-pale">
                                    <th class="bg-pale">HIV STATUS</th>
                                    <th class="bg-pale">HB RESULTS</th>
                                    <th class="bg-pale">GESTATION PERIOD (WEEKS)</th>
                                    <th class="bg-pale">HEPATITIS</th>
                                    <th class="bg-pale">SYPHILIS</th>
                                    <th class="bg-pale">ALLERGIES</th>
                                    <th class="bg-pale">IDENTIFIED TEST RISKS</th>
                                    <th class="bg-pale">PATIENT DUE FOR TD</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Positive</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <div class="row col-12 pl-0 pr-0 mb-2">
                        <div class="col-12 pl-0 pr-0">
                           ${ ui.includeFragment("botswanaemr", "art/tptPastEncounters", [patientId: patient.uuid])}
                        </div>
                     </div>
                     <div class="col-6 pl-0 pr-0">
                        <h5 class="text-primary text-left mt-3 pl-3">VISIT DETAILS</h5>
                        <hr class="divider pt-1 bg-primary ml-3"/>
                        <div class="row">
                           <div class="col">
                              <table class="table mt-3 pr-0">
                                 <tbody>
                                    <tr>
                                       <td>VISIT DATE:</td>
                                       <td class="text-danger text-left">
                                          <span>${ui.format(currentVisit?.startDatetime)}</span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>PATIENT TYPE:</td>
                                       <td class="text-danger text-left">
                                          <span>${srhPatientType}</span>
                                       </td>
                                       <td class="text-danger text-left">
                                          <% if (srhEnrolmentEncounter) { %>
                                          <span class="text-danger">
                                          <a href="${ ui.pageLink("botswanaemr", "editEncounter", [encounterId:srhEnrolmentEncounter.uuid, patientId: patient.id, returnUrl: returnUrl]) }">Edit</a>
                                          </span>
                                          <% } %>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="col-6 pl-0 pr-0">
                        <h5 class="text-primary text-left mt-3 pl-3">PAST VISIT</h5>
                        <hr class="divider pt-1 bg-primary ml-3"/>
                        <div class="row">
                           <div class="col">
                              <table class="table mt-3 pr-0">
                                 <tbody>
                                    <tr>
                                       <td>VISIT DATE:</td>
                                       <td class="text-danger text-left">
                                          <span>${ui.format(previousPatientVisit?.startDatetime)}</span>
                                       </td>
                                       <td>View</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="row col-12 pl-0 pr-0">
                        <div class="col-12 pl-0 pr-0">
                           <h5 class="text-primary text-left mt-3 pl-3">OBSTETRIC FORMS</h5>
                           <hr class="divider pt-1 bg-primary ml-3"/>
                           <div class="row">
                              <div class="col-md-6">
                                 <table id="availableForms" class="table table-bordered mt-2">
                                    <thead>
                                       <tr>
                                          <th scope="col" class="bg-pale">
                                             <h6 class="text-dark text-center">Available Forms:</h6>
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <% if (hasVisit) { %>
                                       <% obstetricForms.each { %>
                                       <tr>
                                          <td class="text-primary text-left text-capitalize">
                                             <% if (ancForms.contains(it.form.uuid)) { %>
                                             <a href="${ui.pageLink('botswanaemr', 'anc/ancProfile', [
                                                patientId: patient.id,
                                                selectedFormUuid : it.form.uuid,
                                                visitId  : currentVisit.id])}">
                                             <u>${it.form.name}</u>
                                             </a>
                                             <% } else { %>
                                             <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                patientId: patient.id,
                                                formUuid : it.form.uuid,
                                                visitId  : currentVisit.uuid,
                                                returnUrl: returnUrl])}">
                                             <u>${it.form.name}</u>
                                             </a>
                                             <% } %>
                                          </td>
                                       </tr>
                                       <% } %>
                                       <% } else { %>
                                       <tr>
                                          <td class="text-center">
                                             <div class="alert alert-warning text-danger" role="alert">
                                                <strong><i class="fa fa-warning"></i>
                                                Please start a patient visit to proceed!
                                                </strong>
                                             </div>
                                          </td>
                                       </tr>
                                       <% } %>
                                    </tbody>
                                 </table>
                              </div>
                              <div class="col-md-6 mr-0 pr-0">
                                 <div class="table-responsive">
                                    <table class="table table-bordered mt-2">
                                       <thead>
                                          <tr>
                                             <th scope="col" class="bg-pale">
                                                <h6 class="text-dark">Completed Forms:</h6>
                                             </th>
                                             <th scope="col" class="bg-pale">
                                                <h6 class="text-dark">Date:</h6>
                                             </th>
                                             <th scope="col" class="bg-pale">
                                                <h6 class="text-dark">User:</h6>
                                             </th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <% obstetricEncounters.each { %>
                                          <tr>
                                             <td class="text-primary text-capitalize">
                                                <u>${it.form.name}</u>
                                                <i class="icon-pencil edit-action"
                                                title="${it.form.name}"
                                                onclick="location.href = '${ ui.pageLink("botswanaemr", "editEncounter", [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                                                </i>
                                             </td>
                                             <td>${it.encounterDatetime}</td>
                                             <td>${it.creator}</td>
                                          </tr>
                                          <% } %>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- High Risk Factors Modal -->
      <div class="modal fade" id="highRiskFactorsModal" tabindex="-1" role="dialog"
         aria-labelledby="highRiskFactorsModalLabel"
         aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="highRiskFactorsModalLabel">High Risk Factors</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <ul>
                     <li class="text-danger">${obstetricProfileRiskFactors.size() > 0 ? obstetricProfileRiskFactors.riskFactors.flatten().join(", ") : ""}</li>
                  </ul>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>