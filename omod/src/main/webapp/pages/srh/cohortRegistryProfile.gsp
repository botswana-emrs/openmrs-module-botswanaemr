<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'},
        {
            label: "Cohort",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/cohortRegistryProfile.page?patientId=${patient.uuid}&visitId=${visit.uuid}&returnUrl=${returnUrl}'
        },
        {label: "Cohort Registry Profile"}
    ];
</script>

<div class="row">
    <div class="col-12">
        <div class="card bg-pale pl-0 pr-0">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h5 class="text-muted">Client Profile:
                            <span class="text-primary bg-transparent">${patient.familyName}&nbsp;${patient.givenName}</span>
                        </h5>
                    </div>

                    <div class="col">
                        ${ui.includeFragment("botswanaemr", "widget/cancelAndGoBack")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-body">
                        ${ui.includeFragment("botswanaemr", "clientProfileBioData")}
                        <div class="row">
                            <div class="col-12 pl-0 pr-0">
                                <h5 class="text-primary text-left mt-3 pl-3">COHORT FORMS</h5>
                                <hr class="divider pt-1 bg-primary ml-3"/>

                                <div class="row">
                                    <div class="col-md-6">
                                        <table id="availableForms" class="table table-bordered mt-2">
                                            <thead>
                                                <tr>
                                                    <th scope="col" class="bg-pale">
                                                        <h6 class="text-dark text-center">Available Forms:</h6>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <% if (hasVisit) { %>
                                                    <tr>
                                                        <td class="text-primary text-center text-uppercase">
                                                            <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                patientId: patient.id,
                                                                formUuid : infantFacilityVisitFormUuid,
                                                                visitId  : visitId,
                                                                returnUrl: returnUrl])}"
                                                                class="list-group-item list-group-item-action">
                                                                <u>Infant Facility Visits Form</u>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-primary text-center text-uppercase">
                                                            <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                                                                patientId: patient.id,
                                                                formUuid : cohortRegistryFormUuid,
                                                                visitId  : visitId,
                                                                returnUrl: returnUrl])}"
                                                                class="list-group-item list-group-item-action">
                                                                <u>Cohort Registry Form</u>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <% } else { %>
                                                    <tr>
                                                        <td class="text-center">
                                                            <div class="alert alert-warning text-danger" role="alert">
                                                                <strong><i class="fa fa-warning"></i>
                                                                    Please start a patient visit to proceed!
                                                                </strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <% } %>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6 mr-0 pr-0">
                                        <div class="table-responsive">
                                            <table class="table table-bordered mt-2">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" class="bg-pale">
                                                            <h6 class="text-dark">Completed Forms:</h6>
                                                        </th>
                                                        <th scope="col" class="bg-pale">
                                                            <h6 class="text-dark">Date:</h6>
                                                        </th>
                                                        <th scope="col" class="bg-pale">
                                                            <h6 class="text-dark">User:</h6>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <% cohortEncounters.each { %>
                                                        <tr>
                                                            <td class="text-primary text-uppercase">
                                                                <u>${it.form.name}</u>
                                                                <i class="icon-pencil edit-action"
                                                                   title="${it.form.name}"
                                                                   onclick="location.href='${ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl])}'">
                                                                </i>
                                                            </td>
                                                            <td>${it.encounterDatetime}</td>
                                                            <td>${it.creator}</td>
                                                        </tr>
                                                    <% } %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
