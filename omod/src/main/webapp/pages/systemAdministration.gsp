<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }

    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage", [title: ui.message("User profile")])
    def genderOptions = [ [label: ui.message("Person.gender.male"), value: 'M'],
                          [label: ui.message("Person.gender.female"), value: 'F']]
%>
<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page'},
        {
            label: "${ ui.message('coreapps.app.systemAdministration.label')}",
            link: '${ui.pageLink("botswanaemr", "manageAccounts")}'
        }
    ];

    jQuery(document).ready(function() {

        jQuery('#forcePassword').on('change', function() {
            if (this.checked) {
               jQuery('#forcePassword').val(true);
            } else {
                jQuery('#forcePassword').val(false);
            }
        });

       jQuery("#userName, #password, #confirmPassword").keyup(validatePassword);

       jQuery("#userDetailsForm").submit(function() {
          if (!validatePassword()) {
            return false;
          }
        });
      function validatePassword() {
        var password = jQuery("#password").val();
        var confirmPassword = jQuery("#confirmPassword").val();
        var regex = /^(?=.*\\d)(?=.*\\D)(?=.*[a-z])(?=.*[A-Z])/;
        var users = '${users}';
        var selectedUserName = jq("#selectedUserName").val();

        if(users.includes(jQuery('#userName').val())) {
            if (selectedUserName != null && selectedUserName === jQuery('#userName').val()) {
                jQuery("#userNameError").hide();
            }else {
                jQuery("#userNameError").show();
                jQuery("#error").hide();
                return false;
            }
        } else {
            jQuery("#userNameError").hide();
        }

        if (password.length < 8 || regex.test(password) == false) {
            jQuery("#error").show();
            return false;
        } else {
                if (password !== confirmPassword) {
                    jQuery("#error").hide();
                    jQuery("#confirm-error").show();
                    return false;
                } else {
                    jQuery("#confirm-error").hide();
                    return true;
                }
            }
       }

        jQuery("#togglePassword").click(function(event) {
            event.preventDefault();
            togglePasswordVisibility();
        });

        function togglePasswordVisibility() {
            var passwordField = document.getElementById("password");
            var toggleIcon = document.getElementById("togglePassword");
                if (passwordField.type === "password") {
                    passwordField.type = "text";
                    toggleIcon.classList.remove("fa-eye");
                    toggleIcon.classList.add("fa-eye-slash");
                } else {
                    passwordField.type = "password";
                    toggleIcon.classList.remove("fa-eye-slash");
                    toggleIcon.classList.add("fa-eye");
                }
        }

        // provision where the user can select all facilities 
        // when assigning a new user to facilities
        jQuery("#selectAllButton").on("click", function () {
            jQuery("#allowedLoginFacilities").hide();

            jQuery("#userFacilitiesOptions").val([]).trigger("change");

            const allOptions = jQuery("#selectAllFacilities option");
            allOptions.each(function () {
                jQuery(this).prop("selected", true);
            });

            jQuery("#userFacilitiesOptions").removeAttr("name").removeAttr("required");
            jQuery("#selectAllFacilities").attr("name", "userLoginFacility").show();
            jQuery("#selectAllFacilities").attr("required", "required");

            // deselect button
            jQuery("#deselectAllButton").show();
            jQuery("#selectAllButton").hide();
            jQuery("#selectAllFacilities").trigger("change");
        });

        jQuery("#deselectAllButton").on("click", function () {
            jQuery("#allowedLoginFacilities").show();

            jQuery("#userFacilitiesOptions").attr("name", "userLoginFacility").attr("required", "required");
    
            jQuery("#selectAllFacilities").hide();
            jQuery("#selectAllFacilities").removeAttr("name").removeAttr("required");
            jQuery("#selectAllFacilities option").prop("selected", false);

            jQuery("#selectAllButton").show();
            jQuery("#deselectAllButton").hide();
            jQuery("#userFacilitiesOptions").trigger("change");
        });
    });

    function pageReload() {
        location.reload();
    }

</script>

<div class="card">
    <div class="row">
        <div class="col-sm-2">
            <!-- Tab navs -->
            <div class="nav flex-column nav-pills text-left"
                 id="v-pills-tab"
                 role="tablist"
                 aria-orientation="vertical">
                <a class="nav-link active"
                   id="v-pills-manage-accounts-tab"
                   data-toggle="pill"
                   href="#v-pills-manage-accounts"
                   role="tab"
                   aria-controls="v-pills-manage-accounts"
                   aria-selected="true">Manage Accounts</a>

                <a class="nav-link"
                   id="v-pills-manage-facilities-tab"
                   data-toggle="pill"
                   href="#v-pills-manage-facilities"
                   role="tab"
                   aria-controls="v-pills-manage-facilities"
                   aria-selected="false">Manage facilities</a>

                <a class="nav-link"
                   id="v-pills-appointment-bookings-tab"
                   data-toggle="pill"
                   href="#v-pills-appointment-bookings"
                   role="tab"
                   aria-controls="v-pills-appointment-bookings"
                   aria-selected="false">Appointment Bookings</a>
            </div>
            <!-- Tab navs -->
        </div>

        <div class="col-9">
            <!-- Tab content -->
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade active show"
                     id="v-pills-manage-accounts"
                     role="tabpanel"
                     aria-labelledby="v-pills-manage-accounts-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Manage Accounts</h5>
                    </div>
                    ${ui.includeFragment("botswanaemr", "manageAccounts")}
                </div>

                <div class="tab-pane fade"
                     id="v-pills-manage-facilities"
                     role="tabpanel"
                     aria-labelledby="v-pills-manage-facilities-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Manage facilities</h5>

                        <div class="col p-r-0">
                            <button
                                    type="submit"
                                    onclick="addMflFacility()"
                                    class="btn btn-sm btn-primary mb-3 float-right">
                                ${ui.message("Add Facility")}
                            </button>
                        </div>
                    </div>
                    ${ui.includeFragment("botswanaemr", "admin/facilities")}
                </div>

                <div class="tab-pane fade"
                     id="v-pills-appointment-bookings"
                     role="tabpanel"
                     aria-labelledby="v-pills-appointment-bookings-tab">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h5 class="text-right">Appointment Bookings</h5>
                    </div>
                    ${ui.includeFragment("botswanaemr", "appointmentBookings")}
                </div>
            </div>
            <!-- Tab content -->

        </div>
    </div>
</div>
<div class="modal fade" id="editAccountModal" tabindex="-1"
     data-controls-modal="editAccountModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editAccountModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editAccountModalTitle">Manage account</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close" onclick="pageReload()">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    <form  id="userDetailsForm" method="post" name="userDetailsForm" class="simple-form-ui">
                        <fieldset class="w-100">
                            <legend class="w-auto"><b><small>${ ui.message("adminui.person.details") }</small></b></legend>

                            <div class="adminui-account-person-details">
                                <input id="personId" name="personId" style="display:none;">
                                <table class="adminui-display-table" cellpadding="0" cellspacing="0">
                                    <tr class="adminui-border-bottom">
                                        <td valign="top">${ui.message("PersonName.firstName")}</td>
                                        <td valign="top">${ui.message("PersonName.middleName")}</td>
                                        <td valign="top">${ui.message("coreapps.person.familyName")}</td>
                                    </tr>
                                    <tr><td><input class="form-control person-details" name="personGivenName" required id="personGivenName"></td>
                                        <td><input class="form-control person-details" name="personMiddleName" id="personMiddleName"></td>
                                        <td><input class="form-control person-details" name="personFamilyName" required id="personFamilyName"></td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="text-align: left">${ui.message("Person.gender")}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select class="form-control person-details" name="personGender" required id="personGender">
                                                <option value="male">${ui.message("Patient.gender.male")}</option>
                                                <option value="female">${ui.message("Patient.gender.female")}</option>
                                            </select>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="col">
                                <fieldset class="w-auto">
                                    <legend class="w-auto"><b><small>${ui.message("adminui.user.account.details")}</small>
                                    </b></legend>

                                    <div class="form-group">
                                        <label class="form-label" for="userName">Username <span
                                                class="text-danger">*</span></label>
                                        <input type="text" minlength="3" class="form-control person-details" id="userName" name="userName"
                                               required placeholder="Enter Username"
                                               maxlength=" ${propertyMaxLengthMap['username']} ">
                                        <input type="hidden" name="selectedUserName" id="selectedUserName">
                                        <div class="alert alert-danger pt-3" role="alert" id="userNameError" style="display: none">
                                            Username already taken!
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="password">User Password <span
                                                class="text-danger">*</span></label>
                                        <div class="input-group form-group">
                                            <input type="password" minlength="8" name="password" id="password" placeholder="Password"
                                               class="form-control icon-key" />
                                            <div class="input-group-append">
                                                <a href="javascript:void(0);" class="form-control input-group-text" id="togglePasswordLink">
                                                    <i class="fa fa-eye" id="togglePassword" style="cursor: pointer;" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="alert alert-danger pt-3" role="alert" id="error" style="display: none">
                                                Password should be at least 8 characters long. <br/>
                                                Must contain Uppercase and Lowercase letters,  at least one digit, and at least one non-digit!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="confirmPassword">Confirm Password <span class="text-danger">*</span></label>
                                        <input type="password" minlength="8" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password"
                                            class="form-control" />
                                        <small class="form-text text-muted">
                                           <span class="text-danger"></span> Password should be 8 characters long and <br/>
                                           should have both upper and lower case characters, <br/>
                                           at least one digit, and at least one non-digit.
                                        </small>
                                        <div class="alert alert-danger pt-3" role="alert" id="confirm-error" style="display: none">
                                            Passwords do not match.
                                        </div>
                                    </div>
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="forcePassword" id="forcePassword">
                                      <label class="form-check-label" for="forcePassword">
                                        Force password change on next login
                                      </label>
                                    </div>

                                </fieldset>
                            </div>

                            <div class="col">
                                <fieldset class="w-100">
                                    <legend class="w-auto"><b><small>${ui.message("Roles")}</small>
                                    </b></legend>
                                    <div class="form-group">
                                        <label class="form-label"
                                               for="userRole">${ui.message("User roles")} <span
                                                class="text-danger">*</span></label>
                                        <select id="userRole"  placeholder="select provider role" name="userRole" class="form-control custom-select" multiple required>
                                            <% userRoles.each { role -> %>
                                            <option value="${role.uuid}">${role.name.split(":")[1]}</option>
                                            <% } %>
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <fieldset class="w-100">
                                    <legend class="w-auto"><small>${ui.message("Allowed Login Facilities")}</small>
                                    </legend>
                                    <div class="form-group" id="allowedLoginFacilities">
                                        <label class="form-label"
                                               for="userFacilitiesOptions">${ui.message("Allowed Login Facilities")}
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select id="userFacilitiesOptions"  placeholder="select allowed login facilities" name="userLoginFacility" class="form-control custom-select" multiple required>
                                            <% parentLocations.each { location -> %>
                                            <option value="${location.uuid}">${location.name}</option>
                                            <% } %>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="selectAllButton" class="btn btn-link">${ui.message("Select All")}</button>
                                        <select id="selectAllFacilities"  placeholder="select all login facilities" name="userAllLoginFacility" class="form-control custom-select"  style="display: none;" multiple>
                                            <% parentLocations.each { location -> %>
                                            <option value="${location.uuid}">${location.name}</option>
                                            <% } %>
                                        </select>
                                        <button type="button" id="deselectAllButton" class="btn btn-link" style="display: none;">${ui.message("Deselect All")}</button>
                                    </div>
                                </fieldset>
                                <br/>
                                <div class="align-center p-t-2">
                                    <button id="adminui-user-save" type="submit" class="btn btn-sm btn-primary float-right ml-1">
                                        ${ ui.message("general.save") }
                                    </button>
                                    <button id="adminui-user-cancel" type="button" class="btn btn-sm btn-dark bg-dark float-right" data-dismiss="modal" onclick="pageReload()">
                                        ${ ui.message("Close") }
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addMflFacilitiesModal" tabindex="-1"
     data-controls-modal="addFacilitiesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addMflFacilitiesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addMflFacilitiesModalTitle">Add facilities</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                ${ui.includeFragment("botswanaemr", "admin/addMflFacilities")}
            </div>
        </div>
    </div>
</div>
