<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
    ui.includeJavascript("botswanaemr", "payment.js")
%>

<script type="text/javascript">
    var breadcrumbs = _.compact(_.flatten([
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        {
            label: "${ ui.message("registrationapp.registration.label") }",
            link: "${ ui.pageLink("botswanaemr", "regularRegistration") }"
        }
    ]));
</script>

<style>
.zero-margin {
    margin: 0px;
}

.horizontal-margin {
    margin: 0px 1.5rem 0px 0px;
}
</style>

<div id="validation-errors" class="note-container" style="display: none">
    <div class="note error">
        <div id="validation-errors-content" class="text">

        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="container-fluid center-content-container content-section">
                ${ui.includeFragment("botswanaemr", "capturePayment")}
            </div>
        </div>
    </div>
</div>