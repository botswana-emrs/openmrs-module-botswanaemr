<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard'},
        { label: "Screening Patient Pool"}
    ];
</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <h4 class="pl-0"> Screening and triage patient pool</h4>
                        </div>
                    </div>
                    <div id="screeningPool" class="table-responsive">
                        ${ui.includeFragment("botswanaemr", "screeningAndTriagePatientPool")}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>