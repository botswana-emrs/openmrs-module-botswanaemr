<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + getHomePage()
        },
        {label: "Print Visit Summary"}
    ];

    function getHomePage() {
        var locationUrl  = null;
        var locationUuid = '${currentServicePoint}';
        if (locationUuid === '${artServicesUuid}') {
            locationUrl =  '/botswanaemr/art/artDashboard.page';
        } else if (locationUuid === '${registrationPageUuid}') {
            locationUrl =  '/botswanaemr/registrationAdminDashboard.page?appId=botswanaemr.registrationAdminDashboard';
        } else if (locationUuid === '${pharmacyPortalUuid}') {
            locationUrl =  '/botswanaemr/pharmacy/pharmacyAllPrescriptions.page';
        } else if (locationUuid === '${doctorsPortalUuid}') {
            locationUrl =  '/botswanaemr/consultation/doctorsPatientPoolDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard';
        } else if (locationUuid === '${nursePortalUuid}') {
            locationUrl =  '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard';
        } else if (locationUuid === '${vmmcPortalUuid}') {
            locationUrl =  '/botswanaemr/vmmc/vmmcDashboard.page?appId=botswanaemr.vmmcDashboard';
        } else if (locationUuid === '${sexualReproductiveHealthPortalUuid}') {
            locationUrl =  '/botswanaemr/srh/srhDashboard.page';
        } else {
            locationUrl = '#';
        }
        return locationUrl;
    }

    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div class="row mt-3">
    <div class="col-md-9 pl-4">
        <button id="print" onclick="printContent('#visitSummaryCard');" class="btn btn-primary float-left">
            <i class="fa fa-print fa-1x"></i> Print Summary
        </button>
    </div>
    <div class="col-md-3 justify-content-end">
        ${ui.includeFragment("botswanaemr", "widget/cancelAndGoBack")}
    </div>
</div>

<div class="row mt-3">
    ${ui.includeFragment("botswanaemr", "visit/visitSummary")}
</div>
