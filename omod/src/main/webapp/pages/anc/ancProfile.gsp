<%
    if (sessionContext.authenticated && !sessionContext.currentProvider) {
        throw new IllegalStateException("Logged-in user is not a Provider")
    }
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>
<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {
            icon: "icon-home",
            link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'
        },
        {label: "Antenatal Profile"}
    ];
</script>
<script type="text/javascript">
    jq = jQuery;
    jQuery(function () {
        jQuery("#smartwizard").smartWizard({
            selected: 0,
            theme: 'dots',
            transition: {
                animation: 'none',
                speed: '400',
            },
            toolbarSettings: {
                showNextButton: false,
                showPreviousButton: false,
                overrideNextButton: true,
                overridePreviousButton: true,
                toolbarExtraButtons: [
                ]
            },
            keyboardSettings: {
                keyNavigation: false
            },
            lang: {
                next: 'Next',
                previous: 'Previous'
            },
            errorSteps: [],
        });

        let patientId = '${patient.id}';
        let visitId = '${visit.id}';
        let ancForms = JSON.parse('${results}');
        let selected = '${selected}';

        jq("#close-button").click(function (e) {
            e.preventDefault();
            let parent = document.referrer;
            var previousUrl = sessionStorage.getItem('previousUrl');
            var currentUrl = window.location.href;
            history.replaceState(null, '', currentUrl);
            window.location.href = parent;
        })

        if (ancForms.length > 0) {
            let index = 0;
            if (selected) {
                index = ancForms.findIndex(obj => obj.form === selected);
            }
            jq('#smartwizard').smartWizard("goToStep", index);
            setIframe(ancForms[index]);
        }

        function resizeIframe() {
            let mainSection = jq('#iframe').contents().find("#mainPageSection");
            jq('#iframe').height(mainSection.height());
        }

        function setIframe(ancForm) {

            const currentUrl = window.location.href;
            let url = "/${ui.contextPath()}/botswanaemr/srh/antenatalRegistryForm.page?patientId=" + patientId + "&visitId=" + visitId + "&formUuid=" + ancForm.form + "&encounterTypeUuid=" + ancForm.encounterType + "&returnUrl=" + currentUrl;
            jq("#iframe").attr("src", url);
            jq('#iframe').on('load', function() {
                resizeIframe();
            });
        }

        jq('#btnPrevious').on('click', function () {
            jq('#smartwizard').smartWizard("prev");
        });

        jq('#btnNext').on('click', function () {
            jq('#smartwizard').smartWizard("next");
        });

        jq("#smartwizard").on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
        });

        jq("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
            jq('#iframe').attr('src', '');
            if (ancForms[nextStepIndex]) {
                setIframe(ancForms[nextStepIndex]);
            }
            return true
        });
    });
</script>
<style>
    .tab-full {
        height: fit-content !important;
    }
    .form-group .text-danger {
        color: orange !important;
    }
    .error {
        color: orange !important;
    }
    .iti--allow-dropdown {
        width: 100% !important;
    }
    #iframe {
        width: 100%;
        border: none;
    }
</style>
<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="row mb-4">
                <div class="row col-sm-12">
                    <div class="col-6">
                        <h5>Antenatal Profile</h5>
                    </div>
                    <div class="col-6" style="display: flex;justify-content: flex-end;">
                        <button type="button" id="addAppointmentBtn" class="btn btn-sm btn-primary mb-3">
                            <a href="#" class="">
                                Lab & Pharmacy
                            </a>
                        </button>
                        <button type="button" id="clientProfile" class="btn btn-sm btn-primary mb-3 ml-3">
                            <a href="#" class="">
                                Client Profile
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div id="smartwizard" class="px-5">
                <ul class="nav">
                    <% ancForms.eachWithIndex { ancForm, index -> %>
                        <li class="nav-item">
                            <a class="nav-link" href="#step-${index++}">
                                <span class="badge badge-pill badge-primary">${index++}</span>
                                <small class="text-center text-danger">
                                    ${ancForm['title']}
                                </small>
                            </a>
                        </li>
                    <% } %>
                </ul>
                <button id="close-button" style="display: none"></button>

                <div class="row">
                    <div class="tab-content tab-full" style="width: 100%">
                        <iframe id="iframe"  src="" height="150"></iframe>
                    </div>
                </div>
                <div class="button-section">
                    <button id="btnPrevious" class="btn btn-dark bg-dark horizontal-button">Previous</button>
                    <button id="btnNext" class=" btn btn-primary horizontal-button">Next Step</button>
                </div>
            </div>
        </div>
    </div>
</div>