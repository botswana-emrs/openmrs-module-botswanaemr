<%
    ui.decorateWith("botswanaemr", "botswanaStandardEmrPage")
%>

<script type="text/javascript">
    var breadcrumbs = [
        {icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/registrationAdminDashboard.page'},
        {
            label: "${ ui.message('coreapps.app.systemAdministration.label')}",
            link: '${ui.pageLink("botswanaemr", "systemAdministration")}'
        },
        {
            label: "${ ui.message('Manage Facility Appointment Bookings')}",
            link: '${ui.pageLink("botswanaemr", "manageAccounts")}'
        }
    ];

    function addAppointmentBookingLimits() {
        jQuery("#addAppointmentBookingLimitsModal").modal('show');
    }

</script>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-header mb-4">&nbsp;
                        <div class="row">
                            <div class="col-8">
                                <h5>Manage Facility Appointment Bookings</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label"><span
                                class="text-dark">Name:</span> ${facilityName}
                            </label>
                        </div>&nbsp;
                        <div class="col">
                            <label class="form-label"><span
                                class="text-dark">MFL Code:</span> ${facilityMfl}
                            </label>
                        </div>
                        <div class="col p-r-0">
                            <button
                                    type="submit"
                                    onclick="addAppointmentBookingLimits()"
                                    class="btn btn-sm btn-primary mb-3 float-right">
                                ${ui.message("Set Booking Limits")}
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            ${ ui.includeFragment("botswanaemr", "appointmentBookingLimit")}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

