<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap(document, ['triageDataApp']);
        });
</script>

<table class="table table-striped">
    <tbody>
        <tr>
            <td>Patient ID</td>
            <td>${pin}</td>
        </tr>
        <tr>
            <td>Date of Birth</td>
            <td>${patient.patient.person.birthdate.format("dd-MMM-yyyy")}</td>
        </tr>
        <tr>
            <td>Sex</td>
            <td>${patientGender}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>${patient.patient.getAttribute('Email') ?: ''}</td>
        </tr>
        <tr>
            <% def occupation = patient.patient.getAttribute('Occupation') %>
            <% if (occupation) { %>
            <td>Occupation</td>
            <td>${occupation}</td>
            <% } %>
        </tr>
        <tr>
            <% def education = patient.patient.getAttribute('Education')?.getValue() ?: '' %>
            <% educationLevels.each { key, val -> %>
            <% if (education && education.toInteger() == val.answerConcept.id) { %>
            <td>Education</td>
            <td>${key.name}</td>
            <% } %>
            <% } %>
        </tr>
        <tr>
            <td>Name of Employer</td>
            <td>${patient.patient.getAttribute('Name of employer') ?: ''}</td>
        </tr>
        <tr>
            <td>Other Employment Sector</td>
            <td>${patient.patient.getAttribute('Other Employment Sector') ?: ''}</td>
        </tr>
        <% if (patient.patient.getAttribute('Telephone Number')) { %>
        <tr>
            <td>Telephone Number</td>
            <td>${patient.patient.getAttribute('Telephone Number')} 
                <% def numberType = patient.patient.getAttribute('Number Type') ?: '' %>
                <% if (numberType) { %> - ${numberType}<% } %>
            </td>
        </tr>
        <% } %>
        <tr>
            <td>Citizen Type</td>
            <td>${pin}</td>
        </tr>
        <tr>
            <% 
                def maritalStatus = patient.patient.getAttribute('Marital Status')
                def statusOptions = ["Single", "Married", "Divorced", "Widowed", "Separated"]
            %>
            <% statusOptions.each { status -> %>
            <% if (maritalStatus.toString() == status) { %>
            <td>Marital Status</td>
            <td><%= status %></td>
            <% } %>
            <% } %>
        </tr>
        <tr>
            <td>Patient-Type-At-Registration</td>
            <td>${pin}</td>
        </tr>
        <tr>
            <td>Identifier Type</td>
            <td>${identifierType}</td>
        </tr>
        <tr>
            <td>Identifier Value</td>
            <td>${identifierValue}</td>
        </tr>
        <tr>
            <td>Address</td>
            <td>
                [District, City, Ward]
                <% if(patientAddresses != null) { %>
                    <div>
                        <% patientAddresses.each { %>
                            <span class="text-dark">${it.value ?: ""},</span>
                        <% } %>
                    </div>
                <% } %>
            </td>
        </tr>
    </tbody>
</table>

<p>Contact Person Details</p>
<table class="table table-striped" ng-app="triageDataApp" ng-controller="TriageDataController" ng-init="getPatientNextOfKins('${patient.patient.id}')">
    <tbody>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>ID Type</td>
            <td>{{ nokAttributeMap.idType ? nokAttributeMap.idType.value : 'N/A' }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>ID Number</td>
            <td>{{ nokAttributeMap.idNumber ? nokAttributeMap.idNumber.value : 'N/A' }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>Full Name</td>
            <td>{{ nokAttributeMap.givenName }} {{ nokAttributeMap.middleName }} {{ nokAttributeMap.familyName }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>Relationship</td>
            <td>{{ nokAttributeMap.relationShip ? nokAttributeMap.relationShip.value : 'N/A' }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>District</td>
            <td>{{ nokAttributeMap.district || 'N/A' }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>City</td>
            <td>{{ nokAttributeMap.city || 'N/A' }}</td>
        </tr>
        <tr ng-repeat="nokAttributeMap in personNokAttributes">
            <td>Contact Information</td>
            <td>
                <ul class="list-group">
                    <li class="list-group-item" ng-repeat="(key, value) in nokAttributeMap" ng-show="value.value && key.includes('contact')">
                        <div class="row">
                            <div class="col">
                                <span>{{ toSentenceCase(key) }}:</span>
                            </div>
                            <div class="col">
                                {{ value.value }}
                            </div>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>