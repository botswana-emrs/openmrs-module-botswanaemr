<script>
    jq(function () {
        namesSet = true;
        jQuery.validator.addMethod("validate_email", function (value, element) {

            if(value) {
                if (validateEmail(value)) {
                    return true;
                } else {
                    return false;
                }
            }else {
                return true;
            }
        }, "Please enter a valid Email.");

        jQuery('#editPatientDetailsForm').validate({
            rules: {
                email: {
                    validate_email: true
                },
            }
        });

        jq("#editPatientDetailsForm").on("submit", function (e) {
            e.preventDefault();
            let valid = jQuery("#editPatientDetailsForm").valid();
            if (!valid) {
                return false;
            }

            if (jq("#phoneNumber").val()){
                let code = extraPhoneInstances["phoneNumber"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumberCode']").val(code);
            }
            if (jq("#phoneNumber1").val()){
                let code = extraPhoneInstances["phoneNumber1"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumber1Code']").val(code);
            }
            if (jq("#phoneNumber2").val()){
                let code = extraPhoneInstances["phoneNumber2"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumber2Code']").val(code);
            }
            if (jq("#phoneNumber3").val()){
                let code = extraPhoneInstances["phoneNumber3"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumber3Code']").val(code);
            }if (jq("#phoneNumber4").val()){
                let code = extraPhoneInstances["phoneNumber4"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumber4Code']").val(code);
            }
            if (jq("#phoneNumber5").val()){
                let code = extraPhoneInstances["phoneNumber5"].intlTelInput('getSelectedCountryData').dialCode;
                jq("input[name='phoneNumber5Code']").val(code);
            }

            let self = jq(this);
            jq("#editPatientDetailsForm").off("submit");
            self.unbind('submit');
            self.submit();
        });
    });
</script>

<script type="text/javascript">
    jq(document).ready(function() {
        let val = "${patient.patient.getAttribute('Telephone Number') ?: ''}";
        let altPhone1 = "${patient.patient.getAttribute('Alt_phone_number1') ?: ''}";
        let altPhone2 = "${patient.patient.getAttribute('Alt_phone_number2') ?: ''}";
        let altPhone3 = "${patient.patient.getAttribute('Alt_phone_number3') ?: ''}";
        let altPhone4 = "${patient.patient.getAttribute('Alt_phone_number4') ?: ''}";
        let altPhone5 = "${patient.patient.getAttribute('Alt_phone_number5') ?: ''}";
        let citizenType = "${patient.patient.getAttribute('Citizen Type') ?: ''}";

        let patientBirthDate = "${patient.patient.person.birthdate}";
        let birthDate = new Date(Date.parse(patientBirthDate));
        let today = new Date();
        let ageStr = "";
        if (today.getFullYear() > birthDate.getFullYear()) {
            let yearDiff = today.getFullYear() - birthDate.getFullYear();
            ageStr += yearDiff + " Years ";
            jq("#age").val(yearDiff);
        } else {
            jq("#age").val(0);
            let monthsDiff = today.getMonth() - birthDate.getMonth();
            if (monthsDiff > 0) {
                let monthDiff = monthsDiff;
                ageStr += monthDiff + " Months ";
                jq("#ageMonth").val(monthDiff);
            }

            let daysDiff = today.getDate() - birthDate.getDate();
            if (daysDiff => 7) {
                let weeksDiff = Math.floor(daysDiff / 7);
                ageStr += weeksDiff + " Weeks ";
                jq("#ageWeeks").val(weeksDiff);
            }
        }
        jq("#spanAge").html(ageStr);

        jq("#passportNumberLabel").hide();

        if (citizenType === "Non-citizen") {
            jq("#fgCountry").show();
        }

        function setValue(val, elem) {
            let input = jQuery("#" + elem)
            input.intlTelInput({
                initialCountry: "bw",
                hiddenInput: elem + "Code",
            });

            extraPhoneInstances[elem] = input;
            if (val) {
                const result = val.trim().split(" ");
                if (result.length > 1) {
                    let code = "+" + result[0];
                    input.intlTelInput('setNumber', code + result[1]);
                    jq("#" + elem).val(result[1]);
                }
            }
        }

        function setupTelListeners(elemId){
            jq("#" + elemId).on("countrychange", function () {
                let code = extraPhoneInstances[elemId].intlTelInput('getSelectedCountryData').dialCode;
                if (code === '267') {
                    jq("#" + elemId).attr("maxlength", "8");
                } else {
                    jq("#" + elemId).attr("maxlength", "15");
                }
            });
        }

        jq(window).on('load', function () {
            setValue(val, "phoneNumber");
            setValue(altPhone1, "phoneNumber1");
            setValue(altPhone2, "phoneNumber2");
            setValue(altPhone3, "phoneNumber3");
            setValue(altPhone4, "phoneNumber4");
            setValue(altPhone5, "phoneNumber5");

            setupTelListeners("phoneNumber1");
            setupTelListeners("phoneNumber2");
            setupTelListeners("phoneNumber3");
            setupTelListeners("phoneNumber4");
            setupTelListeners("phoneNumber5");
        });
    });

</script>

<div class="row">
    <div class="col">
        <h4>Basic Information</h4>
    </div>
    <div class="col">
        <button type="button" class="btn btn-sm btn-primary float-right" data-toggle="modal" data-target="#basicInfoModal">EDIT</button>
    </div>
</div>
<div class="row">
    <div class="col">
        <ul class="list-unstyled">
            <li class="py-0">
                <span>${identifierType} :
                    <span class="text-success">
                       ${identifierValue}
                    </span>
                </span>
            </li>
            <li class="py-0">
                <span>Name:
                    <span class="text-dark">
                        ${patient.patient.person.personName.familyName}&nbsp;${patient.patient.person.personName.givenName}&nbsp;${patient.patient.person.personName.middleName ?: ""}
                    </span>
                </span>
            </li>
            <li class="py-0">
                <span>Age:
                    <span class="text-dark" id="spanAge">
                        ${patient.patient.person.age}
                    </span>
                </span>
            </li>
            <li class="py-0">
                <span>Sex:
                    <span class="text-dark">
                        <% def gen = patient.patient.person.gender %>
                        <% if(gen.equals('M')) { %>
                             Male
                        <% } else if(gen.equals('F')) { %>
                             Female
                        <% } else {%>
                             Other
                        <% } %>
                    </span>
                </span>
            </li>
        </ul>
    </div>

    <div class="col">
        <ul class="list-unstyled">
            <li class="py-0">
                <span>Patient ID &nbsp;
                    <span class="text-danger">${pin}</span>
                </span>
            </li>
            <li class="py-0">
                <span>Registered Date:
                    <span class="text-dark">
                        ${patient.patient.person.personDateCreated}
                    </span>
                </span>
            </li>
            <% if (patient.patient.person.personAddress?.country) { %>
            <li class="py-0">
                <span> Country:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.country}
                    </span>
                </span>
            </li>
            <% } %>
            <li class="py-0">
                <span>District:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.address2}
                    </span>
                </span>
            </li>
            <% if (patient.patient.person.personAddress?.cityVillage) { %>
            <li class="py-0">
                <span> City/Town/Village:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.cityVillage}
                    </span>
                </span>
            </li>
            <% } %>
            <% if (patient.patient.person.personAddress?.address5) { %>
            <li class="py-0">
                <span> Ward/Locality:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.address5}
                    </span>
                </span>
            </li>
            <% } %>
            <% if (patient.patient.person.personAddress?.address6) { %>
            <li class="py-0">
                <span> Plot No.:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.address6}
                    </span>
                </span>
            </li>
            <% } %>
            <% if (patient.patient.person.personAddress?.address7) { %>
            <li class="py-0">
                <span> House/Unit/Apartment/Plot No.:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.address7}
                    </span>
                </span>
            </li>
            <% } %>
            <% if (patient.patient.person.personAddress?.address4) { %>
            <li class="py-0">
                <span>Other address information:
                    <span class="text-dark">
                        ${patient.patient.person.personAddress?.address4}
                    </span>
                </span>
            </li>
            <% } %>
            <li class="py-0">
                <span>Date Of Birth:
                    <span class="text-dark">
                        <%if (patient.patient.person.birthdate != null ) { %>
                            ${patient.patient.person.birthdate.format("dd-MMM-yyyy")}
                        <% } else { %>
                            Missing
                        <% } %>
                    </span>
                </span>
            </li>
        </ul>
    </div>
    <div class="col">
        <ul class="list-unstyled">
            <% if (patient.patient.getAttribute('Telephone Number')) { %>
                <li class="py-0">
                    <% def numberType =  patient.patient.getAttribute('Number Type') ?: '' %>
                    <span>Telephone Number : <span class="text-dark">${patient.patient.getAttribute('Telephone Number') } <% if (numberType) { %> - ${numberType} <% } %> </span></span>
                </li>
            <% } %>
            <%  patient.patient.person.getAllAttributeMap().sort{it.key}.grep{!it.key.startsWith("NOK")}.each{ %>
                <% if (it.key.contains("Alt_phone_number")) { %>
                    <li class="py-0">
                        <% def lastChar = it.key.charAt(it.key.length() -1) %>
                        <% def typeStr =  "Alt Number Type " + lastChar %>
                        <% def numType =  patient.patient.getAttribute(typeStr)?: '' %>
                        <span>${it.key} : <span class="text-dark">${it.value} <% if (numType) { %> - ${numType} <% } %></span></span>
                    </li>
                <% } %>
            <% } %>
            <%  patient.patient.person.getAllAttributeMap().grep{!it.key.startsWith("NOK")}.each{ %>
                <% if (it.key != 'LocationAttribute' && !it.key.contains("phone_number") && !it.key.contains("Telephone Number") && !it.key.contains("Number Type")) { %>
                    <li class="py-0">
                    <% if(it.key == 'Patient ART Regimen'){%>
                        <span>${it.key} : <span class="text-dark">${context.conceptService.getConceptByUuid(it.value.value).getDisplayString()}</span></span>
<% }                else {%> 
                        <span>${it.key} : <span class="text-dark">${it.value}</span></span>
<% }                %>
                    </li>
                <% } %>
            <% } %>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="basicInfoModal" tabindex="-1"
     data-controls-modal="basicInfoModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="basicInfoModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="basicInfoModalTitle">Patient's Basic Information</h4>
                <button type="button" id="editPatientDetails" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post"
                    id="editPatientDetailsForm"
                    action="${ ui.actionLink('botswanaemr', 'patientInformation', 'editPatientDetails', [patientId: patient.id])}">
                    <input type="hidden" id="patient" name="patient" value="${patient.id}"/>

                    <div class="form-group">
                        <label>Citizen Type :</label>

                        <% def citizenType = patient.patient.getAttribute('Citizen Type') %>
                        <% if(citizenType != null) {%>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="citizenType" value="Citizen" data-attr="Citizen"
                                <% if(citizenType.toString().equals("Citizen")) { %> checked <% } %> >
                            <label class="form-check-label radio-label" for="citizen">Citizen</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="citizenType" value="Non-citizen" data-attr="Non-citizen"
                                <% if(citizenType.toString().equals("Non-citizen")) { %> checked <% } %>  >
                            <label class="form-check-label radio-label" for="nonCitizen">Non-citizen</label>
                        </div>
                        <% }else{ %>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="citizenType" id="citizen" value="Citizen" data-attr="Citizen" checked>
                            <label class="form-check-label radio-label" for="citizen">Citizen</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="citizenType" id="nonCitizen" value="Non-citizen" data-attr="Non-citizen" >
                            <label class="form-check-label radio-label" for="nonCitizen">Non-citizen</label>
                        </div>
                        <% } %>
                    </div>

                    <div class="form-group">
                        <input id="idTypeHolder" type="hidden" class="form-control" value="${identifierType}"/>
                        <label>Select identification type</label>
                        <div id="identifierTypes" class="btn-group btn-group-toggle inline-button-group" data-toggle="buttons">
                            <label id="nationalIdLabel" class="btn btn-outline btn-primary">
                                <input type="radio" name="idType" value="nationalIdNumber" id="nokBtnId" autocomplete="off"> National ID
                            </label>
                            <label id="passportNumberLabel" class="btn btn-outline btn-primary">
                                <input type="radio" name="idType" value="passportNumber" id="nokBtnPassport" autocomplete="off"> Passport No.
                            </label>
                            <label id="birthCertificateNumberLabel" class="btn btn-outline btn-primary">
                                <input type="radio" name="idType" value="birthCertificateNumber" id="nokBtnBirthCertificate" autocomplete="off"> Birth Certificate No.
                            </label>
                            <label id="noIdentificationLabel" class="btn btn-outline btn-primary">
                                <input type="radio" name="idType" value="noIdentification" id="noIdentification" autocomplete="off"> No identification
                            </label>
                        </div>
                        <script type="text/javascript">
                            function setText(){
                                let text = "ID number";
                                if (patientIdType === "passportNumber"){
                                    text = "Passport Number"
                                } else if (patientIdType === "birthCertificateNumber"){
                                    text = "Birth Certificate Number"
                                }
                                jq("#idNumberLabel").html(text);
                            }
                            function resetPatientIdError() {
                                jq("#patientIdError").text('');
                                jq("#patientIdError").hide();
                            }

                            jq('input[type=radio][name=idType]').change(function() {
                                patientIdType = this.value;
                                resetPatientIdError();
                                if (patientIdType !== "passportNumber") {
                                    jq("#patientId").prop("type", "number");
                                    jq("#patientId").attr("minlength", "9");
                                    jq("#patientId").attr("maxlength", "9");
                                } else {
                                    jq("#patientId").prop("type", "text");
                                    jq("#patientId").removeAttr("minlength");
                                    jq("#patientId").removeAttr("maxlength");
                                }
                                setText();

                                if (patientIdType !== "noIdentification") {
                                    jq("#patientId").attr("required", true);
                                    jq("#address2").attr("required", true);
                                    jq("#cityVillage").attr("required", true);
                                    jq("#phoneNumber").attr("required", true);
                                    jq(".temp-label").show();
                                } else {
                                    jq("#patientId").attr("required", false);
                                    jq("#address2").attr("required", false);
                                    jq("#cityVillage").attr("required", false);
                                    jq("#phoneNumber").attr("required", false);
                                    jq(".temp-label").hide();
                                }
                            });
                            jq(document).ready(function() {
                                let identifier = "${identifierType}";
                                let citizenType  = jq('input[name="citizenType"]:checked').val();
                                if (identifier === "National ID" && citizenType === "Citizen") {
                                    jq("#nationalIdLabel").toggleClass("active");
                                    jq("#patientId").prop("type", "number");
                                    jq("#nokBtnId").prop("checked", true);
                                    resetPatientIdError();
                                } else if (identifier === "Passport Number" && citizenType === "Non-citizen")  {
                                    jq("#passportNumberLabel").toggleClass("active");
                                    jq("#patientId").prop("type", "text");
                                    jq("#nokBtnPassport").prop("checked", true);
                                    patientIdType = "passportNumber";
                                    resetPatientIdError();
                                } else if (identifier === "Birth Certificate Number" && citizenType === "Citizen") {
                                    jq("#birthCertificateNumberLabel").toggleClass("active");
                                    jq("#patientId").prop("type", "text");
                                    jq("#nokBtnBirthCertificate").prop("checked", true);
                                    patientIdType = "birthCertificateNumber";
                                    resetPatientIdError();
                                } else if (identifier === "") {
                                    jq('input:radio').change(function() {
                                        let citizenType  = jq('input[name="citizenType"]:checked').val();
                                        if (jq(this).val() === 'nationalIdNumber' && citizenType === "Citizen") {
                                            jq().button('toggle');
                                            jq("#patientId").prop("type", "number");
                                            resetPatientIdError();
                                        } else if (jq(this).val() === 'passportNumber' && citizenType === "Non-citizen") {
                                            jq().button('toggle');
                                            jq("#patientId").prop("type", "text");
                                            patientIdType = "passportNumber";
                                            resetPatientIdError();
                                        } else if (jq(this).val() === 'birthCertificateNumber' && citizenType === "Citizen") {
                                            jq().button('toggle');
                                            jq("#patientId").prop("type", "text");
                                            patientIdType = "birthCertificateNumber";
                                            resetPatientIdError();
                                        }else if (jq(this).val() === 'noIdentification') {
                                            jq().button('toggle');
                                            patientIdType = "noIdentification";
                                            resetPatientIdError();
                                        }
                                        if (patientIdType !== "noIdentification") {
                                            jq("#patientId").attr("required", true);
                                        } else {
                                            jq("#patientId").attr("required", false);
                                        }
                                    });
                                } else {
                                    identifier === "" ? jq('#idTypeError').show() : jq('#idTypeError').hide();
                                }

                                if (patientIdType !== "passportNumber") {
                                    jq("#patientId").prop("type", "number");
                                    jq("#patientId").attr("minlength", "9");
                                    jq("#patientId").attr("maxlength", "9");
                                } else {
                                    jq("#patientId").prop("type", "text");
                                    jq("#patientId").removeAttr("minlength");
                                    jq("#patientId").removeAttr("maxlength");
                                }
                                if (patientIdType !== "noIdentification") {
                                    jq("#patientId").attr("required", true);
                                } else {
                                    jq("#patientId").attr("required", false);
                                }

                                jq('#noIdentificationLabel').on('click', function() {
                                        jq("#patientId").attr("required", false);
                                        jq("#address2").attr("required", false);
                                        jq("#cityVillage").attr("required", false);
                                        jq("#phoneNumber").attr("required", false);
                                        jq(".temp-label").hide();
                                });

                            });
                        </script>

                        <label for="patientId"><span class="black-text" id="idNumberLabel">ID number</span>
                            <span class="text-danger temp-label">*</span>
                        </label>

                        <div class="form-group is-loading">
                            <input type="text"
                                   class="form-control spinner-text"
                                   id="patientId"
                                   name="idNumber"
                                   required
                                   value="${identifierValue}"
                                   placeholder="Enter the patient's ID number" minlength="9" maxlength="9">

                            <div class="spinner-border spinner-section" role="status" id="patientSpinner">
                                <span class="sr-only">Loading...</span>
                            </div>
                            <label id="patientIdError"
                                   class="form-text text-danger">ID not found in registry. Register patient manually.</label>
                        </div>
                    </div>

                    <div class="form-group" id="fgCountry">
                        <label>Country
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text"
                               class="form-control"
                               id="country"
                               name="country"
                               required
                               placeholder="Patient's Country"
                               value="${patient.patient.person.personAddress?.country ?: ''}"/>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col pl-0 pr-0">
                                <label for="givenName">First name
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" id="givenName" aria-describedby="nameHelp"
                                       name="givenName"
                                       value="${patient.patient.person.personName.givenName}"
                                       required placeholder="Enter the first name">
                            </div>
                            <div class="col">
                                <label for="middleName">Middle name
                                </label>
                                <input type="text" class="form-control" id="middleName" aria-describedby="nameHelp"
                                       name="middleName"
                                       value="${patient.patient.person.personName.middleName == null ? '': patient.patient.person.personName.middleName}"
                                       placeholder="Enter the middle name">
                            </div>
                            <div class="col pr-0 pl-0">
                                <label for="familyName">Last name
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" id="familyName" aria-describedby="nameHelp"
                                       name="familyName"
                                        value="${patient.patient.person.personName.familyName}"
                                       required placeholder="Enter the last name">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="gender"> Gender
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control custom-select" id="gender" name="gender" required>
                            <% def gender = patient.patient.person.gender %>
                            <% if(gender.equals('F')) {%>
                                 <option selected value="Female">Female</option>
                                 <option value="Male">Male</option>
                                 <option value="Other">Other</option>
                            <% }else if(gender.equals('M')) { %>
                                 <option selected value="Male">Male</option>
                                 <option value="Female">Female</option>
                                 <option value="Other">Other</option>
                            <% }else{ %>
                                 <option selected disabled>Other</option>
                                 <option value="Female">Female</option>
                                 <option value="Male">Male</option>
                            <% } %>
                        </select>
                        <label id="genderError"
                               class="form-text text-danger">Wrong gender selected.
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="dateOfBirth">Date Of Birth
                            <span class="text-danger">*</span>
                        </label>
                        <input required
                               type="text"
                               class="form-control datepicker"
                               value="${patient.patient.person.birthdate?patient.patient.person.birthdate.format('yyyy-MM-dd'):''}"
                               id="dateOfBirth"
                               name="dateOfBirth"
                               placeholder="Patient's date of birth"
                        />
                        <script type="text/javascript">
                             jq('#dateOfBirth').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                showButtonPanel: true,
                                "setDate": new Date(),
                                dateFormat: "yy-mm-dd",
                                 yearRange: "-150:+0",
                                 maxDate: 0,
                                "autoclose": true,
                                 onSelect: function (data) {
                                     jq("#dateOfBirth").trigger('change');
                                 }
                             });
                        </script>
                    </div>

                    <div class="form-group">
                        <input class="form-check-input" name="dobEstimate" type="checkbox" id="dobEstimate"
                        <% if(patient.patient.person.birthdateEstimated) { %> checked <% } %> >
                        <label class="form-check-label" for="dobEstimate"
                               style="margin-top: auto">
                            Date of birth estimated?
                        </label>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col pl-0 pr-0">
                                <label for="age">Age
                                    <span class="required-label">*</span>
                                </label>
                                <input type="number" class="form-control" id="age" name="age" min="0"
                                       required placeholder="Years">
                            </div>

                            <div class="col pl-0 pr-0 age-div" id="months">
                                <label for="ageMonth">Months
                                </label>
                                <input type="number" class="form-control" id="ageMonth" name="ageMonth" min="0"
                                       placeholder="Months">
                            </div>

                            <div class="col pl-0 pr-0 age-div" id="weeks">
                                <label for="ageWeeks">Weeks
                                </label>
                                <input type="number" class="form-control" id="ageWeeks" name="ageWeeks" min="0"
                                       placeholder="Weeks">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="maritalStatus">Marital Status
                        </label>
                        <select type="text"
                                class="form-control custom-select"
                                id="maritalStatus"
                                name="maritalStatus" >
                            <% def maritalStatus = patient.patient.getAttribute('Marital Status') %>
                            <option disabled selected>Select the patient's marital status</option>
                            <option value="Single" <% if(maritalStatus.toString() == "Single") { %> selected <% } %> >Single</option>
                            <option value="Married" <% if(maritalStatus.toString()  == "Married") { %> selected <% } %> >Married</option>
                            <option value="Divorced" <% if(maritalStatus.toString()  == "Divorced") { %> selected <% } %> >Divorced</option>
                            <option value="Widowed" <% if(maritalStatus.toString()  == "Widowed") { %> selected <% } %> >Widowed</option>
                            <option value="Separated" <% if(maritalStatus.toString()  == "Separated") { %> selected <% } %> >Separated</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="email"> Email </label>
                        <input id="email"
                               name="email"
                               type="email"
                               maxlength="250"
                               class="form-control"
                               placeholder="Patient's email address"
                               value="${patient.patient.getAttribute('Email') ?: ''}"
                        />
                     </div>

                    <div class="form-group">
                        <div class="row">
                            <label for="phoneNumber"> Telephone Number
                                <span class="text-danger temp-label">*</span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-3 pl-0 pr-0 type-col">
                                <select class="form-control custom-select" id="numberType" name="numberType"
                                        style="min-width: fit-content;">
                                    <% def numberType = patient.patient.getAttribute('Number Type') ?: '' %>
                                    <option <% if(numberType.toString() == "Mobile") { %> selected <% } %> >Mobile</option>
                                    <option <% if(numberType.toString() == "Telephone") { %> selected <% } %> >Telephone</option>
                                </select>
                            </div>
                            <div class="col pl-0 pr-0">
                                <input required
                                       id="phoneNumber"
                                       name="telephoneNumber"
                                       type="tel"
                                       minlength="7"
                                       maxlength="8"
                                       class="form-control"
                                       placeholder="Patient's phone number"
                                       oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                       oninput="this.setCustomValidity('')"
                                       value="${patient.patient.getAttribute('Telephone Number') ?: ''}"
                                />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input class="form-check-input" type="checkbox" value="" id="phoneNumberNa">
                        <label class="form-check-label" id="phoneNumberNaLabel" for="phoneNumberNa"
                               style="margin-top: auto">
                            Confirm the patient does not have a phone number
                            <span class="required-label">*</span>
                        </label>
                    </div>

                    <div class="contact-template">
                        <div class="form-group">
                            <div class="row">
                                <label>Contact number
                                    <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="row">
                                <div class="col-3 pl-0 pr-0 type-col">
                                    <select class="form-control custom-select" id="extraNumberType" name="extraNumberType"
                                            style="min-width: fit-content;">
                                        <option>Mobile</option>
                                        <option>Telephone</option>
                                    </select>
                                </div>

                                <div class="col pl-0 pr-0">
                                    <div class="text-icon-div">
                                        <input type="tel" class="form-control" id="extraPhone" name="extraPhone"
                                               required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                        <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete"></i>
                                    </div>
                                </div>
                            </div>

                            <label id="extraPhoneError" class="error" for="extraPhone"></label>
                        </div>
                    </div>

                    <div id="extraContacts">
                        <% if (patient.patient.getAttribute('Alt_phone_number1') != null) { %>
                        <div class="form-group extraContact" id="extraContact">
                            <div class="row">
                                <label>Contact number
                                    <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="row">
                                <div class="col-3 pl-0 pr-0 type-col">
                                    <select class="form-control custom-select" id="extraNumberType1" name="extraNumberType1"
                                            style="min-width: fit-content;">
                                        <% def numberType1 = patient.patient.getAttribute('Alt Number Type 1') ?: '' %>
                                        <option <% if(numberType1.toString() == "Mobile") { %> selected <% } %> >Mobile</option>
                                        <option <% if(numberType1.toString() == "Telephone") { %> selected <% } %> >Telephone</option>
                                    </select>
                                </div>

                                <div class="col pl-0 pr-0">
                                    <div class="text-icon-div">
                                        <input type="tel" class="form-control" id="phoneNumber1" name="phoneNumber1"
                                               oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                               oninput="this.setCustomValidity('')"
                                               value="${patient.patient.getAttribute('Alt_phone_number1') ?: ''}"
                                               required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                        <i class="fa fa-trash-o  fa-2x delete-icon" id="deletePhone1"></i>
                                    </div>
                                </div>
                            </div>
                            <label id="phoneNumber1-error" class="error" for="phoneNumber1"></label>
                        </div>
                        <% } %>

                        <% if (patient.patient.getAttribute('Alt_phone_number2') != null) { %>
                        <div class="form-group extraContact" id="extraContact">
                            <div class="row">
                                <label>Contact number
                                    <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="row">
                                <div class="col-3 pl-0 pr-0 type-col">
                                    <select class="form-control custom-select" id="extraNumberType2" name="extraNumberType2"
                                            style="min-width: fit-content;">
                                        <% def numberType2 = patient.patient.getAttribute('Alt Number Type 2') ?: '' %>
                                        <option <% if(numberType2.toString() == "Mobile") { %> selected <% } %> >Mobile</option>
                                        <option <% if(numberType2.toString() == "Telephone") { %> selected <% } %> >Telephone</option>
                                    </select>
                                </div>

                                <div class="col pl-0 pr-0">
                                    <div class="text-icon-div">
                                        <input type="tel" class="form-control" id="phoneNumber2" name="phoneNumber2"
                                               oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                               oninput="this.setCustomValidity('')"
                                               value="${patient.patient.getAttribute('Alt_phone_number2') ?: ''}"
                                               required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                        <i class="fa fa-trash-o  fa-2x delete-icon" id="deletePhone2"></i>
                                    </div>
                                </div>
                            </div>
                            <label id="phoneNumber2-error" class="error" for="phoneNumber2"></label>
                        </div>
                        <% } %>

                        <% if (patient.patient.getAttribute('Alt_phone_number3') != null) { %>
                        <div class="form-group extraContact" id="extraContact">
                            <div class="row">
                                <label>Contact number
                                    <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="row">
                                <div class="col-3 pl-0 pr-0 type-col">
                                    <select class="form-control custom-select" id="extraNumberType3" name="extraNumberType3"
                                            style="min-width: fit-content;">
                                        <% def numberType3 = patient.patient.getAttribute('Alt Number Type 3') ?: '' %>
                                        <option <% if(numberType3.toString() == "Mobile") { %> selected <% } %> >Mobile</option>
                                        <option <% if(numberType3.toString() == "Telephone") { %> selected <% } %> >Telephone</option>
                                    </select>
                                </div>

                                <div class="col pl-0 pr-0">
                                    <div class="text-icon-div">
                                        <input type="tel" class="form-control" id="phoneNumber3" name="phoneNumber3"
                                               oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                               oninput="this.setCustomValidity('')"
                                               value="${patient.patient.getAttribute('Alt_phone_number3') ?: ''}"
                                               required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                        <i class="fa fa-trash-o  fa-2x delete-icon" id="deletePhone3"></i>
                                    </div>
                                </div>
                            </div>
                            <label id="phoneNumber3-error" class="error" for="phoneNumber3"></label>
                        </div>
                        <% } %>

                        <% if (patient.patient.getAttribute('Alt_phone_number4') != null) { %>
                        <div class="form-group extraContact" id="extraContact">
                            <div class="row">
                                <label>Contact number
                                    <span class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="row">
                                <div class="col-3 pl-0 pr-0 type-col">
                                    <select class="form-control custom-select" id="extraNumberType4" name="extraNumberType4"
                                            style="min-width: fit-content;">
                                        <% def numberType4 = patient.patient.getAttribute('Alt Number Type 4') ?: '' %>
                                        <option <% if(numberType4.toString() == "Mobile") { %> selected <% } %> >Mobile</option>
                                        <option <% if(numberType4.toString() == "Telephone") { %> selected <% } %> >Telephone</option>
                                    </select>
                                </div>

                                <div class="col pl-0 pr-0">
                                    <div class="text-icon-div">
                                        <input type="tel" class="form-control" id="phoneNumber4" name="phoneNumber4"
                                               oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                               oninput="this.setCustomValidity('')"
                                               value="${patient.patient.getAttribute('Alt_phone_number4') ?: ''}"
                                               required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                        <i class="fa fa-trash-o  fa-2x delete-icon" id="deletePhone4"></i>
                                    </div>
                                </div>
                            </div>
                            <label id="phoneNumber4-error" class="error" for="phoneNumber4"></label>
                        </div>
                        <% } %>

                        <% if (patient.patient.getAttribute('Alt_phone_number5') != null) { %>
                        <div class="form-group extraContact" id="extraContact">
                            <label>Contact number
                                <span class="text-danger">*</span>
                            </label>

                            <div class="text-icon-div">
                                <input type="tel" class="form-control" id="phoneNumber5" name="phoneNumber5"
                                       oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                       oninput="this.setCustomValidity('')"
                                       value="${patient.patient.getAttribute('Alt_phone_number5') ?: ''}"
                                       required placeholder="Patient's phone number" minlength="7" maxlength="8">
                                <i class="fa fa-trash-o  fa-2x delete-icon" id="deletePhone5"></i>
                            </div>
                            <label id="phoneNumber5-error" class="error" for="phoneNumber5"></label>
                        </div>
                        <% } %>
                    </div>

                    <button class="dashed-button rounded"
                            id="btnAddContact">+ Add another contact number</button>

                    <% if (isBf) { %>
                    <div class="form-group">
                        <label for="affiliation">Patient Affiliation
                        </label>
                        <select type="text"
                                class="form-control custom-select"
                                id="affiliation"
                                name="affiliation" >
                            <% def affiliation = patient.patient.getAttribute('Patient Affiliation') %>
                            <option disabled selected>Select the patient's affiliation</option>
                            <option value="Civilian" <% if(affiliation.toString() == "Civilian") { %> selected <% } %> >Civilian</option>
                            <option value="Force Officer" <% if(affiliation.toString()  == "Force Officer") { %> selected <% } %> >Force Officer</option>
                            <option value="Dependent to a force officer" <% if(affiliation.toString()  == "Dependent to a force officer") { %> selected <% } %> >Dependent to a force officer</option>
                        </select>
                    </div>

                    <div class="form-group" id="fgForceNumber">
                        <label for="forceNumber">Force Number
                        </label>
                        <input
                                id="forceNumber"
                                name="forceNumber"
                                type="text"
                                maxlength="250"
                                class="form-control"
                                placeholder="Force Number"
                                value="${patient.patient.getAttribute('Force Number') ?: ''}"
                        />
                    </div>

                    <div class="form-group" id="fgRank">
                        <label for="rank"> Rank
                        </label>
                        <input
                                id="rank"
                                name="rank"
                                type="text"
                                maxlength="250"
                                class="form-control"
                                placeholder="Rank"
                                value="${patient.patient.getAttribute('Rank') ?: ''}"
                        />
                    </div>
                    <% } %>

                    <div class="form-group">
                        <label>District
                            <span class="text-danger temp-label">*</span>
                        </label>
                        <select type="text"
                                class="form-control custom-select"
                                id="address2" required
                                name="address2">
                            <% def district = patient.patient.person.personAddress?.address2 ?: '' %>
                            <% if (district) { %>
                            <option value="${district}">${district}</option>
                            <% } else { %>
                            <option selected disabled value="">Select District</option>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>City/Town/Village
                            <span class="text-danger temp-label">*</span>
                        </label>
                        <input type="text"
                               class="form-control"
                               id="cityVillage"
                               name="cityVillage"
                               placeholder="Patient's City"
                               required
                               value="${patient.patient.person.personAddress?.cityVillage ?: ''}"/>
                    </div>
                    <script type="text/javascript">
                        jq("#cityVillage").autocomplete({
                            source: cities,
                            appendTo: "#editPatientDetailsForm"
                        });
                    </script>

                    <div class="form-group">
                        <label>Ward/Locality
                        </label>
                        <input type="text"
                               class="form-control"
                               id="address5"
                               name="address5"
                               placeholder="Patient's Ward/Locality"
                               value="${patient.patient.person.personAddress?.address5 ?: ''}"/>
                    </div>

                    <div class="form-group">
                        <label>House/Unit/Apartment/Plot No
                        </label>
                        <input type="text"
                               class="form-control"
                               id="address7"
                               name="address7"
                               placeholder="Patient's House/Unit/Apartment/Plot No"
                               value="${patient.patient.person.personAddress?.address7 ?: ''}"/>
                    </div>

                    <div class="form-group">
                        <label>Other address information e.g Landmark
                        </label>
                        <input type="text"
                               class="form-control"
                               id="address4"
                               name="address4"
                               placeholder="Patient's Landmark"
                               value="${patient.patient.person.personAddress?.address4 ?: ''}"/>
                    </div>

                     <div class="form-group">
                        <label for="occupation">Occupation
                        </label>
                        <select type="text"
                                class="form-control custom-select"
                                id="occupation"
                                name="occupation"
                                >
                                <% def occupation = patient.patient.getAttribute('Occupation') %>
                                <% if(occupation) {%>
                                     <option value="${occupation}">${occupation}</option>
                                <% }else{ %>
                                     <option selected disabled value="">Patient's Occupation</option>
                                <% } %>
                        </select>
                         <input type="text" class="form-control" id="otherOccupation" name="otherOccupation"
                                placeholder="Enter the occupation">
                    </div>

                    <div class="form-group">
                        <label for="education"> Education Level
                        </label>

                        <select type="text" class="form-control custom-select" id="education"
                                name="education">
                            <% def education = patient.patient.getAttribute('Education')?.getValue() ?: '' %>
                                <option  value="">Select patient's education level </option>
                            <%  educationLevels.each {  key, val -> %>
                                <% if(education && education.toInteger() == val.answerConcept.id) {%>
                                <option value="${val.answerConcept.id}" selected>${key.name}</option>
                                <% }else{ %>
                                <option value="${val.answerConcept.id}" >${key.name}</option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nameOfEmployer"> Name Of Employer
                        </label>
                        <input
                               id="nameOfEmployer"
                               name="nameOfEmployer"
                               type="text"
                               maxlength="250"
                               class="form-control"
                               placeholder="Name of employer"
                               value="${patient.patient.getAttribute('Name of employer') ?: ''}"
                        />
                    </div>

                    <div class="form-group">
                        <label for="employmentSector"> Employment Sector
                        </label>

                        <select type="text" class="form-control custom-select" id="employmentSector"
                                name="employmentSector">
                            <% def sector = patient.patient.getAttribute('Employment Sector')?.getValue() ?: '' %>
                            <option  value="">Select patient's employment sector </option>
                            <%  employmentSectors.each{ %>
                                <% if(sector && sector.toInteger() == it.getId()) {%>
                                <option value="${it.getId()}" selected>${it.getName().getName()}</option>
                                <% }else{ %>
                                <option value="${it.getId()}" >${it.getName().getName()}</option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group" id="otherEmploymentSectorGrp">
                        <label for="otherEmploymentSector"> Other employment sector
                        </label>
                        <input
                                id="otherEmploymentSector"
                                name="otherEmploymentSector"
                                type="text"
                                maxlength="250"
                                class="form-control"
                                placeholder="Specify other emplyment sector"
                                value="${patient.patient.getAttribute('Other Employment Sector') ?: ''}"
                        />
                    </div>

                    <span style="display: none">
                        ${ui.includeFragment("botswanaemr", "widget/facilityLocation")}
                    </span>
                     <div class="row">
                        <div class="col pr-0">
                            <div class="col pr-0">
                                <button type="submit"
                                        class="btn btn-sm btn-primary float-right ml-1">Save
                                </button>
                            </div>
                            <button type="button"
                                    class="btn btn-sm btn-dark bg-dark float-right"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
         jQuery("#basicInfoModal").appendTo("body");
    });
</script>

