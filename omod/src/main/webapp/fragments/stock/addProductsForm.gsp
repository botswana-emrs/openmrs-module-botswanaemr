<script type="text/javascript">
    jQuery.noConflict();
    jq(document).ready(function() {
        var concepts = ${conceptNames}
        jq(".concept").autocomplete({
            source: concepts,
            change: function (event, ui) {
                if(!ui.item){
                   jq(".concept").val("");
                }
		    }
        });

        jq('.concept').keyup(function(event) {
            var textBox = event.target;
            var start = textBox.selectionStart;
            var end = textBox.selectionEnd;
            textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
            textBox.setSelectionRange(start, end);
        });

        jq("#drug").autocomplete({
            source: function(request, response) {
                jq.getJSON('${ ui.actionLink("botswanaemr", "stock/addProductsForm", "getDrugs") }', {
                    q: request.term
                }).success(function(data) {
                    var results = [];
                    for (var i in data) {
                        var result = {
                            label: data[i].name,
                            value: data[i].uuid
                        };
                        results.push(result);
                    }
                    response(results);
                });
            },
            minLength: 3,
            select: function (event, ui) {
                event.preventDefault();
                jq("#drug").val(ui.item.label);
                jq("#drugUuid").val(ui.item.value);

            }
        });

        jq("#category").change(function() {
            if (jq(this).val() === 'DRUG') {
                jq("#drug").attr("disabled", false).attr("required", true).show();
                jq("#concept").attr("disabled", true).attr("required", false).hide().val('');
            } else {
                jq("#drug").attr("disabled", true).attr("required", false).hide().val('');
                jq("#concept").attr("disabled", false).attr("required", true).show();
            }
        });

        if(jq("#category").val() === 'DRUG') {
            jq("#drug").attr("disabled", false).attr("required", true).show();
            jq("#concept").attr("disabled", true).attr("required", false).hide();
        } else {
            jq("#drug").attr("disabled", true).attr("required", false).hide();
            jq("#concept").attr("disabled", false).attr("required", true).show();
        }


        jq(function() {
            jq("#addItemForm").submit(function(e){
                e.preventDefault();
                var params = {
                    'name': jq('#name').val(),
                    'code': jq('#code').val(),
                    'status': jq('#status').val(),
                    'category': jq('#category').val(),
                    'departmentId': jq('#departmentId').val(),
                    'conceptName': jq('#concept').val(),
                    'description': jq('#description').val(),
                    'unitOfIssue': jq('#unitOfIssue').val(),
                    'drugUuid': jq('#drugUuid').val()
                };
                console.log("params", params);

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/addProductsForm", "saveCreatedProducts")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Product saved successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/products")}';
                    })
                    .fail(function (err) {
                            jq().toastmessage('showErrorToast', err);
                            console.log(err)
                        }
                    )
            });

            jq("#editItemForm").submit(function(e){
                e.preventDefault();
                var params = {
                    'name': jq('#itemName').val(),
                    'code': jq('#itemCode').val(),
                    'itemId': ${itemId},
                    'status': jq('#itemStatus').val(),
                    'category': jq('#category').val(),
                    'departmentId': jq('#departmentId').val(),
                    'conceptName': jq('#concept').val(),
                    'description': jq('#itemDescription').val(),
                    'unitOfIssue': jq('#unitOfIssue').val(),
                    'drugUuid': jq('#drugUuid').val()
                };

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/addProductsForm", "saveEditedProducts")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Product updated successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/products")}';
                    })
                    .fail(function (err) {
                        console.log(err);
                            jq().toastmessage('showErrorToast', "Error: Unable to update product");
                            console.log(err)
                        }
                    )
            });
	    });
    });
</script>

<% if(inventoryItem == "") {%>
    <div class="card mt-4">
        <div class="card-body">
            <form method="post" id="addItemForm">
                <button type="submit" id="addProductBtn" class="btn btn-primary p-2 mt-1 float-right"> + Add Product</button>
                <h5 class="text-primary">PRODUCT DETAILS</h5>
                <hr class="divider pt-1 bg-primary"/>

                <div class="form-row">
                    <div class="form-group col-md-4 pr-3">
                        <label for="name">Product Name:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name"/>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="code"> Code:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="code" name="code" class="form-control" placeholder="Code" required/>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="unitOfIssue"> Unit:
                            <span class="text-danger">*</span>
                        </label>
                        <select name="unitOfIssue" id="unitOfIssue" class="form-control" required>
                            <option value="">Select one</option>
                            <% if (unitsOfIssue != null) {
                                unitsOfIssue.each { %>
                            <option value="${it.id}">${it.name}</option>
                            <%
                                        }
                                    }
                            %>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4 pr-3">
                        <label for="status"> Status:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="custom-select" id="status" name="status" required>
                            <option value="">Select one</option>
                            <option value="ACTIVE">Active</option>
                            <option value="NOT_ACTIVE">Not Active</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="departmentId"> Program/Section:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="custom-select" id="departmentId" name="departmentId" required>
                            <option value="">Select one</option>
                            <% departments.each { %>
                            <option value="${it.id}">${it.name}</option>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="category"> Category:
                            <span class="text-danger">*</span>
                        </label>
                        <select name="category" id="category" class="form-control" required>
                            <option value="">Select one</option>
                            <% if (categories != null) {
                                categories.each { %>
                            <option value="${it}">${it}</option>
                            <%
                                        }
                                    }
                            %>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="drug">Select drug/Concept:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="hidden" id="drugUuid" name="drugUuid" />
                        <input type="text" id="drug" name="drug" class="form-control" placeholder="Select Drug" />
                        <input type="text" id="concept" name="concept" class="form-control concept" placeholder="Select Concept"/>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Description:
                            <span class="text-danger">*</span>
                        </label>
                        <textarea class="form-control" rows="2"  id="description" name="description"></textarea>
                    </div>
                </div>
                <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
            </form>
        </div>
    </div>
<% } else { %>
    <div class="card mt-4">
        <div class="card-body">
            <form method="post" id="editItemForm">
                <button type="submit" id="editProductBtn" class="btn btn-primary p-2 mt-1 float-right"> + Update Product</button>
                <h5 class="text-primary">PRODUCT DETAILS</h5>
                <hr class="divider pt-1 bg-primary"/>
                <input type="number" id="itemId" name="itemId" value="${inventoryItem.id ?: ''}" class="form-control" style="display: none"/>
                <div class="form-row">
                    <div class="form-group col-md-4 pr-3">
                        <label for="name">Product Name:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="itemName" name="name" class="form-control" placeholder="Name" value="${inventoryItem.name ?: ''}"/>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="code"> Code:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="itemCode" name="code" class="form-control" placeholder="Code" value="${inventoryItem.code ?: ''}" required/>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="unitOfIssue"> Unit:
                            <span class="text-danger">*</span>
                        </label>
                        <select name="unitOfIssue" id="unitOfIssue" class="form-control" required>
                            <option value="">Select one</option>
                            <% if (unitsOfIssue != null) {
                                unitsOfIssue.each { %>
                            <option value="${it.id}" <% if(inventoryItem?.unitOfIssueId.equals(it.id)) { %> selected <% } %> >${it.name}</option>
                            <%
                                        }
                                    }
                            %>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4 pr-3">
                        <label for="status"> Status:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="custom-select" id="itemStatus" name="status" required>
                            <option value="">
                                Select one
                            </option>
                            <option value="ACTIVE" <% if(inventoryItem?.status == "ACTIVE") { %> selected <% } %>>Active</option>
                            <option value="NOT_ACTIVE" <% if(inventoryItem?.status == "NOT_ACTIVE") { %> selected <% } %>>Not Active</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="departmentId"> Program/Section:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="custom-select" id="departmentId" name="departmentId" required>
                            <option value="">Select one</option>
                            <% departments.each { %>
                            <option value="${it.id}" <% if(inventoryItem?.department == it.name) { %> selected <% } %> >${it.name}</option>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group col-md-4 pl-3">
                        <label for="category"> Category:
                            <span class="text-danger">*</span>
                        </label>
                        <select name="category" id="category" class="form-control" required>
                            <option value="">Select one</option>
                            <% if (categories != null) {
                                categories.each { %>
                            <option value="${it}" <% if (inventoryItem.category == it) { %>Selected<% } %> >${it}</option>
                            <%
                                        }
                                    }
                            %>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="drug">Select drug/Concept:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="hidden" id="drugUuid" name="drugUuid" value="${inventoryItem.drugUuid ?: ''}" />
                        <input type="text" id="drug" name="drug" class="form-control" placeholder="Select Drug" value="${inventoryItem.drugName}" />
                        <input type="text" id="concept" name="concept" class="form-control concept" placeholder="Select Concept"  value="${inventoryItem.conceptName}"/>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="itemDescription">Description:
                            <span class="text-danger">*</span>
                        </label>
                        <textarea class="form-control" rows="2"  id="itemDescription" name="description">${inventoryItem.description ?: ''}</textarea>
                    </div>
                </div>
                <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
            </form>
        </div>
    </div>
<% } %>

