<script type="text/javascript">
    jQuery(function () {

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
            console.log("value: ", jq("#searchPhrase").val())
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                    table.search(searchText).draw();
                }, delay);
            }
        });

        jQuery('#stockRoom').change(function() {
            if (jQuery(this).val() !== '' ) {
                let stockRoomId = jQuery('#stockRoom').val();
                if (stockRoomId === '') {
                    jq().toastmessage('showErrorToast', "Select a destination Stockroom to proceed");
                    return;
                }
                table.draw()
            }
        });

        jq('#dateReceived').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            "setDate": new Date(),
            dateFormat: "yy-mm-dd",
            maxDate: 0,
            "autoclose": true
        });

        jq("#dateReceived").datepicker("setDate", new Date());

        let getStockRoomId = function() {
            let stockRoomId = jQuery('#stockRoom').val();
            if (stockRoomId === '') {
                stockRoomId = 0;
            }
            return stockRoomId;
        };

        const table = jQuery('#stockTakeTable').DataTable({
            dom: 'rtp',
            searching: true,
            serverSide: true,
            serverMethod: 'get',
            ajax: {
                url: '${ui.actionLink("botswanaemr","stock/discrepancyListings","getDiscrepancyListingsByStockRoom")}',
                data: function (d) {
                    d.stockRoomId = getStockRoomId();
                },
                "dataSrc": ""
            },
            columns: [
                {data: "id"},
                {data: "itemName"},
                {data: "enteredQuantityInStore"},
                {data: "batchNumber"},
                {data: "discrepancy"},
                {data: "operationDate"},
                {data: "institution"},
                {data: "receiver"},
                {data: "destinationStockRoom"},
                {data:
                    "itemId",
                    "render": function (itemId, type, row) {
                        return "<a href='/${ui.contextPath()}/botswanaemr/stock/stockCard.page?itemId=" + itemId + "' class='color-primary'> View</a>";
                    }
                },
            ],
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });
</script>

<div class="card">
    <div class="row mb-2">
        <div class="form-group col-md-6 pr-3">
            <label for="stockRoom">Destination Stockroom:</label>
            <select class="form-control" id="stockRoom" required>
                <option value="">Select Destination Stockroom</option>
                <% stockrooms.each { %>
                    <option value= ${it.id}>${it.name}</option>
                <% } %>
            </select>
        </div>
        <div class="form-group col-md-6 pr-3">
            <label for="dateReceived">Date received:</label>
            <input type="text" id="dateReceived" name="dateReceived" class="form-control"
                placeholder="Date received"/>
        </div>
        <div class="input-group col-md-6 pr-3">
            <input type="text"
                class="form-control py-2"
                id="searchPhrase"
                name="searchPhrase"
                placeholder="Search Product">
            <span class="input-group-append">
                <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                    <i class="fa fa-search"></i> search
                </button>
            </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="stockTakeTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Product</th>
                <th>Quantity received</th>
                <th>Batch</th>
                <th>Discrepancy</th>
                <th>Date received</th>
                <th>Received from</th>
                <th>Received By</th>
                <th>Destination Stockroom</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
</div>