<script type="text/javascript">
    jQuery(function () {
        const table = jQuery('#directReceiptTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#resetBtn').on('click', () => {
            jq('#batchNumber').val('');
            jq('#searchPhrase').val('');
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#searchPhrase').on('keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if (timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () {
                    table.search(searchText).draw();
                }, delay);
            } else {
                table.search('').draw();
            }
        });

        jq('#batchNumber').on('keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if (timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () {
                    table.search(searchText).draw();
                }, delay);
            } else {
                table.search('').draw();
            }
        });
    });
</script>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Product</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Enter Product name or code')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Batch Number</label>
        <input id="batchNumber"
               type="text"
               name="batchNumber"
               class="form-control"
               value=""
               placeholder="${ui.message('Enter Batch')}"
        />
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="searchBtn"
                type="button"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="directReceiptTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>Receipt Date</th>
        <th>Code</th>
        <th>Product</th>
        <th>Qty</th>
        <th>Batch No.</th>
        <th>Expiry Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% directReceipts.each { %>
    <tr>
        <td>${it.receiptDate}</td>
        <td>${it.itemCode}</td>
        <td>${it.itemName}</td>
        <td>${it.quantity}</td>
        <td>${it.batchNumber}</td>
        <td>${it.expiryDate}</td>
        <td>
            <a href="/${ui.contextPath()}/botswanaemr/stock/addProducts.page?itemId=${it.itemId}" class="color-primary"> View Item</a> |
            <a href="/${ui.contextPath()}/botswanaemr/stock/stockCard.page?itemId=${it.itemId}" class="color-primary"> Bin stock card </a>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>
