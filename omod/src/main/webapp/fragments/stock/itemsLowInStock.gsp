
<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#itemsLowInStockTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.search( this.value ).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            table.search( this.value ).draw();
        });
    });
</script>

<div class="row mb-5">
    <div class="input-group col-12 pl-0 pr-0">
        <input type="text"
               class="form-control py-2"
               id="searchPhrase"
               name="searchPhrase" placeholder="Search">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
</div>

<div class="table-responsive">
    <table id="itemsLowInStockTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Stockroom</th>
            <th>Item Name</th>
            <th>Unit</th>
            <th>Quantity</th>
            <th>Minimum Qty</th>
            <th>Expiry date</th>
            <th>Batch number</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% itemList.each { %>
            <tr>
                <td>${it.stockRoomName}</td>
                <td>${it.itemName}</td>
                <td>${it.units}</td>
                <td>${it.quantity}</td>
                <td>${it.minimumQuantity}</td>
                <td>${it.expiryDate}</td>
                <td>${it.batchNumber}</td>
                <td>
                    <a href="/${ui.contextPath()}/botswanaemr/stock/addProducts.page?itemId=${it.itemId}">View</a>
                </td>
            </tr>
            <%}%>
        </tbody>
    </table>
</div>

