<script type="text/javascript">
    jQuery(function () {
        // Initialize datepickers
        jq("#date").datepicker();

        const table = jQuery('#externalRequisitionTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#resetBtn').on('click', () => {
            jq('#date').val('');
            jq('#status').val('');
            table.search(jq("#date").val()).draw();
        });

        jq('#searchBtn').on('click', () => {
            let date = jq("#date").val();
            let status = jq("#status").val();

            if (date && status){
                let combinedSearch = date + ' ' + status;
                table.search(combinedSearch).draw();
            }else if (date) {
                table.search(date).draw();
            } else if (status) {
                table.search(status).draw();
            } else {
                table.search(date).draw();
            }
        });


    });
</script>
<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Date</label>
        <input id="date"
               type="text"
               name="date"
               class="form-control"
               value=""
               placeholder="${ui.message('Date')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Status</label>
        <select class="custom-select" id="status" name="status" required>
            <option value="">Select one</option>
            <option value="PENDING">PENDING</option>
            <option value="COMPLETED">COMPLETED</option>
            <option value="CANCELLED">CANCELLED</option>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="searchBtn"
                type="button"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
    </div>
</div>
<table id="externalRequisitionTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>Description</th>
        <th>Date</th>
        <th>Request From</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% externalRequisitions.each { %>
    <tr>
        <td>${it.id}</td>
        <td>${it.description}</td>
        <td>${it.date}</td>
        <td>${it.requestFrom}</td>
        <td>
            <% if (it.status == "PENDING"){ %>
                <i class="fa fa-circle text-warning"></i> ${it.status}
            <% } else {%>
                <i class="fa fa-circle text-success"></i> ${it.status}
            <% } %>
        </td>
        <td>
            <% if (it.status == "PENDING" && it.user == currentUser){ %>
                <a href="${ui.pageLink('botswanaemr', 'stock/viewExternalRequisition', [id: it.id, requestType: "external", action: "edit"])}">Edit</a>
            <% } else if (it.status ==  "PENDING") {%>
                <a href="${ui.pageLink('botswanaemr', 'stock/viewExternalRequisition', [id: it.id, requestType: "external", action: "approve"])}">Approve</a>
            <% } else {%>
                <a href="${ui.pageLink('botswanaemr', 'stock/viewExternalRequisition', [id: it.id, requestType: "external", action: "view"])}">View</a>
            <% } %>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>