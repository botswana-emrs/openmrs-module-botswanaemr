
<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#outgoingExternalRequisitionsTable, #incomingExternalRequisitionsTable, #approvedIncomingExternalRequisitionsTable, #itemsToReceiveTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="row mb-5">
    <div class="input-group col-12 pl-0 pr-0">
        <input type="text"
               class="form-control py-2"
               value="" id="searchPhrase"
               name="searchPhrase" placeholder="Search">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
</div>

<div class="table-responsive">
    <h3 class="body">Positive adjustments</h3>
    <table id="outgoingExternalRequisitionsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request to</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
             <% outgoingOperations.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.source.location}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/viewRequisition', [id: it.id, requestType: "external", action: "edit"])}">Edit</a>|
                    <a href="${ui.pageLink('botswanaemr', 'stock/viewRequisition', [id: it.id, requestType: "external", action: "approve"])}">Approve</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <h3 class="body">Items to Receive</h3>
    <table id="itemsToReceiveTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request From</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
             <% incomingStockOperationsToReceive.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.destination.location}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/requisitionItemReceipt', [id: it.id, requestType: "externalReceive", actionType: "transfer"])}">View</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#completedPositiveAdjustmentsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<!-- completed positive adjustments -->
<div class="table-responsive">
    <h3 class="body">Completed Positive Adjustments</h3>
    <table id="completedPositiveAdjustmentsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request to</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% completedPositiveAdjustments.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.source?.location} - ${it.source?.name}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/printRequisitions', [id: it.id])}&returnUrl=${ui.pageLink('botswanaemr', 'stock/positiveAdjustmentsDashboard')}"><i class="fa fa-file"></i> Gen 12</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>
        
            



