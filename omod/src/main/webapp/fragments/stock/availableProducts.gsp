<script type="text/javascript">
        jQuery(function () {

    jQuery('#stockRoom').change(function() {
            if (jQuery(this).val() !== '' ) {
                let stockRoomId = jQuery('#stockRoom').val();
                if (stockRoomId === '') {
                    jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                    return;
                }
                console.log("ssssssssss")
                table.draw()
            }
        });


        let getStockRoomId = function() {
                    let stockRoomId = jQuery('#stockRoom').val();
                    if (stockRoomId === '') {
                        stockRoomId = 0;
                    }
                    return stockRoomId;
                };
        const table = jQuery('#allProductsTable').DataTable({
            dom: 'rtp',
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: {
                url: '${ui.actionLink("botswanaemr","stock/availableProducts","getAvailableProducts")}',
                data: function (d) {
                    d.stockRoomId = getStockRoomId();
                }
            },
            columns: [
                { data: 'stockRoom' },
                { data: 'code' },
                { data: 'name' },
                { data: 'department' },
                { data: 'category' },
                { data: 'unitOfIssue' },
                { data: 'status' },
                null,
            ],
            columnDefs: [
                {
                    targets: -1,
                    data: null,
                    className: "text-left",
                    render: function(data, type, row, meta ) {
                        // console.log(data)
                     return '<a href="/${ui.contextPath()}/botswanaemr/stock/addProducts.page?itemId=' + data.id + '" class="color-primary"> View Item</a> | <br/>' +
                        '<a href="/${ui.contextPath()}/botswanaemr/stock/stockCard.page?itemId=' + data.id + '" class="color-primary"> Bin stock card </a>'
                    },
                }
            ],
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   table.search(searchText).draw();
                }, delay);
            }
        });
 });   
</script>

<div class="card">
    <div class="row mb-5">
        <div class="col-6 pr-3">
                <select class="form-control" id="stockRoom" required col-md-6 pr-3>
                    <option value="">Select Stock Room</option>
                    <% stockrooms.each { %>
                    <option value= ${it.id}>${it.name}</option>
                    <% } %>
                </select>
        </div>
        <div class="input-group col-6 pl-0 pr-3">
            <input type="text"
                   class="form-control py-2"
                   id="searchPhrase"
                   name="searchPhrase"
                   placeholder="Search Item"/>
            <span class="input-group-append">
                <button type="submit" class="btn btn-primary" id="searchBtn" name="searchBtn">
                    <i class="fa fa-search"></i> search
                </button>
            </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="allProductsTable" class="table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th>Stock room</th>
                <th>Code</th>
                <th>Item Name</th>
                <th>Department</th>
                <th>Category</th>
                <th>Unit</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
