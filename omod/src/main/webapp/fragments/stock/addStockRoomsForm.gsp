<script type="text/javascript">
    jQuery.noConflict();
    jq(document).ready(function () {

        jq(function () {
            jq("#addStockRoomForm").submit(function (e) {
                e.preventDefault();
                const params = {
                    'name': jq('#name').val(),
                    'description': jq('#description').val(),
                    'stockRoomType': jq('#stockRoomType').val()
                };

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/addStockRoomsForm", "saveCreatedStockRoom")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Stock room saved successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/stockRooms")}';
                    })
                    .fail(function (err) {
                            jq().toastmessage('showErrorToast', err);
                            console.log(err)
                        }
                    )
            });

            jq("#editStockRoomForm").submit(function (e) {
                e.preventDefault();
                var params = {
                    'stockRoomId': '${stockRoomId}',
                    'name': jq('#stockRoomName').val(),
                    'description': jq('#stockRoomDescription').val(),
                    'stockRoomType': jq('#stockRoomType').val()
                };

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/addStockRoomsForm", "saveEditedStockRoom")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Stock room updated successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/stockRooms")}';
                    })
                    .fail(function (err) {
                            jq().toastmessage('showErrorToast', err);
                            console.log(err)
                        }
                    )
            });
        });
    });
</script>

<% if (stockRoom == null) { %>
<div class="card mt-4">
    <div class="card-body">
        <form method="post" id="addStockRoomForm">
          <h5 class="text-primary">STOCK ROOM DETAILS</h5>
            <hr class="divider pt-1 bg-primary"/>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">StockRoom Name:
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Name"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">StockRoom Type:
                        <span class="text-danger">*</span>
                    </label>
                    <select id="stockRoomType" type="text" placeholder="dispensing or main"
                            class="custom-select form-control">
                        <option disabled selected>Select one</option>
                        <% stockroomTypes.each { %>
                        <option <% if (it.equals("Main Bulk") && hasMainRoom) {%> disabled<% } %> value="${it}">${ui.encodeHtmlContent(ui.format(it))}</option>
                        <% } %>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="description">Description:
                        <span class="text-danger">*</span>
                    </label>
                    <textarea class="form-control" rows="2" id="description" name="description"></textarea>
                </div>
            </div>

            <div class="card-footer text-muted mt-4 pr-1">
                <button type="submit" id="addStockRoomBtn" class="btn btn-primary p-2 mt-1 float-right update-stock-room-btn"><% if (stockRoomId == null) { %> + Add <% } else { %> Update <% } %>StockRoom</button>
                &nbsp;
            </div>
        </form>
    </div>
</div>
<% } else { %>
<div class="card mt-4">
    <div class="card-body">
        <form method="post" id="editStockRoomForm">
            <h5 class="text-primary">STOCK ROOM DETAILS</h5>
            <hr class="divider pt-1 bg-primary"/>
            <input type="number" id="stockRoomId" name="stockRoomId" value="${stockRoom.id ?: ''}" class="form-control"
                   style="display: none"/>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">StockRoom Name:
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" id="stockRoomName" name="name" class="form-control" placeholder="Name"
                           value="${stockRoom.name ?: ''}"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">StockRoom Type:
                        <span class="text-danger">*</span>
                    </label>
                    <select id="stockRoomType" type="text" placeholder="dispensing or main"
                            class="custom-select form-control">
                        <option disabled selected>Select one</option>
                        <% stockroomTypes.each { %>
                        <option <% if (it.equals("Main Bulk") && hasMainRoom) {%> disabled<% } %>  <% if (stockRoomType.equals(it)) { %> selected <% } %>  value="${it}">${ui.encodeHtmlContent(ui.format(it))}</option>
                        <% } %>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="stockRoomDescription">Description:
                        <span class="text-danger">*</span>
                    </label>
                    <textarea class="form-control" rows="2" id="stockRoomDescription" name="description">${stockRoom.description ?: ''}</textarea>
                </div>
            </div>

            <div class="card-footer text-muted mt-4 pr-1" id="card-footer-stock">
                <button type="submit" id="editStockRoomBtn" class="btn btn-primary p-2 mt-1 float-right update-stock-room-btn">+ Update StockRoom</button>
                &nbsp;
            </div>
        </form>
    </div>
</div>
<% } %>

