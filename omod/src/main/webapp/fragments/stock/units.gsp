<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#unitsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="card px-0">
    <form class="row">
        <div class="form-group col-sm-12 col-md-12 col-lg-10 pl-3">
            <label for="unitName"> Unit Name:
                <span class="text-danger">*</span>
            </label>
            <input type="text" id="unitName" name="unitName" class="form-control" placeholder="Unit Name"/>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-2 pt-3 pr-3">
            <label for="unitName">&nbsp;</label>
            <button class="btn btn-primary btn-block float-right">Add Unit</button>
        </div>
    </form>
    <div class="table-responsive">
        <table id="unitsTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Unit Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Masks</td>
                    <td>
                        <a href="#">Remove</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>