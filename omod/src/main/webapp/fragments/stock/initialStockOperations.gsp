<script type="text/javascript">
    jQuery(function () {

        let stockRoomId = 0;

        jQuery('#stockTakeTable').on('click', "input:button", function (e) {

            stockRoomId = jQuery('#stockRoom').val();
            let operationDate = jQuery('#operationDate').val();
            let parentRow = jQuery(e.target).parents('tr');
            let itemId = jQuery(e.target).attr('id');
            let affectedRow = parentRow;

            if (operationDate.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select date to proceed");
                return;
            }

            if (stockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                return;
            }

            if (itemId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Item Id is missing");
                return;
            }

            let invalidQtys = parentRow.find('input[type=number]').filter(function() {
                return jq(this).val().trim().length === 0;
            });
            if (invalidQtys.length > 0) {
                jq().toastmessage('showErrorToast', "Enter a valid quantity to proceed");
                return;
            }

            let invalidDates = parentRow.find('input[type=date]').filter(function() {
                return jq(this).val().trim().length === 0;
            });

            if (invalidDates.length > 0) {
                jq().toastmessage('showErrorToast', "Expiration Date is missing");
                return;
            }

            let invalidBatchNos = parentRow.find('input[type=text]').filter(function() {
                return jq(this).val().trim().length === 0;
            });

            if (invalidBatchNos.length > 0) {
                jq().toastmessage('showErrorToast', "Batch Number is missing");
                return;
            }

            let batchInfo = [];
            parentRow.find('table[name^=batchDetails] tbody tr').each(function() {
                batchInfo.push(
                    {
                        "qty": jq(this).find('input[type=number]').val(),
                        "expirationDate": jq(this).find('input[type=date]').val(),
                        "batchNo": jq(this).find('input[type=text]').val()
                    }
                );
            })

            saveStockItems(stockRoomId, operationDate, itemId, Object.assign({}, batchInfo), affectedRow);

        });

        let getBatchesModalHtml = function(itemCode, itemName, batches, availableQuantity) {
            let batchesHtml = '';
            for (let i = 0; i < batches.length; i++) {
                batchesHtml += `<tr><td>\${batches[i].batchNumber}</td><td>\${batches[i].quantity}</td><td>\${batches[i].expiryDate}</td></tr>`;
            }
            return `
                <div class="modal fade" id="batchesModal" tabindex="-1" role="dialog" aria-labelledby="batchesModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="batchesModalLabel">Batches for <span class="text-primary">\${itemName}</span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <strong>Item Code:</strong> \${itemCode}
                                        </div>
                                        <div class="col-md-6">
                                            <strong>Available Quantity:</strong> \${availableQuantity}
                                        </div>
                                    </div>
                                    <table class="table table-striped table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Batch Number</th>
                                                <th>Quantity</th>
                                                <th>Expiry Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            \${batchesHtml}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        };

        // Show batches
        jQuery('#stockTakeTable').on('click', 'a.showBatches', function(e) {
            e.preventDefault();
            let itemCode = jQuery(this).data('code');
            let itemName = jQuery(this).data('name');
            jQuery.ajax({
                url: '${ui.actionLink("botswanaemr","search","fetchBatchesByItemCode")}',
                data: {
                    itemId: itemCode,
                    stockroomId: getStockRoomId()
                },
                success: function(data) {
                    // show the batches in a bootstrap modal
                    let batches = data.batches;
                    let batchNumbers = data.batchNumbers;
                    let availableQuantity = data.availableQuantity;
                    let modalHtml = getBatchesModalHtml(itemCode, itemName, batches, availableQuantity);

                    // remove any existing modal and remove markup
                    jq('#batchesModal').modal('dispose');
                    jq('#batchesModal').remove();

                    jq('body').append(modalHtml);
                    jQuery('#batchesModal').modal('show');
                }
            });

        });


        jQuery('#stockTakeTable').on('click', 'a.addBatch', function(e) {
            e.preventDefault();
            let thisRow = jQuery(this).parents('tr:first');
            let batchRow = thisRow.clone();
            batchRow.find('input').val('');
            batchRow.find('a').removeClass('btn-info addBatch').addClass('btn-success removeBatch text-white').text('Remove-');
            batchRow.insertAfter(thisRow);
        });

        jQuery('#stockTakeTable').on('click', 'a.removeBatch', function(e) {
            e.preventDefault();
            jQuery(this).parents('tr:first').remove();
        });

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#operationDate').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            "setDate": new Date(),
            dateFormat: "yy-mm-dd",
            maxDate: 0,
            "autoclose": true
        });
        jq("#operationDate").datepicker("setDate", new Date());

        jQuery('#stockRoom').change(function() {
            if (jQuery(this).val() !== '' ) {
                let stockRoomId = jQuery('#stockRoom').val();
                if (stockRoomId === '') {
                    jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                    return;
                }
                table.draw()
            }
        });

        let getStockRoomId = function() {
            let stockRoomId = jQuery('#stockRoom').val();
            if (stockRoomId === '') {
                stockRoomId = 0;
            }
            return stockRoomId;
        };

        let getUpdateMissingBatches = function() {
            return jQuery('#updateMissingBatches').is(':checked');
        };

        let today = new Date().toISOString().split('T')[0];

        const table = jQuery('#stockTakeTable').DataTable({
            dom: 'rtp',
            processing: true,
            serverSide: true,
            serverMethod: 'get',
            ajax: {
                url: '${ui.actionLink("botswanaemr","stock/initialStockOperations","getInitialItems")}',
                data: function (d) {
                    d.stockRoomId = getStockRoomId();
                    d.updateMissingBatches = getUpdateMissingBatches();
                },
                "dataSrc": ""
            },
            columns: [
                {data: "code"},
                {data: "name"},
                {
                    "data": "uuid",
                    "render": function (uuid, type, row) {
                        return "<table name='batchDetails[]'>" +
                            "<thead>" +
                            "<tr>" +
                            "<th>Quantity</th>" +
                            "<th>Expiration Date</th>" +
                            "<th>Batch No</th>" +
                            "<th><a href='#' class='btn btn-sm btn-default btn-outline-info showBatches' data-code='" + row.code + "' data-name='" + row.name + "' id='" + uuid + "' title='Click here to show other Batches'><i class='fa fa-info-circle'></i> Batches</a></th>" +
                            "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                            "<tr>" +
                            "<td><input type='number' class='form-control input-sm' name='quantity[]' min='0' /><input type='hidden' value='" + uuid + "' name='itemId[]'></td>" +
                            "<td><input type='date' class='form-control input-sm' name='expirationDate[]' min='" + today + "' /></td>" +
                            "<td><input type='text' class='form-control input-sm' style='min-width: 100px' name='BatchNo[]' /></td>" +
                            "<td><a href='#' class='btn btn-sm btn-info addBatch text-white'>Batch+</a></td>" +
                            "</tr>" +
                            "</tbody>" +
                            "</table>";
                    }
                },
                {
                    "data": "uuid",
                    "render": function (uuid, type, row) {
                        return "<input type='button' class='btn btn-sm btn-primary' id='" + uuid + "' value='Save'/></td>";
                    }
                }
            ],
            columnDefs: [
                {
                    targets: -2,
                    data: null,
                    className: "text-left",
                    render: function (data, type, row, meta) {
                        // console.log(data)
                        return "<table name='batchDetails[]'>" +
                            "<thead>" +
                            "<tr>" +
                            "<th>Quantity</th>" +
                            "<th>Expiration Date</th>" +
                            "<th>Batch No</th>" +
                            "<th></th>" +
                            "</tr>" +
                            "</thead>" +
                            "<tbody>" +
                            "<tr>" +
                            "<td><input type='number' class='form-control input-sm' name='quantity[]' min='0' /><input type='hidden' value='" + item.uuid + "' name='itemId[]'></td>" +
                            "<td><input type='date' class='form-control input-sm' name='expirationDate[]' min='" + today + "' /></td>" +
                            "<td><input type='text' class='form-control input-sm' style='min-width: 100px' name='BatchNo[]' /></td>" +
                            "<td><a href='#' class='btn btn-sm btn-info addBatch text-white'>Batch+</a></td>" +
                            "</tr>" +
                            "</tbody>" +
                            "</table>"
                    },
                },
            ],
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                    table.search(searchText).draw();
                }, delay);
            }
        });

        jQuery('#updateMissingBatches').on('change', function() {
            table.draw();
        });

    });

    function saveStockItems(stockRoomId, operationDate, itemId, batchInfo, affectedRow) {

        console.log(itemId);
        console.log(batchInfo);
        showLoadingOverlay();
        var searchResult;
        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","stock/initialStockOperations","saveInitialStockItem")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                stockRoomId: stockRoomId,
                operationDate: operationDate,
                itemId: itemId,
                batchInfo: JSON.stringify(batchInfo)
            },
            success: function (data) {
                hideLoadingOverlay();
                jq().toastmessage('showNoticeToast', "Operation completed successfully");
                affectedRow.remove();
                return data;
            },
            fail: function(err) {
                console.log(err);
                jq().toastmessage('showErrorToast',  "Operation Failed");
            }
        });

    }
</script>

<form id="initialStockOperationForm" method="post">
    <div class="card">
        <div class="row">
            <div class="form-group col-md-6 pr-3">
                <label for="stockRoom">Stock Room:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control" id="stockRoom" required>
                    <option value="">Select Stock Room</option>
                    <% stockrooms.each { %>
                    <option value= ${it.id}>${it.name}</option>
                    <% } %>

                </select>
            </div>


            <div class="form-group col-md-6 pr-3">
                <label for="operationDate">Date:
                    <span class="text-danger">*</span>
                </label>
                <input type="text" id="operationDate" name="operationDate" class="form-control"
                       placeholder="Date"/>
            </div>
        </div>
        <fieldset><legend>Stock Items</legend>

            <div class="row mb-1">
                <div class="input-group col-6 pl-0 pr-0">
                    <input type="text"
                           class="form-control py-2"
                           id="searchPhrase"
                           name="searchPhrase"
                           placeholder="Search">
                    <span class="input-group-append">
                        <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                            <i class="fa fa-search"></i> search
                        </button>
                    </span>
                </div>
                <div class="col-4 pl-5 pr-0">
                    <!-- Add a checkbox labelled "Update missing batches" -->
                    <input class="form-check-input" type="checkbox" value="true" id="updateMissingBatches">
                    <label class="form-check-label" for="updateMissingBatches"
                           style="margin-top: auto">
                        Add missing batches
                    </label>
                </div>
            </div>

            <div class="table-responsive">
                <table id="stockTakeTable"
                       class="table table-striped table-sm table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Product Name</th>
                        <th></th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </fieldset>

    </div>
</form>
