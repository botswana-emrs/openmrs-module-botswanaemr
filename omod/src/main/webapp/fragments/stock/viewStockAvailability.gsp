<script type="text/javascript">
    jQuery(function () {
        function populateTransactionsTbody(retVal) {
            jq('#stockDetailsTbody').empty();
            retVal.transactionSimplifierList.forEach(function (data) {
                let transactionReference = data.transactionReference;
                let transactionDate = new Date(data.transactionDate).toDateInputValue();
                let facilityName = data.facilityName;
                let quantityReceived = data.quantityReceived;
                let quantityIssued = data.quantityIssued;
                let expiryDate = new Date(data.expiryDate).toDateInputValue();
                let batchNumber = data.batchNumber;
                let adjustment  = data.adjustment;
                let losses = data.losses;
                let quantityOnHand = data.quantityOnHand;
                let remarks = data.remarks;
                let initials = data.initials;
                jq('#stockDetailsTbody').append("<tr><td>" + transactionReference + "</td> <td>" + transactionDate + "</td> <td>" + facilityName + "</td> <td>" + quantityReceived + "</td> <td>" + quantityIssued + "</td> <td>" + expiryDate + "</td> <td>" + batchNumber + "</td> <td>" + adjustment + "</td> <td>" + losses + "</td> <td>" + quantityOnHand + "</td> <td>" + remarks + "</td> <td>" + initials + "</td></tr>");
            });
        }

        function populateCommodityDetailsTBody(data) {
            let minimumStockLevel = data.minimumStockLevel;
            let maximumStockLevel = data.maximumStockLevel;
            let emergencyOrderPoint = data.emergencyOrderPoint;
            jq("#minStockLevel").empty().text(minimumStockLevel);
            jq("#maxStockLevel").empty().text(maximumStockLevel);
            jq("#emergencyOrderPoint").empty().text(emergencyOrderPoint);
        }

        function populateSelector(data) {
            let optionText = data.stockRoomName;
            let optionValue = data.stockRoomId;
            let option = jq('<select></select>').val(optionValue).text(optionText);
            jq("#optionValue").empty().append(option);
        }

        jq("#stockRoomId").change(function(e){
            e.preventDefault();
            var result;
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr", "stock/viewStockAvailability", "changeStockRoom")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    stockRoomId: jq('#stockRoomId option:selected').val(),
                    itemId: jq('#itemId').val()
                },
                success: function (data) {
                    result = data;
                }
            });
            populateSelector(result)
            populateCommodityDetailsTBody(result)
            populateTransactionsTbody(result)
        });
   });

</script>

<div class="row">
    <div class="col pl-0 pr-0">
        <div class="row pl-0">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col pl-0">
                                <div class="table-responsive">
                                    <table class="table mt-3 table-info">
                                        <tbody>
                                            <tr>
                                                <td>Facility:</td>
                                                <td class="text-left"><span>${hospital}</span></td>
                                            </tr>
                                            <tr>
                                                <td>Facility Code:</td>
                                                <td class="text-danger text-left"><span>${facilityCode}</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col pr-0">
                                <div class="table-responsive">
                                    <table class="table mt-3 table-info">
                                        <tbody>
                                            <tr>
                                                <th>Store Name:</th>
                                                <td class="text-left pb-0">
                                                    <form id="changeStockRoomForm">
                                                        <input type="hidden" class="form-control" id="itemId" name="itemId" value="${itemId}">
                                                        <select class="border-0 bg-light p-0" name="stockRoomId" id="stockRoomId" style="select:focus{outline: none;}" placeholder="Select stock room">
                                                            <% if(stockRooms != null && stockRooms != "") { %>
                                                            <option value="">Select stock room</option>
                                                            <% stockRooms.each { %>
                                                            <option id="${it.id}" value="${it.id}">${it.name}</option>
                                                            <% } %>
                                                            <% } %>
                                                        </select>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-1 pb-2">
                            <div class="col pl-0 pr-0">
                                <div class="table-responsive">
                                    <table id="commodityDetailsTable1" class="table mt-3 table-info">
                                        <tbody id="commodityDetailsTBody1">
                                        <tr>
                                            <td class="d-md-table-cell">Commodity Code: <span>${commodityCode}</span> </td>
                                            <td colspan="3" class="text-left">Commodity Name: <span>${commodityName}</span></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left d-md-table-cell">Unit of Issue: <span>${unitOfIssue}</span></td>
                                            <td class="text-left">Mini. Stock Level: <span id="minStockLevel"></span></td>
                                            <td class="text-left">Max. Stock Level: <span id="maxStockLevel"></span> </td>
                                            <td class="text-left">Emergency Order Point: <span id="emergencyOrderPoint"></span> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-primary mt-5">STOCK AVAILABILITY DETAILS</h5>
                        <hr class="divider pt-1 bg-primary"/>
                        
                        <div class="row">
                            <div class="col pl-0 pr-0">
                                <div class="table-responsive">
                                    <table id="stockDataTable"
                                           class="table table-sm table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Transaction Reference No.</th>
                                                <th>Date</th>
                                                <th>User Site</th>
                                                <th>Quantity Received</th>
                                                <th>Quantity Issued</th>
                                                <th>Expiry Date</th>
                                                <th>Batch / Lot No.</th>
                                                <th>Adjust</th>
                                                <th>Losses</th>
                                                <th>Quantity on Hand</th>
                                                <th>Remarks</th>
                                                <th>Initial</th>
                                            </tr>
                                        </thead>
                                        <tbody id="stockDetailsTbody">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
