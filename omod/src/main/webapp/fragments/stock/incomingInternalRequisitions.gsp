<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#incomingInternalRequisitionsTable_${action}').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="table-responsive">
    <table id="incomingInternalRequisitionsTable_${action}"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Description</th>
            <th>Date</th>
            <th>Request From</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% operations.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <% if (action.equals("receive")) { %>
                        <a href="${ui.pageLink('botswanaemr', 'stock/requisitionItemReceipt', [id: it.id])}&requestType=internalReceive&actionType=transfer&returnUrl=${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}" class="text-capitalize">${action}</a> | 
                    <% } else { %>
                        <a href="${ui.pageLink('botswanaemr', 'stock/requisitionApplications', [id: it.id])}&actionType=${action}Internal&returnUrl=${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}" class="text-capitalize">${action}</a> | 
                    <% } %>
                    <a href="${ui.pageLink('botswanaemr', 'stock/printRequisitions', [id: it.id])}&returnUrl=${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}">Gen 12</a>

                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>
