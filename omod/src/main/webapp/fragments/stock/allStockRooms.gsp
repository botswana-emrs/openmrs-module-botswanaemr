<script type="text/javascript">
    jQuery(function () {
        let table = jQuery('#allStockRoomsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBtn').on('click', () => {
            table.search( this.value ).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            table.search( this.value ).draw();
        });

        jq('#allStockRoomsTable').on('click', "a.delete", function (e) {
            e.preventDefault();
            let param = {
                'stockRoomId': jQuery(this).attr('data-overlay')
            };

            let stockroomName = jq(this).parents('tr').find('td:first').text();

            jq('.dialog-instructions').text('Are you sure you want to delete the ' + stockroomName + ' Stockroom?');

            const deleteStockroomDialog = emr.setupConfirmationDialog({
                selector: '#confirm-delete-dialog',
                actions: {
                    confirm: function () {
                        jq.getJSON('${ui.actionLink("botswanaemr", "stock/allStockRooms", "retireStockRoom")}', param)
                            .success(function (data) {
                                emr.successMessage("Stockroom deleted successfully!");
                                location.href = '${ui.pageLink("botswanaemr", "stock/stockRooms")}';
                            })
                            .fail(function (err) {
                                    emr.errorMessage(err);
                                    console.log(err)
                                }
                            )
                        console.log("yes");
                    },
                    cancel: function () {
                        console.log("no");
                    }
                }
            });

            deleteStockroomDialog.show();
        });
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="input-group col-12 pl-0 pr-0">
            <input type="text"
                   class="form-control py-2"
                   id="searchPhrase"
                   name="searchPhrase"
                   placeholder="Search">
            <span class="input-group-append">
            <button type="submit" class="btn btn-primary" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="allStockRoomsTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Description</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <% allStockRooms.each {%>
                    <tr>
                        <td>${it.getStockroom()?.getName()}</td>
                        <td>${it.getType()}</td>
                        <td>${it.getStockroom()?.getDescription()}</td>
                        <td>
                            <% if(it.getStockroom()?.getRetired() == false){ %>
                                <i class="fa fa-circle text-success"></i> Active
                            <% } else {%>
                                <i class="fa fa-circle text-warning"></i> Not Active
                            <% } %>
                        </td>
                        <td>
                            <a href="/${ui.contextPath()}/botswanaemr/stock/addStockRooms.page?stockRoomId=${it.getStockroom()?.getId()}"
                               class="color-primary"> Edit
                            </a> |
                            <a href="#" class="delete" data-overlay="${it.getStockroom()?.getId()}">Delete</a>
                             |
                            <a href="/${ui.contextPath()}/botswanaemr/stock/viewStockItems.page?stockRoomId=${it.getStockroom()?.getId()}">View Stock Items</a>
                        </td>
                    </tr>
                <% } %>
            </tbody>
        </table>
    </div>
</div>

<div id="confirm-delete-dialog" class="alert alert-info" role="alert" style="display: none">
    <div class="dialog-header">
        <div class="row bg-info">
            <h4 class="text-white text-bolder">
                <i class="fa fa-info-circle fa-1x pl-2"></i>
                ${ui.message("Delete confirmation")}
            </h4>
        </div>
    </div>
    <div class="dialog-content">
        <p class="dialog-instructions py-4">
            ${ui.message("Are you sure you want to delete this record?")}
        </p>
        <br/>
        <div class="buttons">
            <button type="button" class="btn btn-sm btn-dark bg-dark cancel" data-dismiss="modal">
                ${ui.message("Close")}
            </button>
            <button class="confirm btn btn-sm btn-primary right ml-1">${ui.message("Delete")}
            </button>



        </div>
    </div>
</div>
