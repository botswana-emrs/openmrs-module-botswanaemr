<script type="text/javascript">
    jQuery(function () {
        // Initialize datepickers
        jq("#fromDate").datepicker();
        jq("#toDate").datepicker();

        const table = jQuery('#internalRequisitionTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#resetBtn').on('click', () => {
            jq('#toDate').val('');
            jq('#fromDate').val('');
            table.search(jq("#fromDate").val()).draw();
        });

        jq('#searchBtn').on('click', () => {
            let fromDate = jq("#fromDate").val();
            let toDate = jq("#toDate").val();

            if (fromDate && toDate) {
                table.search(fromDate + ' to ' + toDate).draw();
            } else {
                jq().toastmessage('showErrorToast', 'Please select both From Date and To Date.');
            }
        });


    });
</script>
<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Date</label>
        <input id="fromDate"
               type="text"
               name="fromDate"
               class="form-control"
               value=""
               placeholder="${ui.message('From Date')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label> <br></label>
        <input id="toDate"
               type="text"
               name="toDate"
               class="form-control"
               value=""
               placeholder="${ui.message('To Date')}"
        />
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="searchBtn"
                type="button"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="internalRequisitionTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>Description</th>
        <th>Date</th>
        <th>Request From</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <% internalRequisitions.each { %>
    <tr>
        <td>${it.id}</td>
        <td>${it.description}</td>
        <td>${it.date}</td>
        <td>${it.requestFrom}</td>
        <td>${it.status}</td>
        <td>
            <a href="/${ui.contextPath()}/botswanaemr/stock/createStockAdjustment.page?stockOperationId=${it.id}" class="color-primary"> View</a>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>