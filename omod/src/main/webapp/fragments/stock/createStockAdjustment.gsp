<script type = "text/javascript">
    var allMaps = [];

function updateSecondSelect(id, value) {
    if (document.getElementById("expireNum0") == null) {} else {

    }
    currentMap = allMaps[id.substring(11)];
    document.getElementById("expireNum" + id.substring(11)).value = new Date(new Map(Object.entries(currentMap)).get(value).expiration).toDateInputValue();
}
var num = 0;

jQuery(function() {
    var table = jQuery('#createStockAdjustmentTable').DataTable({
        dom: 'rtp',
        searching: true,
        "oLanguage": {
            "oPaginate": {
                "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
            }
        }
    });
    var table = jQuery('#updateStockAdjustmentTable').DataTable({
        dom: 'rtp',
        searching: true,
        "oLanguage": {
            "oPaginate": {
                "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
            }
        }
    });

    function updateSecondSelect() {}
    jq('#createStockForm').submit((e) => {
        e.preventDefault();
        getItems();
        // table.draw();
    });

    jq('#updateStockForm').submit((e) => {
        e.preventDefault();
        getItems();
        // table.draw();
    });

    jQuery('#stockRoom').change(function() {
        if (jQuery(this).val() !== '') {
            getItems();
        }
    });

    function getItemStock(trow) {
        let itemStock = {};
        itemStock["uuid"] = jq(trow).find("input[name^=uuid]").val();
        itemStock["quantity"] = jq(trow).find("input[name^=quantity]").val();
        if (itemStock["quantity"] && !jq(trow).find("select[name^=batchNumber]").val()) {
            jq().toastmessage('showErrorToast', "Select batch number for specified adjustment");
            return;
        }
        itemStock["expiryDate"] = jq(trow).find("input[name^=expiryDate]").val();
        itemStock["batchNumber"] = jq(trow).find("select[name^=batchNumber] option:selected").text();

        if (itemStock["batchNumber"] && itemStock["quantity"] && !jq(trow).find("input[name^=comment]").val()) {
            jq().toastmessage('showErrorToast', "Set a comment for specified adjustment");
            return;
        }
        itemStock["comment"] = jq(trow).find("input[name^=comment]").val();
        return itemStock;
    }

    jq('#createStockAdjustmentBtn').on('click', (e) => {
        e.preventDefault();
        let stockRoomId = jQuery('#stockRoom').val().trim();

        if (stockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select a stock room to proceed");
            return;
        }

        let tb = jq("#createStockAdjustmentTable tbody");
        let jsonObj = [];

        tb.find("tr").each(function(index, element) {
            let itemStock = getItemStock(element);
            if (itemStock.quantity !== '') {
                jsonObj.push(itemStock);
            }
        });
        if (jsonObj.length === 0) {
            jq().toastmessage('showErrorToast', "Add itemStock(s) in store to proceed");
            return;
        }
        const newObj = Object.assign({}, jsonObj);

        saveItemStock(JSON.stringify(newObj), stockRoomId);
    });

    jq('#updateStockAdjustmentBtn').on('click', (e) => {
        e.preventDefault();
        let stockRoomId = jQuery('#stockRoom').val().trim();

        if (stockRoomId.trim().length === 0) {
            jq().toastmessage('showErrorToast', "Select a stock room to proceed");
            return;
        }

        var tb = jq("#updateStockAdjustmentTable tbody");
        jsonObj = [];

        tb.find("tr").each(function(index, element) {
            let itemStock = getItemStock(element);
            jsonObj.push(itemStock);
        });

        if (jsonObj.length === 0) {
            jq().toastmessage('showErrorToast', "Add itemStock(s) quantity in store to proceed");
            return;
        }

        const newObj = Object.assign({}, jsonObj);

        saveEditedItemStock(JSON.stringify(newObj), stockRoomId);
    });
});

function getItems() {
    let searchResult;

    let params = {};
    let stockRoomId = jQuery('#stockRoom').val();
    if (stockRoomId === '') {
        jq().toastmessage('showErrorToast', "Select a stock room to proceed");
        return;
    }

    let searchString = jQuery('#searchPhrase').val();
    if (searchString.trim().length > 0) {
        params = {
            q: searchString,
            stockRoomId: stockRoomId
        }
    } else {
        params = {
            stockRoomId: stockRoomId
        }
    }

    function createMap(myMap, num) {

        var dataMap = myMap;
        for (var key in dataMap) {
            var option = document.createElement("option");
            option.value = key;
            option.text = dataMap[key].batchNumber;
            document.getElementById("batchNumber" + num).add(option);
        }
    }


    jQuery.ajax({
        type: "GET",
        url: '${ui.actionLink("botswanaemr","stock/createStockAdjustment","getItemStockData")}',
        dataType: "json",
        global: false,
        async: false,
        data: params,
        success: function(data) {
            searchResult = data;
            jQuery("#createStockAdjustmentTable tbody").empty();
            searchResult.map((item) => {
                let today = new Date().toISOString().split('T')[0];
                jQuery("#createStockAdjustmentTable tbody")
                    .append("<tr><td>" + item.code + "<input type='text' name='uuid[]' value='" + item.uuid + "' class='hidden'/></td>" +
                        "<td>" + item.name + "</td>" +
                        "<td>" + item.quantity + "</td>" +
                        "<td><input type='number' name='quantity[]' class='form-control form-control-sm'/></td>" +
                        "<td><select name='batchNumber[]' id='batchNumber" + num + "' onchange='updateSecondSelect(this.id,this.value)'><option value=''>Select Batch Number</option></select></td>" +

                        "<td><input readonly='readonly' type='date' id='expireNum" + num + "' class='form-control' name='expiryDate[]' min='" + today + "' value='' /></td>" +
                        "<td><input type='text' name='comment[]' class='form-control form-control-sm'/></td></tr>");

                createMap(item.myMap, num);
                allMaps.push(item.myMap);
                num = num + 1;

            });

            jQuery('#createStockAdjustmentTable').DataTable();
            return searchResult;
        }
    });

}

function saveItemStock(jsonObj, stockRoomId) {
    jQuery.ajax({
        url: '${ui.actionLink("botswanaemr","stock/createStockAdjustment","saveItemStockData")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemStock: jsonObj,
            stockRoomId: stockRoomId
        },
        success: function(data) {
            jq().toastmessage('showNoticeToast', "Operation completed successfully");
            jq("#createStockAdjustmentTable > tbody").empty();
            jQuery('#stockRoom').val('');
            jQuery('#updateQuantity').val('');
            location.href = '${ui.pageLink("botswanaemr", "stock/stockAdjustment")}';
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
}

function saveEditedItemStock(jsonObj, stockRoomId) {
    jQuery.ajax({
        url: '${ui.actionLink("botswanaemr","stock/createStockAdjustment","saveEditedItemStockData")}',
        dataType: "json",
        global: false,
        async: false,
        data: {
            itemStock: jsonObj,
            stockRoomId: stockRoomId,
            stockOperationId: jQuery('#stockOperationId').val()
        },
        success: function(data) {
            jq().toastmessage('showNoticeToast', "Operation completed successfully");
            jq("#updateStockAdjustmentTable > tbody").empty();
            jQuery('#stockRoom').val('');
            jQuery('#updateQuantity').val('');
            location.href = '${ui.pageLink("botswanaemr", "stock/stockAdjustment")}';
            return data;
        },
        fail: function(err) {
            jq().toastmessage('showErrorToast', "Operation Failed");
        }
    });
} 
</script>
<% if(simplifiedStockAdjustmentList == "") {%>
<div class="card mt-4">
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Create Stock Adjustment (Add or reduce quantities to stock items)</h5>
            </div>

            <div class="col pr-0">
                <button id="createStockAdjustmentBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="card px-0">
                <h5 class="text-primary pl-4">STOCK ADJUSTMENT DETAILS</h5>
                <hr class="divider pt-1 bg-primary"/>

                <form id="createStockForm" class="row" method="post">
                    <div class="form-group col-md-5 pr-3">
                        <label for="stockRoom">Stock Room:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" id="stockRoom" required>
                            <option value="">Select Stock Room</option>
                            <% inventoryStockRooms.each { %>
                            <option value= ${it.id}>${it.name}</option>
                            <% } %>
                        </select>
                    </div>
                    <div class="form-group col-md-5 pr-3">
                        <label for="searchPhrase">Search</label>
                        <input type="text"
                               class="form-control"
                               id="searchPhrase"
                               name="searchPhrase"
                               placeholder="Search"/>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                        <label for="searchBtn">&nbsp;</label>
                        <button class="btn btn-primary float-right" type="submit" id="searchBtn" name="searchBtn">
                            search
                        </button>
                    </div>

                    <div class="table-responsive">
                        <table id="createStockAdjustmentTable"
                               class="table table-striped table-sm table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Product Name</th>
                                <th>Quantity In System</th>
                                <th>Adjustment Quantity</th>
                                <th>Batch Number</th>
                                <th>Expiry Date</th>
                                <th>Comment</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><input type='number' id='updateQuantity' name='updateQuantity'
                                           class='form-control form-control-sm' min='0'/></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<% } else { %>
<div class="card mt-4">
    <% if (stockOperationStatus == "PENDING") { %>
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Update Stock Take</h5>
            </div>
            <% if (selectedStockRoomId != null) { %>
            <div class="col pr-0">
                <button id="updateStockAdjustmentBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
            <% } %>
        </div>
    </div>
    <% } %>
    <div class="card-body">
            <div class="table-responsive">
                <div class="card px-0">
                    <h5 class="text-primary pl-4">STOCK ADJUSTMENT DETAILS</h5>
                    <hr class="divider pt-1 bg-primary"/>

                    <form id="updateStockForm" class="row" method="post">
                        <div class="table-responsive">
                            <table id="updateStockAdjustmentTable"
                                   class="table table-striped table-sm table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Stock room</th>
                                    <th>Product Name</th>
                                    <th>Quantity Present</th>
                                    <th>Quantity Adjusted</th>
                                    <th>Batch number</th>
                                    <th>Expiry Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% simplifiedStockAdjustmentList.each { %>
                                <tr>
                                    <td>${it.itemCode}<input type='text' id='code' name='uuid[]' value='${it.itemStockUuid}' class='hidden'/></td>
                                    <td>${it.stockRoomName}</td>
                                    <td>${it.itemName}<input type='text' id='stockOperationId' name='stockOperationId' value='${it.id}' class='hidden'/></td>
                                    <td>${it.enteredQuantityInStore}</td>
                                    <td>${it.itemStockQuantity}</td>
                                    <td>${it.batchNumber}</td>
                                    <td>${it.expiryDate}</td>
                                </tr>
                                <% } %>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<% } %>
