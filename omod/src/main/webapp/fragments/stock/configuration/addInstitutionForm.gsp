<script type="text/javascript">
    jQuery.noConflict();
    jq(document).ready(function () {

        jq(function () {
            jq("#addInstitutionForm").submit(function (e) {
                e.preventDefault();
                const params = {
                    'name': jq('#name').val(),
                    'description': jq('#description').val(),
                };

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/configuration/addInstitutionForm", "saveCreatedInstitution")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Institution saved successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/configuration/institutions")}';
                    })
                    .fail(function (err) {
                            jq().toastmessage('showErrorToast', err.responseJSON.fieldErrors.name[0]);
                            // console.log(err)
                        }
                    )
            });

            jq("#editInstitutionForm").submit(function (e) {
                e.preventDefault();
                let params = {
                    'id': '${institutionId}',
                    'name': jq('#institutionName').val(),
                    'description': jq('#institutionDescription').val()
                };

                jq.getJSON('${ui.actionLink("botswanaemr", "stock/configuration/addInstitutionForm", "saveEditedInstitution")}', params)
                    .success(function (data) {
                        jq().toastmessage('showNoticeToast', "Institution updated successfully!");
                        location.href = '${ui.pageLink("botswanaemr", "stock/configuration/institutions")}';
                    })
                    .fail(function (err) {
                            jq().toastmessage('showErrorToast', err);
                            console.log(err)
                        }
                    )
            });
        });
    });
</script>
<% if (institution == null) { %>
<div class="card mt-4">
    <div class="card-body">
        <form method="post" id="addInstitutionForm">
            <button type="submit" id="addInstitutionBtn"
                    class="btn btn-primary p-2 mt-1 float-right"><% if (institutionId == null) { %> + Add <%
                } else { %> Update <% } %>Institution</button>
            <h5 class="text-primary">INSTITUTION DETAILS</h5>
            <hr class="divider pt-1 bg-primary"/>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">Institution Name:
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" id="name" name="name" required="true" class="form-control" placeholder="Name"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="description">Description:
                        <span class="text-danger">*</span>
                    </label>
                    <textarea class="form-control" rows="2" id="description" name="description"
                              required="true"></textarea>
                </div>
            </div>

            <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
        </form>
    </div>
</div>
<% } else { %>
<div class="card mt-4">
    <div class="card-body">
        <form method="post" id="editInstitutionForm">
            <button type="submit" id="editInstitutionBtn"
                    class="btn btn-primary p-2 mt-1 float-right">+ Update Institution</button>
            <h5 class="text-primary">INSTITUTION DETAILS</h5>
            <hr class="divider pt-1 bg-primary"/>
            <input type="number" id="institutionId" name="institutionId" value="${institution.id ?: ''}"
                   class="form-control"
                   style="display: none"/>

            <div class="form-row">
                <div class="form-group col-md-4 pr-3">
                    <label for="name">Institution Name:
                        <span class="text-danger">*</span>
                    </label>
                    <input type="text" id="institutionName" name="name" required="true" class="form-control"
                           placeholder="Name"
                           value="${institution.name ?: ''}"/>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="institutionDescription">Description:
                        <span class="text-danger">*</span>
                    </label>
                    <textarea class="form-control" rows="2" id="institutionDescription" name="description"
                              required="true">${institution.description ?: ''}</textarea>
                </div>
            </div>

            <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
        </form>
    </div>
</div>
<% } %>

