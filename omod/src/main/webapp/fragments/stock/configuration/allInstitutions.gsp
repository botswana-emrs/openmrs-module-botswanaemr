<script type="text/javascript">
    jQuery(function () {
        let table = jQuery('#institutionsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="card px-0">
    <form class="row">
        <div class="col-sm-12 col-md-12 col-lg-10">
            <input class="form-control" id="nameOfInstitution" name="nameOfInstitution" placeholder="Name Of institution"/>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-2 pt-3 pr-3">
            <button type="submit" class="btn btn-primary" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </div>
    </form>
    <div class="table-responsive">
        <table id="institutionsTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            <% allInstitutions.each {%>
                <tr>
                    <td>${it.id}</td>
                    <td>${it.name}</td>
                    <td>${it.description}</td>
                    <td>
                        <a href="/${ui.contextPath()}/botswanaemr/stock/configuration/addInstitutionForm.page?id=${it.id}"
                           class="color-primary"> View
                        </a> |
                        <a href="#" class="delete" data-overlay="${it.id}">Delete</a>
                    </td>
                </tr>
            <% } %>
            </tbody>
        </table>
    </div>
</div>
