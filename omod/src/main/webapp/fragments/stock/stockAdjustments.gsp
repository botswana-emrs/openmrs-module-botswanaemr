<script type="text/javascript">
    
    jQuery(function () {
        const table = jQuery('#stockAdjustmentTable').DataTable({
            dom: 'rtp',
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: '${ui.actionLink("botswanaemr", "stock/stockAdjustments", "getAllAdjustments")}',
            columns: [
                { data: 'id' },
                { data: 'operationDate' },
                { data: 'officer' },
                { data: 'stockOperationType' },
                { data: 'comment' },
                { data: 'status' },
                null,
            ],
            columnDefs: [
                {
                    targets: -1,
                    data: null,
                    className: "text-left",
                    render: function(data, type, row, meta ) {
                     return '<a href="/${ui.contextPath()}/botswanaemr/stock/createStockAdjustment.page?stockOperationId=' + data.id + '" class="color-primary"> View </a>'

                    },
                },
            ],
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;

            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   table.search(searchText).draw();
                }, delay);
            }
        });
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="input-group col-12 pl-0 pr-0">
            <input type="text"
                   class="form-control py-2"
                   id="searchPhrase"
                   name="searchPhrase"
                   placeholder="Search">
            <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="stockAdjustmentTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date Of Stock Take</th>
                <th>Officer</th>
                <th>Type of Adjustment</th>
                <th>Comment</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
</div>