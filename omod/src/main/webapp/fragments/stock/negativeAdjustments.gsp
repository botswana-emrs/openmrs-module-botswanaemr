
<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#outgoingExternalRequisitionsTable, #incomingExternalRequisitionsTable, #approvedIncomingExternalRequisitionsTable, #itemsToReceiveTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="row mb-5">
    <div class="input-group col-12 pl-0 pr-0">
        <input type="text"
               class="form-control py-2"
               value="" id="searchPhrase"
               name="searchPhrase" placeholder="Search">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
</div>

<div class="table-responsive">
    <h3 class="body">Negative adjustments</h3>
    <table id="incomingExternalRequisitionsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request From</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
             <% incomingStockOperations.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.destination.location}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/requisitionApplications', [id: it.id, requestType: "external"])}">View</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>


<div class="table-responsive">
    <h3 class="body">Approved negative adjustments</h3>
    <table id="approvedIncomingExternalRequisitionsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request From</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
             <% outgoingStockOperationsToDispatch.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.destination.location}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/requisitionApplications', [id: it.id, requestType: "external", actionType: "transfer"])}">View</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>


<div class="table-responsive">
    <h3 class="body">Items to Receive</h3>
    <table id="itemsToReceiveTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Request From</th>
            <th>Description</th>
            <th>Date</th>
            <th>Requestor</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
             <% incomingStockOperationsToReceive.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.destination.location}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/requisitionItemReceipt', [id: it.id, requestType: "externalReceive", actionType: "transfer"])}">View</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>


