<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55"
             height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'stock/stockManagementLandingPg')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>

            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="#" class="nav_link"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <div class="nav_icon_container">
                <i class="fa fa-file-text fa-1x"></i>
            </div>

            <div class="nav_label">Requisitions</div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/positiveAdjustmentsDashboard')}">
                    <i class="fas fa-file-alt"></i> Positive adjustments
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/negativeAdjustmentsDashboard')}">
                    <i class="fas fa-file-alt"></i> Negative adjustments
                </a>
                <!--<a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/genReports')}">
                    <i class="fas fa-file-alt"></i> Gen 12 Reports
                </a>-->
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}">
                    <i class="fas fa-file-alt"></i> Internal
                </a>
            </div>
        </a>
    </li>
<!--    <li>
        <a href="#" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calculator fa-1x"></i>
            </div>

            <div class="nav_label">Adjustment</div>
        </a>
    </li>
    <li>
        <a href="#" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-dolly-flatbed fa-1x"></i>
            </div>
            <div class="nav_label">CMS Returns</div>
        </a>
    </li>
-->
    <!--    <li>
        <a href="#" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-dolly fa-1x"></i>
            </div>
            <div class="nav_label">Internal Surplus</div>
        </a>
    </li>
    -->
    <li>
        <a href="#" class="nav_link"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <div class="nav_icon_container">
                <i class="fa fa-pie-chart fa-1x"></i>
            </div>

            <div class="nav_label">
                Stock Management
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/products')}">
                    <i class="fas fa-file-alt"></i> Products
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/initialStockOperation')}">
                    <i class="fas fa-clipboard"></i> Initial stock operation
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/stockTake')}">
                    <i class="fas fa-clipboard-check"></i> Stock Take
                </a>
<!--                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/stockAdjustment')}">
                    <i class="fas fa-clipboard-check"></i> Stock Adjustment
                </a>
-->
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/directReceipt')}">
                    <i class="fas fa-clipboard-check"></i> Direct Receipt
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/discrepancyListing')}">
                    <i class="fas fa-clipboard-check"></i> Discrepancy Listing
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/configuration')}">
                    <i class="fas fa-tools"></i> Configuration
                </a>
            </div>
        </a>
    </li>
    <li>

    </li>
    <!--    <li>
        <a href="#" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-prescription-bottle fa-1x"></i>
            </div>
            <div class="nav_label">Prescription Management</div>
        </a>
    </li>
-->
    <li>
        <a href="${ui.pageLink('botswanaemr', 'stock/batchHistory')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-plus-square-o fa-1x"></i>
            </div>
            <div class="nav_label">Batch Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

