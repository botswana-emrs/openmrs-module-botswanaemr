<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#viewStockItemsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jQuery('#searchStockItemForm').submit(function (e) {
            e.preventDefault();
            let searchString = jQuery('#searchPhrase').val();
            if (searchString.trim().length > 0) {
                getItems(searchString);
            }
        });

        function getItems(searchString) {
            let searchResult;

            let params = {};
            let stockRoomId = '${stockRoomId}';
            if (stockRoomId === '') {
                jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                return;
            }

            if (searchString.trim().length > 0) {
                params = {
                    q: searchString,
                    stockRoomId: stockRoomId
                }
            } else {
                params = {
                    stockRoomId: stockRoomId
                }
            }

            jQuery.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","stock/createStockTake","getItemStockData")}',
                dataType: "json",
                global: false,
                async: false,
                data: params,
                success: function (data) {
                    console.log(data);
                    searchResult = data;
                    jQuery("#viewStockItemsTable tbody").empty();
                    searchResult.map((item) => {
                        jQuery("#viewStockItemsTable tbody")
                            .append("<tr><td>" + item.code + "</td><td>" + item.name + "</td><td>" + item.quantity + "</td><td>" + new Date(item.expiryDate).toDateInputValue() + "</td></tr>");
                    });
                    jQuery('#viewStockItemsTable').DataTable();
                    return searchResult;
                }
            });
        }
    });
</script>

<div class="card mt-4">
    <div class="card-body">
        <div class="table-responsive">
            <div class="card px-0">
                <div class="row">
                    <div class="col col-md-6">
                        <h5 class="text-primary pl-4">STOCK ITEMS</h5>
                    </div>

                    <div class="col col-md-6">
                        <h4>${stockRoomName}</h4>
                    </div>
                </div>

                <hr class="divider pt-1 bg-primary"/>

                <form id="searchStockItemForm" class="row" method="post">
                    <div class="form-group col-md-5 pr-3">
                        <label for="searchPhrase">Search</label>
                        <input type="text"
                               class="form-control"
                               id="searchPhrase"
                               name="searchPhrase"
                               placeholder="Search"/>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                        <label for="searchBtn">&nbsp;</label>
                        <button class="btn btn-primary float-right" type="submit" id="searchBtn" name="searchBtn">
                            search
                        </button>
                    </div>

                    <div class="table-responsive">
                        <table id="viewStockItemsTable"
                               class="table table-striped table-sm table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Product Name</th>
                                <th>Quantity In System</th>
                                <th>Expiry Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <% simplifiedStockList.each { %>
                            <tr>
                                <td>${it.itemCode}</td>
                                <td>${it.itemName}</td>
                                <td>${it.itemStockQuantity}</td>
                                <td>${it.expiryDate}</td>
                            </tr>
                            <% } %>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
