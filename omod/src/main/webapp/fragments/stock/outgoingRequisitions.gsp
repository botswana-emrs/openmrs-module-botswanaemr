<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#outgoingRequisitionsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="table-responsive">
    <table id="outgoingRequisitionsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Description</th>
            <th>Date</th>
            <th>Request From</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% operations.each { %>
            <tr>
                <td>${it.id}</td>
                <td>${it.description}</td>
                <td>${ui.formatDatePretty(it.operationDate)}</td>
                <td>${ui.format(it.creator)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i> ${it.status}
                </td>
                <td>
                    <a href="${ui.pageLink('botswanaemr', 'stock/viewRequisition', [id: it.id])}&returnUrl=${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}"><i class="fa fa-eye"></i> View</a> |
                    <a href="${ui.pageLink('botswanaemr', 'stock/printRequisitions', [id: it.id])}&returnUrl=${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}"><i class="fa fa-file"></i> Gen 12</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>
