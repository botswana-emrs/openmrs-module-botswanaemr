
<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#externalRequisitionsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="row mb-5">
    <div class="input-group col-12 pl-0 pr-0">
        <input type="text"
               class="form-control py-2"
               id="searchPhrase"
               name="searchPhrase" placeholder="Search">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
</div>

<div class="table-responsive">
    <table id="externalRequisitionsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Description</th>
            <th>Date</th>
            <th>Request From</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>New Requisitions</td>
            <td>05-June-2022</td>
            <td>Jake Smith</td>
            <td>
                <i class="fa fa-circle text-success"></i> Approved
            </td>
            <td>
                <button type="button"
                        id="viewExternalRequisitionsBtn"
                        class="btn btn-sm btn-primary btn-outline border-0"
                        data-toggle="modal"
                        data-target="#viewExternalRequisitionsModal"> View
                </button>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<div class="modal fade" id="viewExternalRequisitionsModal" data-controls-modal="viewExternalRequisitionsModalControls"
     data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="viewExternalRequisitionsModalTitle">

    <div id="viewExternalRequisitionsData" class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
         role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="viewExternalRequisitionsModalTitle">View Positive and negative adjustments</h5>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>ID:</th>
                            <td>1</td>
                        </tr>
                        <tr>
                            <th>ITEM:</th>
                            <td>N95 Face Masks</td>
                        </tr>
                        <tr>
                            <th>QUANTITY REQUESTED:</th>
                            <td>3,000</td>
                        </tr>
                        <tr>
                            <th>AVAILABLE IN STOCK:</th>
                            <td>5,000</td>
                        </tr>
                        <tr>
                            <th>DATE:</th>
                            <td>Jan 2 2022</td>
                        </tr>
                        <tr>
                            <th>REQUEST FROM:</th>
                            <td>Jane Doe: Lab</td>
                        </tr>
                        <tr>
                            <th>STATUS:</th>
                            <td>Not Fulfilled</td>
                        </tr>
                        <tr>
                            <th>DESCRIPTION:</th>
                            <td>This is the very first request for new masks</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Reject</button>
                <button type="button" class="btn btn-primary">Approve</button>
            </div>
        </div>
    </div>
</div>
