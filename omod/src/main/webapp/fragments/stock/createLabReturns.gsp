<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#createLabReturnsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        var table = jQuery('#updateLabReturnsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#tBodyAddReturn').on('change', 'select[name^="kitName"]', function() {
            if (jq("#stockRoom").val()) {
                let conceptId = jq(this).val();
                let selectedKitQuantity = jQuery(this).closest('tr').find('input[name^="returnQuantity"]');
                let batchIdInput = jQuery(this).closest('tr').find('input[name^="batchNumber"]');
                let quantityInput = jQuery(this).closest('tr').find('input[name^="itemStockQuantity"]');
                let expiryInput = jQuery(this).closest('tr').find('input[name^="expiryDate"]');
                expiryInput.prop('disabled', true);

                let stockRoomInput = jq("#stockRoom");
                fetchHtsKitAvailableBatches(conceptId, selectedKitQuantity, batchIdInput, expiryInput, stockRoomInput, quantityInput);
            } else {
                jq().toastmessage('showErrorToast', "Select the stock room first");
            }
        });

        function cloneRow() {
            let rowCount = jq('#tBodyAddReturn > tr').length;
            let index = rowCount + 1;
            let newRow = jQuery('#mainTr').clone();
            jq(newRow).attr("id", "trReturn" + index)
            newRow.find('input,textarea').each(function () {
                jQuery(this).val('');
            });
            jq(newRow).find('#kitName').attr("id", "kitName" + rowCount);
            jq(newRow).find('#stockOperationId').attr("id", "stockOperationId" + rowCount)

            newRow.insertAfter('#createLabReturnsForm table:first>tbody>tr:last').fadeIn();
            // jq("#tBodyAddReturn").append(newRow);

            return index;
        }

        cloneRow();

        jQuery('#createLabReturnsForm table:first').on('click', 'a', function () {
            jQuery(this).parents('tr').remove().fadeIn();
        });


        jq("#btnAddReturn").click(function (e) {
            e.preventDefault();
            cloneRow();
        });

        jq('#createLabReturnsForm').submit((e) => {
            e.preventDefault();
            getItems();
            // table.draw();
        });

        jq('#updateLabReturnsForm').submit((e) => {
            e.preventDefault();
            getItems();
            // table.draw();
        });

        jQuery('#stockRoom').change(function () {
            if (jQuery(this).val() !== '') {
                // getItems();
            }
        });

        function fetchHtsKitAvailableBatches(conceptId, selectedKitQuantity, batchIdInput, expiryInput, stockRoomInput, quantityInput) {
            let parameter = {
                'conceptId': conceptId,
                'stockroomId': stockRoomInput.val()
            };
            let batchNumberToExpiry = {};

            jq.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsKitAvailableBatches.action', parameter)
                .done(function (data) {
                    if (data.status === 'success') {
                        // populate autocomplete with batches
                        if (data.batches) {
                            let quantity = data.availableQuantity;

                            jq.each(data.batches, function (i, item) {
                                batchNumberToExpiry[item.batchNumber] = item;
                                if (data.batchNumbers[i]) {
                                    let val = item.batchNumber + " (" + batchNumberToExpiry[item.batchNumber].expiryDate.replaceAll("/", "-") + ")";
                                    data.batchNumbers[i] = val;
                                    batchNumberToExpiry[val] = item;
                                }
                            })

                            jq(batchIdInput).autocomplete({
                                source: data.batchNumbers,
                                minLength: 0,
                                select: function (event, ui) {
                                    jq(expiryInput).val(batchNumberToExpiry[ui.item.value].expiryDate.replaceAll("/", "-"));
                                    jq(quantityInput).val(batchNumberToExpiry[ui.item.value].quantity);
                                    jQuery(this).closest('tr').find('input[name^="uuid"]').val(batchNumberToExpiry[ui.item.value].itemStockUuid);
                                    jQuery(this).closest('tr').find('#codeVal').html(batchNumberToExpiry[ui.item.value].code);
                                    jQuery(this).closest('tr').find('input[name^="stockOperationId"]').val(batchNumberToExpiry[ui.item.value].batchOperation);
                                }
                            }).blur(function () {
                                jq(this).autocomplete('enable');
                            }).focus(function () {
                                jq(this).autocomplete('search', '');
                            });
                        }
                    } else {
                        if (selectedKitQuantity.length) {
                            selectedKitQuantity.replaceWith('<input type="hidden" class="kit-quantity" value="">');
                        } else {
                            jq(".kit-quantity").val('');
                        }
                    }
                });
        }

        jq('#createLabReturnsTable').on('change', 'input[name^="returnQuantity"]', function () {
            let systemQty = jq(this).closest('tr').find('input[name^="itemStockQuantity"]').val();
            if (jq(this).val() && systemQty) {
                let balance = systemQty - jq(this).val();
                jq(this).closest('tr').find('input[name^="balance"]').val(balance);
            }
        })

        function getItemStock(trow) {
            let itemStock = null;
            let quantity = jq(trow).find("input[name^=returnQuantity]").val();
            if (quantity !== '') {
                itemStock = {};
                itemStock["uuid"] = jq(trow).find("input[name^=uuid]").val();
                itemStock["quantity"] = quantity;
                itemStock["expiryDate"] = jq(trow).find("input[name^=expiryDate]").val();
                itemStock["returnReason"] = jq(trow).find("select[name^=returnReason]").val();
                itemStock["batchNumber"] = jq(trow).find("input[name^=batchNumber]").val();
                itemStock["batchOperationId"] = jq(trow).find("input[name^=stockOperationId]").val();
            }
            return itemStock;
        }

        jq('#createLabReturnsBtn').on('click', (e) => {
            e.preventDefault();
            let stockRoomId = jQuery('#stockRoom').val().trim();
            let destinationStockRoomId = jQuery('#destinationStockRoom').val().trim();

            if (stockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select a source stock room to proceed");
                return;
            }

            if (destinationStockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select a destination stock room to proceed");
                return;
            }

            if (stockRoomId === destinationStockRoomId) {
                jq().toastmessage('showErrorToast', "The source and destination stock rooms cannot be the same");
                return;
            }

            let tb = jq("#createLabReturnsTable tbody");
            let jsonObj = [];

            tb.find("tr").each(function (index, element) {
                let itemStock = getItemStock(element);
                if (itemStock !== null) {
                    jsonObj.push(itemStock);
                }
            });

            if (jsonObj.length === 0) {
                jq().toastmessage('showErrorToast', "Add itemStock(s) quantity in store to proceed");
                return;
            }

            const newObj = Object.assign({}, jsonObj);

            saveItemStock(JSON.stringify(newObj), stockRoomId, destinationStockRoomId);
        });

         jq('#updateLabReturnsBtn').on('click', (e) => {
                    e.preventDefault();
                    let stockRoomId = jQuery('#stockRoom').val().trim();
                    let destinationStockRoomIdstockRoomId = jQuery('#destinationStockRoom').val().trim();

                    if (stockRoomId.trim().length === 0) {
                        jq().toastmessage('showErrorToast', "Select a source stock room to proceed");
                        return;
                    }

                    if (destinationStockRoomId.trim().length === 0) {
                        jq().toastmessage('showErrorToast', "Select a destination stock room to proceed");
                        return;
                    }

                    if (stockRoomId === destinationStockRoomId) {
                        jq().toastmessage('showErrorToast', "The source and destination stock rooms cannot be the same");
                        return;
                    }

                    var tb = jq("#updateLabReturnsTable tbody");
                    jsonObj = [];

                    tb.find("tr").each(function (index, element) {
                        let itemStock = getItemStock(element);
                        if (itemStock !== null) {
                            jsonObj.push(itemStock);
                        }
                    });

                    if (jsonObj.length === 0) {
                        jq().toastmessage('showErrorToast', "Add itemStock(s) quantity in store to proceed");
                        return;
                    }

                    const newObj = Object.assign({}, jsonObj);

                    saveEditedItemStock(JSON.stringify(newObj), stockRoomId);
                });
    });

    function getItems() {
        let searchResult;

        let params = {};
        let stockRoomId = jQuery('#stockRoom').val();
        if (stockRoomId === '') {
            jq().toastmessage('showErrorToast', "Select a stock room to proceed");
            return;
        }

        let searchString = jQuery('#searchPhrase').val();
        if (searchString.trim().length > 0) {
            params = {
                q: searchString,
                stockRoomId: stockRoomId
            }
        } else {
            params = {
                stockRoomId: stockRoomId
            }
        }

        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","stock/createLabReturns","getItemStockData")}',
            dataType: "json",
            global: false,
            async: false,
            data: params,
            success: function (data) {
                searchResult = data;
                jQuery("#createLabReturnsTable tbody").empty();
                searchResult.map((item) => {
                    let today = new Date().toISOString().split('T')[0];
                    jQuery("#createLabReturnsTable tbody")
                        .append("<tr><td>" + item.code + "<input type='text' name='uuid[]' value='" + item.uuid + "' class='hidden'/></td>" +
                            "<td>" + item.name + "</td>" +
                            "<td>" +
                            "<input type='text' name='batchNumber[]' class='form-control form-control-sm' value='" + item.batchNumber + "' />" +
                            "<input type='hidden' name='batchOperationId[]' class='form-control form-control-sm' value='" + item.batchOperationId + "' />" +
                            "</td>" +
                            "<td><input type='date' class='form-control' name='expiryDate[]' min='" + today + "' value='" + new Date(item.expiryDate).toDateInputValue() + "' /></td>" +
                            "<td>" + item.quantity + "</td>" +
                            "<td><input type='number' name='returnQuantity[]' class='form-control form-control-sm' min='1'/></td>" +
                            "<td></td>" +
                            "<td>" +
                                "<select class='form-control' name='returnReason[]'>" +
                                    "<option value='Damaged'>Damaged</option> " +
                                    "<option value='Close to expiry'>Close to expiry</option>" +
                                "</select>" +
                            "</td></tr>");
                });
                jQuery('#createLabReturnsTable').DataTable();
                return searchResult;
            }
        });

    }

    function saveItemStock(jsonObj, stockRoomId, destinationStockRoomId) {
        jQuery.ajax({
            type: 'POST',
            url: '${ui.actionLink("botswanaemr","stock/createLabReturns","saveItemStockData")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                itemStock: jsonObj,
                stockRoomId: stockRoomId,
                destinationStockRoomId: destinationStockRoomId
            },
            success: function (data) {
                jq().toastmessage('showNoticeToast', "Operation completed successfully");
                jq("#createLabReturnsTable > tbody").empty();
                jQuery('#stockRoom').val('');
                jQuery('#destinationStockRoom').val('');
                jQuery('#returnQuantity').val('');
                jQuery('#returnReason').val('');
                location.href = '${ui.pageLink("botswanaemr", "stock/labReturns")}';
                return data;
            },
            fail: function (err) {
                jq().toastmessage('showErrorToast', "Operation Failed");
            }
        });
    }

    function saveEditedItemStock(jsonObj, stockRoomId) {
            jQuery.ajax({
                url: '${ui.actionLink("botswanaemr","stock/createLabReturns","saveEditedItemStockData")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    itemStock: jsonObj,
                    stockRoomId: stockRoomId,
                    stockOperationId: jQuery('#stockOperationId').val()
                },
                success: function (data) {
                    jq().toastmessage('showNoticeToast', "Operation completed successfully");
                    jq("#updateLabReturnsTable > tbody").empty();
                    jQuery('#stockRoom').val('');
                    jQuery('#destinationStockRoom').val('');
                    jQuery('#returnQuantity').val('');
                    jQuery('#returnReason').val('');
                    location.href = '${ui.pageLink("botswanaemr", "stock/labReturns")}';
                    return data;
                },
                fail: function (err) {
                    jq().toastmessage('showErrorToast', "Operation Failed");
                }
            });
        }
</script>

<style>
    .dataTables_wrapper {
        min-height: fit-content !important;
    }
</style>

<% if(simplifiedLabReturnsList == "") {%>
<div class="card mt-4">
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Create Stock Returns</h5>
            </div>

            <div class="col pr-0">
                <button id="createLabReturnsBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="card px-0">
                <h5 class="text-primary pl-4">Returns Details</h5>
                <hr class="divider pt-1 bg-primary"/>

                <form id="createLabReturnsForm" class="row" method="post">
                    <div class="form-group col-md-5 pr-3">
                        <label for="stockRoom">Source Stock Room:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" id="stockRoom" required>
                            <option value="">Select Stock Room</option>
                            <% sourceInventoryStockRooms.each { %>
                            <option value= ${it.uuid}>${it.name}</option>
                            <% } %>
                        </select>
                    </div>
                    <div class="form-group col-md-5 pr-3">
                        <label for="destinationStockRoom">Destination Stock Room:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" id="destinationStockRoom">
                            <option value="">Select Stock Room</option>
                            <% destinationInventoryStockRooms.each { %>
                            <option value= ${it.id}>${it.name}</option>
                            <% } %>
                        </select>
                    </div>

                    <div class="form-group col-md-5 pr-3">
                        <label for="searchPhrase">Search</label>
                        <input type="text"
                               class="form-control"
                               id="searchPhrase"
                               name="searchPhrase"
                               placeholder="Search"/>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                        <label for="searchBtn">&nbsp;</label>
                        <button class="btn btn-primary float-right" type="submit" id="searchBtn" name="searchBtn">
                            search
                        </button>
                    </div>

                    <div class="table-responsive">
                        <table id="createLabReturnsTable"
                               class="table table-striped table-sm table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Product Name</th>
                                <th>Batch Number</th>
                                <th>Expiry Date</th>
                                <th>Qty In System</th>
                                <th>Qty To Return</th>
                                <th>Balance</th>
                                <th>Reason</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody id="tBodyAddReturn">
                            <tr id="mainTr" class="qualityTest hidden">
                                <td><span id="codeVal"></span><input type='text' id='code' name='uuid[]' class='form-control form-control-sm hidden'/></td>
                                <td>
                                    <select type='text' id="kitName" name='kitName[]'
                                            class='form-control form-control-sm kitName'>
                                        <option value="" selected disabled>Select kit name</option>
                                        <% testKitNames.each { key, val -> %>
                                        <option value="${val}">
                                            ${key}
                                        </option>
                                        <% } %>
                                    </select>
                                    <input type='text' id='batchOperationId' name='stockOperationId[]' class='hidden'/>
                                </td>
                                <td><input type='text' id='batch' name='batchNumber[]' class='form-control form-control-sm'/>
                                </td>
                                <td><input type='text' id='expiryDate' name='expiryDate[]'
                                           class='form-control form-control-sm'/></td>
                                <td><input type='number' id='systemQuantity' name='itemStockQuantity[]'
                                           class='form-control form-control-sm' min='0' disabled/></td>
                                <td><input type='number' id='returnQuantity' name='returnQuantity[]'
                                           class='form-control form-control-sm' min='0'/></td>
                                <td id='balance'><input type="text" readonly name="balance[]" class="form-control form-control-sm"/></td>
                                <td>
                                    <select name="returnReason">
                                        <option value="Damaged">Damaged</option>
                                        <option value="Close to expiry">Close to expiry</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="title-margin-left">
                                        <a class="btn btn-sm btn-light right">Delete</a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="row mt-1">
                            <button class="dashed-button rounded add-more"
                                    id="btnAddReturn">+ Add
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<% } else { %>
<div class="card mt-4">
    <% if (stockOperationStatus == "PENDING") { %>
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Update Stock Returns</h5>
            </div>
            <% if (selectedStockRoomId != null) { %>
            <div class="col pr-0">
                <button id="updateLabReturnsBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
            <% } %>
        </div>
    </div>
    <% } %>
    <div class="card-body">
            <div class="table-responsive">
                <div class="card px-0">
                    <h5 class="text-primary pl-4">Stock Returns Details</h5>
                    <hr class="divider pt-1 bg-primary"/>

                    <form id="updateLabReturnsForm" class="row" method="post">
                        <div class="form-group col-md-5 pr-3">
                            <label for="stockRoom">Source Stock Room:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="form-control" id="stockRoom" required>
                                <option value="">Select Stock Room</option>
                                <% sourceInventoryStockRooms.each { %>
                                <option value="${it.id}" <% if(selectedStockRoomId == it.id) { %> selected <% } %> >${it.name}</option>
                                <% } %>
                            </select>
                        </div>
                        <div class="form-group col-md-5 pr-3">
                            <label for="stockRoom">Destination Stock Room:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="form-control" id="destinationStockRoom" required>
                                <option value="">Select Stock Room</option>
                                <% destinationInventoryStockRooms.each { %>
                                <option value="${it.id}" <% if(selectedStockRoomId == it.id) { %> selected <% } %> >${it.name}</option>
                                <% } %>
                            </select>
                        </div>

                        <div class="form-group col-md-5 pr-3">
                            <label for="searchPhrase">Search</label>
                            <input type="text"
                                   class="form-control"
                                   id="searchPhrase"
                                   name="searchPhrase"
                                   placeholder="Search"/>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                            <label for="searchBtn">&nbsp;</label>
                            <button class="btn btn-primary float-right" type="button" id="updateSearchBtn" name="searchBtn">
                                search
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table id="updateLabReturnsTable"
                                   class="table table-striped table-sm table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Product Name</th>
                                    <th>Batch number</th>
                                    <th>Expiry Date</th>
                                    <th>Qty returned</th>
                                    <th>Reason</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% simplifiedLabReturnsList.each { %>
                                <tr>
                                    <td>${it.itemCode}<input type='text' id='code' name='uuid[]' value='${it.itemStockUuid}' class='hidden'/></td>
                                    <td>${it.itemName}<input type='text' id='stockOperationId' name='stockOperationId[]' value='${it.id}' class='hidden'/></td>
                                    <td>${it.batchNumber}</td>
                                    <td>${it.expiryDate}</td>
                                    <td>${it.itemStockQuantity ?: ''}</td>
                                    <td>${it.reason}</td>
                                </tr>
                                <% } %>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <!-- <div>
                    <button type="button" class="dashed-button rounded add-more" id="btnAddLabReturns">+ Add</button>                
                </div>
                -->
            </div>
        </div>
    </div>
<% } %>
