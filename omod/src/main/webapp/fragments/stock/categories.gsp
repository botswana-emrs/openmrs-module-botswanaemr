<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#categoriesTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="card px-0">
    <form class="row">
        <div class="col-sm-12 col-md-12 col-lg-10">
            <input class="form-control" id="nameOfCategory" name="nameOfCategory" placeholder="Name Of Category"/>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-2 pt-3 pr-3">
            <button class="btn btn-primary float-right">Add Category</button>
        </div>
    </form>
    <div class="table-responsive">
        <table id="categoriesTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Masks</td>
                    <td>
                        <a href="#">Remove</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>