<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#createStockTakeTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        var table = jQuery('#updateStockTakeTable').DataTable({
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

        jq('#createStockForm').submit((e) => {
            e.preventDefault();
            getItems();
            // table.draw();
        });

        jq('#updateStockForm').submit((e) => {
            e.preventDefault();
            getItems();
            // table.draw();
        });

        jQuery('#stockRoom').change(function () {
            if (jQuery(this).val() !== '') {
                getItems();
            }
        });

        function getItemStock(trow) {
            let itemStock = null;
            let quantity = jq(trow).find("input[name^=quantity]").val();
            let systemQuantity = jq(trow).find("input[name^=systemQuantity]").val();
            if (quantity !== '') {
                itemStock = {};
                itemStock["uuid"] = jq(trow).find("input[name^=uuid]").val();
                itemStock["quantity"] = quantity;
                itemStock["systemQuantity"] = systemQuantity;
                itemStock["expiryDate"] = jq(trow).find("input[name^=expiryDate]").val();
                itemStock["comments"] = jq(trow).find("input[name^=comments]").val();
                itemStock["batchNumber"] = jq(trow).find("input[name^=batchNumber]").val();
                itemStock["batchOperationId"] = jq(trow).find("input[name^=batchOperationId]").val();
                itemStock["varianceQty"] = jq(trow).find("input[name^=varianceQty]").val();
                itemStock["varianceReason"] = jq(trow).find("input[name^=varianceReason]").val();
            }
            return itemStock;
        }

        jq('#createStockTakeBtn').on('click', (e) => {
            e.preventDefault();
            let stockRoomId = jQuery('#stockRoom').val().trim();

            if (stockRoomId.trim().length === 0) {
                jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                return;
            }

            let tb = jq("#createStockTakeTable tbody");
            let jsonObj = [];

            tb.find("tr").each(function (index, element) {
                let itemStock = getItemStock(element);
                if (itemStock !== null) {
                    jsonObj.push(itemStock);
                }
            });

            if (jsonObj.length === 0) {
                jq().toastmessage('showErrorToast', "Add itemStock(s) quantity in store to proceed");
                return;
            }

            const newObj = Object.assign({}, jsonObj);

            saveItemStock(JSON.stringify(newObj), stockRoomId);
        });

         jq('#updateStockTakeBtn').on('click', (e) => {
                    e.preventDefault();
                    let stockRoomId = jQuery('#stockRoom').val().trim();

                    if (stockRoomId.trim().length === 0) {
                        jq().toastmessage('showErrorToast', "Select a stock room to proceed");
                        return;
                    }

                    var tb = jq("#updateStockTakeTable tbody");
                    jsonObj = [];

                    tb.find("tr").each(function (index, element) {
                        let itemStock = getItemStock(element);
                        if (itemStock !== null) {
                            jsonObj.push(itemStock);
                        }
                    });

                    if (jsonObj.length === 0) {
                        jq().toastmessage('showErrorToast', "Add itemStock(s) quantity in store to proceed");
                        return;
                    }

                    const newObj = Object.assign({}, jsonObj);

                    saveEditedItemStock(JSON.stringify(newObj), stockRoomId);
                });

         // Update the variance cell when quantity in store is changed
        jq('#createStockTakeTable').on('change', 'input[name^=quantity]', function () {
            let quantityInStore = jq(this).val();
            let quantityInSystem = jq(this).closest('tr').find('td:eq(2)').text();
            let variance = quantityInStore - quantityInSystem;
            let sign = variance >= 0 ? '+' : '';
            // jq(this).closest('tr').find('td:eq(4)').text(variance);
            jq(this).closest('tr').find('input[name^=varianceQty]').val(sign + variance);
        });
    });

    function getItems() {
        let searchResult;

        let params = {};
        let stockRoomId = jQuery('#stockRoom').val();
        if (stockRoomId === '') {
            jq().toastmessage('showErrorToast', "Select a stock room to proceed");
            return;
        }

        let searchString = jQuery('#searchPhrase').val();
        if (searchString.trim().length > 0) {
            params = {
                q: searchString,
                stockRoomId: stockRoomId
            }
        } else {
            params = {
                stockRoomId: stockRoomId
            }
        }

        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","stock/createStockTake","getItemStockData")}',
            dataType: "json",
            global: false,
            async: false,
            data: params,
            success: function (data) {
                console.log(data);
                searchResult = data;
                jQuery("#createStockTakeTable tbody").empty();
                searchResult.map((item) => {
                    let today = new Date().toISOString().split('T')[0];
                    jQuery("#createStockTakeTable tbody")
                        .append("<tr><td>" + item.code + "<input type='text' name='uuid[]' value='" + item.uuid + "' class='hidden'/></td>" +
                            "<td>" + item.name + "</td>" +
                            "<td>" + item.quantity + "</td>" +
                            "<td><input type='number' name='quantity[]' class='form-control form-control-sm' min='0'/></td>" +
                            "<td><input type='text' name='varianceQty[]' class='form-control form-control-sm' readonly='readonly' min='0'/></td>" +
                            "<td><input type='text' class='form-control' name='varianceReason[]' value='' /></td>" +
                            "<td>" +
                            "<input type='text' name='batchNumber[]' class='form-control form-control-sm' value='" + item.batchNumber + "' />" +
                            "<input type='hidden' name='batchOperationId[]' class='form-control form-control-sm' value='" + item.batchOperationId + "' />" +
                            "<input type='hidden' name='systemQuantity[]' class='form-control form-control-sm' value='" + item.quantity + "' />" +
                            "</td>" +
                            "<td><input type='date' class='form-control' name='expiryDate[]' min='" + today + "' value='" + new Date(item.expiryDate).toDateInputValue() + "' /></td>" +
                            "<td><input type='text' class='form-control' name='comments[]' value='' /></td></tr>");
                });
                jQuery('#createStockTakeTable').DataTable();
                return searchResult;
            }
        });

    }

    function saveItemStock(jsonObj, stockRoomId) {
        showLoadingOverlay();
        jQuery.ajax({
            type: 'POST',
            url: '${ui.actionLink("botswanaemr","stock/createStockTake","saveItemStockData")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                itemStock: jsonObj,
                stockRoomId: stockRoomId
            },
            success: function (data) {
                // console.log(data);
                jq().toastmessage('showNoticeToast', "Operation completed successfully");
                jq("#createStockTakeTable > tbody").empty();
                jQuery('#stockRoom').val('');
                jQuery('#updateQuantity').val('');
                location.href = '${ui.pageLink("botswanaemr", "stock/stockTake")}';
                hideLoadingOverlay();
                return data;
            },
            fail: function (err) {
                jq().toastmessage('showErrorToast', "Operation Failed");
            }
        });
    }

    function saveEditedItemStock(jsonObj, stockRoomId) {
            jQuery.ajax({
                url: '${ui.actionLink("botswanaemr","stock/createStockTake","saveEditedItemStockData")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    itemStock: jsonObj,
                    stockRoomId: stockRoomId,
                    stockOperationId: jQuery('#stockOperationId').val()
                },
                success: function (data) {
                    // console.log(data);
                    jq().toastmessage('showNoticeToast', "Operation completed successfully");
                    jq("#updateStockTakeTable > tbody").empty();
                    jQuery('#stockRoom').val('');
                    jQuery('#updateQuantity').val('');
                    location.href = '${ui.pageLink("botswanaemr", "stock/stockTake")}';
                    return data;
                },
                fail: function (err) {
                    jq().toastmessage('showErrorToast', "Operation Failed");
                }
            });
        }
</script>

<% if(simplifiedStockTakeList == "") {%>
<div class="card mt-4">
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Create Stock Take</h5>
            </div>

            <div class="col pr-0">
                <button id="createStockTakeBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="card px-0">
                <h5 class="text-primary pl-4">STOCK TAKE DETAILS</h5>
                <hr class="divider pt-1 bg-primary"/>

                <form id="createStockForm" class="row" method="post">
                    <div class="form-group col-md-5 pr-3">
                        <label for="stockRoom">Stock Room:
                            <span class="text-danger">*</span>
                        </label>
                        <select class="form-control" id="stockRoom" required>
                            <option value="">Select Stock Room</option>
                            <% inventoryStockRooms.each { %>
                            <option value= ${it.id}>${it.name}</option>
                            <% } %>
                        </select>
                    </div>
                    <div class="form-group col-md-5 pr-3">
                        <label for="searchPhrase">Search</label>
                        <input type="text"
                               class="form-control"
                               id="searchPhrase"
                               name="searchPhrase"
                               placeholder="Search"/>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                        <label for="searchBtn">&nbsp;</label>
                        <button class="btn btn-primary float-right" type="submit" id="searchBtn" name="searchBtn">
                            search
                        </button>
                    </div>

                    <div class="table-responsive">
                        <table id="createStockTakeTable"
                               class="table table-striped table-sm table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Product Name</th>
                                <th>Quantity In System</th>
                                <th>Quantity In Store</th>
                                <th>Variance</th>
                                <th>Variance reason</th>
                                <th>Batch Number</th>
                                <th>Expiry Date</th>
                                <th>Comments</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><input type='number' id='updateQuantity' name='updateQuantity'
                                           class='form-control form-control-sm' min='0'/></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<% } else { %>
<div class="card mt-4">
    <% if (stockOperationStatus == "PENDING") { %>
    <div class="card-header mb-4">&nbsp;
        <div class="row">
            <div class="col-10">
                <h5>Update Stock Take</h5>
            </div>
            <% if (selectedStockRoomId != null) { %>
            <div class="col pr-0">
                <button id="updateStockTakeBtn" type="button" class="btn btn-success btn-block float-right">Submit</button>
            </div>
            <% } %>
        </div>
    </div>
    <% } %>
    <div class="card-body">
            <div class="table-responsive">
                <div class="card px-0">
                    <h5 class="text-primary pl-4">STOCK TAKE DETAILS</h5>
                    <hr class="divider pt-1 bg-primary"/>

                    <form id="updateStockForm" class="row" method="post">
                        <div class="form-group col-md-5 pr-3">
                            <label for="stockRoom">Stock Room:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="form-control" id="stockRoom" required>
                                <option value="">Select Stock Room</option>
                                <% inventoryStockRooms.each { %>
                                <option value="${it.id}" <% if(selectedStockRoomId == it.id) { %> selected <% } %> >${it.name}</option>
                                <% } %>
                            </select>
                        </div>

                        <div class="form-group col-md-5 pr-3">
                            <label for="searchPhrase">Search</label>
                            <input type="text"
                                   class="form-control"
                                   id="searchPhrase"
                                   name="searchPhrase"
                                   placeholder="Search"/>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-2 pr-3">
                            <label for="searchBtn">&nbsp;</label>
                            <button class="btn btn-primary float-right" type="button" id="updateSearchBtn" name="searchBtn">
                                search
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table id="updateStockTakeTable"
                                   class="table table-striped table-sm table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Product Name</th>
                                    <th>Quantity In System</th>
                                    <th>Quantity In Store</th>
                                    <th>Variance</th>
                                    <th>Variance reason</th>
                                    <th>Batch number</th>
                                    <th>Expiry Date</th>
                                    <th>Comments</th>
                                </tr>
                                </thead>
                                <tbody>
                                <% simplifiedStockTakeList.each { %>
                                <tr>
                                    <td>${it.itemCode}<input type='text' id='code' name='uuid[]' value='${it.itemStockUuid}' class='hidden'/></td>
                                    <td>${it.itemName}<input type='text' id='stockOperationId' name='stockOperationId' value='${it.id}' class='hidden'/></td>
                                    <td>${it.itemStockQuantity}</td>
                                    <td>${it.enteredQuantityInStore ?: ''}</td>
                                    <td>${it.variance}</td>
                                    <td>${it.varianceReason}</td>
                                    <td>${it.batchNumber}</td>
                                    <td>${it.expiryDate}</td>
                                    <td>${it.comments}</td>
                                </tr>
                                <% } %>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<% } %>
