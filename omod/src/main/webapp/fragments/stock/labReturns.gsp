<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#labReturnsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.search( this.value ).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            table.search( this.value ).draw();
        });
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="input-group col-12 pl-0 pr-0">
            <input type="text"
                   class="form-control py-2"
                   id="searchPhrase"
                   name="searchPhrase"
                   placeholder="Search by name.">
            <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="labReturnsTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date of return</th>
                <th>Officer</th>
                <th>Reason</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
             <% allLabReturns.each { %>
                <tr>
                    <td>${it.id}</td>
                    <td>${it.operationDate}</td>
                    <td>${it.officer}</td>
                    <td>${it.reason}</td>
                    <td>
                     <% if (it.status == "COMPLETED") { %>
                        <i class="fa fa-circle text-success"></i> COMPLETED
                     <% } else if(it.status == "CANCELLED"){ %>
                        <i class="fa fa-circle text-success"></i> CANCELLED
                     <% } else {%>
                        <i class="fa fa-circle text-warning"></i> PENDING
                    <% } %>
                    </td>
                    <td>
                        <a href="/${ui.contextPath()}/botswanaemr/stock/createLabReturns.page?stockOperationId=${it.id}"
                             class="color-primary"> View
                        </a>
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </div>
</div>