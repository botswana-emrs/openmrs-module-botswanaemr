<script type="text/javascript">
    jQuery(function () {
        const table = jQuery('#allProductsTable').DataTable({
            dom: 'rtp',
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: '${ui.actionLink("botswanaemr", "stock/allProducts", "getAllProducts")}',
            columns: [
                { data: 'code' },
                { data: 'name' },
                { data: 'department' },
                { data: 'category' },
                { data: 'unitOfIssue' },
                { data: 'status' },
                null,
            ],
            columnDefs: [
                {
                    targets: -1,
                    data: null,
                    className: "text-left",
                    render: function(data, type, row, meta ) {
                        // console.log(data)
                     return '<a href="/${ui.contextPath()}/botswanaemr/stock/addProducts.page?itemId=' + data.id + '" class="color-primary"> View Item</a> | <br/>' +
                        '<a href="/${ui.contextPath()}/botswanaemr/stock/stockCard.page?itemId=' + data.id + '" class="color-primary"> Bin stock card </a>'
                    },
                },
            ],
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBtn').on('click', () => {
            table.search(jq("#searchPhrase").val()).draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 2000;   // 2 seconds // We want to delay fror 2 seconds while user is typing

            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   table.search(searchText).draw();
                }, delay);
            }
        });
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="input-group col-12 pl-0 pr-0">
            <input type="text"
                   class="form-control py-2"
                   id="searchPhrase"
                   name="searchPhrase"
                   placeholder="Search">
            <span class="input-group-append">
            <button type="submit" class="btn btn-primary" id="searchBtn" name="searchBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
        </div>
    </div>
    <div class="table-responsive">
        <table id="allProductsTable" class="table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th>Code</th>
                <th>Item Name</th>
                <th>Department</th>
                <th>Category</th>
                <th>Unit</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
