<script type="text/javascript">
    jq(document).ready(function () {

    });
    function addReportPermission() {
        jQuery("#addReportPermissionModal").modal('show');
    }

    function editReportPermission(reportId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "reportSetup", "getMailConfig") }&reportId=' + reportId,
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                jq('#reportId').val(data.mailReportId).change()
                jq('#cccList').val(data.cccList);
                jq('#bccList').val(data.bccList);
                jq('#mailSubject').val(data.subject);
                 jq('#mailContentText').val(data.mailContent);
                jQuery("#addReportPermissionModal").modal('show');
            }
        });
    }
</script>

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-head">
                <div class="row">
                    <i class="fa icon-cog"></i>
                    <i><a href="#">${hospitalName} Report Mailing lists</a></i>

                    <div class="col p-r-0">
                        <button type="submit" onclick="addReportPermission()"
                                class="btn icon-plus btn-sm btn-info mb-3 float-right">
                            Add
                        </button>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover data-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Report</th>
                            <th>Email address(es)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% if (mailConfigs) {
                            int count =1
                            mailConfigs.each{%>
                        <tr>
                            <td>${count++}</td>
                            <td>${it.reportName}</td>
                            <td>
                                <span class="label label-success text-white">${it.cccList}</span>
                            </td>
                            <td><a href="javascript:editReportPermission('${it.mailReportId}')">Edit</a></td>
                        </tr>
                        <% }
                        } else { %>
                        <tr>
                            <td colspan="5"> <center><small>No Mailable Report permissions set up.</small></center></td>
                        </tr>
                        <%}%>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>