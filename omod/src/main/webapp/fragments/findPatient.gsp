<%
    def afterSelectedUrl = ""
    def searchContext = '';
    if (isRegistrationPage || isRegistrationPage == "true") {
        afterSelectedUrl = '/botswanaemr/confirmPatient.page?patientId={{ patientId }}&action=' + action + '&returnUrl=' + returnUrl
        searchContext = true;
    } else {
        afterSelectedUrl = '/botswanaemr/patientProfile.page?patientId={{ patientId }}'
        searchContext = false;
    }
%>
<script type="text/javascript">

    function getQueryParam(param) {
        let urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(param);
    }

    function formatBirthdate(dateText) {
        if (!dateText) return '';
        var date = new Date(dateText);
        var day = ("0" + date.getDate()).slice(-2);
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var year = date.getFullYear();
        return day + "/" + month + "/" + year;
    }

    jq(function () {
        let location = getQueryParam('location');
        let resultTemplate = _.template(jq('#result-template').html());
        jq('#patient-search-form').submit(function () {
            let query = jq('#find-patients').val();

            if (${searchContext}) {
                jq.getJSON('${ui.actionLink("botswanaemr", "findPatient", "findPatients")}', {q: query}, function (data) {
                    let resultTarget = jq('#patient-search-results');
                    if (data.results === null || data.results === undefined || data.results.length === 0) {
                        noPatientSearchResultsFound(location);
                    } else {
                        // console.log(data)
                        resultTarget.html('');
                        _.each(data.results, function (patient) {
                            patient.person.birthdate = formatBirthdate(patient.person.birthdate);
                            var url = '/' + OPENMRS_CONTEXT_PATH + emr.applyContextModel('${ ui.escapeJs(afterSelectedUrl) }', {patientId: patient.uuid});

                            let name = patient.person.preferredName.display;
                            let regex = new RegExp(`(\${query})`, "gi");
                            let highlightedName = name.replace(regex, `<span class="highlighted-name">\$1</span>`);
                            setTimeout(() => {
                                let listItem = document.querySelector(`#patient-search-results .patient-search-result[data-patient-uuid="\${patient.uuid}"]`);

                                if (listItem) {
                                    let newSpan = document.createElement("span");
                                    newSpan.className = "preferred-name p-1 text-justify text-capitalize";
                                    newSpan.innerHTML = highlightedName;
                                    listItem.insertBefore(newSpan, listItem.firstChild);
                                }

                            }, 100);
                            resultTarget.append(resultTemplate({patient: patient, url: url}));
                        });

                        // let newRegUrl = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page?action=${action}&returnUrl=${returnUrl}";
                        let hospital = "${hospital}";
                        let regType = getQueryParam('regType');
                        let newRegUrl = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page?action=${action}&returnUrl=${returnUrl}&location=" + encodeURIComponent(hospital)+  "&regType=" + regType;
                        let newRegistrationTemplate = _.template(jq('#new-registration-template').html());
                        resultTarget.append(newRegistrationTemplate({url: newRegUrl}));
                    }
                });
            } else {
                var customRep = 'custom:(uuid,identifiers:(identifierType:(name),identifier),person)';
                jq.getJSON('/ws/rest/v1/patient', {v: customRep, q: query}, function (data) {
                    var resultTarget = jq('#patient-search-results');
                    resultTarget.html('');
                    _.each(data.results, function (patient) {
                        patient.person.birthdate = formatBirthdate(patient.person.birthdate);
                        var url = '/' + OPENMRS_CONTEXT_PATH + emr.applyContextModel('${ ui.escapeJs(afterSelectedUrl) }', {patientId: patient.uuid});
                        resultTarget.append(resultTemplate({patient: patient, url: url}));
                    });
                });
            }
            return false;
        });

        jq('#find-patients').focus();
        jq("input#find-patients").keyup(function () {
            if (jq(this).val() === "") {
                var resultTarget = jq('#patient-search-results');
                resultTarget.html('');
            }
        });
    });

    function noPatientSearchResultsFound(location) {
        var noResultTemplate = _.template(jq('#no-result-template').html());
        var resultTarget2 = jq('#patient-search-results');
        resultTarget2.html('');
        var regisrationUrl = "";
        let action = urlUtils.urlParam('action');
        if (action == 'quick') {
            regisrationUrl = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/quickRegistration.page?returnUrl=${returnUrl}&quick=true";
        } else {
            let regType = getQueryParam('regType');
            regisrationUrl = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/regularRegistration.page?action=${action}&returnUrl=${returnUrl}&location=" + encodeURIComponent(location)+ "&regType=" + regType +"&searchParam=" + jq("#find-patients").val();
        }
        resultTarget2.append(noResultTemplate({url: regisrationUrl}));
    }
</script>

<form method="get" id="patient-search-form">
    <div class="input-group md-form form-sm form-2 pl-0">
        <input type="text"
               class="form-control my-0 py-1"
               id="find-patients"
               placeholder="${ui.message("coreapps.findPatient.search.placeholder")}"
               aria-label="${ui.message("coreapps.findPatient.search.placeholder")}">

        <div class="input-group-append">
            <button class="btn btn-md btn-primary" type="submit">
                <i class="fa fa-search text-white" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</form>

<ul id="patient-search-results" class="list-group"></ul>

<script type="text/template" id="result-template">
<li class="patient-search-result list-group-item d-flex justify-content-between align-items-center"
    data-patient-uuid="{{- patient.uuid }}">
    <span class="birthdate p-1">
        <span class="dob-label">DOB:</span>
        {{- patient.person.birthdate }} ({{- patient.person.age }})
    </span>
    <span class="gender p-1">
        <span class="gender-label">Sex:</span>
        {{- patient.person.gender }}
    </span>
    {{ let openmrsIdShown = false; }}
    {{ let allowedIdentifierTypes = ["OpenMRS ID", "National ID", "Birth Certificate Number", "Passport Number"]; }}

    {{ _.each(patient.identifiers, function(id) { }}
        {{ if (!allowedIdentifierTypes.includes(id.identifierType.name)) { }}
            {{ return; }}
        {{ } }}

        {{ if (id.identifierType.name === "OpenMRS ID") { }}
            {{ if (!openmrsIdShown) { }}
                <span class="patient-identifier p-1">
                    <span class="identifier-type">Patient ID</span>
                    <span class="identifier">{{- id.identifier }}</span>
                </span>
                {{ openmrsIdShown = true; }}
            {{ } }}
        {{ } else { }}
            <span class="patient-identifier p-1">
                <span class="identifier-type">{{- id.identifierType.name }}</span>
                <span class="identifier">{{- id.identifier }}</span>
            </span>
        {{ } }}
    {{ }) }}
    <span class="proceed-link p-1">
        <a class="badge badge-primary badge-pill text-white px-1 py-1 proceed-link" href="{{= url }}" data-identifier="{{- patient.identifiers[0].identifier }}" 
       data-identifier-type="{{- patient.identifiers[0].identifierType.name.replace('OpenMRS ID', 'Patient ID') }}" data-url="{{= url }}">${ui.message("Proceed >>")}</a>
    </span>
</li>
</script>

<script type="text/template" id="no-result-template">
<li class="patient-search-result list-group-item d-flex justify-content-between align-items-center">
    <span>
        Patient not registered.
    </span>
    <a class="badge badge-primary badge-pill text-white px-1 py-1" href="{{= url }}">${ui.message("Register Patient >>")}</a>
</li>
</script>

<script type="text/template" id="new-registration-template">
<li class="patient-search-result list-group-item d-flex justify-content-between align-items-center">
    <span class="text-warning">
        Not the right match found?
    </span>
    <a class="badge badge-warning badge-pill text-white px-1 py-1"
       href="{{= url }}">${ui.message("Create New Record >>")}</a>
</li>
</script>


