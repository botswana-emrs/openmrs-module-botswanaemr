<script>
    jq(document).ready(function () {
        var patientQueueLocationsUuidList = '${patientQueueLocationsUuidList}';

        jq("#reassignPatientForm").submit(function(e){
            e.preventDefault();
            var selectedValue = jq('#queueRoomUuid').val();
            if(selectedValue !== '${patientRegistrationPortal.uuid}'){
                if(patientQueueLocationsUuidList.includes(selectedValue)){
                    jq().toastmessage('showErrorToast', "Patient has already been queued to this location!");
                } else {
                    var params = {
                        'patientId': jq('#patientId').val(),
                        'queueRoomUuid': jq('#queueRoomUuid').val(),
                        'patientQueueId': jq('#patientQueueId').val(),
                        'locationUuid': jq('#locationUuid').val()
                    };

                    jq.getJSON('${ui.actionLink('botswanaemr','reassignPatientToQueue','reassignPatientToQueueLocation')}', params)
                        .done(function (data) {
                            jq().toastmessage('showNoticeToast', "Patient reassigned to pool successfully");
                            history.back();
                        }).fail(function() {
                            jq().toastmessage('showErrorToast', "An error occurred while assigning the patient to the designated pool");
                        });

                        jQuery('#confirmPatientAssignmentModal').modal('show');
                        jq('#flag').text(jq('#queueRoom').find(":selected").text());
                        jq('#queuePatientSection').hide();
                }
            } else {
                if(patientQueueLocationsUuidList.includes(selectedValue)){
                    jq('#queuePatientSection').hide();
                    jq().toastmessage('showErrorToast', "Patient has already been queued to this location!");
                } else {
                    jq('#queuePatientSection').show();
                }
            }
        });
    });
</script>

<!-- Reassign Patient Modal -->
<div class="modal fade pt-3 mt-3 pl-4" id="reassignPatientModal" tabindex="-1"
     data-controls-modal="reassignPatientModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="reassignPatientModalTitle">

    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="reassignPatientModalTitle">Reassign Patient</h4>
                <button type="button" onclick="history.back()" id="editPatientDetails" class="btn btn-sm btn-primary">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="reassignPatientForm" method="post">
                    <div>
                        <div class="form-group content-section">
                            <label for="queueRoomUuid">Reassign patient to
                                <span class="text-danger">*</span>
                            </label>
                            <input type="hidden" id="locationUuid" name="locationUuid" value="${currentServicePoint}">
                            <input type="hidden" id="patientId" name="patientId" value="${patient.id}">
                            <input type="hidden" id="patientQueueId" name="patientQueueId" value="${patientQueue.id}">
                            <select class="custom-select" id="queueRoomUuid" name="queueRoomUuid" required>
                                <option selected required value="" >
                                    Select location to reassign the patient to:
                                </option>
                                <% if (queueLocations != null) {
                                queueLocations.grep{it.uuid != currentServicePoint}.each { %>
                                <option value="${it.uuid}">${it.name}</option>
                                <% }
                                }%>
                            </select>
                        </div>
                        <div class="row">
                            <button id="reassignPatient" type="submit" class=" btn btn-primary btn-block">${ui.message("Reassign Patient")}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
         jQuery("#reassignPatientModal").appendTo("body");
    });
</script>
