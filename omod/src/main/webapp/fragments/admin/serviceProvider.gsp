<script type="text/javascript">
    jQuery(function () {
        jQuery('#serviceProvidertbl').DataTable({
            dom: 'rtp',
            searching: false,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jQuery("#editServiceProviderModal").appendTo("body");
    });

    function selectProvider(providerId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "admin/provider/editServiceProvider", "getServiceProvider") }&providerId='+providerId,
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                let modalBody = jq("div#editServiceProviderModal").find(".modal-body");
                modalBody.find("#editDescription").val(data.description);
                modalBody.find("#editProviderName").val(data.name);
                modalBody.find("#editProviderId").val(data.id);

                jQuery("#editServiceProviderModal").modal('show');
                console.log(data);
            }
        });
    }

    jq("#serviceProvidertbl").on('click', '#deleteBtn', function (e) {
        jQuery("#deleteServiceProviderModal").modal('hide');
    });

    jq("#removeServiceProviderBtn").on("click", function(confirm) {
        jQuery("deleteServiceProviderModal").modal('show');
    });

    jq("#closeModalBtn").on("click", function() {
        jQuery("#deleteServiceProviderModal").modal('hide');
    });
</script>

<style type="text/css">
.modal-backdrop {
    z-index: -1;
}
</style>

<div id="serviceProvider" class="table-responsive">
    <table id="serviceProvidertbl" class="table table-sm table-bordered table-striped">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Date Created</th>
            <th>Created by</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% serviceProvider.each { %>
        <tr>
            <td>${it.id}</td>
            <td>${it.name}</td>
            <td>${it.dateCreated}</td>
            <td>${it.creator}</td>
            <% if (it.name == none) { %>
            <td>No Action</td>
            <% } else { %>
            <td><a class="text-primary" href="javascript:selectProvider(${it.id})">Edit</a>&nbsp;|
                <a type="button"
                   class="text-primary"
                   id="removeServiceProviderBtn${it.id}"
                   data-toggle="modal"
                   data-target="#deleteServiceProviderModal${it.id}"> Delete
                </a>

                <!-- Modal -->
                <div class="modal fade"
                     id="deleteServiceProviderModal${it.id}"
                     tabindex="-1"
                     data-controls-modal="deleteServiceProviderModal${it.id}"
                     data-backdrop="static"
                     data-keyboard="false"
                     role="dialog"
                     aria-hidden="true"
                     aria-labelledby="deleteServiceProviderModalLabel">

                    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <h5 class="text-white text-center modal-title" id="deleteServiceProviderModalLabel"> Remove</h5>
                                <button type="button" id="closeModal" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-muted text-center">
                                    Are you sure you want to remove this record? All fields for this record will be cleared!
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button"
                                        id="closeModalBtn"
                                        class="btn btn-dark bg-dark float-right"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <form method="post" action="${ui.actionLink('botswanaemr', 'admin/provider/editServiceProvider', 'voidServiceProvider')}">
                                    <input id="providerId" type="hidden" name="providerId" value="${it.id}" />
                                    <button type="submit" id="deleteBtn" class="btn btn-primary text-white mt-3">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <% } %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>

<div id="addRecord">
    <form id="serviceProviderForm" method="post" action="${ui.actionLink('registerServiceProvider')}">
        <div class="form-group">
            <label for="provider">Service Provider:
                <span class="text-danger">*</span>
            </label>
            <input type="text" id="provider" class="form-control" name="provider" placeholder="Enter service provider's name"
                   required/>
        </div>

        <div class="form-group">
            <label for="description">Description
                <span class="text-danger">*</span>
            </label>
            <textarea class="form-control" name="description" id="description" rows="5" required></textarea>
        </div>

        <div class="row">
            <div class="col pr-0">
                <button id="saveServiceProvider"
                        class="btn btn-sm btn-primary float-right ml-1"
                        type="submit"> ${ui.message("Save")}
                </button>
                <button class="btn btn-sm btn-dark bg-dark float-right"
                        onclick="history.back()"> ${ui.message("Close")}
                </button>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="editServiceProviderModal" tabindex="-1"
     data-controls-modal="editServiceProviderModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editServiceProviderModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editServiceProviderModalTitle">Edit Service Provider</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr","admin/provider/editServiceProvider",[providerId:0])}
                </div>
            </div>
        </div>
    </div>
</div>
