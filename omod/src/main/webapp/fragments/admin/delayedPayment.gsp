<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchInput').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchInput').val() || name.toLowerCase().includes(jq('#searchInput').val().toLowerCase())
                );
            }

            if (matchingOptions.length === 0) {
                return  true;
            }

            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );
    
    jQuery(function () {
        var delayedTable = jQuery('#delayedPaymentsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#findBtn').on('click', () => {
            delayedTable.draw();
        });

        jq('#undoBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#pills-delayed-payments'));
            delayedTable.draw();
        });

        jq('#searchInput').on( 'keyup', function () {
            let timeout;
            let delay = 1000;
            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   delayedTable.draw();
                }, delay);
            }
        });
    });

    function selectPaymentTransaction(registrationId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "admin/paymentHistory", "getTransaction") }&registrationId=' + registrationId,
            dataType: "json",
            global: false,
            async: true,
            success: function (data) {
                jq('#registrationId').val(data.registrationId);
                jq('#billableAmount').val(data.billableAmount);
                jq('#amountPaid').val(data.amountPaid);
                jQuery("#updateDelayedPaymentModal").modal('show');
            }
        });
    }
</script>

<div class="row">
    <div class="input-group col-10 pl-0 pr-0 py-4">
        <input type="text"
               value=""
               class="form-control"
               id="searchInput"
               name="searchInput"
               placeholder="Search by Name or ID">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="findBtn" name="findBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
    <div class="col-md-2 pr-0">
        <button id="undoBtn"
                type="reset"
                class="btn btn-md btn-dark bg-dark mt-3 py-2 float-right">${ui.message("Reset")}
        </button>
    </div>
</div>

<div class="table-responsive">
    <table id="delayedPaymentsTable" class="table table-sm table-bordered">
        <thead>
        <tr>
            <th>Identifier</th>
            <th>Patient Name</th>
            <th>Billable Amount</th>
            <th>Amount Paid</th>
            <th>Balance</th>
            <th>Transaction Date</th>
            <th>Created By</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody id="paymentsTbody">
        <% if (allDelayedPayments.empty) { %>
        <tr>
            <td colspan="7">
                No records found
            </td>
        </tr>
        <% } %>
        <% allDelayedPayments.each { %>
        <tr id= ${it.registrationId}>
            <td>${it.identifier}</td>
            <td>${it.patientName}</td>
            <td>${it.billableAmount}</td>
            <td>${it.amountPaid}</td>
            <td>${it.balance}</td>
            <td>${it.transactionDate}</td>
            <td>${it.createdBy}</td>
            <td><a class="text-primary" href="javascript:selectPaymentTransaction(${it.registrationId})">Update</a></td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
