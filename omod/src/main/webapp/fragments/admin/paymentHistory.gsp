<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchBox').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchBox').val() || name.toLowerCase().includes(jq('#searchBox').val().toLowerCase())
                );
            }

            if (matchingOptions.length === 0) {
                return  true;
            }

            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            
            return matchingStatus;
        }
    );

    jQuery(function () {
        jQuery('#updateDelayedPaymentModal').appendTo('Body');
        var paymentTable = jQuery('#paymentHistoryTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#searchBoxBtn').on('click', () => {
            paymentTable.draw();
        });

        jq('#clearBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#pills-payment-history'));
            paymentTable.draw();
        });

        jq('#searchBox').on( 'keyup', function () {
            let timeout;
            let delay = 1000;
            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   paymentTable.draw();
                }, delay);
            }
        });
    });

    function displayVoidPaymentModal(registrationId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "admin/paymentHistory", "getTransaction") }&registrationId=' + registrationId,
            dataType: "json",
            global: false,
            async: true,
            success: function (data) {
                // console.log(data)
                jq('#vPaymentMethod').val(data.paymentMethod);
                jq('#vRegistrationId').val(data.registrationId);
                jq('#vBillableAmount').val(data.billableAmount);
                jq('#vAmountPaid').val(data.amountPaid);
                jq('#vPaymentDate').val(data.registrationDatetime);
                jQuery('#voidPaymentModal').modal('show');
            }
        });
    }

    function selectPaymentTransaction(registrationId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "admin/paymentHistory", "getTransaction") }&registrationId=' + registrationId,
            dataType: "json",
            global: false,
            async: true,
            success: function (data) {
                jq('#registrationId').val(data.registrationId);
                jq('#billableAmount').val(data.billableAmount);
                jq('#amountPaid').val(data.amountPaid);
                jQuery('#updateDelayedPaymentModal').modal('show');
            }
        });
    }
</script>
<div class="row">
    <div class="input-group col-10 pl-0 pr-0 py-4">
        <input type="text"
               value=""
               class="form-control"
               id="searchBox"
               name="searchBox"
               placeholder="Search by Name or ID">
        <span class="input-group-append">
            <button class="btn btn-primary" type="button" id="searchBoxBtn" name="searchBoxBtn">
                <i class="fa fa-search"></i> search
            </button>
        </span>
    </div>
    <div class="col-md-2 pr-0">
        <button id="clearBtn"
                type="reset"
                class="btn btn-md btn-dark bg-dark mt-3 py-2 float-right">${ui.message("Reset")}
        </button>
    </div>
</div>

<div class="table-responsive">
    <table id="paymentHistoryTable" class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>Patient Id</th>
                <th>Patient Name</th>
                <th>Billable Amount</th>
                <th>Amount Paid</th>
                <th>Balance</th>
                <th>Transaction Date</th>
                <th>Created By</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        <% paymentHistory.each { %>
            <tr>
                <td>${it.identifier}</td>
                <td>${it.patientName}</td>
                <td>${it.billableAmount}</td>
                <td>${it.amountPaid}</td>
                <td>${it.balance}</td>
                <td>${it.transactionDate}</td>
                <td>${it.createdBy}</td>
                <td><a class="text-primary" href="javascript:selectPaymentTransaction(${it.registrationId})">Update</a> | <a class="text-primary" href="javascript:displayVoidPaymentModal(${it.registrationId})">Void</a></td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>
<div class="modal fade" id="voidPaymentModal" tabindex="-1"
     data-controls-modal="voidPaymentModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="voidPaymentModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="voidPaymentModalTitle">Void Payment</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "admin/voidPayment")}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateDelayedPaymentModal" tabindex="-1"
     data-controls-modal="updateDelayedPaymentModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="updateDelayedPaymentModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="updateDelayedPaymentModalTitle">Update delayed payment</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "admin/updateDelayedPayment")}
                </div>
            </div>
        </div>
    </div>
</div>
