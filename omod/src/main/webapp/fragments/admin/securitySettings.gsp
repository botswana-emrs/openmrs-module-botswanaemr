<script type="text/javascript">

    jQuery(function () {
    \$("form#newSecurityPhoneNumberForm").validate();
        jq("form#newSecurityPhoneNumberForm").submit(function (e) {
            e.preventDefault();
            if (\$(e.target).valid())
            {
                jq.getJSON('${ ui.actionLink("botswanaemr", "admin/securitySettings", "updateSecurityPhoneNumber")}',
                    {
                        'newSecurityPhoneNumber': jq('#newSecurityPhoneNumber').val()
                    })
                    .success(function (data) {
                        jQuery("#modifySecurityPhoneNumberModal").modal('hide');
                        jq().toastmessage('showNoticeToast', "Phone number Updated");
                        location.reload();
                    })
                    .fail(function (err) {
                            console.log(err)
                        }
                    )
            }
        });

        \$("form#changeBackupEmailForm").validate();
        jq("form#changeBackupEmailForm").submit(function (e) {
            e.preventDefault();
            if (\$(e.target).valid())
            {
                jq.getJSON('${ ui.actionLink("botswanaemr", "admin/securitySettings", "updateBackupEmail")}',
                    {
                        'newBackupEmail': jq('#newBackupEmail').val()
                    })
                    .success(function (data) {
                        jQuery("#changeBackupEmailModal").modal('hide');
                        jq().toastmessage('showNoticeToast', "Email Updated");
                        location.reload();
                    })
                    .fail(function (err) {
                            console.log(err)
                        }
                    )
            }
        });

        \$("form#modifyPasswordForm").validate();
        jq("form#modifyPasswordForm").submit(function (e) {
            e.preventDefault();

            let form = \$(e.target);
            if (form.valid())
            {
                let data = {newPassword: jq("#newPassword"), oldPassword: jq("#oldPassword")};
 //               jQuery.post('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/password/changeOwnPassword', form.serialize(), function (data) {
                   jQuery.post(form.attr('action'), form.serialize(), function(result) {
                       if (result.status === 200 || result.status.toString().indexOf('200') > -1) {
                           jQuery("#changeBackupEmailModal").modal('hide');
                           jq().toastmessage('showNoticeToast', "Password Changed successfully");
                           location.reload();
                       } else {
                           jq().toastmessage('showErrorToast',  result.response);
                       }
                    },'json')
                    .fail(function (err) {
                        console.log(err)
                        jq().toastmessage('showErrorToast', + err);
                    })
            }
        });

    });

</script>

<div>

    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-top-0 border-left-0">
            <div>
                <label>Account Password</label>

                <p>Current Password strength: strong</p>
            </div>

            <div>
                <a id="modifyPassword" class="color-primary" data-toggle="modal"
                   data-target="#modifyPasswordModal">Modify</a>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Security Phone</label>

                <p>Bound Phone: ${user.getPerson().getAttribute('USER-Security-phone-number') ?: 'No security phone No. provided'}</p>
            </div>

            <div>
                <a id="modifyPhoneNo" class="color-primary" data-toggle="modal"
                   data-target="#modifySecurityPhoneNumberModal">Modify</a>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Backup Email</label>

                <p>Bound Email: : ${user.getPerson().getAttribute('USER-Backup-email') ?: 'No backup email provided'}</p>
            </div>

            <div>
                <a id="backupEmail" class="color-primary" data-toggle="modal"
                   data-target="#changeBackupEmailModal">Backup Email</a>
            </div>
        </li>
    </ul>

</div>

<div class="modal fade" id="modifyPasswordModal" tabindex="-1"
     data-controls-modal="modifyPasswordModal" data-backdrop="false"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="modifyPasswordModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="modifyPasswordModalTitle">Modify password</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    <form id="modifyPasswordForm"
                          action="${ui.actionLink("botswanaemr", "admin/securitySettings", "changePassword")}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="oldPassword">Enter old password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="password" class="form-control"
                                           id="oldPassword" required name="oldPassword" value="" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="newPassword">Enter new password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="password" class="form-control"
                                           id="newPassword" required name="newPassword" value="" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="confirmPassword">Confirm new password
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="password" class="form-control"
                                           id="confirmPassword" required name="confirmPassword" value="" data-rule-password="true" data-rule-equalTo="#newPassword" required>
                                </div>
                            </div>
                        </div>

                        <div class="actions mt-5">
                            <button class="btn btn-dark bg-dark float-right ml-1" data-dismiss="modal">
                                ${ui.message("Close")}
                            </button>
                            <button id="savePassword" class="btn btn-primary float-right"
                                    type="submit">
                                ${ui.message("Save")}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modifySecurityPhoneNumberModal" tabindex="-1"
     data-controls-modal="modifySecurityPhoneNumberModal" data-backdrop="false"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="modifySecurityPhoneNumberModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="modifySecurityPhoneNumberModalTitle">Modify Security phone</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    <form id="newSecurityPhoneNumberForm"
                          action="${ui.actionLink("botswanaemr", "admin/securitySettings", "updateSecurityPhoneNumber")}">
                        <div class="form-group">
                            <label for="newSecurityPhoneNumber">Enter new security phone Number
                                <span class="text-danger">*</span>
                            </label>
                            <input type="number" minlength="7" maxlength="8" class="form-control"
                                   id="newSecurityPhoneNumber" required name="newSecurityPhoneNumber" value="${user.getPerson().getAttribute('USER-Security-phone-number')}">
                        </div>

                        <div class="actions mt-5">
                            <button class="btn btn-dark bg-dark float-right ml-1" data-dismiss="modal">
                                ${ui.message("Close")}
                            </button>
                            <button id="saveNewSecurityNumber" class="btn btn-primary float-right ml-1"
                                    type="submit">
                                ${ui.message("Save")}
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="changeBackupEmailModal" tabindex="-1"
     data-controls-modal="changeBackupEmailModal" data-backdrop="false"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="changeBackupEmailModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="changeBackupEmailModalTitle">Modify backup email</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    <form id="changeBackupEmailForm"
                          action="${ui.actionLink("botswanaemr", "admin/securitySettings", "updateBackupEmail")}">
                        <div class="form-group">
                            <label for="newBackupEmail">Enter your backup email
                                <span class="text-danger">*</span>
                            </label>
                            <input type="email" class="form-control" email required
                                   id="newBackupEmail" name="newBackupEmail" value="${user.getPerson().getAttribute('USER-Backup-email')}">
                        </div>

                        <div class="actions mt-5">
                            <button class="btn btn-dark bg-dark float-right ml-1" data-dismiss="modal">
                                ${ui.message("Close")}
                            </button>
                            <button id="saveBackupEmail" class="btn btn-primary float-right ml-1"
                                    type="submit">
                                ${ui.message("Save")}
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

