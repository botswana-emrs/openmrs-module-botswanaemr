<script type="text/javascript">
    jQuery(function () {
        jQuery("#paymentDate").attr("max",new Date().toISOString().split("T")[0]);

        jQuery("form#updatePaymentForm").submit(function (e) {
            e.preventDefault();
            jq.getJSON('${ ui.actionLink("botswanaemr", "admin/delayedPayment", "updatePayment")}',
                {
                    'registrationId': jq('#registrationId').val(),
                    'paymentMethodId': jq('#paymentMethod').val(),
                    'paymentDate': jq('#paymentDate').val(),
                    'amountPaid': jq('#amountPaid').val()
                })
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Transaction updated");
                    jQuery("#updateDelayedPaymentModal").modal('hide');
                    location.reload();
                })
                .fail(function (err) {
                        console.log(err)
                    }
                )
        });
    });

</script>

<form id="updatePaymentForm" method="post"
      action="${ui.actionLink("botswanaemr", "delayedPayment", "updatePayment")}">
    <div class="form-group">
        <label for="paymentMethod">Payment Method
            <span class="text-danger">*</span>
        </label>
        <select name="paymentMethod" id="paymentMethod" class="form-control" required>
            <option value="">Select method</option>
            <% paymentMethods.each { %>
            <option value="${it.getId()}">
                ${it.getName()}
            </option>
            <% } %>
        </select>

    </div>

    <div class="form-group">
        <label for="billableAmount">Billable amount
            <span class="text-danger">*</span>
        </label>
        <input class="form-control" name="billableAmount" id="billableAmount" readonly>
    </div>

    <div class="form-group">
        <label for="amountPaid">Enter Amount paid
            <span class="text-danger">*</span>
        </label>
        <input type="number" class="form-control" id="amountPaid" required name="amountPaid">
    </div>

    <div class="form-group">
        <label for="paymentDate">Payment Date
            <span class="text-danger">*</span>
        </label>
        <input type="date" class="form-control" id="paymentDate"
               name="paymentDate"
               required placeholder="Date payment done">
    </div>

    <div class="row">
        <input type="hidden" name="registrationId" id="registrationId"/>
    </div>

    <div class="row">
        <div class="col pr-0">
            <button id="savePayment"
                    class="btn btn-sm btn-primary float-right ml-1"
                    type="submit"> ${ui.message("Save")}
            </button>
            <button class="btn btn-sm btn-dark bg-dark float-right" data-dismiss="modal">
                ${ui.message("Close")}
            </button>
        </div>
    </div>
</form>
