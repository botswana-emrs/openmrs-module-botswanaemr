<script type="text/javascript">
    jQuery(function () {
        jQuery("form#editServiceProviderForm").submit(function (e) {
            e.preventDefault();
            let serviceProviderForm = jQuery(this);
            let params = {
                editProviderId: serviceProviderForm.find("#editProviderId").val(),
                editDescription: serviceProviderForm.find("#editDescription").val(),
                editProviderName: serviceProviderForm.find("#editProviderName").val()
            }
            jq.getJSON('${ ui.actionLink("botswanaemr", "admin/provider/editServiceProvider", "editServiceProvider")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The Service Provider has been updated");
                    jQuery("#editServiceProviderModal").modal('hide');
                    location.reload();
                })
        });
    });

</script>
<form id="editServiceProviderForm" method="post" action="${ ui.actionLink('botswanaemr', 'editServiceProvider','editServiceProvider')}">
    <div class="form-group">
        <label for="editProviderName"> Service Provider:
            <span class="text-danger">*</span>
        </label>
        <input type="hidden" id="editProviderId" class="form-control" name="editProviderId" value="${provider.uuid}" required/>
        <input type="text" id="editProviderName" class="form-control" name="editProviderName" placeholder="Enter service provider's name" value="${provider.name}" required/>
    </div>
    <div class="form-group">
        <label for="editDescription"> Description
            <span class="text-danger">*</span>
        </label>
        <textarea class="form-control" name="editDescription" id="editDescription" rows="5" required>${provider.description}</textarea>
    </div>

    <div class="row">
        <div class="col pr-0">
            <button id="saveServiceProvider"
                    class="btn btn-sm btn-primary float-right ml-1"
                    type="submit">${ui.message("Save")}
            </button>
            <button type="button"
                    class="btn btn-sm btn-dark bg-dark float-right"
                    data-dismiss="modal">Close
            </button>
        </div>
    </div>
</form>
