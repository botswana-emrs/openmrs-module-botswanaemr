<script type="text/javascript">
    jQuery(function () {
        jQuery('#paymentMethodtbl').DataTable({
            dom: 'rtp',
            searching: false,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jQuery("#editPaymentMethodModal").appendTo("body");
    });

    function selectPaymentMethod(paymentMethodId) {
        jq.ajax({
            type: "GET",
            url: '${ ui.actionLink("botswanaemr", "admin/paymentMethod/editPaymentMethod", "getPaymentMethod") }&paymentMethodId='+paymentMethodId,
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                let modalBody = jq("div#editPaymentMethodModal").find(".modal-body");
                modalBody.find("#editDescription").val(data.description);
                modalBody.find("#editPaymentMethodName").val(data.name);
                modalBody.find("#editPaymentMethodId").val(data.id);

                jQuery("#editPaymentMethodModal").modal('show');
                console.log(data);
            }
        });
    }

    jq("#paymentMethodtbl").on('click', '#deleteBtn', function (e) {
        jQuery("#deleteModal").modal('hide');
    });

    jq("#removeBtn").on("click", function(confirm) {
        jQuery("deleteModal").modal('show');
    });

    jq("#closeBtn").on("click", function() {
        jQuery("#deleteModal").modal('hide');
    });
</script>

<style type="text/css">
.modal-backdrop {
    z-index: -1;
}
</style>

<div id="paymentMethods" class="table-responsive">
    <table id="paymentMethodtbl" class="table table-sm table-bordered table-striped">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Date Created</th>
            <th>Created by</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% paymentMethods.each { %>
        <tr>
            <td>${it.id}</td>
            <td>${it.name}</td>
            <td>${it.dateCreated}</td>
            <td>${it.creator}</td>
            <% if (it.name == none) { %>
            <td>No Action</td>
            <%  } else { %>
            <td><a class="text-primary" href="javascript:selectPaymentMethod(${it.id})">Edit</a>&nbsp;|
                <a  type="button"
                    class="text-primary"
                    id="removeBtn"
                    data-toggle="modal"
                    data-target="#deleteModal${it.id}"> Delete
                </a>

                <!-- Delete Payment Modal -->
                <div class="modal fade"
                     id="deleteModal${it.id}"
                     tabindex="-1"
                     data-controls-modal="deleteModal${it.id}"
                     data-backdrop="static"
                     data-keyboard="false"
                     role="dialog" 
                     aria-hidden="true"
                     aria-labelledby="deleteModal${it.id}Title">

                    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <h5 class="text-white text-center modal-title" id="deleteModalLabel"> Remove</h5>
                                <button type="button" id="closeModal" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p class="text-muted text-center">
                                    Are you sure you want to remove this record? All fields for this record will be cleared!
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button"
                                        id="closeBtn"
                                        class="btn btn-dark bg-dark"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <form method="post" action="${ui.actionLink('botswanaemr', 'admin/paymentMethod/editPaymentMethod', 'voidPaymentMethod')}">
                                    <input id="paymentMethodId" type="hidden" name="paymentMethodId" value="${it.id}" />
                                    <button type="submit" id="deleteBtn" class="btn btn-primary text-white mt-3">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
            <% } %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>

<div id="addRecord">
    <form id="paymentMethodForm" method="post" action="${ui.actionLink('registerPaymentMethod')}">
        <div class="form-group">
            <label for="methodName">Payment Method:
                <span class="text-danger">*</span>
            </label>
            <input type="text" id="methodName" name="methodName" class="form-control"
                   placeholder="Enter payment method's name" required/>
        </div>

        <div class="form-group">
            <label for="description">Description:
                <span class="text-danger">*</span>
            </label>
            <textarea class="form-control" name="description" id="description" rows="5" required></textarea>
        </div>

        <div class="row">
            <div class="col pr-0">
                <button id="savePaymentMethod"
                        class="btn btn-sm btn-primary float-right ml-1"
                        type="submit"> ${ui.message("Save")}
                </button>
                <button class="btn btn-sm btn-dark bg-dark float-right"
                        onclick="history.back()"> ${ui.message("Close")}
                </button>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="editPaymentMethodModal" tabindex="-1"
     data-controls-modal="editPaymentMethodModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editPaymentMethodModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editPaymentMethodModalTitle">Edit Payment Method</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "admin/paymentMethod/editPaymentMethod", [paymentMethodId: 0])}
                </div>
            </div>
        </div>
    </div>
</div>
