<script type="text/javascript">
    var  userChoices;
    jQuery(document).ready(function () {
        jq('.table, .th, .tr ').css("border", "0");
        jQuery('#facilitiesTable').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        userChoices = new Choices('#facilityAdmins', {
            removeItemButton: true,
            maxItemCount: 4,
            searchResultLimit: 5,
            renderChoiceLimit: 5,
        });
    });


    function setFacilityAdmins(facilityLocationUuid,facilityLocationMFlCode,isBdf) {
        jq("#editAccountModal")
            .find("input,textarea,select,multiple")
            .val('')
            .end();
        userChoices.unhighlightAll();
        jQuery("#facilityAdminSettingModal").modal('show');
        jq('#facilityAdminUserSearchForm').attr('action', '${ ui.actionLink('botswanaemr', 'admin/facilities','post')}');
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","admin/facilities","getFacilityUsers")}',
            dataType: "json",
            global: false,
            async: true,
            data: {
                facilityLocationUuid: facilityLocationUuid,
                facilityLocationMFlCode:facilityLocationMFlCode,
                isBdf: jq('#isBdf').is(":checked").toString()
            },
            success: function (data) {
                userChoices.clearStore();
                userChoices.setChoices(
                    data.userObjects,
                    'value',
                    'label',
                    true,
                );
                jq("#facilityName").text('');
                jq("#facilityCode").text('');
                jq("#facilityName").text(data.facilityName)
                jq("#facilityCode").text(data.newFacilityCode);
                jq("#facilityMflCode").val(data.newFacilityCode);
                jq("#facilityLocationUuid").val(data.facilityLocationUuid);
                jq("#isBdf").prop('checked', isBdf === 'true');
            }
        });

    }

     function deleteFacility(facilityLocationUuid, facilityName) {
         jQuery("#deleteFacilityModal").modal('show');
         jq('#deleteFacilityModalTitle').text('Delete Facility: ' + facilityName);
         jq("#deleteFacility").click(function(e) {
             jq.ajax({
                 type: "GET",
                 url: '${ui.actionLink("botswanaemr","admin/facilities","retireFacility")}',
                 dataType: "json",
                 global: false,
                 async: false,
                 data: {
                     facilityLocationUuid: facilityLocationUuid,
                     retireReason: jq("#retireReason").val()
                 },
                 success: function (data) {
                    jq('#reasonfacilityForm').attr('action', '${ ui.actionLink('botswanaemr', 'admin/facilities','post')}');
                 }
             });
         });
     }
</script>

<div>
    <div class="table-responsive">
        <table id="facilitiesTable"
               class="table table-sm " style="width:100%">
            <thead>
            <tr>
                <th>Facility</th>
                <th>Status</th>
                <th>User role</th>
                <th>BDF Facility?</th>
                <th>Registered on</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="facilitiesTblBody">
            <% parentLocationsz.grep { !it.facilityName.equals("Unknown Location") }.each { %>
            <tr>
                <td>${it.facilityName}</td>
                <td><i class="status active"></i>active</td>
                <td>${it.country}</td>
                <td>${it.isBdf ? 'Yes' : 'No'}</td>
                <td>${ui.format(it.dateCreated)}</td>
                <td>
                <a href="javascript:setFacilityAdmins('${it.id}','${it.facilityMfl}','${it.isBdf}')"><i class="icon-pencil edit-action"></i>&nbsp;</a>
                <a href="javascript:deleteFacility('${it.id}', '${it.facilityName}')"><i class="icon-trash delete-action" style="color: red;"></i>&nbsp;</a>
                </td>
            </tr>
            <% } %>
            </tbody>
        </table>

    </div>
</div>

<div class="modal fade" id="facilityAdminSettingModal" tabindex="-1"
     data-controls-modal="facilityAdminSettingModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="facilityAdminSettingModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <i class="modal-title text-white" id="facilityAdminSettingModalTitle">Manage facility Admins</i>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form class="simple-form-ui" id="facilityAdminUserSearchForm" >
                    <div class="col">
                        <fieldset class="w-100 p-5">
                            <legend class="w-auto"><small>Facility Details</small></legend>

                            <div class="row">
                                <div class="col-auto">
                                    <label for="facilityName">Name :</label>
                                    <label for="facilityCode">MFL Code :</label>
                                </div>

                                <div class="col-auto">
                                    <label id="facilityName">Shila Vela</label>
                                    <label id="facilityCode">3030-1</label>
                                    <input id="facilityMflCode" name ="facilityMflCode" style="display: none">
                                    <input id="facilityLocationUuid" name ="facilityLocationUuid" style="display: none">
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="col mt-2 mb-2">
                        <div class="form-group">
                            <input class="form-check-input" name="isBdf" type="checkbox" id="isBdf">
                            <label class="form-check-label" for="isBdf"
                                   style="margin-top: auto">
                                BDF Facility?
                            </label>
                        </div>
                    </div>

                    <div class="col">
                        <fieldset class="w-100 p-5">
                            <legend class="w-auto"><b><small>${ui.message("Administrators")}</small></b></legend>

                            <div class="form-group">
                                <label class="form-label"
                                       for="facilityAdmins">${ui.message("Admin users")} <span
                                        class="text-danger">*</span></label>
                                <select id="facilityAdmins" placeholder="Search by Name or email" name="facilityAdmins"
                                        class="form-control custom-select" multiple>
                                    <% users.each { user -> %>
                                    <option value="${user.uuid}">${user.getNames().join()}   ${user.getEmail() ? user.getEmail() : ""}</option>
                                    <% } %>
                                </select>
                            </div>

                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                            ${ui.message("Close")}
                        </button>
                        <button id="saveFacilityAdmins" type="submit"
                                class="btn btn-sm btn-primary float-right ml-1">${ui.message("Save")}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteFacilityModal" tabindex="-1"
     data-controls-modal="deleteFacilityModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="deleteFacilityModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <i class="modal-title text-white" id="deleteFacilityModalTitle">Delete Facility: </i>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form class="simple-form-ui" id="reasonfacilityForm" >
                    <div class="col">
                        <fieldset class="w-100 p-5 mt-4">
                            <legend class="w-auto"><b><small>${ui.message("Enter reason for deleting")}</small></b></legend>
                            <div class="form-group">
                                <label class="form-label"
                                       for="retireReason">${ui.message("Reason")} <span
                                        class="text-danger">*</span></label>
                               <input type="text" class="form-control reason" id="retireReason" name="retireReason"
                                        required placeholder="Enter Reason">
                            </div>

                        </fieldset>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                            ${ui.message("Cancel")}
                        </button>
                        <button id="deleteFacility" type="submit"
                                class="btn btn-sm btn-danger float-right ml-1">${ui.message("Delete")}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
