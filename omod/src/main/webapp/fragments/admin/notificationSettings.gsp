<script type="text/javascript">
    jq(function () {
        //update the switches with previously saved user preferences
        jq('#newPatientsInPoolSwitch').attr('checked', ${notifyNewPatientsInPool});
        jq('#patientSelectedSwitch').attr('checked', ${notifyPatientSelected});
        jq('#consultationEditsSwitch').attr('checked', ${notifyConsultationEdits});
        jq('#labResultsSwitch').attr('checked', ${notifyLabResults});
        jq('#successfulAdmissionsSwitch').attr('checked', ${notifySuccessfulAdmissions});


        jq('#newPatientsInPoolSwitch').click(function () {
            var notifyNewPatientsInPool = "false"
            if (jq(this).is(':checked')) {
                notifyNewPatientsInPool = "true"
            }
            jq.getJSON('${ui.actionLink("botswanaemr", "admin/notificationSettings", "notifyNewPatientsInPoolSetting")}',
                {'notifyNewPatientsInPool': notifyNewPatientsInPool}
            ).success(function (data) {
                console.log(data);
            })
        });
        jq('#patientSelectedSwitch').click(function () {
            var notifyPatientSelected = "false"
            if (jq(this).is(':checked')) {
                notifyPatientSelected = "true"
            }
            jq.getJSON('${ui.actionLink("botswanaemr", "admin/notificationSettings", "notifyPatientSelectedSetting")}',
                {'notifyPatientSelected': notifyPatientSelected}
            ).success(function (data) {
                console.log(data);
            })
        });
        jq('#consultationEditsSwitch').click(function () {
            var notifyConsultationEdits = "false"
            if (jq(this).is(':checked')) {
                notifyConsultationEdits = "true"
            }
            jq.getJSON('${ui.actionLink("botswanaemr", "admin/notificationSettings", "notifyConsultationEditsSetting")}',
                {'notifyConsultationEdits': notifyConsultationEdits}
            ).success(function (data) {
                console.log(data);
            })
        });

        jq('#labResultsSwitch').click(function () {
            var notifyLabResults = "false"
            if (jq(this).is(':checked')) {
                notifyLabResults = "true"
            }
            jq.getJSON('${ui.actionLink("botswanaemr", "admin/notificationSettings", "notifyLabResultsSetting")}',
                {'notifyLabResults': notifyLabResults}
            ).success(function (data) {
                console.log(data);
            })
        });
        jq('#successfulAdmissionsSwitch').click(function () {
            var notifySuccessfulAdmissions = "false"
            if (jq(this).is(':checked')) {
                notifySuccessfulAdmissions = "true"
            }
            jq.getJSON('${ui.actionLink("botswanaemr", "admin/notificationSettings", "notifySuccessfulAdmissionsSetting")}',
                {'notifySuccessfulAdmissions': notifySuccessfulAdmissions}
            ).success(function (data) {
                console.log(data);
            })
        });

    });
</script>

<div>
    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-top-0 border-left-0">
            <div>
                <label>New patient in patient pool</label>

                <p>Receive notifications on new patients being registered and added to the patient pool</p>
            </div>

            <div class="custom-control custom-switch switch">
                <input type="checkbox" class="custom-control-input" id="newPatientsInPoolSwitch">
                <label class="custom-control-label" for="newPatientsInPoolSwitch"></label>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Selecting a patient to treat</label>

                <p>Receive notifications on selection of a patient to treat</p>
            </div>

            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="patientSelectedSwitch">
                <label class="custom-control-label" for="patientSelectedSwitch"></label>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Edits of consultations</label>

                <p>Notifications on any edits of consultations conducted by you</p>
            </div>

            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="consultationEditsSwitch">
                <label class="custom-control-label" for="consultationEditsSwitch"></label>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Lab results</label>

                <p>Receive notifications on lab orders being completed</p>
            </div>

            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="labResultsSwitch">
                <label class="custom-control-label" for="labResultsSwitch"></label>
            </div>
        </li>
        <li class="list-group-item d-flex justify-content-between align-items-center border-right-0 border-left-0">
            <div>
                <label>Successful admission</label>

                <p>Receive notifications when a patient has been admitted successfully by you</p>
            </div>

            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="successfulAdmissionsSwitch">
                <label class="custom-control-label" for="successfulAdmissionsSwitch"></label>
            </div>
        </li>
    </ul>

</div>