<script type="text/javascript">
    var rows = []; //make this accessible globally
    jq(document).ready(function () {
        jq('#list-mfl-facilities').on('click', '.remove', function (event) {
            event.preventDefault()
            var table = \$('#list-mfl-facilities').DataTable();
            var row = \$(this).parents('tr');
            var rowIndex = row.index()
            if (rowIndex > -1) // only splice array when item is found
            {
                rows.splice(rowIndex, 1); // remove one item only
            }
            if ((row).hasClass('child')) {
                table.row((row).prev('tr')).remove().draw();
            } else {
                table.row(\$(this).parents('tr')
            ).remove().draw();
            }
        });
    })

    function addMflFacility() {
        rows = [];
        jQuery("#list-mfl-facilities").DataTable().clear().destroy();
        jq("#mfl-facility-search-form")[0].reset();
        initializeFacilitiesTable()
        jQuery("#addMflFacilitiesModal").modal('show');
        var sourceValues = [];
        jq.getJSON('${ui.actionLink("botswanaemr", "admin/addMflFacilities", "getMflFacilities")}'
        ).success(function (data) {
            data.map((item)=>{
                var removeAction = "<input hidden name='mflFacilityCode' value='"+ item.newFacilityCode +"|"+ item.facilityName +"'/><button class='remove color-primary btn btn-sm '>Remove</button>";
                var result = {label:item.newFacilityCode +" : "+ item.facilityName , value:"<tr><td></td><td>"+item.newFacilityCode+"</td><td>"+item.facilityName+"</td><td>"+item.status + "</td><td>"+ removeAction +"</td></tr>"};
                sourceValues.push(result)
            });
            jq('#mflFacilitySearch').autocomplete({
                source: sourceValues,
                minLength: 2,
                select: function (event, ui) {
                    event.preventDefault();
                    rows.push(ui.item.value);
                    jQuery("#list-mfl-facilities").DataTable().clear().destroy();
                    jq("#list-mfl-facilities").append(...new Set(rows));
                    initializeFacilitiesTable();
                    jq(this).val('');
                    return false;
                }
            });
        }).fail(function() {
            jq().toastmessage('showErrorToast',  "MFL Error");
        });
    }

    function initializeFacilitiesTable(){
        var jq = jQuery;
       var t = jq('#list-mfl-facilities').DataTable({
            searchPanes: false,
            searching: false,
           responsive: true,
            "pagingType": 'simple_numbers',
            "dom": 'rtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            },
            "language": {
                "emptyTable": "No facilities picked yet"
            },
            columnDefs: {
                searchable: false,
                orderable: false,
                targets: 0,
            },
           order: [[1, 'asc']],

       });

        t.on('order.dt search.dt', function () {
            let i = 1;
            t.cells(null, 0, {search: 'applied', order: 'applied'}).every(function (cell) {
                this.data(i++);
            });
        }).draw();
    }

</script>
<div>
<form method="get" class="form-group" id="mfl-facility-search-form" onsubmit="return false">
    <input type="text" id="mflFacilitySearch" class="form-control" placeholder="${ui.message("Search by name or code...")}"/>
</form>
</div>

<form id="userDetailsForm" name="mflFacilitiesForm" class="simple-form-ui"
      action="${ui.actionLink('botswanaemr', 'admin/addMflFacilities', 'post')}" method="post">
    <div class="table-responsive">
        <table id="list-mfl-facilities" class="table table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>${ui.message("#")}</th>
                <th>${ui.message("MFL Code")}</th>
                <th>${ui.message("Facility Name")}</th>
                <th>${ui.message("Status")}</th>
                <th>${ui.message("general.action")}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
            ${ui.message("Close")}
        </button>
        <button id="saveFacilities" 
                type="submit"
                class="btn btn-sm btn-primary float-right float-left ml-1">${ui.message("Save")}
        </button>
    </div>
</form>
</div>
