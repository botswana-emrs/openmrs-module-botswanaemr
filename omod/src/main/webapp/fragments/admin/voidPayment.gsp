<script type="text/javascript">
    jQuery(function () {

        jQuery("form#voidPaymentForm").submit(function (e) {
            e.preventDefault();
            jq.getJSON('${ ui.actionLink("botswanaemr", "admin/voidPayment", "voidPaymentTransaction")}',
                {
                    'vRegistrationId': jq('#vRegistrationId').val(),
                    'voidingReason': jq('#voidingReason').val()
                })
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Transaction voided successfully");
                    jQuery("#voidPaymentModal").modal('hide');
                    location.reload();
                })
                .fail(function (err) {
                        console.log(err)
                    }
                )
        });
    });

</script>

<form id="voidPaymentForm" method="post"
      action="${ui.actionLink("botswanaemr", "voidPayment", "voidPaymentTransaction")}">
    <div class="form-group">
        <label for="vPaymentMethod">Payment Method
            <span class="text-danger">*</span>
        </label>
        <input name="vPaymentMethod" id="vPaymentMethod" class="form-control" readonly >
    </div>

    <div class="form-group">
        <label for="vBillableAmount">Billable amount
            <span class="text-danger">*</span>
        </label>
        <input class="form-control" name="vBillableAmount" id="vBillableAmount" readonly>
    </div>

    <div class="form-group">
        <label for="vAmountPaid">Amount paid
            <span class="text-danger">*</span>
        </label>
        <input type="number" class="form-control" id="vAmountPaid" required name="vAmountPaid" readonly>
    </div>

    <div class="form-group">
        <label for="vPaymentDate">Payment Date
            <span class="text-danger">*</span>
        </label>
        <input type="text" class="form-control" id="vPaymentDate"
               name="vPaymentDate" readonly>
    </div>

    <div class="form-group">
        <label for="voidingReason">Reason for Voiding:
            <span class="text-danger">*</span>
        </label>
        <textarea class="form-control" name="voidingReason" id="voidingReason" rows="5" required></textarea>
    </div>

    <div class="row">
        <input type="hidden" name="vRegistrationId" id="vRegistrationId"/>
    </div>

    <div class="row">
        <div class="col pr-0">
            <button id="savePayment"
                    class="btn btn-sm btn-primary float-right ml-1"
                    type="submit"> ${ui.message("Void Transaction")}
            </button>
            <button class="btn btn-sm btn-dark bg-dark float-right" data-dismiss="modal">
                ${ui.message("Close")}
            </button>
        </div>
    </div>

</form>
