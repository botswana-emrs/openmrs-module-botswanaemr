<script type="text/javascript">
    jQuery(function () {
        jQuery("form#editPaymentMethodForm").submit(function (e) {
            e.preventDefault();
            let paymentMethodForm = jQuery(this);
            let params = {
                editPaymentMethodId: paymentMethodForm.find("#editPaymentMethodId").val(),
                editDescription: paymentMethodForm.find("#editDescription").val(),
                editPaymentMethodName: paymentMethodForm.find("#editPaymentMethodName").val()
            }
            jq.getJSON('${ ui.actionLink("botswanaemr", "admin/paymentMethod/editPaymentMethod", "editPaymentMethod")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The Payment Method has been updated");
                    jQuery("#editPaymentMethodModal").modal('hide');
                    location.reload();
                })
        });
    });

</script>
<form id="editPaymentMethodForm" method="post" action="${ ui.actionLink('botswanaemr', 'editPaymentMethod','editPaymentMethod')}">
    <div class="form-group">
        <label for="editPaymentMethodName"> Payment Method:
            <span class="text-danger">*</span>
        </label>
        <input type="hidden" id="editPaymentMethodId" class="form-control" name="editPaymentMethodId" value="${paymentMethod.uuid}" required/>
        <input type="text" id="editPaymentMethodName" class="form-control" name="editPaymentMethodName" placeholder="Enter payment Method's name" value="${paymentMethod.name}" required/>
    </div>
    <div class="form-group">
        <label for="editDescription"> Description
            <span class="text-danger">*</span>
        </label>
        <textarea class="form-control" name="editDescription" id="editDescription" rows="5" required>${paymentMethod.description}</textarea>
    </div>
    
    <div class="row">
        <div class="col pr-0">
            <button id="savePaymentMethod"
                    class="btn btn-sm btn-primary float-right ml-1"
                    type="submit">${ui.message("Save")}
            </button>
            <button type="button"
                    class="btn btn-sm btn-dark bg-dark float-right"
                    data-dismiss="modal">Close
            </button>
        </div>
    </div>
</form>
