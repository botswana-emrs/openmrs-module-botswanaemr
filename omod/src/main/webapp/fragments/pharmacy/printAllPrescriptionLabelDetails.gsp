<style>
    #labelStrip.card * {
        font-family: sans-serif !important;
        text-transform: capitalize !important;
        color: #000000 !important;
    }
    @media print {
        html, body {
            height:100vh;
            margin: 0 !important;
            padding: 0 !important;
            overflow: hidden;
        }
    }
    .print {
        page-break-after: avoid;
    }
</style>

<div class="col-12" id="labelStrip">
    <% orders.each { %>
        ${ ui.includeFragment("botswanaemr", "pharmacy/printPrescriptionLabelDetails", [orderId: it.id])}
    <% } %>
</div>