<script type="text/javascript">

  jQuery(function () {
    jQuery('#dailyTotals').highcharts({
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Daily workload'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
      },
      series: [{
        name: 'Statistics',
        colorByPoint: true,
        data: [{
          name: '00am',
          y:${t00},
        }, {
          name: '01am',
          y: ${t01},
        }, {
          name: '02am',
          y: ${t02},
        }, {
          name: '03am',
          y: ${t03},
        }, {
          name: '04am',
          y: ${t04},
        }, {
          name: '05am',
          y: ${t05},
        },{
          name: '06am',
          y: ${t06},
        },{
          name: '07am',
          y: ${t07},
        },{
          name: '08am',
          y: ${t08},
        },{
          name: '09am',
          y: ${t09},
        },{
          name: '10am',
          y: ${t10},
        },{
          name: '11am',
          y: ${t11},
        },{
          name: '12pm',
          y: ${t12},
        },{
          name: '1pm',
          y: ${t13},
        },{
          name: '2pm',
          y: ${t14},
        },{
          name: '3pm',
          y: ${t15},
        },{
          name: '4pm',
          y: ${t16},
        },{
          name: '5pm',
          y: ${t17},
        },{
          name: '6pm',
          y: ${t18},
        },{
          name: '7pm',
          y: ${t19},
        },{
          name: '8pm',
          y: ${t20},
        },{
          name: '9pm',
          y: ${t21},
        },{
          name: '10pm',
          y: ${t22},
        },{
          name: '11pm',
          y: ${t23},
        }]
      }],
    });
  });
</script>
<div id="dailyTotals" style="min-width: 100%; height:100px; margin: 0; padding-bottom: 5px;"></div>
