<style>
#labelsCard.card * {
    font-family: sans-serif !important;
    text-transform: capitalize !important;
    color: #000000 !important;
}
</style>

<%  prescriptionLabels.each {%>
    <div class="col">
        <div id="labelsCard" class="card">
                <div class="row">
                    <div class="justify-content-center">
                        <ul class="list-unstyled">
                            <li class="mt-1">
                                <div class="col pl-0 text-capitalize font-weight-normal">${it.drug}</div>
                            </li>
                            <li class="mt-1">
                                <div class="col pl-0 text-capitalize font-weight-normal">${it.dosage} <% if(it.duration != null) { %> For ${it.duration} <% } %></div>
                            </li>
                            <% if(it.dosingInstructions != "") { %>
                            <li class="mt-1">
                                <div class="col pl-0 text-capitalize font-weight-normal">${it.dosingInstructions}</div>
                            </li>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <hr class="divider bg-primary"/>
                <div class="row">
                    <div class="justify-content-center">
                        <ul class="list-unstyled">
                            <li class="mt-0">
                                <li class="text-capitalize">Name: ${it.patientNames}</li>
                            </li>
                            <li class="mt-0">
                                <li class="text-capitalize">Dispensed Qty: ${ui.format(it.quantityDispensed)} ${it.dispensedUnits ?: ""}</li>
                            </li>
                            <li class="mt-0">
                                <div class="col pl-0">
                                    <div class="text-capitalize">
                                        Batch Number: ${it.lotNumber ?: ""}
                                    </div>
                                </div>
                            </li>
                            <li class="mt-0">
                                <div class="col pl-0">
                                    <div class="text-capitalize">Expiry Date: ${it.expiryDate}</div>
                                </div>
                            </li>
                            <li class="mt-0">
                                <div class="text-capitalize">Date Dispensed: ${it.effectiveStartDate ?: ""}</div>
                            </li>
                            <li class="mt-0">
                                <div class="text-capitalize">Dispensed By: ${it.dispensedBy ?: ""}</div>
                            </li>
                            <li class="mt-0">
                                <div class="text-capitalize">Prescribed By: ${it.prescribedBy ?: ""}</div>
                            </li>
                            <li class="mt-0">
                                <div class="text-capitalize font-weight-normal">${it.facility} Pharmacy</div>
                            </li>
                            <li class="mt-0">
                                <div class="text-capitalize font-weight-normal">Visit Number: ${it.visitNumber}</div>
                            </li>
                        </ul>
                    </div>
                </div>
            <hr class="divider bg-primary"/>
        </div>
    </div>
<% } %>
