<script type="text/javascript">

  jQuery(function () {
    jQuery('#totalsEver').highcharts({
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Weekly workload'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
      },
      series: [{
        name: 'Statistics',
        colorByPoint: true,
        data: [{
          name: 'Mon',
          y:${mon},
        }, {
          name: 'Tue',
          y: ${tue},
        }, {
          name: 'Wed',
          y: ${wed},
        }, {
          name: 'Thur',
          y: ${thur},
        }, {
          name: 'Fri',
          y: ${fri},
        }, {
          name: 'Sat',
          y: ${sat},
        },{
          name: 'Sun',
          y: ${sun},
        }]
      }],
    });
  });
</script>
<div id="totalsEver" style="min-width: 100%; height:100px; margin: 0; padding-bottom: 5px;"></div>
