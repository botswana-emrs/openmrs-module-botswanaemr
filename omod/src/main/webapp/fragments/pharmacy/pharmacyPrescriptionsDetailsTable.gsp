<script type="text/javascript">
    jQuery(function () {
       var table = jQuery('#pharmacyPrescriptionsDetailsTable').DataTable({
            dom: 'rtp',
            searching: false,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jQuery("#fillItemsFromStores").on("click", "a", function (e) {
            let facilityName = jQuery(this).parents('tr').find('td:first').text();
            if (!confirm('You are about to make a hold request to facility ' + facilityName + '. Do you want to continue?')) {
                return;
            }

            let stockroomId = jQuery(this).parents('tr').find("input[name^=holdStockroom]").val();
            var parameters = {
                'stockRoomId': stockroomId,
                'itemId': jQuery("#operationsRefModalItemValue").val(),
                'orderDetailsId': jQuery("#orderDetailsId").val(),
                'quantityToFill': jQuery("#quantityToFill").val()
            };
            jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "makeHoldRequest")}', parameters)
                .done(function (data) {
                    jq().toastmessage('showNoticeToast', "Hold operation was successful");
                    location.reload();
                });
        })



        jQuery("#stockRoomId").change(function() {
            //Fetch available batches
            let orderId = jQuery("#orderDetailsId").val();
            let stockRoomId = jQuery("#stockRoomId").val();
            let drugId = jQuery("#altDrugRef").val();


            if (stockRoomId !== '' && drugId !== '') {
                fetchAvailableBatchesByDrug(drugId, stockRoomId);
            }
        });

        jQuery("#batchNo").change(function() {
            jQuery("#itemStockDetailId").val(jQuery(this).find('option:selected').attr('data-overlay'));
            //Fetch available quantities
            let orderId = jQuery("#orderDetailsId").val();
            let stockroomId = jQuery('#stockRoomId').val();
            let drugId = jQuery('#altDrugRef').val();
            editOrder(orderId, drugId, stockroomId);
        });

        jQuery('#pharmacyPrescriptionsDetailsTable').find('button[name^=fillPrescription]').click(function() {
            let orderId = jQuery(this).parents('tr').find('input[name^=orderId]').val();
            let drugConceptId = jQuery(this).parents('tr').find('input[name^=drugConceptId]').val();
            let stockRoomId = jQuery("#stockRoomId").val();
            let drugId = null;

            jQuery("#orderDetailsId").val(orderId);
            editOrder(orderId, drugId, stockRoomId);
            fetchHoldStockOperation(orderId);
            getAlternativeDrugs(drugConceptId).then(function(drugAlternatives) {
                fillAlternativeDrugs(drugAlternatives);
            });
        });
    });

    function fetchAvailableBatchesByDrug(drugId, stockRoomId) {
        // jQuery("#orderDetailsId").val(orderId);
        let parameters = {
            'drugId': drugId,
            'stockRoomId': stockRoomId
        };
        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "fetchAvailableBatchesByDrug")}', parameters)
            .done(function (data) {
                if (data.status === 'success') {
                    // populate dropdown with batches
                    let options = jQuery("#batchNo");
                    options.find('option').remove();

                    if (data.batches) {
                        if (data.batches.length > 0 ) {
                            options.append(jQuery('<option/>').val("").text("Auto"));
                            jQuery.each(data.batches, function(i, item) {
                                options.append(jQuery("<option data-overlay='" + item.id + "' />").val(item.batchNumber).text(item.expiryDate + '(' + item.quantity + ')'));
                            })
                            jQuery("#updateRefills").show();
                        } else {
                            options.append(jQuery('<option/>').val("").text("No batches found"));
                            jQuery("#updateRefills").hide();
                        }
                    }

                    setAvailability(data.quantity);

                    jQuery("#itemUuidRef").text(data.itemCode);

                    // Set quantityToFill: TODO: Fetch the qty to fill
                    //jQuery("#quantityToFill").val('');


                    // editOrder(orderId, stockRoomId);
                } else {
                    // clear batches from dropdown list
                    options.find("option").remove();
                }
            });
    }

    // change event for the altDrugRef dropdown to fetch available batches for drug fetchAvailableBatchesByDrug
    jQuery("body").on('change','#altDrugRef',function() {
        let orderId = jQuery("#orderDetailsId").val();
        let drugId = jQuery(this).val();
        let stockRoomId = jQuery("#stockRoomId").val();
        fetchAvailableBatchesByDrug(drugId, stockRoomId);
        editOrder(orderId, drugId, stockRoomId);

    });

    let fillAlternativeDrugs = function(drugAlternatives) {
        jQuery("#altDrugRef").empty();
        // append empty option
        jQuery("#altDrugRef").append(new Option("", ""));
        // Append new options from drugAlternatives
        jQuery.each(drugAlternatives, function (key, drug) {
            jQuery("#altDrugRef").append(new Option(drug.name, drug.drugId));
        });
        // jQuery("#altDrugRef").val(data.drugId);
        jQuery("#altDrugRef").val('');
    }
    
    function editOrder(orderId, drugId, stockRoomId) {
        jQuery("#orderDetailsId").val(orderId);

        let parameters = {
            'orderDetailsId': orderId,
            'stockRoomId': stockRoomId,
            'drugId': drugId
        };
        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "getSingleOrderDetails")}', parameters)
            .done(function (data) {
                jQuery("#quantityToFill").val(data.calculatedQuantity);
                if (data.totalRefills > 0) {
                    jQuery("#refillFraction").text(data.fillCount + "/" + data.totalRefills);
                }
                jQuery("#drugRef").empty().append(data.drug);
                jQuery("#dosageRef").empty().append(data.dosage);
                jQuery("#refillDurationRef").empty().append(data.refillDuration);
                jQuery("#refillsRef").empty().append(data.refillRepeat);
                jQuery("#frequencyReff").empty().append(data.frequency);
                jQuery("#routeReff").empty().append(data.route);
                jQuery("#orderStatus").empty().val(data.status);
                jQuery("#itemUuidRef").empty().append(data.itemCode);
                jQuery("#operationsRefModalItemValue").val(data.itemId);
                jQuery("#operationsRefModalItemOrderId").val(data.drugOrderId);
                setAvailability(data.stockAvailability);

                jQuery(".open-editPrescription").attr('data-id', data.drugOrderUuid);
            });
    }

    function setAvailability(stockAvailability) {
        if (stockAvailability <= 0) {
            jQuery("#stockAvailabilityRef").empty().append(stockAvailability);
            jQuery("#stockAvailabilityRef").css('color', 'red');
            jQuery("#updateRefills").hide();
            jQuery("#updateUnavailable").show();
        } else {
            jQuery("#stockAvailabilityRef").empty().append(stockAvailability);
            jQuery("#updateRefills").show();
            jQuery("#updateUnavailable").hide();
        }
    }

    function getAlternativeDrugs(conceptId) {
        return new Promise(function(resolve, reject) {
            // Perform AJAX request to fetch drug alternatives
            // Replace the URL and parameters with your actual API endpoint
            jQuery.ajax({
                url: '${ ui.actionLink("botswanaemr", "search", "getAlternativeDrugs")}',
                dataType: "json",
                method: 'GET',
                data: { conceptId: conceptId },
                success: function(response) {
                    // Resolve the promise with the array of drug alternatives
                    resolve(response.drugAlternatives);
                },
                error: function(error) {
                    // Reject the promise with the error message
                    reject(error);
                }
            });
        });
    }

    function updateOrderRefills() {
        // if quantityToFill is less than the quantity available, fail validation
        let quantityAvailable = parseInt(jQuery("#stockAvailabilityRef").text());
        let quantityToFill = jQuery("#quantityToFill").val();

        if (quantityToFill <= 0) {
            jq().toastmessage('showErrorToast', 'The quantity to dispense is should be greater than 0');
            return;
        }

        if (quantityAvailable < quantityToFill) {
            jq().toastmessage('showErrorToast', 'The quantity available is less than the quantity to dispense');
            return;
        }

        let parameters = {
            'orderDetailsId': jQuery("#orderDetailsId").val(),
            'quantityToFill': jQuery("#quantityToFill").val(),
            'stockRoomId': jQuery("#stockRoomId").val(),
            'batchNo': jQuery("#batchNo").val(),
            'itemStockDetailId': jQuery("#itemStockDetailId").val(),
            'drugId': jQuery("#altDrugRef").val(),
            'itemCode': jQuery("#itemUuidRef").text()
        };

        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "updateDrugOrder")}', parameters)
            .done(function (data) {
                jq().toastmessage('showNoticeToast', "Refills successful");
                location.reload();
            })
            .fail(function (response) {
                jq().toastmessage('showErrorToast', "Something went wrong while trying to update prescription");
                console.log(response.error);
            });
    }

    function updateUnavailableDrug() {
        let parameters = {
            'orderDetailsId': jQuery("#orderDetailsId").val()
        };
        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "updateUnavailableDrug")}', parameters)
            .done(function (data) {
                jq().toastmessage('showNoticeToast', "Updated successful");
                location.reload();
            });
    }

    function checkAvailabilityFromOtherLocations() {
        checkOrderToFindFromOtherStores();
        checkItemStockToFindFromOtherStores();
    }

    function checkOrderToFindFromOtherStores() {
        var parameters = {
            'operationsRefModalItemOrderId': jQuery("#operationsRefModalItemOrderId").val()
        };
        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "getItemsAsAnOrder")}', parameters)
            .done(function (data) {
                jQuery("#drug").empty().text(data.drug);
                jQuery("#dosage").empty().text(data.dosage);
                jQuery("#refillDuration").empty().text(data.refillDuration);
                jQuery("#refillRepeat").empty().text(data.refillRepeat);
                jQuery("#frequency").empty().text(data.frequency);
                jQuery("#route").empty().text(data.route);
            });
    }

    function checkItemStockToFindFromOtherStores() {
        var parameters = {
            'operationsRefModalItemValue': jQuery("#operationsRefModalItemValue").val()
        };
        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "getItemStockToSearchInOtherStoreRooms")}', parameters)
            .done(function (data) {
                populateTableBodyCheckingFromTheStore(data);
            });
    }

    function populateTableBodyCheckingFromTheStore(data) {
        jQuery("#itemDetailsFromStores").empty();
        data.map((item) => {
            jQuery("#itemDetailsFromStores").append("<tr><td>" + item.storeRoomLocation + "</td><td>" + item.drug + "</td><td>" + item.stockAvailability + "</td><td><a class='btn btn-sm btn-warning' href='#'>" + "Request Hold" + "</a><input type='hidden' name='holdStockroom[]' value='" + item.stockroomId + "'/></td></tr>");
        });
    }

    function toggleDisplayViewFilled(orderId) {
        jQuery("#orderDetailsIdToView").val(orderId);
        getPatientPrescriptionOrdersToView();
    }

    function toggleDisplayView(orderId) {
        jQuery("#orderDetailsIdToViews").val(orderId);
        getPatientPrescriptionOrdersToViewDetails();
    }

    function populateTableBody(data) {
        jQuery("#orderDetailsFilledView").empty();
        data.map((item) => {
            jQuery("#orderDetailsFilledView").append("<tr><td>" + item.drug + "</td><td>" + item.dateRefilled + "</td><td>" + item.refillRepeat + "</td><td>" + item.facility + "</td><td>" + item.pharmacist + "</td></tr>");
        });
    }

    function populateTableBodyView(data) {
        jQuery("#orderDetailsViews").empty();
        data.map((item) => {
            jQuery("#orderDetailsViews").append("<tr><td>" + item.drug + "</td><td>" + item.dateRefilled + "</td><td>" + item.refillRepeat + "</td><td>" + item.facility + "</td><td>" + item.pharmacist + "</td></tr>");
        });
    }

    function populateTableBodyViewForItemsFromOtherStores(data) {
        jQuery("#fillItemsFromStores").empty();
        data.map((item) => {
            jQuery("#itemDetailsFromStores").append("<tr><td>" + item.facility + "</td><td>" + item.drug + "</td><td>" + item.availability + "</td><td></tr>");
        });
    }

    function getPatientPrescriptionOrdersToView() {
        var searchResult;
        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","refillsDoneList")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                orderDetailsIdToView: jQuery("#orderDetailsIdToView").val()
            },
            success: function (data) {
                searchResult = data;
            }
        });
        populateTableBody(searchResult)
        return searchResult;
    }

    function getPatientPrescriptionOrdersToViewDetails() {
        var searchResult;
        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","refillsDoneListView")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                orderDetailsIdToViews: jQuery("#orderDetailsIdToViews").val()
            },
            success: function (data) {
                searchResult = data;
            }
        });
        populateTableBodyView(searchResult)
        return searchResult;
    }

    function getItemsFromStores() {
        var searchResult;
        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","getItemsFromStores")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                orderDetailsIdToView: jQuery("#operationsRefModalItemValue").val()
            },
            success: function (data) {
                searchResult = data;
            }
        });
        populateTableBodyViewForItemsFromOtherStores(searchResult)
        return searchResult;
    }

    function getItemsFromStores() {
        var searchResult;
        jQuery.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","search","getItemsFromStores")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
                orderDetailsIdToView: jQuery("#operationsRefModalItemValue").val()
            },
            success: function (data) {
                searchResult = data;
            }
        });
        populateTableBodyViewForItemsFromOtherStores(searchResult)
        return searchResult;
    }

    function fetchHoldStockOperation(orderId) {
        // Get approved hold Tx and use that info to populate the relevant UI sections
        var parameters = {
            'orderId': jQuery("#orderDetailsId").val(),
        };

        jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "fetchHoldStockOperation")}', parameters)
            .done(function (data) {
                if (data.status.indexOf('200') > -1) {
                    if (data.response != null) {
                        let stockRoomId = data.response.stockroomId;
                        let drugId = jQuery("#altDrugRef").val();

                        jQuery('#operationsRefModal').hide();
                        jQuery('#stockRoomId').val(stockRoomId);
                        fetchAvailableBatchesByDrug(drugId, stockRoomId);
                    }
                }
            });
    }
</script>

<div class="table-responsive">
    <table id="pharmacyPrescriptionsDetailsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Drug</th>
            <th>Dose</th>
            <th>Duration(Days)</th>
            <th>Refill repeat</th>
            <th>Frequency</th>
            <th>Route</th>
            <th>
                <a class="btn btn-sm btn-primary py-1 text-white" href="/${ui.contextPath()}/botswanaemr/pharmacy/printAllPrescriptionsLabels.page?encounterId=${encounter.encounterId}">
                    <i class="fa fa-print"> </i> Print all labels
                </a>
            </th>
            <th>Operations</th>
        </tr>
        </thead>
        <tbody>
        <% drugOrder.each { %>
        <tr>
            <td>${it.drug}</td>
            <td>${it.dosage}</td>
            <td>${it.refillDuration}</td>
            <% if (it.fillCount > 0) { %>
            <td>${it.fillCount}/${it.totalRefills}</td>
            <% } else { %>
            <td>${it.refillRepeat}</td>
            <% } %>
            <td>${it.frequency}</td>
            <td>${it.route}</td>
            <td>
                <div class="row">
                    <% if (it.status.toString() != "COMPLETED" && it.refillRepeat.toInteger() >= 0) { %>
                    <div class="col">
                        <input type="hidden" value="${it.orderId}" name="orderId[]" />
                        <input type="hidden" value="${it.drugConceptId}" name="drugConceptId[]" />
                        <!--
                        For a hold operation, an order has to be accepted first (Transitioned to "RECEIVED" status) before filling can be done.
                        -->
                        <% if (it.orderReason == null || (it.orderReason.contains("Hold") && it.status.equals("RECEIVED"))) { %>
                        <button type="button" class="btn btn-sm btn-dark bg-dark border-0 float-right ml-1" data-toggle="modal"
                                data-target="#prescriptionModal" name="fillPrescription[]">Fill</button>
                        <% } %>
                        <% if (it.status == 'EXCEPTION') { %>
                            <span class="label label-danger text-white px-1 py-1">Marked Unavailable</span>
                        <% } %>
                    </div>
                    <% } %>

                    <% if (it.status.toString() == "IN_PROGRESS" || it.status.toString() == "COMPLETED") { %>
                    <div class="col">
                        <a class="btn btn-sm btn-primary py-1 text-white" href="/${ui.contextPath()}/botswanaemr/pharmacy/printPrescriptionLabels.page?patientId=${patient.id}&encounterId=${encounter.encounterId}&visit=${visit.visitId}&orderId=${it.orderId}">
                            Print
                        </a>
                    </div>
                    <% } %>
    
                    <% if (it.fillCount.toInteger() == it.refillRepeat.toInteger()) { %>
                    <div class="col">
                        <button type="button" class="btn btn-sm btn-dark bg-dark border-0 float-right" data-toggle="modal"
                                data-target="#prescriptionViewModal" onclick="toggleDisplayView(${it.orderId})">View</button>
                    </div>
                    <% } %>
                </div>
            </td>
            <td>
                <% if (it.fillCount.toInteger() >= 0 && it.refillRepeat.toInteger() >= 0 && it.fillCount.toInteger() != it.refillRepeat.toInteger()) { %>
                <div class="col">
                    <button type="button" class="btn btn-sm btn-dark bg-dark border-0 float-right" data-toggle="modal"
                            data-target="#viewRelatedOrdersModal"
                            onclick="toggleDisplayViewFilled(${it.orderId})">Filled</button>
                </div>
                <% } %>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="prescriptionModal" tabindex="-1"
     data-controls-modal="prescriptionModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="prescriptionModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="prescriptionModalTitle">Fill Prescription</h4>
                <hr/>
                <button type="button" id="editPatientDetails" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <h6>Are you sure you want to fill <span id="refillFraction"></span> prescription?</h6>
                <hr/>
                <input type="hidden" id="orderDetailsId" name="orderDetailsId"/>
                <table id="drugRefillDialog" style="width:100%">
                    <tr>
                        <th>Stock Room</th>
                        <th>Drug</th>
                        <th>Dose</th>
                        <th>Duration</th>
                        <th>Refills</th>
                        <th>Frequency</th>
                        <th>Route</th>
                        <th>Availability</th>
                        <th>Item Code</th>
                        <th>Batch</th>
                        <th>Operations</th>
                    </tr>
                    <tr>
                        <td id="stockRoom">
                            <select name="stockRoomId" id="stockRoomId" class="form-control form-control-sm input input-sm" style="min-width:100px">
                                <option <% if (dispensingStockRooms.size() > 1) { %> selected <% } %>
                                                                                     value="">Select dispensing stockroom</option>
                                <% dispensingStockRooms.each { %>
                                <option <% if (dispensingStockRooms.size() == 1) { %> selected <% } %>
                                                                                      value="${it.id}">${it.name}</option>
                                <% } %>
                            </select>
                        </td>
                        <td><span id="drugRef" class="hidden"></span><select id="altDrugRef" class="form-control form-control-sm input input-sm" style="min-width:100px"><option></option><select/></td>
                        <!--<td id="drugRef">&nbsp;</td>-->
                        <td id="dosageRef">&nbsp;</td>
                        <td id="refillDurationRef">&nbsp;</td>
                        <td id="refillsRef">&nbsp;</td>
                        <td id="frequencyReff">&nbsp;</td>
                        <td id="routeReff">&nbsp;</td>
                        <td id="stockAvailabilityRef">&nbsp;</td>
                        <td id="itemUuidRef">&nbsp;</td>
                        <td>
                            <input type="hidden" id="orderStatus">
                            <input type="hidden" id="itemStockDetailId">
                            <select name="batchNo" id="batchNo" class="form-control form-control-sm input input-sm" style="min-width:100px"></select>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-info  btn-outline-info text-white" href="#" data-toggle="modal" data-target="#operationsRefModal"
                               onclick="checkAvailabilityFromOtherLocations()"><i class="fa fa-question-circle"> </i> Check other locations</a>
                            <br/>

                            <!-- Hide this functinality until further guidance is provided -->
                            <a class="btn btn-sm btn-primary text-white open-EditPrescription hidden" data-id="Order UUID"
                               data-toggle="modal" data-target="#editPrescriptionModal" href="#">Edit</a>
                            <br/>
<!--                            <a class="btn btn-sm btn-info text-white" href="#">Previous</a>-->
                        </td>
                    </tr>
                </table>
                <br/>
                <label for="quantityToFill">Quantity to fill</label>
                <input type="text" id="quantityToFill" name="quantityToFill" size="10"/>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark" data-dismiss="modal">Close</button>
                <a class="btn btn-primary text-white open-EditPrescription" data-id="Order UUID"
                               data-toggle="modal" data-target="#editPrescriptionModal" style="margin-left: 2rem" href="#">Edit</a>
                <button type="button" class="btn btn-primary" style="margin-left: 2rem" id="updateUnavailable"
                        onclick="updateUnavailableDrug()">Mark Unavailable</button>
                <button type="submit" class="btn btn-primary" style="margin-left: 2rem" id="updateRefills"
                        onclick="updateOrderRefills()">Yes,fill</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="prescriptionViewModal" tabindex="-1"
     data-controls-modal="prescriptionViewModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="prescriptionViewModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="prescriptionViewModalTitle">View filled Prescription</h4>
                <hr/>
                <button type="button" id="viewOrderDetailsView" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <input type="hidden" id="orderDetailsIdToViews" name="orderDetailsIdToViews"/>

            <div class="modal-body">

                <table id="drugRefilledDetailsDialogView" style="width:100%">
                    <thead>
                    <tr>
                        <th>Drug</th>
                        <th>Date and Time filled</th>
                        <th>Refill</th>
                        <th>Facility</th>
                        <th>Pharmacist</th>
                    </tr>
                    </thead>
                    <tbody id="orderDetailsViews"></tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn " data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="viewRelatedOrdersModal" tabindex="-1"
     data-controls-modal="viewRelatedOrdersModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="viewRelatedOrdersModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="viewRelatedOrdersModalTitle">Already filled Prescription</h4>
                <hr/>
                <button type="button" id="viewOrderDetails" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <input type="hidden" id="orderDetailsIdToView" name="orderDetailsIdToView"/>

            <div class="modal-body">

                <table id="filledDetails" style="width:100%">
                    <thead>
                    <tr>
                        <th>Drug</th>
                        <th>Date and time filled</th>
                        <th>Refill</th>
                        <th>Facility</th>
                        <th>Pharmacist</th>
                    </tr>
                    </thead>
                    <tbody id="orderDetailsFilledView"></tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="operationsRefModal" tabindex="-1"
     data-controls-modal="operationsRefModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="operationsRefModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="operationsRefModalTitle">Filled Prescription</h4>
                <hr/>
                <button type="button" id="operationsRefId" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <input type="hidden" id="operationsRefModalItemValue" name="operationsRefModalItemValue"/>
            <input type="hidden" id="operationsRefModalItemOrderId" name="operationsRefModalItemOrderId"/>

            <div class="modal-body">
                <p>Check Availability</p>
                <table width="100%">
                    <tr>
                        <td>Drug:<span id="drug"></span></td>
                        <td>Dosage:<span id="dosage"></span></td>
                        <td>Duration:<span id="refillDuration"></span></td>
                        <td>Refill Repeat:<span id="refillRepeat"></span></td>
                        <td>Frequency:<span id="frequency"></span></td>
                        <td>Route:<span id="route"></span></td>
                    </tr>
                </table>
                <hr/>
                <table id="fillItemsFromStores" style="width:100%">
                    <thead>
                    <tr>
                        <th>Facility</th>
                        <th>Drug</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="itemDetailsFromStores"></tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#prescriptionModal").appendTo("body");
        jQuery("#viewRelatedOrdersModal").appendTo("body");
        jQuery("#prescriptionViewModal").appendTo("body");
        jQuery("#operationsRefModal").appendTo("body");
    });
</script>
