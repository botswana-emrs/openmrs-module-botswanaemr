<script type="text/javascript">
    let updateTable = async function () {
        let response = await getPatientPrescriptionOrders();
        jQuery('#pharmacyPrescriptionsTable').DataTable().buttons().destroy();
        jQuery('#pharmacyPrescriptionsTable').DataTable().clear().destroy();

        if (response.status === 'success') {
            populateTableBody(response.data);
        }
    };
    jQuery(function () {
        updateTable();
        jQuery('#inquireBtn').on('click', () => {
            showLoadingOverlay();
            updateTable();
        });
        jQuery('#resetBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#pharmacyPrescriptions'));
            updateTable();
        });
    });

    async function getPatientPrescriptionOrders() {
        let promise = new Promise(function (resolve, reject) {
            jQuery.ajax({
                type: "POST",
                url: '${ui.actionLink("botswanaemr","search","getOrderPrescriptions")}',
                dataType: "json",
                global: false,
                async: true,
                data: {
                    nameOrUniqueId: jQuery("#searchPhrase").val(),
                    prescriptionStatus: jQuery("#prescriptionStatus").val(),
                    filteredLocation: jQuery("#filterLocation").val(),
                    prescriptionStartDate: jQuery("#prescriptionStartDate").val(),
                    prescriptionEndDate: jQuery("#prescriptionEndDate").val()
                },
                beforeSend: function() {
                    showLoadingOverlay();
                },
                success: function (data) {
                    resolve({'status': 'success', 'data': data});
                    hideLoadingOverlay();
                },
                error: function (response) {
                    console.log("response");
                    hideLoadingOverlay();
                    resolve({'status': 'error', 'data': response});
                },
                complete: function () {
                    hideLoadingOverlay();
                }
            });
        });

        return promise;
    }

    function populateTableBody(data) {
        if (data !== undefined) {
            data.map((item) => {
                let url = "href = /${ui.contextPath()}/botswanaemr/pharmacy/pharmacyPrescriptionDetails.page?patientId=" + item.patientId + "&encounterId=" + item.encounterId + "&visitId=" + item.visitId;
                let print = "href = /${ui.contextPath()}/botswanaemr/pharmacy/printPrescriptions.page?patientId=" + item.patientId + "&encounterId=" + item.encounterId;
                let status;

                jQuery("#prescriptionTableDetails").append("<tr><td>" + "</td><td>" + item.orderId + "</td><td>" + item.caseId + "</td><td>" + item.pin + "</td> <td>" + item.patientNames + "</td><td>" + item.prescriber + "</td><td>" + item.facility + "</td><td>" + item.datePrescribed + "</td><td>" + item.status + "</td><td><a " + url + " class='color-primary pl-0'>" + 'View' + "</a> |  <a " + print + " class='color-primary float-right pr-0'>" + 'Print' + "</a></td></tr>");
                //jQuery("#prescriptionTableDetails").append("<tr><td>" + "</td><td>" + item.orderId + "</td><td>" + item.caseId + "</td><td>" + item.pin + "</td> <td>" + item.patientNames + "</td><td>" + item.prescriber + "</td><td>" + item.facility + "</td><td>" + item.datePrescribed + "</td><td>" + item.status + "</td><td><a " + url + " class='color-primary pl-0'>" + 'View' + "</a></td></tr>");

            });
        }

        initializePatientOrderPrescriptionTable();
    }

    function initializePatientOrderPrescriptionTable() {
        let table = jQuery('#pharmacyPrescriptionsTable').DataTable({
            'columnDefs': [
                {
                    'targets': 0,
                    'className': 'select-checkbox',
                    'checkboxes': {
                        'selectRow': true,
                        'select': true
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'td:first-child'
            },
            'dom': 'lBrtip',
            'buttons': [
                {
                    'extend': 'collection',
                    'text': 'More operations',
                    'className': 'btn btn-sm p-1 ',
                    'exportOptions': {
                        'modifier': {
                            'selected': true
                        }
                    },
                    'buttons': [
                        {
                            'extend': 'csv',
                            'text': 'Export to CSV',
                            'className': 'btn btn-sm '
                        },
                        {
                            'extend': 'pdf',
                            'text': 'Export to PDF',
                            'className': 'btn btn-sm '
                        }
                    ]
                    ,
                    'dropup': false
                }
            ],
            'searching': true,

            "pagingType": 'simple_numbers',

            "language": {
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "paginate": {
                    "previous": "&lt;",
                    "next": "&gt;"
                }
            },
            'order': [[1, 'asc']]

        });
        jQuery('#selectedRowsAlertDiv').hide();

        table.on('select', function (e, dt, type, indexes) {
            if (table.rows({selected: true}).count() === 1) {
                jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " item selected");
                jQuery('#selectedRowsAlertDiv').show();
            } else {
                jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                jQuery('#selectedRowsAlertDiv').show();
            }
        })
            .on('deselect', function (e, dt, type, indexes) {
                if (table.rows({selected: true}).count() === 0) {
                    jQuery('#selectedRowsAlertDiv').hide();
                } else {
                    jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                    jQuery('#selectedRowsAlertDiv').show();
                }

            });

        table.buttons().container().appendTo(jQuery('#other '));

        jQuery('.card-link').click(() => {
            if (jQuery('#genderDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jQuery("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jQuery('#collapseBtn').html(span.append(i));
                jQuery('.collapse').removeClass("show");
            } else {
                let span = jQuery("<span/>").text("Collapse")
                i = jQuery("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jQuery('#collapseBtn').html(span.append(i));
                jQuery('.collapse').addClass("show");
            }
        });
    }

    function actionRedirect(patientId, encounterId) {
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/pharmacy/pharmacyPrescriptionDetails.page?patientId=" + patientId + "&encounterId=" + encounterId;
    }</script>


<div class="row mb-4">
    <div class="col-4 pl-0">
        <label for="searchPhrase" class="p-r-4">Search:</label>
        <input type="text" class="form-control border" id="searchPhrase"
               name="searchPhrase" placeholder="Search by Patient ID or name">
    </div>
    <div class="col-4" id="statusDiv">
        <div class="form-horizontal form-group">
            <label for="prescriptionStatus">Status:</label>
            <select class="custom-select form-control" id="prescriptionStatus" name="prescriptionStatus">
                <option selected value="">Select status:</option>
                <option value="awaiting">Awaiting</option>
                <option value="filled">Filled</option>
            </select>
        </div>
    </div>

    <div class="col-auto">
        <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
            ${ui.message("Search")}
        </button>
    </div>
    <div class="col-auto">
        <button type="submit" id="resetBtn" class="btn btn-sm btn-primary mb-3 bg-light text-dark border">
            ${ui.message("Reset")}
        </button>
    </div>
    <div class="col-auto">
        <a id="collapseBtn" class="card-link btn btn-sm btn-warning bg-white color-primary border-0"
           data-target=".multi-collapse"
           aria-expanded="false"
           aria-controls="dateStartDiv locationDiv"> ${ui.message("Expand")}
            <i id="collapse-icon" class="icon-chevron-down"></i>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-4 pl-0">
        <div class="form-horizontal form-group collapse multi-collapse" id="locationDiv">
            <label for="filterLocation" class="p-r-4">Facility:</label>
            <select class="custom-select form-control border form-inline" id="filterLocation" name="filteredLocation">
                <option selected value="">Select facility</option>
                <% facilities.each { %>
                <option value="${it.locationId}">${it.name}</option>
                <%}%>
            </select>
        </div>
    </div>
    <div class="col-2">
        <div class="form-horizontal form-group collapse multi-collapse" id="dateStartDiv">
            <label for="prescriptionStartDate">Start Date:</label>
            <input type="text"
                   class="form-control datepicker"
                   id="prescriptionStartDate"
                   name="prescriptionStartDate"
                   placeholder="Select start date prescribed"
            />
            <script type="text/javascript">
              jq('#prescriptionStartDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                "setDate": new Date(),
                dateFormat: "dd-M-yy",
                "autoclose": true
              });
            </script>
        </div>
    </div>
    <div class="col-2">
        <div class="form-horizontal form-group collapse multi-collapse" id="dateEndDiv">
            <label for="prescriptionEndDate">End Date:</label>
            <input type="text"
                   class="form-control datepicker"
                   id="prescriptionEndDate"
                   name="prescriptionEndDate"
                   placeholder="Select end date prescribed"
            />
            <script type="text/javascript">
              jq('#prescriptionEndDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                "setDate": new Date(),
                dateFormat: "dd-M-yy",
                "autoclose": true
              });
            </script>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
        <div id="selectedRowsAlertDiv" class="alert alert-info color-info p-0" role="alert">
            <label class="icon-info-sign color-primary" for="selectedRowsCount"></label>
            <label id="selectedRowsCount"></label>
        </div>
        <div class="table-responsive">
            <table id="pharmacyPrescriptionsTable"
                   class="table table-striped table-sm table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Prescription</th>
                    <th>Case</th>
                    <th>Patient ID</th>
                    <th>Patient Name</th>
                    <th>Prescribed by</th>
                    <th>Location</th>
                    <th>Prescribed on</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="prescriptionTableDetails">
                </tbody>
            </table>
        </div>
    </div>
</div>
