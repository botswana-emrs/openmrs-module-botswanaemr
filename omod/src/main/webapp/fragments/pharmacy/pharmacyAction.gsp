<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
<!--
    <li>
        <a href="${ui.pageLink('botswanaemr', 'pharmacy/pharmacyDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
-->
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ["action": "quick","returnUrl": patientPoolReturnUrl])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-users fa-1x"></i>
            </div>
            <div class="nav_label">Quick Patient Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'pharmacy/pharmacyPatientPool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-hourglass-start fa-1x"></i>
            </div>
            <div class="nav_label">Pharmacy Patient Pool</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'pharmacy/pharmacyAllPrescriptions')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-pencil-square-o fa-1x"></i>
            </div>
            <div class="nav_label">Prescriptions</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="#" class="nav_link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="nav_icon_container">
                <i class="fa fa-pie-chart fa-1x"></i>
            </div>
            <div class="nav_label">Stock Management</div>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/internalRequisition')}">
                    <i class="fas fa-clipboard"></i> Internal Requisitions
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/directReceipts')}">
                    <i class="fas fa-clipboard"></i> Direct receipts
                </a>
            </div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

