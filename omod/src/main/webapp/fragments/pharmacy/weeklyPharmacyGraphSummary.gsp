<script type="text/javascript">

  jQuery(function () {
    jQuery('#weeklyChart').highcharts({
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Weekly workload'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
      },
      series: [{
        name: 'Statistics',
        colorByPoint: true,
        data: [{
          name: 'Sun',
          y:${t0},
        }, {
          name: 'Mon',
          y: ${t1},
        }, {
          name: 'Tue',
          y: ${t2},
        }, {
          name: 'Wed',
          y: ${t3},
        }, {
          name: 'Thur',
          y: ${t4},
        }, {
          name: 'Fri',
          y: ${t5},
        },{
          name: 'Sat',
          y: ${t6},
        }]
      }],
    });
  });
</script>
<div id="weeklyChart" style="min-width: 100%; height:100px; margin: 0; padding-bottom: 5px;"></div>
