<script type="text/javascript">

  jQuery(function () {
    jQuery('#monthChart').highcharts({
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Monthly workload'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
      },
      series: [{
        name: 'Statistics',
        colorByPoint: true,
        data: [{
          name: '01',
          y:${t0},
        }, {
          name: '02',
          y: ${t1},
        }, {
          name: '03',
          y: ${t2},
        }, {
          name: '04',
          y: ${t3},
        }, {
          name: '05',
          y: ${t4},
        }, {
          name: '06',
          y: ${t5},
        },{
          name: '07',
          y: ${t6},
        },{
          name: '08',
          y: ${t7},
        },{
          name: '09',
          y: ${t8},
        },{
          name: '10',
          y: ${t9},
        },{
          name: '11',
          y: ${t10},
        },{
          name: '12',
          y: ${t11},
        },{
          name: '13',
          y: ${t12},
        },{
          name: '14',
          y: ${t13},
        },{
          name: '15',
          y: ${t14},
        },{
          name: '16',
          y: ${t15},
        },{
          name: '17',
          y: ${t16},
        },{
          name: '18',
          y: ${t17},
        },{
          name: '19',
          y: ${t18},
        },{
          name: '20',
          y: ${t19},
        },{
          name: '21',
          y: ${t20},
        },{
          name: '22',
          y: ${t21},
        },{
          name: '23',
          y: ${t22},
        },{
          name: '24',
          y: ${t23},
        },
          {
            name: '25',
            y: ${t24},
          },{
            name: '26',
            y: ${t25},
          },{
            name: '27',
            y: ${t26},
          },
          {
            name: '28',
            y: ${t27},
          },{
            name: '29',
            y: ${t28},
          },{
            name: '30',
            y: ${t29},
          },{
            name: '31',
            y: ${t30},
          }
        ]
      }],
    });
  });
</script>
<div id="monthChart" style="min-width: 100%; height:100px; margin: 0; padding-bottom: 5px;"></div>
