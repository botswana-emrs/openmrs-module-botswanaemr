<script type="text/javascript">
    jQuery(function () {
        jQuery('#pharmacyHoldRequestsTable a').click(function () {

            let orderId = jQuery(this).parents('tr').find("input[name^=holdOrderId]").val();
            let action = stringUtils.toSentenceCase(jQuery(this).prop("class"));

            jQuery('#selectedOrderId').val(orderId);
            jQuery('#selectedAction').val(action);
            jQuery('#acceptRejectViewModal').find('.modal-title').text('Confirm ' + action + '!');
            jQuery('#acceptRejectViewModal').find('.modal-body').text('If you submit, the hold request will be ' + action + 'ed');

        });

        jQuery('#acceptRejectForm').submit(function (e) {
            e.preventDefault();
            let orderId = jQuery('#selectedOrderId').val();
            let action = jQuery('#selectedAction').val();
            let parameters = {
                'orderId': orderId,
                'action': action
            };

            jQuery.getJSON('${ ui.actionLink("botswanaemr", "search", "approveHoldRequest")}', parameters)
                .done(function (data) {
                    if (data.status.indexOf('200') > -1) {
                        jq().toastmessage('showNoticeToast', "Hold Successful");
                    }

                    if (data.status.indexOf('500') > -1) {
                        jq().toastmessage('showErrorToast', "Hold operation was NOT successful");
                    }

                    location.reload();
                });

            return false;
        });
    })
</script>
<% if (holdDrugOrder) { %>
<div class="table-responsive">
    <div class="row">
        <div class="col col-md-4"><h4>Request Hold</h4></div>
    </div>
    <table id="pharmacyHoldRequestsTable"
           class="table table-striped table-sm table-borderless" style="width:100%">
        <thead>
        <tr>
            <th>Facility from</th>
            <th>Facility to</th>
            <th>Drug</th>
            <th>Dosage</th>
            <th>Duration</th>
            <th>Refill repeat</th>
            <th>Frequency</th>
            <th>Route</th>
            <th>Status</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <% holdDrugOrder.each { %>
        <tr>
            <td>${it.requestingFacility}</td>
            <td>${it.holdingFacility}</td>
            <td>${it.drug}</td>
            <td>${it.dosage}</td>
            <td>${it.refillDuration}</td>
            <% if (it.fillCount > 0) { %>
            <td>${it.fillCount}/${it.totalRefills}</td>
            <% } else { %>
            <td>${it.refillRepeat}</td>
            <% } %>
            <td>${it.frequency}</td>
            <td>${it.route}</td>
            <td class="text-warning text-sm-left">${it.status}</td>
            <% if (!it.requestingFacility.equals(it.currentFacility)) { %>
            <td class="text-primary text-sm-left">
                <% if (it.status == 'Hold requested') { %>
                <a href="#" class="accept" data-toggle="modal" data-target="#acceptRejectViewModal">Accept</a> |
                <a href="#" class="reject" data-toggle="modal" data-target="#acceptRejectViewModal">Reject</a>
                <input type="hidden" name="holdOrderId[]" value="${it.orderId}">
                <% } %>
            </td>
            <% } else { %>
            <td></td>
            <% } %>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>

<div class="modal fade" id="acceptRejectViewModal" tabindex="-1"
     data-controls-modal="acceptRejectViewModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="acceptRejectViewModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-md" role="document">
        <div class="modal-content">
            <form name="acceptRejectForm" id="acceptRejectForm">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="acceptRejectViewModalTitle">Accept/Reject</h4>
                    <hr/>
                    <button type="button" id="viewOrderDetailsView" class="btn btn-sm btn-primary" data-dismiss="modal"
                            aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <input type="hidden" id="selectedOrderId"/>
                <input type="hidden" id="selectedAction"/>

                <div class="modal-body">
                    Confirm ?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn " data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary float-right ml-1" id="action">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<% } %>
<!--
<div class="modal fade" id="viewRelatedOrdersModal" tabindex="-1"
     data-controls-modal="viewRelatedOrdersModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="viewRelatedOrdersModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="viewRelatedOrdersModalTitle">Already filled Prescription</h4>
                <hr/>
                <button type="button" id="viewOrderDetails" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <input type="hidden" id="orderDetailsIdToView" name="orderDetailsIdToView"/>

            <div class="modal-body">

                <table id="filledDetails" style="width:100%">
                    <thead>
                    <tr>
                        <th>Drug</th>
                        <th>Date and time filled</th>
                        <th>Refill</th>
                        <th>Facility</th>
                        <th>Pharmacist</th>
                    </tr>
                    </thead>
                    <tbody id="orderDetailsFilledView"></tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="operationsRefModal" tabindex="-1"
     data-controls-modal="operationsRefModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="operationsRefModalTitle">

    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="operationsRefModalTitle">Filled Prescription</h4>
                <hr/>
                <button type="button" id="operationsRefId" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <input type="hidden" id="operationsRefModalItemValue" name="operationsRefModalItemValue"/>
            <input type="hidden" id="operationsRefModalItemOrderId" name="operationsRefModalItemOrderId"/>

            <div class="modal-body">
                <p>Check Availability</p>
                <table width="100%">
                    <tr>
                        <td>Drug:<span id="drug"></span></td>
                        <td>Dosage:<span id="dosage"></span></td>
                        <td>Duration:<span id="refillDuration"></span></td>
                        <td>Refill Repeat:<span id="refillRepeat"></span></td>
                        <td>Frequency:<span id="frequency"></span></td>
                        <td>Route:<span id="route"></span></td>
                    </tr>
                </table>
                <hr/>
                <table id="fillItemsFromStores" style="width:100%">
                    <thead>
                    <tr>
                        <th>Facility</th>
                        <th>Drug</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="itemDetailsFromStores"></tbody>
                </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
-->

<script>
    jQuery(function () {
        <!--
                jQuery("#prescriptionModal").appendTo("body");
                jQuery("#viewRelatedOrdersModal").appendTo("body");
        -->
        jQuery("#acceptRejectViewModal").appendTo("body");
        <!--
                jQuery("#operationsRefModal").appendTo("body");
        -->
    });
</script>


