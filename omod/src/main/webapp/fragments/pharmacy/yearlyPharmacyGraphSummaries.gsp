<script type="text/javascript">

  jQuery(function () {
    jQuery('#yearlyChart').highcharts({
      credits: {
        enabled: false
      },
      chart: {
        type: 'column'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category'
      },
      yAxis: {
        title: {
          text: 'Yearly workload'
        }
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: '{point.y:.0f}'
          }
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
      },
      series: [{
        name: 'Statistics',
        colorByPoint: true,
        data: [{
          name: '01',
          y:${t0},
        }, {
          name: '02',
          y: ${t1},
        }, {
          name: '03',
          y: ${t2},
        }, {
          name: '04',
          y: ${t3},
        }, {
          name: '05',
          y: ${t4},
        }, {
          name: '06',
          y: ${t5},
        },{
          name: '07',
          y: ${t6},
        },{
          name: '08',
          y: ${t7},
        },{
          name: '09',
          y: ${t8},
        },{
          name: '10',
          y: ${t9},
        },{
          name: '11',
          y: ${t10},
        },{
          name: '12',
          y: ${t11},
        }
        ]
      }],
    });
  });
</script>
<div id="yearlyChart" style="min-width: 100%; height:100px; margin: 0; padding-bottom: 5px;"></div>
