
<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
        ${ui.includeFragment("botswanaemr", "patient/minimalPatientHeader", [patient: patient])}
    </div>
</div>

<div class="row">
<div class="card table-responsive"> Patient Prescriptions
    <table id="pastPrescriptionsTable"
           class="table table-striped table-sm table-bordered mt-1" style="width:100%">
        <thead>
        <tr>
            <th>Drug</th>
            <th>Dosage</th>
            <th>Duration</th>
            <th>Refill repeat</th>
            <th>Frequency</th>
            <th>Route</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <% drugOrders.each { %>
        <tr>
            <td>${it.drug}</td>
            <td>${it.dosage}</td>
            <td>${it.refillDuration}</td>
            <% if (it.fillCount > 0) { %>
            <td>${it.fillCount}/${it.totalRefills}</td>
            <% } else { %>
            <td>${it.refillRepeat}</td>
            <% } %>
            <td>${it.frequency}</td>
            <td>${it.route}</td>
            <td style="display: ruby-text;">
                <% if (it.fillCount.toInteger() >= 0 && it.refillRepeat.toInteger() >= 0 && it.fillCount.toInteger() != it.refillRepeat.toInteger()) { %>
                <span class="badge badge-dark">Filled</span>
                <% } else { %>
                <span class="badge badge-dark">Not Filled</span>
                <% } %>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
</div>