<script type="text/javascript">

    let district = "${patient.patient.person.personAddress?.address2}";
    let city = "${patient.patient.person.personAddress?.cityVillage}";
    let phoneInstances = {};

    function setIdText(){
        let text = "ID number";
        if (patientIdType === "passportNumber"){
            text = "Passport Number"
        } else if (patientIdType === "birthCertificateNumber"){
            text = "Birth Certificate Number"
        }
        jq("#idNumberLabelText").html(text);
    }

    function selectIdType(targetId) {
        if (targetId === "btnIdType" && patientIdType !== "nationalIdNumber") {
            jq("#btnIdType").addClass("outline-button-active");
            jq("#btnPassportType").removeClass("outline-button-active");
            jq("#btnBirthCertificateType").removeClass("outline-button-active");
            patientIdType = "nationalIdNumber";
            resetPatientIdError();
        } else if (targetId === "btnPassportType" && patientIdType !== "passportNumber") {
            jq("#btnPassportType").addClass("outline-button-active");
            jq("#btnIdType").removeClass("outline-button-active");
            jq("#btnBirthCertificateType").removeClass("outline-button-active");
            patientIdType = "passportNumber";
            genderValid = true;
            resetPatientIdError();
        } else if (targetId === "btnBirthCertificateType" && patientIdType !== "birthCertificateNumber") {
            jq("#btnBirthCertificateType").addClass("outline-button-active");
            jq("#btnIdType").removeClass("outline-button-active");
            jq("#btnPassportType").removeClass("outline-button-active");
            patientIdType = "birthCertificateNumber"
            genderValid = true;
            resetPatientIdError();
        } else {
            jq("#btnIdType").removeClass("outline-button-active");
            jq("#btnPassportType").removeClass("outline-button-active");
            jq("#btnBirthCertificateType").removeClass("outline-button-active");
            patientIdType = "";
            genderValid = true;
            resetPatientIdError();
        }
        setIdText();
        if (patientIdType !== "passportNumber") {
            jq("#idNumber").prop("type", "number");
        } else {
            jq("#idNumber").prop("type", "text");
        }
        patientIdType === "" ? jq('#idTypeError').show() : jq('#idTypeError').hide();
        jq("#nokIdTypeHolder").val(patientIdType);

    }

    jq(function () {
        jq("#nokPatientIdError").hide();
        jq("#nokSpinner").hide();

        const SEARCH_REGISTRY_TEXT = "Searching registry..."
        const SEARCH_LOCAL_DATABASE_TEXT = "Searching local database..."
        const NOK_ID_NOT_FOUND = "ID not found in registry. Register next of kin manually.";
        const ID_LOCAL_SEARCH_FOUND = "The Patient is already registered in the local database.";
        const INVALID_BIRTH_NUMBER = "Invalid Birth Number. Should be in the format XX00/00000000/0000"
        const INVALID_BIRTH_NUMBER_YEAR = "Birth year can not be more than the current year";
        const MAX_PATIENT_CONTACTS = 2;
        const MAX_NEXT_OF_KINS = 5;

        if (patientIdType !== "passportNumber") {
            jq("#idNumber").prop("type", "number");
        } else {
            jq("#idNumber").prop("type", "text");
        }

        function selectIdType(targetId) {
            if (targetId === "btnIdType" && patientIdType !== "nationalIdNumber") {
                jq("#btnIdType").addClass("outline-button-active");
                jq("#btnPassportType").removeClass("outline-button-active");
                jq("#btnBirthCertificateType").removeClass("outline-button-active");
                patientIdType = "nationalIdNumber";
                resetPatientIdError();
            } else if (targetId === "btnPassportType" && patientIdType !== "passportNumber") {
                jq("#btnPassportType").addClass("outline-button-active");
                jq("#btnIdType").removeClass("outline-button-active");
                jq("#btnBirthCertificateType").removeClass("outline-button-active");
                patientIdType = "passportNumber";
                genderValid = true;
                resetPatientIdError();
            } else if (targetId === "btnBirthCertificateType" && patientIdType !== "birthCertificateNumber") {
                jq("#btnBirthCertificateType").addClass("outline-button-active");
                jq("#btnIdType").removeClass("outline-button-active");
                jq("#btnPassportType").removeClass("outline-button-active");
                patientIdType = "birthCertificateNumber"
                genderValid = true;
                resetPatientIdError();
            } else {
                jq("#btnIdType").removeClass("outline-button-active");
                jq("#btnPassportType").removeClass("outline-button-active");
                jq("#btnBirthCertificateType").removeClass("outline-button-active");
                patientIdType = "";
                genderValid = true;
                resetPatientIdError();
            }
            if (patientIdType !== "passportNumber") {
                jq("#idNumber").prop("type", "number");
            } else {
                jq("#idNumber").prop("type", "text");
            }
            patientIdType === "" ? jq('#idTypeError').show() : jq('#idTypeError').hide();
            jq("#nokIdTypeHolder").val(patientIdType);

        }

        function resetNokInputs() {
            jq("#nokGivenName").val('');
            jq("#nokFamilyName").val('');
            jq("#nokMiddleName").val('');

            jq("#nokGivenName").prop("disabled", false);
            jq("#nokFamilyName").prop("disabled", false);
            jq("#nokMiddleName").prop("disabled", false);

            jq("#nokEmail").prop("disabled", false);
            jq("#nokPhoneNumber").prop("disabled", false);
        }

        function checkLocalRegistration(idType, idValue) {
            const url = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/getPatientsByIdentifier.action';
            let params = {
                idType: idType,
                idValue: idValue
            }
            jq("#nokSpinner").show();
            jq("#nokPatientIdError").text(SEARCH_LOCAL_DATABASE_TEXT);
            jq("#nokPatientIdError").show();

            return jq.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                data: params
            });
        }

        function extractNames(fullName){
            let array = fullName.split(" ");
            if (array.length === 1){
                setInputVal("#nokGivenName", array[0]);
            }else if (array.length === 2 ) {
                setInputVal("#nokGivenName", array[0]);
                setInputVal("#nokFamilyName", array[1]);
            }else if(array.length > 2) {
                setInputVal("#nokGivenName", array[0]);
                setInputVal("#nokFamilyName", array[array.length -1]);
                setInputVal("#nokMiddleName", array[1]);
            }
        }

        function searchNokInRegistry(idValue) {
            jq("#nokSpinner").show();
            jq("#nokPatientIdError").text(SEARCH_REGISTRY_TEXT);
            jq("#nokPatientIdError").show();

            const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/' + idValue;

            jq.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                timeout: 5000,
                success: function (response) {
                    if (response.status.indexOf('200') > -1) {
                        jq("#nokPatientIdError").hide();
                        if (response.response.length > 0) {
                            const nokDetails = response.response[0];

                            if (nokDetails.fullName !== "") {
                                extractNames(nokDetails.fullName)
                            }
                        }
                    } else {
                        resetNokInputs();
                        jq("#nokPatientIdError").text(NOK_ID_NOT_FOUND);
                        jq("#nokPatientIdError").show();
                        jq("#nokSpinner").hide();
                    }
                    jq("#nokSpinner").hide();
                },
                error: function (response) {
                    resetNokInputs();
                    jq("#nokPatientIdError").text(NOK_ID_NOT_FOUND);
                    jq("#nokPatientIdError").show();
                    jq("#nokSpinner").hide();
                }
            });
        }

        function setCityAutocomplete(element, districtElemId){
        element.autocomplete({
            source: function(request, response) {
                jq.ajax({
                    url: '/' + OPENMRS_CONTEXT_PATH + '/module/addresshierarchy/ajax/getPossibleAddressHierarchyEntriesWithParents.form',
                    type: "GET",
                    dataType: "json",
                    data: {
                        searchString: request.term,
                        addressField: defaultCityAddressField,
                        limit: 20
                    },
                    success: function (data) {
                        let district = jq("#"+ districtElemId).val();
                        let results = [];
                        jq.each( data, function( key, value ) {
                            let result = {
                                label: value.name,
                                value: value.name,
                                parent: value.parent.name
                            };
                            if (!district) {
                                results.push(result);
                            } else if (district === value.parent.name) {
                                results.push(result);
                            }
                        });
                        response(results);
                    },
                    error: function (response) {
                        console.log("response");
                    }
                });
            },
            select: function(event, ui) {
                if (!jq("#" + districtElemId).val()) {
                    jq("#" + districtElemId).val(ui.item.parent);
                }
            },
        });
    }

        function setInputVal(id, val) {
            jq(id).val(val);
            jq(id).prop("disabled", true);
        }

        function setInputValEditable(id, val) {
            jq(id).val(val);
            jq(id).prop("disabled", false);
        }

        function resetPatientIdError() {
            jq("#nokPatientIdError").text('');
            jq("#nokPatientIdError").hide();
        }

        jq("#idNumber").on("keyup", function () {
            let idValue = jq(this).val();

            if (patientIdType !== "" && idValue.length >= 9) {
                let getRegisteredStatus = checkLocalRegistration(patientIdType, idValue);

                getRegisteredStatus.then(function (data) {
                    if (data.patientId > 0) {
                        resetPatientIdError();
                        nokRelationShips['0'] = {
                            "patientId": data.patientId
                        }

                        // Local patient found
                        jq("#nokPatientIdError").hide();
                        if (data.fullName !== "") {
                            extractNames(data.fullName);
                        }
                        if (data.email !== "") {
                            setInputValEditable("#nokEmail", data.email);
                        }
                        if (data.phoneNumber !== "") {
                            setInputValEditable("#nokPhoneNumber", data.phoneNumber);
                        }
                        jq("#nokSpinner").hide();
                    } else {
                        // Local ID not found.Search in registry
                        searchNokInRegistry(idValue);
                    }
                })
                    .fail(function (error) {
                        console.log(error);
                    });

            } else if (patientIdType === "") {
                jq('#idTypeError').show();
            }
        });

        jq("#btnAddNokContact").on("click", function (e) {
            e.preventDefault();
            let len = jq('.extraNokContact').length + 1;

            if (len < MAX_NEXT_OF_KINS) {
                let contactIds = [];
                jq(".extraNokContact").find("input").each(function(){ contactIds.push(this.id); });
                contactIds.sort((a, b) => a.localeCompare(b));

                let contactIndex = '';
                for (let i =2; i<= 6; i++){
                    let temp = "nokPhoneNumber" + i;
                    if (!contactIds.includes(temp)) {
                        contactIndex = i;
                        break;
                    }
                }

                let template = jq(".nok-contact-template").children().clone(true, true);

                template.find('[name=extraPhone]')[0].name = "contact" + contactIndex;
                template.find('[id=extraPhone]')[0].id = "nokPhoneNumber" + contactIndex;
                template.find('[id=extraDelete]')[0].id = "deletePhone" + contactIndex;
                jq(template.find('[id=extraPhoneError]')[0]).attr("for", "nokPhoneNumber" + contactIndex);
                template.find('[id=extraPhoneError]')[0].id = "nokPhoneNumber" + contactIndex + "-error";

                jq('<div/>', {
                    'class': 'extraNokContact',
                    'id': 'extraNokContact',
                    html: template
                }).hide().appendTo('#extraNokContacts').slideDown('slow');
                jq("#nokPhoneNumber" + contactIndex).attr("maxlength", "8");

                let input = jQuery("#nokPhoneNumber" + contactIndex);
                input.intlTelInput({
                    initialCountry: "bw",
                });

                phoneInstances["nokPhoneNumber" + contactIndex] = input;
                jq("#nokPhoneNumber" + contactIndex).on("countrychange", function () {
                    let code = phoneInstances["nokPhoneNumber" + contactIndex].intlTelInput('getSelectedCountryData').dialCode;
                    if (code === '267') {
                        jq("#nokPhoneNumber" + contactIndex).attr("maxlength", "8");
                    } else {
                        jq("#nokPhoneNumber" + contactIndex).attr("maxlength", "15");
                    }
                });
            }
        });

        jq('#extraNokContacts').on("click", function (e) {
            const element = e.target.nodeName;

            // Delete icon clicked
            if (element.toLowerCase() === "i") {
                jq(e.target).closest('#extraNokContact').remove();
            }
        });

        jq("#btnNokAddress").on("click", function (e) {
            e.preventDefault();
            if (district && district !== "null") {
                jq("#nokDistrict").val(district);
            }
            if (city && city !== "null") {
                jq("#nokCity").val(city);
            }
        });

        setCityAutocomplete(jq("#nokCity"), "nokDistrict");

        jq("#nokDistrict").on("change", function () {
            jq("#nokCity").val('');
        });


        function getTelNumberWithCode(elemId) {
            let val = jq("#" + elemId).val();
            if (val) {
                let code = phoneInstances[elemId].intlTelInput('getSelectedCountryData').dialCode;
                val = code + " " + val.replace(" ", '');
                return val;
            }
            return "";
        }

        function getFormData() {
            let formData = [];
            jq("#editNexOfKinForm").find("input, select, textarea").map(function () {
                let elemId = jq(this).attr("id");
                if (elemId.startsWith("nokPhoneNumber")) {
                    let val = getTelNumberWithCode(elemId);
                    formData.push({
                        "value": val,
                        "uuid": jq(this).attr("data-overlay"),
                        "name": jq(this).attr("name"),
                    });
                } else {
                    formData.push({
                        "value": jq(this).val(),
                        "uuid": jq(this).attr("data-overlay"),
                        "name": jq(this).attr("name"),
                    });
                }
            });
            jq('#nokContactPersonType  input[type=checkbox]').each(function () {
                if (jq(this).is(":checked")) {
                    let selectedVal = jq(this).attr("id");
                    formData.push({
                        "value": selectedVal,
                        "uuid": jq("#nokContactPersonType").attr("data-overlay"),
                        "name": "contactType"
                    });
                }
            });
            return formData;
        }

        jQuery.validator.addMethod("validate_email", function (value, element) {
            if (value) {
                if (validateEmail(value)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }, "Please enter a valid Email.");

        jQuery.validator.addMethod("validate_id_number", function (value, element) {
            if (!value) {
                return true;
            }
            if (patientIdType !== "passportNumber") {
                const genderVal = value.charAt(4);
                return !(genderVal < 1 || genderVal > 2);
            } else {
                return true;
            }
        }, "Invalid ID number. The fifth number ID should be 1 or 2");

        jQuery('#editNexOfKinForm').validate({
            rules: {
                'email': {
                    validate_email: true
                },
                "idNumber": {
                    validate_id_number: true
                }
            }
        });

        function setValue(elem) {
            let code = "";
            let input = jQuery("#" + elem);
            input.intlTelInput({
                initialCountry: "bw",
            });

            phoneInstances[elem] = input;
            let val = jq("#" + elem).val();
            if (val) {
                code = jq("#" + elem).attr("code");
                if (code) {
                    code = "+" + code;
                }

                input.intlTelInput('setNumber', code + jq("#" + elem).val());
                jq("#" + elem).val(val);
            }
        }

        jq(window).on('shown.bs.modal', function () {
            setValue("nokPhoneNumber");
            setValue("nokPhoneNumber2");
            setValue("nokPhoneNumber3");
            setValue("nokPhoneNumber4");
            setValue("nokPhoneNumber5");

            jq("#nokPhoneNumber").on("countrychange", function () {
                let code = phoneInstances["nokPhoneNumber"].intlTelInput('getSelectedCountryData').dialCode;
                if (code === '267') {
                    jq("#nokPhoneNumber").attr("maxlength", "8");
                } else {
                    jq("#nokPhoneNumber").attr("maxlength", "15");
                }
            });
        });

        jq("#editNexOfKinForm").submit(function (e) {
            e.preventDefault();
            let valid = jQuery("#editNexOfKinForm").valid();
            if (!valid) {
                return false;
            }

            const UPDATE_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/update-nok-attributes/" + '${patient.patient.uuid}';
            let formData = {"attributes": getFormData()}

            let settings = {
                "url": UPDATE_URL,
                "method": "POST",
                "timeout": 8000,
                "headers": {
                    "Content-Type": "application/json",
                },
                "data": JSON.stringify(formData),
            };

            jq.ajax(settings).done(function (response) {
                location.reload();
            }).fail(function (response) {
                alert("Failed: " + response.statusText);
            });

        });

        let initialialiseNokPhoneNumber = function () {
            let nokPhoneNumber = jQuery("#nokPhoneNumber");
            nokPhoneNumber.intlTelInput({
                initialCountry: "bw",
            });

            phoneInstances["nokPhoneNumber"] = nokPhoneNumber;
        }

        initialialiseNokPhoneNumber();
    });

</script>
<hr>

<div ng-init="getPatientNextOfKins('${patient.patient.id}')">
    <div class="row">
        <div class="col col-sm-10 col-md-10 col-lg-11 pr-0">
            <h4>Contact Person Details</h4>
        </div>

        <div class="col col-sm-12 col-md-12 pr-0 text-right">
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#nextOfKinModal"
                    ng-click="addNextOfKin()">ADD</button>
        </div>
    </div>

    <div class="row">
        <div class="col-12 px-0">
            <ul class="list-group" ng-repeat="nokAttributeMap in personNokAttributes">
                <div class="card">
                    <div class="col text-right">
                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                                data-target="#nextOfKinModal"
                                ng-click="setAttributeToEdit(nokAttributeMap)">EDIT</button>
                    </div>
                    <li class="list-group-item" ng-if="nokAttributeMap.idType">
                        <div class="row">
                            <div class="col">
                                <span>ID Type:</span>
                            </div>
                            <div class="col">
                                {{ nokAttributeMap.idType.value === 'nationalIdNumber' ? 'First Next of Kin ID' : nokAttributeMap.idType.value }}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" ng-if="nokAttributeMap.idNumber">
                        <div class="row">
                            <div class="col">
                                <span>ID Number:</span>
                            </div>
                            <div class="col" ng-if="nokAttributeMap.idNumber.value">
                                {{nokAttributeMap.idNumber.value}}
                            </div>
                            <div class="col" ng-if="!nokAttributeMap.idNumber.value">
                                N/A
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col">
                                <span>Full Name:</span>
                            </div>

                            <div class="col">
                                {{nokAttributeMap.givenName}}  {{nokAttributeMap.middleName}} {{nokAttributeMap.familyName}}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" ng-if="nokAttributeMap.relationShip">
                        <div class="row">
                            <div class="col">
                                <span>Relationship:</span>
                            </div>
                            <div class="col">
                                {{nokAttributeMap.relationShip.value}}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" ng-if="nokAttributeMap.district">
                        <div class="row">
                            <div class="col">
                                <span>District:</span>
                            </div>
                            <div class="col">
                                {{nokAttributeMap.district}}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" ng-if="nokAttributeMap.city">
                        <div class="row">
                            <div class="col">
                                <span>City:</span>
                            </div>
                            <div class="col">
                                {{nokAttributeMap.city}}
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item" ng-repeat="(key, value) in nokAttributeMap"
                        ng-show="value.value && key.includes('contact')">
                        <div class="row">
                            <div class="col">
                                <span>{{ toSentenceCase(key) }}:</span>
                            </div>

                            <div class="col">
                                {{ value.value === 'contactPersonType' ? 'Contact Person' : value.value }}
                            </div>
                        </div>
                    </li>
                </div>
            </ul>
        </div>
        <hr/>
        <!-- Modal -->
        <div class="modal fade"
             id="nextOfKinModal" tabindex="-1"
             data-backdrop="static"
             data-keyboard="false"
             role="dialog"
             aria-hidden="true"
             aria-labelledby="nextOfKinModalTitle">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title text-white" id="nextOfKinModalTitle">Contact Person Details</h4>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="modal-body">
                        <form id="editNexOfKinForm">
                            <div id="nexOfKin">
                                <div class="form-group">
                                    <span ng-show="maxNok" class="error">More than 5 Next of Kins not supported!</span>
                                    <input id="nokUuid" type="hidden" class="form-control" value="{{nokUuid}}"
                                           name="nokUuid"/>
                                    <input id="nokIdTypeHolder" type="hidden" class="form-control"
                                           value={{idType.value}} name={{idType.name}} data-overlay={{idType.uuid}} />

                                    <div class="form-group nokContactPersonType" id="nokContactPersonType" data-overlay={{contactType.uuid}}>
                                        <label>Select Contact Person type <span class="text-danger">*</span></label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="nextOfKinType" name="nokContactPersonType"
                                                   ng-checked="contactType.value === 'nextOfKinType' ">
                                            <label class="form-check-label" for="nextOfKinType">
                                                Next of Kin : With decision making rights on the health of the patient.
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="contactPersonType" name="nokContactPersonType"
                                                   ng-checked="contactType.value === 'contactPersonType' ">
                                            <label class="form-check-label" for="contactPersonType">
                                                Contact Person : A person to contact in relation with the patient
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="treatmentBuddyType" name="nokContactPersonType"
                                                   ng-checked="contactType.value === 'treatmentBuddyType' ">
                                            <label class="form-check-label" for="treatmentBuddyType">
                                                Treatment Buddy : A person to be contacted for a particular program issue
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="unspecifiedPersonType" name="nokContactPersonType"
                                                   ng-checked="contactType.value === 'unspecifiedPersonType' ">
                                            <label class="form-check-label" for="unspecifiedPersonType">
                                                Unspecified : Unspecified contact person
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Select identification type</label>

                                        <div class="btn-group inline-button-group" role="group">
                                            <button type="button" id="btnIdType"
                                                    class="btn btn-outline-secondary outline-button "
                                                    ng-class="{'outline-button-active': idType.value == 'nationalIdNumber'}"
                                                    onclick="selectIdType(this.id)">ID</button>
                                            <button type="button" id="btnPassportType"
                                                    class="btn btn-outline-secondary outline-button"
                                                    ng-class="{'outline-button-active': idType.value == 'passportNumber'}"
                                                    onclick="selectIdType(this.id)">Passport</button>
                                            <button type="button" id="btnBirthCertificateType" disabled
                                                    class="btn btn-outline-secondary outline-button"
                                                    ng-class="{'outline-button-active': idType.value == 'birthCertificateNumber'}"
                                                    onclick="selectIdType(this.id)">Birth Certificate</button>
                                        </div>
                                        <label id="idTypeError"
                                               class="form-text text-danger">Patient identification type is required.</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="idNumber"><span class="black-text" id="idNumberLabelText">ID number</span>
                                    </label>

                                    <div class="form-group is-loading">
                                        <input type="number"
                                               ng-if="idType.value == 'nationalIdNumber'"
                                               class="form-control"
                                               id="idNumber"
                                               name={{idNumber.name}}
                                               value={{idNumber.value}}
                                               data-overlay={{idNumber.uuid}}
                                               ng-model="idNumber.value"
                                               minlength="9"
                                               placeholder="Next of kin Id number"/>
                                        <input type="text"
                                               ng-if="idType.value != 'nationalIdNumber'"
                                               class="form-control"
                                               id="idNumber"
                                               name={{idNumber.name}}
                                               value={{idNumber.value}}
                                               data-overlay={{idNumber.uuid}}
                                               ng-model="idNumber.value"
                                               minlength="9"
                                               placeholder="Next of kin Id number"/>

                                        <div class="spinner-border spinner-section" role="status" id="nokSpinner">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    <label id="nokPatientIdError"
                                           class="form-text text-danger">ID not found in registry. Register next of kin manually.</label>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col pl-0 pr-0">
                                            <label for="nokGivenName">First name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text"
                                                   class="form-control"
                                                   id="nokGivenName"
                                                   name={{givenName.name}}
                                                   aria-describedby="nameHelp"
                                                   placeholder="Next of kin First name"
                                                   value={{givenName.value}}
                                                   required/>
                                        </div>

                                        <div class="col">
                                            <label for="nokMiddleName">Middle name
                                            </label>
                                            <input type="text"
                                                   class="form-control"
                                                   id="nokMiddleName"
                                                   name={{middleName.name}}
                                                   aria-describedby="nameHelp"
                                                   placeholder="Next of kin Middle name"
                                                   value={{middleName.value}}
                                                   />
                                        </div>

                                        <div class="col pr-0 pl-0">
                                            <label for="nokFamilyName">Last name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text"
                                                   class="form-control"
                                                   id="nokFamilyName"
                                                   name={{familyName.name}}
                                                   aria-describedby="nameHelp"
                                                   placeholder="Next of kin Last name"
                                                   value={{familyName.value}}
                                                   required/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nokRelationship">Relationship
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select class="form-control" id="nokRelationship" name={{relationship.name}}
                                            data-overlay={{relationship.uuid}}>
                                        <option>{{relationship.value}}</option>
                                        <%  relationships.each{ %>
                                        <option value="${it}" ng-selected="relationship.value === '${it}' ">${it}</option>
                                        <% } %>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nokEmail">Email</label>
                                    <input type="email"
                                           class="form-control"
                                           id="nokEmail"
                                           name={{email.name}}
                                           value={{email.value}}
                                           data-overlay={{email.uuid}}
                                           placeholder="Next of kin email address"/>
                                </div>

                                <div class="form-group">
                                    <label for="nokPhoneNumber">Contact number
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="tel"
                                           class="form-control"
                                           id="nokPhoneNumber"
                                           name={{contact1.name}}
                                           placeholder="Next of kin contact"
                                           value={{contact1.value}}
                                           data-overlay={{contact1.uuid}}
                                           code="{{code1}}"
                                           minlength="7"
                                           maxlength="8"
                                           oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                           oninput="this.setCustomValidity('')"
                                           required/>
                                </div>

                                <div class="nok-contact-template">
                                    <div class="form-group">
                                        <label>Contact number
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="extraPhone"
                                                   name="extraPhone"
                                                   placeholder="Next of kin contact"
                                                   data-overlay=""
                                                   minlength="7"
                                                   maxlength="8"
                                                   oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                                   oninput="this.setCustomValidity('')"
                                            />
                                            <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete"></i>
                                        </div>
                                        <label id="extraPhoneError" class="error" for="extraPhone"></label>
                                    </div>
                                </div>

                                <div id="extraNokContacts">
                                    <div class="form-group extraNokContact" ng-if="contact2.value">
                                        <label>Contact 2
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="nokPhoneNumber2"
                                                   name={{contact2.name}}
                                                   placeholder="Next of kin contact"
                                                   value={{contact2.value}}
                                                   data-overlay={{contact2.uuid}}
                                                   code="{{code2}}"
                                                   minlength="7"
                                                   maxlength="8"
                                                   oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                                   oninput="this.setCustomValidity('')"
                                            />
                                            <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete2"></i>
                                        </div>
                                        <label id="nokPhoneNumber2-error" class="error" for="nokPhoneNumber2"></label>
                                    </div>

                                    <div class="form-group extraNokContact" ng-if="contact3.value">
                                        <label>Contact 3
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="nokPhoneNumber3"
                                                   name={{contact3.name}}
                                                   placeholder="Next of kin contact"
                                                   value={{contact3.value}}
                                                   data-overlay={{contact3.uuid}}
                                                   code="{{code3}}"
                                                   minlength="7"
                                                   maxlength="8"
                                                   oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                                   oninput="this.setCustomValidity('')"
                                            />
                                            <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete3"></i>
                                        </div>
                                        <label id="nokPhoneNumber3-error" class="error" for="nokPhoneNumber3"></label>
                                    </div>

                                    <div class="form-group extraNokContact" ng-if="contact4.value">
                                        <label>Contact 4
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="nokPhoneNumber4"
                                                   name={{contact4.name}}
                                                   placeholder="Next of kin contact"
                                                   value={{contact4.value}}
                                                   data-overlay={{contact4.uuid}}
                                                   code="{{code4}}"
                                                   minlength="7"
                                                   maxlength="8"
                                                   oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                                   oninput="this.setCustomValidity('')"
                                            />
                                            <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete4"></i>
                                        </div>
                                        <label id="nokPhoneNumber4-error" class="error" for="nokPhoneNumber4"></label>
                                    </div>

                                    <div class="form-group extraNokContact" ng-if="contact5.value">
                                        <label>Contact 5
                                            <span class="text-danger">*</span>
                                        </label>

                                        <div class="text-icon-div">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="nokPhoneNumber5"
                                                   name={{contact5.name}}
                                                   placeholder="Next of kin contact"
                                                   value={{contact5.value}}
                                                   data-overlay={{contact5.uuid}}
                                                   code="{{code5}}"
                                                   minlength="7"
                                                   maxlength="8"
                                                   oninvalid="this.setCustomValidity('Required field and MUST be between 7 and 8 characters long')"
                                                   oninput="this.setCustomValidity('')"
                                            />
                                            <i class="fa fa-trash-o  fa-2x delete-icon" id="extraDelete5"></i>
                                        </div>
                                        <label id="nokPhoneNumber5-error" class="error" for="nokPhoneNumber5"></label>
                                    </div>
                                </div>

                                <button class="dashed-button rounded"
                                        id="btnAddNokContact">+ Add another contact number</button>

                                <div class="text-icon-div">
                                    <h6></h6>
                                    <button class="mini-button rounded" id="btnNokAddress">Copy Patient Address</button>
                                </div>

                                <div class="form-group">
                                    <label for="nokEmail">District</label>

                                    <select type="text"
                                            class="form-control custom-select"
                                            id="nokDistrict"
                                            name="{{district.name}}">
                                        <option selected disabled value="">Select District</option>
                                        <option ng-if="district.value" selected>{{ district.value}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nokEmail">City</label>
                                    <input type="text"
                                           class="form-control"
                                           id="nokCity"
                                           name={{city.name}}
                                           value={{city.value}}
                                           placeholder="City"/>
                                </div>

                                <script type="text/javascript">
                                    jq("#city").autocomplete({
                                        source: cities,
                                        appendTo: "#editNexOfKinForm"
                                    });
                                </script>

                                <div class="row">
                                    <div class="col pr-0">
                                        <div class="col pr-0">
                                            <button type="submit"
                                                    ng-disabled="maxNok"
                                                    class="btn btn-sm btn-primary float-right ml-1">Save
                                            </button>
                                        </div>
                                        <button type="button"
                                                class="btn btn-sm btn-dark bg-dark float-right"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            jQuery(function () {
                jQuery("#nextOfKinModal").appendTo("body");
            });
        </script>
    </div>
</div>
