<div class="form-group">
    <label class="form-inline">
        ${ui.message("Facility Name")}
    </label>
    <select id="facilityLocationField" class="custom-select" name="facilityLocationField">
        <% activeLocations.each { location -> %>
        <option value="${location.uuid}"
            ${ui.encodeHtmlContent(location.uuid == selectedLocationUuid ? ' selected' : '')}>${location.name}</option>
        <% } %>
    </select>

</div>

