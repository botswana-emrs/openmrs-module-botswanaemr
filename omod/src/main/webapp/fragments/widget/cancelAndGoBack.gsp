<script type="text/javascript">
   jQuery(document).ready(function(){
        jQuery('#cancelAndGoBack').click(function(e){
            e.preventDefault();
            urlUtils.redirectToReturnUrl();
        });
   });

</script>
<button class="btn btn-outline border-0 text-danger float-right" id="cancelAndGoBack">
   <i class="fa fa-close text-dark"></i> Go back
</button>
