<style>
    div.alert-md {
        font-size: 0.88em;
    }
</style>

<div id="patient-flags-container" class="row">

</div>

<script type="text/javascript">
    jQuery(document).ready(function(jq) {
        const patientId = ${patient.id}; // Replace with actual patient ID
        const endpoint = `/\${OPENMRS_CONTEXT_PATH}/botswanaemr/search/fetchPatientFlags.action?patientId=\${patientId}`;

        let tooltipOptions = {
            position: {
                my: "center bottom-20",
                at: "center top",
                using: function( position, feedback ) {
                jq( this ).css( position );
                jq( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
                }
            }
        };
        jq('#patient-flags-container').tooltip();

        jq.ajax({
            url: endpoint,
            method: 'GET',
            success: function(data) {
                const container = jq('#patient-flags-container'); // Replace with actual container ID
                container.empty();

                data.results.forEach(flag => {
                    let alertClass = 'alert-success';
                    let iconClass = 'fa-check-circle';

                    if (flag.tag === 'info') {
                        alertClass = 'alert-info';
                        iconClass = 'fa-info-circle';
                    } else if (flag.tag === 'risk') {
                        alertClass = 'alert-warning text-danger';
                        iconClass = 'fa-exclamation-triangle';
                    }

                    const flagHtml = `
                        <div class="col-md-2" data-toggle="tooltip" data-placement="top" title="\${flag.message}">
                            <div class="alert-md p-1 \${alertClass}" role="alert">
                                &nbsp;<i class="fa \${iconClass}"> </i> \${flag.flagName}
                            </div>
                        </div>
                    `;
                    container.append(flagHtml);
                });
                        jq('#patient-flags-container').tooltip('open');

            },
            error: function(error) {
                console.error('Error fetching patient flags:', error);
            }
        });
    });
</script>


<!-- 

Example flags display

<div class="col-md-3">
    <div class="alert-md alert-warning" role="alert">
        <i class="fa fa-exclamation-triangle"></i> Allergic to Penicillin
    </div>
</div>
<div class="col-md-3">
    <div class="alert-md alert-danger" role="alert">
        <i class="fa fa-heartbeat"></i> High Blood Pressure
    </div>
</div>
<div class="col-md-3">
    <div class="alert-md alert-info" role="alert">
        <i class="fa fa-info-circle"></i> Recent Surgery
    </div>
</div>
<div class="col-md-3">
    <div class="alert-md alert-success" role="alert">
        <i class="fa fa-check-circle"></i> Vaccinated for COVID-19
    </div>
</div>
-->
