<script type="text/javascript">
if (typeof showLoadingOverlay === 'undefined') {
    var showLoadingOverlay = function() {
        jq('#saveOverlay').show().delay(7000).fadeOut(400);
    }
}

if (typeof hideLoadingOverlay === 'undefined') {
    var hideLoadingOverlay = function() {
        jq('#saveOverlay').hide();
    }
}
</script>
<style>
.page-overlay {
    position: fixed;
    display: none;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.5);
    z-index: 9999999999;
    cursor: pointer;
}
</style>

<!-- Loading overlay -->
<div class="page-overlay" id="saveOverlay" style="display: none;">
    <div class="container mt-5">
        <div class="row" style="padding: 200px;">
            <div class="col d-flex justify-content-center">
                <img src="${ui.resourceLink('botswanaemr', 'images/loading-spinner.gif')}"
                     width="100px" height="100px"
                     alt="loading..."/>
            </div>
        </div>
    </div>
</div>
