
<li>
    <a href="${ui.pageLink('botswanaemr', 'systemAdministration')}" class="nav_link">
        <div class="nav_icon_container">
            <i class="fa icon-cogs fa-1x"></i>
        </div>
        <div class="nav_label">System Administration</div>
    </a>
</li>
