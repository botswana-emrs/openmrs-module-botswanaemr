<%
    ui.includeJavascript("botswanaemr", "utils/vitals-utils.js")
%>
<style>
.text-separator {
  margin: auto 0.5rem;
}
</style>
<script type="text/javascript">
  jq(function () {
  \$("#editChecksForm").validate({
      rules: {
        esbp: {
          digits: true,
          max: 250,
          min: 0
        },
        edbp: {
          digits: true,
          max: 150,
          min: 0
        },
        etemperature: {
          number: true,
          max: 43,
          min: 25
        },
        eweight: {
          number: true,
          max: 250,
          min: 0
        },
        eheight: {
          number: true,
          max: 272,
          min: 10
        },
        ebmi: {
          number: true
        },
        ebsa: {
          number: true
        },
        errate: {
          number: true,
          max: 99,
          min: 0
        },
        epulse: {
          number: true,
          max: 230,
          min: 0
        },
        eHcircumference: {
          number: true
        }
      }
    });

    function updateBmi() {
      let wt = jq("#eweight").val();
      let ht = jq("#eheight").val();

      let bmi = calculateBmi(wt, ht);

      if (bmi != null && !isNaN(bmi)) {
        jq("#ebmi").val(bmi.toFixed(1));
      } else {
        jq("#ebmi").val('');
      }
    }

    function updateBsa() {
      let wt = jq("#eweight").val();
      let ht = jq("#eheight").val();

      let bsa = calculateBsa(wt, ht);

      if (bsa != null && !isNaN(bsa)) {
        jq("#ebsa").val(bsa.toFixed(2));
      } else {
        jq("#ebsa").val('');
      }
    }

    jq("#eLmp").datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        "setDate": new Date(),
        dateFormat: "dd-M-yy",
        "autoclose": true
    });

    jq("#eweight, #eheight").change(function (e) {
      updateBmi();
      updateBsa()
    });

    jq("#editChecksForm").submit(function (e) {
      e.preventDefault();
      let valid = \$("#editChecksForm").valid();
      if (!valid) {
        return false;
      }
      var vitals = {
          'etemperature': jq('#etemperature').val(),
          'esbp': jq('#esbp').val(),
          'edbp': jq('#edbp').val(),
          'eweight': jq('#eweight').val(),
          'eheight': jq('#eheight').val(),
          'ebmi': jq('#ebmi').val(),
          'ebsa': jq('#ebsa').val(),
          'erRate': jq('#erRate').val(),
          'epulse': jq('#epulse').val(),
          'ehCircumference': jq('#eHCircumference').val(),
          'eGlucoseLevel': jq('#eGlucoseLevel').val(),
          'eOxygenSaturation': jq("#eOxygenSaturation").val(),
          'eLmp': jq("#eLmp").val(),
          'ePregnancyKnown': jq("#ePregnancyKnown").val(),
          'eConscious': jq("#eConscious").val(),
          'eResponsive': jq("#eResponsive").val(),
          'temperatureObsId': jq('#temperatureObsId').val(),
          'systolicBpObsId': jq('#systolicBpObsId').val(),
          'diastolicBpObsId': jq('#diastolicBpObsId').val(),
          'heightObsId': jq('#heightObsId').val(),
          'weightObsId': jq('#weightObsId').val(),
          'bmiObsId': jq('#bmiObsId').val(),
          'bsaObsId': jq('#bsaObsId').val(),
          'rRateObsId': jq('#rRateObsId').val(),
          'pRateObsId': jq('#pRateObsId').val(),
          'headCircumferenceObsId': jq('#headCircumferenceObsId').val(),
          'glucoseLevelObsId': jq('#glucoseLevelObsId').val(),
          'oxygenSaturationObsId': jq('#oxygenSaturationObsId').val(),
          'lmpObsId': jq('#lmpObsId').val(),
          'pregnancyKnownObsId': jq('#pregnancyKnownObsId').val(),
          'consciousObsId': jq('#consciousObsId').val(),
          'responsiveObsId': jq('#responsiveObsId').val(),
          'encounterToEdit': jq('#encounterToEdit').val(),
      };

      jq.getJSON('${ ui.actionLink("botswanaemr", "newChecks", "editCapturedVitals")}', vitals)
          .success(function (data) {
            jq().toastmessage('showNoticeToast', "The vitals have been updated successfully");
            showLoadingOverlay();
            location.reload();
          })
      });
  });
</script>
<% if(hasConsultationToBeEdited) {%>
<form method="post" id="editChecksForm">
    <div class="row">
        <label>Please fill in the details below to capture the patients vitals</label>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="etemperature">Temperature(C)</label>
                <input  id="etemperature"
                        name="etemperature"
                        type="text"
                        value="${temperature}"
                        digits
                        class="form-control"/>
            </div>
        </div>
        <input type="hidden" value="${temperatureObsId}" id="temperatureObsId" name="temperatureObsId">
    </div>

    <div class="row">
        <div class="col">
            <label>Blood pressure (mmHg)</label>
        </div>

        <div class="row">
            <div class="col-md-3 mr-0 pr-0">
                <div class="form-group">
                    <input  id="esbp"
                            name="esbp"
                            type="text"
                            value="${systolicBp}"
                            digits
                            class="form-control"/>
                </div>
                <input type="hidden" value="${systolicBpObsId}" id="systolicBpObsId" name="systolicBpObsId">
            </div>

            <div class="text-separator">
                <span style="position: relative;top: -0.5rem; font-size: 2rem">/</span>
            </div>

            <div class="col-md-3 ml-0 pl-0">
                <input  id="edbp"
                        name="edbp"
                        type="text"
                        value="${diastolicBp}"
                        digits
                        class="form-control"/>
            </div>
            <input type="hidden" value="${diastolicBpObsId}" id="diastolicBpObsId" name="diastolicBpObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="epulse">Pulse(b/m)</label>
                <input  id="epulse"
                        name="epulse"
                        type="text"
                        value="${pRate}"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${pRateObsId}" id="pRateObsId" name="pRateObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eweight">Weight(Kg)</label>
                <input  id="eweight"
                        name="eweight"
                        type="text"
                        value="${weight}"
                        digits
                        class="form-control"/>
            </div>
            <input type="hidden" value="${weightObsId}" id="weightObsId" name="weightObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eheight">Height(cm)</label>
                <input  id="eheight"
                        name="eheight"
                        type="text"
                        value="${height}"
                        digits
                        class="form-control"/>
            </div>
            <input type="hidden" value="${heightObsId}" id="heightObsId" name="heightObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="ebmi">BMI(Kg/M2)</label>
                <input  id="ebmi"
                        name="ebmi"
                        type="text"
                        value="${bmi}"
                        digits
                        readonly="readonly"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${bmiObsId}" id="bmiObsId" name="bmiObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="ebsa">BSA(M2)</label>
                <input  id="ebsa"
                        name="ebsa"
                        type="text"
                        value="${bsa}"
                        digits
                        readonly="readonly"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${bsaObsId}" id="bsaObsId" name="bsaObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="erRate">Respiratory Rate(b/m)</label>
                <input  id="erRate"
                        name="erRate"
                        type="text"
                        value="${rRate}"
                        digits
                        class="form-control"/>
            </div>
            <input type="hidden" value="${rRateObsId}" id="rRateObsId" name="rRateObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eHCircumference">Head Circumference(cm)</label>
                <input  id="eHCircumference"
                        name="eHCircumference"
                        type="text"
                        value="${headCircumference}"
                        digits
                        class="form-control"/>
            </div>
            <input type="hidden" value="${headCircumferenceObsId}" id="headCircumferenceObsId" name="headCircumferenceObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eHCircumference">Glucose Level(mm/gl)</label>
                <input  id="eGlucoseLevel"
                        name="eGlucoseLevel"
                        type="text"
                        value="${glucoseLevel}"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${glucoseLevelObsId}" id="glucoseLevelObsId" name="glucoseLevelObsId">
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eHCircumference">Oxygen Saturation</label>
                <input  id="eOxygenSaturation"
                        name="eOxygenSaturation"
                        type="text"
                        value="${oxygenSaturation}"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${oxygenSaturationObsId}" id="oxygenSaturationObsId" name="oxygenSaturationObsId">
        </div>
    </div>

    <% if (patient.gender == 'F') { %>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="eLmp">LNMP</label>
                <input  id="eLmp"
                        name="eLmp"
                        type="text"
                        value="${lmp}"
                        class="form-control"/>
            </div>
            <input type="hidden" value="${lmpObsId}" id="lmpObsId" name="lmpObsId">
        </div>
    </div>
    <% } %>

    <% if (patient.gender == 'F') { %>
    <div>
        <div class="col">
            <div class="form-group">
                <label for="ePregnancyKnown">Are you pregnant?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="ePregnancyKnown"
                        name="ePregnancyKnown">
                    <option disabled selected value="">Select Status</option>
                    <option value="1065" <% if(pregnancyKnown == "1065") { %> selected <% } %>>Yes</option>
                    <option value="1066" <% if(pregnancyKnown == "1066") { %> selected <% } %>>No</option>
                    <option value="1067" <% if(pregnancyKnown == "1067") { %> selected <% } %>>Unknown</option>
                </select>
                <input type="hidden" value="${pregnancyKnownObsId}" id="pregnancyKnownObsId" name="pregnancyKnownObsId">
            </div>
        </div>
    </div>
    <% } %>

    <div>
        <div class="col">
            <div class="form-group">
                <label for="eConscious">Conscious?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="eConscious"
                        name="eConscious">
                    <option selected disabled value="">Select Status</option>
                    <option value="1065" <% if(conscious == "1065") { %> selected <% } %>>Yes</option>
                    <option value="1066" <% if(conscious == "1066") { %> selected <% } %>>No</option>
                </select>
                <input type="hidden" value="${consciousObsId}" id="consciousObsId" name="consciousObsId">
            </div>
        </div>
    </div>

    <div>
        <div class="col">
            <div class="form-group">
                <label for="eResponsive">Responsive?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="eResponsive"
                        name="eResponsive">
                    <option selected disabled value="">Select Status</option>
                    <option value="1065" <% if(responsive == "1065") { %> selected <% } %>>Yes</option>
                    <option value="1066" <% if(responsive == "1066") { %> selected <% } %>>No</option>
                </select>
                <input type="hidden" value="${responsiveObsId}" id="responsiveObsId" name="responsiveObsId">
            </div>
        </div>
    </div>

    <div class="row button-section-right pr-3">
        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
        <button id="saveEditedVitals" type="submit" class="btn btn-primary float-right ml-1" value="saveEditedVitals"
                name="save">${ui.message("Save")}
        </button>
    </div>
</form>
<%}%>
