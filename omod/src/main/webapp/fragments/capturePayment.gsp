<script type="text/javascript">
    jq = jQuery;
    jq(function() {
        jq("input[type=radio][name=paymentRadio]").change(function (){
            if (jq(this).is(":checked") && jq(this).val() === "1") {
                jq("#payment_method_div").removeClass("hidden");
            } else {
                fieldHelper.clearAllFields("#payment_method_div");
                jq("#payment_method_div").addClass("hidden");
            }

            if (jq(this).is(":checked") && jq(this).val() === "3") {
                jq("#amount").attr("min", 0);
                //jq("#amount").attr("max", 0);
                jq("#amount").val(0);
                jq("#amount").trigger("change");                
            } else {
                //jq("#amount").attr("min", <%=minimumBillableAmount %>);
                //jq("#amount").attr("max", <%=minimumBillableAmount %>);
            }

        });

        jq("#capturePayment").submit(function(e){
            e.preventDefault();
            if (jq("#paymentMade").is(":checked")) {
                if (jq("#paymentMethod").val() === "") {
                    alert("Select payment method");
                    return false;
                }
            }

            e.currentTarget.submit();
        });

        jq("input[type=radio][name=paymentRadio]").trigger("change");

    });
</script>
<form id="capturePayment" method="POST">
    <span style=" color: #008000b5;">
        <i class="fa fa-check-circle  fa-4x"></i>
    </span>
    <h5 class="h5 content-section">Registration Completed</h5>

    <p class="text-center">You have completed the registration of this patient successfully</p>
    <hr/>

    <p class="text-center">Complete the payment information around payment before the patient <br/>
        can proceed to screening and triage or admission
    </p>
    <h6 class="h6 content-section">Payment</h6>

    <p>Please fill out the details around the patients payment</p>


    <div class="form-group">
        <label for="amount">Please enter the amount
            <span class="text-danger">*</span>
        </label>
        <input type="number" class="form-control" id="amount" name="billableAmount" value="<%=minimumBillableAmount%>" min="0" max="500"
               placeholder="Enter the amount BWP" pattern="[0-9]+">
    </div>

    <p>Payment Status</p>

    <div class="radios">
        <input id="paymentMade" name="paymentRadio" type="radio" value="1"
               class="horizontal-margin">
        <label for="paymentMade" class="zero-margin">Payment Made</label>

        <input name="paymentRadio" id="delayPayment" type="radio" class="horizontal-margin" value="2">
        <label for="delayPayment" class="zero-margin">Delay Payment</label>

        <input name="paymentRadio" id="notApplicable" type="radio" class="horizontal-margin" value="3">
        <label for="notApplicable" class="zero-margin">Not applicable</label>
    </div>

    <div class="row" id="payment_method_div">
        <div class="form-group">
            <label for="paymentMethod">Select payment method
                <span class="text-danger">*</span>
            </label>
                <select name="paymentMethod" id="paymentMethod" class="form-control">
                    <option value="">Select One</option>
                    <% paymentMethodList.each { %>
                    <option value="${it.getId()}">
                        ${it.getName()}
                    </option>
                    <% } %>
                </select>
        </div>
    </div>

    <div class="row">
        <input type="hidden" name="paymentCode" value="12345" />
    </div>

    <div class="row">
        <div></div>
    </div>

    <div class="row">
        <button id="btnCapturePayment" type="submit" class="btn btn-primary btn-block horizontal-button">${ui.message("Next")}
        </button>
    </div>

</form>
