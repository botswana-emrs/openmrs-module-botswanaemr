<%
    def defaultEncounterDate = visit ? visit.startDatetime : new Date()
%>

<% if(htmlForm){%>
    <a href="${ui.pageLink("htmlformentryui", "htmlform/enterHtmlFormWithStandardUi", [
            patientId: patient.uuid,
            formUuid: it.form.uuid,
            visitId: currentVisit.uuid,
            returnUrl:returnUrl
    ]
    )}">${it.form.name}</a>
<%}%>
