<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'art/artDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl, 'action': 'self'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'art/artScreeningPool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-search-plus fa-1x"></i>
            </div>
            <div class="nav_label">Patient Pool</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'art/linkagePool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-link fa-1x"></i>
            </div>
            <div class="nav_label">Linkage Pool</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/myAppointments')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Calendar</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'pharmacy/pharmacyAllPrescriptions')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-pencil-square-o fa-1x"></i>
            </div>
            <div class="nav_label">Prescriptions</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'art/regimenManagement', [regimenCategory: 'bd237e86-cf2c-4f50-ba62-01f2167264b7'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-pencil-square fa-1x"></i>
            </div>
            <div class="nav_label">Regimen Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'lab/groupedLaboratoryOrders')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-syringe fa-1x"></i>
            </div>
            <div class="nav_label">Lab Orders</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'adverseEvents')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-heartbeat fa-1x"></i>
            </div>
            <div class="nav_label">ADR</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

