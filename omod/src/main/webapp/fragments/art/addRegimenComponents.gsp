<script type="text/javascript">
    jq(document).ready(function () {
        let simplifiedDrugs = [];

        const drugMap = new Map();
        //getSimplifiedDrugs
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','consultation/drugPrescription','getSimplifiedDrugs')}",
            dataType: "json",
            success: (result) => {
                result.data.forEach((simplifiedDrug) => {
                    simplifiedDrugs.push(simplifiedDrug.name);
                    drugMap.set(simplifiedDrug.name, simplifiedDrug.uuid);
                });
                setAutocomplete(jq('#drugsTable tbody tr:last-child input[name="drug[]"]'));
            }
        });

        let setAutocomplete = function (jqobj) {
            jqobj.autocomplete({
                source : simplifiedDrugs
            });
        }

        let drugsListTemplate = '';
        drugsListTemplate += '<tr>';
        drugsListTemplate += '<td><input type="text" class="form-control" name="drug[]" value=""></td>';
        drugsListTemplate += '<td><input type="text" class="form-control" name="strength[]" value=""></td>';
        drugsListTemplate += '<td><button type="button" class="btn btn-success addRow">Add</button> <button type="button" class="btn btn-danger removeRow">Remove</button></td>';
        drugsListTemplate += '</tr>';


        let loadRegimenComponentDialog = function() {
            jQuery("#addRegimenComponentsModal").modal('show');
        }

        let hideShowActionButtons = function () {
            jq('#drugsTable tbody tr').each(function (index) {
                if (index === 0) {
                    jq(this).find('.removeRow').hide();
                }
                if (index === jq('#drugsTable tbody tr').length - 1) {
                    jq(this).find('.addRow').show();
                    //jq(this).find('.removeRow').hide();
                } else {
                    jq(this).find('.addRow').hide();
                    jq(this).find('.removeRow').show();
                }
            });
        };

        jq("body").on("click", "#addRegimenComponent", function () {
            // reset the form
            jq('#regimenComponentLabel').val('');
            jq('#regimen').val('');
            jq('#regimenComponentId').val('');
            jq('#drugsTable tbody').html('');
            jq('#drugsTable tbody').append(drugsListTemplate);
            hideShowActionButtons();
            setAutocomplete(jq('#drugsTable tbody tr:last-child input[name="drug[]"]'));
            loadRegimenComponentDialog();
        });

        jq("body").on("click", ".addRow", function () {
            jq('#drugsTable tbody').append(drugsListTemplate);
            let drugField = jq('#drugsTable tbody tr:last-child input[name="drug[]"]');
            setAutocomplete(drugField);
            drugField.focus();
            hideShowActionButtons();
        });

        jq("body").on("click", ".removeRow", function () {
            jq(this).closest("tr").remove();
            hideShowActionButtons();
        });
        hideShowActionButtons();

        let saveRegimenComponent = function() {
            let drugs = [];
            let valid = true;
            let regimen = jq("#regimen").val();
            let regimenComponentLabel = jq("#regimenComponentLabel").val();

            if (!regimen || regimen === 'Select regimen') {
                jq().toastmessage('showErrorToast', 'Regimen is required.');
                valid = false;
            }

            if (!regimenComponentLabel) {
                jq().toastmessage('showErrorToast', 'Component Label is required.');
                valid = false;
            }

            jq('#drugsTable tbody tr').each(function () {
                let drug = jq(this).find('input[name="drug[]"]').val();
                let strength = jq(this).find('input[name="strength[]"]').val();
                if (!validateStrength(strength)) {
                    jq().toastmessage('showErrorToast', `Invalid strength format for '\${strength}'. Please enter a valid strength format e.g. '300mg', '200mg/150mg', and '100mg/100mg/100mg'`);
                    // exit  the loop
                    valid = false;
                }
                if (drug && strength) {
                    drugs.push({
                        drug: drugMap.get(drug),
                        strength: strength
                    });
                }
            });

            if (!valid) {
                return;
            }

            let regimenComponentId = jq("#regimenComponentId").val();
            let drugsObj = Object.assign({}, drugs);
            jq.getJSON('${ ui.actionLink("botswanaemr", "art/manageRegimenLines", "saveRegimenComponent") }', {
                regimenComponentId: regimenComponentId,
                regimen: regimen,
                regimenComponentLabel: regimenComponentLabel,
                drugs: JSON.stringify(drugsObj),
            }).done(function(data) {
                jq().toastmessage('showSuccessToast', "Regimen Component created successfully");
                location.reload();
            }).fail(function() {
                jq().toastmessage('showErrorToast', "Something wrong happened while trying to save Regimen Component");
            });
        }

        let addRegimenComponent = function() {
            saveRegimenComponent();
        }

        jq("#saveRegimenComponent").on('click', function () {
            addRegimenComponent();
        });

        let updateRegimenComponentDrugForm = function(data) {
            jq("#regimenComponentLabel").val(data.componentLabel);
            jq("#regimen").val(data.regimenId);
            jq("#regimenComponentId").val(data.regimenComponentId);

            jq('#drugsTable tbody').html('');
            if (data.componentDrugs.length === 0) {
                jq('#drugsTable tbody').append(drugsListTemplate);
                setAutocomplete(jq('#drugsTable tbody tr:last-child input[name="drug[]"]'));
            }

            data.componentDrugs.forEach((drug) => {
                // load template with data
                let html = jq(drugsListTemplate);
                html.find('input[name="drug[]"]').val(drug.drug);
                html.find('input[name="strength[]"]').val(drug.strength);
                jq('#drugsTable tbody').append(html);

                setAutocomplete(jq('#drugsTable tbody tr:last-child input[name="drug[]"]'));
            });

            hideShowActionButtons();
        }

        let getRegimenComponentWithMockData = function(id) {
            let data = {};
            data.regimenComponentId = id;
            data.componentLabel = "Regimen Component 1";
            data.regimenId = 1;
            data.componentDrugs = [];
            data.componentDrugs.push({
                drug: "Lamivudine",
                strength: "300mg"
            });
            data.componentDrugs.push({
                drug: "Tenofovir",
                strength: "300mg"
            });
            data.componentDrugs.push({
                drug: "dolutegravir",
                strength: "50mg"
            });
            updateRegimenComponentDrugForm(data);
            loadRegimenComponentDialog();
        }

        let getRegimenComponent = function(id) {
            jq.getJSON('${ ui.actionLink("botswanaemr", "art/manageRegimenLines", "getRegimenComponent") }', {
                regimenComponentId: id
            }).done(function(retObj) {
                updateRegimenComponentDrugForm(retObj.data);
                loadRegimenComponentDialog();
            }).fail(function(error) {
                    jq().toastmessage('showErrorToast', 'Error occcured while fetching data');                
            });
        }

        jq("body").on("click", ".editRegimenComponent", function () {
            let regimenComponentId = jq(this).data('id');
            getRegimenComponent(regimenComponentId);
            // getRegimenComponentWithMockData(regimenComponentId);
        });

        // When regimen is selected, populate text on the component label
        jq("#regimen").change(function() {
            var selectedRegimen = jq(this).find("option:selected").text();
            if (selectedRegimen) {
                jq("#regimenComponentLabel").val(selectedRegimen);
            } else {
                jq("#regimenComponentLabel").val("");
            }
        });

    });

    let validateStrength = function (strength) {
        // the last part of the regex is the frequency of the dosage contains 2 characters and is not mandatory
        let strengthSplit = strength.split(" ");
        if (strengthSplit.length > 1) {
            let frequencyPattern = /(?:OD|BD|QID|TDS)\$/;

            if (!frequencyPattern.test(strengthSplit[1])) {
                return false;
            }
        } else {
            // This regular expression would match strings like "300mg", "200mg/150mg", and "100mg/100mg/100mg",
            // but it would not match a string like "100mg/100mg/100mg/100mg" (too many groups) or "1000mg" (number too large).
            let strengthPattern = /^([0-9]{1,3}mg\\/?){1,3}/;

            if (!strengthPattern.test(strengthSplit[0])) {
                return false;
            }
        }

        return true;
    }


    /*
  jq(function() {
   jq("#regimenComponentLabel").on("focus.autocomplete", function () {
        jq(this).autocomplete({
            source: function(request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "art/manageRegimenLines", "getRegimenConcepts") }', {
                      query: request.term
                    }).success(function(data) {
                      var results = [];
                      for (var i in data) {
                          var result = {
                            label: data[i].displayString,
                            value: data[i].uuid
                          };
                          results.push(result);
                      }
                      response(results);
                    });
                  },
                  minLength: 3,
                  select: function(event, ui) {
                    event.preventDefault();
                    jq(this).val(ui.item.label);
                  }
        });
    });
  });
  */
</script>

<div>
    <form class="simple-form-ui" id="add-regimen-line-form">
        <div class="col">
            <label class="form-label"
                   for="regimen">${ui.message("Regimen")} <span
                    class="text-danger">*</span></label>
            <Select type="text" class="form-control reason" id="regimen" name="regimen">
                <option>Select regimen</option>
                <% regimens.each { %>
                <option value="${it.regimenId}">${it.regimenName}</option>
                <% } %>
            </select>
        </div>
        <div class="col">
            <input type="hidden" id="regimenComponentId" name="regimenComponentId"/>
            <label class="form-label"
                   for="regimenComponentLabel">${ui.message("Component Label")} <span
                    class="text-danger">*</span></label>
            <input type="text" class="form-control reason" id="regimenComponentLabel" name="regimenComponentLabel"
                   required placeholder="Enter Component Label">
        </div>
        <div class="col mt-2">
            <div class="table-responsive">
                <table class="table" id="drugsTable">
                    <thead>
                    <tr>
                        <th>Drug</th>
                        <th>Strength</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><input type="text" class="form-control" name="drug[]"></td>
                        <td><input type="text" class="form-control" name="strength[]"></td>
                        <td>
                            <button type="button" class="btn btn-success addRow">Add</button>
                            <button type="button" class="btn btn-danger removeRow">Remove</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </form>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
        ${ui.message("Close")}
    </button>
    <button id="saveRegimenComponent"
            type="submit"
            class="btn btn-sm btn-primary float-right float-left ml-1">${ui.message("Save Regimen Component")}
    </button>
</div>
