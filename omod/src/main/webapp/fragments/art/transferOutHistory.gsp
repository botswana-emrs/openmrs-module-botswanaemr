<script type="text/javascript">
    function printContent(el) {
        var restorePage  = jq('body').html();
        var printContent = jq(el).clone();
        jq('body').empty().html(printContent);
        window.print();
        jq('body').html(restorePage);
    }
</script>

<div id="transferHistoryCard" class="card text-center">
    <div class="card-header">
        <h5 class="text-primary text-left mt-3">PATIENT ART TRANSFER HISTORY</h5>
    </div>
    <div class="card-body">
        ${ ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
            visit: visit,
            encounter: transferEncounter,
            patient: patient,
            returnUrl: returnUrl,
            definitionUiResource: definitionUiResource ?: ""
        ])}
    </div>
    <div class="card-footer text-muted">
        <div class="row">
            <div class="col">
                <button id="print" onclick="printContent('#transferHistoryCard');" class="btn btn-primary float-left">
                    <i class="fa fa-print fa-1x"></i> Print
                </button>
            </div>
            <div class="col">

            </div>
        </div>
    </div>
</div>