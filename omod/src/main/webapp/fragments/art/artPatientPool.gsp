<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#screenedBy').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                matchingOptions.push(screenedBy === jq('#screenedBy').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function() {
        jq("body").on('click', '#artPatientQueueListTable a.screen' , function(e) {
            e.preventDefault();
            artUtils.setServiceTypeData(jq(this).data('patientid'), jq(this).data('visitnumber'), jq(this).data('hivstatus'), '#patientId', '#visitId', '#serviceType', '#artServiceTypeModal');
        });
    });

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            data: { sourcePageName: "art/artPatientPool"},
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                var table = jQuery('#artPatientQueueListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'providerNames'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Screened</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if (type === 'display') {
                                    if (row.status == "TREATED") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/programs/programs.page?patientId=' + row.patientId + '&programId=${artProgramId}&visitId=' + row.visitNumber + '" class="text-primary float-left pl-3" id="view">View</a>';
                                    } else {
                                        return '<a class="screen" data-patientId="' + row.patientId + '" data-visitNumber="' + row.visitNumber + '" data-hivStatus="' + data.hivStatus + '" href="#" class="text-primary pl-0" id="screen">Screen</a>&nbsp;|&nbsp;' +
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary pl-0" id="reassign">Reassign</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#artPatientPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });

    function saveServiceType(params) {
        jq.getJSON('${ ui.actionLink("botswanaemr", "art/artPatientPool", "startArtServiceByType")}', params)
            .success(function (data) {
                let artProgramId = `${artProgramId}`
                if (data.status === "Successful") {
                    location.href = "/${ui.contextPath()}/botswanaemr/programs/programs.page?patientId=" + data.patientId + "&programId=" + artProgramId + "&visitId=" + data.visitId;
                } else {
                    jq().toastmessage('showErrorToast', "Error starting art service type");
                }
            });
    }

    function openDialog(visitType, params) {
        let serviceType = jq('#serviceType option:selected').text();
        jq('<div></div>').appendTo('body')
            .html('<div><p>The patient has an active visit of type <b>' + visitType + '. </b> If you proceed, the visit will be closed and a new one of type <b>' + serviceType + '</b> opened"</p></div>')
            .dialog({
                modal: true,
                title: 'Confirmation Required',
                zIndex: 10000,
                autoOpen: true,
                width: 400,
                resizable: false,
                buttons: {
                    "Yes": function () {
                        saveServiceType(params);
                        jq(this).dialog("close");
                    },
                    "Cancel": function () {
                        jq(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    jq(this).remove();
                }
            });
    }

    jq(function () {
        jq("#serviceTypeForm").submit(function (e) {
            e.preventDefault();
            showLoadingOverlay();
            var formData = jq(this).serializeArray();
            let params = {};

            jq.each(formData, function(index, field) {
                params[field.name] = field.value;
            });

            jq.getJSON('${ ui.actionLink("botswanaemr", "art/artPatientPool", "checkPatientActiveVisit")}', params)
                .success(function (data) {
                    let serviceType = jq('#serviceType option:selected').text();
                    if (data.active && data.visitType !== serviceType) {
                        openDialog(data.visitType, params);
                    } else {
                        saveServiceType(params);
                    }
                });


        });
    });

</script>
<style>
    .ui-dialog-titlebar {
        background: #0099CC !important;
        color: #ffffff !important;
        fill: #0099CC !important;
    }
    .ui-dialog-title {
        background-color: transparent !important;
        color: #fff !important;
    }
</style>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Screened by</label>
        <select id="screenedBy"
                class="form-control"
                placeholder="${ui.message('Select nurse ')}">
            <option value="">Select One</option>
            <% providerList.each { provider-> %>
            <% if (provider != null && provider?.person != null && provider?.person?.personName != null) { %>
            <option value="<%=provider.uuid %>"><%=provider.person.personName %></option>
            <% } %>
            <% } %>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="artPatientQueueListTable"
       class="table table-striped table-sm table-bordered">
    <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Sex</th>
            <th>Age</th>
            <th>Screened By</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>

<!-- Edit complaint Nodal -->
<div class="modal fade" id="artServiceTypeModal" tabindex="-1"
     data-controls-modal="artServiceTypeModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="artServiceTypeModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="artServiceTypeModalTitle">Service Type</h4>
                <button type="button" id="serviceTypeBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="serviceTypeForm">
                    <div class="form-group">
                        <input type="hidden" id="visitId" name="visitId" />
                        <input type="hidden" id="patientId" name="patientId" data-hivStatus="" />
                        <label for="serviceType">Service Type</label>
                        <select id="serviceType" placeholder="Select service typel" name="serviceType"
                                class="form-control custom-select" required>
                            <option value="" disabled selected>Select service type</option>
                            <% visitTypes.each { visitType -> %>
                            <option value="${visitType.id}">${visitType.name}</option>
                            <% } %>
                        </select>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#artServiceTypeModal").appendTo("body");
    });
</script>
