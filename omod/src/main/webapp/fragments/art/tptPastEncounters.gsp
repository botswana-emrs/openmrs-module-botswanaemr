<script>
    jQuery(function () {
        jQuery("#dateCreated").on('change', function () {
            let value = jQuery(this).val().toLowerCase().trim();
            let rows =  jQuery("#tptPastEncounters tbody tr");

            rows.each(function () {
                let dateCell = jQuery(this).find("td:first");
                if (!value) {
                    jQuery(this).show();
                    return;
                }

                // Get the date text and trim it for comparison
                let rowDate = dateCell.text().trim().toLowerCase();

                // Comparing dates
                if (rowDate === value) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        });

        // Set today's date initially
        let todayDate = new Date().toISOString().split('T')[0];
        jQuery("#dateCreated").val(todayDate).trigger('change');

        // Reset button functionality
        jQuery('#resetBtn').on('click', () => {
            jQuery('#dateCreated').val('').trigger('change');
        });
    });
</script>
<div class="accordion">
    <div class="panel-group" id="allTests">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title text-primary text-left mt-3 pl-3">TPT PAST ENCOUNTERS</h5>
            </div>
            <div class="panel-body">
                    <div class="panel-search row mb-1">
                        <div class="col-4">
                            <div class="form-group">
                                <input type="text"
                                       class="form-control py-2"
                                       id="dateCreated"
                                       name="dateCreated"
                                       placeholder="YYYY-MM-DD">
                                <script type="text/javascript">
                                    jQuery('#dateCreated').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        showButtonPanel: true,
                                        "setDate": new Date(),
                                        dateFormat: "yy-mm-dd",
                                        yearRange: "-150:+0",
                                        maxDate: 0,
                                        "autoclose": true,
                                        onSelect: function (data) {
                                            console.log("data", data)
                                            jQuery("#dateCreated").trigger('change');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="col-2 pl-0 pr-0">
                            <button type="submit" class="btn btn-outline btn-secondary float-right" id="resetBtn" name="resetBtn">
                                <i class="fa fa-undo fa-1x"></i>
                            </button>
                        </div>
                    </div>

                    <table class="table mt-1 mb-1" id="tptPastEncounters">
                        <thead>
                        <tr>
                            <th class="bg-pale"><h6 class="text-dark text-center">Date</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Contraindication</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Eligible</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Action</h6></th>
                        </tr>
                        </thead>
                        <tbody>
                           <% artTptRegisterEncounters.each { encounter -> %>
                               <tr class="text-center">
                                   <td class="text-uppercase">${encounter.encounterDatetime.format("yyyy-MM-dd")}</td>
                                   <%
                                   def contraindicationValues = [];
                                   def eligibleValues = [];
                                   encounter.obs.each { obs ->
                                       if (obs.concept.uuid == tptContraindicationConceptUuid) {
                                           contraindicationValues << ui.encodeHtmlContent(ui.format(obs.valueCoded));
                                       }
                                       if (obs.concept.uuid == tptEligibleConceptUuid) {
                                           eligibleValues << ui.encodeHtmlContent(ui.format(obs.valueCoded));
                                       }
                                   }
                                   %>
                                   <td>
                                       <% if (!contraindicationValues.empty) { %>
                                           ${contraindicationValues.join(',  ')}
                                       <% } %>
                                   </td>
                                   <td>
                                       <% if (!eligibleValues.empty) { %>
                                           ${eligibleValues.join(', ')}
                                       <% } %>
                                   </td>
                                   <td class="text-primary">
                                       <a href="${ui.pageLink("botswanaemr", "editEncounter", [encounterId: encounter.uuid, patientId: patient.id, returnUrl: returnUrl])}" class="dropdown-item">VIEW</a>
                                   </td>
                               </tr>
                           <% } %>
                        </tbody>
                    </table>
            </div>
    </div>
</div>
