<%
    ui.includeCss("adminui", "systemadmin/accounts.css")
    ui.includeCss("botswanaemr", "choices.min.css")
    ui.includeJavascript("botswanaemr", "choices.min.js")
%>
<script type="text/javascript">
    jq(document).ready(function () {
     var jq = jQuery;
        jq('#list-regimen-lines').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq("#saveRegimenLine").on('click', function () {
            addRegimenLine();
        });
    });
    function loadRegimenLineDialog() {
        jQuery("#addRegimenLineModal").modal('show');
    }
    function addRegimenLine() {
        rows = [];
                jQuery("#list-regimen-lines").DataTable().clear().destroy();
                saveRegimenLines();
                initializeRegimenLinesTable()
    }

    function initializeRegimenLinesTable(){
            var jq = jQuery;
           var t = jq('#list-regimen-lines').DataTable({
                searchPanes: false,
                searching: false,
               responsive: true,
                "pagingType": 'simple_numbers',
                "dom": 'rtip',
                "oLanguage": {
                    "oPaginate": {
                        "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                        "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                    }
                },
                "language": {
                    "emptyTable": "No Regimen Lines Saved yet"
                },
                columnDefs: {
                    searchable: false,
                    orderable: false,
                    targets: 0,
                },
               order: [[1, 'asc']],

           });

            t.on('order.dt search.dt', function () {
                let i = 1;
                t.cells(null, 0, {search: 'applied', order: 'applied'}).every(function (cell) {
                    this.data(i++);
                });
            }).draw();
        }
        function saveRegimenLines() {
                        jq.getJSON('${ ui.actionLink("botswanaemr", "art/manageRegimenLines", "saveRegimenLines") }', {
                            regimenLineName:jq("#regimenLineName").val(),
                            regimenLineCode: jq("#regimenLineCode").val(),
                            regimenLineDescription: jq("#regimenLineDescription").val(),
                        }).success(function(data) {
                            jq().toastmessage('showSuccessToast', "Regimen Line created successfully");
                            location.reload();
                        }).fail(function(error) {
                            console.error(error);
                            jq().toastmessage('showErrorToast', "Something went wrong");
                        })
            }
</script>
<div class="card">
        <div class="col p-r-0">
            <button
                    type="submit"
                    onclick="loadRegimenLineDialog()"
                    class="btn btn-sm btn-primary mb-3 float-left">
                ${ui.message("Add New Regimen Category")}
            </button>
        </div>
        <hr>
        <table id="list-regimen-lines" class="table table-sm table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <% lines.each { %>
                    <tr>
                        <td>${it.name}</td>
                        <td>${it.code}</td>
                        <td>${it.description}</td>
                    </tr>
                <%}%>
            </tbody>
        </table>
</div>
