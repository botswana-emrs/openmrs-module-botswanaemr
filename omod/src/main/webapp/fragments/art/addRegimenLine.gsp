
<div>
    <form class="simple-form-ui" id="add-regimen-line-form" >
        <div class="col">
            <label class="form-label"
               for="regimenLineName">${ui.message("Regimen Line Name")} <span
                class="text-danger">*</span></label>
            <input type="text" class="form-control reason" id="regimenLineName" name="regimenLineName"
                    required placeholder="Enter regimen line name">
        </div>
        <div class="col">
            <label class="form-label"
               for="regimenLineCode">${ui.message("Regimen Line Code")} <span
                class="text-danger">*</span></label>
            <input type="text" class="form-control reason" id="regimenLineCode" name="regimenLineCode"
                    required placeholder="Enter regimen line Code">
        </div>
        <div class="col">
            <label class="form-label"
               for="regimenLineDescription">${ui.message("Regimen Line Description")}</label>
            <textarea class="form-control reason" id="regimenLineDescription" name="regimenLineDescription" cols="20" rows="5"
                    required placeholder="Enter regimen line Description"></textarea>
        </div>

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
        ${ui.message("Close")}
    </button>
    <button id="saveRegimenLine"
            type="submit"
            class="btn btn-sm btn-primary float-right float-left ml-1">${ui.message("Save Regimen Line")}
    </button>
</div>