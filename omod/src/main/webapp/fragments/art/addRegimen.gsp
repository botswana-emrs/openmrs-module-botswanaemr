<script>
  jq(function() {
   jq("#regimenConcept").on("focus.autocomplete", function () {
        jq(this).autocomplete({
            source: function(request, response) {
                    jq.getJSON('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/concept', {
                      q: request.term, class: 'drug'
                    }).done(function(data) {
                      var results = [];
                        data.results.forEach(function(item) {
                          results.push({
                            label: item.display,
                            value: item.uuid
                          });
                        });
                      response(results);
                    });
                  },
                  minLength: 3,
                  select: function(event, ui) {
                    event.preventDefault();
                    jq(this).val(ui.item.label);
                  }
        });
    });
  });
</script>
<div>
    <form class="simple-form-ui" id="add-regimen-line-form" >
        <div class="col">
            <label class="form-label"
               for="regimenName">${ui.message("Regimen Name")} <span
                class="text-danger">*</span></label>
            <input type="text" class="form-control reason" id="regimenName" name="regimenName"
                    required placeholder="Enter regimen name">
        </div>
        <div class="col">
            <label class="form-label"
               for="regimenLine">${ui.message("Regimen Line")} <span
                class="text-danger">*</span></label>
            <Select type="text" class="form-control reason" id="regimenLine" name="regimenLine">
                    <option>Select regimen line</option>
                    <% lines.each {%>
                        <option value="${it.regimenLineId }">${it.code}-${it.name}</option>
                    <%}%>
            </select>
        </div>
        <div class="col">
            <label class="form-label"
               for="regimenConcept">${ui.message("Regimen Concept")}<span
                class="text-danger">*</span></label>
            <input class="form-control reason" id="regimenConcept" name="regimenConcept"
                    required placeholder="Enter concept name" />
        </div>

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
        ${ui.message("Close")}
    </button>
    <button id="saveRegimen"
            type="submit"
            class="btn btn-sm btn-primary float-right float-left ml-1">${ui.message("Save Regimen")}
    </button>
</div>