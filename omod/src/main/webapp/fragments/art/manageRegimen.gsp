<%
    ui.includeCss("adminui", "systemadmin/accounts.css")
    ui.includeCss("botswanaemr", "choices.min.css")
    ui.includeJavascript("botswanaemr", "choices.min.js")
%>
<script type="text/javascript">
    jq(document).ready(function () {
     var jq = jQuery;
        jq('#list-regimen').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq("#saveRegimen").on('click', function () {
            addRegimen();
        });
    });
    function loadRegimenDialog() {
        jQuery("#addRegimenModal").modal('show');
    }
    function addRegimen(){
        rows = [];
                jQuery("#list-regimen").DataTable().clear().destroy();
                saveRegimen();
                initializeRegimenTable();
                location.reload();
    }

    function initializeRegimenTable(){
            var jq = jQuery;
           var t = jq('#list-regimen').DataTable({
                searchPanes: false,
                searching: false,
               responsive: true,
                "pagingType": 'simple_numbers',
                "dom": 'rtip',
                "oLanguage": {
                    "oPaginate": {
                        "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                        "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                    }
                },
                "language": {
                    "emptyTable": "No Regimen Lines Saved yet"
                },
                columnDefs: {
                    searchable: false,
                    orderable: false,
                    targets: 0,
                },
               order: [[1, 'asc']],

           });

            t.on('order.dt search.dt', function () {
                let i = 1;
                t.cells(null, 0, {search: 'applied', order: 'applied'}).every(function (cell) {
                    this.data(i++);
                });
            }).draw();
        }

        function saveRegimen() {
                jq.getJSON('${ ui.actionLink("botswanaemr", "art/manageRegimenLines", "saveRegimen") }', {
                    regimenName:jq("#regimenName").val(),
                    regimenLine: jq("#regimenLine").val(),
                    regimenConcept: jq("#regimenConcept").val(),
                }).done(function(data) {
                    console.log("The data found is", data);
                    jq().toastmessage('showSuccessToast', "Regimen created successfully");
                }).fail(function() {
                    jq().toastmessage('showErrorToast', "Something went wrong while truing to save");
                });
    }
</script>
<div class="card">
        <div class="col p-r-0">
            <button
                    type="button"
                    onclick="loadRegimenDialog()"
                    class="btn btn-sm btn-primary mb-3 float-left">
                ${ui.message("Add New Regimen")}
            </button>
        </div>
        <hr>
        <table id="list-regimen" class="table table-sm table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Regimen Line</th>
                    <th>Concept Reference</th>
                </tr>
            </thead>
            <tbody>
                <% regimen.each { %>
                    <tr>
                        <td>${it.regimenName}</td>
                        <td>${it.regimenLine}</td>
                        <td>${it.conceptsName}</td>
                    </tr>
                <%}%>
            </tbody>
        </table>
</div>
