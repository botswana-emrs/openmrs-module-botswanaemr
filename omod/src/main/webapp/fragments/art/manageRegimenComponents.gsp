<script type="text/javascript">
    jq(document).ready(function () {
        var jq = jQuery;
        jq('#list-regimen-component').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });

    function initializeRegimenComponentTable() {
        var jq = jQuery;
        var t = jq('#list-regimen-component').DataTable({
            searchPanes: false,
            searching: false,
            responsive: true,
            "pagingType": 'simple_numbers',
            "dom": 'rtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            },
            "language": {
                "emptyTable": "No Regimen Component Saved yet"
            },
            columnDefs: {
                searchable: false,
                orderable: false,
                targets: 0,
            },
            order: [[1, 'asc']],

        });

        t.on('order.dt search.dt', function () {
            let i = 1;
            t.cells(null, 0, {search: 'applied', order: 'applied'}).every(function (cell) {
                this.data(i++);
            });
        }).draw();
    }
</script>

<div class="card">
    <div class="col p-r-0">
        <button
                type="submit"
                class="btn btn-sm btn-primary mb-3 float-left"
                id = "addRegimenComponent">
            ${ui.message("Add Regimen Component")}
        </button>
    </div>
    <hr>
    <table id="list-regimen-component" class="table table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Component Label</th>
            <th>Regimen Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <% regimenComponents.each { %>
        <tr>
            <td>${it.regimenComponentLabel}</td>
            <td>${it.regimenName}</td>
            <td><a href="#" class="editRegimenComponent" data-id="${it.regimenComponentId}">View</a></td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
