<%
    def defaultEncounterDate = currentVisit ? currentVisit.startDatetime : new Date()
%>
<% if(hasVisit){%>
    <div class="card">
        <div class="row">
            <div id="available-forms" class="col px-0">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-primary">
                       <h4 class="text-white">${ ui.message("Available Forms") }</h4>
                    </a>
                    <%  availbleForms.each {%>
                        <a href="${ui.pageLink("botswanaemr", "enterForm", [
                                  patientId: patient.uuid,
                                  formUuid: it.form.uuid,
                                  visitId: currentVisit.uuid,
                                  returnUrl:returnUrl])}"
                                  class="list-group-item list-group-item-action">
                                  ${it.form.name}
                        </a>
                    <%}%>
                </div>
            </div>
            <div id="completed-forms" class="col px-0">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-success">
                       <h4 class="text-white">${ ui.message("Completed Forms") }</h4>
                    </a>
                    <% encounters.each {%>
                        <span
                           class="list-group-item list-group-item-action">
                           ${it.form.name}
                           <i class="icon-pencil edit-action" title="${ ui.message("coreapps.edit") }"
                              onclick="location.href='${ ui.pageLink("botswanaemr", "editEncounter", [encounterId:it.uuid, patientId: patient.uuid, returnUrl: returnUrl]) }'">
                           </i>
                        </span>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
<%}else{%>
<h5>Please create visit for this patient to access the forms</h5>
<%}%>
