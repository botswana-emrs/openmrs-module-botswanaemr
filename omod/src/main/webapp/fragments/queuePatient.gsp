<script type="text/javascript">
    jQuery(document).ready(function () {
        var patientQueueLocationsUuidList = '${patientQueueLocationsUuidList}';
        var queueLocationsListSize = '${queueLocationsList.size}';

        jq("#queueRoom").change(function () {
            handleQueueRoomChange();
        });

        jq("#cancelAssignment").on('click', function (e) {
            jq("#queueRoom").val('');
            jQuery("#confirmPatientAssignmentModal").modal('hide');
        });

        jq("#closeModal").on('click', function (e) {
            jq("#queueRoom").val('');
            jQuery("#confirmPatientAssignmentModal").modal('hide');
        });
    
        jq('#confirmAssignment').click(function(){
            jq("#queuePatient").click();
            jq("#saveOverlay").show();
            jQuery("#confirmPatientAssignmentModal").modal('hide');
        });

        jq('#queuePatient').click(function(){
            jq("#saveOverlay").show();
            jQuery("#confirmPatientAssignmentModal").modal('hide');
        });

        function handleQueueRoomChange() {
            var selectedValue = jq('#queueRoom').find(":selected").val();
            if(selectedValue !== '${patientScreeningPortal.uuid}'){
                if(patientQueueLocationsUuidList.includes(selectedValue)){
                    jq().toastmessage('showSuccessToast', "Patient has already been queued to this location!");
                } else {
                    jQuery('#confirmPatientAssignmentModal').modal('show');
                    jq('#flag').text(jq('#queueRoom').find(":selected").text());
                    jq('#queuePatientSection').hide();
                }
            } else {
                if(patientQueueLocationsUuidList.includes(selectedValue)){
                    jq('#queuePatientSection').hide();
                    jq().toastmessage('showSuccessToast', "Patient has already been queued to this location!");
                } else {
                    jq('#queuePatientSection').show();
                }
            }
        }

        if (parseInt(queueLocationsListSize) === 1) {
            var singleLocation = {
                uuid: jq('#queueRoom option:eq(1)').val(),
                name: jq('#queueRoom option:eq(1)').text()
            };

            jq('#queueRoom').val(singleLocation.uuid);
            jq("#queuePatient").prop("disabled", false);
            handleQueueRoomChange();
        }

        // if isBpScreening is checked, hide queueRoom field, set redirectUrl field to blank, set the queuePatient button text to Proceed
        jq("#isBpScreening").change(function () {
            if (jq(this).is(":checked")) {
                jq("#queueRoom").closest('.form-group').hide('slow');
                jq("#redirectUrl").val('${ui.pageLink("botswanaemr", "consultation/auxilliaryNurseDashboard?appId=botswanaemr.auxilliaryNurseDashboard")}');
                jq("#queuePatient").text('Proceed').show();
                jq("#queuePatientSection").show();
                jq("#queueRoom").removeAttr('required')
            } else {
                jq("#queueRoom").closest('.form-group').show('slow');
                jq("#redirectUrl").val('${returnUrl}');
                jq("#queuePatient").text('${ui.message("Assign Patient")}');
                jq("#queueRoom").attr('required', true);

            }
        });

    });
</script>

<form id="queuePatientForm" method="post">
    <div>
        <% if (currentServicePoint == patientScreeningPortal?.uuid) { %>
        <div class="form-group pt-1">
            <label for="isBpScreening">Did the patient come For Vitals Only?
                <input class="form-check-input" name="isBpScreening" type="checkbox" id="isBpScreening">
            </label>
        </div>
        <% } %>

        <div class="form-group content-section">
            <label for="queueRoom">Assign patient to
                <span class="text-danger">*</span>
            </label>
            <input type="hidden" id ="redirectUrl" name="redirectUrl" value="${returnUrl}">
            <select class="custom-select" id="queueRoom" name="queueRoom" required>
                <option selected required value="" >Select where the patient needs to be assigned to</option>
                <% if(queueLocationsList != null) {
                queueLocationsList.each { %>
                <option value="${it.uuid}">${it.name}</option>
                <%}
                }%>
            </select>
        </div>

        <div id="queuePatientSection" class="row">
            <button id="queuePatient" type="submit" class="btn btn-primary btn-block">${ui.message("Assign Patient")}</button>
        </div>
    </div>
</form>

<!-- Confirm Patient Assignment Modal -->
<div class="modal fade" id="confirmPatientAssignmentModal" tabindex="-1"
     data-controls-modal="confirmPatientAssignmentModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="confirmPatientAssignmentModalTitle">

    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="text-white text-center modal-title" id="confirmPatientAssignmentModalTitle">
                    Confirm
                </h4>
                <button type="button" id="closeModal" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <p class="text-left h6 text-muted pl-0">Are you sure you want to assign patient to the <span id="flag"></span> Pool?</p>
                    </div>
                    <div class="col-md-3 pl-0">
                        <div class="col">
                            <button id="confirmAssignment" type="button" class="btn btn-sm btn-primary float-right ml-1">${ui.message("Assign")}</button>
                            <button id="cancelAssignment" type="button" class="btn btn-sm btn-dark bg-dark float-right">${ui.message("Close")}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
