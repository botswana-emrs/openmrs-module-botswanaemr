<%
    ui.includeCss("adminui", "systemadmin/accounts.css")
    ui.includeCss("botswanaemr", "choices.min.css")
    ui.includeCss("botswanaemr", "base.min.css")
    ui.includeJavascript("botswanaemr", "choices.min.js")
%>
<script type="text/javascript">
    var choices;
    var selections;
    jq(document).ready(function () {
        var jq = jQuery;
        jq('#list-accounts').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        choices = new Choices('#userRole', {
            removeItemButton: true,
            searchResultLimit: 5,
            renderChoiceLimit: 5,
        });

        selections = new Choices('#userFacilitiesOptions', {
            removeItemButton: true,
        });
    });

    function editAccount(personId = null) {
        jq("#editAccountModal")
            .find("input,textarea,select,multiple")
            .val('')
            .end();
        jQuery("#editAccountModal").modal('show');
        if (personId) {
            choices.clearStore();
            jq('#userDetailsForm').attr('action', '${ ui.actionLink('botswanaemr', 'manageAccounts','post')}');
            jq("#personId").val(personId);
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","manageAccounts","getPersonDetails")}',
                dataType: "json",
                global: false,
                async: true,
                data: {
                    personId: personId,
                },
                success: function (data) {
                    jq(".person-details").prop('required', false);
                    jq("#userName").val(data.userDetails.username);
                    jq("#userName").attr('readonly', true);
                    jq("#selectedUserName").val(data.userDetails.username);
                    choices.setChoices(
                        data.roleOptions,
                        'value',
                        'label',
                        true,
                    );
                    jq("#personFamilyName").val(data.simplePerson.familyName)
                    jq("#personMiddleName").val(data.simplePerson.middleName != null ? data.simplePerson.middleName : '');
                    jq("#personGivenName").val(data.simplePerson.givenName);
                    jq("#personGender option").filter(function() {
                        return jq(this).text() === (data.simplePerson.gender === 'M' ? "Male" : "Female");
                    }).prop('selected', true);
                }
            });

            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","manageAccounts","getUserAssignedFacilities")}',
                dataType: "json",
                global: false,
                async: true,
                data: {
                    personId: personId,
                },
                success: function (data) {
                    data.forEach(function (facility) {
                        jq('#userFacilitiesOptions').removeClass("is-hidden");
                        jq('#userFacilitiesOptions').append(jq('<option selected>').val(facility.uuid).text(facility.name));
                    });
                }
            });
        } else {
            choices.unhighlightAll();
            selections.unhighlightAll();
            jq('#userDetailsForm').attr('action', '${ ui.actionLink('botswanaemr', 'manageAccounts','createNewAccount')}');
        }
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","manageAccounts","getAllActiveParentFacilities")}',
            dataType: "json",
            global: false,
            async: true,
            data: {
                personId: personId,
            },
            success: function (data) {
                selections.setChoices(
                    data.name,
                );
            }
        });
    }

    function deleteUser(personUuid, givenName) {
        jQuery("#deleteAccountModal").modal('show');
        jq('#deleteUserModalTitle').text('Delete User: ' + givenName);
        jq("#deleteAccountBtn").click(function(e) {
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","manageAccounts","deleteUser")}',
                dataType: "json",
                   global: false,
                async: false,
                data: {
                    personUuid: personUuid,
                },
                success: function (data) {
                    if(data.hasException){
                        jQuery("#deleteAccountModal").modal('hide');
                        jq().toastmessage('showErrorToast', 'Cannot delete User Account for '+ givenName +' !!');
                    } else{
                        jq('#userDetailsForm').attr('action', '${ ui.actionLink('botswanaemr', 'manageAccounts','post')}');
                    }
                }
            });
        });
    }

    function retireUser(personUuid, givenName) {
        jQuery("#disableAccountModal").modal('show');
        jq('#disableAccountModalTitle').text('Disable User: ' + givenName);
        jq("#disableAccountBtn").click(function(e) {
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","manageAccounts","retireUser")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                   personUuid: personUuid,
                   userRetireReason: jq("#disableRetireReason").val()
                },
                success: function (data) {
                   jq('#userDetailsForm').attr('action', '${ ui.actionLink('botswanaemr', 'manageAccounts','post')}');
                }
            });
        });
    }

    function unRetireUser(personUuid, givenName) {
        jQuery("#enableAccountModal").modal('show');
        jq('#enableAccountModalTitle').text('Enable User: ' + givenName);
        jq("#enableAccountBtn").click(function(e) {
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","manageAccounts","unRetireUser")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    personUuid: personUuid,
                },
                success: function (data) {
                    jq('#userDetailsForm').attr('action', '${ ui.actionLink('botswanaemr', 'manageAccounts','post')}');
                }
            });
        });
    }
</script>


<div class="card">
    <div class="col p-r-0">
        <button
                type="submit"
                onclick="editAccount()"
                class="btn btn-sm btn-primary mb-3 float-left">
            ${ui.message("adminui.addAccount.label")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/account.page";
            };
        </script>
    </div>
    <br/>

    <hr>
    <table id="list-accounts" class="table table-sm table-bordered" style="width:100%">

        <thead>
        <tr>
            <th>${ui.message("general.name")}</th>
            <th>${ui.message("adminui.gender")}</th>
            <th>${ui.message("adminui.accounts.numberOfUserAccounts")}</th>
            <th>${ui.message("adminui.accounts.numberOfProviderAccounts")}</th>
            <th>${ui.message("SystemInfo.status")}</th>
            <th>${ui.message("general.action")}</th>
        </tr>
        </thead>
        <tbody>
        <% accounts.each { %>
        <tr>
            <td>
                <% //In case of providers not linked to person records
                    def name = ""
                    def isLinkedToPerson = false
                    if ((it.person != null && it.person.personId != null)) {
                        isLinkedToPerson = true
                        name = ui.encodeHtmlContent(ui.format(it.person))
                    }
                    if (name == null || name.trim() == "") {
                        name = ui.encodeHtmlContent(providerNameMap[it.providerAccounts[0]])
                    }
                    if (name == null || name.trim() == "") {
                        name = ui.encodeHtmlContent(it.providerAccounts[0].identifier)
                    }
                    if (name == null || name.trim() == "") {
                        name = "<i>" + ui.message("adminui.noname") + "</i>"
                    }
                %>

                ${name} ${!isLinkedToPerson ? "*" : ""}
            </td>
            <td>${ui.format(it.person.gender)}</td>
            <td>${ui.format(it.userAccounts.size)}</td>
            <td>${ui.format(it.providerAccounts.size)}</td>
            <td>
                <%
                    boolean isRetired = userRetiredStatusMap.get(it.userAccounts[0]);
                    String retiredStatus = isRetired ? "Disabled" : "Active";
                %>
                ${retiredStatus}
            </td>
            <td>
                <% if (isLinkedToPerson) { %>
                <% if (isRetired) { %>
                    <i class="fas fa-eye" title="${ui.message('User.unRetire')}" onclick="unRetireUser('${it.person.uuid}', '${it.person.givenName}')"></i>
                 <% } else  {%>
                   <i class="fas fa-eye-slash" title="${ui.message('User.retire')}" onclick="retireUser('${it.person.uuid}', '${it.person.givenName}')"></i>
                 <% } %>
                <i class="icon-pencil edit-action" title="${ui.message('general.edit')}"
                    onclick="editAccount(${it.person.id})"></i>
                <i class="icon-trash delete-action" style="color: red;" title="${ui.message('general.void')}"
                    onclick="deleteUser('${it.person.uuid}', '${it.person.givenName}')"></i>
                <% } %>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <i>* ${ui.message("adminui.account.provider.notLinkedToPerson")}</i>

    <div class="modal fade" id="deleteAccountModal" tabindex="-1"
         data-controls-modal="deleteAccountModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="deleteAccountModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <i class="modal-title text-white" id="deleteUserModalTitle">Delete User: </i>
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form class="simple-form-ui" id="deleteAccountForm" >
                        <p>${ui.message("Are you sure you want to delete this user? This action cannot be undone.")}</p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                                ${ui.message("Cancel")}
                            </button>
                            <button id="deleteAccountBtn" type="submit"
                                    class="btn btn-sm btn-danger float-right ml-1">${ui.message("Delete")}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

 <div class="modal fade" id="enableAccountModal" tabindex="-1"
         data-controls-modal="enableAccountModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="enableAccountModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <i class="modal-title text-white" id="enableAccountModalTitle">Enable User: </i>
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form class="simple-form-ui" id="enableAccountForm">
                       <p>${ui.message("Are you sure you want to enable account of  this user?")}</p>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                               ${ui.message("Cancel")}
                           </button>
                           <button id="enableAccountBtn" type="submit"
                                   class="btn btn-sm btn-primary float-right ml-1">${ui.message("Enable")}
                           </button>
                       </div>
                    </form>
                </div>
            </div>
        </div>
 </div>

<div class="modal fade" id="disableAccountModal" tabindex="-1"
         data-controls-modal="disableAccountModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="disableAccountModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <i class="modal-title text-white" id="disableAccountModalTitle">Disable Account: </i>
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form class="simple-form-ui" id="disableAccountForm">
                        <div class="col">
                            <fieldset class="w-100 p-5 mt-4">
                                <legend class="w-auto"><b><small>${ui.message("Enter reason for disabling")}</small></b></legend>
                                <div class="form-group">
                                    <label class="form-label"
                                           for="disableRetireReason">${ui.message("Reason")} <span
                                            class="text-danger">*</span></label>
                                   <input type="text" class="form-control reason" id="disableRetireReason" name="disableRetireReason"
                                            required placeholder="Enter Reason">
                                </div>

                            </fieldset>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                                ${ui.message("Cancel")}
                            </button>
                            <button id="disableAccountBtn" type="submit"
                                    class="btn btn-sm btn-danger float-right ml-1">${ui.message("Disable")}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
