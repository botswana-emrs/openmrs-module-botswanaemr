<script type="text/javascript">

    jQuery(function () {

        function formatData(searchResult) {
            let data = {
                "temperature": [],
                "weight": [],
                "height": [],
                "bmi": [],
                "diastolic": [],
                "systolic": [],
                "pulse": []
            };

            data["dateRanges"] = searchResult["dateRanges"];

            if (searchResult['temperature']) {
                let val = searchResult['temperature'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["temperature"].push(mappedVal);
                });
            }
            if (searchResult['weight']) {
                let val = searchResult['weight'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["weight"].push(mappedVal);
                });
            }
            if (searchResult['bmi']) {
                let val = searchResult['bmi'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["bmi"].push(mappedVal);
                });
            }
            if (searchResult['height']) {
                let val = searchResult['height'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["height"].push(mappedVal);
                });
            }
            if (searchResult['pulse']) {
                let val = searchResult['pulse'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["pulse"].push(mappedVal);
                });
            }
            if (searchResult['systolic']) {
                let val = searchResult['systolic'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["systolic"].push(mappedVal);
                });
            }
            if (searchResult['diastolic']) {
                let val = searchResult['diastolic'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);

                    data["diastolic"].push(mappedVal);
                });
            }


            return data;
        }

        function getPatientVitals(patientId) {
            let searchResult;
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","vitalsTrends","getPatientVitals")}',
                dataType: "json",
                global: false,
                async: true,
                data: {
                    patientId: patientId,
                },
                success: function (data) {
                    let mappedDta = formatData(data);
                    updateChartData(mappedDta);
                    searchResult = data;
                }
            });
            return searchResult;
        }

        var chart;
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'vitalsTrends',
                backgroundColor: null,
                type: 'spline'
            },
            credits: {
                enabled: false
            },
            rangeSelector: {
                verticalAlign: 'top',
                x: 0,
                y: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Date Recorded'
                },
            },
            yAxis: {
                title: {
                    text: 'Level'
                },
                tickInterval: 50
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: [
                {
                    name: 'Temperature',
                    data: []
                },
                {
                    name: 'Weight',
                    data: []
                },
                {
                    name: 'Height',
                    data: []
                },
                {
                    name: 'BMI',
                    data: []
                },
                {
                    name: 'Diastolic',
                    data: []
                },
                {
                    name: 'Systolic',
                    data: []
                },
                {
                    name: 'Pulse',
                    data: []
                }
            ],
        });

        function updateChartData(data) {
            chart.update({
                xAxis: {
                    title: {
                        text: 'Date Recorded'
                    },
                    categories: data["dateRanges"]
                },
                series: [
                    {
                        name: "Temperature",
                        data: data["temperature"]
                    },
                    {
                        name: "Weight",
                        data: data["weight"]
                    },
                    {
                        name: "Height",
                        data: data["height"]
                    },
                    {
                        name: "Bmi",
                        data: data["bmi"]
                    },
                    {
                        name: "Diastolic",
                        data: data["diastolic"]
                    },
                    {
                        name: "Systolic",
                        data: data["systolic"]
                    },
                    {
                        name: "Pulse",
                        data: data["pulse"]
                    }
                ]
            });
        }

        getPatientVitals('${patient.id}')
    });
</script>

<div class="col-12">
    <div class="row">
        <div class="col-5 zero-padding text-left">
            <h5>${ui.message('Vitals Trends')}</h5>
        </div>
    </div>

    <div id="vitalsTrends" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>
</div>