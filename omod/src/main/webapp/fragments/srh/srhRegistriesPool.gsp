<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let name = aData[1].trim();
                matchingOptions.push(
                    name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );
    
    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            dataType: "json",
            global: false,
            async: false,
            data: {
                servicePoint: "${srhServicePoint != null ? srhServicePoint : ''}",
                sourcePageName: "srh/srhDashboard",
            },
            success: function (data) {
                console.log(data);
                var table = jQuery('#maternityRegistryTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Treated</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.patientId != null){
                                        return '<a href="/${ui.contextPath()}/botswanaemr/srh/maternityRegistryForm.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary p-1" id="maternity">Maternity</a>&nbsp;|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/srh/postnatalRegistryForm.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary p-1" id="postnatal">Postnatal</a>&nbsp;|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/anc/ancProfile.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary p-1" id="antenatal">Antenatal</a>&nbsp;|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/srh/cohortRegistryProfile.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary p-1" id="cohort">Cohort</a>&nbsp;|&nbsp;';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#maternityPatientPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="card-header pr-1">
    <div class="row mt-3 mb-3 pr-1">
        <div class="col-8 pl-0">
            <input id="searchPhrase"
                   type="text"
                   name="searchPhrase"
                   class="form-control"
                   value=""
                   placeholder="${ui.message('Patient Names')}"/>
        </div>

        <div class="col">
            <button id="searchButton"
                    type="submit"
                    class="btn btn-primary float-right">
                    <i class="fa fa-search"></i>
                    ${ui.message("Search")}
            </button>
        </div>
        <div class="col pr-0">
            <button id="resetBtn"
                    type="reset"
                    class="btn btn-dark bg-dark float-right">
                    <i class="fa fa-undo"></i>
                    ${ui.message("Reset")}
            </button>
        </div>
    </div>
</div>

<div class="card-body mt-5">
    <div class="table-responsive">
        <table id="maternityRegistryTable"
               class="table table-striped table-sm table-bordered">
            <thead>
                <tr>
                    <th>Pin</th>
                    <th>Patient Names</th>
                    <th>Sex</th>
                    <th>Age</th>
                    <th>Status</th>
                    <th class="text-center">Registry Forms</th>
                </tr>
            </thead>
        </table>
    </div>
</div>