<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#screenedBy').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                matchingOptions.push(screenedBy === jq('#screenedBy').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            dataType: "json",
            global: false,
            async: false,
            data: {
                servicePoint: "${srhServicePoint != null ? srhServicePoint : ''}",
                sourcePageName: "srh/srhDashboard",
            },
            success: function (data) {
                console.log(data);
                var table = jQuery('#patientQueueListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'providerNames'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Screened</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                        return '<a href="/${ui.contextPath()}/botswanaemr/srh/srhClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'" class="text-primary p-1" id="view">View</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/srh/srhClientProfile.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary p-1" id="view">SRH Profile & Forms</a>|&nbsp;'+
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0" id="reassign">Reassign</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#familyPlanningPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Screened by</label>
        <select id="screenedBy"
                class="form-control"
                placeholder="${ui.message('Select nurse ')}">
            <option value="">Select One</option>
            <% providerList.each { provider-> %>
            <% if (provider != null && provider?.person != null && provider?.person?.personName != null) { %>
            <option value="<%=provider.uuid %>"><%=provider.person.personName %></option>
            <% } %>
            <% } %>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="patientQueueListTable"
       class="table table-striped table-sm table-bordered">
    <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Sex</th>
            <th>Age</th>
            <th>Screened By</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>
