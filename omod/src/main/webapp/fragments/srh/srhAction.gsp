<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'srh/srhDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-laptop-medical fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl, 'action': 'self'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'laboratoryOrders')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-syringe fa-1x"></i>
            </div>
            <div class="nav_label">Lab Orders</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'adverseevents/adverseEvents')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-syringe fa-1x"></i>
            </div>
            <div class="nav_label">Adverse Drug Effects</div>
        </a>
    </li>
    <!--
    <li>
        <a href="${ui.pageLink('botswanaemr', 'pharmacy/pharmacyAllPrescriptions')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-plus-square fa-1x"></i>
            </div>
            <div class="nav_label">Prescriptions</div>
        </a>
    </li>
    -->
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/myAppointments')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'srh/srhRegistriesPool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-address-book-o fa-1x"></i>
            </div>
            <div class="nav_label">Registries</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>
