<script type="text/javascript">

    jQuery(function () {
        let numBabies = `${numBabies}`;
        let chartData = {};
        for(let i = 1; i <= numBabies; i++) {
            jq('#baby-selector').append(jq('<option>', {
                value: i,
                text: "Baby " + i
            }));
        }
        jq('#baby-selector').on('change', function() {
            jq("#babyNum").html(" - Baby " + jq(this).val());
            getGestationData('${patient.id}', jq(this).val() || 1);
        });

        function formatData(searchResult) {
            let data = {
                "motherWeights": [],
                "foetalWeights": [],
                "babySizes": [],
            };

            // data["dateRanges"] = searchResult["dateRanges"];

            if (searchResult['motherWeights']) {
                let val = searchResult['motherWeights'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.xValue);
                    mappedVal.push(item.yValue);

                    data["motherWeights"].push(mappedVal);
                });
            }
            if (searchResult['foetalWeights']) {
                let val = searchResult['foetalWeights'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.xValue);
                    mappedVal.push(item.yValue);

                    data["foetalWeights"].push(mappedVal);
                });
            }
            if (searchResult['babySizes']) {
                let val = searchResult['babySizes'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.xValue);
                    mappedVal.push(item.yValue);

                    data["babySizes"].push(mappedVal);
                });
            }
            return data;
        }

        function getGestationData(patientId, babyNumber) {
            if (chartData[babyNumber]) {
                updateChartData(chartData[babyNumber]);
                return chartData[babyNumber];
            }
            let searchResult;
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","srh/gestationTrends","getGestationData")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    patientId: patientId,
                    babyNumber: babyNumber
                },
                success: function (data) {
                    let mappedDta = formatData(data);
                    updateChartData(mappedDta);
                    chartData[babyNumber] = mappedDta;
                    searchResult = data;
                }
            });
            return searchResult;
        }

        var chart;
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'gestationTrends',
                backgroundColor: null,
                type: 'spline'
            },
            credits: {
                enabled: false
            },
            rangeSelector: {
                verticalAlign: 'top',
                x: 0,
                y: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Gestation Week'
                },
                max: 42,
                min: 8,
                tickInterval: 1,
                gridLineWidth: 1, // Width of grid lines on x-axis
                gridLineColor: '#e0e0e0', // Color of grid lines on x-axis
            },
            yAxis: {
                title: {
                    text: 'Weight (kg), Height (cm)'
                },
                min: 0,
                max: 45,
                tickInterval: 5,
                minorGridLineWidth: 2, // Width of minor grid lines
                minorTickInterval: 'auto',
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: [
                {
                    name: '',
                    data: []
                },
                {
                    name: '',
                    data: []
                },
                {
                    name: '',
                    data: []
                },
                {
                    name: '',
                    data: []
                },
                {
                    name: '',
                    data: []
                },
                {
                    name: 'Mother Weight (kg)',
                    data: []
                },
                {
                    name: 'Foetal Weight (kg)',
                    data: []
                },
                {
                    name: 'Symphysis-Fundus height (s-f) cm',
                    data: []
                }
            ],
        });

        function updateChartData(data) {
            chart.update({
                series: [
                    {
                        name: "",
                        dashStyle: 'Dot',
                        data:[[5,0],[10,0],[15,4],[20,8],[25,11],[30,14],[35,18],[40,22]],
                        marker: {
                            enabled: false
                        },
                        color: "#666666"
                    },
                    {
                        name: "",
                        dashStyle: 'Dot',
                        data:[[15,10],[20,16],[25,20.5],[30,24.5],[35,29],[40,31.5]],
                        marker: {
                            enabled: false
                        },
                        color: "#666666"
                    },
                    {
                        name: "Normal",
                        data:[[10,1],[15,12],[20,18],[25,23],[30,27.5],[35,32],[40,34.1]],
                        marker: {
                            enabled: false
                        },
                        color: "#666666"
                    },
                    {
                        name: "50th percentile",
                        dashStyle: 'Dot',
                        data:[[15,15],[20,20.5],[25,26],[30,31.5],[35,36.5],[40,39.2]],
                        marker: {
                            enabled: false
                        },
                        color: "#666666"
                    },
                    {
                        name: "90th percentile",
                        dashStyle: 'Dot',
                        data:[[17,18],[20,22],[25,28],[30,33.5],[35,39],[38,42.4],[40,43]],
                        marker: {
                            enabled: false
                        },
                        color: "#666666"
                    },
                    {
                        name: 'Symphysis-Fundus height (s-f) cm',
                        data: data["babySizes"]
                    },
                    {
                        name: 'Mother Weight (kg)',
                        data: data["motherWeights"]
                    },
                    {
                       name: 'Foetal Weight (kg)',
                       data: data["foetalWeights"]
                    }
                ]
            });
        }
        getGestationData('${patient.id}', jq("baby-selector").val() || 1);
    });
</script>

<div class="col-12">
    <div class="row">
        <div class="col zero-padding text-left">
            <h5 id="chartTitle">${ui.message('Gestation Trends: Symphysis-Fundus height (s-f) cm, foetal Weight and maternal Weight')} <span id="babyNum"></span></h5>
        </div>
    </div>

    <select id="baby-selector" class="pt-1 pb-1">
    </select>

    <div id="gestationTrends" style="min-width: 100%; margin: 0; padding-bottom: 5px;height: 500px"></div>
</div>
