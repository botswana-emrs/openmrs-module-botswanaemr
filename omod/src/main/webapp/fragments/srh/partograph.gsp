<script type="text/javascript">
    jQuery(function () {
        function createChart(title, renderTo, data, series, xAxis, yAxis) {
            return new Highcharts.chart({
                chart: {
                    renderTo: renderTo,
                    backgroundColor: null,
                    type: 'spline'
                },
                credits: {
                    enabled: false
                },
                rangeSelector: {
                    verticalAlign: 'top',
                    x: 0,
                    y: 0
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    title: {
                        text: xAxis
                    },
                    categories: data["timeRanges"]
                },
                yAxis: {
                    title: {
                        text: yAxis.title
                    },
                    tickInterval: yAxis.tick || 5,
                    min : yAxis.min || 0,
                    max : yAxis.max || 0
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                series: series,
            });
        }
        function formatData(searchResult) {
            let data = {
                "fetalHeartRate": [],
                "cervixSize": [],
                "descentOfHead": [],
                "contractions": [],
                "diastolic": [],
                "systolic": [],
                "pulse": [],
                "temperature": [],
                "urineVolume": []
            };
            data["timeRanges"] = searchResult["timeRanges"];
            if (searchResult['fetalHeartRate']) {
                let val = searchResult['fetalHeartRate'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["fetalHeartRate"].push(mappedVal);
                });
            }
            if (searchResult['cervixSize']) {
                let val = searchResult['cervixSize'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["cervixSize"].push(mappedVal);
                });
            }
            if (searchResult['contractions']) {
                let val = searchResult['contractions'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["contractions"].push(mappedVal);
                });
            }
            if (searchResult['descentOfHead']) {
                let val = searchResult['descentOfHead'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["descentOfHead"].push(mappedVal);
                });
            }
            if (searchResult['pulse']) {
                let val = searchResult['pulse'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["pulse"].push(mappedVal);
                });
            }
            if (searchResult['systolic']) {
                let val = searchResult['systolic'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["systolic"].push(mappedVal);
                });
            }
            if (searchResult['diastolic']) {
                let val = searchResult['diastolic'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["diastolic"].push(mappedVal);
                });
            }
            if (searchResult['temperature']) {
                let val = searchResult['temperature'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["temperature"].push(mappedVal);
                });
            }
            if (searchResult['urineVolume']) {
                let val = searchResult['urineVolume'];
                val.map((item) => {
                    let mappedVal = [];
                    mappedVal.push(item.yValue);
                    mappedVal.push(item.xValue);
                    data["urineVolume"].push(mappedVal);
                });
            }
            return data;
        }

        function getPartographData(patientId) {
            let searchResult;
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","srh/partograph","getPartographData")}',
                dataType: "json",
                global: false,
                async: false,
                data: {
                    patientId: patientId,
                },
                success: function (data) {
                    let mappedDta = formatData(data);
                    // updateChartData(mappedDta);
                    searchResult = data;

                    //Foetal chart
                    createChart(
                        "Foetal Heart Rate",
                        "fhrGraph",
                        mappedDta,
                        [
                            {
                                name: "Fetal Heart Rate",
                                data: mappedDta["fetalHeartRate"]
                            }
                        ],
                        "Time Record",
                        {
                            title: "Fetal Heart Rate",
                            min: 80,
                            max: 190,
                            tick: 10
                        });

                    //Cervix size chart
                    createChart(
                        "Cervix and Descent of Head",
                        "cervixGraph",
                        mappedDta,
                        [
                            {
                                name: "Cervix size",
                                data: mappedDta["cervixSize"]
                            },
                            {
                                name: "Descent of head",
                                data: mappedDta["descentOfHead"]
                            }
                        ],
                        "Time Record",
                        {
                            title: "Cervix and Descent of Head",
                            min: 0,
                            max: 10,
                            tick: 1
                        });

                    //Contractions
                    createChart(
                        "Contractions per 10 minutes",
                        "contractionsGraph",
                        mappedDta,
                        [
                            {
                                name: "Contractions per 10 minutes",
                                data: mappedDta["contractions"]
                            }
                        ],
                        "Time Record",
                        {
                            title: "Contractions per 10 minutes"
                        });

                    //vitals
                    createChart(
                        "Vitals",
                        "vitalsGraph",
                        mappedDta,
                        [
                            {
                                name: "Diastolic",
                                data: data["diastolic"]
                            },
                            {
                                name: "Systolic",
                                data: data["systolic"]
                            },
                            {
                                name: "Pulse",
                                data: data["pulse"]
                            },
                            {
                                name: "Temperature",
                                data: data["temperature"]
                            },
                            {
                                name: "Weight",
                                data: data["weights"]
                            }
                        ],
                        "Time Record",
                        {
                            title: "Level"
                        });
                }
            });
            return searchResult;
        }


        getPartographData('${patient.id}')
    });
</script>

<div class="col-12 pt-5">
    <div class="row">
        <div class="col zero-padding text-left">
            <h5>${ui.message('Foetal Heart Rate Graph')}</h5>
        </div>
    </div>
    <div id="fhrGraph" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>

    <div class="row">
        <div class="col zero-padding text-left">
            <h5>${ui.message('Cervix and Descent of Head Graph')}</h5>
        </div>
    </div>
    <div id="cervixGraph" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>

    <div class="row">
        <div class="col zero-padding text-left">
            <h5>${ui.message('Contractions per 10 minutes')}</h5>
        </div>
    </div>
    <div id="contractionsGraph" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>
                    
    <div class="row">
        <div class="col zero-padding text-left">
            <h5>${ui.message('Vitals Graph')}</h5>
        </div>
    </div>
    <div id="vitalsGraph" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>
</div>