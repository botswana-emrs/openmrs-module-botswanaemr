<div class="row">
    <div class="row col-12 pl-0 pr-0 mb-2">
        <div class="col-12 pl-0 pr-0">
            <h5 class="text-primary text-left mt-3 pl-3">SCREENING HISTORY</h5>
            <hr class="divider pt-1 bg-primary ml-3"/>
            <div class="row">
                <% if (screeningHistoryObs.size() > 0) { %>
                <div class="col-sm-6">
                    <table class="table mt-3">
                        <tbody>
                        <tr>
                            <td> Screened for Cervical Cancer in the Past?:</td>
                            <td>${screeningHistoryObs?.get('pastScreening')?.get('val') ?: ''}</td>
                        </tr>
                        <tr>
                            <td> Screening Through:</td>
                            <td>${screeningHistoryObs?.get('screeningThrough')?.get('val') ?: ''}</td>
                        </tr>
                        <tr>
                            <td>Past Screening Results:</td>
                            <td>${screeningHistoryObs?.get('screeningResult')?.get('val') ?: ''}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table class="table mt-3">
                        <tbody>
                        <tr>
                            <td>Treatment Performed:</td>
                            <td>${screeningHistoryObs.get('treatmentTypePerformed') ? "Yes" : "No"}</td>
                        </tr>
                        <tr>
                            <td>Type of Treatment Performed:</td>
                            <td>${screeningHistoryObs.get('treatmentTypePerformed')?.get('val') ?: ''}</td>
                        </tr>
                        <tr>
                            <td>Treatment Date:</td>
                            <td>${screeningHistoryObs.get('treatmentDate')?.get('val') ?: ''}</td>
                        </tr>
                        </tbody>
                    </table>
                    <button role="button"
                            class="btn btn-md btn-primary float-right mr-0 pr-4 pl-4" style="color: #fff !important;" data-toggle="modal"
                            data-target="#cervicalCancerModal">
                        Edit
                    </button>
                </div>
                <% } else {%>
                <div class="col-sm-6">
                    <table class="table mt-3">
                        <tbody>
                        <tr>
                            <td>Screening done for cervical cancer:</td>
                            <td>No</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <button role="button"
                            class="btn btn-md btn-primary float-right mr-0 pr-4 pl-4" style="color: #fff !important;" data-toggle="modal"
                            data-target="#cervicalCancerModal">
                        Add
                    </button>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="cervicalCancerModal" tabindex="-1"
     data-controls-modal="cervicalCancerModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="cervicalCancerModallTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cervicalCancerModallTitle">Cervical Cancer Screening</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="cervicalCancerForm">
                    <div class="form-group">
                        <label>Screened for Cervical Cancer in the past?</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" id="screenedYes" name="screened" value="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Yes
                            <input type="radio" id="screenedNo" name="screened"  value="1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> No
                        </div>
                    </div>

                    <div id="screeningTypeSection" class="form-group">
                        <div class="form-group">
                            <label>Screening was through?</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="screeningVia" name="screeningType" value="164805AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> VIA
                                <input type="radio" id="screeningPap" name="screeningType" value="885AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Pap Smear
                                <input type="radio" id="screeningHPV" name="screeningType" value="1213AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> HPV-VAT
                            </div>
                        </div>
                    </div>

                    <div id="papSmearResultSection" class="form-group">
                        <div class="form-group">
                            <label>Result of past screening:</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="papPositive" name="papResult" value="1115AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Normal
                                <input type="radio" id="papNegative" name="papResult" value="1116AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Abnormal
                            </div>
                        </div>

                         <div id="papAbnormalTypeSection" class="form-group">
                            <div class="form-group">
                                <label>Type of abnormality</label>
                                <select id="abnormalityType" class="form-control">
                                    <option value="">Select One</option>
                                    <option value="d0de779e-a16d-4c26-8ab7-81c1675df1cf">LSIL/ASCUS</option>
                                    <option value="aa22c3d5-1093-4100-a883-a178418fb825">HSIL/ACG-H/ASG</option>
                                    <option value="36f0e286-8d04-43df-be27-e686ccc18160">SCC</option>
                                </select>
                            </div>
                         </div>

                        <div id="papAbnormalResultsSection" class="form-group">
                            <div class="form-group">
                                <label>Specify Abnormal Results</label>
                                <textarea id="papAbnormalDetails" name="papAbnormalDetails" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div id="hpvResultSection" class="form-group">
                        <div class="form-group">
                            <label>Result of past screening:</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="hpvPositive" name="hpvResult" value="703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Positive
                                <input type="radio" id="hpvNegative" name="hpvResult" value="664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Negative
                            </div>
                        </div>
                    </div>

                    <div id="hpvTreatmentSection" class="form-group">
                        <div class="form-group">
                            <label>Was treatment performed?</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="treatmentYes" name="hpvTreatment" value="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Yes
                                <input type="radio" id="treatmentNo" name="hpvTreatment" value="1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> No
                                <input type="radio" id="treatmentUnknown" name="hpvTreatment" value="1067AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Unknown
                            </div>
                        </div>

                        <div id="treatmentDetailsSection" class="form-group">
                            <div class="form-group">
                                <label>What type of treatment was performed?</label>
                                <div class="form-check form-check-inline">
                                    <input type="radio" id="treatmentCryo" name="treatmentType" value="162812AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Cryotherapy
                                    <input type="radio" id="treatmentLEEP" name="treatmentType" value="162810AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> LEEP
                                    <input type="radio" id="treatmentThermo" name="treatmentType" value="166706AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Thermocoagulation
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Date of Treatment</label>
                                <input type="date" id="treatmentDate" name="treatmentDate" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveScreening">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    jq(document).ready(function() {
        jq('#screeningTypeSection').hide();
        jq('#papSmearResultSection').hide();
        jq('#papAbnormalResultsSection').hide();
        jq('##papAbnormalTypeSection').hide();
        jq('#hpvResultSection').hide();
        jq('#hpvTreatmentSection').hide();
        jq('#treatmentDetailsSection').hide();

        let screeningHistoryObs = JSON.parse(`${screeningHistoryObsObjs}`);
        let lastScreeningEncounterUuid = "${lastScreeningEncounter?.uuid}";

        function resetinputs() {
            jq('input[name="papResult"]').prop('checked', false);
            jq('input[name="hpvResult"]').prop('checked', false);
            jq('input[name="hpvTreatment"]').prop('checked', false);
            jq('input[name="treatmentType"]').prop('checked', false);
        }

        jq('input[name="screened"]').change(function() {
            if (jq(this).val() === '1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#screeningTypeSection').show();
            } else {
                jq('#screeningTypeSection').hide();
                jq('#screeningTypeSection input').prop('checked', false);
                jq('#papSmearResultSection, #papAbnormalResultsSection, #papAbnormalTypeSection, #hpvResultSection, #hpvTreatmentSection, #treatmentDetailsSection').hide();
                //jq('#papSmearResultSection input, #papAbnormalResultsSection textarea, #hpvResultSection input, #hpvTreatmentSection input, #treatmentDetailsSection input').val('');
                resetinputs();
            }
        });

        jq('input[name="screeningType"]').change(function() {
            jq('#papSmearResultSection, #papAbnormalResultsSection, #papAbnormalTypeSection, #hpvResultSection, #hpvTreatmentSection, #treatmentDetailsSection').hide();
            //jq('#papSmearResultSection input, #papAbnormalResultsSection textarea, #hpvResultSection input, #hpvTreatmentSection input, #treatmentDetailsSection input').val('');
            resetinputs()

            if (jq(this).val() === '885AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#papSmearResultSection').show();
            } else if (jq(this).val() === '1213AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#hpvResultSection').show();
            }
        });

        jq('input[name="papResult"]').change(function() {
            if (jq(this).val() === '1116AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#papAbnormalResultsSection').show();
                jq('#papAbnormalTypeSection').show();
            } else {
                jq('#papAbnormalResultsSection').hide();
                jq('#papAbnormalTypeSection').hide();
                jq('#papAbnormalResultsSection textarea').val('');
                jq('#papAbnormalTypeSection select').val('');
            }
        });

        jq('input[name="hpvResult"]').change(function() {
            if (jq(this).val() === '703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#hpvTreatmentSection').show();
            } else {
                jq('#hpvTreatmentSection').hide();
                jq('#hpvTreatmentSection input').prop('checked', false);
                jq('#treatmentDetailsSection').hide();
                jq('#treatmentDetailsSection input').val('');
            }
        });

        jq('input[name="hpvTreatment"]').change(function() {
            if (jq(this).val() === '1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                jq('#treatmentDetailsSection').show();
            } else {
                jq('#treatmentDetailsSection').hide();
                jq('#treatmentDetailsSection input').val('');
            }
        });

        function padTo2Digits(num) {
            return num.toString().padStart(2, '0');
        }

        function formatDate(date) {
            return [
                date.getFullYear(),
                padTo2Digits(date.getMonth() + 1),
                padTo2Digits(date.getDate()),
            ].join('-');
        }

        jq('#cervicalCancerModal').on('shown.bs.modal', function (event) {
            if (screeningHistoryObs["pastScreening"]) {
                jq('input[name="screened"]').each(function() {
                    if (screeningHistoryObs["pastScreening"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="screened"]').attr("data-id", screeningHistoryObs["pastScreening"]?.id);
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["treatmentTypePerformed"]) {
                jq('input[name="treatmentType"]').each(function() {
                    if (screeningHistoryObs["treatmentTypePerformed"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="treatmentType"]').attr("data-id", screeningHistoryObs["treatmentTypePerformed"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["treatmentDate"]) {
                let dateObj = new Date(Date.parse(screeningHistoryObs["treatmentDate"].val));
                jq("#treatmentDate").val(formatDate(dateObj));
            }

            if (screeningHistoryObs["screeningThrough"]) {
                jq('input[name="screeningType"]').each(function() {
                    if (screeningHistoryObs["screeningThrough"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="screeningType"]').attr("data-id", screeningHistoryObs["screeningThrough"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["screeningResult"] && screeningHistoryObs["screeningThrough"].val === "885AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") {
                jq('input[name="papResult"]').each(function() {
                    if (screeningHistoryObs["screeningResult"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="papResult"]').attr("data-id", screeningHistoryObs["screeningResult"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["screeningResult"] && screeningHistoryObs["screeningThrough"].val === "1213AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") {
                jq('input[name="hpvResult"]').each(function() {
                    if (screeningHistoryObs["screeningResult"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="papResult"]').attr("data-id", screeningHistoryObs["screeningResult"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["treatmentPerformed"]) {
                jq('input[name="hpvTreatment"]').each(function() {
                    if (screeningHistoryObs["treatmentPerformed"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="hpvTreatment"]').attr("data-id", screeningHistoryObs["treatmentPerformed"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (screeningHistoryObs["treatmentTypePerformed"]) {
                jq('input[name="treatmentType"]').each(function() {
                    if (screeningHistoryObs["treatmentTypePerformed"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="treatmentType"]').attr("data-id", screeningHistoryObs["treatmentTypePerformed"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

        });

        jq('#saveScreening').on('click', function(e) {
            e.preventDefault();
            var formData = {};
            jq("#cervicalCancerForm").find('textarea, input:not(:radio)').each(function() {
                var thisVal = jq(this);
                if (thisVal.is(':visible')) {
                    formData[thisVal.attr('name')] =
                        {
                            "id": thisVal.attr("data-id") || '',
                            "val" : thisVal.val()
                        };
                } else {
                    formData[thisVal.attr('name')] =
                        {
                            "id": thisVal.attr("data-id") || '',
                            "val" : ''
                        };// Default value for hidden inputs
                }
            });

            // Handle radio buttons separately to manage unchecked groups
            jq("#cervicalCancerForm").find('input:radio').each(function() {
                var thisVal = jq(this);
                var name = thisVal.attr('name');
                if (thisVal.is(':visible')) {
                    if (thisVal.is(':checked')) {
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : thisVal.val()
                            };
                    } else if (!formData.hasOwnProperty(name)) {
                        // Set default empty string if no radio button is checked in its group
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : ''
                            };
                    }
                } else {
                    if (!formData.hasOwnProperty(name)) {
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : ''
                            };// Default value for hidden radio groups
                    }
                }
            });

            formData["lastScreeningEncounterUuid"] = lastScreeningEncounterUuid;
            let payload = JSON.stringify(formData);
            jq.post('${ ui.actionLink("botswanaemr", "srh/cervicalScreeningHistory", "saveCervicalScreening") }',
                {returnFormat: 'json', patientId: ${patient.patientId}, data: payload, visitId: ${visit.id}},
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patients cervical screening has been saved");
                    location.reload();
                }, 'json')
                .fail(function() {
                    console.log("error");
                })



        });

    });
</script>
