<script type="text/javascript">
    jq(function() {
        jq("#saveSrhVisit").click(function () {
            var params = {
                'visitId': <%= visit.id %> ,
                'visitDate': jq('visitDate').val(),
                'serviceType': jq('#serviceType option:selected').text(),
                'patientType': jq('#patientType').val()
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "srh/srhVisit", "saveVisitDetails")}', params).success(function(data) {
                jq().toastmessage('showNoticeToast', "Visit gate and reason captured successfully!");
                window.location = "/"+ OPENMRS_CONTEXT_PATH + <%= returnUrl %>;
            })
        });
    });
</script>

<div class="row">
    <div class="modal fade" id="visitDateAndReason" tabindex="-1"
         data-controls-modal="visitDateAndReason" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="visitDateAndReasonTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
                <form method="post" name="visitDateAndReasonForm" id="visitDateAndReasonForm">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title text-white" id="visitDateAndReasonTitle">Visit Date & Reason</h4>
                        <hr/>
                        <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                                aria-label="Close">
                            <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col">
                                <label for="visitDate"> Visit Date
                                    <span class="text-danger">*</span>
                                </label>
                                <input class="form-control" type="text" id="visitDate" name="visitDate" required/>
                                <script type="text/javascript">
                                    jQuery('#visitDate').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        showButtonPanel: true,
                                        "setDate": new Date(),
                                        dateFormat: "yy-mm-dd",
                                        yearRange: "-150:+0",
                                        maxDate: 0,
                                        "autoclose": true,
                                        onSelect: function (data) {
                                            jQuery("#visitDate").trigger('change');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <div class="form-group">
                                    <label for="serviceType"> Service Type
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="serviceType"
                                            class="form-control"
                                            name="serviceType">
                                        <option value="">Select One</option>
                                        <% visitTypes.each { visittype -> %>
                                        <% if (visittype != null && visittype?.name != null && visittype?.uuid != null) { %>
                                        <option value="<%=visittype.uuid %>"><%=visittype.name %></option>
                                        <% } %>
                                        <% } %>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="patientType"> Patient Type
                                        <span class="text-danger">*</span>
                                    </label>
                                    <select id="patientType"
                                            class="form-control"
                                            name="patientType">
                                        <option value="">Select One</option>
                                        <option value="1">BBA Booker</option>
                                        <option value="2">BBA Non-booker</option>
                                        <option value="3">Home Deliveries</option>
                                        <option value="4">ANC Booker Delivery</option>
                                        <option value="5">Non-Booker</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row actions mt-5 mb-3 px-3">
                            <div class="col-12 px-0">
                                <button type="button" id="cancel" data-dismiss="modal" class="btn btn-sm btn-dark bg-dark float-right ml-1">
                                    Close
                                </button>
                                <button type="submit" id="saveSrhVisit" class="btn btn-sm btn-primary float-right mr-0">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        jQuery(function () {
            jQuery('#visitDateAndReason').modal('show');
        });
    </script>
</div>