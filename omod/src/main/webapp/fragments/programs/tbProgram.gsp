<% if (hasVisit) { %>
<div class="row">
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.uuid, visitId: currentVisit?.id])}
</div>
${ui.includeFragment("botswanaemr", "widget/patientFlags", [patientId: patient.uuid, category: "tb"])}
<!-- Prescriptions Section -->
<div class="row mb-4">
    <div class="col-md-12">
        <div class="d-flex justify-content-between align-items-center bg-pale py-3 my-3">
            <h6 class="text-primary">PRESCRIPTIONS:</h6>
            <div>
                <button style="margin:10px;" type="button" class="btn btn-info">
                <a href="${ui.pageLink('botswanaemr', 'consultation/drugPrescription', [patientId: patient.id, visitId: currentVisit?.id])}" class="">
                Singular prescription
                </a>
                </button>
                <button style="margin:10px;" type="button" class="btn btn-info" data-toggle="modal" data-target="#addPrescriptionModal">Prescribe Drug</button>
            </div>
        </div>
    </div>
</div>
<% } %>
<% if (!enrolled) { %>
<!-- Pre Enrollment  -->
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col pb-4 card pl-0 pr-0">
                <div class="col pb-3">
                    TB Program Status
                </div>
                <div class="col">
                    <% if (!eligible) { %>
                    <span class="alert alert-sm alert-warning color-info" role="alert">
                    <i class="fa fa-info-circle"></i> Not Eligible for enrolment!
                    </span>
                    <% } %>
                    <% if (eligible) { %>
                    <button class="btn btn-sm btn-danger text-white right" data-toggle="modal" data-target="#beginProgramModal">
                    <i class="fa fa-arrow-circle-up"></i> Enroll into program
                    </button>
                    <% } %>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                <h5 class="text-primary text-left mt-3">PRE-ENROLMENT</h5>
                <hr class="divider pt-1 bg-primary"/>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h5>Forms</h5>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                    <% if (hasVisit) { %>
                    <a href="${ui.pageLink('botswanaemr', 'hts/htsClientProfile', [patientId: patient.id, returnUrl: returnUrl])}" class="list-group-item list-group-item-action" target="_blank">HIV Testing Services (HTS)</a>
                    <% availableForms.each { %>
                        <% if (it.form.name == 'TB Contact Listing') { %>
                            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                    patientId: patient.id,
                                    formUuid: it.form.uuid,
                                    visitId: currentVisit.id,
                                    returnUrl: returnUrl])}" class="list-group-item list-group-item-action">
                                Contact History
                            </a>
                        <% } else { %>
                            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                    patientId: patient.id,
                                    formUuid: it.form.uuid,
                                    visitId: currentVisit.id,
                                    returnUrl: returnUrl])}" class="list-group-item list-group-item-action">
                                ${it.form.name}
                            </a>
                        <% } %>
                    <% } %>
                    <% } else { %>
                    <div class="alert alert-warning text-danger" role="alert">
                        <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                    </div>
                    <% } %>
                </div>
            </div>
            <div class="col">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-success">Completed forms</a>
                    <% encounters.each { %>
                    <span class="list-group-item list-group-item-action">
                    ${it.form.name}
                    <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                        onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                    </i>
                    </span>
                    <% } %>
                </div>
            </div>
        </div>
        <div class="row" style="border: #0099CC solid 1px; margin-top:10px; padding: 10px; background: #0099CC;">
            <h6 class="text-white">Lab Results</h6>
        </div>
        <div class="row" style="border: #0099CC solid 1px; margin: auto; padding: 10px;">
            <div class="col-md-12 col-xl-12">
                <div class="fullwidth">
                    ${ui.includeFragment("botswanaemr", "programs/tb/tbLabResults", [patientId: patient])}
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
<!-- Post Enrollment and Contacts Section -->
<% if (enrolled) { %>
<div class="row mb-4">
    <div class="col-md-6">
        <h6 class="text-primary text-left">POST ENROLMENT</h6>
        <hr class="divider pt-1 bg-primary">
        <div class="row">
            <div class="col-md-6">
                <button class="btn btn-sm btn-info text-white mb-4">Discontinue from Program</button>
            </div>
            <div class="col-md-6 text-right">
                <button class="btn btn-sm btn-info text-white mb-4">Community TB Compliance Update</button>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <ul class="list-group mt-3">
                    <li class="list-group-item">
                        <a href="${ui.pageLink('botswanaemr', 'programs/tb/treatmentSummary', [patientId: patient.id, returnUrl: returnUrl])}"
                            class="list-group-item list-group-item-action" target="_blank">TB Treatment Summary</a>
                    </li>
                    <li class="list-group-item">
                        <a href="${ui.pageLink('botswanaemr', 'programs/tb/communityTbCareCompliance', [patientId: patient.id, returnUrl: returnUrl])}"
                            class="list-group-item list-group-item-action" target="_blank">Community TB Care Treatment Compliance</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <h6 class="text-primary text-left">Contact Tracing</h6>
        <hr class="divider pt-1 bg-primary">
        <div class="card">
            <div class="card-body">
                ${ui.includeFragment("botswanaemr", "programs/tb/listOfcontacts", [patientId: patient, returnUrl: returnUrl])}
            </div>
        </div>
    </div>
</div>
<div class="row mb-4">
    <div class="col-12">
        <div class="d-flex justify-content-between align-items-center py-3 my-3">
            <h6>TB Program Status</h6>
            <div>
                <button class="btn btn-primary btn-info text-white right ml-2" data-toggle="modal">
                    <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                        patientId: patient.id,
                        formUuid : changeRegimenForm,
                        visitId  : currentVisit.uuid,
                        returnUrl: returnUrl + 'programId=' + tbProgramId])}" class="">
                        <i class="fa fa-refresh" aria-hidden="true"></i> Change Regimen
                    </a>
                </button>
            </div>
        </div>
        <hr class="divider pt-1 bg-primary">
        <div class="row d-flex">
            <% if (enrolled) { %>
            <span class="badge-primary d-inline-block mr-2"><i class="fa fa-pill"></i> Enrolled</span>
            <% } %>
            <span class="badge-primary d-inline-block mr-2"><i class="fa fa-pill"></i> On Treatment: ${onTreatment}</span>
            <span class="badge-primary d-inline-block"><i class="fa fa-pill"></i>Current Regimen: ${currentRegimen ? currentRegimen : 'None'}</span>
        </div>
    </div>
</div>
<!-- Forms Section -->
<div class="row mb-4">
    <div class="col-md-12">
        <h6 class="text-primary text-left">FORMS</h6>
        <hr class="divider pt-1 bg-primary">
        <div class="form-group mt-3 text-right">
            <label for="select-date" class="mr-2">Select date</label>
            <input type="date" class="form-control d-inline-block w-auto" id="select-date">
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="list-group">
                            <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                            <% if (hasVisit) { %>
                            <a href="${ui.pageLink('botswanaemr', 'hts/htsClientProfile', [patientId: patient.id, returnUrl: returnUrl])}" class="list-group-item list-group-item-action" target="_blank">HIV Testing Services (HTS)</a>
                            <% availableForms.each { %>
                                <% if (it.form.name == 'TB Contact Listing') { %>
                                    <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                            patientId: patient.id,
                                            formUuid: it.form.uuid,
                                            visitId: currentVisit.id,
                                            returnUrl: returnUrl])}" class="list-group-item list-group-item-action">
                                        Contact Tracing
                                    </a>
                                <% } else { %>
                                    <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                            patientId: patient.id,
                                            formUuid: it.form.uuid,
                                            visitId: currentVisit.id,
                                            returnUrl: returnUrl])}" class="list-group-item list-group-item-action">
                                        ${it.form.name}
                                    </a>
                                <% } %>
                            <% } %>
                            <% } else { %>
                            <div class="alert alert-warning text-danger" role="alert">
                                <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                            </div>
                            <% } %>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-header list-group-item list-group-item-action bg-success">Completed Forms</div>
                        <% encounters.each { %>
                        <span class="list-group-item list-group-item-action">
                        ${it.form.name}
                        <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                            onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid,programId:program.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                        </i>
                        </span>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- DOT Compliance Section -->
<div class="row mb-4">
    <div class="col-md-12">
        <h6 class="text-primary">DIRECT OBSERVATION THERAPY</h6>
        <hr class="divider pt-1 bg-primary">
    </div>
</div>
<div class="row mb-4">
    <div class="col-md-6">
        <h6 class="text-primary">DOT COMPLIANCE</h6>
        <hr class="divider pt-1 bg-primary">
        <div class="row" style="border: #0099CC solid 1px; margin: auto; padding: 10px;">
            <div class="col-md-12 col-xl-12">
                <div class="fullwidth align-center">
                    ${ui.includeFragment("botswanaemr", "pillCount", [patientId: patient])}
                </div>
            </div>
        </div>
        <h6 class="text-primary text-left">WEIGHT TRACKING</h6>
        <hr class="divider pt-1 bg-primary">
        <div class="row" style="border: #0099CC solid 1px; margin: auto; padding: 10px;">
            <div class="col-md-12 col-xl-12">
                <div class="fullwidth">
                    ${ui.includeFragment("botswanaemr", "weightTracking", [patientId: patient])}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <h6 class="text-primary text-left">DOT</h6>
        <hr class="divider pt-1 bg-primary">
        <div id="calender-div" class="" style="border: #0099CC solid 1px; margin:auto; padding: 10px;">
            <div class="fullwidth">
                ${ui.includeFragment("botswanaemr", "drugTrackingCalendar", [patientId: patient])}
            </div>
        </div>
    </div>
</div>
<% } %>
<div>
    ${ui.includeFragment("botswanaemr", "programs/tb/dotBatchUpdates", [patientId: patient])}
</div>