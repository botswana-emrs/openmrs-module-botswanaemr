<div class="row">
    <div class="table-responsive">
        <table id="cultureDstReport" class="table table-sm table-bordered" style="text-align: start">
            <thead>
            <tr>
                <th colspan="2">Diabetes Mellitus Screening</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong>Date of test</strong></td>
                <td><strong>Blood Sugar Result (mmol/L)</strong></td>
            </tr>
            <% results.each {%>
            <tr>
                <td>${it.dateOfTest ? it.dateOfTest : ''}</td>
                <td>${it.bloodSugarResult ? it.bloodSugarResult : ''}</td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
</div>
