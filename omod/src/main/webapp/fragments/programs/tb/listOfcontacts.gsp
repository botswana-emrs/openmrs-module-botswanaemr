<div id="tbListOfContacts">
    <% if (patientsInContact.size() == 0) { %>
    <div class="row no-data-section">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No contacts</p>
    </div>
    <% } else { %>

    <div style="margin: 4px; padding: 8px;">
        <div class="table-responsive table-sm table-borderless">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <% patientsInContact.each { %>
                <tr>
                    <td>
                        ${it.getPatient()?.getGivenName()}
                        ${it.getPatient()?.getMiddleName() != null ? it.getPatient()?.getMiddleName() : ''}
                        ${it.getPatient()?.getFamilyName() != null ? it.getPatient()?.getFamilyName() : ''}
                    </td>
                    <td>${it.getPatient()?.getAge()}</td>
                    <td>
                        <% if (it.isScreen()) { %>
                        <a href="#" class="color-success">
                            <i class="fa fa-check-circle"></i>
                            Screened
                        </a>
                        <% } else { %>
                        <a class="color-primary" href="${ui.pageLink("botswanaemr", "enterForm",
                                [
                                        patientId: it.getPatient()?.getUuid(),
                                        formUuid : 'd930f5a8-760f-4255-9bd2-ee153f4dc07b',
                                        returnUrl: '/openmrs/botswanaemr/programs/programs.page?patientId=' + patient.id,
                                        visitId  : it.getVisit()?.getUuid()
                                ]
                        )}">
                            screen
                        </a>
                        <% } %>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
    <% } %>
</div>
