<% results.each { %>
<div>
    <div class="table-responsive-sm">
        <table id="chestXRayResult" class="table table-sm table-bordered">
            <thead>
            <tr>
                <th class="text-center" colspan="2">Chest X-Ray Results</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-start"><strong>NO. ${it.chestNumber ? it.chestNumber : ''}</strong></td>
            </tr>
            <tr class="align-content-start">
                <td><div>
                    <img src="${it.xrayImage}" alt="x-ray-image" width="500px" height="300px">
                </div></td>
            </tr>
            <tr>
                <td class="text-start">Date: ${it.date ? it.date : ''}</td>
            </tr>
            <tr>
                <td class="text-start">${it.cxrFindings ? it.cxrFindings : ''}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<% } %>


