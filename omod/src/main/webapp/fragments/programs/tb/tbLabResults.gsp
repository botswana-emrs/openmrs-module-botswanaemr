<div class="row">
    <div class="table-responsive">
        <table id="labResultsTable" class="table table-sm table-bordered" style="text-align: start">
            <thead>
            <tr>
                <th colspan="4">Sputum Examination Report</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong>At month</strong></td>
                <td><strong>Date</strong></td>
                <td><strong>Test</strong></td>
                <td><strong>Result</strong></td>
            </tr>
            <% results.each {%>
            <tr>
                <td>${it.atWhichMonth ? it.atWhichMonth : ''}</td>
                <td>${it.date ? it.date : ''}</td>
                <td>${it.tbTest ? it.tbTest : ''}</td>
                <td>${it.result ? it.result : ''}</td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
</div>
