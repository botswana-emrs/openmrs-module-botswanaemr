<div class="row">
    <div class="table-responsive">
        <table id="cultureDstReport" class="table table-sm table-bordered" style="text-align: start">
            <thead>
            <tr>
                <th colspan="3">Culture and DST Reports</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Date</td>
                <td>Drug</td>
                <td>Resistance</td>
            </tr>
            <% results.each {%>
            <tr>
                <td>${it.date ? it.date : ''}</td>
                <td>${it.drug ? it.drug : ''}</td>
                <td>${it.resistance ? it.resistance : ''}</td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
</div>
