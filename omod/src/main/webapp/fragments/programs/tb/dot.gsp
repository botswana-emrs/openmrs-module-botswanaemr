<div class="row">
    <div class="table-responsive">
        <table id="dot-calendar" class="table table-sm table-bordered" style="text-align: center">
            <thead>
            <tr>
                <th>Month Date</th>
                <% days.each { %>
                <th>${it}</th>
                <% } %>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Month 1</td>
                <% mOne.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 2</td>
                <% mTwo.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 3</td>
                <% mThree.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table id="dot-calendar-key" class="table table-sm table-bordered" style="text-align: left">
            <thead>
                <tr>
                    <th colspan="2">KEY</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> <strong>X</strong> </td>
                    <td> Not started or Not administered </td>
                </tr>
                <tr>
                    <td> <strong>User Initial</strong> </td>
                    <td> Who administered the drug </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
