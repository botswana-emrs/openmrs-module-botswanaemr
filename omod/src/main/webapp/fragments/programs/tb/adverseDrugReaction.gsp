<div class="row">
    <div class="table-responsive">
        <table id="cultureDstReport" class="table table-sm table-bordered" style="text-align: start">
            <thead>
            <tr>
                <th colspan="3">Adverse Drug Reaction</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><strong>Reaction</strong></td>
                <td><strong>Suspected Drug</strong></td>
                <td><strong>Other Drugs</strong></td>
            </tr>
            <% results.each {%>
            <tr>
                <td>${it.adverseDrugReaction ? it.adverseDrugReaction : ''}</td>
                <td>${it.suspectedDrugs ? it.suspectedDrugs : '' }</td>
                <td>${it.otherDrugs ? it.otherDrugs : ''}</td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
</div>
