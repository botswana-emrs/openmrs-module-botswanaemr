<script>
    jQuery(() => {
        jQuery("#addDOTModal").appendTo("body");

        let hasMoreRows = 1;

        jQuery("#btn_add_new_dot_row").click(function () {
            hasMoreRows ++;
            jQuery('#tbDot tbody>tr:last')
                .clone(true)
                .insertAfter('#tbDot tbody>tr:last').hide().fadeIn().find('input').each(function () {
                jQuery(this).val('');
            });
        });

        jQuery("#delete-row").click(function () {
            if (hasMoreRows > 1) {
                jQuery(this).parents("tr").remove();
                hasMoreRows --;
            } else {
                jq().toastmessage('showNoticeToast', "Sorry, you are not allowed to remove this row");
            }
        });

        jQuery("#date").attr("min", "${enrollmentDate}");

        // jQuery('#administerBy').change(function () {
        //     const val = jQuery(this).val();
        //     if (val === '1574AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
        //         jQuery('#provider').show();
        //     } else {
        //         jQuery('#provider').hide();
        //     }
        // });


        const getInputValues = (name) => {
            const values = [];
            jQuery("#addDOTForm input[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const getSelectValues = (name) => {
            const values = [];
            jQuery("#addDOTForm select[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const validateFields = (data) => {
            const nullFields = [];
            for (const value of data) {
                if (!value) {
                    nullFields.push(value);
                }
            }
            return nullFields;
        }

        jQuery('#btnSaveDOT').click((event) => {
            event.preventDefault();

            const treatmentTypes = getSelectValues('treatmentType');
            const drugs = getSelectValues('drug');
            const providers = getSelectValues('provider');
            const administers = getSelectValues('administerBy');
            const datesSet = getInputValues('date');
            const remarks = getInputValues('remarks');
            const weight = getInputValues('weight');

            const errors = [];
            errors.push(...validateFields(treatmentTypes), ...validateFields(drugs),
                ...validateFields(administers), ...validateFields(datesSet), ...validateFields(providers),
                ...validateFields(remarks), ...validateFields(weight));

            const payload = [];
            for (let i = 0; i < treatmentTypes.length; i++) {
                payload.push({
                    treatmentType: treatmentTypes[i],
                    drug: drugs[i],
                    administerBy: administers[i],
                    date: datesSet[i],
                    remarks: remarks[i],
                    weight: weight[i],
                    provider: providers[i]
                });
            }

            jq.getJSON('${ ui.actionLink("botswanaemr", "programs/tb/dotBatchUpdates", "dotBatchUpdate")}', {
                'patientId': ${patient.id},
                'data': JSON.stringify(payload)
            }).success(function (data) {
                jq().toastmessage('showNoticeToast', "DOT batch updates have been submitted successfully");
                location.reload();
            }).error(function (data) {
                jq().toastmessage('showErrorToast', "An error occurred while submitting DOT batch updates");
            });
        });
    });
</script>

<style>
table#tbDot th {
    font-weight: normal;
    font-size: 14px;
}

table#tbDot input {
    max-width: 160px;
}

table#tbDot select {
    min-width: 120px;
    max-width: 160px !important;
}

</style>

<div class="modal fade" id="addDOTModal" data-controls-modal="AddDOTModalControls"
     data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="AddDOTModalTitle">

    <div id="addDOTFormData" class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable"
         role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="addDOTTitle">DOT BATCH UPDATES</h5>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" id="addDOTForm">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover w-auto" id="tbDot">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        Date
                                    </th>
                                    <th class="text-center">
                                        Treatment Phase
                                    </th>
                                    <th class="text-center">
                                        Drug
                                    </th>
                                    <th class="text-center">
                                        Weight
                                    </th>
                                    <th class="text-center">
                                        AdministerBy
                                    </th>
                                    <th class="text-center">
                                        Facility Name
                                    </th>
                                    <th class="text-center">
                                        Select Provider
                                    </th>
                                    <th class="text-center">
                                        Remarks
                                    </th>
                                    <th class="text-center">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id='row'>
                                    <td>
                                        <label for="date"></label>
                                        <input type="date" name="date[]" id="date" class="form-control input-sm">
                                    </td>
                                    <td>
                                        <label for="treatmentType"></label>
                                        <select name="treatmentType[]" id="treatmentType" class="form-control input-sm">
                                            <option value="">select treatment phase</option>
                                            <% treatmentTypes.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="drug"></label>
                                        <select name="drug[]" id="drug" class="form-control input-sm">
                                            <option value="">select drug</option>
                                            <% drugs.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="weight"></label>
                                        <input type="text" name="weight[]" id="weight" value="${lastWeight}"
                                               class="form-control input-sm">
                                    </td>
                                    <td>
                                        <label for="administerBy"></label>
                                        <select name="administerBy[]" id="administerBy" class="form-control input-sm">
                                            <option value="">select administer by</option>
                                            <% administers.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="facilityName"></label>
                                        <select name="facilityName[]" id="facilityName" class="form-control input-sm">
                                            <option value="">select Facility Name </option>
                                            <% facilities.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="provider"></label>
                                        <select name="provider[]" id="provider" class="form-control input-sm">
                                            <option value="">select provider</option>
                                            <% providers.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getName()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="remarks"></label>
                                        <input type="text" name="remarks[]" id="remarks" class="form-control input-sm">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger" id="delete-row">
                                            <i class="fa fa-remove" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-12">
                            <button type="button" class="btn btn-dashed-button rounded"
                                    id="btn_add_new_dot_row">+ Add New Row</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="cancel" type="button" class="btn btn-dark bg-dark" data-dismiss="modal"
                                aria-label="Close">Close</button>
                        <button id="btnSaveDOT" type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
