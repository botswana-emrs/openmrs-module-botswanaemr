<div class="row">
    <div class="table-responsive">
        <table id="continuation-dot-calendar" class="table table-sm table-bordered" style="text-align: center">
            <thead>
            <tr>
                <th>Month Date</th>
                <% days.each { %>
                <th>${it}</th>
                <% } %>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Month 4</td>
                <% mFour.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 5</td>
                <% mFive.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 6</td>
                <% mSix.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 7</td>
                <% mSeven.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 8</td>
                <% mEight.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            <tr>
                <td>Month 9</td>
                <% mNine.each { %>
                <td>${it}</td>
                <% } %>
            </tr>
            </tbody>
        </table>
    </div>
</div>
