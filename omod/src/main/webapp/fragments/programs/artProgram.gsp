<script type="text/javascript">
    let redirectToClientProfilePage = () => {
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/hts/htsClientProfile.page?patientId=$patient.id";
    };
</script>
<style>
    .rounded {
        padding: .5rem 1rem;
    }
    .success-td {
        background-color: #28a745 !important;
        color: white;
    }
</style>
<% if (hasVisit) { %>
<div class="row">
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.uuid, visit: currentVisit, autoActivateConsultation: true, onArt: onArt])}
</div>
<div class="row">
    <div class="col-md-12">
        <button style="margin:10px;" type="button" class="btn btn-default left" onclick="redirectToClientProfilePage()">Hts Profile</button>

        <button style="margin:10px;" type="button" class="btn btn-default left" data-toggle="modal"
                data-target="#addPrescriptionModal" data-service-type="${currentVisit.visitType.name}" >Prescribe Drug</button>
        <button style="margin:10px;" type="button" class="btn btn-default right">
            <a href="${ui.pageLink('botswanaemr', 'consultation/drugPrescription', [patientId: patient.id, visitId: currentVisit.id])}" class="">
               <i class="fa fa-folder-o"></i> Singular prescription
            </a>
        </button>
        <% if (needsTesting) { %>
        <button style="margin:10px;" type="button" class="btn btn-success right">
            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                    patientId: patient.id,
                    formUuid : newTestFormUuid,
                    visitId  : currentVisit.uuid,
                    returnUrl: returnUrl + 'programId=' + artProgramId])}"
               class="">New Test
            </a>
        </button>
        <% } %>
        <% if (needsVerification) { %>
        <button style="margin:10px;" type="button" class="btn btn-success right">
            <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                    patientId: patient.id,
                    formUuid : verificationFormUuid,
                    visitId  : currentVisit.uuid,
                    returnUrl: returnUrl + 'programId=' + artProgramId])}"
               class="">New Test Verification
            </a>
        </button>
        <% } %>
    </div>
</div>
<% } %>
<% if (!enrolled) { %>
    <div class="row">
        <div class="col-12 pb-4">
            <div class="card pl-0 pr-0">
                <div class="row">
                    <div class="col pb-0">
                        <h5 class="text-primary text-left mt-3">ART Program Status</h5>                        
                    </div>
                </div>
                <div class="col-md-12">
                    <hr class="divider pt-1 bg-primary"/>
                </div>
                <div class="col">
                    <% if (!eligible) { %>
                    <span class="alert alert-sm color-danger" role="alert">
                        <span class="badge badge-info rounded"><i class="fa fa-warning"></i> Not Eligible for enrolment!</span>
                    </span>
                    <% } else { %>
                    <span class="badge badge-sm badge-warning color-info m-l-2 m-r-2" role="alert">
                        <i class="fa fa-info-circle"></i> Eligible for enrolment!
                    </span>
                    <button class="btn btn-sm btn-info text-white right ml-2" data-toggle="modal"
                            onclick="location.href = '${ui.pageLink('botswanaemr', 'enterForm', [
                            patientId: patient.id,
                            formUuid : artProgramEnrolmentForm.form.uuid,
                            visitId  : currentVisit.uuid,
                            returnUrl: returnUrl + 'programId=' + artProgramId])}'">
                        <i class="fa fa-arrow-circle-up"></i> Enroll
                    </button>
                    <button class="btn btn-sm btn-info text-white right ml-2" data-toggle="modal">
                        <a  href="${ui.pageLink("botswanaemr", "lab/addLaboratoryTestOrders?patientId=" + patient.uuid)}" class="" target="_blank">
                            <i class="fa fa-flask" aria-hidden="true"></i> Lab Order
                        </a>
                    </button>
                    <% } %>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col">
            <h5 class="text-primary text-left mt-3">PRE-ENROLMENT</h5>
            <hr class="divider pt-1 bg-primary"/>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                <% if (hasVisit) { %>
                    <% availablePreArtForms.each { %>
                        <% if (formsWithTbSection.contains(it.form.uuid)) { %>
                            <a  href="${ui.pageLink('botswanaemr', 'art/artForms', [
                                        encounterId: it.uuid,
                                        patientId: patient.id,
                                        visitId  : currentVisit.id,
                                        selectedFormUuid : it.form.uuid,
                                        returnUrl: returnUrl + 'programId=' + artProgramId])}"
                                class="list-group-item list-group-item-action">
                                ${it.form.name}
                            </a>
                        <% } else { %>

                            <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                                            patientId: patient.id,
                                            formUuid : it.form.uuid,
                                            visitId  : currentVisit.uuid,
                                            returnUrl: returnUrl + 'programId=' + artProgramId])}"
                                class="list-group-item list-group-item-action">
                                ${it.form.name}
                            </a>
                        <% } %>
                    <% } %>
                <% } else { %>
                    <div class="alert alert-warning text-danger" role="alert">
                        <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                    </div>
                <% } %>
            </div>
        </div>

        <div class="col">
            <table class="table table-striped table-bordered">
                <thead style="background-color: #28a745; color: white;" class="bg-success">
                <tr>
                    <th class="success-td">Completed Forms</th>
                    <th class="success-td">Date</th>
                    <th class="success-td">Actor</th>
                </tr>
                </thead>
                    <tbody>
                        <% encounters.each { %>
                            <tr>
                                <td>
                                    <% if (formsWithTbSection.contains(it.form.uuid)) { %>
                                        <a href="${ui.pageLink('botswanaemr', 'art/artForms', [encounterId: it.uuid,encounterTypeUuid: it.encounterType.uuid, patientId: patient.id, selectedFormUuid : it.form.uuid, visitId  : currentVisit.id, formUuid: 'ggg', returnUrl: returnUrl + 'programId=' + artProgramId]) }"
                                           class="text-decoration-underline">
                                            ${it.form.name}
                                        </a>
                                    <% } else { %>
                                    <a href="${ui.pageLink('botswanaemr', 'editEncounter', [encounterId: it.uuid, patientId: patient.id, returnUrl: returnUrl + 'programId=' + artProgramId]) }"
                                       class="text-decoration-underline">
                                        ${it.form.name}
                                    </a>
                                    <% } %>
                                    <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                                       onclick="location.href = '${ui.pageLink('botswanaemr', 'editEncounter', [encounterId: it.uuid, patientId: patient.id, returnUrl: returnUrl + 'programId=' + artProgramId]) }'">
                                    </i>
                                </td>
                                <td>${it.encounterDatetime.format('yyyy-MM-dd hh:mm a')}</td>
                                <td>${ui.format(it.creator ?: "")}</td>
                            </tr>
                        <% } %>
                    </tbody>
            </table>
        </div>
    </div>
<div class="row">
    <div class="col">
        ${ ui.includeFragment("botswanaemr", "hts/htsPastTests", [patientId: patient.uuid])}
    </div>
</div>
<% } %>
<% if (enrolled) { %>
<div class="row">
    <div class="col">
        <div class="col pb-4 card">
            <div class="row">
                <div class="col pb-0">
                    <strong>ART Program Status</strong>
                </div>
                <div class="col pb-0">
                    <% if (!onArt || artStatus.toLowerCase().contains('stop')) { %>
                        <% if (artReady) { %>
                            <button class="btn btn-sm btn-success text-white right" data-toggle="modal"
                                    onclick="location.href = '${ui.pageLink('botswanaemr', 'enterForm', [
                                            patientId: patient.id,
                                            formUuid : artEligibilityForm.form.uuid,
                                            visitId  : currentVisit.uuid,
                                            returnUrl: returnUrl + 'programId=' + artProgramId])}'">
                                <i class="fa fa-medkit"></i> Start ART
                            </button>
                        <% } %>
                    <% } else { %>
                <button class="btn btn-md btn-danger text-white right ml-2" data-toggle="modal">
                    <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                        patientId: patient.id,
                        formUuid : stopArtForm,
                        visitId  : currentVisit.uuid,
                        returnUrl: returnUrl + 'programId=' + artProgramId])}" class="">
                        <i class="fa fa-stop-circle" aria-hidden="true"></i> Stop ART
                    </a>
                </button>
                <button class="btn btn-md btn-info text-white right ml-2" data-toggle="modal">
                    <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                        patientId: patient.id,
                        formUuid : changeRegimenForm,
                        visitId  : currentVisit.uuid,
                        returnUrl: returnUrl + 'programId=' + artProgramId])}" class="">
                        <i class="fa fa-refresh" aria-hidden="true"></i> Change Regimen
                    </a>
                </button>
                <button class="btn btn-md btn-info text-white right ml-2" data-toggle="modal">
                        <a  href="${ui.pageLink("botswanaemr", "lab/addLaboratoryTestOrders?patientId=" + patient.uuid)}" class="" target="_blank">
                            <i class="fa fa-flask" aria-hidden="true"></i> Lab Order
                        </a>
                    </button>
                <% } %>
                </div>
            </div>
            <div class="col-12">
                <hr class="divider pt-1 bg-primary"/>
            </div>
            <div class="col">
                <span class="badge badge-primary"><i class="fa fa-info-circle"></i> Enrolled</span>
                <% if (!onArt || artStatus.toLowerCase().contains('stop')) { %>
                    <% if (!artReady) { %>
                        <span class="badge badge-warning"><i class="fa fa-warning"></i> Not ready for ART</span>
                    <% } else { %>
                        <span class="badge badge-info"><i class="fa fa-info-circle"></i> Ready for ART</span>
                    <% } %>
                <% } else { %>
                    <span class="badge badge-primary"><i class="fa fa-info-circle"></i> On ART</span>
                    <span class="badge badge-primary"><i class="fa fa-pill"></i> Current Regimen: ${currentRegimen}</span>
                <% } %>
                <% if (onCtx) { %>
                    <span class="badge badge-primary"><i class="fa fa-info-circle"></i> On CTX</span>
                <% } %>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col">
                <h5 class="text-primary text-left mt-3">POST-ENROLMENT</h5>
            </div>
            <div class="col pt-2">
                <button class="btn btn-sm btn-info text-white right" data-toggle="modal" data-target="#endProgramModal">
                    <i class="fa fa-undo"></i> Discontinue from program
                </button>
            </div>
            <div class="col-md-12">
                <hr class="divider pt-1 bg-primary"/>            
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                    <% if (hasVisit && enrolled) { %>
                    <% if (consultationForm != null) {%>
                        <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                            patientId: patient.id,
                            formUuid : consultationForm.form.uuid,
                            visitId  : currentVisit.uuid,
                            returnUrl: returnUrl + 'programId=' + artProgramId])}" class="list-group-item list-group-item-action">
                            ${consultationForm.form.name}
                        </a>
                    <% } %>
                    <% availablePreArtForms.each { %>
                        <% if (formsWithTbSection.contains(it.form.uuid)) { %>
                            <a  href="${ui.pageLink('botswanaemr', 'art/artForms', [
                                        encounterId: it.uuid,
                                        patientId: patient.id,
                                        visitId  : currentVisit.id,
                                        selectedFormUuid : it.form.uuid,
                                        returnUrl: returnUrl + 'programId=' + artProgramId])}"
                                class="list-group-item list-group-item-action">
                                ${it.form.name}
                            </a>
                        <% } else { %>
                            <a  href="${ui.pageLink('botswanaemr', 'enterForm', [
                                        patientId: patient.id,
                                        formUuid : it.form.uuid,
                                        visitId  : currentVisit.uuid,
                                        returnUrl: returnUrl + 'programId=' + artProgramId])}"
                                class="list-group-item list-group-item-action">
                                ${it.form.name}
                            </a>
                        <% } %>

                    <% } %>
                    <% } else { %>
                    <div class="alert alert-warning text-danger" role="alert">
                        <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                    </div>
                    <% } %>
                </div>
            </div>

            <div class="col">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="success-td">Completed Forms</th>
                        <th class="success-td">Date</th>
                        <th class="success-td">Actor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <% encounters.each { %>
                    <tr>
                        <td>
                            <% if (formsWithTbSection.contains(it.form.uuid)) { %>
                                <a href="${ui.pageLink('botswanaemr', 'art/artForms', [encounterId: it.uuid,encounterTypeUuid: it.encounterType.uuid, patientId: patient.id, selectedFormUuid : it.form.uuid, visitId  : currentVisit.id, formUuid: 'ggg', returnUrl: returnUrl + 'programId=' + artProgramId]) }"
                                   class="text-decoration-underline">
                                    ${it.form.name}
                                </a>
                            <% } else { %>
                                <a href="${ui.pageLink('botswanaemr', 'editEncounter', [encounterId: it.uuid, patientId: patient.id, returnUrl: returnUrl + 'programId=' + artProgramId]) }"
                                   class="text-decoration-underline">
                                    ${it.form.name}
                                </a>
                            <% } %>
                            <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                               onclick="location.href = '${ui.pageLink('botswanaemr', 'editEncounter', [encounterId: it.uuid, patientId: patient.id, returnUrl: returnUrl + 'programId=' + artProgramId]) }'">
                            </i>
                        </td>
                        <td>${it.encounterDatetime.format('yyyy-MM-dd hh:mm a')}</td>
                        <td>${ui.format(it.creator ?: "")}</td>
                    </tr>
                    <% } %>
                    </tbody>
                </table>
            </div>
        </div>
        <% if (transferOutFormEncounter != "") { %>
            <div class="row">
                <div class="col-12">
                    <hr class="divider pt-1 bg-primary mb-3"/>
                    ${ui.includeFragment("botswanaemr", "art/transferOutHistory", [patientId: patient.uuid, visit: currentVisit, encounter: transferOutFormEncounter])}
                </div>
            </div>
        <% } %>
    </div>
</div>
<% } %>
