<script>
    jQuery(document).ready(function () {
        jQuery('#btn-enroll-cpt').click((event) => {
            event.preventDefault();
            jq.getJSON('${ ui.actionLink("botswanaemr", "programs/cptProgram", "enrollIntoCPT")}', {
                'patientId': ${patient.id}
            }).success(function (data) {
                jq().toastmessage('showNoticeToast', "Patient Enrolled successfully");
                location.reload();
            }).error(function (data) {
                jq().toastmessage('showErrorToast', "Error Enrolling patient");
            });
        });

        const patientProgramUuid  = jQuery("#patientProgramUuid").val();

        jQuery('#btn-discountinue-cpt').click((event) => {
            event.preventDefault();
            jq.getJSON('${ ui.actionLink("botswanaemr", "programs/cptProgram", "discontinueCPTProgram")}', {
                'patientProgramUuid': patientProgramUuid
            }).success(function (data) {
                jq().toastmessage('showNoticeToast', "Patient Discontinued successfully");
                location.reload();
            }).error(function (data) {
                jq().toastmessage('showErrorToast', "Error discontinuing patient for CPT Program");
            });
        });
    });
</script>
<% if (!enrolled) { %>
<div class="row">
    <div class="col">
        <div class="row mb-2">
            <div class="col" style="margin: 10px;">
                <% if (!eligible) { %>
                <span class="alert alert-sm alert-warning color-info" role="alert">
                    <i class="fa fa-info-circle"></i> Not Eligible for enrolment!
                </span>
                <% } %>

                <% if (eligible) { %>
                <button id="btn-enroll-cpt" class="btn btn-sm btn-info text-white">
                    <i class="fa fa-arrow-circle-up"></i> Enroll into program
                </button>
                <% } %>
            </div>
        </div>
    </div>
</div>
<% } %>
<% if (enrolled) { %>
</script>
<div id="active-enrollment">
    <div style="padding: 8px;">
        <div style="border: #0099CC solid 1px; margin: auto; padding: 10px; background: #0099CC;">
            <h5 class="text-white center">Active CPT Program Enrollment</h5>
        </div>
    </div>

    <div style="margin: 4px; padding: 8px;">
        <div class="table-responsive table-sm table-borderless">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Date of Enrollment</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <% activePrograms.each { %>
                <tr>
                    <td>${it.getProgram()?.getDescription()}</td>
                    <td>${it.getDateEnrolled()}</td>
                    <td>
                        <button class="btn btn-sm btn-danger" id="btn-discountinue-cpt">
                            <i class="fa fa-arrow-circle-down"></i> Discontinue
                        </button>
                        <input type="text" id="patientProgramUuid" value="${it.getUuid()}" class="form-control input-sm hidden">
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<% } %>
