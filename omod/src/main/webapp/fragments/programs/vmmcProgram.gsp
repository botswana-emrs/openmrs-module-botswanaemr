<% if (!enrolled) { %>
    <div class="row mb-2">
        <div class="col">
            <h5 class="text-primary text-left mt-3">PRE-ENROLMENT</h5>
            <hr class="divider pt-1 bg-primary"/>
        </div>
        <div class="col">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h5>Forms</h5>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                <% if (hasVisit) { %>
                <% availableVmmcForms.each { %>
                <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                        patientId: patient.id,
                        formUuid : it.form.uuid,
                        visitId  : currentVisit.uuid,
                        returnUrl: returnUrl])}"
                   class="list-group-item list-group-item-action">
                    ${it.form.name}
                </a>
                <% } %>
                <% } else { %>
                <div class="alert alert-warning text-danger" role="alert">
                    <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                </div>
                <% } %>
            </div>
        </div>

        <div class="col">
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action bg-success">Completed forms</a>
                <% encounters.each { %>
                <span class="list-group-item list-group-item-action">
                    ${it.form.name}
                    <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                       onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                    </i>
                </span>
                <% } %>
            </div>
        </div>
    </div>
<% } %>
<% if (enrolled) { %>
    <div class="row">
        <div class="col">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="text-primary text-left mt-3">POST-ENROLMENT</h5>
                    <hr class="divider pt-1 bg-primary"/>
                </div>

                <div class="col">
    <!--                <button class="btn btn-sm btn-info text-white right" data-toggle="modal" data-target="#endProgramModal">
                        <i class="fa fa-undo"></i> Discontinue from program
                    </button>
    -->
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>Forms</h5>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action bg-primary">Available forms</a>
                        <% if (hasVisit) { %>
                        <% availableVmmcForms.each { %>
                        <a href="${ui.pageLink('botswanaemr', 'enterForm', [
                                patientId: patient.id,
                                formUuid : it.form.uuid,
                                visitId  : currentVisit.uuid,
                                returnUrl: returnUrl])}"
                           class="list-group-item list-group-item-action">
                            ${it.form.name}
                        </a>
                        <% } %>
                        <% } else { %>
                        <div class="alert alert-warning text-danger" role="alert">
                            <strong><i class="fa fa-warning"></i> Please start a patient visit to proceed!</strong>
                        </div>
                        <% } %>
                    </div>
                </div>

                <div class="col">
                    <a href="#" class="list-group-item list-group-item-action bg-success">Completed forms</a>
                    <% encounters.each { %>
                    <span class="list-group-item list-group-item-action">
                        ${it.form.name}
                        <i class="icon-pencil edit-action" title="${ui.message('coreapps.edit')}"
                           onclick="location.href = '${ ui.pageLink('botswanaemr', 'editEncounter', [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }'">
                        </i>
                    </span>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
<% } %>
