${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}
<script>

    const allergiesPatientUuid = "${patient.patient.uuid}";

    jQuery(function () {
        loadAllergies();
        jQuery("#editAllergiesModal").appendTo("body");
        jQuery("#viewPreviousChecksModal").appendTo("body");

        jQuery('#editAllergiesModal').on('hidden.bs.modal', function () {
            jQuery('#saveAllergies').prop('disabled', false);
        });
    });
    
    function loadAllergies() {
        jq.getJSON('${ui.actionLink("botswanaemr", "allergies", "getAllergies")}',
            {'patientId': allergiesPatientUuid}
        ).done(function (data) {
            renderAllergies(data);
        }).fail(function (jqXHR) {
            console.error('Error loading allergies');
        });
    }

    function renderAllergies(data) {
        const allergiesSection = jQuery('.allergies-body');
        allergiesSection.empty();

        let listHtml = '<ul>';
        data.allergies.forEach(function (allergy) {
            let reactionsHtml = '';
            if (allergy.reactions) {
                allergy.reactions.split(',').forEach(function (reaction, index) {
                    reactionsHtml += `<span class="allergyReaction normal-text">\${index > 0 ? ',' : '&rArr;'} \${ui.encodeHtmlContent(reaction)}</span>`;
                });
            }

            listHtml += `
                <li>
                    <div class="row">
                        <div class="col-6 zero-padding">
                            <span class="allergen normal-text">\${allergy.allergen}</span>
                            \${reactionsHtml}
                        </div>
                        <div class="col-6 zero-padding">
                            \${allergy.comment && allergy.comment !== "" ? allergy.comment : '<span class="p normal-text">-</span>'}
                        </div>
                    </div>
                </li>
            `;
        });
        
        listHtml += '</ul>';
        allergiesSection.append(listHtml);
    }
</script>

<div class="row">
    <div class="col-12">
        <div class="info-section allergies">
            <div class="info-header">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                        <h5>${ui.message('allergyui.allergies')}</h5>
                    </div>

                    <div class="col-6 zero-padding text-left">
                        <% if (allergies.allergyStatus != "Unknown") { %>
                            <h5 class="h5">Notes</h5>
                        <% } %>
                    </div>
                </div>
            </div>

            <div class="info-body">
                <% if (allergies.allergyStatus != "See list") { %>
                <% if (allergies.allergyStatus == "Unknown") { %>
                <div class="row no-data-section text-center">
                    <img class=""
                         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

                    <p class="text-center">No data captured</p>
                </div>
                <% } else if (allergies.allergyStatus == "No known allergies") { %>
                ${ui.message("allergyui.noKnownAllergies")}
                <% } else { %>
                ${ui.message(allergies.allergyStatus)}
                <% } %>
                <% } else { %>
                <div class="row">
                    <div class="col-12  allergies-body"></div>
                </div>
                <% } %>
            </div>
            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editAllergiesModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Allergy</button>
            </div>
        </div>
    </div>

</div>

<!-- Nodal -->
<div class="modal fade" id="editAllergiesModal" tabindex="-1"
     data-controls-modal="editAllergiesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editAllergiesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editAllergiesModalTitle">Edit Allergies</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "consultation/editAllergies", [patientId: patient.patient.uuid, allergies: allergies])}
                </div>
            </div>
        </div>
    </div>
</div>
