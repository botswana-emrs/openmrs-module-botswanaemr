<script type="text/javascript">
    jq(document).ready(function () {
        jq(".accordion").accordion({
            collapsible: true,
            active: false,
            heightStyle: "content",
            header: "> div > div > .panel-heading"
        });
    });
</script>
<div class="accordion">
    <div class="group-panel">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="text-left text-info mt-3"> Bio data and contact details</h5>
            </div>
            <div class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col">
                        <h5 class="text-primary text-left">BIO DATA</h5>
                        <hr class="divider pt-1 bg-primary"/>
                        <table class="table mt-3">
                            <tbody>
                                <% if(identifierType != null && identifierType != "") { %>
                                <tr>
                                    <td>${identifierType}: </td>
                                    <td class="text-primary text-left">
                                        <span>${identifierValue}</span>
                                    </td>
                                </tr>
                                <% } %>
                                <tr>
                                    <td>Name/Sex:</td>
                                    <td class="text-danger text-left">
                                        <span>
                                            <i
                                            <% if (patient.gender == "M") { %> class="fa fa-mars"
                                            <% } else if (patient.gender == "F") { %> class="fa fa-venus"
                                            <% } else { %> class="fa fa-genderless"
                                            <% } %>>
                                        </i> ${patient.familyName}&nbsp;${patient.givenName}&nbsp;${patient.person.personName.middleName ?: ""}/${patient.gender}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>DOB/AGE:</td>
                                    <td class="text-danger text-left">
                                        <span>${patient.person.birthdate?.format("dd-MMM-yyyy")} / <span>${patient.person.age}</span></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Citizen / Non Citizen:</td>
                                    <td class="text-danger text-left"><span>${citizenship ?: ""}</span></td>
                                </tr>
                                    <% if (patient.getAttribute('Marital Status')) { %>
                                <tr>
                                    <td>Marital Status:</td>
                                    <td class="text-danger text-left">
                                        <span>${patient.getAttribute('Marital Status')}</span>
                                    </td>
                                </tr>
                                    <% } %>
                                <tr>
                                    <td>Educational Attainment:</td>
                                    <td class="text-danger text-left">
                                        <span>${educationLeveAttainment ?: ""}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col pl-0 pr-0">
                        <h5 class="text-primary text-left">CONTACT DETAILS</h5>
                        <hr class="divider pt-1 bg-primary"/>
                        <div class="col pl-0 pr-0">
                            <table class="table mt-3">
                                <tbody>
                                <tr>
                                    <% if (patient.getAttribute('Telephone Number')) { %>
                                    <td>Tel. Number :</td>
                                    <td class="text-left">
                                        <span>${patient.getAttribute('Telephone Number') }</span>
                                    </td>
                                    <% } %>
                                </tr>
                                <% nextOfKins.each { count=0 %>
                                <tr>
                                    <td>Next of Kin (${++count}):</td>
                                    <td class="text-left">
                                        <span>${it.person.familyName}&nbsp;${it.person.givenName}&nbsp;${it.person.personName.middleName ?: ""}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Next of Kin Relationship</td>
                                    <td class="text-left">
                                        <span>${it.relationshipType ?: ""}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tel. Number :</td>
                                    <td class="text-left">
                                        <span>${it.person.getAttribute('NOK-Contact-1') ?:"" }</span>
                                    </td>
                                </tr>
                                <% } %>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col pl-0 pr-0">
                            <h5 class="text-primary text-left mt-3">ADDRESS DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            <table class="table mt-3">
                                <tbody>
                                <% if (patient.person.personAddress?.address2) { %>
                                <tr>
                                    <td>Current Physical Address:</td>
                                    <td class="text-left">
                                        <span>${patient.person.personAddress?.address2 ?: ""}</span>
                                    </td>
                                </tr>
                                <% } %>
                                <% if (patient.person.personAddress?.address5) { %>
                                <tr>
                                    <td>Alternative Physical Address:</td>
                                    <td class="text-danger text-left">
                                        <span>${patient.person.personAddress?.address5}</span>
                                    </td>
                                </tr>
                                <% } %>
                                </tbody>
                            </table>
                        </div>
                        <% if (showPrescription) {%>
                        <div class="col pl-0 pr-0">
                            <h5 class="text-primary text-left mt-3">Drug Prescription</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            <div class="row">
                                ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.uuid, visit: currentVisit, autoActivateConsultation: true])}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button style="margin:10px;" type="button" class="btn btn-success right" data-toggle="modal"
                                            data-target="#addPrescriptionModal">Prescribe Drug</button>
                                    <button style="margin:10px;" type="button" class="btn btn-success right">
                                        <a href="${ui.pageLink('botswanaemr', 'consultation/drugPrescription', [patientId: patient.id, visitId: currentVisit.id])}" class="">
                                           <i class="fa fa-folder-o"></i> Singular prescription
                                        </a>
                                    </button>
                                </div>
                            </div>
                       </div>
                       <% }%>
                    </div>
                </div>
           </div>
           </div>
        </div>
    </div>
</div>
