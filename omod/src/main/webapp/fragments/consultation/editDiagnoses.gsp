<%
    ui.includeJavascript("botswanaemr", "icd11/icd11ect-1.6.1.js")
%>
<script>
    // Embedded Coding Tool settings object
    // please note that only the property "apiServerUrl" is required
    // the other properties are optional
   // const mySettings = {
        // The API located at the URL below should be used only for software
        // development and testing. ICD content at this location might not
        //  be up to date or complete. For production, use the API located at
        // id.who.int with proper OAUTH authentication

   //     apiServerUrl: "https://icd11restapi-developer-test.azurewebsites.net",
   //     autoBind: false
   // };

    const mySettings = {
        apiServerUrl: "https://id.who.int",
        apiSecured: true,
        autoBind: false
    };

    // example of an Embedded Coding Tool using the callback selectedEntityFunction
    // for copying the code selected in an <input> element and clear the search results
    const myCallbacks = {
        selectedEntityFunction: (selectedEntity) => {
            // clear the searchbox and delete the search results
            ECT.Handler.clear(selectedEntity.iNo)

            let selected = selectedEntity.code + ' - ' + selectedEntity.bestMatchText;
            // console.log(selected);
            let diagnosisInput = jq('input[data-ctw-ino=' + selectedEntity.iNo + ']');
            diagnosisInput.val(selected);
            diagnosisInput.attr('data-overlay', selected);
            // console.log(diagnosisInput);
            acceptedValues.push(selected);
            console.log(acceptedValues);

        },
        getNewTokenFunction: async () => {
            // if the embedded browser is working with the cloud hosted ICD-API, you need to set apiSecured=true
            // In this case embedded browser calls this function when it needs a new token.
            // In this case you backend web application should provide updated tokens

            const url = '${ ui.actionLink("botswanaemr", "search", "getIcd11ApiToken") }' // we assume this backend script returns a JSON {'token': '...'}
            try {
                const response = await fetch(url);
                const result = await response.json();
                const token = result.token;
                return token; // the function return is required
            } catch (e) {
                console.log("Error during the request");
            }
        }
    };

    // configure the ECT Handler with mySettings and myCallbacks
    ECT.Handler.configure(mySettings, myCallbacks);
</script>
<script type="text/javascript">
    let diagnosisData = {
        "deleted": [],
        "edited": [],
        "new": []
    };
    let acceptedValues = [];

    jq(function () {
        jq("#diagnosisTemplate").hide();

        /*
                function setAutocomplete(element){
                    element.autocomplete({
                        source: function(request, response) {
                            jq.getJSON('





        ${ ui.actionLink("botswanaemr", "consultation/editConditions", "searchConditions") }', {
                        searchTerm: request.term
                    }).success(function(data) {
                        var results = [];
                        for (var i in data) {
                            var result = {
                                label: data[i].name,
                                value: data[i].uuid
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function(event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay',ui.item.value);
                },
            });
        }
*/
        function setAutocomplete(element) {
            element.autocomplete({
                source: function (request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "search", "searchIcd11") }', {
                        q: request.term
                    }).success(function (data) {
                        if (data.length === 0) {
                            jq(element).attr('data-overlay', '');
                        }
                        var results = [];
                        for (var i in data) {
                            var result = {
                                label: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text(),
                                value: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text()
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay', ui.item.value);
                    acceptedValues.push(ui.item.value);
                },
            });
        }

        jq('#editDiagnoses').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let certainty = tr.find(('select[name=certainty]'));
                        let diagnosisType = tr.find(('select[name=diagnosisType]'));

                        let data = {
                            "diagnosis": tr.attr("data-overlay"),
                            "certainty": certainty.attr("data-overlay"),
                            "type": diagnosisType.attr("data-overlay")
                        }
                        diagnosisData.deleted.push(tr.attr("data-overlay"));
                    }

                    // jq(e.target).closest('tr').remove();
                    jq(e.target).parents('table:first').parent().closest('tr').remove();
                    jq("#editDiagnoses :input").trigger("change");
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                        if (jq(this).attr("name") === "diagnosis") {
                            setAutocomplete(jq(this));
                        }
                    });
                }
            }

        });

        jq("#btnAddDiagnosis").on("click", function (e) {
            e.preventDefault();
            let template = jq("#diagnosisTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });
            // setAutocomplete(template.find("input[name='diagnosis']"));

            jq('<tr/>', {
                'class': 'newDiagnosis',
                'id': 'newDiagnosis',
                html: template
            }).hide().appendTo('#diagnosesBody').slideDown('slow');

            // Set the ICD11 ect tool
            let diagnoses = jq('#diagnosesBody').find('.newDiagnosis').length;
            let ctwId = diagnoses + 1;
            template.find('.ctw-input').attr('data-ctw-ino', ctwId);
            template.find('div.ctw-window').attr('data-ctw-ino', ctwId);
            ECT.Handler.bind(ctwId);

        });

        function closeModal() {
            diagnosisData.new = [];
            diagnosisData.edited = [];
            jQuery('#editDiagnosesModal').modal('toggle');
        }

        jq("#close").on("click", function (e) {
            e.preventDefault();
            closeModal();
        })

        function getFormData() {
            jq("#diagnosesBody").find("tr.diagnosisData:gt(0)").map(function (row) {
                let diagnosisInput = jq(this).find(('input[name=diagnosis]'));
                let diagnosis = diagnosisInput.attr('data-overlay');
                let diagnosisCertainty = jq(this).find(('select[name=certainty]'));
                let certainty = diagnosisCertainty.val();

                let diagnosisTypeInput = jq(this).find("#diagnosisTypetype option:selected");
                let diagnosisType = diagnosisTypeInput.val();

                let existingUuid = jq(this).attr("data-overlay");
                if (existingUuid) {
                    console.log("diagnosis", diagnosis);
                    diagnosisData.edited.push({
                        "uuid": existingUuid,
                        "diagnosis": diagnosis,
                        "certainty": certainty,
                        "type": diagnosisType
                    })
                } else if (diagnosis && acceptedValues.includes(diagnosis)) {
                    diagnosisData.new.push({
                        "diagnosis": diagnosis,
                        "certainty": certainty,
                        "type": diagnosisType
                    });
                }
            });
        }

        jq("#editDiagnoses").submit(function (e) {
            e.preventDefault();
            getFormData();

            let payload = JSON.stringify(diagnosisData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editDiagnoses", "updateDiagnoses") }',
                {
                    returnFormat: 'json',
                    patientId: ${activePatient.patientId},
                    data: payload,
                    visitId: ${activeVisit.id},
                    diagnosisEncounterType: "52AE669B-2B27-4DCA-A580-A1A939BBB564"
                },
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patient's diagnoses have been updated");
                    location.reload();
                }, 'json')
                .error(function () {
                    console.log("error");
                });
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editDiagnoses">
            <div class="row">
                <table class="table multi table-borderless">
                    <thead>
                    </thead>
                    <tbody id="diagnosesBody">
                    <tr id="diagnosisTemplate" style="display: none">
                        <td>
                            <table class="table table-borderless">
                                <thead>
                                <th scope="col">Diagnosis</th>
                                <th scope="col">Certainty</th>
                                <th scope="col">Type</th>
                                <th scope="col">Operations</th>
                                </thead>
                                <tr class="diagnosisData">
                                    <td>
                                        <!-- input element used for typing the search  -->
                                        <input type="text" name="diagnosis" class="form-control ctw-input"
                                               autocomplete="off" data-ctw-ino="0" disabled
                                               placeholder="Start typing for hints... ">
                                        <!-- example of an extra input element for testing the Embedded Coding Tool select entity function -->
                                        <!-- Selected code: <input type="text" id="paste-selectedEntity" value="">-->
                                    </td>
                                    <td>
                                        <select name="certainty" type="text" placeholder="Provisional or Confirmed"
                                                class="custom-select form-control">
                                            <option disabled selected>Select one</option>
                                            <option value="PROVISIONAL">${ui.encodeHtmlContent(ui.format("Provisional"))}</option>
                                            <option value="CONFIRMED">${ui.encodeHtmlContent(ui.format("Confirmed"))}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="diagnosisType" id="diagnosisTypetype"
                                                class="custom-select form-control">
                                            <option disabled>Select one</option>
                                            <option value="N" selected>New</option>
                                            <option value="R">Repeat/Follow up</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="row text-right" style="display: inline-flex">
                                            <div class="title-margin-right">
                                                <a class="btn btn-sm btn-light right edit">Edit</a>
                                            </div>

                                            <div class="title-margin-left">
                                                <a class="btn btn-sm btn-light right remove">Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <!-- div element used for showing the search results -->
                                        <div class="ctw-window" data-ctw-ino="0"></div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <% diagnoses.each { diagnosis -> %>
                        <% if (diagnosis.getEncounter()?.getEncounterType()?.getName() == "Diagnosis Encounter") { %>
                    <tr id="diagnosisRow" data-overlay="${diagnosis.uuid}">
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <% if (diagnosis.diagnosis?.coded != null) { %>
                                        <input name="diagnosis" type="text" class="form-control"
                                               placeholder="Start typing for hints... "
                                               data-overlay="${diagnosis.diagnosis?.coded?.uuid}"
                                               value="${ui.encodeHtmlContent(ui.format(diagnosis?.diagnosis?.coded))}"
                                               disabled/>
                                        <% } else { %>
                                        <!-- TODO: Handle non-code diagnoses-->
                                        ${ui.encodeHtmlContent(ui.format(diagnosis?.diagnosis?.nonCoded))}
                                        <% } %>
                                    </td>
                                    <td>
                                        <select name="certainty" type="text" placeholder="Provisional or Confirmed"
                                                class="custom-select form-control" disabled>
                                            <option>Select one</option>
                                            <option value="PROVISIONAL"
                                                    <% if (diagnosis.certainty.toString() == "PROVISIONAL") { %>selected <%
                                                    } %>>${ui.encodeHtmlContent(ui.format("Provisional"))}</option>
                                            <option value="CONFIRMED"
                                                    <% if (diagnosis.certainty.toString() == "CONFIRMED") { %>selected <%
                                                    } %>>${ui.encodeHtmlContent(ui.format("Confirmed"))}</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="diagnosisType" id="diagnosisTypetype"
                                                class="custom-select form-control"
                                                disabled>
                                            <option disabled>Select one</option>
                                            <option value="N"
                                                    <% if (diagnosisAttributes[diagnosis.id] == "N") { %>selected <%
                                                    } %>>New</option>
                                            <option value="R"
                                                    <% if (diagnosisAttributes[diagnosis.id] == "R") { %>selected <%
                                                    } %>>Repeat/Follow up</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="row text-right" style="display: inline-flex">
                                            <div class="edit title-margin-right">
                                                <a class="btn btn-sm btn-light right edit">Edit</a>
                                            </div>

                                            <div class="title-margin-left">
                                                <a class="btn btn-sm btn-light right remove">Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <% } %>
            <% } %>
                    </tbody>
                </table>
                <button type="button" class="dashed-button rounded add-more"
                        id="btnAddDiagnosis">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" id="close">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
            </div>
        </form>
    </div>

</div>
