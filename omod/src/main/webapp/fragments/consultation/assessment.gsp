<% if (activeVisit == null) { %>
<div class="col col-sm-12 col-md-12">
    <div class="alert alert-warning text-danger" role="alert">
        <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!</strong>
    </div>
</div>
<% } else { %>
<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/assessment/nursingDiagnosisFreeText", [visit: activeVisit?.uuid])}
</div>

<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/assessment/nursingDiagnosis", [visit: activeVisit?.uuid])}
</div>

<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/assessment/diagnosis", [visit: activeVisit?.uuid])}
</div>

<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/assessment/additionalNotes", [visit: activeVisit?.uuid])}
</div>
<% } %>
