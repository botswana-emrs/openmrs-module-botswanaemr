<script type="text/javascript">
  jQuery(function () {
        var table = jQuery('#labResultsTable').DataTable({
            searchPanes: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        table.searchPanes.container().prependTo(table.table().container());
        table.searchPanes.resizePanes();

    jQuery("#viewLabResultsModal").appendTo("body");
    jQuery("#viewHistory").click(function(e){

    });
    });
      function viewResults(labSetId) {
        var resultsMap;
        jQuery("#labSetId").val(labSetId);
        var params = {
          'labSetId': jQuery("#labSetId").val()
        };
          jQuery.getJSON('${ ui.actionLink("botswanaemr", "consultation/labResults", "getLabResultsFilled")}', params)
              .done(function (data) {
                  let labContents = jq('#labContents');
                  labContents.html('');

                  let row = jq('<div/>',{class: 'row'});
                  row.append(jq('<span/>',{class: 'text-primary pl-3 key'}));
                  row.append(jq('<span/>',{class: 'text-muted pl-3 values'}));

                  jq.each(data, function(key, value){
                      let newRow = row.clone();
                      newRow.find(".key").text(key);
                      newRow.find(".values").text(value);
                      labContents.append(newRow);
                  });

              });
      }
</script>

<table id="labResultsTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>Patient</th>
        <th>Test</th>
        <th>Added</th>
        <th>Received</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        <% lab.each {%>
            <tr>
                <td>${it.patientNames}</td>
                <td>${it.testName}</td>
                <td>${it.dateAdded}</td>
                <td>${it.dateReceived==null?'':it.dateReceived}</td>
                <td>
                    <% if(it.status.toString() == "COMPLETED"){%>
                    <i class="fa fa-circle text-success"></i> Completed
                    <% } else { %>
                    <i class="fa fa-circle text-warning"></i> Pending
                    <% } %>
                </td>
                <td>
                    <% if(it.status.toString() == "COMPLETED") {%>
                        <a href="#" data-toggle="modal" data-target="#viewLabResultsModal" id="viewHistory"
                           class="color-primary" onclick="viewResults(${it.labOrderTestId})">
                            view
                        </a>|
                    <%}%>
                    <a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${it.patient.uuid}&labTestOrder=${it.labOrderTestId}" id="lab-details"
                       class="color-primary">
                         profile
                    </a>
                </td>
            </tr>
        <%}%>
    </tbody>
</table>

<div class="modal fade" id="viewLabResultsModal" tabindex="-1"
     data-controls-modal="viewLabResultsModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="viewLabResultsModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="viewLabResultsModalTitle">Lab Results</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="labSetId" name="labSetId" />
                <div class="col" id="labContents">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="text-primary pl-3 key"></span>
                            <span class="text-muted values"></span>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

