<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/doctorDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/doctorsPatientPoolDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-bed fa-1x"></i>
            </div>
            <div class="nav_label">Patient Pool</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/myAppointments')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Calendar</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/consultationsList')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-folder-open fa-1x"></i>
            </div>
            <div class="nav_label">Consultations List</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/referralsPool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-external-link fa-1x"></i>
            </div>
            <div class="nav_label">Referrals</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>
