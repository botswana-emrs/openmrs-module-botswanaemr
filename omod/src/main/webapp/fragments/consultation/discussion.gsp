<script>
    jq(function () {

        let discussion = null;

        jq("#save-overlay").hide();

        const fetchDiscussion = () => {
            jq("#save-overlay").show();
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "getDiscussion")}', {
                'visitId': <%=visit.visitId%>
            }).success(result => {
                if (result.success) {
                    discussion = result.data;
                    // console.log("Discussion: " + JSON.stringify(discussion))
                    updateDOM(discussion);
                    fetchMessages(discussion)
                } else {
                    jq().toastmessage('showErrorToast', result.data);
                }
            });
        }

        const fetchMessages = (discussion) => {
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "getMessages")}', {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0
            }).success(result => {
                if (result.success) {
                    let messages = result.data;
                    // console.log("Messages: " + JSON.stringify(discussion))
                    refreshMessages(messages);
                    fetchParticipants(discussion)
                } else {
                    jq().toastmessage('showErrorToast', result.data);
                }
            });
        }

        const fetchParticipants = (discussion) => {
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "getParticipants")}', {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0
            }).success(result => {
                if (result.success) {
                    let participants = result.data;
                    jq('#participants-title').text(participants.length + ' participants in this discussion');
                    const participantsDiv = jq('#participants-list');
                    participantsDiv.html("");
                    participants.forEach((discussionParticipant) => {
                        const status = discussionParticipant.online ? 'online' : 'offline';
                        const you = discussion.mine && discussion.userId === discussionParticipant.userId ? ' (You)' : '';
                        participantsDiv.append(
                            '<p id=user-' + discussionParticipant.userId + ' data-user=' + discussionParticipant.name + '>' +
                            '<span class="left-margin">' + discussionParticipant.name + you + ' - ' + status + '</span>' +
                            '<button id=' + discussionParticipant.userId + ' class="remove-participant" type="button">x</button>' +
                            '</p>'
                        );
                    });
                    fetchCandidates(discussion);
                } else {
                    jq().toastmessage('showErrorToast', result.data);
                }
            });
        }

        const fetchCandidates = (discussion) => {
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "getCandidates")}', {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0
            }).success(result => {
                if (result.success) {
                    let candidates = result.data;
                    jq('#participant-select').autocomplete({
                        source: candidates,
                        multiselect: true,
                        select: (event, ui) => {
                            jq('#participant-select').val(ui.item.label);
                            jq('#participant-value').val(ui.item.value);
                            return false;
                        }
                    });
                    if (discussion.discussionId) {
                        jq('#participant-select').attr("placeholder", "Search to add a participant");
                    } else {
                        jq('#participant-select').attr("placeholder", "Search to add participants and start a discussion");
                    }
                    jq("#save-overlay").hide();
                } else {
                    jq().toastmessage('showErrorToast', result.data);
                }
            });
        }

        const postParticipant = () => {
            if (!discussion) {
                fetchDiscussion();
            }
            const params = {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0,
                'visitId': <%=visit.visitId%>,
                'userId': jq('#participant-value').val()
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "postParticipant")}', params)
                .success(result => {
                    if (result.success) {
                        discussion = result.data;
                        updateDOM(discussion);
                        jq('#participant-select').val('');
                        fetchDiscussion();
                    } else {
                        jq().toastmessage('showErrorToast', result.data);
                        fetchDiscussion();
                    }
                })
                .error(() => {
                    jq().toastmessage('showErrorToast', "Error adding participant");
                    fetchDiscussion();
                })
        }

        const deleteParticipant = (userId) => {
            if (!discussion) {
                fetchDiscussion();
            }
            const params = {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0,
                'visitId': <%=visit.visitId%>,
                'userId': userId
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "deleteParticipant")}', params)
                .success(result => {
                    if (result.success) {
                        fetchDiscussion();
                    } else {
                        jq().toastmessage('showErrorToast', result.data);
                    }
                })
                .error(() => {
                    jq().toastmessage('showErrorToast', "Error removing participant");
                    fetchDiscussion();
                })
        }

        const postMessage = () => {
            jq("#save-overlay").show();
            const params = {
                'discussionId': discussion.discussionId ? discussion.discussionId : 0,
                'visitId': <%=visit.visitId%>,
                'content': jq('#message-content').val()
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/discussion", "postMessage")}', params)
                .success(result => {
                    if (result.success) {
                        let message = result.data;
                        refreshMessages(message);
                        jq('#message-content').val('');
                        jq('#message-content').focus();
                        // update message count.
                        jq("#save-overlay").hide();
                    } else {
                        jq().toastmessage('showErrorToast', result.data);
                    }
                })
                .error(() => {
                    jq().toastmessage('showErrorToast', "Error posting message");
                    fetchDiscussion();
                })
        }

        const refreshMessages = (messages) => {
            const messageBody = jq('#message-body');
            messageBody.html('');
            messages.forEach((message) => {
                messageBody.append(
                    '<div class="d-flex justify-content-end mb-4">' +
                    (message.mine ? '<div class="message-container-send">' : '<div class="message-container">') +
                    '<span class="message-time"><span class="font-weight-bold">' + message.author + '</span> . ' + message.time + '</span>' +
                    message.content +
                    '</div>' +
                    '</div>'
                );
            });
            jq('#message-count').text(messages.length + ' ' + (messages.length == 1 ? 'message' : 'messages'));
        }

        const updateDOM = (discussion) => {
            // console.log(JSON.stringify(discussion));
            if (discussion.discussionId) {
                jq('#discussion-init').hide();
                jq('#discussion-header').show();
                jq('#participants').show();
                if (discussion.mine) {
                    jq('#search-participant').show();
                } else {
                    jq('#search-participant').hide();
                }
                jq('#conversation').show();
                if (discussion.participationAllowed) {
                    jq('#message').show();
                } else {
                    jq('#message').hide();
                }

                jq('#discussion_title').text(discussion.title);
            } else {
                jq('#discussion-init').show();
                jq('#discussion-header').hide();
                jq('#participants').hide();
                jq('#search-participant').show();
                jq('#conversation').hide();
                jq('#message').hide();
            }
            if (discussion.hasParticipants) {
                jq('#message').show();
            } else {
                jq('#message').hide();
            }
        }

        fetchDiscussion();

        jq('#participant-add').click(e =>
            postParticipant()
        );

        // Do not use arrow function here since it has no binding for the "this" reference.
        jq(document).on('click', ".remove-participant", function () {
            const userId = jq(this).attr("id");
            const user = jq('#user-' + userId).attr('data-user');
            jq('#remove-button').attr('data-user', userId);
            jq('#remove-participant-message').text('Remove ' + user + ' from the discussion?');
            jq('#delete-modal').show();
        });

        jq("#message-form").submit(e =>
            postMessage()
        );

        jq('#remove-button').click(e => {
            deleteParticipant(jq('#remove-button').attr('data-user'));
            jq("#delete-modal").hide()
        });

        jq('#cancel-button').click(e => {
            jq("#delete-modal").hide()
        });

        jq('#close-button').click(e => {
            jq("#delete-modal").hide()
        });
    });
</script>
<% ui.includeCss("botswanaemr", "discussion.css") %>
<div class="card-overlay" id="save-overlay">
    <img
            class="spinner-img"
            src="${ui.resourceLink("botswanaemr", "images/loading-spinner.gif")}"
            alt=""/>
</div>

<div class="col col-sm-12 col-md-12 col-lg-12 overflow-auto">
    <div class="discussion-card">

        <div id="discussion-init" class="left-margin">
            <span>Discussion</span>

            <p>Start a discussion with another doctor for advice and alternative opinions on this patient.</p>
        </div>

        <div id="discussion-header" class="discussion-card-header message-head left-margin">
            <div class="d-flex bd-highlight">
                <div class="image-container">
                    <span class="lettered-circle">D</span>
                </div>

                <div class="discussion-info">
                    <span id="discussion_title" class="discussion-title">Discussion title</span>

                    <p id="message-count">Message count</p>
                </div>
            </div>
        </div>

        <div id="participants">
            <span id="participants-title" class="left-margin">
                Participants in this discussion
            </span>

            <div id="participants-list"></div>
        </div>

        <div id="search-participant">
            <form>
                <div class="participants-search">
                    <input id="participant-select" type="text" placeholder="Search to add a participant"/>
                    <input id="participant-value" type="hidden">
                    <button id="participant-add" type="submit"
                            class="btn btn-primary participants-search left-margin">Add participant</button>
                </div>
            </form>
        </div>

        <span id="conversation">
            <div id="message-body" class="discussion-card-body message-card-body">

            </div>
        </span>

        <div id="message" class="discussion-card-footer">
            <form method="post" id="message-form">
                <div class="new-message-box">
                    <textarea rows="2" id="message-content" class="form-control type-message"
                              placeholder="Type your message here ..."></textarea>
                </div>

                <div class="discussion-footer">
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Remove participant</h5>
                <button id="close-button" type="button" class="btn btn-sm bg-white text-dark" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <p id="remove-participant-message" class="text-muted text-center">
                    Remove this participant from the discussion?
                </p>
            </div>

            <div class="modal-footer">
                <button type="submit" id="remove-button" class="btn btn-primary text-white">Remove</button>
                <button type="button" id="cancel-button" class="btn btn btn-dark bg-dark" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
