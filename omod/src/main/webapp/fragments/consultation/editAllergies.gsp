<script type="text/javascript">
    let allergyData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#allergyTemplate").hide();

        function setAutocomplete(element){
            element.autocomplete({
                source: function(request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/editAllergies", "searchAllergies") }', {
                        searchTerm: request.term
                    }).success(function(data) {
                        if (data.length === 0) {
                            jq(element).attr('data-overlay', '');
                        }
                        let results = [];
                        for (let i in data) {
                            let result = {
                                label: data[i].name,
                                value: data[i].name,
                                uuid: data[i].uuid
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                select: function(event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay',ui.item.uuid);
                },
            });
        }

        jq('#editAllergies').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        allergyData.deleted.push(tr.attr("data-overlay"));
                    }

                    jq(e.target).closest('tr').remove();
                    jq("#editAllergies :input").trigger("change");
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                        if (jq(this).attr("name") === "allergen") {
                            setAutocomplete(jq(this));
                        }
                    });
                }
            }
        });

        jq("#btnAddAllergy").on("click", function (e) {
            e.preventDefault();
            let template = jq("#allergyTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });

            setAutocomplete(template.find("input[name='allergen']"));

            jq('<tr/>', {
                'class': 'newAllergy',
                'id': 'newAllergy',
                html: template
            }).hide().appendTo('#allergiesBody').slideDown('slow');
        });

        function getFormData() {
            jq("#allergiesBody").find("tr:gt(0)").map(function (row) {
                let allergenInput = jq(this).find(('input[name=allergen]'));
                let allergen = allergenInput.val();
                let existingAllergen = allergenInput.attr('data-overlay');

                let allergenComment = jq(this).find(('input[name=comment]'));
                let comment = allergenComment.val();

                let existingUuid = jq(this).attr("data-overlay");
                if (existingUuid) {
                    allergyData.edited.push({
                        "uuid": existingUuid,
                        "allergen": existingAllergen || allergen,
                        "comment": comment
                    })
                } else {

                    allergyData.new.push({
                        "allergen": existingAllergen || allergen,
                        "comment": comment
                    })
                }
            });
        }

        jq("#editAllergies").submit(function (e) {
            e.preventDefault();

             // Disable the save button
             jq('#saveAllergies').prop('disabled', true);

            showLoadingOverlay();
            getFormData();

            let payload = JSON.stringify(allergyData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editAllergies", "updateAllergies") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload},
                function (data) {

                }, 'json')
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The patients allergies have been updated");

                    //fetch and render DOM dynamically with updated allergies
                    loadAllergies();
                    
                    jQuery('#editAllergiesModal').modal('toggle');

                    hideLoadingOverlay();
                })
                .fail(function() {
                    console.log("error");
                     // enable the save button
                     jq('#saveAllergies').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editAllergies">
            <div class="row">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Allergy</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="allergiesBody">
                    <tr id="allergyTemplate" style="display: none">
                        <td>
                            <input name="allergen" type="text" placeholder="Start typing for hints... "/>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% allergies.each { allergy -> %>
                    <tr id="allergyRow" data-overlay="${allergy.uuid}">
                        <td>
                            <input name="allergen" type="text" disabled placeholder="Start typing for hints... "
                                   data-overlay="${allergy.allergen.codedAllergen.uuid}"
                                   value="${ui.encodeHtmlContent(ui.format(allergy.allergen))}"/>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                   value="${allergy.comment}"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                </table>

                <button class="dashed-button rounded add-more"
                        id="btnAddAllergy">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="saveAllergies">Save</button>
            </div>
        </form>
    </div>

</div>
