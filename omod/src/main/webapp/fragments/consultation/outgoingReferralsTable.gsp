<script type="text/javascript">
    jQuery(function () {
        var outTable = jQuery('#outgoingReferralsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#inquiryBtn').on('click', () => {

            outTable.draw();

        });
        jq('.card-link-out').click(() => {
            if (jq('#reasonOutDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseOutBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseOutBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });
        jq('#resetOutBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#my-referrals'));
            outTable.draw();
        });
    });
</script>

<div class="row mb-4">
    <div class="col-4 pl-0">
        <label for="outSearchPhrase" class="p-r-4">Search:</label>
        <input type="text" class="form-control border" id="outSearchPhrase"
               name="outSearchPhrase" placeholder="Search by Patient ID or name">
    </div>
    <div class="col-4" id="statusOutDiv">
        <div class="form-horizontal form-group">
            <label for="outReferralStatus">Status:</label>
            <select class="custom-select form-control" id="outReferralStatus">
                <option selected value="">Select status:</option>
                <option value="Awaiting">Awaiting</option>
                <option value="Completed">Completed</option>
            </select>
        </div>
    </div>

    <div class="col-auto">
        <button type="submit" id="inquiryBtn" class="btn btn-sm btn-primary mb-3">
            ${ui.message("Search")}
        </button>
    </div>
    <div class="col-auto">
        <button type="submit" id="resetOutBtn" class="btn btn-sm btn-dark bg-dark mb-3 border">
            ${ui.message("Reset")}
        </button>
    </div>
    <div class="col-auto">
        <a id="collapseOutBtn" class="card-link-out btn btn-sm btn-warning bg-white color-primary border-0"
           data-target=".multi-collapse"
           aria-expanded="false"
           aria-controls="dateOutDiv reasonOutDiv"> ${ui.message("Expand")}
            <i id="collapse-icon" class="icon-chevron-down"></i>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-4 pl-0">
        <div class="form-horizontal form-group collapse multi-collapse" id="reasonOutDiv">
            <label for="filterReferralOutReason" class="p-r-4">Reason:</label>
            <select class="custom-select form-control border form-inline" id="filterReferralOutReason">
                <option selected value="">Select reason</option>
                <% if (outgoingReferral != "") {%>
                <% outgoingReferral.toUnique { a, b -> a.orderReason?.name?.name <=> b.orderReason?.name?.name }.each { %>
                <option value="${it.orderReason?.name}">${it.orderReason?.name}</option>
                <% } %>
                <% } %>
            </select>
        </div>
    </div>
    <div class="col-4">
        <div class="form-horizontal form-group collapse multi-collapse" id="dateOutDiv">
            <label for="outReferralDate">Date:</label>
            <input required
                   type="text"
                   class="form-control datepicker"
                   id="outReferralDate"
                   name="outReferralDate"
                   placeholder="Select date referred"
            />
            <script type="text/javascript">
                 jq('#outReferralDate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    "setDate": new Date(),
                    dateFormat: "dd-M-yy",
                    "autoclose": true
                 });
            </script>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table id="outgoingReferralsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Case</th>
            <th>Patient ID</th>
            <th>Patient Name</th>
            <th>Location</th>
            <th>Reason</th>
            <th>Date</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <% outgoingReferral.each { %>
        <tr>
            <td>${it.orderId}</td>
            <td>${it.patient.getPatientIdentifier(openmrsId)}</td>
            <td>${it.patient.personName}</td>
            <td>${it.encounter?.location?.name}</td>
            <% if(it.orderReason) { %>
            <td>${it.orderReason?.name}</td>
            <% } else { %>
            <td>${it.orderReasonNonCoded}</td>
            <% } %>
            <td>${it.dateCreated.format("dd-MMM-yyyy")}</td>
            <% if(it.fulfillerStatus.toString().equals("COMPLETED")){%>
            <td>
                <i class="fa fa-circle text-success"></i>
                <span class="text-muted px-0" style="background: none;"> Completed</span>
            </td>
            <% } else {%>
            <td>
                <i class="fa fa-circle text-warning"></i>
                <span class="text-muted px-0" style="background: none;"> Awaiting</span>
            </td>
            <% } %>
            <td>
                <a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${it.patient.id}"
                   class="color-primary"> Patient Profile
                </a>
                <span class="text-dark">|</span>
                <a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=${it.getPatient().getPatientId()}&visitId=${it.encounter.visit.id}"
                   class="color-primary"> Case
                </a>
                <span class="color-primary">
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
</div>
