<script>
  jQuery(function () {
    jQuery("#beginConsultationModal").appendTo("body");

      var table = jQuery("#caseTable").DataTable({
          searchPanes: true,
          "oLanguage": {
              "oPaginate": {
                  "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                  "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
              }
          }
      });

    jQuery("#caseTable tbody").on( 'click', 'tr', function () {
      var itemsValues = table.row( this ).data();
      jQuery("#cases-id").val(itemsValues[0]);
    });
    jQuery("input[name='caseradiogroup']").change(function(){
      let selectedValue = jQuery(this).val();
      if(selectedValue === 'new') {
        jQuery("#existingCasesDiv").hide('slow');
        jQuery("#descriptionDiv").show('slow');
        jQuery("#descriptionDiv *").prop('disabled',false);
      }
      else {
        jQuery("#existingCasesDiv").show('slow');
        jQuery("#descriptionDiv").hide('slow');
        jQuery("#descriptionDiv *").prop('disabled',true);
      }

    });

    jQuery("#beginConsultationForm").submit(function() {
        showLoadingOverlay();
        return true;
    });

  });
</script>
<div class="modal fade" id="beginConsultationModal" tabindex="-1"
     data-controls-modal="beginConsultationModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="beginConsultationModalTitle">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="beginConsultationModalTitle">Begin Consultation</h4>
                <button type="button" id="beginPatientConsultation" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="beginConsultationForm">
                    <div class="form-group">
                        <p>You have chosen to begin the consultation with this patient <span>${patient.personName.fullName} (${patient.gender}) - Age: ${patient.age}</span> </p> 
                    </div>
                    <div class="form-group hidden">
                        <em>Please select below</em>
                    </div>
                    <div class="radios hidden" id="radioDivs">
                        <p>
                            <input id="newCaseCheck" checked="checked" name="caseradiogroup" type="radio" value="new" />
                            <label for="newCaseCheck">Create new case</label>
                        </p>
                        <p>
                            <input name="caseradiogroup" id="linkToExistingCaseCheck" type="radio" value="existing" />
                            <label for="linkToExistingCaseCheck">Link to an existing case</label>
                        </p>
                    </div>
                    <div id="descriptionDiv" class="hidden">
                        <label for="description">Add a description for this case</label>
                        <textarea cols="50" rows="2" id="description" name="description" required="required">${defaultCaseDescription}</textarea>
                    </div>
                    <div id="existingCasesDiv" class="table-responsive" style="display: none">
                        <p>Select case to link to</p>
                        <table id="caseTable" class="table table-striped table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Case No</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <% allExistingCases.each {%>
                                <tr>
                                    <td>${it.caseId}</td>
                                    <th>${it.description}</th>
                                    <td>${ui.format(it.dateCreated)}</td>
                                    <td>${it.caseStatus}</td>
                                    <td>link case<input type="checkbox" id="selectCase" /></td>
                                </tr>
                            <%}%>
                            </tbody>
                        </table>
                        <input type="hidden" id="cases-id" name="cases" />
                    </div>
                    <div class="modal-footer">
                        <button id="complete" type="submit"  class=" btn btn-primary right" value="start" name="complete">${ui.message("Confirm to proceeed")}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

