<script type="text/javascript">
</script>
<style>
    h6 {
        color: #363463;
    }

    .badge {
        min-width: 100% !important;
    }
</style>
<div class="row">
    <div class="row col-sm-12">
        <div class="col-12" style="display: flex;justify-content: flex-end;">
            <button type="button" id="addAppointmentBtn" class="btn btn-sm btn-primary pr-3 pl-3">
                <!--
                <a href="${ui.pageLink('botswanaemr', 'consultation/addVitals', [ patientId: patient.uuid ])}" class="">
                    Add Vital
                </a>
                -->
                <a href="${ui.pageLink("botswanaemr", "enterForm", [
                                  patientId: patient.uuid,
                                  formUuid: "c5683586-083c-11ee-be56-0242ac120002",
                                  visitId: visit.uuid,
                                  returnUrl:returnUrl])}"
                                  class="btn btn-sm btn-primary pr-3 pl-3">
                                  Add Vitals
                </a>                
            </button>
        </div>
    </div>
    <div class="card mb-3" style="width: 100%">
        <div class="col col-sm-12 col-md-12 col-lg-12">
            <div class="col pr-0 pl-0">
                ${ui.includeFragment("botswanaemr", "vitalsTrends", [patientId: patient.uuid])}
            </div>
        </div>
    </div>
    <% vitalsData.each { vitalData -> %>
    <div class="card mb-3">
        <div class="col col-sm-12 col-md-12 col-lg-12">
            <div class="col pr-0 pl-0">
                <h6 class="mb-2 mt-2">Vitals ${vitalData.get('day')}</h6>
                <hr class="divider pt-1"/>
            </div>
            <div class="row mb-3" style="overflow-x:scroll">
                <table class="table mt-3">
                    <thead>
                    <tr>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-start">TIME</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">TEMP</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">BP</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">WEIGHT</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">HEIGHT</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">BMI</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">BSA</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">RR</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">PULSE</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">HEAD CIRCUMFERENCE</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">GLUCOSE LEVEL</h6>
                        </th>
                        <th scope="col" class="bg-pale">
                            <h6 class="text-dark text-center">OXYGEN LEVEL</h6>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <% vitalData.get('data')?.each { data -> %>
                            <tr>
                                <td>${data?.get('time')}</td>
                                <td style="color:{{getColorCode('Temperature', '${data?.get('temp')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('temp')?.getValueNumeric() ?: ''}</td>
                                <td><span style="color:{{getColorCode('systolic', '${data?.get('sys')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('sys')?.getValueNumeric() ?: ''}</span> / <span style="color:{{getColorCode('systolic', '${data?.get('dia')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('dia')?.getValueNumeric() ?: ''}</span></td>
                                <td>${data?.get('weight')?.getValueNumeric() ?: ''}</td>
                                <td>${data?.get('height')?.getValueNumeric() ?: ''}</td>
                                <td style="color:{{getColorCode('body mass', '${data?.get('bmi')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('bmi')?.getValueNumeric() ?: ''}</td>
                                <td>${data?.get('bsa')?.getValueNumeric() ?: ''}</td>
                                <td style="color:{{getColorCode('respiratory', '${data?.get('rr')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('rr')?.getValueNumeric() ?: ''}</td>
                                <td style="color:{{getColorCode('pulse', '${data?.get('pulse')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('pulse')?.getValueNumeric() ?: ''}</td>
                                <td>${data?.get('head')?.getValueNumeric() ?: ''}</td>
                                <td>${data?.get('glucose')?.getValueNumeric() ?: ''}</td>
                                <td style="color:{{getColorCode('oxygen', '${data?.get('oxygen')?.getValueNumeric() ?: ''}', '${activePatientAge}')}};">${data?.get('oxygen')?.getValueNumeric() ?: ''}</td>
                            </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <% } %>
</div>