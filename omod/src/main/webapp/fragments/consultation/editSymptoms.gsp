<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script type="text/javascript">
    let symptomsData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#symptomTemplate").hide();
/*
        function setAutocomplete(element){
            element.autocomplete({
                source: function(request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "search", "searchIcd11") }', {
                        q: request.term
                    }).success(function(data) {
                        var results = [];
                        for (var i in data) {
                            var result = {
                                label: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text(),
                                value: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text()
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function(event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay',ui.item.value);
                },
            });
        }
*/
        jq('#editSymptoms').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let comment = tr.find(("input[name='comment']"));
                        let duration = tr.find(("input[name='duration']"));
                        let durationUnit = tr.find(("select[name='durationUnit']"));

                        let data = {
                            "symptom" : tr.attr("data-overlay"),
                            "comment": comment.attr("data-overlay"),
                            "duration": duration.attr("data-overlay"),
                            "durationUnit": durationUnit.attr("data-overlay")
                        }
                        symptomsData.deleted.push(data);
                    }

                    jq(e.target).closest('tr').remove();
                    jq("#editSymptoms :input").trigger("change");
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                        if (jq(this).attr("name") === "symptom"){
                            // setAutocomplete(jq(this));
                        }
                    });
                }
            }
        });

        jq("#btnAddSymptom").on("click", function (e) {
            e.preventDefault();
            const len = jq('.newSymptom').length + 1;
            let template = jq("#symptomTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
                // jq(this).attr("id", this.id + len).attr("name", this.name + len);
            });
            // setAutocomplete(template.find("input[name='symptom']"));

            jq('<tr/>', {
                'class': 'newSymptom',
                'id': 'newSymptom',
                html: template
            }).hide().appendTo('#symptomsBody').slideDown('slow');
        });

        function getFormData() {
            jq("#symptomsBody").find("tr:gt(0)").map(function (row) {
                let symptomInput = jq(this).find(("input[name='symptom']"));
                let symptom = symptomInput.attr('data-overlay');

                if (!symptom && symptomInput.val()) {
                    symptom = symptomInput.val();
                }

                let symptomComment = jq(this).find(("input[name='comment']"));
                let comment = symptomComment.val();

                let symptomDuration = jq(this).find(("input[name='duration']"));
                let duration = symptomDuration.val();

                let symptomDurationUnit = jq(this).find(("select[name='durationUnit']"));
                let durationUnit = symptomDurationUnit.val();

                let existingUuid = jq(this).attr("data-overlay");
                let isDisabled = symptomInput.prop('disabled');
                if (existingUuid) {
                    if (!isDisabled) {
                        let commentUuid = symptomComment.attr("data-overlay");
                        let durationUuid = symptomDuration.attr("data-overlay");
                        let durationUnitUuid = symptomDurationUnit.attr("data-overlay");
                        symptomsData.edited.push({
                            "uuid": existingUuid,
                            "symptom": symptom,
                            "commentUuid": commentUuid,
                            "comment": comment,
                            "duration": duration,
                            "durationUuid": durationUuid,
                            "durationUnit": durationUnit,
                            "durationUnitUuid": durationUnitUuid
                        });
                    }
                } else {
                    symptomsData.new.push({
                        "symptom": symptom,
                        "comment": comment,
                        "duration": duration,
                        "durationUnit": durationUnit
                    })
                }
            });

        }

        jq("#editSymptoms").submit(function (e) {
            e.preventDefault();

             // Disable the save button
             jq('#saveSymptoms').prop('disabled', true);

            getFormData();
            let payload = JSON.stringify(symptomsData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editSymptoms", "updateSymptoms") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload, visitId: ${activeVisit.id}},
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patients symptoms have been updated");
                    location.reload();
                }, 'json')
                .fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#saveSymptoms').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editSymptoms">
            <div class="row" ng-app="triageDataApp" ng-controller="TriageDataController"
                 ng-init="getSymptomsData('${visit.uuid}')">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Symptom</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Unit</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="symptomsBody">
                    <tr id="symptomTemplate" style="display: none">
                        <td>
                            <input name="symptom" type="text" class="form-control" disabled />
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <input name="duration" type="text" style="width: 75px"/>
                        </td>
                        <td>
                            <select type="text" class="form-control custom-select"
                                    name="durationUnit" style="min-width: 75px;width: 75px;">
                                <option  value="" disabled selected>Unit </option>
                                <%  durationUnits.each{ %>
                                <option value="${it.getAnswerConcept().getUuid()}" >${it.getAnswerConcept().getName().getName()}</option>
                                <% } %>
                            </select>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% symptomObjects.each { symptomObject -> %>
                    <% if (symptomObject.symptom) { %>
                    <tr data-overlay="${symptomObject.symptom.uuid}">
                        <td>
                            <input name="symptom" type="text" class="form-control"
                                   disabled
                                <% if (symptomObject.symptom.valueCoded) { %>
                                   data-overlay="${symptomObject.symptom.valueCoded.uuid}"
                                   value="${ui.encodeHtmlContent(ui.format(symptomObject.symptom.valueCoded))}"
                                <% } else { %>
                                   data-overlay="${symptomObject.symptom.valueText}"
                                   value="${ui.encodeHtmlContent(ui.format(symptomObject.symptom.valueText))}"
                                <% } %>/>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (symptomObject.comment && symptomObject.comment.valueText) { %>
                                   value="${symptomObject?.comment?.valueText}" <% } %>
                                   data-overlay="${symptomObject?.comment?.uuid}"/>
                        </td>

                        <td>
                            <input type="number" name="duration" disabled style="width: 75px;"
                                <% if (symptomObject.duration && symptomObject.duration.valueNumeric) { %>
                                   value="${symptomObject?.duration?.valueNumeric}" <% } %>
                                   data-overlay="${symptomObject?.duration?.uuid}"/>
                        </td>

                        <td>
                            <select type="text" class="form-control custom-select" disabled
                                    name="durationUnit" data-overlay="${symptomObject?.durationUnit?.uuid}" style="min-width: 75px;width: 75px;">
                                <% def unit = symptomObject?.durationUnit?.valueCoded?.id ?: '' %>
                                <option  value="" disabled selected>Unit ${unit}</option>
                                <%  durationUnits.each{ %>
                                <% if(unit && unit == it.getAnswerConcept().getId()) {%>
                                <option value="${it.getAnswerConcept().getUuid()}" selected>${it.getAnswerConcept().getName().getName()}</option>
                                <% }else{ %>
                                <option value="${it.getAnswerConcept().getUuid()}" >${it.getAnswerConcept().getName().getName()}</option>
                                <% } %>

                                <% } %>
                            </select>
                        </td>

                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddSymptom">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" style="margin-left: 2rem" id="saveSymptoms">Save</button>
            </div>
        </form>
    </div>
</div>
