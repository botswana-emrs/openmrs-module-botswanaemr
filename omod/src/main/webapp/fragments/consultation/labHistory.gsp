<div class="row">
    <div class="col-md-3">
        <!-- Tabs nav -->
        <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
<% labTests.eachWithIndex {summary, index -> %>
<a class="nav-link mb-3 p-3 shadow <%if(index==0){%>active<%}%>" data-toggle="tab" href="#v-pills-lab-history-tab-${index}" role="tab" aria-controls="v-pills-lab-history-tab-${index}" <%if(index==0){%>aria-selected="true"<%}%>>
    <i class="fa fa-clock mr-2"></i>
    <span class="font-weight-bold small text-uppercase">${summary.acceptDate}</span>
</a>
<% } %>
</div>
</div>


<div class="col-md-9">
    <!-- Tabs content -->
    <div class="tab-content" id="v-pills-tabContent">
        <% labResults.eachWithIndex {testResult, index -> %>
        <div class="tab-pane fade shadow rounded bg-white p-5 <%if(index==0){%>show active<%}%>" id="v-pills-lab-history-tab-${index}" role="tabpanel" aria-labelledby="v-pills-lab-history-tab-${index}">
            <h4 class="mb-3 mt-3 pl-3">
                <i class="icon-filter"></i> Laboratory Information
            </h4>
            <span class="text-primary pl-3">Test Name:
                <span class="text-muted">${testResult.testName}</span>
            </span>
            <br/>
            <span class="text-primary pl-3">Test Status:
            <% if(testResult.status.toString().equals("COMPLETED")){%>
                <td>
                    <i class="fa fa-circle text-success"></i>
                    <span class="text-muted px-0" style="background: none;"> Completed </span>
                </td>
                <% } else {%>
                <td>
                    <i class="fa fa-circle text-warning"></i>
                    <span class="text-muted px-0" style="background: none;"> Awaiting </span>
                </td>
                <% } %>
            </span>
            <br/>
            <% if(testResult.resultsList) {%>
                <h5 class="mb-3 mt-3 pl-3">Test Results</h5>
                <% testResult.resultsList.each {key, value -> %>
                    <span class="text-primary pl-3">${key}:
                        <span class="text-muted">${value}</span>
                    </span><br />
                <%}%>
            <%}%>
            <br/>
        </div>
        <%}%>
    </div>
</div>
</div>