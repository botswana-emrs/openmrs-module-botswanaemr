<script type="text/javascript">
    let UrlParams = new URLSearchParams(window.location.search);
       jq(function() {
           fetchNonPharmaPrescription();
           jq("#createNonPharmaPrescriptionForm").on("submit", function(event){
               event.preventDefault();
               var params = {
                   'patientId': jq('#patientId').val(),
                   'nonPharmaPrescription': jq('#nonPharmaPrescription').val(),
                   'visitId': ${activeVisit.id}
               };
               showLoadingOverlay();
               jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/plan/nonPharmaPrescription", "createNonPharmaPrescription")}', params)
                   .success(function (data) {
                       jq().toastmessage('showNoticeToast', "Non-Pharmacological Prescription captured successfully");
                       fetchNonPharmaPrescription();
                       jQuery('#createNonPharmaPrescriptionModal').modal('toggle');
                       hideLoadingOverlay();
       
                   })
           });
           
           jq("#editNonPharmaPrescriptionForm").on("submit", function(event){
               event.preventDefault();
               var params = {
                   'editableNonPharmaPrescriptionPlan': jq('#editableNonPharmaPrescription').val(),
                   'nonPharmaPrescriptionObsUuid': jq('#nonPharmaPrescriptionObsUuid').val()
               };
               showLoadingOverlay();
               jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/plan/nonPharmaPrescription", "editNonPharmaPrescriptionPlan")}', params)
                   .success(function (data) {
                       jq().toastmessage('showNoticeToast', "Non-Pharmacological Prescription edited successfully");
                       jQuery('#editNonPharmaPrescriptionModal').modal('toggle');
                       fetchNonPharmaPrescription();
                       hideLoadingOverlay();
                   })
           });
       });
    
        function fetchNonPharmaPrescription() {
            jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/plan/nonPharmaPrescription", "fetchNonPharmaPrescription")}',
                { 'patientId': UrlParams.get('patientId'), 'visitId': UrlParams.get('visitId') }
               ).done(function(data) {
                   renderNonPharmaPrescription(data);
               }).fail(function(jqXHR) {
                   console.error('Error loading non pharmacological prescriptions');
               });
        }
           
        function renderNonPharmaPrescription(data) {
            const container = document.querySelector('#nonPharmaPrescriptionContainer');
            container.innerHTML = '';
            if (!data || !Array.isArray(data) || data.length === 0 || !data[0].nonPharmaPrescriptionObs) {
                container.innerHTML = `
                    <div class="row no-data-section">
                        <img src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
                        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
                    </div>
                `;
            } else {
                const prescription = data[0].nonPharmaPrescriptionObs;
                container.innerHTML = `
                    <div class="row">
                        <div class="col-md-8">
                            <p>\${prescription}</p>
                        </div>
                        <div class="col-md-4">
                            <button type="button" id="edit-nonPharmaPrescription-plan"
                                class="btn btn-sm btn-dark bg-dark right" data-toggle="modal" 
                                data-target="#editNonPharmaPrescriptionModal">Edit</button>
                        </div>
                    </div>
                `;
            }
        }
</script>
<div class="row">
    <div class="col-md-8">
        <h5 class="text-dark">Non-Pharmacological Prescription</h5>
    </div>
</div>
<div id="nonPharmaPrescriptionContainer"></div>

<div class="col col-sm-12 col-md-12 col-lg-12">
    <button type="button"
        id="create-nonPharmaPrescription-plan"
        class="dashed-button rounded"
        data-toggle="modal"
        data-target="#createNonPharmaPrescriptionModal" <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Non-Pharmacological Prescription
    </button>
</div>
<!-- Create Non-Pharma Prescription Modal -->
<div class="modal fade" id="createNonPharmaPrescriptionModal" data-controls-modal="createNonPharmaPrescriptionModalControls"
    data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
    aria-labelledby="createNonPharmaPrescriptionModalTitle">
    <div id="createNonPharmaPrescriptionData" class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="createNonPharmaPrescriptionModalTitle">Add Non-Pharmacological Prescription</h5>
                <button type="button" id="closeCreateNonPharmaPrescriptionModalBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                    aria-label="Close">
                <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="createNonPharmaPrescriptionForm" method="post">
                    <input type="hidden" value="${patientId}" id="patientId" name="patientId"/>
                    <div class="form-group">
                        <label for="nonPharmaPrescription">Non-Pharmacological Prescription</label>
                        <textarea class="form-control" rows="5" id="nonPharmaPrescription" name="nonPharmaPrescription" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button id="cancelCreateNonPharmaPrescriptionBtn" type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal"
                            aria-label="Close">Cancel</button>
                        <button id="createNonPharmaPrescriptionSaveBtn" type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Non-Pharma Prescription Modal -->
<% if(nonPharmaPrescriptionObs != null) {%>
<div class="modal fade" id="editNonPharmaPrescriptionModal" data-controls-modal="editNonPharmaPrescriptionModalControls"
    data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
    aria-labelledby="editNonPharmaPrescriptionModalTitle">
    <div id="editNonPharmaPrescriptionData" class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="editNonPharmaPrescriptionModalTitle">Edit Non-Pharmacological Prescription</h5>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                    aria-label="Close">
                <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="editNonPharmaPrescriptionForm" method="post">
                    <input type="hidden" value="${nonPharmaPrescriptionObs.uuid}" id="nonPharmaPrescriptionObsUuid" name="nonPharmaPrescriptionObsUuid"/>
                    <div class="form-group">
                        <label for="nonPharmaPrescription">Non-Pharmacological Prescription</label>
                        <textarea class="form-control" rows="5" id="editableNonPharmaPrescription" name="editableNonPharmaPrescription" required>${nonPharmaPrescriptionObs.valueText ?: ""}</textarea>
                    </div>
                    <div class="modal-footer">
                        <button id="cancelEditNonPharmaPrescriptionBtn" type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal"
                            aria-label="Close">Close</button>
                        <button id="editNonPharmaPrescriptionSaveBtn" type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<% } %>