<script type="text/javascript">
    let dietaryPlanparams = new URLSearchParams(window.location.search);

    jq(function() {
        fetchDietaryPlan();
        jq("#createDietaryPlanForm").on("submit", function(event){
            event.preventDefault();
            var params = {
                'patientId': jq('#patientId').val(),
                'dietaryPlan': jq('#dietaryPlan').val(),
                'visitId': ${activeVisit.id}
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/plan/dietaryPlan", "createDietaryPlan")}', params)
                .success(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Dietary plan captured successfully");
                    fetchDietaryPlan();
                    jQuery('#createDietaryPlanModal').modal('toggle');
                    hideLoadingOverlay();
                })
        });

        jq(".edit-button").click(function (e) {
            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html();

            jq("#editableDietaryPlan").val(val);
            jq("#dietaryObsUuid").val(id);
        });
        
        jq("#editDietaryPlanForm").on("submit", function(event){
            event.preventDefault();
            var params = {
                'editableDietaryPlan': jq('#editableDietaryPlan').val(),
                'dietaryObsUuid': jq('#dietaryObsUuid').val()
            };
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/plan/dietaryPlan", "editDietaryPlan")}', params)
                .success(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Dietary plan edited successfully");
                    fetchDietaryPlan();
                    jQuery('#editDietaryPlanModal').modal('toggle');
                    hideLoadingOverlay();
                })
        });
    });

    function fetchDietaryPlan() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/plan/dietaryPlan", "getDietaryPlans")}', 
            { 'patientId': dietaryPlanparams.get('patientId'), 'visitId': dietaryPlanparams.get('visitId') }
        ).done(function(data) {
            if (Array.isArray(data)) {
                renderDietaryPlan(data);
            } else {
                console.error('Unexpected response format');
            }
        }).fail(function(jqXHR) {
            console.error('Error loading data');
        });
    }

    function renderDietaryPlan(data) {
        const container = jq(".dietary-plan-container");
        container.empty();

        if (data.length === 0) {
            container.html(`
                <div class="row no-data-section text-center">
                    <img src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
                    <p class="text-center">No data captured. Add data by clicking "Add" below</p>
                </div>
            `);
        } else {
            data.forEach(item => {
                container.append(`
                    <div class="row">
                        <div class="col-md-8" id="\${item.uuid}">
                            <p>\${item.valueText}</p>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-sm btn-dark bg-dark right edit-button" data-toggle="modal" 
                                    data-target="#editDietaryPlanModal" data-id="\${item.uuid}">
                                Edit
                            </button>
                        </div>
                    </div>
                `);
            });
        }

        container.append(`
            <div class="row">
                <div class="col-md-12">
                    <button type="button" id="create-dietary-plan" class="dashed-button rounded" 
                            data-toggle="modal" data-target="#createDietaryPlanModal">
                        + Add Dietary Plan
                    </button>
                </div>
            </div>
        `);

        jq(".edit-button").off("click").on("click", function() {
            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html();

            jq("#editableDietaryPlan").val(val);
            jq("#dietaryObsUuid").val(id);
        });
    }
</script>

    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Dietary Plan</h5>
        </div>
    </div>
    <div class="dietary-plan-container"></div>

    <div class="modal fade" id="createDietaryPlanModal" data-controls-modal="createDietaryPlanModalControls"
             data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
             aria-labelledby="createDietaryPlanModalTitle">

            <div id="createDietaryPlanData" class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
                 role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title text-white" id="createDietaryPlanModalTitle">Add Dietary Plan</h5>
                        <button type="button" id="closeCreateDietaryPlanModalBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                                aria-label="Close">
                            <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div class="modal-body">
                        <form id="createDietaryPlanForm" method="post">
                            <input type="hidden" value="${patientId}" id="patientId" name="patientId"/>
                            <div class="form-group">
                                <label for="dietaryPlan">Dietary Plan</label>
                                <textarea class="form-control" rows="5" id="dietaryPlan" name="dietaryPlan" required></textarea>
                            </div>
                            <div class="modal-footer pr-0">
                                <button id="cancelCreateDietaryPlanBtn" type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal"
                                        aria-label="Close">Close</button>
                                <button id="createDietaryPlanSaveBtn" type="submit" class="btn btn-primary float-right ml-1">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    <div class="modal fade" id="editDietaryPlanModal" data-controls-modal="editDietaryPlanModalControls"
         data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="editDietaryPlanModalTitle">

        <div id="editDietaryPlanData" class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
             role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white" id="editDietaryPlanModalTitle">Edit Dietary Plan</h5>
                    <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                            aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="editDietaryPlanForm" method="post">
                        <input type="hidden" id="dietaryObsUuid" name="dietaryObsUuid"/>
                        <div class="form-group">
                            <label for="dietaryPlan">Dietary Plan</label>
                            <textarea class="form-control" rows="5" id="editableDietaryPlan" name="editableDietaryPlan" required></textarea>
                        </div>
                        <div class="modal-footer">
                            <button id="cancelEditDietaryPlanBtn" type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal"
                                    aria-label="Close">Close</button>
                            <button id="editDietaryPlanSaveBtn" type="submit" class="btn btn-primary float-right ml-1">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
