<script>
    let prescriptionListParams = new URLSearchParams(window.location.search);
    
    jq(() => {
        getPrescriptionList();
        jq("#deletePrescriptionModal").appendTo("body");

        let orderUuid = '';
        jq(document).on("click", ".open-deletePrescription", function () {
            orderUuid = jq(this).data('id');
        });

        jq('#btnDeletePrescription').click(() => {
            jq.ajax({
                type: "DELETE",
                url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/order/" + orderUuid + "?!purge=false",
                dataType: "json",
                contentType: "application/json",
                success: function () {
                    jq().toastmessage('showSuccessToast', "Prescription was successfully deleted.");
                    jq(function () {
                        jQuery('#modal').modal('toggle');
                    });
                    location.reload();
                },
                error: (error) => {
                    console.log(error)
                    jq().toastmessage('showErrorToast', "Error: " + error);
                }
            });
        });
    });

    function getPrescriptionList() {
        jq.getJSON('${ui.actionLink("botswanaemr", "consultation/plan/prescription/prescriptionList", "getPrescriptionList")}', 
            { 'patientId': prescriptionListParams.get('patientId'), 'visitId': prescriptionListParams.get('visitId')}
        ).done(function(data) {
            renderPrescriptionList(data);
        }).fail(function(jqXHR) {
            console.error('Error loading prescription data');
        });
    }

    function renderPrescriptionList(data) {
        const { prescription, issuedBy, issuedOn, drugOrders, hasActiveVisit } = data;
        if (!drugOrders || drugOrders.length === 0) {
            const noDataHtml = `
                <div class="row no-data-section text-center">
                    <img class="" src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
                    <p class="text-center">No data captured. Add data by clicking "Add" below</p>
                </div>
            `;
            jq("#prescriptionListContainer").html(noDataHtml);
            return;
        }

        const detailsHtml = `
            <div class="row">
                <div class="col">
                    <span>Prescription no:
                        <span class="text-danger">\${prescription}</span>
                    </span>
                </div>
                <div class="col">
                    <span>Issued on:
                        <span class="text-dark">\${issuedOn}</span>
                    </span>
                </div>
                <div class="col">
                    <span>Issued by:
                        <span class="text-dark">\${issuedBy}</span>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <span>
                        <a href="/${ui.contextPath()}/botswanaemr/pharmacy/printPrescriptions.page?patientId=\${data.patientId}&encounterId=\${data.encounterId}" class="btn btn-sm btn-primary text-white">
                            <i class="fa fa-print"></i> Print Prescription
                        </a>
                    </span>
                </div>
            </div>
        `;

        const ordersHtml = drugOrders.map(order => `
            <tr>
                <td>\${order.drug}</td>
                <td>\${order.dosage}</td>
                <td>\${order.route}</td>
                <td>\${order.instructions ? order.instructions.join(' ') : ''}</td>
                <td>\${order.refillDuration}</td>
                <td>\${order.refillRepeat}</td>
                <td>
                    \${hasActiveVisit ? `
                        <a class="color-primary open-editPrescription" data-id="\${order.uuid}" data-toggle="modal" data-target="#editPrescriptionModal">Edit</a> |
                        <a class="color-primary open-deletePrescription" data-id="\${order.uuid}" data-toggle="modal" data-target="#deletePrescriptionModal">Delete</a>
                    ` : ''}
                </td>
            </tr>
        `).join('');

        const tableHtml = `
            <div class="row">
                <div class="table-responsive table-sm table-borderless pl-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Drug</th>
                                <th>Dosage</th>
                                <th>Route</th>
                                <th>Dosing Instructions</th>
                                <th>Duration</th>
                                <th>Refill Repeat</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>\${ordersHtml}</tbody>
                    </table>
                </div>
            </div>
        `;
        const finalHtml = `
            <div>\${detailsHtml}</div>
            <h6 class="pl-3 pt-3">Orders</h6>
            <div>\${tableHtml}</div>
        `;

        jq("#prescriptionListContainer").html(finalHtml);
    }
</script>

<h5 class="pl-3">Prescription</h5>
<div id="prescriptionListContainer"></div>

<div>
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/addPrescription", [patientId: patient.uuid, visit: activeVisit])}
    ${ui.includeFragment("botswanaemr", "consultation/plan/prescription/editPrescription", [patientId: patient.uuid, visit: activeVisit])}
</div>

<div class="row">
    <div class="col col-sm-6 col-md-6 col-lg-6">
        <button class="dashed-button rounded" data-toggle="modal" data-target="#addPrescriptionModal"
                id="btnPrescription" <% if (!isConsultationActive || !hasActiveVisit) { %> disabled <% } %>>+ Add Prescription</button>
    </div>
    <div class="col col-sm-6 col-md-6 col-lg-6">
        <button class="dashed-button rounded" <% if (!isConsultationActive || !hasActiveVisit) { %> disabled <% } %>>
            <a href="${ui.pageLink("botswanaemr", "consultation/drugPrescription", [patientId: patient.uuid, visitId: activeVisit.id])}"
               class="">
                <i class="fa fa-plus-square" aria-hidden="true"></i> Singular prescription</a>
        </button>
    </div>
</div>

<div id="deletePrescriptionModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Are you sure?</h4>
                <button type="button" id="closeBtn" class="btn btn-sm btn-outline-danger" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <p>Do you really want to delete this prescription? This operation may not be undone.</p>
            </div>

            <div class="modal-footer justify-content-end">
                <button type="button" class="btn btn-dark bg-dark" data-dismiss="modal">Close</button>
                <button id="btnDeletePrescription" type="submit" class="btn btn-danger">Yes, delete</button>
            </div>
        </div>
    </div>
</div>
