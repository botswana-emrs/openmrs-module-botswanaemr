<script>
    jQuery(() => {
        jQuery("#editPrescriptionModal").appendTo("body");

        let simplifiedDrugs = [];

        const drugMap = new Map();
        //getSimplifiedDrugs
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','consultation/drugPrescription','getSimplifiedDrugs')}",
            dataType: "json",
            success: (result) => {
                result.data.forEach((simplifiedDrug) => {
                    simplifiedDrugs.push(simplifiedDrug.name);
                    drugMap.set(simplifiedDrug.name, simplifiedDrug.uuid);
                });
            }
        });

        let orderUuid = '';

        jQuery('.open-editPrescription').click(function () {
            orderUuid = jq(this).data('id');
            drugOrderForRevision(orderUuid, (order) => {
                // search for drugName from the map using the value(drugUuid)
                const reversedMap = collectionUtils.reverseMap(drugMap);

                jq('input[name="drug"]').val(reversedMap.get(order.drug.uuid));
                jq('select[name="doseUnit"]').val(order.doseUnits.uuid);
                jq('select[name="frequency"]').val(order.frequency.uuid);
                jq('select[name="route"]').val(order.route.uuid);
                jq('input[name="dosage"]').val(order.dose);
                jq('input[name="refillDuration"]').val(order.duration);
                jq('input[name="refillRepeat"]').val(order.numRefills);
                jq('textarea[name="dosingInstruction"]').val(parseDosingInstruction(order.instructions));
            });
        });

        const parseDosingInstruction = (instruction) => {
            let dosingInstruction = '';
            if (instruction !== null && instruction !== '') {
                try {
                    dosingInstruction = JSON.parse(instruction).dosingInstructions;
                } catch (parseError) {
                    dosingInstruction = '';
                }

            } else {
                dosingInstruction = '';
            }

            return dosingInstruction;
        }

        const drugOrderForRevision = (uuid, callback) => {
            jq.ajax({
                type: "GET",
                url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/order/"+uuid+"?v=full",
                dataType: "json",
                async: false,
                success: function (data) {
                    callback(data)
                }
            });
        }

        const getDosingInstruction = (name) => {
            let dosingInstruction = jQuery("#tbPrescription textarea[name='" + name + "']").val();
            let result = {
                'data': [{
                    'frequency': '',
                    'frequencyDisplay': '',
                    'fromDate': '',
                    'toDate': '',
                }],
                'dosingInstructions': dosingInstruction
            };

            return JSON.stringify(result);
        }

        const getInputValue = (name) => {
            return jQuery("#tbPrescription input[name='" + name + "']").val();
        }

        const getSelectValue = (name) => {
            return jQuery("#tbPrescription select[name='" + name + "']").val();
        }

        const getOverlayValue = (name) => {
            return drugMap.get(jQuery("#tbPrescription input[name='" + name + "']").val());
        }

        const validateFields = (data) => {
            const nullFields = [];
            for (const value of data) {
                if (!value) {
                    nullFields.push(value);
                }
            }
            return nullFields;
        }

        jQuery('#editPrescriptionForm').submit((e) => {
            e.preventDefault();
            const dosage = getInputValue('dosage');
            const drug = getOverlayValue('drug');
            const refillDuration = getInputValue('refillDuration');
            const refillRepeat = getInputValue('refillRepeat');
            const doseUnit = getSelectValue('doseUnit');
            const frequency = getSelectValue('frequency');
            const route = getSelectValue('route');
            const comments = jq('#comments').val();
            const dosingInstruction = getDosingInstruction('dosingInstruction');

            const errors = [];
            errors.push(...validateFields([dosage, drug, refillDuration, refillRepeat,
                doseUnit, frequency, route, comments]));

            if (errors.length === 0) {
                //Construct payload
                const payload = {
                    drugUuid: drug,
                    dosage: dosage,
                    doseUnits: doseUnit,
                    frequency: frequency,
                    numRefills: refillRepeat,
                    duration: refillDuration,
                    durationUnits: "${durationUnits}",
                    route: route,
                    instructions: dosingInstruction,
                    comments: comments
                }

                jq.ajax({
                    type: "POST",
                    url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/drugOrder/"+orderUuid,
                    dataType: "json",
                    data: JSON.stringify(payload),
                    contentType: "application/json",
                    success: function (data) {
                        console.log(data);
                        if (data.status === "201 CREATED") {
                            jq(function () {
                                jQuery('#editPrescriptionModal').modal('toggle');
                            });
                            jq().toastmessage('showSuccessToast', "Prescription Updated Successfully");
                            showLoadingOverlay();
                            getPrescriptionList();
                            jq('#editPrescriptionForm')[0].reset();
                            hideLoadingOverlay();
                        } else {
                            let errorMessage = data.responseJSON.message;
                            if (errorMessage === 'Order.cannot.have.more.than.one') {
                                errorMessage = "Duplicate prescription"
                            }
                            jq().toastmessage('showErrorToast', "Prescription Not Edited!: " + errorMessage);
                        }
                    },
                    error: (error) => {
                        console.log(error)
                        jq().toastmessage('showErrorToast', "Error: " + error);
                    }
                });

            } else {
                jq().toastmessage('showErrorToast', "Empty/Null Fields: Make sure you fill all the fields");
            }
        });

        jq('input[name^=drug]').autocomplete({
            appendTo: "#tbPrescription",
            source : simplifiedDrugs
        });
    });

</script>

<style type="text/css">
    table#tbPrescription th {
        font-weight: normal;
        font-size: 13px;
    }

    table#tbPrescription input {
        max-width: 96px;
    }

    table#tbPrescription select {
        min-width: 120px;
        max-width: 140px !important;
    }
</style>

<div class="modal fade" id="editPrescriptionModal" data-controls-modal="editPrescriptionModalControls"
     data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editPrescriptionModalTitle">

    <div id="editPrescriptionFormData" class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable"
         role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="editPrescriptionTitle">Edit Prescription</h5>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" id="editPrescriptionForm">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover w-auto" id="tbPrescription">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        Drug Name
                                    </th>
                                    <th class="text-center">
                                        Dosage
                                    </th>
                                    <th class="text-center">
                                        Strength
                                    </th>
                                    <th class="text-center">
                                          Route
                                      </th>
                                    <th class="text-center">Frequency/Duration
                                    </th>
                                    <th class="text-center">
                                        Refill
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id='row'>
                                    <td>
                                        <input type="text" readonly name="drug" data-overlay="" class="form-control input-sm" />

<!--                                        <select name="drug" id="drug" class="form-control input-sm">
                                            <option value="">select drug</option>
                                            <% drugs.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayName()}
                                            </option>
                                            <% } %>
                                        </select>
-->
                                    </td>
                                    <td style="width: 40px;">
                                        <label>
                                            <input id="dosage" name="dosage" type="number" placeholder="" class="form-control input-sm"
                                                   style="width: 100px;">
                                        </label>
                                    </td>
                                    <td>
                                        <label for="doseUnit"></label>
                                        <select name="doseUnit" id="doseUnit" class="form-control input-sm">
                                            <% dosageUnits.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="route"></label>
                                        <select name="route" id="route" class="form-control input-sm">
                                            <% routes.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <th class="text-center">
                                                    Frequency
                                                </th>
                                                <th class="text-center">
                                                    Duration (days)
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="frequency"></label>
                                                    <select name="frequency" id="frequency" class="form-control input-sm">
                                                        <option value="">select frequency</option>
                                                        <% frequencies.each { %>
                                                        <option value="${it.getUuid()}">
                                                            ${it.getName()}
                                                        </option>
                                                        <% } %>
                                                    </select>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input id="refillDuration" name="refillDuration" type="number" class="form-control input-sm" min="1" max="180"
                                                               placeholder="" style="width: 100px;">
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <textarea name="dosingInstruction" class="form form-control input-sm" id="" cols="20" rows="3" placeholder="Additional dosing instructions (e.g. take with food, once a day at 8am)"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <label>
                                            <input id="refillRepeat" name="refillRepeat" type="number" class="form-control input-sm"
                                                   placeholder="" style="width: 100px;">
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7"> <label>Comments *
                                        <textarea name="comments" id="comments" cols="30" rows="3" class="form-control"></textarea>
                                        </label>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button id="cancel" type="button" class="btn btn-dark bg-dark" data-dismiss="modal"
                                aria-label="Close">Close</button>
                        <button id="btnEditPrescription" type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

