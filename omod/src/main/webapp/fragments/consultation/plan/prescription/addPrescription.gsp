<script>
    jq(document).ready(function() {
        jQuery("#addPrescriptionModal").appendTo("body");

        jQuery("#addPrescriptionForm").submit(function (e) {
            e.preventDefault();
        });



        //Add table row
        const getTextValues = (name) => {
            const values = [];
            jQuery("#addPrescriptionForm table:first textarea[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const getInputValues = (name) => {
            const values = [];
            jQuery("#addPrescriptionForm table:first input[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const getOverlayValues = (name) => {
            const values = [];
            jQuery("#addPrescriptionForm table:first input[name='" + name + "[]']").each(function () {
                values.push(drugMap.get(jQuery(this).val()));
            });
            return values;
        }

        const getSelectValues = (name) => {
            const values = [];
            jQuery("#addPrescriptionForm table:first select[name='" + name + "[]']").each(function () {
                values.push(jQuery(this).val());
            });
            return values;
        }

        const validateFields = (data) => {
            const nullFields = [];
            for (const value of data) {
                if (!value) {
                    nullFields.push(value);
                }
            }
            return nullFields;
        }

        jQuery('#btnSavePrescription').click(() => {
            const dosages = getInputValues('dosage');
            const drugs = getOverlayValues('drug');
            const refillDurations = getInputValues('refillDuration');
            const refillRepeats = getInputValues('refillRepeat');
            const doseUnits = getSelectValues('doseUnit');
            const frequencies = getSelectValues('frequency');
            const routes = getSelectValues('route');
            const dosingInstruction = getTextValues('dosingInstruction');

            const errors = [];
            errors.push(...validateFields(dosages), ...validateFields(drugs), ...validateFields(refillDurations),
                ...validateFields(refillRepeats), ...validateFields(doseUnits), ...validateFields(frequencies),
                ...validateFields(routes));

            if (errors.length === 0) {
                //Construct payload
                const payload = [];
                for (let i = 0; i < dosages.length; i++) {
                    payload.push({
                        patient: {
                            uuid: "${patient}"
                        },
                        careSetting: {
                            uuid: "${careSettingUuid}"
                        },
                        encounter: {
                            uuid: "${encounter}"
                        },
                        orderer: {
                            uuid: "${orderer}"
                        },
                        orderType: {
                            uuid: "${orderType}"
                        },
                        drug: {
                            uuid: drugs[i]
                        },
                        dosingType: "${dosingType}",
                        dose: dosages[i],
                        doseUnits: {
                            uuid: doseUnits[i]
                        },
                        frequency: {
                            uuid: frequencies[i]
                        },
                        numRefills: refillRepeats[i],
                        //Seems quantity can't be null
                        //Let's fill will 0
                        quantity: "0",
                        quantityUnits: {
                            uuid: "${quantityUnit}"
                        },
                        duration: refillDurations[i],
                        durationUnits: {
                            uuid: "${durationUnits}"
                        },
                        instructions: getDosingInstruction(dosingInstruction[i]),
                        route: {
                            uuid: routes[i]
                        },
                        type: "${type}"
                    })
                }

                //Rest doesn't save array: sucks
                payload.forEach(drugOrder => {
                    jq.ajax({
                        type: "POST",
                        url: '/' + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/order",
                        dataType: "json",
                        data: JSON.stringify(drugOrder),
                        contentType: "application/json",
                        success: function (data) {
                            console.log(data);
                            //close modal
                            jq(function () {
                                jQuery('#addPrescriptionModal').modal('toggle');
                            });
                            jq().toastmessage('showSuccessToast', "Prescriptions added successfully");
                            showLoadingOverlay();
                            getPrescriptionList();
                            jq('#addPrescriptionForm')[0].reset();
                            hideLoadingOverlay();
                        },
                        error: (error) => {
                            let errorMessage = error.responseJSON.message;
                            if (errorMessage === 'Order.cannot.have.more.than.one') {
                                errorMessage = "Duplicate prescription"
                            }
                            jq().toastmessage('showErrorToast', "Error: " + errorMessage);
                        }
                    });
                })
            } else {
                jq().toastmessage('showErrorToast', "Empty/Null Fields: Make sure you fill all the fields");
            }
        });

        const getDosingInstruction = (instruction) => {
            let result = {
                'data': [{
                    'frequency': '',
                    'frequencyDisplay': '',
                    'fromDate': '',
                    'toDate': '',
                }],
                'dosingInstructions': instruction
            };

            return JSON.stringify(result);
        }

        let simplifiedDrugs = [];

        const drugMap = new Map();
        //getSimplifiedDrugs
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','consultation/drugPrescription','getSimplifiedDrugs')}",
            dataType: "json",
            success: (result) => { 
                result.data.forEach((simplifiedDrug) => {
                    if (simplifiedDrugs.indexOf(simplifiedDrug.name) < 0) {
                        simplifiedDrugs.push(simplifiedDrug.name);
                        drugMap.set(simplifiedDrug.name, simplifiedDrug.uuid);
                    }
                });
            }
        });

        let setAutocomplete = function (jqobj) {
            jqobj.autocomplete({
                appendTo: "#addPrescriptionForm table:first",
                source: simplifiedDrugs
            });
        }

        function clonePrescription() {
            let rowCount = jq('#tbBodyPrescription .tr-main').length;
            let index = rowCount + 1;
            let newRow = jQuery('#addPrescriptionForm table:first>tbody>tr:last').clone();
            jq(newRow).attr("id", "prescription" + index)
            newRow.find('input,textarea').each(function () {
                jQuery(this).val('');
            });
            newRow.find('input[name^=refillRepeat]').val(0);

            newRow.insertAfter('#addPrescriptionForm table:first>tbody>tr:last').fadeIn();

            newRow.find('input[name^=drug]').autocomplete({
                appendTo: "#addPrescriptionForm table:first",
                source: simplifiedDrugs
            });
            return index;
        }

        jQuery('#btnAddPrescription').click(function () {
            clonePrescription();

        });

        jQuery('#addPrescriptionForm table:first').on('click', 'a', function () {
            // Don't delete if only none row left
            if (jQuery('#addPrescriptionForm table:first').find('a').length === 1) {
                jq().toastmessage('showErrorToast', "Cannot remove this record");
            } else {
                jQuery(this).parents('tr').remove().fadeIn();
            }
        });

        setAutocomplete(jq('input[name^=drug]'));

        jq("#arvCombination").prop('disabled', true);

        jQuery("#includeArv").change(function () {
            jq("#arvCombination").prop('disabled', !jq(this).is(':checked'));
        });

        jQuery("#includeCtx").prop('disabled', true);
        jQuery("#includeCtx").change(function () {
            if (jq(this).is(':checked')) {
                let ctxDrug = ${cotrimaxDrug.drugId};
                let index = 1;
                let originalDrug = jq("#prescription" + index).find('input[name="drug[]"]').val();
                if (originalDrug) {
                    index = clonePrescription();
                }

                jq("#prescription" + index).find('input[name="drug[]"]').attr("data-overlay", '${cotrimaxDrug.uuid}');
                jq("#prescription" + index).find('input[name="drug[]"]').val("${cotrimaxDrug.fullName}").change();
                jq("#prescription" + index).find('input[name="dosage[]"]').val("${cotrimaxDrug?.maximumDailyDose}");
                jq("#prescription" + index).find('input[name="doseUnit[]"]').val("${cotrimaxDrug?.unitsUuid}");
            }
        });

        jQuery("#includePep").prop('disabled', true);
        <% if (isPepVisitType ) {%>
            jQuery("#includePep").prop('disabled', false);
        <%}%>        
        jQuery("#includePep").change(function () {
            if (jq(this).is(':checked')) {
                let pepDrug = ${pepDrug.drugId};
                let index = 1;
                let originalDrug = jq("#prescription" + index).find('input[name="drug[]"]').val();
                if (originalDrug) {
                    index = clonePrescription();
                }

                jq("#prescription" + index).find('input[name="drug[]"]').attr("data-overlay", '${pepDrug.uuid}');
                jq("#prescription" + index).find('input[name="drug[]"]').val("${pepDrug.fullName}").change();
                jq("#prescription" + index).find('input[name="dosage[]"]').val("${pepDrug?.maximumDailyDose}");
                jq("#prescription" + index).find('input[name="doseUnit[]"]').val("${pepDrug?.unitsUuid}");
            }
        });

        jQuery("#includeTpt").prop('disabled', true);
        jQuery("#includeTpt").change(function () {
            if (jq(this).is(':checked')) {
                let tptDrug = ${tptDrug.drugId};
                let index = 1;
                let originalDrug = jq("#prescription" + index).find('input[name="drug[]"]').val();
                if (originalDrug) {
                    index = clonePrescription();
                }

                jq("#prescription" + index).find('input[name="drug[]"]').attr("data-overlay", '${tptDrug.uuid}');
                jq("#prescription" + index).find('input[name="drug[]"]').val("${tptDrug.fullName}").change();
                jq("#prescription" + index).find('input[name="dosage[]"]').val("${tptDrug?.maximumDailyDose}");
                jq("#prescription" + index).find('input[name="doseUnit[]"]').val("${tptDrug?.unitsUuid}");
            }
        });

        jQuery("#includePrep").prop('disabled', true);
        <% if (isPrepVisitType ) {%>
            jQuery("#includePep").prop('disabled', false);
        <%}%>        
        jQuery("#includePrep").change(function () {
            if (jq(this).is(':checked')) {
                let tptDrug = ${tptDrug.drugId};
                let index = 1;
                let originalDrug = jq("#prescription" + index).find('input[name="drug[]"]').val();
                if (originalDrug) {
                    index = clonePrescription();
                }

                jq("#prescription" + index).find('input[name="drug[]"]').attr("data-overlay", '${tptDrug.uuid}');
                jq("#prescription" + index).find('input[name="drug[]"]').val("${tptDrug.fullName}").change();
                jq("#prescription" + index).find('input[name="dosage[]"]').val("${tptDrug?.maximumDailyDose}");
                jq("#prescription" + index).find('input[name="doseUnit[]"]').val("${tptDrug?.unitsUuid}");
            }
        });

        jq('#addPrescriptionModal').on('show.bs.modal', function(event) {
            let prepCheckbox = jq(this).find('#includePrep');
            prepCheckbox.prop('checked', ${isPepVisitType});
            prepCheckbox.trigger('change');

            let pepCheckbox = jq(this).find('#includePep');
            pepCheckbox.prop('checked', ${isPepVisitType});
            pepCheckbox.trigger('change');

        });

        let regimenComponentDrugsArray = ${regimenComponentDrugs};
        //let regimenComponentDrugsArray = [[{"id":1,"regimenId":1,"regimenComponentId":1,"drug":1,"drugUuid":"0df19424-8e2d-468d-8c99-ea784c6b41c8","drugName":"TLD","dose":60.0,"unitsId":161553,"units":"Milligram","unitsUuid":"161553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","strength":"60mg"}],[{"id":3,"regimenId":1,"regimenComponentId":2,"drug":2,"drugUuid":"07cad2f1-8de3-461d-a23d-dcf789e06895","drugName":"Tenofovir/Lamivudine","dose":100.0,"unitsId":161553,"units":"Milligram","unitsUuid":"161553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","strength":"100mg"},{"id":2,"regimenId":1,"regimenComponentId":2,"drug":3,"drugUuid":"38b3bdb6-1471-41b1-8303-4e9184c8c2bf","drugName":"DTG","dose":50.0,"unitsId":161553,"units":"Milligram","unitsUuid":"161553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","strength":"50mg"}]];

        <% if (!regimenComponents.isEmpty()) {%>
        jq("#arvCombination").on("change", function () {            
            let combinationId = jq(this).val();
            let regimenComponentDrugs = null;

            for(let i = 0; i < regimenComponentDrugsArray.length; i++) {
                for(let j = 0; j < regimenComponentDrugsArray[i].length; j++) {
                    if(regimenComponentDrugsArray[i][j].id === parseInt(combinationId)) {
                        regimenComponentDrugs = regimenComponentDrugsArray[i];
                        break;
                    }
                }
                if(regimenComponentDrugs) break;
            }

            let index = 1;
            let originalDrug = jq("#prescription" + index).find('input[name="drug[]"]').val();
            jq.each(regimenComponentDrugs, function (i, regimenComponentDrug) {
                let tbPrescriptionRows = jq("#tbPrescription tr");
                let drugFound = false;
                tbPrescriptionRows.each(function () {
                    let drugInput = jq(this).find('input[name="drug[]"]');
                    let drugName = drugInput.val();
                    if (regimenComponentDrug.drugName === drugName) {
                        drugFound = true;
                    }
                });

                if (!drugFound) {
                    if (i > 0 || (i === 0 && originalDrug)) {
                        index = clonePrescription();
                    }
                    jq("#prescription" + index).find('input[name="drug[]"]').attr("data-overlay", regimenComponentDrug.drugUuid);
                    jq("#prescription" + index).find('input[name="drug[]"]').val(regimenComponentDrug.drugName).change();
                    jq("#prescription" + index).find('input[name="dosage[]"]').val(regimenComponentDrug.dose);
                    jq("#prescription" + index).find('input[name="doseUnit[]"]').val(regimenComponentDrug.unitsUuid);
                    jq("#prescription" + index).find('input[name="route[]"]').val(regimenComponentDrug.route);
                    jq("#prescription" + index).find('input[name="frequency[]"]').val(regimenComponentDrug.frequencyUuid);
                }
            });
        })
        <% } %>
    });
</script>

<style type="text/css">
table#tbPrescription th {
    font-size: 13px;
}

table#tbPrescription input {
    max-width: 96px;
}

table#tbPrescription select {
    min-width: 120px;
    max-width: 140px !important;
}

</style>

<div class="modal fade" id="addPrescriptionModal" data-controls-modal="AddPrescriptionModalControls"
     data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="AddPrescriptionModalTitle">

    <div id="addPrescriptionFormData" class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable"
         role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="addPrescriptionTitle">Add Prescription</h5>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body p-1">
                <form method="post" id="addPrescriptionForm">
                    <div class="row clearfix">
                        <% if (onArt) { %>
                        <div class="col-md-12">
                            <fieldset class="w-100">
                                <legend>For patients on ART only</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Regimen
                                            <input name="regimen" value='${currentRegimen?.valueCoded?.name?.name}' disabled
                                               class="form-control input-sm"/>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Regimen Line
                                            <input name="regimenLine" value='${currentRegimenLine?.valueCoded?.name?.name}'
                                                   type="text" disabled class="form-control input-sm"/>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>
                                            <input class="form-check-input" type="checkbox" value="" id="includeArv"/>
                                            <label class="form-check-label" for="includeArv">
                                                Include ARVs
                                            </label>
                                        </div>
                                        <div>
                                            <input class="form-check-input" type="checkbox" value="" id="includeCtx">
                                            <label class="form-check-label" for="includeCtx">
                                                Include CTX
                                            </label>
                                        </div>
                                        <div>
                                            <input class="form-check-input" type="checkbox" value="" id="includeTpt">
                                            <label class="form-check-label" for="includeTpt">
                                                Include TPT
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="arvCombination">Preferred ARV combination</label>
                                        <select id="arvCombination" class="form-control input-sm">
                                            <option value="" disabled selected>Select Combination</option>
                                            <% regimenComponents.each { %>
                                            <option value='${it.id}'>${it.componentLabel}</option>
                                            <% } %>
                                        </select>
                                    </div>

                                </div>
                            </fieldset>
                        </div>
                        <% } else { %>
                        <div class="col-md-12 hidden">
                            <fieldset class="w-100">
                                <legend>For Non-ART patients only</legend>
                                
                                <div class="row">
                                        <div class="col-md-6">
                                        <div>
                                            <input class="form-check-input" type="checkbox" value="" id="includeCtx">
                                            <label class="form-check-label" for="includeCtx">
                                                Include CTX
                                            </label>
                                        </div>

                                        <div>
                                            <input class="form-check-input" type="checkbox" value="" id="includePep">
                                            <label class="form-check-label" for="includePep">
                                                Include PEP
                                            </label>
                                        </div>
                                        <div>
                                            <input class="form-check-input" type="checkbox"  id="includePrep" value="" >
                                            <label class="form-check-label" for="includePrep">
                                                Include PREP
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <% } %>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover w-auto" id="tbPrescription">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        Drug Name
                                    </th>
                                    <th class="text-center">
                                        Dose
                                    </th>
                                    <th class="text-center">
                                        Units
                                    </th>
                                    <th class="text-center">
                                        Route
                                    </th>
                                    <th>
                                        Frequency and Duration
                                    </th>
                                    <th class="text-center">
                                        Refill
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="tbBodyPrescription">
                                <tr id="prescription1" id='row' class="tr-main">
                                    <td>
                                        <input type="text" name="drug[]" data-overlay="" placeholder="Type drug name"
                                               class="form-control input-sm" style="min-width: 130px"/>
                                    </td>
                                    <td style="width: 40px;">
                                        <label>
                                            <input id="dosage" name="dosage[]" type="number" placeholder=""
                                                   class="form-control input-sm"
                                                   style="min-width: 65px;">
                                        </label>
                                    </td>
                                    <td>
                                        <label for="doseUnit"></label>
                                        <select name="doseUnit[]" id="doseUnit" class="form-control input-sm">
                                            <option value="">select units</option>
                                            <% dosageUnits.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <label for="route"></label>
                                        <select name="route[]" id="route" class="form-control input-sm">
                                            <option value="">select route</option>
                                            <% routes.each { %>
                                            <option value="${it.getUuid()}">
                                                ${it.getDisplayString()}
                                            </option>
                                            <% } %>
                                        </select>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <th class="text-center">
                                                    Frequency
                                                </th>
                                                <th class="text-center">
                                                    Duration (days)
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="frequency"></label>
                                                    <select name="frequency[]" id="frequency"
                                                            class="form-control input-sm">
                                                        <option value="">select frequency</option>
                                                        <% frequencies.each { %>
                                                        <option value="${it.getUuid()}">
                                                            ${it.getName()}
                                                        </option>
                                                        <% } %>
                                                    </select>
                                                </td>
                                                <td>
                                                    <label>
                                                        <input id="refillDuration" name="refillDuration[]" type="number" value="0" min="1" max="180"
                                                               class="form-control input-sm"
                                                               placeholder="" style="width: 100px;">
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <textarea name="dosingInstruction[]"
                                                              class="form form-control input-sm" id="" cols="20"
                                                              rows="3"
                                                              placeholder="Additional dosing instructions (e.g. take with food, once a day at 8am)"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <label>
                                            <input id="refillRepeat" name="refillRepeat[]" type="number"
                                                   class="form-control input-sm" value="0"
                                                   placeholder="" style="width: 100px;">
                                        </label>
                                    </td>
                                    <td>
                                        <div class="title-margin-left">
                                            <a class="btn btn-sm btn-light right">Delete</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-sm-12 col-md-12 col-lg-12">
                            <button class="dashed-button rounded"
                                    id="btnAddPrescription" <% if (!isConsultationActive) { %> disabled <%
                                } %>>+ Add Prescription</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button id="cancel" type="button" class="btn btn-dark bg-dark" data-dismiss="modal"
                                aria-label="Close">Close</button>
                        <button id="btnSavePrescription" type="button" class="btn btn-primary ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

