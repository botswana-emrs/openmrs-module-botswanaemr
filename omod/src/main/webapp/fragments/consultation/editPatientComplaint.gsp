<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script type="text/javascript">
    let complaintsData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#complaintTemplate").hide();

        jq('#editComplaints').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let comment = tr.find(("input[name='comment']"));

                        let data = {
                            "complaint": tr.attr("data-overlay"),
                            "comment": comment.attr("data-overlay")
                        }
                        complaintsData.deleted.push(data);
                    }

                    jq(e.target).closest('tr').remove();
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                    });
                }
            }
        });

        jq("#btnAddComplaint").on("click", function (e) {
            e.preventDefault();
            let template = jq("#complaintTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });

            jq('<tr/>', {
                'class': 'newComplaint',
                'id': 'newComplaint',
                html: template
            }).hide().appendTo('#complaintsBody').slideDown('slow');
        });

        function getFormData() {
            jq("#complaintsBody").find("tr:gt(0)").map(function (row) {
                let complaintInput = jq(this).find(("textarea[name='complaint']"));
                let complaint = complaintInput.val();
                let complaintComment = jq(this).find(("input[name='comment']"));
                let comment = complaintComment.val();

                let existingUuid = jq(this).attr("data-overlay");
                let isDisabled = complaintInput.prop('disabled');
                if (existingUuid) {
                    if (!isDisabled) {
                        let commentUuid = complaintComment.attr("data-overlay");
                        complaintsData.edited.push({
                            "uuid": existingUuid,
                            "complaint": complaint,
                            "commentUuid": commentUuid,
                            "comment": comment
                        });
                    }
                } else {
                    complaintsData.new.push({
                        "complaint": complaint,
                        "comment": comment
                    })
                }
            });

        }

        jq("#editComplaints").submit(function (e) {
            e.preventDefault();

             // Disable the save button
             jq('#saveComplaint').prop('disabled', true);

            getFormData();
            let payload = JSON.stringify(complaintsData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editPatientComplaint", "updateComplaints") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload, visitId: ${activeVisit.id}},
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patients complaints have been updated");
                    location.reload();
                }, 'json')
                .fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#saveComplaint').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editComplaints">
            <div class="row">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Patient Complaint</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="complaintsBody">
                    <tr id="complaintTemplate" style="display: none">
                        <td>
                            <textarea name="complaint" cols="30" rows="5"></textarea>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% complaintsObjects.each { complaintObject -> %>
                    <tr data-overlay="${complaintObject.complaint.uuid}">
                        <td>
                            <textarea name="complaint" disabled>${complaintObject.complaint.valueText}</textarea>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (complaintObject.comment && complaintObject.comment.valueText) { %>
                                   value="${complaintObject?.comment?.valueText}" <% } %>
                                   data-overlay="${complaintObject?.comment?.uuid}"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddComplaint">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="saveComplaint">Save</button>
            </div>
        </form>
    </div>
</div>
