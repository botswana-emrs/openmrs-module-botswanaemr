<script type="text/javascript">
    jQuery(function () {
        const table = jQuery('#appointmentsTable').DataTable({
            searching: false,
            info: false,
            bLengthChange: false,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });
</script>

<div id="appointments" class="table-responsive">
    <table id="appointmentsTable" class="table table-sm table-borderless">
        <thead class="thead-light">
            <tr>
                <th>Patient</th>
                <th>Time</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <% if(isProvider) {%>
                <% appointments.each {%>
                    <tr>
                        <td>${it.patient}</td>
                        <td>
                            <span class="fa fa-clock-o text-dark"></span>
                            <a href="#" data-toggle="tooltip" title="${appointmentsDateTimeToolTip}">
                                ${it.timeSlot}
                            </a>
                        </td>
                        <td>
                            <span id="appointmentFlag" class="fa fa-circle"></span>
                            <input type="hidden" id="appointmentStatus" readonly value="${it.status}"/>
                            <span id="appointmentStatusText" class="text-muted px-0" style="background: none;">${it.status}</span>
                            <script type="text/javascript">
                                jq(document).ready(function() {
                                    var appointmentStatus = jq("#appointmentStatus").val();
                                    if (appointmentStatus === "Completed") {
                                        jq("#appointmentFlag").toggleClass("text-success");
                                    } else {
                                        jq("#appointmentFlag").toggleClass("text-warning");
                                    }
                                });
                            </script>
                        </td>
                        <td>
                            <a href="/${ ui.contextPath() }/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId=${it.patientObject.uuid}&appointment=${it.appointmentId}&visitId=${it.visit}"
                               class="color-primary">
                                 View
                            </a>
                        </td>
                    </tr>
                <%}%>
            <%}%>
        </tbody>
    </table>
</div>
