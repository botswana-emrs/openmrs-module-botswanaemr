<script>
    let diagnosis1048Params = new URLSearchParams(window.location.search);
    jq(function() {
        let diagnosisConcepts = [
            {
                category: 'Notifiable Diseases',
                concepts: [
                    {name: 'Suspected malaria', icd11Code: '1F4Z'},
                    {name: 'Parasitologically tested malaria', icd11Code: '1F45'},
                    {name: 'Parasitologically confirmed malaria', icd11Code: '1F44'},
                    {name: 'Confirmed Malaria in pregnancy', icd11Code: 'JB63.6Z'},
                    {name: 'Acute malnutrition', icd11Code: '5B52'},
                    {name: 'Diarrhoea (acute) with dehydration', icd11Code: '5C70.Z'},
                    {name: 'Diarrhoea (acute) with no dehydration', icd11Code: 'DA90.0'},
                    {name: 'Diarrhoea with blood', icd11Code: '1A40.Z'},
                    {name: 'Suspected Measles', icd11Code: '1F03.0'},
                    {name: 'Rabies exposure', icd11Code: 'QC90.3'},
                    {name: 'Whooping cough', icd11Code: '1C12.Z'},
                    {name: 'Pneumonia', icd11Code: 'CA40.Z'},
                    {name: 'Influenza-like illness (ILI)', icd11Code: '1E32'},
                    {name: 'No notifiable diseases noted', icd11Code: ''}
                ]
            },
            {
                category: 'Other Immediately Reported Notifiable Diseases',
                concepts: [
                    {name: 'AFP', icd11Code: 'MA19.1'},
                    {name: 'Human rabies', icd11Code: '1C82'},
                    {name: 'Measles', icd11Code: '1F03.0'},
                    {name: 'Neonatal tetanus', icd11Code: '1C15'},
                    {name: 'Plague', icd11Code: '1B93.Z'},
                    {name: 'Viral hemorrhagic fever', icd11Code: '1D86'},
                    {name: 'Cholera', icd11Code: '1A00'},
                    {name: 'Diphtheria', icd11Code: '1C17.Z'},
                    {name: 'Meningococcal meningitis', icd11Code: '1C1C.0'},
                    {name: 'Typhoid fever', icd11Code: '1A07.Z'},
                    {name: 'Pandemic Influenza', icd11Code: '`1E31'},
                    {name: 'No immediately reported notifiable diseases', icd11Code: ''}
                ]
            },
            {
                category: 'Sexually Transmitted Infections (Symptoms, Signs, and Tests)',
                concepts: [
                    {name: 'Urethral discharge syndrome', icd11Code: 'GC02.Z'},
                    {name: 'Genital ulcer syndrome', icd11Code: 'GA00.3'},
                    {name: 'Syphilis (RPR+/VDRL+)', icd11Code: '1A6Z'},
                    {name: 'Genital warts', icd11Code: '1A95.1'},
                    {name: 'Scrotal swelling', icd11Code: 'MF40.2Y'},
                    {name: 'HIV positive', icd11Code: '1C62.Z'},
                    {name: 'No STIs noted', icd11Code: ''},
                    {name: 'Vaginal discharge syndrome', icd11Code: 'MF3A'},
                    {name: 'Lower abdominal pains (PID)', icd11Code: 'MD81.12'},
                    {name: 'STI contacts', icd11Code: ''},
                    {name: 'Hepatitis B', icd11Code: '1E51.0Z'},
                    {name: 'Inguinal bubo', icd11Code: 'BD90.Y'},
                    {name: 'AIDS', icd11Code: '1C62.0'}
                ]
            },
            {
                category: 'Other Diseases/Conditions',
                concepts: [
                    {name: 'Asthma', icd11Code: 'CA23'},
                    {name: 'Schistosomiasis', icd11Code: '1F86.Z'},
                    {name: 'Eye diseases/conditions', icd11Code: '9E1Z'},
                    {name: 'Hypertension', icd11Code: 'BA00.Z'},
                    {name: 'Cardiac failure', icd11Code: 'BD1Z'},
                    {name: 'Suspected MDR-TB', icd11Code: 'MG52.00'},
                    {name: 'Laboratory confirmed XDR-TB', icd11Code: 'MG52.02'},
                    {name: 'Diabetes Mellitus type 2', icd11Code: '5A11'},
                    {name: 'Ear diseases/conditions', icd11Code: '9E1Z'},
                    {name: 'Mental disorders', icd11Code: '6E8Z'},
                    {name: 'Other diseases/conditions', icd11Code: ''},
                    {name: 'Other external causes of injuries', icd11Code: 'NF2Z'},
                    {name: 'Leprosy', icd11Code: '1B20.Z'},
                    {name: 'Helminthiasis', icd11Code: '1F90.Z'},
                    {name: 'Malnutrition', icd11Code: '5B7Z'},
                    {name: 'Stroke', icd11Code: '8B20'},
                    {name: 'Pulmonary tuberculosis', icd11Code: '1B10.Z'},
                    {name: 'Laboratory confirmed MDR-TB', icd11Code: 'MG52.00'},
                    {name: 'Diabetes Mellitus type 1', icd11Code: '5A10'},
                    {name: 'Tonsillitis', icd11Code: 'CA03.Z'},
                    {name: 'Skin conditions', icd11Code: 'ME67'},
                    {name: 'Diseases of the Musculo-skeletal system', icd11Code: 'FC0Z'},
                    {name: 'Injuries secondary to road traffic accidents', icd11Code: 'FC0Z'},
                    {name: 'No other diseases/conditions noted', icd11Code: 'QA02'}
                ]
            }
        ];

        const getDiagnosisObs = function (concept) {
            let diagnosisValue = concept.icd11Code + ' - ' + concept.name;
            const diagnosisObsTemplate = `
            <div class="col-md-6 diagnosisObsTemplate">
                <div class="row">
                    <div class="col-md-9 m-0 p-0">
                        <div class="form-check-inline">
                            <label class="form-check-label p-0 m-0" for="\${concept.name}">
                                <input type="checkbox" id="\${concept.name}" class="form-check-input mt-1 p-2" data-overlay="\${diagnosisValue}" value="\${diagnosisValue}"/>\${concept.name}
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3 m-0 p-0 diagnosisType hidden">
                        <div class="form-check-inline">
                            <label class="form-check-label p-0 m-0">
                                <input type="radio" name="diagnosisType-\${concept.name}" value="N" class="form-check-input m-t-9"  title="New"/>N
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label p-0 m-0">
                                <input type="radio" name="diagnosisType-\${concept.name}" value="R" class="form-check-input m-t-9" title="Repeat" />R
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            `;   
            return diagnosisObsTemplate;         
        };

        // For each of the diagnosisConcpets, create a clone of nursingDiagnosisTemplate 
        diagnosisConcepts.forEach(function(category) {
            let templateClone = jq('#nursingDiagnosisTemplate').clone();
            templateClone.removeAttr('id');
            templateClone.removeClass('hidden');
            templateClone.find('th').text(category.category);
            category.concepts.forEach(function(concept) {
                let diagnosisObsTemplateClone = getDiagnosisObs(concept);
                templateClone.find('.diagnosisData td .dataContainer').append(diagnosisObsTemplateClone);
            });
            jq('#diagnosis1048Form .form-group').append(templateClone);
        });
        

        jq('.diagnosisData').find('input[type=checkbox]').click(function() {
            jq(this).parents('.diagnosisObsTemplate').find('.diagnosisType').toggleClass('hidden');
            if (jq(this).is(':checked')) {
                jq(this).parents('.diagnosisObsTemplate').find('input[type=radio]').prop('checked', false);
            }
        });

        fetch1048Diagnosis();

        let nusingDiagnosisData = {
            "deleted": [],
            "edited": [],
            "new": []
        };
        function getNursingDiagnosisFormData() {

            nusingDiagnosisData = {
                "deleted": [],
                "edited": [],
                "new": []
            };

            jq("#diagnosis1048Form").find(".diagnosisObsTemplate").map(function (row) {
                let diagnosisInput = jq(this).find('input[type=checkbox]');
                let diagnosis = diagnosisInput.attr('data-overlay');

                let diagnosisTypeInput = jq(this).find("input:radio:checked");
                let diagnosisType = diagnosisTypeInput.val();

                let existingUuid = jq(this).attr("data-overlay");
                if (existingUuid) {
                    console.log("diagnosis", diagnosis);
                    nusingDiagnosisData.edited.push({
                        "uuid": existingUuid,
                        "diagnosis": diagnosis,
                        "type": diagnosisType,
                        "form": "nursingDiagnosis"
                    })
                } else if (diagnosis && diagnosisInput.is(':checked')) {
                    nusingDiagnosisData.new.push({
                        "diagnosis": diagnosis,
                        "type": diagnosisType,
                        "form": "nursingDiagnosis"
                    });
                }
            });
        }

        jq("#diagnosis1048Form").submit(function(e){
            e.preventDefault();

            getNursingDiagnosisFormData();

            let payload = JSON.stringify(nusingDiagnosisData);

            jq('button[type=submit]').prop('disabled', true);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editDiagnoses", "updateDiagnoses") }',
                {
                    returnFormat: 'json',
                    patientId: patientId,
                    data: payload,
                    visitId: visitId,
                    diagnosisEncounterType: "c5ce65c6-263f-4eeb-a623-e60129e50be7"
                },
                function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "1048 diagnosis captured successfully");
                    fetch1048Diagnosis();
                    jQuery('#diagnosis1048Modal').modal('toggle');
                    hideLoadingOverlay();
                    jq('button[type=submit]').prop('disabled', false);
                    jq("#diagnosis1048Form").find('input[type=checkbox]').prop('checked', false);
                    jq("#diagnosis1048Form").find('input[type=radio]').prop('checked', false); 
                }, 'json')
                .error(function () {
                    console.log("error");
                    jq('button[type=submit]').prop('disabled', false);
                });
        });


        jq("#edit1048DiagnosisForm").submit(function(e){
            e.preventDefault();
            var params = {
                'editDiagnosis': jq('#editNursingDiagnosis').val(),
                'lastNursingDiagnosisObsId': jq('#lastNursingDiagnosisObsId').val()
            };
            jq('button[type=submit]').prop('disabled', true);
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/assessment/nursingDiagnosis", "editNursingDiagnosis")}', params)
                .success(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Nursing diagnosis edited successfully");
                    fetch1048Diagnosis();
                    jQuery('#edit1048DiagnosisModal').modal('toggle');
                    
                    jq("#diagnosis1048Form").find('input[type=checkbox]').prop('checked', false);
                    jq("#diagnosis1048Form").find('input[type=radio]').prop('checked', false);  
                    hideLoadingOverlay();
                    jq('button[type=submit]').prop('disabled', false);
                });
        });

        let isUpdating = false;
        jq('#btn1048Diagnosis').on('click', function() {
            const existingDiagnoses = jq('.nursing-diagnosis-container .col-md-8').map(function() {
                return jq(this).text().trim();
            }).get();

            const diagnosisAttrib = jq('.nursing-diagnosis-container .diagnosis-attribute').map(function() {
                return jq(this).text().trim();
            }).get();

            if (existingDiagnoses.length > 0) {
                isUpdating = true;
                populateDiagnosisModal(existingDiagnoses, diagnosisAttrib);
            } else {
                isUpdating = false;
                jq('#diagnosis1048Form').trigger('reset');
            }
        });
    });

    function populateDiagnosisModal(diagnoses, attributes) {
        diagnoses.forEach((diagnosis, index) => {
            const diseaseDetails = diagnosis.split(' - ');
            const diseaseId = diseaseDetails[0];
            const diseaseName = diseaseDetails[1];
            const attributeValue = attributes[index].replace('Diagnosis Type: ', '').trim();
            const diagnosisAtrributeType = attributeValue === 'Repeat' ? 'R' : 'N';
            
            jq('#diagnosis1048ModalTitle').text('Update 1048 Diagnosis');
            jq('#diagnosis1048Form input[type="checkbox"]').each(function() {
                const checkboxValue = jq(this).val();
                const checkboxDiseaseName = checkboxValue.split(' - ')[1];

                if (checkboxDiseaseName === diseaseName) {
                    jq(this).prop('checked', true);
                    jq(this).parents('.diagnosisObsTemplate').find('.diagnosisType').removeClass('hidden');

                    jq(this).parents('.diagnosisObsTemplate').find('input[type="radio"]').each(function() {
                        const radioValue = jq(this).val();
                        if (radioValue === diagnosisAtrributeType) {
                            jq(this).prop('checked', true);
                        }
                    });
                }
            });
        });
    }

    function enablePlanTab() {
        jq('#plan-tab').removeClass('disabled').attr('aria-disabled', 'false');
        jq('#plan-tab').addClass('diagnosis-1048');
    }

    function disablePlanTab() {
        jq('#plan-tab').parent('li').addClass('disabled');
        jq('#plan-tab').removeClass('diagnosis-1048');
    }

    function fetch1048Diagnosis() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/assessment/diagnosis", "fetchDiagnosisData")}', 
            { 'visitId': diagnosis1048Params.get('visitId') }
        ).done(function(data) {
            let diagnosis1048isArray = [];
            if (data && Array.isArray(data.diagnosis1048)) {
                diagnosis1048isArray = data.diagnosis1048;
            } else {
                console.error('Unexpected response format or empty data');
            }

            const filteredDiagnosis = diagnosis1048isArray.filter(diagnosis => diagnosis.encounterType === "Nursing Diagnosis Encounter");
            if (filteredDiagnosis.length > 0) {
                render1048Diagnosis(filteredDiagnosis);
            } else {
                render1048Diagnosis([]);
            }
        }).fail(function(jqXHR) {
            console.error('Error loading nursing diagnosis data:', jqXHR);
        });
    }
    
    function render1048Diagnosis(filteredDiagnosis) {
        const nursingDiagnosisContainer = jQuery('.nursing-diagnosis-container');
        const noDataSection = jQuery('#noData1048NursingDiagnosis');
    
        nursingDiagnosisContainer.empty();

        if (nursingDiagnosisContainer.length > 0) {
            enablePlanTab();

            noDataSection.hide();

            if (Array.isArray(filteredDiagnosis) && filteredDiagnosis.length > 0) {
                enablePlanTab();
                if (noDataSection) {
                    jQuery(noDataSection).addClass('d-none').css('display', 'none');
                }

                const uniqueDiagnoses = filteredDiagnosis.filter((diagnosis, index, self) => {
                    return (
                        self.findIndex(
                            d => d.diagnosisValue === diagnosis.diagnosisValue && d.certainty === diagnosis.certainty
                        ) === index
                    );
                });

                uniqueDiagnoses.forEach(diagnosis => {
                    const diagnosisValue = diagnosis.diagnosisValue.replace(/^[A-Za-z0-9]+ - /, '');
                    const diagnosisHtml = `
                        <div class="row">
                            <div class="row col-md-12">
                                <div class="col col-md-8" id="\${diagnosis.id}">
                                    \${diagnosis.diagnosisValue}
                                </div>
                            </div>
                            <div class="col col-md-12 text-sm-left text-black-50">
                                Certainty: \${diagnosis.certainty}
                            </div>
                            <div class="col col-md-12 text-sm-left text-black-50 diagnosis-attribute">
                                Diagnosis Type: \${diagnosis.attributeValue === 'R' ? 'Repeat' : 'New'}
                            </div>
                        </div>
                    `;
                    nursingDiagnosisContainer.append(diagnosisHtml);
                });
            } else {
                const nursingDiagnosisClass = jq('.nursing-diagnosis');
                if(!nursingDiagnosisClass){
                    disablePlanTab();
                }
                if (noDataSection) {
                    jQuery(noDataSection).removeClass('d-none').css('display', 'block');
                }
            }
        }
    }
</script>

<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">1048 Diagnosis</h5>
        </div>
    </div>
    <div class="nursing-diagnosis-container"></div>
    <div class="row no-data-section text-center" id="noData1048NursingDiagnosis" style="display: none;">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <button class="dashed-button rounded"
            id="btn1048Diagnosis" data-toggle="modal" data-target="#diagnosis1048Modal"
        <% if(!isConsultationActive) { %> disabled <% } %> >+ Add/Update 1048 diagnosis</button>
</div>

<div class="modal fade" id="diagnosis1048Modal" tabindex="-1"
     data-controls-modal="diagnosis1048Modal" data-backdrop="static"
     data-keyboard="false" role="dialog"
     aria-labelledby="diagnosis1048ModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="diagnosis1048ModalTitle">Add 1048 diagnosis</h4>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="diagnosis1048Form">
                    <div class="form-group">
                        <textarea class="form-control hidden" rows="5"  id="nursingDiagnosis" name="nursingDiagnosis"></textarea>
                        <table class="table table-borderless hidden" id="nursingDiagnosisTemplate">
                            <thead>
                                <th scope="col" class="text-left">Notifiable Diseases</th>
                            </thead>
                            <tr class="diagnosisData">
                                <td class="m-0 p-0">
                                    <div class="row dataContainer">
                                    </div>                            
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#diagnosis1048Modal").appendTo("body");
    });
</script>
<div class="modal fade" id="edit1048DiagnosisModal" tabindex="-1"
     data-controls-modal="edit1048DiagnosisModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="edit1048DiagnosisModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="edit1048DiagnosisModalTitle">Edit Nursing diagnosis</h4>
                <button type="button" id="editNursingDiagnosisBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="edit1048DiagnosisForm">
                    <input type="hidden" id="lastNursingDiagnosisObsId" name="lastNursingDiagnosisObsId" />
                    <div class="form-group">
                        <label for="editNursingDiagnosis">Diagnosis</label>
                        <textarea class="form-control" rows="5"  id="editNursingDiagnosis" name="editNursingDiagnosis"></textarea>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#edit1048DiagnosisModal").appendTo("body");
    });
</script>
