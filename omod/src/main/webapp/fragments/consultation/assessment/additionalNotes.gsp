<script>
    let additionalNotesParams = new URLSearchParams(window.location.search);
    let additionalNotesVisitId = additionalNotesParams.get('visitId');
    let additionalNotesPatientId = additionalNotesParams.get('patientId');

    jq(function() {
        fetchAssessmentAdditionalNotes();
        jq("#assessmentAdditionalNotesForm").submit(function(e) {
            e.preventDefault();
            var additionalNotes = jq('#assessmentAdditionalNotes').val().trim();
            if (!additionalNotes) {
                jq().toastmessage('showErrorToast', "Additional notes cannot be empty.");
                fetchAssessmentAdditionalNotes();
                return;
            }

            var params = {
                'patientId': <%=patient.id %>,
                'additionalNotes': additionalNotes,
                'locationId': ${location.id},
                'visitId': ${activeVisit.id}
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/assessment/additionalNotes", "addAdditionalNotes")}', params)
                .done(function(data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Additional notes captured successfully");
                    fetchAssessmentAdditionalNotes();
                    jq('#assessmentAdditionalNotes').val('');
                    jQuery('#assessmentAdditionalNotesModal').modal('toggle');
                    hideLoadingOverlay();
                })
                .fail(function(jqXHR) {
                    console.error("Error submitting additional notes:", jqXHR);
                    jq().toastmessage('showErrorToast', "An error occurred. Please try again.");
                });
        });

        jq("body").on('click', '.edit-button', function (e) {
            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html().trim();

            jq("#editAssessmentAdditionalNotes").val(val);
            jq("#lastAssessmentAdditionalObsId").val(id);
        });

        jq("#editAssessmentAdditionalNotesForm").submit(function(e){
            e.preventDefault();

             // Disable the save button
             jq('#saveAdditionalNotes').prop('disabled', true);

            var params = {
                'editAdditionalNotes': jq('#editAssessmentAdditionalNotes').val(),
                'lastAdditionalObsId': jq('#lastAssessmentAdditionalObsId').val()
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/assessment/additionalNotes", "editAdditionalNotes")}', params)
                .done(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Additional notes edited successfully");
                    fetchAssessmentAdditionalNotes();
                    jq('#saveAdditionalNotes').prop('disabled', false);
                    jQuery('#editAssessmentAdditionalNotesModal').modal('toggle');

                    hideLoadingOverlay();
                })
                .fail(function() {
                    console.log("error");
                    // an error message to the user
                    jq().toastmessage('showErrorToast', "An error occurred while editing the Additional notes. Please try again.");

                    // enable the save button
                    jq('#saveAdditionalNotes').prop('disabled', false);
            });
        });
    });

    function fetchAssessmentAdditionalNotes() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/assessment/additionalNotes", "fetchAssessmentAdditionalNotes")}', 
            { 'patientId': additionalNotesPatientId, 'visitId': additionalNotesVisitId }
        ).done(function(data) {
            if (Array.isArray(data)) {
                renderAssesementAdditionalNotes(data);
            } else {
                console.error('Unexpected response format');
            }
        }).fail(function(jqXHR) {
            console.error('Error loading additional notes data');
        });
    }

    function renderAssesementAdditionalNotes(notes) {
        const notesContainer = jQuery('.additional-assesment-notes-container');
        const noDataSection = jQuery('.no-data-section');
        
        notesContainer.empty();

        if (notes.length > 0) {
            noDataSection.hide();

            notes.forEach(note => {
                const noteHtml = `
                    <div class="row">
                        <div class="row col-md-12">
                            <div class="col col-md-8" id="\${note.obsId}">
                                \${note.value}
                            </div>
                            <div class="col-md-4">
                                <a href="#" class="btn btn-sm btn-dark bg-dark right edit-button" data-toggle="modal" data-target="#editAssessmentAdditionalNotesModal"
                                data-id="\${note.obsId}">Edit</a>
                            </div>
                        </div>
                        <div class="col col-md-12 text-sm-left text-black-50">
                            Added on: \${new Date(note.dateCreated).toLocaleString()}
                        </div>
                    </div>
                `;
                notesContainer.append(noteHtml);
            });
            
        } else {
            noDataSection.show();
        }
    }
</script>

<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Additional notes</h5>
        </div>
    </div>
    <div class="additional-assesment-notes-container"></div>
    <div class="row no-data-section text-center" style="display:none;">
        <img src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <button class="dashed-button rounded" id="btnAssessmentAdditionalNotes" data-toggle="modal" data-target="#assessmentAdditionalNotesModal">+ Add additional notes</button>
</div>

<div class="modal fade" id="assessmentAdditionalNotesModal" tabindex="-1"
     data-controls-modal="assessmentAdditionalNotesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="assessmentAdditionalNotesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="assessmentAdditionalNotesModalTitle">Add Additional notes</h4>
                <button type="button" id="assessmentAdditionalNotesBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="assessmentAdditionalNotesForm">
                    <div class="form-group">
                        <label for="assessmentAdditionalNotes">Notes</label>
                        <textarea class="form-control" rows="5"  id="assessmentAdditionalNotes" name="assessmentAdditionalNotes"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#assessmentAdditionalNotesModal").appendTo("body");
    });
</script>
<div class="modal fade" id="editAssessmentAdditionalNotesModal" tabindex="-1"
     data-controls-modal="editAssessmentAdditionalNotesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editAssessmentAdditionalNotesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editAssessmentAdditionalNotesModalTitle">Edit Additional notes</h4>
                <button type="button" id="editAdditionalNotesBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="editAssessmentAdditionalNotesForm">
                    <input type="hidden" id="lastAssessmentAdditionalObsId" name="lastAssessmentAdditionalObsId" />
                    <div class="form-group">
                        <label for="editAssessmentAdditionalNotes">Notes</label>
                        <textarea class="form-control" rows="5"  id="editAssessmentAdditionalNotes" name="editAssessmentAdditionalNotes"></textarea>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1" id="saveAdditionalNotes">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#editAssessmentAdditionalNotesModal").appendTo("body");
    });
</script>
