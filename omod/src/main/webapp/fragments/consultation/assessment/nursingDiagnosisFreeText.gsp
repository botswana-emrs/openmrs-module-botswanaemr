<script>
    let nursingDiagnosisParams = new URLSearchParams(window.location.search);

    jq(function() {
        fetchNursingDiagnosis();

        jq("#btnNursingDiagnosis").on("click", function () {
            jq("#nursingDiagnosisForm")[0].reset();
            jq("#nursingDiagnosis").val("");
        });

        jq("#nursingDiagnosisForm").submit(function(e){
            e.preventDefault();
            var params = {
                'patientId': <%=patient.id %>,
                'diagnosis': jq('#nursingDiagnosis').val(),
                'locationId': ${location.id},
                'visitId': ${activeVisit.id}
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/assessment/nursingDiagnosis", "addNursingDiagnosis")}', params)
                .success(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Nursing diagnosis captured successfully");
                    enablePlanTabForNursingDiagnosis();
                    fetchNursingDiagnosis();
                    jQuery('#nursingDiagnosisModal').modal('toggle');
                    hideLoadingOverlay();
                });
            });

        jq("body").on('click', '.edit-nursing-diagniosis-button', function (e) {
            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html().trim();

            jq("#editNursingDiagnosis").val(val);
            jq("#lastNursingDiagnosisObsId").val(id);
            jQuery('#edit1048DiagnosisModal').modal('toggle');
        });

        jq("#editNursingDiagnosisForm").submit(function(e){
            e.preventDefault();
            var params = {
                'editDiagnosis': jq('#editNursingDiagnosis').val(),
                'lastNursingDiagnosisObsId': jq('#lastNursingDiagnosisObsId').val()
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/assessment/nursingDiagnosis", "editNursingDiagnosis")}', params)
                .success(function (data) {
                    showLoadingOverlay();
                    jq().toastmessage('showNoticeToast', "Nursing diagnosis edited successfully");
                    fetchNursingDiagnosis();
                    jQuery('#nursingDiagnosisModal').modal('toggle');
                    hideLoadingOverlay();
                });
        });
    });

    function fetchNursingDiagnosis() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/assessment/nursingDiagnosis", "getNursingDiagnoses")}', 
            { 'patientId': nursingDiagnosisParams.get('patientId'), 'visitId': nursingDiagnosisParams.get('visitId') }
        ).done(function(data) {
            if (data.nursingDiagnosis && Array.isArray(data.nursingDiagnosis)) {
                renderNursingDiagnosis(data.nursingDiagnosis);
            } else {
                console.error('Unexpected response format');
            }
        }).fail(function(jqXHR) {
            console.error('Error loading nursing diagnosis data');
        });
    }

    function enablePlanTabForNursingDiagnosis() {
        jq(document).trigger('nursingDiagnosisUpdated', { diagnosisRecorded: true });

        const planTab = jq('#plan-tab');
        const parentLi = planTab.parent('li');

        parentLi.removeClass('disabled');
        parentLi.addClass('nursing-diagnosis');
        planTab.attr('aria-disabled', 'false');
    }

    function disablePlanTabForNursingDiagnosis() {
        const planTab = jq('#plan-tab');
        const parentLi = planTab.parent('li');
        
        planTab
            .addClass('disabled')
            .attr('aria-disabled', 'true')
            .on('click', function (e) {
                e.preventDefault();
            });

        parentLi.addClass('disabled');
        parentLi.removeClass('nursing-diagnosis');
    }

    function renderNursingDiagnosis(notes) {
        const notesContainer = jQuery('.free-text-diagnosis-container');
        notesContainer.empty();
        const noDataSection = jq('.no-data-section');
        if (notes && notes.length > 0) {
            enablePlanTabForNursingDiagnosis();
            if (noDataSection) {
                jQuery(noDataSection).addClass('d-none').css('display', 'none');
            }
            notes.forEach(note => {
                const noteHtml = `
                    <div class="row">
                        <div class="row col-md-12">
                            <div class="col col-md-8" id="\${note.lastNursingDiagnosisObsId}">
                                \${note.value.trim()}
                            </div>
                            <div class="col-md-4">
                                <a href="#" class="btn btn-sm btn-dark bg-dark right edit-nursing-diagniosis-button" 
                                data-toggle="modal" data-target="#editAdditionalNotesModal" 
                                data-id="\${note.lastNursingDiagnosisObsId}">Edit</a>
                            </div>
                        </div>
                        <div class="col col-md-12 text-sm-left text-black-50">
                            Added on: \${new Date(note.obsDatetime).toLocaleDateString()}
                        </div>
                    </div>
                `;
                notesContainer.append(noteHtml);
            });
        } else {
            const diagnois1048Class = jq('.diagnosis-1048');
                if(!diagnois1048Class){
                    disablePlanTabForNursingDiagnosis();
                }
            if (noDataSection.length > 0) {
                noDataSection.removeClass('d-none').css('display', 'block');
            }
        }
    }
</script>

<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Nursing diagnosis</h5>
        </div>
    </div>

    <div class="row free-text-diagnosis-container"></div>

    <% if (!nursingDiagnosis) { %>
    <div class="row no-data-section text-center">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
    <% } %>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <button class="dashed-button rounded"
            id="btnNursingDiagnosis" data-toggle="modal" data-target="#nursingDiagnosisModal"
        <% if(!isConsultationActive) { %> disabled <% } %> >+ Add nursing diagnosis</button>
</div>

<div class="modal fade" id="nursingDiagnosisModal" tabindex="-1"
     data-controls-modal="nursingDiagnosisModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="nursingDiagnosisModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="nursingDiagnosisModalTitle">Add Nursing diagnosis</h4>
                <button type="button" id="closeBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="nursingDiagnosisForm">
                    <div class="form-group">
                        <label for="nursingDiagnosis">Diagnosis</label>
                        <textarea class="form-control" rows="5"  id="nursingDiagnosis" name="nursingDiagnosis"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#nursingDiagnosisModal").appendTo("body");
    });
</script>
<% if(lastNursingDiagnosis) {%>
<div class="modal fade" id="editNursingDiagnosissModal" tabindex="-1"
     data-controls-modal="editNursingDiagnosissModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editNursingDiagnosisModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editNursingDiagnosisModalTitle">Edit Nursing diagnosis</h4>
                <button type="button" id="editNursingDiagnosisBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="editNursingDiagnosisForm">
                    <input type="hidden" id="lastNursingDiagnosisObsId" name="lastNursingDiagnosisObsId" />
                    <div class="form-group">
                        <label for="editNursingDiagnosis">Diagnosis</label>
                        <textarea class="form-control" rows="5"  id="editNursingDiagnosis" name="editNursingDiagnosis"></textarea>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%}%>
<script>
    jQuery(function () {
        jQuery("#editNursingDiagnosissModal").appendTo("body");
    });
</script>