<script>
    let diagnosisParams = new URLSearchParams(window.location.search);

    jQuery(function () {
        jQuery("#editDiagnosesModal").appendTo("body");
        jQuery("#editDiagnosesModal").appendTo("body");
        fetchDiagnosisData();
    });

    function fetchDiagnosisData() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/assessment/diagnosis", "fetchDiagnosisData")}', 
            {'visitId': diagnosisParams.get('visitId') }
        ).done(function(data) {
            if (data && Array.isArray(data.provisionalDiagnoses) && Array.isArray(data.confirmedDiagnoses) && typeof data.diagnosisAttributes === 'object') {
                renderDiagnoses(data.provisionalDiagnoses, data.confirmedDiagnoses, data.diagnosisAttributes);
            } else {
                console.error('Unexpected response format', data);
            }
        }).fail(function(jqXHR) {
            console.error('Error loading diagnosis data', jqXHR);
        });
    }

    function renderDiagnoses(provisionalDiagnoses, confirmedDiagnoses, diagnosisAttributes) {
        const diagnosesContainer = jQuery('#diagnoses-container');
        diagnosesContainer.empty();
        const filteredProvisionalDiagnoses = provisionalDiagnoses.filter(
            diagnosis => diagnosis.encounterType !== 'Nursing Diagnosis Encounter'
        );

        const filteredConfirmedDiagnoses = confirmedDiagnoses.filter(
            diagnosis => diagnosis.encounterType !== 'Nursing Diagnosis Encounter'
        );

        function renderDiagnosisItem(diagnosis, attribute) {
            let diagnosisItem = '';
            if (diagnosis.diagnosisValue) {
                diagnosisItem += `<li>\${diagnosis.diagnosisValue}`;
                if (attribute === "N") {
                    diagnosisItem += ' <span> - New</span>';
                } else if (attribute === "R") {
                    diagnosisItem += ' <span> - Repeat</span>';
                }
                diagnosisItem += '</li>';
            }

            if (diagnosis.nonCoded) {
                diagnosisItem += `<li>\${diagnosis.nonCoded}`;
                if (attribute === "N") {
                    diagnosisItem += ' <span> - New</span>';
                } else if (attribute === "R") {
                    diagnosisItem += ' <span> - Repeat</span>';
                }
                diagnosisItem += '</li>';
            }
            
            return diagnosisItem;
        }

        if (filteredConfirmedDiagnoses.length > 0) {
            let confirmedList = '<h6>Confirmed</h6><ul class="list">';
            filteredConfirmedDiagnoses.forEach(diagnosis => {
                const attribute = diagnosisAttributes[diagnosis.id] || "";
                confirmedList += renderDiagnosisItem(diagnosis, attribute);
            });
            confirmedList += '</ul>';
            diagnosesContainer.append(confirmedList);
        }

        if (filteredProvisionalDiagnoses.length > 0) {
            let provisionalList = '<h6>Provisional</h6><ul class="list">';
            filteredProvisionalDiagnoses.forEach(diagnosis => {
                const attribute = diagnosisAttributes[diagnosis.id] || "";
                provisionalList += renderDiagnosisItem(diagnosis, attribute);
            });
            provisionalList += '</ul>';
            diagnosesContainer.append(provisionalList);
        }

        if (filteredConfirmedDiagnoses.length > 0 || filteredProvisionalDiagnoses.length > 0) {
            const firstDiagnosis = filteredConfirmedDiagnoses[0] || filteredProvisionalDiagnoses[0];
            const dateCreated = firstDiagnosis.dateCreated;
            if (dateCreated) {
                const dateFormatted = formatDate(dateCreated);
                diagnosesContainer.append(
                    `<div class="col col-md-12 text-sm-left text-black-50">Added on: \${dateFormatted}</div>`
                );
            }
        }
    }

    function formatDate(date) {
        const formattedDate = new Date(date);
        return formattedDate.toLocaleDateString();
    }
</script>
<% if (!hasDiagnoses) { %>
<div class="row">
    <div class="col-md-12">
        <h5 class="text-dark">Medical Diagnosis</h5>
    </div>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row no-data-section text-center">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
</div>
<% } else { %>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Diagnosis</h5>
        </div>
        <div class="col-md-4">
            <% if(isConsultationActive) { %>
                <a href="#" class="btn btn-sm btn-dark right" data-toggle="modal" data-target="#editDiagnosesModal">Edit</a>
            <% } %>
        </div>
    </div>

    <div class="row">
        <div class="col col-md-12" id="diagnoses-container"></div>
        <div class="col col-md-12 text-sm-left text-black-50">
            Added on: ${ui.format(diagnoses[0].dateCreated)}
        </div>
    </div>
</div>
<% } %>
<div class="col-md-12">
    <button type="button" class="dashed-button rounded" data-toggle="modal" data-target="#editDiagnosesModal" <% if(!isConsultationActive) { %> disabled <% } %> >+ Add diagnosis</button>
</div>
<!-- Modal goes here -->
<div class="modal fade" id="editDiagnosesModal" tabindex="-1"
     data-controls-modal="editDiagnosesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editDiagnosesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editDiagnosesModalTitle">
                    <% if (hasDiagnoses) { %>
                    Edit Diagnoses
                    <% } else { %>
                    Add Diagnoses
                    <% } %>
                </h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "consultation/editDiagnoses", [patientId: patient.patient.uuid, diagnoses: diagnoses, diagnosisAttributes:diagnosisAttributes, visitId: visit?.id]) }
                </div>
            </div>
        </div>
    </div>
</div>

<!--
 TODO
* Get concept Id for Additional notes. Ensure that a grouping concept has been created for it (Assessment additional notes)
* Create an obs that is added to the current consultation encounter
 -->

<!--
 Search for diagnosis
 Request: https://demo.openmrs.org/openmrs/coreapps/diagnoses/search.action?&term=diar
 Result:
 [{"word":null,"conceptName":{"id":2117,"uuid":"2117BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB","conceptNameType":"FULLY_SPECIFIED","name":"Diarrhea, chronic"},"concept":{"id":5018,"uuid":"5018AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","conceptMappings":[{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"236071009","name":null,"conceptSource":{"name":"SNOMED CT"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"5018","name":null,"conceptSource":{"name":"CIEL"}}},{"conceptMapType":"NARROWER-THAN","conceptReferenceTerm":{"code":"K52.9","name":null,"conceptSource":{"name":"ICD-10-WHO"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"15","name":null,"conceptSource":{"name":"AMPATH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"7544","name":null,"conceptSource":{"name":"PIH Malawi"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"72319","name":null,"conceptSource":{"name":"IMO ProblemIT"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"5018","name":null,"conceptSource":{"name":"AMPATH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"5018","name":null,"conceptSource":{"name":"PIH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"1431","name":null,"conceptSource":{"name":"PIH"}}}],"preferredName":"Diarrhea, chronic"}},{"word":null,"conceptName":{"id":38659,"uuid":"38659BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB","conceptNameType":"FULLY_SPECIFIED","name":"Hemorrhagic diarrhea"},"concept":{"id":138868,"uuid":"138868AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","conceptMappings":[{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"300465","name":null,"conceptSource":{"name":"IMO ProblemIT"}}},{"conceptMapType":"NARROWER-THAN","conceptReferenceTerm":{"code":"A09.0","name":null,"conceptSource":{"name":"ICD-10-WHO"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"2135","name":null,"conceptSource":{"name":"PIH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"95545007","name":null,"conceptSource":{"name":"SNOMED CT"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"138868","name":null,"conceptSource":{"name":"CIEL"}}}],"preferredName":"Hemorrhagic diarrhea"}},{"word":null,"conceptName":{"id":107940,"uuid":"107940BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB","conceptNameType":null,"name":"Bacterial diarrhea"},"concept":{"id":148023,"uuid":"148023AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","conceptMappings":[{"conceptMapType":"NARROWER-THAN","conceptReferenceTerm":{"code":"A04.9","name":null,"conceptSource":{"name":"ICD-10-WHO"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"148023","name":null,"conceptSource":{"name":"CIEL"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"2736","name":null,"conceptSource":{"name":"PIH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"274080003","name":null,"conceptSource":{"name":"SNOMED CT"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"370120","name":null,"conceptSource":{"name":"IMO ProblemIT"}}}],"preferredName":"Bacterial gastroenteritis"}},{"word":null,"conceptName":{"id":105591,"uuid":"105591BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB","conceptNameType":null,"name":"Diarrhea, bacterial"},"concept":{"id":148023,"uuid":"148023AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA","conceptMappings":[{"conceptMapType":"NARROWER-THAN","conceptReferenceTerm":{"code":"A04.9","name":null,"conceptSource":{"name":"ICD-10-WHO"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"148023","name":null,"conceptSource":{"name":"CIEL"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"2736","name":null,"conceptSource":{"name":"PIH"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"274080003","name":null,"conceptSource":{"name":"SNOMED CT"}}},{"conceptMapType":"SAME-AS","conceptReferenceTerm":{"code":"370120","name":null,"conceptSource":{"name":"IMO ProblemIT"}}}],"preferredName":"Bacterial gastroenteritis"}}]


 Diagnoses are saved as Encounter diagnosis
Patient note: Non-coded -Diagnosis 162169: Text of encnounter note
https://api.openconceptlab.org/orgs/CIEL/sources/CIEL/
 -->
