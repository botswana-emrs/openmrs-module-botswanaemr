<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script type="text/javascript">
    let dietData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#dietTemplate").hide();

        jq('#editDiets').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let comment = tr.find(("input[name='comment']"));
                        let amount = tr.find(("input[name='amount']"));

                        let data = {
                            "diet": tr.attr("data-overlay"),
                            "comment": comment.attr("data-overlay"),
                            "amount": amount.attr("data-overlay")
                        }
                        dietData.deleted.push(data);
                    }

                    jq(e.target).closest('tr').remove();
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                    });
                }
            }
        });

        jq("#btnAddDiet").on("click", function (e) {
            e.preventDefault();
            let template = jq("#dietTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input').map(function () {
                jq(this).prop("disabled", false);
                jq(this).attr("name", this.name);
            });

            jq('<tr/>', {
                'class': 'newDiet',
                'id': 'newDiet',
                html: template
            }).hide().appendTo('#dietsBody').slideDown('slow');
        });

        function getFormData() {
            jq("#dietsBody").find("tr:gt(0)").map(function (row) {
                let dietInput = jq(this).find(("input[name='diet']"));
                let diet = dietInput.val();
                console.log("diet", dietInput);
                console.log(diet);
                let dietComment = jq(this).find(("input[name='comment']"));
                let comment = dietComment.val();
                let amountInput = jq(this).find(("input[name='amount']"));
                let amount = amountInput.val();

                let existingUuid = jq(this).attr("data-overlay");
                let isDisabled = dietInput.prop('disabled');
                if (existingUuid) {
                    if (!isDisabled) {
                        let commentUuid = dietComment.attr("data-overlay");
                        let amountUuid = amountInput.attr("data-overlay");
                        dietData.edited.push({
                            "dietUuid": existingUuid,
                            "diet": diet,
                            "commentUuid": commentUuid,
                            "comment": comment,
                            "amountUuid": amountUuid,
                            "amount": amount
                        });
                    }
                } else {
                    dietData.new.push({
                        "diet": diet,
                        "comment": comment,
                        "amount": amount
                    })
                }
            });

        }

        jq("#editDiets").submit(function (e) {
            e.preventDefault();

             // Disable the save button
             jq('#saveDiet').prop('disabled', true);
            
            showLoadingOverlay();
            getFormData();
            let payload = JSON.stringify(dietData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editDiet", "updateDiet") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload, visitId: ${activeVisit.id}},
                function (data) {

                }, 'json').success(function (data) {  

                    jq().toastmessage('showNoticeToast', "The patients diets have been updated");
                    fetchDietData();

                    jQuery('#editDietModal').modal('toggle');

                    hideLoadingOverlay();
                }).fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#saveDiet').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editDiets">
            <div class="row" ng-app="triageDataApp" ng-controller="TriageDataController"
                 ng-init="getDietData('${visit.uuid}')">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Diet</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="dietsBody">
                    <tr id="dietTemplate" style="display: none">
                        <td>
                            <input name="diet" type="text"/>
                        </td>
                        <td>
                            <input name="amount" type="number" min="1" max="1000"/>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% dietObjects.each { dietObject -> %>
                    <tr data-overlay="${dietObject.diet.uuid}">
                        <td>
                            <input type="text" name="diet"  disabled
                                <% if (dietObject.diet && dietObject.diet.valueText) { %>
                                   value="${dietObject?.diet?.valueText}" <% } %>
                                   data-overlay="${dietObject?.diet?.uuid}"/>
                        </td>
                        <td>
                            <input type="number" name="amount"  disabled
                                <% if (dietObject.amount && dietObject.amount.valueNumeric) { %>
                                   value="${dietObject?.amount?.valueNumeric}" <% } %>
                                   data-overlay="${dietObject?.amount?.uuid}"/>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (dietObject.comment && dietObject.comment.valueText) { %>
                                   value="${dietObject?.comment?.valueText}" <% } %>
                                   data-overlay="${dietObject?.comment?.uuid}"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddDiet">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="saveDiet">Save</button>
            </div>
        </form>
    </div>
</div>
