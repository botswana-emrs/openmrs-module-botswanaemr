<div class="card">
    <h5>Past Consultations</h5>
    <% consultationEncounters.each { %>
    <ul class="list-group">
        <li class="list-group-item">
            <small>
                Treated by
                <span>${it.creator}</span> on
                <span class="text-success"> <u><a class="link-primary" href="${ui.pageLink('botswanaemr', 'consultation/consultation', [patientId: patient.id, caseId: it.caseId, visitId: it.visitId])}" target="_blank">${it.encounterDate.format("dd-MMM-yyyy")} </a></u></span>
                <span class="text-muted">at ${it.facility}</span>
            </small>
        </li>
    </ul>
    <% } %>
</div>