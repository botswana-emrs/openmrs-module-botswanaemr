
<script>
  jQuery(function () {
    jQuery("#captureNewChecksModal").appendTo("body");
    jQuery("#viewPreviousChecksModal").appendTo("body");
    jQuery("#editCaptureChecksModal").appendTo("body");
    <!-- jQuery('[data-toggle="popover"]').popover();-->
  });
</script>
<div class="row pt-0">
    <% if (visit == null) {%>
    <div class="col col-sm-12 col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!</strong>
        </div>
    </div>
    <%} else {%>
    <div class="col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
        <div class="row">
            <div class="col-12 ml-0 mr-0">
                <% if(isPickedState) {%>
                    <h5 class="text-dark mb-3 pl-3">Vitals</h5>
                    <button type="button"
                            class="btn btn-sm bg-white text-primary btn-outline-info mb-3 ml-3" data-toggle="modal" data-target="#viewPreviousChecksModal">
                        View previous checks
                    </button>
                    <% if(sameUserAsCurrent) {%>
                        <button class="btn btn-sm btn-dark bg-dark float-center mb-3 mx-5" data-toggle="modal" data-target="#captureNewChecksModal">
                            Capture New Checks
                        </button>
                        <button class="btn btn-sm btn-primary float-right mb-3" data-toggle="modal" data-target="#editCaptureChecksModal">
                            Edit Captured Checks
                        </button>
                    <%} else {%>
                        <button class="btn btn-sm btn-primary float-right mb-3 mr-3" data-toggle="modal" data-target="#captureNewChecksModal">
                            Capture New Checks
                        </button>
                    <%}%>
                <%}%>
            </div>
        </div>

        <% if(currentTriageLastEncounter) {%>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <% if(currentSystollic && currentDiastollic) {%>
                            <div class="col-4" style="margin-bottom: 10px">
                                <div class="card" style="height: 100%">
                                    <div class="stat-widget-one">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="stat-text">Blood pressure (mmHg)</div>
                                                    <div class="stat-digit" ><span style="color:{{getColorCode('Systolic', '${currentSystollic}', '${patient.patient.person.age}')}};">${currentSystollic}</span> / ${currentDiastollic}</div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        <%}%>
                        <% if(currentTemperature) {%>
                            <div class="col-4" style="margin-bottom: 10px">
                                <div class="card" style="height: 100%">
                                    <div class="stat-widget-one">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="stat-text">Temperature(&#8451;)</div>
                                                <% if(temperatureHasChanged) {%>
                                                    <div class="stat-digit" style="color: #FF0000">${currentTemperature}</div>
                                                    <div class="stat-digit" >${previousTemperature}
                                                        <i class='fas fa-caret-up' style='font-size:36px;color:#FF0000'></i>
                                                        <a href="#" data-placement="top" data-toggle="popover" data-html="true" title="Last edited by: ${currentTriageLastEncounter.creator} on ${currentTriageLastEncounter.encounterDatetime}">
                                                            <i class="fa fa-info-circle"></i>
                                                        </a>
                                                    </div>

                                                <%} else {%>
                                                    <div class="stat-digit" style="color:{{getColorCode('Temperature', '${currentTemperature}', '${patient.patient.person.age}')}};">${currentTemperature}</div>
                                                <%}%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <%}%>
                        <% currentTriageData.each {%>
                            <div class="col-4" style="margin-bottom: 10px">
                                <div class="card" style="height: 100%">
                                    <div class="stat-widget-one">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="stat-text" >${it.name}</div>
                                                <div class="stat-digit" style="color:{{getColorCode('${it.name}', '${it.value}', '${patient.patient.person.age}')}};">${it.value}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <%}%>

                    </div>
                </div>
            </div>
        <%}%>
    </div>
    <%}%>
</div>

<% if (isConsultation) { %>
<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/objective/physicalExam", [visit: activeVisit?.uuid])}
</div>

<div class="row pt-0">
    ${ui.includeFragment("botswanaemr", "consultation/objective/additionalNotes", [visit: activeVisit?.uuid])}
</div>
<% } %>

<!-- modals -->
<div class="modal fade" id="viewPreviousChecksModal" tabindex="-1" data-controls-modal="viewPreviousChecksModal"
     data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="viewPreviousChecksModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="viewPreviousChecksModalTitle">Previous Vital Checks</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <% if(previousTriageLastEncounter) {%>
                <div>
                    Date: ${ui.format(previousTriageLastEncounter.encounterDatetime)} <br />
                    Recorded by: ${ui.format(previousProvider)}
                </div>
                <div class="col-12">
                    <div class="row">
                        <% if(previousSystollic && previousDiastollic) {%>
                            <div class="col-4" style="margin-bottom: 10px">
                                <div class="card" style="height: 100%">
                                    <div class="stat-widget-one">
                                        <div class="row">
                                                <div class="col-12">
                                                    <div class="stat-text">Blood pressure (mmHg)</div>

                                                    <div class="stat-digit">${previousSystollic} / ${previousDiastollic}
                                                    </div>

                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <%}%>
                        <% if(previousTemperature) {%>
                        <div class="col-4" style="margin-bottom: 10px">
                            <div class="card" style="height: 100%">
                                <div class="stat-widget-one">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="stat-text">Temperature(&#8451;)</div>
                                            <div class="stat-digit" >${previousTemperature}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%}%>
                        <% previousTriageData.each {%>
                        <div class="col-4" style="margin-bottom: 10px">
                            <div class="card" style="height: 100%">
                                <div class="stat-widget-one">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="stat-text">${it.name}</div>
                                            <div class="stat-digit">${it.value}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%}%>
                    </div>
                </div>
                <%} else {%>
                <div>
                    <p>No previous triage information</p>
                </div>
                <%}%>
            </div>
            <hr />
            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="captureNewChecksModal" tabindex="-1"
     data-controls-modal="captureNewChecksModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="captureNewChecksModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="captureNewChecksModalTitle">Capture Vitals</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "newChecks") }
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editCaptureChecksModal" tabindex="-1"
     data-controls-modal="editCaptureChecksModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editCaptureChecksModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editCaptureChecksModalTitle">Edit Captured Vitals</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    <% if(currentTriageLastEncounter) {%>
                        <input type="hidden" name="encounterToEdit" id="encounterToEdit" value="${currentTriageLastEncounter.id}"/>
                    <%}%>
                    ${ ui.includeFragment("botswanaemr", "editChecks") }
                </div>
            </div>
        </div>
    </div>
</div>
