<script type="text/javascript">

    let physicalExamsParams = new URLSearchParams(window.location.search);
        
    jq(function() {
        getPhysicalExams();

        jq('#physicalExamModal').on('show.bs.modal', function () {
            jq('#addPhysicalExaminationForm')[0].reset();
        });

        jq('#physicalExamEditModal').on('show.bs.modal', function () {
            jq('#editPhysicalExaminationForm')[0].reset();
        });

        jq("#addPhysicalExaminationForm").submit(function(e){
            e.preventDefault();
            var params = {
                'patientId': <%=patient.id%>,
                'physicalExam': jq('#physical-exam').val(),
                'heent': jq('#heent').val(),
                'respiratory': jq('#respiratory').val(),
                'cardiovascular': jq('#cardiovascular').val(),
                'skin': jq('#skin').val(),
                'endocrine': jq('#endocrine').val(),
                'gi': jq('#gi').val(),
                'womenHealth': jq('#womenHealth').val(),
                'cns': jq('#cns').val(),
                'growth': jq('#growth').val(),
                'uroGenital': jq('#uroGenital').val(),
                'locationId': <%=location.id%>
            };

                showLoadingOverlay();

                jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/physicalExam", "addPhysicalExamination")}', params)
                    .success(function (data) {
                
                jq().toastmessage('showNoticeToast', "Physical examination captured successfully");
                getPhysicalExams();
                jq('#addPhysicalExaminationForm')[0].reset();
                jQuery('#physicalExamModal').modal('toggle');
                hideLoadingOverlay();
            })
        });

        jq(".edit-button").click(function (e) {
            var parentSection = jq(this).closest('.exam-section');
            function getValue(attribute) {
                return parentSection.find(`div[exam-type="` + attribute + `"]`).text().split(': ')[1].trim();
            }

            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html();

            jq("#editPhysicalExam").val(val);
            jq("#lastPhysicalObsId").val(id);

            jq("#editPhysicalExam").val(getValue('general'));
            jq("#editHeent").val(getValue('heent'));
            jq("#editRespiratory").val(getValue('respiratory'));
            jq("#editCardiovascular").val(getValue('cardiovascular'));
            jq("#editSkin").val(getValue('skin'));
            jq("#editEndocrine").val(getValue('endocrine'));
            jq("#editGi").val(getValue('gi'));
            jq("#editWomenHealth").val(getValue('womenHealth'));
            jq("#editCns").val(getValue('cns'));
            jq("#editGrowth").val(getValue('growth'));
            jq("#editUroGenital").val(getValue('uroGenital'));

        });

        jq("#editPhysicalExaminationForm").submit(function(e){
            e.preventDefault();

            // Disable the save button
            jq('#savePhysicalExam').prop('disabled', true);

            var params = {
                'lastPhysicalObsId': jq('#lastPhysicalObsId').val(),
                'obsGroupId': jq('#lastPhysicalObsId').val(),
                'editPhysicalExam': jq('#editPhysicalExam').val(),
                'editHeent': jq('#editHeent').val(),
                'editRespiratory': jq('#editRespiratory').val(),
                'editCardiovascular': jq('#editCardiovascular').val(),
                'editSkin': jq('#editSkin').val(),
                'editEndocrine': jq('#editEndocrine').val(),
                'editGi': jq('#editGi').val(),
                'editWomenHealth': jq('#editWomenHealth').val(),
                'editCns': jq('#editCns').val(),
                'editGrowth': jq('#editGrowth').val(),
                'editUroGenital': jq('#editUroGenital').val()
            };
            showLoadingOverlay();
            jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/physicalExam", "editPhysicalExam")}', params)
                .done(function (data) {
                jq().toastmessage('showNoticeToast', "Physical examination edited successfully");
                getPhysicalExams();
                jq('#editPhysicalExaminationForm')[0].reset();

                jq('#physicalExamEditModal').removeClass('show').css('display', 'none').attr('aria-hidden', 'true');
                jq('.modal-backdrop').remove();

                hideLoadingOverlay();

                })
                .fail(function() {
                    console.log("error");
                    // an error message to the user
                    jq().toastmessage('showErrorToast', "An error occurred while editing the physical examination. Please try again.");

                    // enable the save button
                    jq('#savePhysicalExam').prop('disabled', false);
                });
            });
        });

    function getPhysicalExams() {
        jq.getJSON('${ui.actionLink("botswanaemr", "consultation/objective/physicalExam", "getPhysicalExams")}',
        {'patientId': physicalExamsParams.get('patientId'),'visitId': physicalExamsParams.get('visitId')}
        ).done(function (data) {
            console.log('data', data)
            renderPhysicalExams(data);
            }).fail(function (jqXHR) {
            console.error('Error loading phsyical exams data');
        });
    }

    function renderPhysicalExams(data) {
        if (data && data.length) {
            let examHtml = '';
            const exams = data[0].additionalExams;
            exams.forEach(exam => {
                examHtml += `
                <div class="row">
                    <div class="row col-md-12 exam-section">
                    <div class="col-md-10" id="\${exam.obsId}">
                        <div class="row text-row">
                        <div class="text-container pr-1" exam-type="general">General: <b>\${exam.general || ''}</b></div>
                        <div class="text-container pr-1" exam-type="heent">Heent: <b>\${exam.heent || ''}</b></div>
                        <div class="text-container pr-1" exam-type="respiratory">Respiratory: <b>\${exam.respiratory || ''}</b></div>
                        <div class="text-container pr-1" exam-type="cardiovascular">Cardiovascular: <b>\${exam.cardiovascular || ''}</b></div>
                        <div class="text-container pr-1" exam-type="skin">Skin: <b>\${exam.skin || ''}</b></div>
                        <div class="text-container pr-1" exam-type="endocrine">Endocrine: <b>\${exam.endocrine || ''}</b></div>
                        <div class="text-container pr-1" exam-type="gi">Gi: <b>\${exam.gi || ''}</b></div>
                        <div class="text-container pr-1" exam-type="womenHealth">WomenHealth: <b>\${exam.womenHealth || ''}</b></div>
                        <div class="text-container pr-1" exam-type="cns">Cns: <b>\${exam.cns || ''}</b></div>
                        <div class="text-container pr-1" exam-type="growth">Growth: <b>\${exam.growth || ''}</b></div>
                        <div class="text-container pr-1" exam-type="uroGenital">UroGenital: <b>\${exam.uroGenital || ''}</b></div>
                        <div class="text-container pr-1" exam-type="radiology">Radiology: <b>\${exam.radiology || ''}</b></div>
                        <div class="text-container pr-1" exam-type="ecg">ECG: <b>\${exam.ecg || ''}</b></div>
                        <div class="text-container pr-1" exam-type="ultrasound">Ultrasound: <b>\${exam.ultrasound || ''}</b></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="#" class="btn btn-sm btn-dark bg-dark right edit-button" 
                        data-toggle="modal" 
                        data-target="#physicalExamEditModal" 
                        data-id="\${exam.groupingObsId}">Edit</a>
                    </div>
                    </div>
                </div>`;
            });
            document.getElementById('examsContainer').innerHTML = examHtml;
        } else {
        document.getElementById('examsContainer').innerHTML = '<p>No physical exams available.</p>';
        }
    }
</script>
<style>
    .text-row {
        display: flex;
        flex-wrap: wrap;
        gap: 1rem;
    }
    .text-container {
        padding: 0.5rem;
        background-color: #f8f9fa;
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        border-radius: 0.25rem;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: break-spaces;
        word-wrap: revert;
        box-sizing: border-box;
    }
</style>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Physical exam</h5>
        </div>
    </div>
    <% if (additionalExams) { %>
    <% additionalExams.each { exam -> %>
        <div class="row" id="examsContainer">
        </div>
    <% } %>
    <% } else { %>
    <div class="row no-data-section text-center">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
    <% } %>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <button class="dashed-button rounded"
            id="btnPhysicalExam" data-toggle="modal" data-target="#physicalExamModal"
            <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Physical Exam</button>
</div>
<div class="col col-sm-12 col-md-12 col-lg-12">
    <hr/>
</div>

<div class="modal fade" id="physicalExamModal" tabindex="-1"
     data-controls-modal="physicalExamModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="physicalExamModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="physicalExamModalTitle">Add Physical Exam</h4>
                <button type="button" id="beginPatientConsultation" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="addPhysicalExaminationForm">
                    <div class="form-group">
                        <p>Please fill in the details below to capture the patient's physical exam</p>
                    </div>
                    <div class="form-group">
                        <label for="physical-exam">General Exam</label>
                        <textarea class="form-control" rows="5"  id="physical-exam" name="physicalExam"></textarea>
                    </div>
                    <span><h6>Anatomy examination</h6></span>
                    <div class="form-group">
                        <label for="heent">HEENT</label>
                        <input class="form-control" type="text"  id="heent" name="heent"/>
                    </div>
                    <div class="form-group">
                        <label for="respiratory">Respiratory</label>
                        <input class="form-control" type="text"  id="respiratory" name="respiratory"/>
                    </div>
                    <div class="form-group">
                        <label for="cardiovascular">Cardiovascular</label>
                        <input class="form-control" type="text"  id="cardiovascular" name="cardiovascular"/>
                    </div>
                    <div class="form-group">
                        <label for="skin">Skin</label>
                        <input class="form-control" type="text"  id="skin" name="skin"/>
                    </div>
                    <div class="form-group">
                        <label for="endocrine">Endocrine</label>
                        <input class="form-control" type="text"  id="endocrine" name="endocrine"/>
                    </div>
                    <div class="form-group">
                        <label for="gi">GI</label>
                        <input class="form-control" type="text"  id="gi" name="gi"/>
                    </div>
                    <div class="form-group">
                        <label for="womenHealth">GU/ Women's Health</label>
                        <input class="form-control" type="text"  id="womenHealth" name="womenHealth"/>
                    </div>
                    <div class="form-group">
                        <label for="cns">CNS/PNS</label>
                        <input class="form-control" type="text"  id="cns" name="cns"/>
                    </div>
                    <div class="form-group">
                        <label for="growth">Growth and Development</label>
                        <input class="form-control" type="text"  id="growth" name="growth"/>
                    </div>
                    <div class="form-group">
                        <label for="uroGenital">Uro-genital</label>
                        <input class="form-control" type="text"  id="uroGenital" name="uroGenital"/>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1" id="btnAddPhysicalExam">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  jQuery(function () {
    jQuery("#physicalExamModal").appendTo("body");
  });
</script>
<% if(additionalExams) {%>
    <div class="modal fade" id="physicalExamEditModal" tabindex="-1"
         data-controls-modal="physicalExamEditModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="physicalExamEditModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="physicalExamEditModalTitle">Edit Physical Exam</h4>
                    <button type="button" id="editPhysicalExamBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="editPhysicalExaminationForm">
                        <input type="hidden" name="lastPhysicalObsId" id="lastPhysicalObsId" />
                        <div class="form-group">
                            <label for="editPhysicalExam">General exam</label>
                            <textarea class="form-control" rows="5"  id="editPhysicalExam" name="editPhysicalExam"></textarea>
                        </div>
                        <span><h6>Anatomy examination</h6></span>
                        <div class="form-group">
                            <label for="editHeent">HEENT</label>
                            <input class="form-control" type="text"  id="editHeent" name="editHeent"/>
                        </div>
                        <div class="form-group">
                            <label for="editRespiratory">Respiratory</label>
                            <input class="form-control" type="text"  id="editRespiratory" name="editRespiratory"/>
                        </div>
                        <div class="form-group">
                            <label for="editCardiovascular">Cardiovascular</label>
                            <input class="form-control" type="text"  id="editCardiovascular" name="editCardiovascular"/>
                        </div>
                        <div class="form-group">
                            <label for="editSkin">Skin</label>
                            <input class="form-control" type="text"  id="editSkin" name="editSkin"/>
                        </div>
                        <div class="form-group">
                            <label for="editEndocrine">Endocrine</label>
                            <input class="form-control" type="text"  id="editEndocrine" name="editEndocrine"/>
                        </div>
                        <div class="form-group">
                            <label for="editGi">GI</label>
                            <input class="form-control" type="text"  id="editGi" name="editGi"/>
                        </div>
                        <div class="form-group">
                            <label for="editWomenHealth">GU/ Women's Health</label>
                            <input class="form-control" type="text"  id="editWomenHealth" name="editWomenHealth"/>
                        </div>
                        <div class="form-group">
                            <label for="editCns">CNS/PNS</label>
                            <input class="form-control" type="text"  id="editCns" name="editCns"/>
                        </div>
                        <div class="form-group">
                            <label for="editGrowth">Growth and Development</label>
                            <input class="form-control" type="text"  id="editGrowth" name="editGrowth"/>
                        </div>
                        <div class="form-group">
                            <label for="editUroGenital">Uro-genital</label>
                            <input class="form-control" type="text"  id="editUroGenital" name="editUroGenital"/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary float-right ml-1" id="savePhysicalExam">Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
<%}%>
<script>
  jQuery(function () {
    jQuery("#physicalExamEditModal").appendTo("body");
  });
</script>
