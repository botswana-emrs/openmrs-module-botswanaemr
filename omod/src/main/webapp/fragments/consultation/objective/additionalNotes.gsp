<script>
    let objAdditionalNotesParams = new URLSearchParams(window.location.search);
    let objVisitId = objAdditionalNotesParams.get('visitId');
    let objPatientId = objAdditionalNotesParams.get('patientId');
    
  jq(function() {
    fetchAdditionalNotes();

    jq("#additionalNotesForm").submit(function(e){
      var params = {
        'patientId': ${patient.id},
        'additionalNotes': jq('#additionalNotes').val(),
        'locationId': ${location.id},
          'visitId': ${activeVisit.id}
      };

      showLoadingOverlay();
      jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/additionalNotes", "addAdditionalNotes")}', params)
          .success(function (data) {
            jq().toastmessage('showNoticeToast', "Additional notes captured successfully");
            fetchAdditionalNotes();
            jQuery('#additionalNotesModal').modal('toggle');
            hideLoadingOverlay();
          })
    });

      jq(".edit-button").click(function (e) {
          let id = jq(this).attr('data-id');
          let val = jq("#" + id).html();

          jq("#editAdditionalNotes").val(val);
          jq("#lastAdditionalObsId").val(id);
      });

    jq("#editAdditionalNotesForm").submit(function(e){
      e.preventDefault();
        var params = {
            'editAdditionalNotes': jq('#editAdditionalNotes').val(),
            'lastAdditionalObsId': jq('#lastAdditionalObsId').val()
        };

      jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/additionalNotes", "editAdditionalNotes")}', params)
          .success(function (data) {
            showLoadingOverlay();
            jq().toastmessage('showNoticeToast', "Additional edited successfully");
            fetchAdditionalNotes();
            jq('#editAdditionalNotesModal').removeClass('show').css('display', 'none').attr('aria-hidden', 'true');
            jq('.modal-backdrop').remove();
            hideLoadingOverlay();
          });
    });
  });

    function fetchAdditionalNotes() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/objective/additionalNotes", "fetchAdditionalNotes")}', 
            { 'patientId': objPatientId, 'visitId': objVisitId }
        ).done(function(data) {
            if (Array.isArray(data)) {
                renderAdditionalNotes(data);
            } else {
                console.error('Unexpected response format');
            }
        }).fail(function(jqXHR) {
            console.error('Error loading additional notes data');
        });
    }

    function renderAdditionalNotes(notes) {
        const notesContainer = jQuery('.additional-notes-container');
        notesContainer.empty();

        notes.forEach(note => {
            const noteHtml = `
                <div class="row">
                    <div class="row col-md-12">
                        <div class="col col-md-8" id="\${note.obsId}">
                            \${note.value}
                        </div>
                        <div class="col-md-4">
                            <a href="#" class="btn btn-sm btn-dark bg-dark right edit-button" 
                            data-toggle="modal" data-target="#editAdditionalNotesModal" 
                            data-id="\${note.obsId}">Edit</a>
                        </div>
                    </div>
                    <div class="col col-md-12 text-sm-left text-black-50">
                        Added on: \${new Date(note.dateCreated).toLocaleString()}
                    </div>
                </div>
            `;
            notesContainer.append(noteHtml);
        });
    }

</script>
<div class="col col-sm-12 col-md-12 col-lg-9">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Additional notes</h5>
        </div>
    </div>
    <% if (additionalNotes) { %>
    <% additionalNotes.each { note -> %>
        <div class="row additional-notes-container"></div>
    <% } %>
    <% } else { %>
    <div class="row no-data-section text-center">
        <img class=""
             src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
    </div>
    <% } %>
</div>
<div class="col col-sm-12 col-md-12 col-lg-9">
    <button class="dashed-button rounded"
            id="btnAdditionalNotes" data-toggle="modal" data-target="#additionalNotesModal"
            <% if(!isConsultationActive) { %> disabled <% } %> >+ Add additional notes</button>
</div>

<div class="modal fade" id="additionalNotesModal" tabindex="-1"
     data-controls-modal="additionalNotesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="additionalNotesModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="additionalNotesModalTitle">Add Additional notes</h4>
                <button type="button" id="additionalNotesBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="additionalNotesForm">
                    <div class="form-group">
                        <label for="additionalNotes">Notes</label>
                        <textarea class="form-control" rows="5"  id="additionalNotes" name="additionalNotes"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  jQuery(function () {
    jQuery("#additionalNotesModal").appendTo("body");
  });
</script>
<% if(lastAdditionalNotes) {%>
    <div class="modal fade" id="editAdditionalNotesModal" tabindex="-1"
         data-controls-modal="editAdditionalNotesModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="editAdditionalNotesModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="editAdditionalNotesModalTitle">Edit Additional notes</h4>
                    <button type="button" id="editAdditionalNotesBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" id="editAdditionalNotesForm">
                        <input type="hidden" id="lastAdditionalObsId" name="lastAdditionalObsId" />
                        <div class="form-group">
                            <label for="editAdditionalNotes">Notes</label>
                            <textarea class="form-control" rows="5"  id="editAdditionalNotes" name="editAdditionalNotes"></textarea>
                        </div>
                        <div class="modal-footer pr-0">
                            <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<%}%>
<script>
  jQuery(function () {
    jQuery("#editAdditionalNotesModal").appendTo("body");
  });
</script>
