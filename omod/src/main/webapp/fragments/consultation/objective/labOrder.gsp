<script>
    let labOrderParams = new URLSearchParams(window.location.search);
    let labOrderPatientId = labOrderParams.get('patientId');
  jq(function() {
    getLabTestsForPatient();
    var requiredTests = [];

    jq("#labOrder").autocomplete({
      source: function(request, response) {
        jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTests") }', {
          query: request.term
        }).success(function(data) {
          var results = [];
          for (var i in data) {
            var result = {
              label: data[i].name,
              value: data[i].id
            };
            results.push(result);
          }
          response(results);
        });
      },
      minLength: 3,
      select: function(event, ui) {
        event.preventDefault();
        jq(this).val(ui.item.label);

        jq("#list").append('<li id="'+ui.item.value+'">' + ui.item.label + '<button type="button" onclick="deleteItem('+ui.item.value+')" class="delete btn btn-sm btn-outline border-0 bg-transparent"><i class="fa fa-times text-danger"></i></button></li>');
        requiredTests.push(ui.item.value);

        //verifyTests();
        jq('#labOrder').focus();
        jq('#labOrder').val('');
        jq('#task-tests').show();
      },
      open: function() {
        jq(this).removeClass("ui-corner-all").addClass("ui-corner-top");
      },
      close: function() {
        jq(this).removeClass("ui-corner-top").addClass("ui-corner-all");
      }
    });
    jq("#addLabOrderForm").submit(function(e){
      e.preventDefault();

      // Disable the save button
      jq('#addLabOrderBtn').prop('disabled', true);

      jq("#testList").val(requiredTests);
      var params = {
        'patientId': <%=patient.id %>,
        'labList': jq('#testList').val(),
        'locationId': ${location.id}
      };

      jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/objective/labOrder", "addLabOrders")}', params)
      .done(function (data) {
            jq().toastmessage('showNoticeToast', "Lab orders captured successfully");
            location.reload();
          })
          .fail(function() {
            console.log("error");
            // an error message to the user
            jq().toastmessage('showErrorToast', "An error occurred while capturing the Lab orders. Please try again.");

            // enable the save button
            jq('#addLabOrderBtn').prop('disabled', false);
      });
    });
  });

  function getLabTestsForPatient() {
    jQuery.getJSON('${ui.actionLink("botswanaemr", "consultation/objective/labOrder", "getLabTestsForPatient")}',
      { 'patientId': labOrderPatientId})
      .done(function(data) {
        renderLabTests(data);
      }).fail(function(jqXHR) {
        console.error('Error loading lab order data');
      });
  }

  function renderLabTests(data) {
    const labTestsContainer = jq("#labTestsContainer");
        labTestsContainer.empty();

        if (data && data.length > 0) {
            data.forEach(function(test) {
                const testHtml = `
                    <div class="row">
                        <div class="col zero-padding text-left">
                            <span class="p normal-text text-primary">\${test.testName}</span>
                        </div>

                        <div class="col zero-padding text-right">
                            <span class="p normal-text">Added on: \${test.dateAdded}</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col zero-padding text-left mb-2">
                            <span class="p normal-text">Results</span><br/>
                            \${test.status === 'PENDING' ? 
                                `<span id="flag" class="status bg-warning-dark"></span>
                                 <span id="status" class="text-warning text-capitalize" readonly
                                       style="border: none !important;  width: 85px !important;">\${test.status.toLowerCase()}</span>`
                                : 
                                `<span id="flag" class="status bg-success-dark"></span>
                                 <span id="status" class="bg-grey text-capitalize" readonly
                                       style="border: none !important;  width: 85px !important;">\${test.status.toLowerCase()}</span>`
                            }
                            \${test.status !== 'PENDING' && test.resultsList ? 
                                `<h5 class="mb-3 mt-3 pl-3">Test Results</h5>
                                 \${test.resultsList.map(function(result) {
                                     return `<span class="text-primary pl-3">\${result.key}: <span class="text-muted">\${result.value}</span></span><br />`;
                                 }).join('')}`
                            : ''}
                        </div>
                    </div>
                `;

                labTestsContainer.append(testHtml);
            });
        } else {
            labTestsContainer.append(`
                <div class="row no-data-section text-center">
                    <img class="" src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
                    <p class="text-center">No data captured. Add data by clicking "Add" below</p>
                </div>
            `);
        }
    }
  
  function deleteItem(itemId) {
     var itemToRemove = document.getElementById(itemId);
     itemToRemove.parentNode.removeChild(itemToRemove);
  }
</script>
<div class="col col-sm-12 col-md-12 col-lg-9">
    <div class="row">
        <div class="col-md-8">
            <h5 class="text-dark">Investigations</h5>
        </div>
        <div class="col-md-4">
        </div>
    </div>
    <div id="labTestsContainer" class="col-md-12"></div>
</div>
<div class="col col-sm-12 col-md-12 col-lg-9">
    <button type="button" class="dashed-button rounded">
        <a href="${ui.pageLink('botswanaemr', 'lab/addLaboratoryTestOrders', [patientId: patient.id])}" class="">
             + Add test
        </a>
    </button>
</div>
<div class="col col-sm-12 col-md-12 col-lg-9">
    <hr class="color-grey"/>
</div>
<div class="modal fade" id="addLabOrderModal" tabindex="-1"
     data-controls-modal="addLabOrderModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addLabOrderModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollab" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addLabOrderModalTitle">Add Lab Orders</h4>
                <button type="button" id="additionalNotesBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="addLabOrderForm">
                    <div class="form-group">
                        <label for="labOrder">Test</label>
                        <input type="text" class="form-control" id="labOrder" name="labOrder" placeholder="Select test" />
                    </div>
                    <div id="task-tests" class="tasks" style="display:none;">
                        <fieldset class="border p-2">
                            <legend  class="w-auto">Selected Lab Tests</legend>
                            <ul id="list" class="selected-lab-tests" name="labList">
                            </ul>
                            <br />
                        </fieldset>
                    </div>
                    <input type="hidden" id="testList" name="testTest" />
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1" id="addLabOrderBtn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  jQuery(function () {
    jQuery("#addLabOrderModal").appendTo("body");
  });
</script>
