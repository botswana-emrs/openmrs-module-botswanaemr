<script type="text/javascript">
    let conditionsData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#conditionsTemplate").hide();

/*        function setAutocomplete(element) {
            element.autocomplete({
                source: function (request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/editConditions", "searchConditions") }', {
                        searchTerm: request.term
                    }).success(function (data) {
                        var results = [];
                        for (var i in data) {
                            var result = {
                                label: data[i].name,
                                value: data[i].uuid
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay', ui.item.value);
                },
            });
        }*/

        function setAutocomplete(element){
            element.autocomplete({
                source: function(request, response) {
                    jq.getJSON('${ ui.actionLink("botswanaemr", "search", "searchIcd11") }', {
                        q: request.term
                    }).success(function(data) {
                        var results = [];
                        for (var i in data) {
                            var result = {
                                label: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text(),
                                value: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text()
                            };
                            results.push(result);
                        }
                        response(results);
                    });
                },
                minLength: 3,
                select: function(event, ui) {
                    event.preventDefault();
                    jq(element).val(ui.item.label);
                    jq(element).attr('data-overlay',ui.item.value);
                },
            });
        }

        jq('#editConditions').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        conditionsData.deleted.push(tr.attr("data-overlay"));
                    }
                    jq(e.target).closest('tr').remove();
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                        if (jq(this).attr("name") === "condition") {
                            setAutocomplete(jq(this));
                        }
                    });
                }
            }
        });

        jq("#btnAddCondition").on("click", function (e) {
            e.preventDefault();
            const len = jq('.newCondition').length + 1;
            let template = jq("#conditionTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
                // jq(this).attr("id", this.id + len).attr("name", this.name + len);
            });
            setAutocomplete(template.find("input[name='condition']"));

            jq('<tr/>', {
                'class': 'newCondition',
                html: template
            }).hide().appendTo('#conditionsBody').slideDown('slow');
        });

        function getFormData() {
            jq("#conditionsBody").find("tr:gt(0)").map(function () {
                let conditionInput = jq(this).find("input[name='condition']").prop("disabled", false);
                let condition = conditionInput.data("condition-name") || conditionInput.val();

                let conditionComment = jq(this).find("input[name='comment']");
                let comment = conditionComment.val();

                let existingUuid = jq(this).attr("data-overlay");
                if (existingUuid && condition) {
                    conditionsData.edited.push({
                        "uuid": existingUuid,
                        "condition": condition,
                        "comment": comment
                    });
                } else {
                    if (condition) {
                        conditionsData.new.push({
                            "condition": condition,
                            "comment": comment
                        });
                    }
                }
                conditionInput.prop("disabled", true);
            });
        }

        jq("#editConditions").submit(function (e) {
            e.preventDefault();

            // Disable the save button
            jq('#saveConditions').prop('disabled', true);

            showLoadingOverlay();
            getFormData();

            let payload = JSON.stringify(conditionsData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editConditions", "updateConditions") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload},
                function (data) {

                }, 'json')
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The patients comorbidities have been updated");

                     //Async update conditions
                    getConditions(patientUuid);

                    jq('#editConditionsModal').removeClass('show').css('display', 'none').attr('aria-hidden', 'true');
                    jq('.modal-backdrop').remove();

                    hideLoadingOverlay();
                })
                .fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#saveConditions').prop('disabled', false);
                });
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editConditions">
            <div class="row">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Comorbidity</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="conditionsBody">
                    <tr id="conditionTemplate" style="display: none">
                        <td>
                            <input name="condition" type="text" class="form-control" disabled
                                   placeholder="Start typing for hints... "/>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% conditions.each { condition -> %>
                    <tr data-overlay="${condition.uuid}">
                        <td>
                            <% if (condition.condition?.coded != null) { %>
                            <input name="condition" type="text" class="form-control"
                                data-overlay="${condition.condition?.coded?.uuid}"
                                data-condition-name="${ui.encodeHtmlContent(ui.format(condition.condition?.coded))}"
                                placeholder="Start typing for hints..."
                                value="${condition.condition?.coded?.uuid}"
                                disabled />
                            <% } else { %>
                            <input name="condition" type="hidden" class="form-control" 
                                value="${ui.encodeHtmlContent(ui.format(condition.condition?.nonCoded))}" />
                            <span>${ui.encodeHtmlContent(ui.format(condition.condition?.nonCoded))}</span>
                            <% } %>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (condition.additionalDetail) { %>
                                   value="${condition.additionalDetail}" <% } %>/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddCondition">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="saveConditions">Save</button>
            </div>
        </form>
    </div>
</div>
