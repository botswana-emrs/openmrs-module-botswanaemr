<script>
  jQuery(function () {
    jQuery("#endConsultationModal").appendTo("body");

  jq('#endConsultationForm').submit(function (e) {
      e.preventDefault();
      let params = {
          patientId: jq(this).find("input[name=patientId]").val(),
          patientQueueId: jq(this).find("input[name=patientQueueId]").val(),
          visitId: jq(this).find("input[name=visitId]").val(),
          referralId: jq(this).find("input[name=referralId]").val()
      }

      jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/endConsultation", "closeConsultation")}', params)
          .success(function (data) {
              jq().toastmessage('showNoticeToast', "Consultation has been ended");
              jQuery("#endConsultationModal").modal('hide');
              location.href = '${ui.pageLink("botswanaemr", "consultation/doctorsPatientPoolDashboard")}';
          })
          .fail(function(err) {
              console.log(err);
          })
  });
  });
</script>
<div class="modal fade" id="endConsultationModal" tabindex="-1"
     data-controls-modal="endConsultationModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="endConsultationModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="endConsultationModalTitle">End Consultation</h4>
                <button type="button" id="endPatientConsultation" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="endConsultationForm">

                    <br />
                    Ending Consultation  for <b>${name}</b>
                    <hr />
                    <div>
                        <input type="hidden" name="patientId" value="${patient.id}"/>
                        <input type="hidden" name="patientQueueId" value="${patientQueueId}"/>
                        <input type="hidden" name="visitId" value="${activeVisit.id}"/>
                        <input type="hidden" name="referralId" value="${referralId}"/>
                        <p style="display: inline">
                            <button id="endConsultations" type="submit"  class=" btn btn-primary right" value="end" name="complete">${ui.message("Complete")}
                            </button>

                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
