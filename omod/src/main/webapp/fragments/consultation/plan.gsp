<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12 pt-0">
        <div class="card bg-light">
            ${ui.includeFragment("botswanaemr", "consultation/objective/labOrder", [patient: patient?.patient?.uuid])}
        </div>
        <div class="card bg-light">
            ${ ui.includeFragment("botswanaemr", "consultation/plan/prescription/prescriptionList", [ patientId: patient.patient.uuid, visit: activeVisit]) }
        </div>
        <div class="card bg-light">
            ${ ui.includeFragment("botswanaemr", "consultation/plan/dietaryPlan", [ patientId: patient.patient.id, visit: activeVisit]) }
        </div>
        <div class="card bg-light">
            ${ ui.includeFragment("botswanaemr", "consultation/plan/nonPharmaPrescription", [ patientId: patient.patient.id, visit: activeVisit]) }
        </div>
        <div class="card bg-light">
            ${ui.includeFragment("botswanaemr", "referrals", [patientId: patient.patient.id, visit: activeVisit])}
        </div>
        <div class="card bg-light">
            ${ ui.includeFragment("botswanaemr", "scheduleFollowUpConsultation", [ patientId: patient.patient.uuid, visit: activeVisit]) }
        </div>
    </div>
</div>
