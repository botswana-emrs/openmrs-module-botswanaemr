<div class="row pt-0">
        <% if (visit == null) {%>
        <div class="col col-sm-12 col-md-12 col-lg-9">
            <div class="alert alert-warning text-danger" role="alert">
                <strong>
                    <i class="fa fa-warning"></i>
                    No active visit found. Please start a patient visit to proceed!
                    ${ ui.includeFragment("botswanaemr", "visit/patientVisit") }
                </strong>
            </div>
        </div>
        <%} else {%>
        <div class="col col-sm-12 col-md-12 col-lg-12 pt-0">
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "patientComplaint", [ patientId: patient.patient.uuid, visit: visit ]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "symptoms", [ patientId: patient.patient.uuid, visit: visit ]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "conditions", [ patientId: patient.patient.uuid ]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "allergies", [ patientId: patient.patient.uuid ]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "pastOperations", [ patientId: patient.patient.uuid, visit: visit ]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "lifeStyle", [ patientId: patient.patient.uuid, visit: visit]) }
            </div>
            <div class="card mb-3">
                ${ ui.includeFragment("botswanaemr", "diet", [ patientId: patient.patient.uuid, visit: visit]) }
            </div>
        </div>
        <%}%>
    </div>
</div>
