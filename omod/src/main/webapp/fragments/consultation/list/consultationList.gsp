<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#gender').find(":selected").val() !== '') {
                let gender = aData[2].trim();
                matchingOptions.push(gender === jq('#gender').find(":selected").text());
            }

           /*if (jq('#dateConsulted').val() !== '') {
                let consultationDate  = aData[5].trim();
                matchingOptions.push(consultationDate === jq('#dateConsulted').val());
            }*/

            if (jq('#status').find(":selected").val() !== '') {
                let status = aData[6].trim();
                matchingOptions.push(status === jq('#status').find(":selected").text());
            }
            
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function() {
        /*jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','consultation/list/consultationList','getPatientConsultationList')}",
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                let table = jQuery('#patientConsultationListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'patientGender'},
                        {'data': 'patientDateOfBirth'},
                        {'data': 'patientAge'},
                        {'data': 'consultationDate'},
                        {
                            'data': 'status',
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.status == "Active") {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Active</span>';
                                    } else {
                                        return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Expired</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': null,
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.status == "Active") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="profile">Profile</a>&nbsp;|&nbsp;' +
                                            '<a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="case">Case</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="profile">Profile</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });
        });*/

        const table = jQuery('#patientConsultationListTable').DataTable({
            dom: 'rtp',
            processing: true,
            serverSide: true,
            serverMethod: 'get',
            ajax: '${ui.actionLink('botswanaemr','consultation/list/consultationList','getPatientConsultationList')}',
            columns: [
                {'data': 'pin'},
                {'data': 'patientNames'},
                {'data': 'patientGender'},
                {'data': 'patientDateOfBirth'},
                {'data': 'patientAge'},
                {'data': 'consultationDate'},
                {
                    'data': 'status',
                    "render": function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row.status == "Active") {
                                return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Active</span>';
                            } else {
                                return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Expired</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    'data': null,
                    "render": function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row.status == "Active") {
                                return '<a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="profile">Profile</a>&nbsp;|&nbsp;' +
                                    '<a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="case">Case</a>';
                            } else {
                                return '<a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=' + row.patientId + '&visitId=' + row.visitId + '" class="text-primary pl-0" id="profile">Profile</a>';
                            }
                        }
                        return data;
                    }
                }
            ],
            /*columnDefs: [
                {
                    targets: -1,
                    data: null,
                    className: "text-left",
                    render: function(data, type, row, meta ) {
                        // console.log(data)
                     return '<a href="/${ui.contextPath()}/botswanaemr/stock/addProducts.page?itemId=' + data.id + '" class="color-primary"> View Item</a> | <br/>' +
                        '<a href="/${ui.contextPath()}/botswanaemr/stock/stockCard.page?itemId=' + data.id + '" class="color-primary"> Bin stock card </a>'
                    },
                },
            ],*/
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#inquiryBtn').on('click', () => {
            let startDateConsulted = '';
            try {
                startDateConsulted = dateUtils.formatDate(jq('#startDateConsulted').datepicker('getDate'),'yyyy-MM-dd')
            } catch(ex) {};
            let endDateConsulted = '';
            try {
                endDateConsulted = dateUtils.formatDate(jq('#endDateConsulted').datepicker('getDate'),'yyyy-MM-dd')
            } catch(ex) {};

            table.search(startDateConsulted + ' , ' + endDateConsulted).draw();
        });

        jq('#resetBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#consultationList'));
            table.draw();
        });

        jq('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 1000;
            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                   table.draw();
                }, delay);
            }
        });

        jq('.card-link').click(() => {
            if (jq('#genderDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });
    });

</script>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="row">
                <div class="col-md-3 col-lg-3 pl-0 pr-0 pb-3">
                    <label>Search</label>
                    <input id="searchPhrase"
                           type="text"
                           name="searchPhrase"
                           class="form-control"
                           value=""
                           placeholder="${ui.message('Search by Patient ID or Name')}"
                    />
                </div>

                <div class="col-md-3 col-lg-3 pr-0 pb-5">
                    <label>Status</label>
                    <select id="status" class="form-control" placeholder="${ui.message('Select status')}">
                        <option value="" disabled selected>Select status</option>
                        <option value="Active">Active</option>
                        <option value="Expired">Expired</option>
                    </select>
                </div>

                <div class="col-md-3 col-lg-3 pr-0 pb-2" id="dateDiv">
                    <label>Date consulted</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                    <input value=""
                           id="startDateConsulted"
                           type="text"
                           name="startDateConsulted"
                           class="form-control datepicker"
                           placeholder="${ui.message('Start date ')}"/>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <input value=""
                               id="endDateConsulted"
                               type="text"
                               name="endDateConsulted"
                               class="form-control datepicker"
                               placeholder="${ui.message('End date ')}"/>
                    </div>
                    <script type="text/javascript">
                        jq('#startDateConsulted,#endDateConsulted').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            showButtonPanel: true,
                            "setDate": new Date(),
                            dateFormat: "dd-mm-yy",
                            yearRange: "-150:+0",
                            maxDate: 0,
                            "autoclose": true,
                            onSelect: function (data) {
                                jq("#startDateConsulted,#endDateConsulted").trigger('change');
                            }
                        });
                    </script>
                </div>

                <div class="col-md-3 col-lg-3 mt-3 pr-0">
                    <label>&nbsp;</label>
                    <button id="inquiryBtn" type="submit" class="btn btn-primary mt-3">
                        ${ui.message("Search")}
                    </button>&nbsp;&nbsp;
                    <button id="resetBtn" type="reset" class="btn btn-dark bg-dark mt-3">
                        ${ui.message("Reset")}
                    </button>

                    <a id="collapseBtn" class="card-link btn btn-sm bg-white color-primary border-0 float-right mt-3"
                       data-target=".multi-collapse"
                       aria-expanded="false"
                       aria-controls="dateDiv genderDiv">
                       <span>${ui.message("Expand")}
                           <i id="collapse-icon" class="icon-chevron-down"></i>
                       </span>
                    </a>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-3 col-lg-3 pl-0 pr-0 pb-3 collapse multi-collapse" id="genderDiv">
                    <label>Sex</label>
                    <select id="gender" class="form-control" placeholder="${ui.message('Select Sex ')}">
                        <option value="" disabled selected>Select Sex</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>

            </div>

            <table id="patientConsultationListTable"
                   class="table table-striped table-sm table-bordered">
                <thead>
                <tr>
                    <th>Patient ID</th>
                    <th>Name</th>
                    <th>Sex</th>
                    <th>Date of birth</th>
                    <th>Age</th>
                    <th>Date consulted</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
