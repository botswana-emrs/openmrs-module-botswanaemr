<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script type="text/javascript">
    let operationsData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    function setAutocomplete(element){
        element.autocomplete({
            source: function(request, response) {
                jq.getJSON('${ ui.actionLink("botswanaemr", "consultation/editPastOperations", "searchPastOperations") }', {
                    searchTerm: request.term
                }).success(function(data) {
                    if (data.length === 0) {
                        jq(element).attr('data-overlay', '');
                    }
                    var results = [];
                    for (var i in data) {
                        var result = {
                            label: data[i].name,
                            value: data[i].uuid
                        };
                        results.push(result);
                    }
                    response(results);
                });
            },
            minLength: 3,
            select: function(event, ui) {
                event.preventDefault();
                jq(element).val(ui.item.label);
                jq(element).attr('data-overlay',ui.item.value);
            },
        });
    }

    jq(function () {
        jq("#pastOperationTemplate").hide();

        let curYear = new Date().getFullYear();

        jq("input[name=year]").attr({"max" : curYear});


        jq('#editPastOperations').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let comment = tr.find(("input[name='comment']"));
                        let year = tr.find(("input[name='year']"));

                        let data = {
                            "pastOperation": tr.attr("data-overlay"),
                            "comment": comment.attr("data-overlay"),
                            "year": year.attr("data-overlay")
                        }
                        operationsData.deleted.push(data);
                    }

                    jq(e.target).closest('tr').remove();
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                        if (jq(this).attr("name") === "pastOperation"){
                            setAutocomplete(jq(this));
                        }
                    });
                }
            }
        });

        jq("#btnAddPastOperation").on("click", function (e) {
            e.preventDefault();
            let template = jq("#pastOperationTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
                jq(this).attr("name", this.name);
                if (this.name === 'year') {
                    jq(this).attr({"max" : curYear});
                }
            });
            setAutocomplete(template.find("input[name='pastOperation']"));

            jq('<tr/>', {
                'class': 'newPastOperation',
                'id': 'newPastOperation',
                html: template
            }).hide().appendTo('#pastOperationsBody').slideDown('slow');
        });

        function getFormData() {
            jq("#pastOperationsBody").find("tr:gt(0)").map(function (row) {
                let pastOperationInput = jq(this).find(("input[name='pastOperation']"));
                let pastOperation = pastOperationInput.attr('data-overlay');
                let pastOperationVal = pastOperationInput.val();

                let pastOperationComment = jq(this).find(("input[name='comment']"));
                let comment = pastOperationComment.val();
                let yearInput = jq(this).find(("input[name='year']"));
                let year = yearInput.val();

                let existingUuid = jq(this).attr("data-overlay");
                let isDisabled = pastOperationInput.prop('disabled');
                if (existingUuid) {
                    if (!isDisabled) {
                        let commentUuid = pastOperationComment.attr("data-overlay");
                        let yearUuid = yearInput.attr("data-overlay");
                        operationsData.edited.push({
                            "pastOperationUuid": existingUuid,
                            "pastOperation": pastOperation || pastOperationVal,
                            "commentUuid": commentUuid,
                            "comment": comment,
                            "yearUuid": yearUuid,
                            "year": year
                        });
                    }
                } else {
                    operationsData.new.push({
                        "pastOperation": pastOperation || pastOperationVal,
                        "comment": comment,
                        "year": year
                    })
                }
            });

        }

        jq("#editPastOperations").submit(function (e) {
            e.preventDefault();

            // Disable the save button
            jq('#savePastOperations').prop('disabled', true);

            showLoadingOverlay();

            getFormData();
            let payload = JSON.stringify(operationsData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editPastOperations", "updatePastOperations") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload, visitId: ${activeVisit.id}},
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patients pastOperations have been updated");

                    //asynchronously load operations data
                    loadOperations();

                    // Hide modal
                    jQuery('#editPastOperationsModal').modal('toggle');
                    
                    showLoadingOverlay();
                }, 'json')
                .fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#savePastOperations').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editPastOperations">
            <div class="row" ng-app="triageDataApp" ng-controller="TriageDataController"
                 ng-init="getOperationsData('${visit.uuid}')">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Past Operations</th>
                    <th scope="col">Year</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="pastOperationsBody">
                    <tr id="pastOperationTemplate" style="display: none">
                        <td>
                            <input name="pastOperation" type="text" class="form-control" disabled
                                   placeholder="Start typing for hints... "/>
                        </td>
                        <td>
                            <input name="year" type="number" max="getCurYear()"/>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% pastOperationObjects.each { pastOperationObject -> %>
                    <tr data-overlay="${pastOperationObject.pastOperation.uuid}">
                        <td>
                            <input name="pastOperation" type="text" class="form-control"
                                   placeholder="Start typing for hints... "
                                   data-overlay="${pastOperationObject.pastOperation?.valueCoded?.uuid}"
                                <% if (pastOperationObject.pastOperation.valueCoded && pastOperationObject.pastOperation.valueCoded.name.name != "Other") { %>
                                   value="${ui.encodeHtmlContent(ui.format(pastOperationObject.pastOperation.valueCoded))}"
                                <% } else { %>
                                   value="${ui.encodeHtmlContent(ui.format(pastOperationObject.pastOperation.valueText))}"
                                <% } %>
                                   disabled/>
                        </td>
                        <td>
                            <input type="number" name="year"  disabled
                                <% if (pastOperationObject.year && pastOperationObject.year.valueNumeric) { %>
                                   value="${pastOperationObject?.year?.valueNumeric}" <% } %>
                                   data-overlay="${pastOperationObject?.year?.uuid}"/>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (pastOperationObject.comment && pastOperationObject.comment.valueText) { %>
                                   value="${pastOperationObject?.comment?.valueText}" <% } %>
                                   data-overlay="${pastOperationObject?.comment?.uuid}"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddPastOperation">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="savePastOperations">Save</button>
            </div>
        </form>
    </div>
</div>
