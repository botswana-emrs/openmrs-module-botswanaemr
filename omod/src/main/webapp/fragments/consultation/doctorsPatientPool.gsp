<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#screenedBy').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                matchingOptions.push(screenedBy === jq('#screenedBy').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    function showPresumptiveModal(patientId, visitNumber, patientQueueId) {
        let currentServicePoint = `${currentServicePoint}`;
        jQuery('#presumptiveModal').modal('show');
        jq('#continueConsultation').off('click').on('click', function() {
            window.location.href = '/${ui.contextPath()}/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId=' + patientId + '&visitId=' + visitNumber + '&patientQueueId=' + patientQueueId;
        });
        jq('#goToTbManagement').off('click').on('click', function() {
            let url = "/${ui.contextPath()}/botswanaemr/programs/programs.page?patientId=" + patientId + "&currentServicePoint" + currentServicePoint + "&programId=938b5435-dc0e-4d4e-bb6f-79befd636d8f";
            window.location.href = url;
        });
    }
    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            dataType: "json",
            global: false,
            async: true,
            success: function (data) {
                console.log(data);
                let table = jQuery('#patientQueueListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'dateTimeRegistered'},
                        {'data': 'durationOfWait'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {
                            'data': 'priorityComment',
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.priorityComment == "Normal") {
                                        return '<p class="text-success">Normal</p>';
                                    } else if (row.priorityComment == "Severe") {
                                        return '<p class="text-danger">Severe</p>';
                                    } else {
                                        return '<p class="text-warning">' + row.priorityComment + '</p>';
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': 'status',
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.status == "TREATED") {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Treated</span>';
                                    } else if (row.status == "PICKED") {
                                        return '<i class="fa fa-circle text-info"></i><span class="text-muted px-0" style="background: none;"> Treatment started</span>';
                                    } else {
                                        return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': 'presumptive',
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.hasTbScreeningEncounter == false) {
                                        return '<p class="text-center text-warning">Not screened</p>';
                                    }
                                    if (row.presumptive == true) {
                                        return '<p class="text-center text-danger">Yes</p>';
                                    } else {
                                        return '<p class="text-center text-success">No</p>';
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': null,
                            "render": function (data, type, row, meta) {
                                if (type === 'display') {
                                    if (row.status == "TREATED") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary float-left pl-3" id="view">View</a>';
                                    } else if (row.status == "PICKED") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary pl-0" id="screen">Resume treating</a>&nbsp;';
                                    } else {
                                        if (row.presumptive === true) {
                                            return '<a href="#" class="text-primary float-left pl-3" onclick="showPresumptiveModal(' + row.patientId + ',' + row.visitNumber + ',' + row.patientQueueId + ' )">Treat</a>&nbsp;|&nbsp;' +
                                                '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary pl-0" id="reassign">Reassign</a>';
                                        } else {
                                            return '<a href="/${ui.contextPath()}/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary pl-0" id="screen">Treat</a>&nbsp;|&nbsp;' +
                                                '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId=' + row.patientId + '&visitId=' + row.visitNumber + '&patientQueueId=' + row.patientQueueId + '" class="text-primary pl-0" id="reassign">Reassign</a>';
                                        }
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#doctorsPatientPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<style>
    .ui-dialog-titlebar {
        background: #0099CC !important;
        color: #ffffff !important;
        fill: #0099CC !important;
    }
    .ui-dialog-title {
        background-color: transparent !important;
        color: #fff !important;
    }
</style>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Screened by</label>
        <select id="screenedBy"
                class="form-control"
                placeholder="${ui.message('Select nurse ')}">
            <option value="">Select One</option>
            <% providerList.each { provider-> %>
            <% if (provider != null && provider?.person != null && provider?.person?.personName != null) { %>
            <option value="<%=provider.uuid %>"><%=provider.person.personName %></option>
            <% } %>
            <% } %>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="patientQueueListTable"
       class="table table-striped table-md table-bordered">
    <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Time registered</th>
            <th>Duration of Wait</th>
            <th>Sex</th>
            <th>Age</th>
            <th>Severity</th>
            <th>Status</th>
            <th>TB Presumptive</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>

<!-- Modal -->
<div class="modal fade" id="presumptiveModal" tabindex="-1" aria-labelledby="presumptiveModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="presumptiveModalLabel">Presumptive TB Management</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                This patient is TB presumptive. Do you want to go to TB Management?
            </div>
            <div class="modal-footer">
                <button type="button" style="background: #6c757d; border: 1px solid #6c757d;" class="btn btn-secondary" id="continueConsultation">Continue consultation</button>
                <button type="button" class="btn btn-primary" id="goToTbManagement">Go to TB Management</button>
            </div>
        </div>
    </div>
</div>

