<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script type="text/javascript">
    let lifestylesData = {
        "deleted": [],
        "edited": [],
        "new": []
    }

    jq(function () {
        jq("#lifestyleTemplate").hide();

        jq('#editLifestyles').on("click", function (e) {
            const element = e.target.nodeName;

            if (element.toLowerCase() === "a") {
                if (jq(e.target).hasClass("remove")) {
                    let tr = jq(e.target).closest('tr');
                    if (tr.attr("data-overlay")) {
                        let comment = tr.find(("input[name='comment']"));

                        let data = {
                            "lifestyle": tr.attr("data-overlay"),
                            "comment": comment.attr("data-overlay")
                        }
                        lifestylesData.deleted.push(data);
                    }

                    jq(e.target).closest('tr').remove();
                } else if (jq(e.target).hasClass("edit")) {
                    jq(e.target).closest('tr').find('input, select').map(function () {
                        jq(this).prop("disabled", false);
                    });
                }
            }
        });

        jq("#btnAddLifestyle").on("click", function (e) {
            e.preventDefault();
            let template = jq("#lifestyleTemplate").children().clone(true, true);
            template.find('input').val('');
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });

            jq('<tr/>', {
                'class': 'newLifestyle',
                'id': 'newLifestyle',
                html: template
            }).hide().appendTo('#lifestylesBody').slideDown('slow');
        });

        function getFormData() {
            jq("#lifestylesBody").find("tr:gt(0)").map(function (row) {
                let lifestyleInput = jq(this).find(("input[name='lifestyle']"));
                let lifestyle = lifestyleInput.val();
                let lifestyleComment = jq(this).find(("input[name='comment']"));
                let comment = lifestyleComment.val();

                let existingUuid = jq(this).attr("data-overlay");
                let isDisabled = lifestyleInput.prop('disabled');
                if (existingUuid) {
                    if (!isDisabled) {
                        let commentUuid = lifestyleComment.attr("data-overlay");
                        lifestylesData.edited.push({
                            "uuid": existingUuid,
                            "lifestyle": lifestyle,
                            "commentUuid": commentUuid,
                            "comment": comment
                        });
                    }
                } else {
                    lifestylesData.new.push({
                        "lifestyle": lifestyle,
                        "comment": comment
                    })
                }
            });

        }

        jq("#editLifestyles").submit(function (e) {
            e.preventDefault();

             // Disable the save button
            jq('#saveLifeStyle').prop('disabled', true);

            showLoadingOverlay();
            getFormData();
            let payload = JSON.stringify(lifestylesData);

            jq.post('${ ui.actionLink("botswanaemr", "consultation/editLifeStyle", "updateLifestyles") }',
                {returnFormat: 'json', patientId: ${activePatient.patientId}, data: payload, visitId: ${activeVisit.id}},
                function (data) {
                }, 'json') .success(function (data) {

                    jq().toastmessage('showNoticeToast', "The patients lifestyles have been updated");
                    getLifeStyleData();
                    jQuery('#editLifestyleModal').modal('toggle');
                    hideLoadingOverlay();
                })
                .fail(function() {
                    console.log("error");
                    // enable the save button
                    jq('#saveLifeStyle').prop('disabled', false);
                })
        });
    });
</script>

<div class="row">
    <div class="col-12">
        <form id="editLifestyles">
            <div class="row">
                <table class="table multi table-borderless">
                    <thead>
                    <th scope="col">Lifestyle</th>
                    <th scope="col">Notes</th>
                    <th scope="col">Operations</th>
                    </thead>
                    <tbody id="lifestylesBody">
                    <tr id="lifestyleTemplate" style="display: none">
                        <td>
                            <input name="lifestyle" type="text"/>
                        </td>
                        <td>
                            <input name="comment" type="text"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <% lifestyleObjects.each { lifestyleObject -> %>
                    <tr data-overlay="${lifestyleObject.lifestyle.uuid}">
                        <td>

                            <input name="lifestyle" type="text" value="${lifestyleObject.lifestyle.valueText}"
                                   disabled/>
                        </td>
                        <td>
                            <input type="text" name="comment" disabled
                                <% if (lifestyleObject.comment && lifestyleObject.comment.valueText) { %>
                                   value="${lifestyleObject?.comment?.valueText}" <% } %>
                                   data-overlay="${lifestyleObject?.comment?.uuid}"/>
                        </td>
                        <td>
                            <div class="row text-right" style="display: inline-flex">
                                <div class="edit title-margin-right">
                                    <a class="btn btn-sm btn-light right edit">Edit</a>
                                </div>

                                <div class="title-margin-left">
                                    <a class="btn btn-sm btn-light right remove">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <% } %>

                    </tbody>
                </table>
                <button class="dashed-button rounded add-more"
                        id="btnAddLifestyle">+ Add
                </button>
            </div>

            <div class="row button-section-right">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary float-right ml-1" id="saveLifeStyle">Save</button>
            </div>
        </form>
    </div>
</div>
