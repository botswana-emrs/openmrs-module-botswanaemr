<%
    ui.includeJavascript("botswanaemr", "calendar/calendar-main.js")
    ui.includeJavascript("botswanaemr", "calendar/drug-tracking-calendar.js")
    ui.includeCss("botswanaemr", "calendar/calendar-main.css")
%>
<style>
#calendar {
    max-width: 1400px;
    margin: 0 auto;
}
</style>
<script type="text/javascript">

    const initCalendar = () => {
        let calendarEvents = [];
        <% events.each { event -> %>
        calendarEvents.push({
            title: '${event.title}',
            start: '${event.start}'
        });
        <% } %>

        const calendarElement = document.getElementById('calendar');

        const calendarOptions = {
            initialDate: '${today}',
            editable: false,
            selectable: true,
            businessHours: true,
            dayMaxEvents: true,
            events: calendarEvents
        }
        const calendar = new FullCalendar.Calendar(calendarElement, calendarOptions);
        calendar.render();
    }

    document.addEventListener('DOMContentLoaded', () => {
        initCalendar();
    });

    jq("#calender-div").load(() => {
        initCalendar();
    })

</script>


<div id="calendar" style=" width:100%;"></div>
