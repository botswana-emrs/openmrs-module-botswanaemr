<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#status').find(":selected").val() !== '') {
                let status = aData[4].trim();
                matchingOptions.push(status === jq('#status').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink("botswanaemr","search","getVmmcData")}",
            dataType: "json",
            global: false,
            async: true,
            success: function (data) {
                console.log(data);
                var table = jQuery('#vmmcRecentAdmissionsTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'name'},
                        {'data': 'operationDate'},
                        {'data': 'age'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "Completed"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Completed</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "Completed"){
                                        return '<a href="/${ui.contextPath()}/botswanaemr/vmmc/vmmcClinicalView.page?patientId='+row.patientId+'&visitId='+row.visitId+'" class="text-primary float-left pl-3" id="view">View</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/vmmc/vmmcClinicalView.page?patientId='+row.patientId+'&visitId='+row.visitId+'" class="text-primary float-left pl-3" id="view">Treat</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#vmmcRecentAdmissions'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Status</label>
        <select id="status"
                class="form-control"
                placeholder="${ui.message('Select status ')}">
            <option value="">Select One</option>
            <option value="Completed">Completed</option>
            <option value="Awaiting">Awaiting</option>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
                ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
                ${ui.message("Reset")}
        </button>
    </div>
</div>
<table id="vmmcRecentAdmissionsTable"
       class="table table-striped table-sm table-bordered">
    <thead>
        <tr>
            <th>Patient Id</th>
            <th>Patient Name</th>
            <th>Date of Operation</th>
            <th>Age</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>

