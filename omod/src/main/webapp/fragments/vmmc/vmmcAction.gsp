<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'vmmc/vmmcDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-laptop-medical fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
         <a href="${ui.pageLink('botswanaemr', 'vmmc/vmmcPatientPoolDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                 <i class="fa fa-bed fa-1x"></i>
            </div>
            <div class="nav_label">Patient Pool</div>
         </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ["returnUrl": patientPoolReturnUrl, 'action': 'self'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Patient Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'vmmc/vmmcPatientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/myAppointments')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar-check fa-1x"></i>
            </div>
            <div class="nav_label">Appointments</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/consultationsList')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fas fa-user-tie fa-1x"></i>
            </div>
            <div class="nav_label">Consultations</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'vmmc/vmmcReports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>
