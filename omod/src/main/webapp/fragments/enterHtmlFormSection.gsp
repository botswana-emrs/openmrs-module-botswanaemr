<%
    ui.includeCss("botswanaemr", "htmlFormEntry.css")

    ui.includeJavascript("uicommons", "handlebars/handlebars.min.js", Integer.MAX_VALUE - 1);
    ui.includeJavascript("htmlformentryui", "dwr-util.js")
    ui.includeJavascript("botswanaemr", "htmlForm.js")
    ui.includeJavascript("uicommons", "emr.js")
    ui.includeJavascript("uicommons", "moment.js")
%>

<script type="text/javascript" src="/${ contextPath }/moduleResources/botswanaemr/htmlFormEntry.js"></script>
<link href="/${ contextPath }/moduleResources/botswanaemr/htmlFormEntry.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
    function submitHtmlForm() {
        htmlForm.submitHtmlForm();
        return false;
    }

    function showDiv(id) {
        htmlForm.showDiv(id);
    }

    function hideDiv(id) {
        htmlForm.hideDiv(id);
    }

    function getValueIfLegal(idAndProperty) {
        htmlForm.getValueIfLegal(idAndProperty);
    }

    function loginThenSubmitHtmlForm() {
        htmlForm.loginThenSubmitHtmlForm();
    }

    var beforeSubmit = htmlForm.getBeforeSubmit();
    var beforeValidation = htmlForm.getBeforeValidation();
    var propertyAccessorInfo = htmlForm.getPropertyAccessorInfo();

    <% if (returnUrl) { %>
        htmlForm.setReturnUrl('${returnUrl}');
    <% } %>

	jq(document).ready(function() {
		jQuery.each(jq("htmlform").find('input'), function(){
		    jq(this).bind('keypress', function(e){
		       if (e.keyCode == 13) {
		       		if (!jq(this).hasClass("submitButton")) {
		       			e.preventDefault();
		       		}
		       }
		    });
		});
		htmlForm.initialize();
    });
</script>

<div>
    <span class="error" style="display: none" id="general-form-error"></span>
    <form autocomplete="off" id="htmlform" method="post" action="${ ui.actionLink('htmlformentryui', 'htmlform/enterHtmlForm', 'submit')}" onSubmit="submitHtmlForm(); return false;">
        <input type="hidden" name="formUuid" value="${formUuid}"/>
        <% if (returnUrl) { %>
            <input type="hidden" name="returnUrl" value="${returnUrl}"/>
        <% } %>
        ${command.htmlToDisplay}
    </form>
</div>

<% if (command.fieldAccessorJavascript) { %>
    <script type="text/javascript">
        ${command.fieldAccessorJavascript}
    </script>
<% } %>
