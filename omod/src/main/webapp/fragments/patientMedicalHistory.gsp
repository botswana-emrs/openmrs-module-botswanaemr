<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#patientMedicalHistoryTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1"></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1"></i>'
                }
            }
        });
        
        jq('#medicalInquiryBtn').on('click', () => {

            jQuery('#patientMedicalHistoryTable').DataTable().draw();

            table.search(jq('#searchPhrase').val()).draw();

            if (jq('#medicationDate').val() != null) {
                table.columns(2).search(jq('#medicationDate').val()).draw();
            }

            if (jq('#medicalStatus').val() != null) {
                table.columns(3).search(jq('#medicalStatus').val()).draw();
            }
        });
        
        jq('.card-link-medical').click(() => {
            if (jq('#dateDiv').hasClass('show')) {
                let span = jq("<span/>")
                    .text("Expand")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-down");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').removeClass("show");
            } else {
                let span = jq("<span/>").text("Collapse")
                i = jq("<i>")
                    .attr("id", "collapse-icon")
                    .addClass("icon-chevron-up");
                jq('#collapseBtn').html(span.append(i));
                jq('.collapse').addClass("show");
            }
        });
        jq('#resetBtn').on('click', () => {
            fieldHelper.clearAllFields(jq('#pills-medical-history'));
            table.draw();
        });
    });
</script>

<div class="row mb-4">
    <div class="col-4 pl-0">
        <label for="searchPhrase" class="p-r-4">Search:</label>
        <input type="text" class="form-control border" id="searchPhrase"
               name="searchPhrase" placeholder="Search by case no">
    </div>
    <div class="col-4" id="statusDiv">
        <div class="form-horizontal form-group">
            <label for="medicalStatus">Status:</label>
            <select class="custom-select form-control" id="medicalStatus">
                <option selected value="">Select status:</option>
                <option value="Open">Open</option>
                <option value="Closed">Closed</option>
            </select>
        </div>
    </div>

    <div class="col-auto">
        <button type="submit" id="medicalInquiryBtn" class="btn btn-sm btn-primary mb-3">
            ${ui.message("Search")}
        </button>
    </div>
    <div class="col-auto">
        <button type="submit" id="resetBtn" class="btn btn-sm btn-dark bg-dark mb-3 border">
            ${ui.message("Reset")}
        </button>
    </div>
    <div class="col-auto">
        <a id="collapseBtn" class="card-link-medical btn btn-sm btn-warning bg-white color-primary border-0"
           data-target=".multi-collapse"
           aria-expanded="false"
           aria-controls="dateDiv"> ${ui.message("Expand")}
            <i id="collapse-icon" class="icon-chevron-down"></i>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-4 pl-0">
        <div class="form-horizontal form-group collapse multi-collapse" id="dateDiv">
            <label for="medicationDate">Date:</label>
            <input required
                   type="text"
                   class="form-control datepicker"
                   id="medicationDate"
                   name="medicationDate"
                   placeholder="Select date"
            />
            <script type="text/javascript">
                 jq('#medicationDate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    "setDate": new Date(),
                    dateFormat: "dd-M-yy",
                    "autoclose": true
                 });
            </script>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table id="patientMedicalHistoryTable" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Case No.</th>
            <th>Facility</th>
            <th>Description</th>
            <th>Date</th>
            <th>Status</th>
            <th>${ ui.message("coreapps.actions") }</th>
        </tr>
        </thead>
        <tbody>
        <% if (hasGetEncountersPrivilege) { %>
        <% encounters.each { %>
        <tr>
            <td>${it.caseId}</td>
            <td>${it.facility}</td>
            <td>${it.caseDescription != null ? it.caseDescription : ""}</td>
            <td>${it.encounterDate.format("dd-MMM-yyyy")}</td>
            <% if(it.caseStatus.toString().equals("CLOSED")){%>
            <td>
                <i class="fa fa-circle text-success"></i>
                <span class="text-muted px-0" style="background: none;"> Closed</span>
            </td>
            <% } else {%>
            <td>
                <i class="fa fa-circle text-warning"></i>
                <span class="text-muted px-0" style="background: none;"> Open</span>
            </td>
            <% } %>
            <td>
                <% if (!encounters.isEmpty() && it.visitId != null) { %>
                    <a  href="/${ui.contextPath()}/botswanaemr/printPastVisitSummary.page?patientId=${it.patientId}&visitId=${it.visitId}"
                        class="text-primary"> Print Visit Summary
                    </a>
                <% } %>
                |
                <a href="${ui.pageLink('botswanaemr', 'consultation/consultation', [patientId: patient.id, caseId: it.caseId, visitId: it.visitId])}"
                   class="text-primary">
                    View
                </a>
            </td>
        </tr>
        <% } %>
        <% } %>
        </tbody>
    </table>
</div>
