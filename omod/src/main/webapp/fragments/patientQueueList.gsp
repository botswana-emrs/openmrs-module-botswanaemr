<script type="text/javascript">
    var patientQueData = getPatientsFromQueue(null, '${currentServicePoint}');

    var jq=jQuery

    jq(document).ready(function () {
        updateTableResults();
    });

   function queuelistViewModel (){
       this.patientQueueList = ko.observableArray([]);
       this.patientQueueList(patientQueData)
    }
    var list = new queuelistViewModel();


    var updateTableResults = function () {
        patientQueData = ""
        patientQueData = getPatientsFromQueue(jq("#search-phrase").val().trim().toLowerCase(), jq("#queueRoom").val())
        list.patientQueueList(patientQueData)
    }
    
    function getPatientsFromQueue(searchPhase, queueRoom) {
        var toReturn;
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","patientQueueList","getPatientQueueList")}',
            dataType: "json",
            global: false,
            async: true,
            data: {
                searchPhase: searchPhase,
                queueRoom: queueRoom,
            },
            success: function (data) {
                //console.log(data);
                toReturn = data;
            }
        });
        
        return toReturn;
    }
    jq(function(){
        ko.applyBindings(list, jq("#patientQueueListTable")[0]);
    });

    function actionRedirect(patientId){
        window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/patientProfile.page?patient_id="+patientId;
    }
</script>

<div>
    <div class="form-group content-section">
        <label for="queueRoom">
            Select Queue
        </label>
        <select class="col-sm-5 form-control custom-select" id="queueRoom" name="queueRoom" onchange="updateTableResults()">
            <option disabled selected>--Please select--</option>
            <% if (locationList != null) {
                locationList.each { %>
            <option value="${it.uuid}">${it.name}</option>
            <%
                    }
                }
            %>
        </select>
    </div>
        <form method="get" class="form-group" id="patient-search-form" onsubmit="return false">
            <input type="text"  id="search-phrase" placeholder="${ui.message("Search by patient ID, Omang or Name ")}"/>
        </form>
    </div>
<div class="table-responsive">
    <table id="patientQueueListTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Sex</th>
            <th>Age</th>
            <th>Send From</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody data-bind="foreach: patientQueueList">

        <tr>
            <td data-bind="text: patientId"></td>
            <td data-bind="text: patientNames"></td>
            <td data-bind="text: gender"></td>
            <td data-bind="text: age"></td>
            <td data-bind="text: locationFrom"></td>
            <td data-bind="text: status"></td>
            <td>
                <a data-bind="attr:{ href: 'javaScript:actionRedirect(' + patientId + ')' }" class="color-primary">
                     Action
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
