<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function (oSettings, aData, iDataIndex) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#status').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                matchingOptions.push(status === jq('#status').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return true;
            }
            return matchingOptions.reduce(function (overallStatus, currentStatus) {
                return overallStatus && currentStatus;
            });
        }
    );

    jq(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            data: { sourcePageName: "appointments/appointmentsPatientPool"},
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                console.log(data);
                var table = jQuery('#appointmentsPoolTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {
                            'data': null,
                            "render": function (data, type, row, meta) {
                                var formUuid = '${addAppointmentFormUuid}';
                                if (type === 'display') {
                                    return '<a href="/${ui.contextPath()}/botswanaemr/enterForm.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&formUuid='+formUuid+'&returnUrl='+row.returnUrl+'" class="text-primary float-left pl-3" id="view">Add Appointment</a>';
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#appointmentsPatientPool'));
                    table.draw();
                });

                jq('#searchPhrase').on('keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if (timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function () {
                            table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>
<style>
    .transparent-btn {
        background-color: transparent;
        color: #0099CC;
    }
</style>
<div class="row" style="display: flex; justify-content: flex-end;">

    <a href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl])}" class="btn btn-primary text-white mr-0 float-right">
        <span class="btn-label"><i class="fa fa-user-plus"></i></span> ${ui.message("Add")}
    </a>

</div>
<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label for="status">Status</label>
        <select id="status"
                class="form-control">
            <option selected disabled value="">Select One</option>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>
<table id="appointmentsPoolTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>Patient ID</th>
        <th>Name</th>
        <th>Sex</th>
        <th>Age</th>
        <th>Actions</th>
    </tr>
    </thead>
</table>
