<script type="text/javascript">
    jq(document).ready(function () {
        jq("#cancelReason-section").hide();

        jq('input[type=radio][name=cancelAppointment]').change(function() {
            if (jq(this).val() === 'yes') {
                jq("#cancelReason-section").show();
            } else {
                jq("#cancelReason-section").hide();
            }
        });
    });

</script>
<style>
    h6 {
        color: #363463;
    }

    .badge {
        min-width: 100% !important;
    }
</style>
<div class="card pr-0 pl-0">
    <div class="row mb-4">
        <div class="col pr-0 pl-0">
            <h6 class="mb-2 mt-2 pl-4 pr-4">Add Appointment</h6>
            <hr class="divider pt-1"/>
        </div>
        <div class="container-fluid">
            <div class="pr-4 pl-4">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="appointmentType">Appointment Type:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="custom-select form-control" id="appointmentType" name="appointmentType">
                                <option selected disabled value="">Appointment Type</option>
                                <option value="awaiting">Awaiting</option>
                                <option value="filled">Filled</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="service">Service:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="custom-select form-control" id="service" name="service">
                                <option selected disabled value="">Select Service</option>
                                <option value="awaiting">Awaiting</option>
                                <option value="filled">Filled</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="clientCategory">Client Category:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="custom-select form-control" id="clientCategory" name="clientCategory">
                                <option selected disabled value="">Client category</option>
                                <option value="awaiting">Awaiting</option>
                                <option value="filled">Filled</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="serviceProvider">Service Provider:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="custom-select form-control" id="serviceProvider" name="serviceProvider">
                                <option selected disabled value="">Service Provider</option>
                                <option value="awaiting">Awaiting</option>
                                <option value="filled">Filled</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="appointmentDate">Appointment Date:
                                <span class="text-danger">*</span>
                            </label>
                            <input type="date" class="form-control border" id="appointmentDate"
                                   name="appointmentDate" placeholder="Appointment Date Range"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="appointmentStatus">Appointment Status:
                                <span class="text-danger">*</span>
                            </label>
                            <select class="custom-select form-control" id="appointmentStatus" name="appointmentStatus">
                                <option selected disabled value="">Appointment Status</option>
                                <option value="awaiting">Awaiting</option>
                                <option value="filled">Filled</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-6">
                                <span class="badge badge-secondary pb-3 pt-3">Current Bookings : 24</span>
                            </div>
                            <div class="col-6">
                                <span class="badge badge-secondary pb-3 pt-3">Day's Limit : 40</span>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group edit-section">
                            <label for="showUpDate">Show Up Date:
                                <span class="text-danger">*</span>
                            </label>
                            <input type="date" class="form-control border" id="showUpDate"
                                   name="showUpDate" placeholder="Show Up Date"/>
                        </div>
                    </div>
                </div>

                <h5 class="text-primary text-left">EDIT DETAILS</h5>
                <hr class="divider pt-1 bg-primary"/>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="rescheduleDate">Reschedule Date:
                                <span class="text-danger">*</span>
                            </label>
                            <input type="date" class="form-control border" id="rescheduleDate"
                                   name="rescheduleDate" placeholder="Reschedule Date"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="appointmentStatus">Cancel Appointment:
                                <span class="text-danger">*</span>
                            </label>
                            <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="cancelAppointment" type="radio" id="yesCancel" value="yes">
                                <label class="form-check-label" for="yesCancel">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="cancelAppointment" type="radio" id="noCancel" value="no">
                                <label class="form-check-label" for="noCancel">No</label>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="cancelReason-section">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="cancelReason">Cancel Reason:
                                <span class="text-danger">*</span>
                            </label>
                            <textarea
                                    type="text" class="form-control border" id="cancelReason"
                                   name="cancelReason" placeholder="Reschedule Date"></textarea>
                        </div>
                    </div>
                </div>

                <div class="border-0 mt-5">
                    <div class="row actions px-0">
                        <div class="col-12 px-0" id="buttonSection">
                            <button type="button" id="cancel" class="btn btn-md btn-dark bg-dark float-right ml-1 pl-5 pr-5">
                                Back
                            </button>
                            <button type="button" id="saveAppointment"
                                    class="btn btn-md btn-primary float-right mr-0 pl-5 pr-5">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>