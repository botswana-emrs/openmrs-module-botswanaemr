<%
ui.includeJavascript("botswanaemr", "calendar/calendar-main.js")
ui.includeJavascript("botswanaemr", "calendar/moment.min.js")
ui.includeJavascript("botswanaemr", "daterangepicker.min.js")
ui.includeCss("botswanaemr", "daterangepicker.css")

%>
<script type="text/javascript">

   jq(document).ready(function () {
      let table;
      // function datePicker() {
      //    jq(document).ready(function () {
      //       jq("#Datepicker").datepicker({
      //          dateFormat: "dd/mm/yy",
      //          minDate: null,
      //          maxDate: "+3M +0D",
      //          beforeShowDay: dateRange,
      //          onSelect: DRonSelect
      //       });
      //    });
      // }

      jq('#appointmentDateRange').daterangepicker({
        opens: 'left',
        locale: {
          format: 'DD/MM/YYYY'
        }
      }, function (startDate, endDate) {
        jQuery("#appointmentDateRange").val(startDate+"-"+endDate);
        jq("#checkinDate").text(startDate.format("DD/MM/YYYY"));
        jq("#checkoutDate").text(endDate.format("DD/MM/YYYY"));
      });

      //jq("#appointmentDateRange").on("click", function () {
      //  datePicker();
      //  jq("#Datepicker .ui-datepicker").show();
      //});

      //function dateRange(date) {
      //  var date1 = jq.datepicker.parseDate("dd/mm/yy", jq("#checkinDate").text());
      //  var date2 = jq.datepicker.parseDate("dd/mm/yy", jq("#checkoutDate").text());
      //  var isHighlight = date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2));

      //  return [true, isHighlight ? "dp-highlight" : ""];
      //}

      //function DRonSelect(dateText) {
      //  var date1 = jq.datepicker.parseDate("dd/mm/yy", jq("#checkinDate").text());
      //  var date2 = jq.datepicker.parseDate("dd/mm/yy", jq("#checkoutDate").text());

      //  if (!date1 || date2) {
      //     jq("#checkinDate").text(dateText);
      //     jq("#checkoutDate").text("");
      //      jq("#Datepicker").datepicker();
      //   } else {
      //      if (jq.datepicker.parseDate("dd/mm/yy", jq("#checkinDate").text()) >=
      //             jq.datepicker.parseDate("dd/mm/yy", dateText)) {
      //          jq("#checkinDate").text(dateText);
      //          jq("#checkoutDate").text("");
      //          jq("#Datepicker").datepicker();
      //      } else {
      //         jq("#checkoutDate").text(dateText);
      //         jq("#Datepicker").datepicker();
      //         jq("#Datepicker .ui-datepicker").hide();
      //         let dateVal = jq("#checkinDate").text() + " - " + jq("#checkoutDate").text();
      //         jq("#appointmentDateRange").val(dateVal);
      //      }
      //   }
      //}

      function getAppointmentsBySearch(startDate, endDate, nameOrUniqueId, type, status, service) {
         let searchResult = [];
         showLoadingOverlay();
         jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","appointments/appointmentsManagement","searchAppointmentAdvanced")}',
            dataType: "json",
            global: false,
            async: false,
            data: {
               nameOrUniqueId: nameOrUniqueId,
               startDate: startDate,
               endDate: endDate,
               service: service,
               status: status,
               type: type
            },
            success: function (data) {

               searchResult = data;
               populateTbody(searchResult);
               hideLoadingOverlay();
            }
         });

         // Implement Ajax call

         return searchResult;
      }

      function populateTbody(data) {
         var formUuid = '${addAppointmentFormUuid}';
         jQuery('#appointmentsTable').DataTable().buttons().destroy();
         jQuery('#appointmentsTable').DataTable().clear().destroy();

         // Check if data map is not undefined or empty and return
         if (!data || data.length === 0) {
            return;
         }

         data.map((item) => {
            let appointmentDate = moment(new Date(item.appointmentDate)).format("DD/MM/YYYY");
            let returnUrl = urlUtils.encodeUrl('${ui.pageLink("botswanaemr", "appointments/appointmentsManagement")}');
            let viewUrl = "/${ui.contextPath()}/botswanaemr/appointments/viewAppointment.page?appointmentId=" + item.appointmentId + "&patientId=" + item.patientId + "&returnUrl=" + returnUrl;
            let viewTd = '<td class="dt-center editor-view text-primary"> <a href="' + viewUrl +'">View</a></td>';
            viewTd = '';
            let editUrl = item.encounterId ? "/${ui.contextPath()}/botswanaemr/editEncounter.page?encounterId=" + item.encounterId + "&patientId=" + item.patientId + "&returnUrl=" + returnUrl + "&appointmentId=" + item.appointmentId : "";
            let editTd = '';
            if (editUrl == '') {
               editTd = '<td class="dt-center editor-view text-primary"> <a href="' + viewUrl +'">View</a></td>';
            } else {
               editTd = '<td class="dt-center editor-view text-primary"> <a href="' + viewUrl +'">View</a> | <a href="' + editUrl +'">Edit</a></td>';
            }
            let checkinTd = '<td class="dt-center editor-view text-primary"> <a href="">Check-in</a></td>';
            let checkedInByTd = '<td>'  + item.checkedInBy + '</td>'
            let serviceTd = '<td class="dt-center editor-view text-primary"> ' + item.service + '</td>';
            let appointmentTypeTd = '<td class="dt-center editor-view text-primary"> ' + item.appointmentType + '</td>';
            let appointmentStatusTd = '<td class="dt-center editor-view text-primary"> ' + item.appointmentStatus + '</td>';
            checkinTd = '';
            checkedInByTd = '';
            jq("#appointmentsTableDetails").append("<tr><td>" + "</td><td>" + appointmentDate + "</td> <td>" + item.pin + "</td><td>" + item.patientName  + "</td>" + serviceTd + appointmentTypeTd + appointmentStatusTd + viewTd + editTd + checkinTd + "</tr>");
         });

         initializeAppointmentsTable();
      }

      var updateTable = function () {
         getAppointmentsBySearch(
                 jq('#checkinDate').html(),
                 jq('#checkoutDate').html(),
                 jq('#searchPhrase').val(),
                 jq('#appointmentType').val(),
                 jq('#appointmentStatus').val(),
                 jq('#service').val(),
         );
      }

      jq('#resetBtn').on('click', function () {
         jq('#checkinDate').html('')
         jq('#checkoutDate').html('')
         jq('#appointmentDateRange').val('')
         jq('#searchPhrase').val('')
         jq('#appointmentType').val('')
         jq('#appointmentStatus').val('')
         jq('#service').val('')

         updateTable();
      });

      jq('#inquireBtn').on('click', function () {
         updateTable();
      });

      jq("#searchPhrase").on("keyup", function () {
         if (jq(this).val().length > 3) {
            table.search(jq('#searchPhrase').val()).draw();
            if (jq('#filterGender').val() != null) {
               table.columns(2).search(jq('#filterGender').val()).draw();
            }
         }
      })

      function initializeAppointmentsTable() {
         table = jQuery('#appointmentsTable').DataTable({
            'columnDefs': [
               {
                  'targets': 0,
                  'className': 'select-checkbox',
                  'checkboxes': {
                     'selectRow': true,
                     'select': true
                  }
               }
            ],
            'select': {
               'style': 'multi',
               'selector': 'td:first-child'
            },
            'dom': 'lBrtip',
            'buttons': [
               {
                  'extend': 'collection',
                  'text': 'More operations',
                  'className': 'btn btn-sm p-1 ',
                  'exportOptions': {
                     'modifier': {
                        'selected': true
                     }
                  },
                  'buttons': [
                     {
                        'extend': 'csv',
                        'text': 'Export to CSV',
                        'className': 'btn btn-sm '
                     },
                     {
                        'extend': 'pdf',
                        'text': 'Export to PDF',
                        'className': 'btn btn-sm '
                     }
                  ]
                  ,
                  'dropup': false
               }
            ],
            'searching': true,

            "pagingType": 'simple_numbers',

            "language": {
               "info": "Showing _START_ to _END_ of _TOTAL_ entries",
               "paginate": {
                  "previous": "&lt;",
                  "next": "&gt;"
               }
            },
            'order': [[1, 'asc']]

         });
         jQuery('#selectedRowsAlertDiv').hide();

         table.on('select', function (e, dt, type, indexes) {
            if (table.rows({selected: true}).count() === 1) {
               jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " item selected");
               jQuery('#selectedRowsAlertDiv').show();
            } else {
               jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
               jQuery('#selectedRowsAlertDiv').show();
            }
         })
                 .on('deselect', function (e, dt, type, indexes) {
                    if (table.rows({selected: true}).count() === 0) {
                       jQuery('#selectedRowsAlertDiv').hide();
                    } else {
                       jQuery('#selectedRowsCount').text(table.rows({selected: true}).count() + " items selected");
                       jQuery('#selectedRowsAlertDiv').show();
                    }

                 });

         table.buttons().container().appendTo(jQuery('#other '));

         jQuery('.card-link').click(() => {
            if (jQuery('#genderDiv').hasClass('show')) {
               let span = jq("<span/>")
                       .text("Expand")
               i = jQuery("<i>")
                       .attr("id", "collapse-icon")
                       .addClass("icon-chevron-down");
               jQuery('#collapseBtn').html(span.append(i));
               jQuery('.collapse').removeClass("show");
            } else {
               let span = jQuery("<span/>").text("Collapse")
               i = jQuery("<i>")
                       .attr("id", "collapse-icon")
                       .addClass("icon-chevron-up");
               jQuery('#collapseBtn').html(span.append(i));
               jQuery('.collapse').addClass("show");
            }
         });
      }

      updateTable();

   });
</script>
<style>
   .dp-highlight .ui-state-default {
      background: #484 !important;
      color: #FFF !important;
   }

   .ui-datepicker.ui-datepicker-multi {
      width: 100% !important;
   }

   .ui-datepicker-multi .ui-datepicker-group {
      float: none;
   }

   #datepicker {
      height: 300px;
      overflow-x: scroll;
   }

   .overflow-container {
      position: relative;
   }

   .element1,
   .element2 {
      position: absolute;
   }

   .element1 {
      top: 0; /* Adjust as needed */
      left: 0; /* Adjust as needed */
      z-index: 100000;
   }

   .element2 {
      top: 0; /* Align to the top */
      right: 0; /* Align to the right */
   }

   h6 {
      color: #363463;
   }

   .ui-widget {
      font-size: 100%
   }
</style>

<div class="card">
   <div class="row mb-4">
      <div class="row col-sm-12">
         <div class="col-6">
            <h6>Appointments Management</h6>
         </div>
         <div class="col-6" style="display: flex;justify-content: flex-end;">
            <button type="button" id="addAppointmentBtn" class="btn btn-sm btn-primary mb-3">
               <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsPatientPool')}" class=""> New
                  Appointment
               </a>
            </button>
         </div>
      </div>
      <div class="col-sm-9 row">
         <div class="col-4 pl-0">
            <input type="text" class="form-control border" id="searchPhrase"
                   name="searchPhrase" placeholder="Patient ID or name">
         </div>
         <div class="col-4 pl-0">
            <div class="overflow-container">
               <input type="text" class="form-control border element2" id="appointmentDateRange"
                      name="appointmentDateRange" placeholder="Appointment Date Range"/>
               <div class="element1" id="Datepicker"></div>
            </div>
            <label id="checkinDate" hidden></label>
            <label id="checkoutDate" hidden></label>
         </div>
         <div class="col-4 pl-0">
            <div class="form-horizontal form-group">
               <select class="custom-select form-control" id="service" name="service">
                  <option selected value="" disabled>Service</option>
                  <% services.each { service ->  if (service) { %>
                     <option value="${service?.uuid}" >${service?.name?.name}</option>
                  <% } } %>
               </select>
            </div>
         </div>
         <div class="col-4 pl-0">
            <div class="form-horizontal form-group">
               <select class="custom-select form-control" id="appointmentStatus" name="appointmentStatus">
                  <option selected disabled value="">Appointment Status</option>
                  <option value="scheduled">Scheduled</option>
                  <option value="show-up">Show up</option>
                  <option value="missed">Missed</option>
                  <option value="rescheduled">Rescheduled</option>
                  <option value="cancelled">Cancelled</option>
               </select>
            </div>
         </div>
         <div class="col-4 pl-0">
            <div class="form-horizontal form-group">
               <select class="custom-select form-control" id="appointmentType" name="appointmentType">
                  <option selected disabled value="">Appointment Type</option>
                  <option value="pending">Pending</option>
                  <option value="walk-in">Walk in</option>
               </select>
            </div>
         </div>
      </div>
      <div class="col-sm-3 row" style="display: flex;justify-content: flex-end;">
         <div class="col-auto">
            <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
               ${ui.message("Search")}
            </button>
         </div>
         <div class="col-auto">
            <button type="submit" id="resetBtn" class="btn btn-sm btn-primary mb-3 bg-light text-dark border">
               ${ui.message("Reset")}
            </button>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
         <div id="selectedRowsAlertDiv" class="alert alert-info color-info p-0" role="alert">
            <label class="icon-info-sign color-primary" for="selectedRowsCount"></label>
            <label id="selectedRowsCount"></label>
         </div>
         <div class="table-responsive">
            <table id="appointmentsTable"
                   class="table table-striped table-sm table-bordered" style="width:100%">
               <thead>
               <tr>
                  <th></th>
                  <th>Appointment Date</th>
                  <th>Patient ID</th>
                  <th>Patient Name</th>
                  <th>Service</th>
                  <th>Appointment Type</th>
                  <th>Appointment Status</th>
                  <!--<th>Checked-In By</th>-->
                  <th scope="col" class="bg-transparent border-0">Actions</th>
                  <!--<th scope="col" class="bg-transparent border-0">&nbsp;</th>-->
                  <!--<th scope="col" class="bg-transparent">&nbsp;</th>-->
               </tr>
               </thead>
               <tbody id="appointmentsTableDetails">
               </tbody>
            </table>
         </div>
      </div>
   </div>

</div>