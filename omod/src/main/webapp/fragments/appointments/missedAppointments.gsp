<div class="table-responsive">
    <table id="missedAppointmentsTable"
           class="table table-striped table-sm table-bordered">
        <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Date Time</th>
            <th>Service Type</th>
            <th>Status</th>
            <th>No. of Days Late</th>
            <th>Phone No.</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <% if (appointmentsList.size() > 0) { %>
        <% appointmentsList.each { %>
        <tr>
            <td>${it.patient.identifiers[0]}</td>
            <td>${it.patient.givenName}</td>
            <td>${it.timeSlot.startDate.format("HH:MM dd-MMM-yyyy")}</td>
            <td>${it.appointmentType.name}</td>
            <td>${it.status.name}</td>
            <td>${it.timeSlot.startDate - new Date()}</td>
            <td>${it.patient.getAttribute('Telephone Number') ?: '-'}</td>
            <td>${it.patient.getAttribute('Email') ?: '-'}</td>
        </tr>
        <% } %>
        <% } else { %>
        <tr>
            <td>No appointments to show over this period</td>
        </tr>
        <% } %>
        </tbody>

    </table>
</div>