<script type="text/javascript">
    jq(document).ready(function () {

    });

</script>
<style>
    h6 {
        color: #363463;
    }

    .badge {
        min-width: 100% !important;
    }
</style>
<div class="card pr-0 pl-0">
    <div class="row col-sm-12">
        <div class="col-6">
            <h5>View Appointment</h5>
        </div>
        <div class="col-6" style="justify-content: flex-end;">
            ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
            <button type="button" id="addAppointmentBtn" class="btn btn-sm btn-primary mb-3 hidden">
                <a href="${ui.pageLink('botswanaemr', 'appointments/checkIn')}" class="">
                    Check in Patient
                </a>
            </button>
        </div>
    </div>
    <hr class="divider pt-1"/>
    ${ ui.includeFragment("botswanaemr", "clientProfileBioData")}

    <div class="row mb-4 mt-4">
        <div class="col">
            <h5 class="text-primary text-left">APPOINTMENT DETAILS</h5>
            <hr class="divider pt-1 bg-primary"/>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table mt-3">
                <tbody>
                    <tr>
                        <td>Appointment Type</td>
                        <td>${appointment.appointmentType?.name}</td>
                    </tr>
                    <tr>
                        <td>Client Category</td>
                        <td>${clientCategory}</td>
                    </tr>
                    <tr>
                        <td>Appointment Type</td>
                        <td>${appointment.appointmentType.name}</td>
                    </tr>
                    <tr>
                        <td>Appointment Date</td>
                        <td>${appointment.timeSlot?.startDate.format("dd-MMM-yyyy")}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col">
            <table class="table mt-3">
                <tbody>
                <tr>
                    <td>Service</td>
                    <td>${service}</td>
                </tr>
                <tr>
                    <td>Service Provider</td>
                    <td>${appointment?.timeSlot?.appointmentBlock?.provider?.person?.personName}</td>
                </tr>
                <tr>
                    <td>Appointment Status</td>
                    <td>${appointment.status?.name}</td>
                </tr>
                <tr>
                    <td>Show-up Date</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>