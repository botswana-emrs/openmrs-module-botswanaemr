<%
    ui.includeJavascript("botswanaemr", "calendar/calendar-main.js")
    ui.includeJavascript("botswanaemr", "calendar/moment.min.js")
    ui.includeJavascript("botswanaemr", "jquery.timepicker.min.js")
    ui.includeCss("botswanaemr", "calendar/calendar-main.css")
    ui.includeCss("botswanaemr", "jquery.timepicker.min.css")
%>
<script>

    document.addEventListener('DOMContentLoaded', () => {
        const todayDate = moment().startOf("day");
        const YM = todayDate.format("YYYY-MM");
        const YESTERDAY = todayDate.clone().subtract(1, "day").format("YYYY-MM-DD");
        const TODAY = todayDate.format("YYYY-MM-DD");
        const TOMORROW = todayDate.clone().add(1, "day").format("YYYY-MM-DD");

        let calendarEvents = [];
        <% events.each { event -> %>
        calendarEvents.push({
            title: '${event.title}',
            start: '${event.startDate}',
            end: '${event.endDate}'
        });
        <% } %>

        const calendarEl = document.getElementById("schedule-calendar");
        const calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth"
            },

            height: 800,
            contentHeight: 780,
            aspectRatio: 3,

            nowIndicator: true,
            now: TODAY + "T09:25:00",

            views: {
                dayGridMonth: {buttonText: "month"},
                timeGridWeek: {buttonText: "week"},
                timeGridDay: {buttonText: "day"}
            },

            initialView: "timeGridDay",
            initialDate: TODAY,

            eventRender: function (event, element, view) {
                element.bind('click', function () {
                    const day = (jq().fullCalendar.formatDate(event.start, 'dd'));
                    const month = (jq().fullCalendar.formatDate(event.start, 'MM'));
                    const year = (jq().fullCalendar.formatDate(event.start, 'yyyy'));
                    alert(year + '-' + month + '-' + day);
                });
            },
            editable: true,
            dayMaxEvents: true,
            navLinks: true,

            events: calendarEvents,
            selectable: true,
            select: (start, end, allDay, jsEvent, view) => {
            },
            dateClick: function (info) {
                let selectedDate = info.dateStr;
                let formattedDate = moment(selectedDate).format('dddd, MMMM DD yyyy');
                jq('#appointmentDate').val(selectedDate);
                jq('#appointmentDateDisplay').text(formattedDate);
                jQuery('#createAppointmentModal').modal('show');
                let startDateTime = new Date(selectedDate);
                jq("#startTime").timepicker('option', 'minTime', startDateTime);
                jq("#startTime").timepicker('setTime', startDateTime);
                jq("#startTime").timepicker('defaultTime', startDateTime);
                // console.log('Date: ' + info.dateStr);
                // alert('Resource ID: ' + info.resource.id);
            }
        });

        calendar.render();
    });

    jq(function () {
        jq("#createPatientAppointmentForm").submit(function (e) {
            e.preventDefault();

            let appointmentDate = jq('#appointmentDate').val();
            let startTime = jq("#startTime").timepicker('getTime');
            let endTime = jq("#endTime").timepicker('getTime');
            let formattedStartTime = moment(startTime).format('hh:mm:ss');
            let formattedEndTime = moment(endTime).format('hh:mm:ss');
            let startIndex = appointmentDate.indexOf('T')
            let extractedTimeComponent = appointmentDate.substring(startIndex + 1, startIndex + 6);
            let preparedStartTime = appointmentDate.replace(extractedTimeComponent, formattedStartTime);
            let preparedEndTime = appointmentDate.replace(extractedTimeComponent, formattedEndTime);

            const params = {
                'patientId': ${patient.id},
                'appointmentDate': moment(appointmentDate).format('YYYY-MM-DD'),
                'location': ${location.id},
                'startTime': moment(startTime).format("hh:mm a"),
                'endTime': moment(endTime).format("hh:mm a"),
                'flow': '',
                'notes': ''
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "appointments/schedule", "createAppointment")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Appointment created successfully");
                    window.history.back();
                })
        });

        jq('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '08:00am',
            maxTime: '5:00pm',
            defaultTime: '8:00am',
            startTime: '8:00am',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            zindex: 9999999,
            change: function (timeValue) {
                let element = jq(this);
                if (element.is("#startTime")) {
                    const time = element.val();
                    const picker = jq("#endTime");
                    const defaultEndDate = moment(timeValue).add(moment.duration(30, 'minutes')).toDate();
                    const minTime = moment(defaultEndDate).format("hh:mm a");
                    picker.timepicker('setTime', defaultEndDate);
                    picker.timepicker('option', 'minTime', minTime);
                    picker.timepicker('option', 'defaultTime', time);
                    console.log(element.timepicker('getTime'));
                }
            }
            // TODO: Set minTime on the endTime timpicker when startTime is selected
        });
    });
</script>

<div id="schedule-calendar"></div>

<div class="modal fade" id="createAppointmentModal" tabindex="-1"
     data-controls-modal="createAppointmentModalModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="createAppointmentModalModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white">
                <h4 class="modal-title text-black" id="createAppointmentModalModalTitle">Confirm time</h4>
                <button type="button" id="createAppointmentModalBtn" class="btn btn-sm bg-white text-danger"
                        data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" id="createPatientAppointmentForm">
                    <div class="row">
                        <div class="col mb-2">
                            <i class="fa fa-clock text-secondary"></i><span id="appointmentDateDisplay"
                                                                            class="text-secondary pl-2">Friday, October 9 2020</span>
                            <input type="hidden" id="appointmentDate"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-4">
                            <div class="form-group">
                                <label for="startTime">Start Time
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group">
                                    <input required
                                           id="startTime"
                                           name="startTime"
                                           type="text"
                                           class="form-control input-sm timepicker"
                                           placeholder="Enter time">
                                </div>
                            </div>
                        </div>

                        <div class="col col-md-4">
                            <div class="form-group">
                                <label for="endTime">End Time
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group">
                                    <input required
                                           id="endTime"
                                           name="endTime"
                                           type="text"
                                           class="form-control input-sm timepicker"
                                           placeholder="Enter time">
                                </div>
                            </div>
                        </div>

                        <div class="col col-md-4">
                            <button type="submit" class="btn btn-sm btn-primary float-right pl-4 pr-4"
                                    style="position: absolute;right: 0;bottom: 30px;">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#createAppointmentModal").appendTo("body");
    });
</script>
