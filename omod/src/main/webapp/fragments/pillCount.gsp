<div class="card">
    <div class="row" style="margin: 8px;">
        <div id="weigh-tracking" class="col px-0">
            <table>
                <thead>
                <tr>
                    <th colspan="2" class="text-start">Initial Phase</th>
                </tr>
                </thead>
                <tr>
                    <td class="text-start">Compliance</td>
                    <td><strong>${initialPhaseCompliance} %</strong></td>
                </tr>
                <tr>
                    <td class="text-start">Number of pills taken</td>
                    <td><strong>${initialPillsTaken}</strong></td>
                </tr>
                <tr>
                    <td class="text-start">Expected number of pills</td>
                    <td><strong>${initialPhaseExpectedPills}</strong></td>
                </tr>
            </table>
            <% if (hasSecondPhase) { %>
            <table>
                <thead>
                <tr>
                    <th class="text-start" colspan="2">Continuation Phase</th>
                </tr>
                </thead>
                <tr>
                    <td class="text-start">Compliance</td>
                    <td><strong>${continuationPhaseCompliance} %</strong></td>
                </tr>
                <tr>
                    <td class="text-start">Number of pills taken</td>
                    <td><strong>${continuationPhasePillsTaken}</strong></td>
                </tr>
                <tr>
                    <td class="text-start">Expected number of pills</td>
                    <td><strong>${continuationPhaseExpectedPills}</strong></td>
                </tr>
                <% } %>
            </table>
        </div>
    </div>
</div>
