<%
    ui.includeJavascript("botswanaemr", "calendar/moment.min.js")
    ui.includeJavascript("botswanaemr", "jquery.timepicker.min.js")
    ui.includeCss("botswanaemr", "jquery.timepicker.min.css")
%>
<script type="text/javascript">

    jQuery(function (jq) {
        jq("#btnAddFollowupAppointment").click(function () {
            let url = "${ui.pageLink("botswanaemr", "appointments/schedule", [patientId: patient.patient.id, visitid: visit?.id, providerId: provider?.id])}";
            window.location.href = url;
        });
    });

</script>
<h5 class="pl-3">Schedule follow up consultation</h5>

<div class="clearfix"></div>
<%
    def pattern = "yyyy-MM-dd hh:mm:dd"
%>
<% if (!appointments.isEmpty()) { %>
<div class="row">
    <div class="table-responsive table-sm table-borderless pl-3">
        <table class="table">
            <thead>
            <tr>
                <th>Date & time</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <% appointments.each { %>
            <tr>
                <td>${ui.formatDatetimePretty(it.timeSlot.startDate)}</td>
                <td>
                    <i class="fa fa-circle text-success"></i>
                    ${it.status}
                </td>
                <td>
                    <a data-toggle="modal" href="#updateAppointmentModal"
                       data-update-appointment-id="${it.appointmentId}"
                       data-update-start-date="${it.timeSlot.startDate}"
                       data-update-end-date="${it.timeSlot.endDate}" data-update-status="${it.status}"
                       class="open-updateAppointmentModal text-primary px-0 py-0" id="UpdateModel">EDIT</a> |
                    <a data-toggle="modal" href="#deleteAppointmentModal" id="deleteModal"
                       data-delete-appointment-id="${it.appointmentId}"
                       class="open-deleteAppointmentModal text-danger px-0 py-0">DELETE</a>
                </td>
            </tr>
            <% } %>
            </tbody>
        </table>
    </div>
</div>
<% } else { %>
<div class="row no-data-section">
    <img class=""
         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

    <p class="text-center">No data captured. Add data by clicking "Add" below</p>
</div>
<% } %>
<div class="row">
    <div class="col-md-12">
        <button
                class="dashed-button rounded"
                id="btnAddFollowupAppointment" <% if(!isConsultationActive) { %> disabled <% } %> >+ Add a new follow up appointment
        </button>
    </div>
</div>

<div class="modal fade" id="updateAppointmentModal" tabindex="-1"
     data-controls-modal="updateAppointmentModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="updateAppointmentModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white">
                <h4 class="modal-title text-black" id="updateAppointmentModalTitle">Update Appointment</h4>
                <button type="button" id="updateAppointmentModalBtn"
                        class="btn btn-sm bg-white text-danger"
                        data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" id="updatePatientAppointmentForm">
                    <div class="row">
                        <div class="col mb-2">
                            <label for="appointmentDate">Date
                                <span class="text-danger">*</span></label>
                            <input type="date" id="appointmentDate"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-4">
                            <div class="form-group">
                                <label for="startTime">Start Time
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group">
                                    <input required
                                           id="startTime"
                                           name="startTime"
                                           type="text"
                                           class="form-control input-sm timepicker"
                                           placeholder="Enter time">
                                </div>
                            </div>
                        </div>

                        <div class="col col-md-4">
                            <div class="form-group">
                                <label for="endTime">End Time
                                    <span class="text-danger">*</span>
                                </label>

                                <div class="input-group">
                                    <input required
                                           id="endTime"
                                           name="endTime"
                                           type="text"
                                           class="form-control input-sm timepicker"
                                           placeholder="Enter time">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label for="selectStatus">Status <span class="text-danger">*</span></label>
                                <select id="selectStatus" class="selectPicker">
                                    <option>Select status</option>
                                    <% appointmentStatuses.each { %>
                                    <option value="${it.name}">${it.name}</option>
                                    <% } %>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row button-section-right" style="margin-top: 2rem;">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" style="margin-left: 2rem;">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteAppointmentModal" tabindex="-1"
     data-controls-modal="deleteAppointmentModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="deleteAppointmentModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-white">
                <h4 class="modal-title text-black" id="deleteAppointmentModalTitle">Remove Appointment</h4>
                <button type="button" id="deleteAppointmentModalBtn" class="btn btn-sm bg-white text-danger"
                        data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <form method="post" id="deletePatientAppointmentForm">
                    <div class="row">
                        <div class="col mb-2">
                            <i class="fa fa-remove text-secondary"></i>
                            <span class="text-secondary pl-2">Confirm you want to remove this appointment</span>
                        </div>
                    </div>

                    <div class="row button-section-right" style="margin-top: 2rem;">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1"
                                style="margin-left: 2rem; background: #cd201f">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    jq(function () {
        jq("#updateAppointmentModal").appendTo("body");
        jq("#deleteAppointmentModal").appendTo("body");
    });

    jq(document).on("click", ".open-updateAppointmentModal", function () {
        const appointmentId = jq(this).data('updateAppointmentId');
        const appointmentStartDate = jq(this).data('updateStartDate');
        const appointmentEndDate = jq(this).data('updateEndDate');
        const appointmentStatus = jq(this).data('updateStatus');

        jq('#appointmentDate').val(moment(appointmentStartDate).format('YYYY-MM-DD'));
        jq("#startTime").timepicker('setTime', new Date(appointmentStartDate));
        jq("#endTime").timepicker('setTime', new Date(appointmentEndDate));
        jq('select option[value=' + appointmentStatus + ']').attr("selected", true);

        jq("#updatePatientAppointmentForm").submit(function (e) {
            e.preventDefault();

            let appointmentDate = jq('#appointmentDate').val();
            let startTime = moment(jq("#startTime").timepicker('getTime')).format("hh:mm a");
            let endTime = moment(jq("#endTime").timepicker('getTime')).format("hh:mm a");

            const params = {
                'appointmentDate': appointmentDate,
                'appointmentId': appointmentId,
                'startTime': startTime,
                'endTime': endTime,
                'appointmentStatus': jq('#selectStatus').find(":selected").text()
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "scheduleFollowUpConsultation", "updateScheduleAppointment")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Appointment updated successfully");
                    location.reload();
                })
        });

        jq('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            minTime: '08:00am',
            maxTime: '5:00pm',
            defaultTime: '8:00am',
            startTime: '8:00am',
            dynamic: false,
            dropdown: true,
            scrollbar: true,
            zindex: 9999999,
            change: function (timeValue) {
                let element = jq(this);
                if (element.is("#startTime")) {
                    const time = element.val();
                    const picker = jq("#endTime");
                    const defaultEndDate = moment(timeValue).add(moment.duration(30, 'minutes')).toDate();
                    const minTime = moment(defaultEndDate).format("hh:mm a");
                    picker.timepicker('setTime', defaultEndDate);
                    picker.timepicker('option', 'minTime', minTime);
                    picker.timepicker('option', 'defaultTime', time);
                }
            }
        });
    });

    jq(document).on("click", ".open-deleteAppointmentModal", function () {
        const appointment = jq(this).data('deleteAppointmentId');

        jq("#deletePatientAppointmentForm").submit(function (e) {
            e.preventDefault();
            const params = {
                'appointmentId': appointment
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "scheduleFollowUpConsultation", "deleteAppointment")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Appointment deleted successfully");
                    location.reload();
                })
        });
    });
</script>