<script type="text/javascript">
    jq(document).ready(function () {
        jq('#saveAppointmentBookings').on('click', function () {
            let bookingLimits = {};

            jq('#add-list-appointment-bookings tbody tr').each(function () {
                let day = jq(this).find('td:first').text().trim();
                let limit = jq(this).find('input').val();

                bookingLimits[day] = limit;
            });

            let bookingLimitsJson = JSON.stringify(bookingLimits);
            console.log(bookingLimitsJson)

            jq.ajax({
                type: 'POST',
                url: '${ui.actionLink("botswanaemr", "appointmentBookingLimit", "post")}',
                data: {
                    facilityId: jq('#facilityId').val(),
                    bookingLimits: bookingLimitsJson
                },
                dataType: "json",
                global: false,
                async: false,
                success: function (response) {
                    jq().toastmessage('showSuccessToast', "Appointment Booking limits saved successfully");
                    jQuery("#addAppointmentBookingLimitsModal").modal('hide');
                    location.reload();
                },
                error: function (error) {
                    jq().toastmessage('showErrorToast', "Error saving appointment booking limits");
                }
            });
        });
    });
</script>

<div class="card">
    <input type="text" id="facilityId" name="facilityId" value="${facilityId}" hidden>
    <table id="list-appointment-bookings" class="table table-sm table-bordered" style="width:100%">
        <thead>
            <tr>
                <th class="text-center">Day</th>
                <th class="text-center">Limit</th>
            </tr>
        </thead>
        <tbody>
        <%
            List<String> daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            Map<String, Integer> bookingLimitMap = new HashMap<>();

            // Fill the map with the appointmentBookingLimits data(day and limit)
            appointmentBookingLimits.each { bookingLimit ->
                bookingLimitMap.put(bookingLimit.day, bookingLimit.limit);
            }

            for (String day : daysOfWeek) {
            %>
                <tr>
                    <td class="text-center align-middle"><%= day %></td>
                    <td class="text-center align-middle">
                        <%= (bookingLimitMap.containsKey(day) && bookingLimitMap.get(day) != null) ? bookingLimitMap.get(day) : "" %>
                    </td>
                </tr>
            <% } %>
        </tbody>
    </table>
</div>

<div class="modal fade" id="addAppointmentBookingLimitsModal" tabindex="-1"
     data-controls-modal="addFacilitiesModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addAppointmentBookingLimitsModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addAppointmentBookingLimitsModalTitle">Daily Booking Limits</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <table id="add-list-appointment-bookings" class="table table-sm table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Day</th>
                            <th class="text-center">Limit</th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                        for (String day : daysOfWeek) {
                    %>
                            <tr>
                                <td class="text-center align-middle"><%= day %></td>
                                <td class="text-center align-middle">
                                    <input type="number"
                                           id="<%= day.toLowerCase() %>"
                                           name="<%= day.toLowerCase() %>"
                                           class="form-control"
                                           value="<%= (bookingLimitMap.containsKey(day) && bookingLimitMap.get(day) != null) ? bookingLimitMap.get(day) : '' %>" />
                                </td>
                            </tr>
                    <% } %>
                    </tbody>
                </table>

                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-dark bg-dark float-left" data-dismiss="modal">
                        ${ui.message("Close")}
                    </button>
                    <button id="saveAppointmentBookings"
                            type="submit"
                            class="btn btn-sm btn-primary float-right float-left ml-1">${ui.message("Save")}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>