<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>

<div class="row">
    <div class="col col-sm-12 col-md-12 col-lg-12">
        <button type="button" class="btn btn-primary" id="btn-back">
            <i class="fa fa-arrow-left"></i>
        </button>
    </div>
</div>

<div id="manageConditionsApp">
    <div class="row">
        <div class="col-12">
            <div class="info-section" ng-app="triageDataApp" ng-controller="TriageDataController"
                 ng-init="vitalsData = getVitalsObsData('${patient.patient.uuid}')">
                <div class="info-header">
                    Nurse Examinations
                </div>

                <div class="info-body">
                    <ul ng-repeat="vitalsObsData in vitalsObs">
                        <p>Vitals data {{vitalsObs.length}}</p>
                    </ul>

                    <p ng-show="vitalsObs.length == 0">
                        ${ui.message("coreapps.none")}
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>
