<script>
    jQuery(function () {
    });
</script>
<style>
    .bg-pale {
        color : #373757 !important;
    }
</style>
<div class="row">
    <div class="row col-12 pl-0 pr-0 mb-2">
        <div class="col-12 pl-0 pr-0">
            <h5 class="text-primary text-left mt-3 pl-3">HIV STATUS</h5>
            <hr class="divider pt-1 bg-primary ml-3"/>

            <div class="row pl-3">
                <% if (hivStatus == "Negative") { %>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="bg-pale">HIV STATUS</th>
                            <th class="bg-pale text-left">DATE OF NEGATIVE TEST</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Negative</td>
                           <td>${ hivStatusObs?.get('negativeResultDate')?.get('val') ? hivStatusObs.get('negativeResultDate').get('val').format("dd-MMM-yyyy") : '' }</td>
                        </tr>
                    </tbody>
                </table>
                <% } else if (hivStatus == "Positive") { %>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="bg-pale">HIV STATUS</th>
                        <th class="bg-pale text-left">ON ART</th>
                        <th class="bg-pale text-left">SITE</th>
                        <th class="bg-pale text-left">REFERRED TO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>${hivStatus}</td>
                        <td>${hivStatusObs?.get('onArt')?.get('val') ?: ''}</td>
                        <td>${hivStatusObs?.get('artSite')?.get('val') ?: ''}</td>
                        <td>${hivStatusObs?.get('referredTo')?.get('val') ?: ''}</td>
                    </tr>
                    </tbody>
                </table>
                <% } else { %>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="bg-pale">HIV STATUS</th>
                        <th class="bg-pale text-left">PITC ACCEPTED</th>
                        <th class="bg-pale text-left">PITC RESULTS</th>
                        <th class="bg-pale text-left">DATE</th>
                        <th class="bg-pale text-left">ACTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Unknown</td>
                        <td>${ hivStatusObs?.get('pitcAccepted')?.get('val') ?: ''}</td>
                        <td>${ hivStatusObs?.get('pitcResults')?.get('val') ?: ''}</td>
                        <td>${hivStatusObs && hivStatusObs.get('unknownResultDate')?.get('val') ? hivStatusObs.get('unknownResultDate').get('val').format("dd-MMM-yyyy") : ''}</td>
                        <td><span> <a href="${ui.pageLink('botswanaemr', 'hts/htsClientProfile', [patientId: patient.id, returnUrl: returnUrl])}">View HTS</a></span></td>
                    </tr>
                    </tbody>
                </table>
                <% } %>
            </div>
            <div class="col-sm-12">
                <button role="button"
                        class="btn btn-md btn-primary float-right mr-0 pr-4 pl-4" style="color: #fff !important;" data-toggle="modal"
                        data-target="#hivStatusModal">
                    ${ lastHivStatusEncounter ? 'Edit' : 'Add' }
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="hivStatusModal" tabindex="-1"
     data-controls-modal="hivStatusModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="hivStatusModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hivStatusModalTitle">HIV Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="hivStatusForm">
                    <% if(hivStatus == 'Negative') { %>
                    <h6>Negative Within last 12 months</h6>
                    <div class="form-group">
                        <label>Date of Treatment</label>
                        <input type="date" id="negativeResultDate" name="negativeResultDate" class="form-control">
                    </div>
                    <% } %>

                    <% if(hivStatus == 'Unknown') { %>
                    <h6>Unknown</h6>
                    <div class="form-group">
                        <label>PITC Accepted?</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" id="pitcAcceptedYes" name="pitcAccepted" value="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Yes
                            <input type="radio" id="pitcAcceptedNo" name="pitcAccepted"  value="1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> No
                        </div>
                    </div>
                    <div class="form-group">
                        <label>PITC Results:</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" id="pitcResultsPo" name="pitcResults" value="703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Positive
                            <input type="radio" id="pitcResultsNeg" name="pitcResults" value="664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Negative
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="unknownResultDate">Date</label>
                        <input type="date" id="unknownResultDate" name="unknownResultDate" class="form-control">
                    </div>
                    <% } %>

                    <% if(hivStatus == 'Positive') { %>
                    <h6>Positive</h6>
                    <div class="form-group">
                        <label>On ART?</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" id="onArtYes" name="onArt" value="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> Yes
                            <input type="radio" id="onArtNo" name="onArt"  value="1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"> No
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="artSite">Site</label>
                        <input type="text" id="artSite" name="artSite" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="referredTo">If not on ART, referred to:</label>
                        <input type="text" id="referredTo" name="referredTo" class="form-control">
                    </div>
                    <% } %>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveHivStatus">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    jq(document).ready(function() {
        let lastHivStatusEncounter = "${lastHivStatusEncounter?.uuid}";
        let hivStatusObs =  JSON.parse(`${hivStatusObsObjs}`);
        function padTo2Digits(num) {
            return num.toString().padStart(2, '0');
        }

        function formatDate(date) {
            return [
                date.getFullYear(),
                padTo2Digits(date.getMonth() + 1),
                padTo2Digits(date.getDate()),
            ].join('-');
        }
        jq('#hivStatusModal').on('shown.bs.modal', function (event) {
            if (hivStatusObs["negativeResultDate"]) {
                let dateObj = new Date(hivStatusObs["negativeResultDate"].val);
                jq("#negativeResultDate").val(formatDate(dateObj));
            }

            if (hivStatusObs["pitcAccepted"]) {
                jq('input[name="pitcAccepted"]').each(function() {
                    if (hivStatusObs["pitcAccepted"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="pitcAccepted"]').attr("data-id", hivStatusObs["pitcAccepted"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (hivStatusObs["pitcResults"]) {
                jq('input[name="pitcResults"]').each(function() {
                    if (hivStatusObs["pitcResults"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="pitcResults"]').attr("data-id", hivStatusObs["pitcResults"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (hivStatusObs["unknownResultDate"]) {
                let dateObj = new Date(hivStatusObs["unknownResultDate"].val);
                jq("#unknownResultDate").val(formatDate(dateObj));
            }

            if (hivStatusObs["onArt"]) {
                jq('input[name="onArt"]').each(function() {
                    if (hivStatusObs["onArt"].uuid === jq(this).attr("value")) {
                        jq(this).prop('checked', true);
                        jq('input[name="onArt"]').attr("data-id", hivStatusObs["onArt"]?.id)
                        jq(this).trigger("change");
                    }
                });
            }

            if (hivStatusObs["artSite"]) {
                jq("#artSite").val(hivStatusObs["artSite"])
            }

            if (hivStatusObs["referredTo"]) {
                jq("#referredTo").val(hivStatusObs["referredTo"])
            }

        });

        jq('#saveHivStatus').on('click', function(e) {
            e.preventDefault();
            var formData = {};
            jq("#hivStatusForm").find('textarea, input:not(:radio)').each(function() {
                var thisVal = jq(this);
                if (thisVal.is(':visible')) {
                    formData[thisVal.attr('name')] =
                        {
                            "id": thisVal.attr("data-id") || '',
                            "val" : thisVal.val()
                        };
                } else {
                    formData[thisVal.attr('name')] =
                        {
                            "id": thisVal.attr("data-id") || '',
                            "val" : ''
                        };// Default value for hidden inputs
                }
            });

            // Handle radio buttons separately to manage unchecked groups
            jq("#hivStatusForm").find('input:radio').each(function() {
                var thisVal = jq(this);
                var name = thisVal.attr('name');
                if (thisVal.is(':visible')) {
                    if (thisVal.is(':checked')) {
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : thisVal.val()
                            };
                    } else if (!formData.hasOwnProperty(name)) {
                        // Set default empty string if no radio button is checked in its group
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : ''
                            };
                    }
                } else {
                    if (!formData.hasOwnProperty(name)) {
                        formData[name] =
                            {
                                "id": thisVal.attr("data-id") || '',
                                "val" : ''
                            };// Default value for hidden radio groups
                    }
                }
            });

            formData["lastHivStatusEncounter"] = lastHivStatusEncounter;
            let payload = JSON.stringify(formData);
            jq.post('${ ui.actionLink("botswanaemr", "hivStatusBar", "saveHivStatus") }',
                {returnFormat: 'json', patientId: ${patient.patientId}, data: payload, visitId: ${visit.id}},
                function (data) {
                    jq().toastmessage('showNoticeToast', "The patients HIV status info has been saved");
                    location.reload();
                }, 'json')
                .fail(function() {
                    console.log("error");
                })



        });

    });
</script>