<div class="col-md-12">
    <div id="visitSummaryCard" class="container-fluid">
        <h2 class="text-primary text-center">${facility}</h2>
        <hr class="divider pt-1 bg-primary"/>
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-primary text-center">Patient's Details</h5>
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li>
                        <div class="col pl-0"><strong>Patient Name:</strong> ${patientNames}</div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-unstyled">
                    <li>
                        <div class="col p-0"><strong>Age:</strong> ${patientAge}</div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <div class="col p-0"><strong>Date:</strong> ${visitDate}</div>
            </div>
        </div>
         <hr class="divider bg-primary"/>
        <div class="row" ng-app="triageDataApp" ng-controller="TriageDataController">
            <div class="col-md-12">
                ${ ui.includeFragment("botswanaemr", "registrationInformation", [ patientId: patient.patient.uuid ]) }
            </div>
        </div>
        <div class="col-md-3">
            <ul class="list-unstyled mt-2">
                <li>${creatorNames}</li>
                <li>Health Practitioner's Signature:</li>
            </ul>
        </div>
        <div class="col-md-9 pl-0 pr-0">
            <ul class="list-unstyled mt-2">
                <li class="mt-5">
                    <hr style="border-bottom: 1px dotted #eee;"/>
                </li>
            </ul>
        </div>
    </div>
</div>