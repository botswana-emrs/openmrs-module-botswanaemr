<script type="text/javascript">

    jQuery(function () {

        function formatData(searchResult) {
            let data = {
                children: [],
                dateRanges: searchResult.dateRanges
            };

            searchResult.children.forEach((child) => {
                let childData = {
                    number: "",
                    temperature: [],
                    weight: [],
                    pulse: [],
                    respiration: []
                };


                if (child.number) {
                    childData.number = child.number;
                }

                if (child.temperature) {
                    child.temperature.forEach((item) => {
                        childData.temperature.push([item.yValue, item.xValue]);
                    });
                }
                if (child.weight) {
                    child.weight.forEach((item) => {
                        childData.weight.push([item.yValue, item.xValue]);
                    });
                }
                if (child.pulse) {
                    child.pulse.forEach((item) => {
                        childData.pulse.push([item.yValue, item.xValue]);
                    });
                }

                if (child.respiration) {
                    child.respiration.forEach((item) => {
                        childData.respiration.push([item.yValue, item.xValue]);
                    });
                }

                data.children.push(childData);
            });

            return data;
        }

        function getPatientVitals(patientId) {
            let searchResult;
            jq.ajax({
                type: "GET",
                url: '${ui.actionLink("botswanaemr","vitalsPueperium","getMotherAndChildrenVitals")}',
                dataType: "json",
                global: false,
                async: true,
                data: {
                    patientId: patientId,
                },
                success: function (data) {
                    searchResult = data;
                }
            });
            return searchResult;
        }

        const mappedData = formatData(getPatientVitals('${patient.id}'));

        function createSeriesData(childrenData) {
            var seriesData = [];

            for (let i = 0; i < childrenData.length; i++) {
                const child = childrenData[i];
                const childNumber = child.number;
                seriesData.push({
                    name: "Pulse (Mother)",
                    data: child.pulse
                });
                seriesData.push({
                    name: "Temperature (Child " + childNumber + ")",
                    data: child.temperature
                });
                seriesData.push({
                    name: "Weight (Child " + childNumber + ")",
                    data: child.weight
                });
                seriesData.push({
                    name: "Respiration",
                    data: child.respiration
                });
            }

            return seriesData;
        }

        let chart;
        chart = new Highcharts.chart({
            chart: {
                renderTo: 'vitalsPueperium',
                backgroundColor: null,
                type: 'spline'
            },
            credits: {
                enabled: false
            },
            rangeSelector: {
                verticalAlign: 'top',
                x: 0,
                y: 0
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'Day of Puerperium'
                },
            },
            yAxis: {
                title: {
                    text: 'Level'
                },
                tickInterval: 50
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            series: createSeriesData(mappedData.children)
        });

        function updateChartData(data) {
            const seriesData = createSeriesData(data.children);
            console.log("seriesData", seriesData);

            chart.update({
                xAxis: {
                    title: {
                        text: 'Day of Puerperium'
                    },
                    categories: data["dateRanges"]
                },
                series: seriesData
            });
        }

        updateChartData(mappedData);


    });
</script>

<div class="col-12">
    <div class="row">
        <div class="col-5 zero-padding text-left">
            <h5>${ui.message('Vitals Pueperium')}</h5>
        </div>
    </div>

    <div id="vitalsPueperium" style="min-width: 100%; margin: 0; padding-bottom: 5px;"></div>
</div>