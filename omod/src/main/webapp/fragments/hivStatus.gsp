<script>
    jQuery(function () {
    });
</script>

<div class="row">
    <div class="col-12">
        <div class="info-section">
            <div class="info-header">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                        <h5>HIV Status</h5>
                    </div>
                    <div class="col-6 zero-padding text-left">
                        <h5>HIV Test Link</h5>
                    </div>
                </div>
            </div>
            <div class="info-body">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                       <span> ${hivStatus} </span>
                    </div>
                    <div class="col-6 zero-padding text-left">
                    <span> <a href="${ui.pageLink('botswanaemr', 'hts/htsClientProfile', [patientId: patient.id, returnUrl: returnUrl])}">Click for HIV test</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
