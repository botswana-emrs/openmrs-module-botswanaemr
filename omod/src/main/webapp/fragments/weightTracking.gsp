<% if (monthWeights) { %>
<div class="card">
    <div class="row" style="margin: 8px;">
        <div id="weigh-tracking" class="col px-0">
            <table>
                <tr>
                    <th>Month</th>
                    <th>Weight (Kgs)</th>
                </tr>
                <% monthWeights.each { month -> %>
                <tr>
                    <td class="text-start">${month.monthYear}</td>
                    <td><strong>${month.weightAtEndOfMonth}</strong></td>
                </tr>
                <% } %>
            </table>
        </div>
    </div>
</div>
<% } %>
