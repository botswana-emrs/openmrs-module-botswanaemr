<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script>
    let visitUuid = "${visit.uuid}";
    async function loadComplaintsData(visitUuid) {

        const baseUrl = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/visit';
        const customParams = "custom:(uuid,visitId,visitType,patient,encounters:(uuid,encounterType,voided,orders:(uuid,orderType,voided,concept:(uuid,set,name)),obs:(uuid,comment,value,concept,obsDatetime,groupMembers:(uuid,comment,concept:(uuid,name),obsDatetime,value:(uuid,name),groupMembers:(uuid,concept:(uuid,name),value:(uuid,name),groupMembers:(uuid,concept:(uuid,name),value:(uuid,name)))))))"
        const requestUrl = baseUrl+"/"+visitUuid+"?v=" +encodeURIComponent(customParams);

        try {
            const response = await fetch(requestUrl);
            if (!response.ok) throw new Error(response.status);

            const data = await response.json();
            const complaints = extractComplaints(data);
            renderComplaints(complaints);
        } catch (error) {
            console.error("Error loading complaints data:", error);
        }
    }

    function extractComplaints(data) {
        const complaints = [];

        if (data.encounters && Array.isArray(data.encounters)) {
            data.encounters.forEach(encounter => {
                if (encounter.obs && Array.isArray(encounter.obs)) {
                    encounter.obs.forEach(obs => {
                        const topLevelUuid = obs.uuid;

                        if (obs.groupMembers && Array.isArray(obs.groupMembers) && obs.groupMembers.length > 0) {
                            const firstGroupMember = obs.groupMembers[0];
                            const groupMemberValue = firstGroupMember.value;
                            const complaintData = {
                                uuid: topLevelUuid,
                                value: groupMemberValue || null,
                                obsDatetime: obs.obsDatetime
                            };

                            complaints.push(complaintData);
                        }
                    });
                }
            });
        }
        return complaints;
    }

    function renderComplaints(complaints) {
        const complaintsListContainer = document.createElement('ul');
        complaintsListContainer.className = 'complaints-list';
        
        complaints.forEach(complaint => {
            if (!isNaN(parseFloat(complaint.value)) && isFinite(complaint.value)) {
                return;
            }
            const listItem = `
                <li>
                    <div class="row pb-3">
                        <div class="col-9 zero-padding" id="\${complaint.uuid}">
                            \${complaint.value}
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-sm btn-dark bg-dark right edit-complaint-button" data-id="\${complaint.uuid}">
                                Edit
                            </button>
                        </div>
                    </div>
                </li>
            `;

            complaintsListContainer.innerHTML += listItem;
        });

        if (complaints.length === 0) {
            complaintsListContainer.innerHTML = `
                <div class="row no-data-section text-center">
                    <img src="/openmrs/ms/uiframework/resource/botswanaemr/images/no-task.svg" alt="no data"/>
                    <p class="text-center">No data captured</p>
                </div>
            `;
        }

        const container = document.querySelector('.info-body');
        container.innerHTML = '';
        container.appendChild(complaintsListContainer);
    }

    jq(function() {
        jq("#addComplaintsForm").submit(function(e){
            var params = {
                'patientId': ${patient.id},
                'complaint': jq('#complaint').val(),
                'visitId': ${activeVisit.id}
            };
            
            showLoadingOverlay();

            jq.getJSON('${ ui.actionLink("botswanaemr", "patientComplaint", "addPatientComplaints")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Patient complaints captured successfully");
                    jq('#complaint').val('');
                    jQuery('#addComplaintModal').modal('hide');
                    loadComplaintsData(visitUuid);

                    hideLoadingOverlay();
                })
        });

        jq(document).on('click', '.edit-complaint-button', function(e) {
            e.preventDefault();
            let id = jq(this).attr('data-id');
            let val = jq("#" + id).html();

            jq("#editComplaint").val(val);
            jq("#lastComplaintObsUuid").val(id);

            jQuery('#editComplaintModal').modal('show');
        });

        jq("#editComplaintForm").submit(function(e){
            e.preventDefault();
            var params = {
                'editComplaint': jq('#editComplaint').val(),
                'lastComplaintObsUuid': jq('#lastComplaintObsUuid').val()
            };

            showLoadingOverlay();

            jq.getJSON('${ ui.actionLink("botswanaemr", "patientComplaint", "editPatientComplaint")}', params)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "Patient complaint edited successfully");
                    jQuery('#editComplaintModal').modal('hide');
                    loadComplaintsData(visitUuid);
                    hideLoadingOverlay();
                });
        });

        loadComplaintsData(visitUuid);
    });
</script>

<div class="row">
    <% if (visit == null) { %>
    <div class="col col-sm-12 col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!
            </strong>
        </div>
    </div>
    <% } else { %>
    <div class="col-12">
        <div class="info-section conditions">
            <div class="info-header">
                <div class="row">
                    <div class="col-8 zero-padding text-left">
                        <h5>${ui.message('Patient Complaints')}</h5>
                    </div>
                </div>
            </div>

            <div class="info-body complaints-list"></div>

            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#addComplaintModal">+ Add Patient Complaint</button>
            </div>

        </div>
    </div>
    <% } %>
</div>
<!-- Add complaint Nodal -->
<div class="modal fade" id="addComplaintModal" tabindex="-1"
     data-controls-modal="addComplaintModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="addComplaintModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="addComplaintModalTitle">Add Patient Complaints</h4>
                <button type="button" id="addComplaintBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="addComplaintsForm">
                    <div class="form-group">
                        <label for="complaint">Complaint</label>
                        <textarea class="form-control" rows="5"  id="complaint" name="complaint"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#addComplaintModal").appendTo("body");
    });
</script>

<!-- Edit complaint Nodal -->
<div class="modal fade" id="editComplaintModal" tabindex="-1"
     data-controls-modal="editComplaintModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editComplaintModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editComplaintModalTitle">Edit Patient complaint</h4>
                <button type="button" id="editComplaintsBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="editComplaintForm">
                    <input type="hidden" id="lastComplaintObsUuid" name="lastComplaintObsUuid" />
                    <div class="form-group">
                        <label for="editComplaint">Complaint</label>
                        <textarea class="form-control" rows="5"  id="editComplaint" name="editComplaint"></textarea>
                    </div>
                    <div class="modal-footer pr-0">
                        <button type="button" class="btn btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#editComplaintModal").appendTo("body");
    });
</script>

