<%
    ui.includeCss("botswanaemr", "htmlFormEntry.css")
    ui.includeCss("botswanaemr", "jquery-ui-1.13.1.min.css")

    ui.includeJavascript("uicommons", "handlebars/handlebars.min.js", Integer.MAX_VALUE - 1);
    ui.includeJavascript("htmlformentryui", "dwr-util.js")
    ui.includeJavascript("botswanaemr", "htmlForm.js")
    ui.includeJavascript("botswanaemr", "htmlFormEntry.js")
    ui.includeJavascript("botswanaemr", "jquery-3.6.0.js")
    ui.includeJavascript("botswanaemr", "jquery-ui-1.13.1.min.js")
    ui.includeJavascript("uicommons", "emr.js")
    ui.includeJavascript("uicommons", "moment.js")
%>

<script type="text/javascript">
    function submitHtmlForm() {
        htmlForm.submitHtmlForm();
        return false;
    }
    function showDiv(id) {
        htmlForm.showDiv(id);
    }
    function hideDiv(id) {
        htmlForm.hideDiv(id);
    }
    function getValueIfLegal(idAndProperty) {
        htmlForm.getValueIfLegal(idAndProperty);
    }
    function loginThenSubmitHtmlForm() {
        htmlForm.loginThenSubmitHtmlForm();
    }
    var beforeSubmit = htmlForm.getBeforeSubmit();
    var beforeValidation = htmlForm.getBeforeValidation();
    var propertyAccessorInfo = htmlForm.getPropertyAccessorInfo();
    <% if (command.returnUrl) { %>
        htmlForm.setReturnUrl('${ command.returnUrl }');
    <% } %>
    jq(function() {
        <% if (visit) { %>
            <% if (command.context.mode.toString().equals('ENTER') && !visit.isOpen()) { %>
                <% if (ui.convertTimezones()) { %>
                    htmlForm.adjustEncounterDatetimeWithTimezone('${visitStartDatetime}');
                <% } else {%>
                    htmlForm.setEncounterDate('${ visitStartDatetime}');
                <% } %>
            <% } %>
            htmlForm.setEncounterStartDateRange('${visitStartDatetime}', ${ui.convertTimezones()});
            htmlForm.setEncounterStopDateRange('${visitStopDatetime}', ${ui.convertTimezones()});
        <% } else { %>
            <% if (ui.convertTimezones()) { %>
                htmlForm.adjustEncounterDatetimeWithTimezone('${currentDate}');
            <% } else { %>
                htmlForm.setEncounterDate('${(currentDate)}');
            <% } %>
            htmlForm.setEncounterStopDateRange('${(currentDate)}' , ${ui.convertTimezones()});
        <% } %>
        htmlForm.disableEncounterDateManualEntry();
    });
	jq(document).ready(function() {
		jQuery.each(jq("htmlform").find('input'), function(){
		    jq(this).bind('keypress', function(e){
		       if (e.keyCode == 13) {
		       		if (!jq(this).hasClass("submitButton")) {
		       			e.preventDefault();
		       		}
		       }
		    });
		});
		htmlForm.initialize();
    });
</script>

<script type="text/javascript">
    jq(function(){
        jq( "#encounterDate" ).datepicker();
    });
</script>

<div id="${ config.id }" <% if (config.style) { %>style="${ config.style }"<% } %> <% if (config.cssClass) { %>class="${config.cssClass}"<% } %>>
    <span class="error" style="display: none" id="general-form-error"></span>
    <form autocomplete="off" id="htmlform" method="post" action="${ ui.actionLink("botswanaemr", "enterHtmlForm", "submit") }" onSubmit="submitHtmlForm(); return false;">
        <input type="hidden" name="personId" value="${ command.patient.personId }"/>
        <input type="hidden" name="htmlFormId" value="${ command.htmlFormId }"/>
        <input type="hidden" name="createVisit" value="${ createVisit }"/>
        <input type="hidden" name="formModifiedTimestamp" value="${ command.formModifiedTimestamp }"/>
        <input type="hidden" name="encounterModifiedTimestamp" value="${ command.encounterModifiedTimestamp }"/>
        <% if (command.encounter) { %>
        <input type="hidden" name="encounterId" value="${ command.encounter.encounterId }"/>
        <% } %>
        <% if (visit) { %>
        <input type="hidden" name="visitId" value="${ visit.visitId }"/>
        <% } %>
        <% if (command.returnUrl) { %>
            <input type="hidden" name="returnUrl" value="${ command.returnUrl }"/>
        <% } %>
        <input type="hidden" name="closeAfterSubmission" value="${ config.closeAfterSubmission }"/>

        ${ command.htmlToDisplay }

        <div id="passwordPopup" style="position: absolute; z-axis: 1; bottom: 25px; background-color: #ffff00; border: 2px black solid; display: none; padding: 10px">
            <center>
                <table>
                    <tr>
                        <td colspan="2"><b>${ ui.message("htmlformentry.loginAgainMessage") }</b></td>
                    </tr>
                    <tr>
                        <td align="right"><b>${ ui.message("coreapps.user.username") }:</b></td>
                        <td><input type="text" id="passwordPopupUsername"/></td>
                    </tr>
                    <tr>
                        <td align="right"><b>${ ui.message("coreapps.user.password") }:</b></td>
                        <td><input type="password" id="passwordPopupPassword"/></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="button" value="Submit" onClick="loginThenSubmitHtmlForm()"/></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</div>

<% if (command.fieldAccessorJavascript) { %>
<script type="text/javascript">
    ${ command.fieldAccessorJavascript }
</script>
<% } %>