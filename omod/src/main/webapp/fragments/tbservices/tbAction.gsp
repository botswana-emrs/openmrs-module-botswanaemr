<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'tbservices/tbServicesDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ["returnUrl": tbDashboardReturnUrl, "action": "self"])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'programPatientManagement', Collections.singletonMap("programId", "938b5435-dc0e-4d4e-bb6f-79befd636d8f"))}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">TB Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'tbservices/tbReports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-folder fa-1x"></i>
            </div>
            <div class="nav_label">TB Reports</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'art/regimenManagement',[regimenCategory:'75b76eae-7d8e-451b-871f-7718579a505f',returnUrl:'/'+contextPath+'/botswanaemr/tbservices/tbServicesDashboard'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-pencil-square fa-1x"></i>
            </div>
            <div class="nav_label">Regimen Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'adverseEvents')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-heartbeat fa-1x"></i>
            </div>
            <div class="nav_label">ADR</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

