<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#screenedBy').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                let user = jq('#screenedBy').find(":selected").text();
                if (user === '${currentUser}') {
                    user = "You";
                }

                matchingOptions.push(screenedBy === user);
            }

            if (jq('#treatmentPhase').find(":selected").val() !== '') {
                let treatmentPhase = aData[6].trim();
                matchingOptions.push(treatmentPhase === jq('#treatmentPhase').find(":selected").text());
            }

            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: '${ui.actionLink("botswanaemr","tbservices/tbDotPatientPool","getDotPatientsInProgram")}',
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                let returnUrl = urlUtils.encodeUrl('${ui.pageLink("botswanaemr", "tbservices/tbServicesDashboard")}');
                var table = jQuery('#tbDotPatientsListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'identifier'},
                        {'data': 'name'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'registeredBy'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status === "COMPLETED"){
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Completed</span>';
                                    } else {
                                        return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': 'treatmentPhase'},
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(data.isTodayDot){
                                        return '<a href="/${ui.contextPath()}/botswanaemr/programs/programs.page?patientId='+row.patientUuid+'&ensureActiveVisit=true&programId='+row.programId+'" class="text-primary float-left pl-3" id="screen">Treatment profile</a> | ' +
                                            '<a href="/${ui.contextPath()}/botswanaemr/editEncounter.page?patientId='+row.patientId+'&encounterId='+data.encounterIdentifier+'&returnUrl='+returnUrl+'"  class="text-primary float-left pl-3" id="view">View DOT</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/programs/programs.page?patientId='+row.patientUuid+'&ensureActiveVisit=true&programId='+row.programId+'" class="text-primary float-left pl-3" id="screen">Treat</a>'
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#tbDotPatientPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                            table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<style>
.dataTables_wrapper {
    min-height: fit-content !important;
}
</style>

<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pr-0 pb-5">
        <label>Screened by</label>
        <select id="screenedBy"
                class="form-control"
                placeholder="${ui.message('Select nurse ')}">
            <option value="">Select One</option>
            <% providerList.each { provider-> %>
            <% if (provider != null && provider?.person != null && provider?.person?.personName != null) { %>
            <option value="<%=provider.uuid %>"><%=provider.person.personName %></option>
            <% } %>
            <% } %>
        </select>
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 pr-0 pb-5">
        <label>Treatment phase</label>
        <select id="treatmentPhase"
                class="form-control"
                placeholder="${ui.message('Select phase')}">
            <option value="">Select One</option>
            <% phaseAnswers.each { %>
            <option value="${it.getAnswerConcept().getUuid()}" >${it.getAnswerConcept().getName().getName()}</option>
            <% } %>
        </select>
    </div>

    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
            ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
            ${ui.message("Reset")}
        </button>
    </div>
</div>
<div class="row">

</div>
<table id="tbDotPatientsListTable"
       class="table table-striped table-sm table-bordered">
    <thead>
    <tr>
        <th>Patient ID</th>
        <th>Name</th>
        <th>Sex</th>
        <th>Age</th>
        <th>Screened By</th>
        <th>Status</th>
        <th>Treatment Phase</th>
        <th>Actions</th>
    </tr>
    </thead>
</table>
