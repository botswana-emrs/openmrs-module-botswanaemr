<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script>

    const patientId = '${patient.patient.uuid}';
    const visitId = '${visit.uuid}';

    jQuery(function () {
        jQuery("#editLifestyleModal").appendTo("body");
        jq('#saveLifeStyle').prop('disabled', false);
        getLifeStyleData();
    });

    function getLifeStyleData() {
        jq.getJSON('${ui.actionLink("botswanaemr", "lifeStyle", "getLifeStylesData")}',
        {'patientId': patientId,'visitId': visitId}
        ).done(function (data) {
            renderLifestyleData(data);
        }).fail(function (jqXHR) {
            console.error('Error loading lifestyle data');
        });
    }

    function renderLifestyleData(data) {
        const infoBody = document.querySelector(".lifestyle-info ul");
        infoBody.innerHTML = ""; 

        if (data.length === 0) {
            document.querySelector(".no-data-section").style.display = "block";
            return;
        } else {
            document.querySelector(".no-data-section").style.display = "none";
        }

        data.forEach(lifestyle => {
            const lifestyleValue = lifestyle.lifestyle.valueText || "-";
            const commentValue = lifestyle.comment ? lifestyle.comment.valueText : "-";

            const lifestyleHtml = `
                <li>
                    <div class="row">
                        <div class="col-6 zero-padding">
                            <span class="p normal-text">\${lifestyleValue}</span>
                        </div>
                        <div class="col-6 zero-padding">
                            <span class="p normal-text">\${commentValue}</span>
                        </div>
                    </div>
                </li>
            `;

            infoBody.insertAdjacentHTML("beforeend", lifestyleHtml);
        });
    }
</script>

<div class="row">
    <% if (visit == null) { %>
    <div class="col col-sm-12 col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!
            </strong>
        </div>
    </div>
    <% } else { %>
    <div class="col-12">
        <div class="info-section conditions">
            <div class="info-header">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                        <h5>${ui.message('Lifestyle Habits')}</h5>
                    </div>

                    <div class="col-6 zero-padding text-left">
                        <h5>${ui.message('Notes')}</h5>
                    </div>
                </div>
            </div>

            <div class="info-body lifestyle-info">
                <ul></ul>

                <div ng-show="lifestyleObservations.length == 0" class="row no-data-section text-center">
                    <img class=""
                         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

                    <p class="text-center">No data captured</p>
                </div>
            </div>

            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editLifestyleModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Lifestyle Habit</button>
            </div>

        </div>
    </div>
    <% } %>
</div>

<!-- Nodal -->
<div class="modal fade" id="editLifestyleModal" tabindex="-1"
     data-controls-modal="editLifestyleModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editLifestyleModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editLifestyleModalTitle">Edit Lifestyle</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "consultation/editLifeStyle", [patientId: patient.patient.uuid, visit: visit])}
                </div>
            </div>
        </div>
    </div>
</div>
