<script type="text/javascript">
    var choices;
    jq(document).ready(function () {
        var jq = jQuery;
        jq('#list-location-appointment-bookings').DataTable({
            searchPanes: true,
            searching: true,
            "pagingType": 'simple_numbers',
            'dom': 'flrtip',
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        choices = new Choices('#userRole', {
            removeItemButton: true,
            searchResultLimit: 5,
            renderChoiceLimit: 5,
        });

    });
</script>

<div class="card">
    <table id="list-location-appointment-bookings" class="table table-sm table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Facility</th>
                <th>${ui.message("SystemInfo.status")}</th>
                <th>Registered on</th>
                <th>${ui.message("general.action")}</th>
            </tr>
        </thead>
        <tbody>
        <% locations.each { %>
            <tr>
                <td>${it.facilityName}</td>
                <td>
                    <%
                        String status = (it.status != null) ? "Active" : "Disabled";
                        String iconClass = (it.status != null) ? "active" : "disabled";
                    %>
                    <i class="status <%= iconClass %>"></i>${status}
                </td>
                <td>${ui.format(it.dateCreated)}</td>
                <td>
                  <a href="${ui.pageLink('botswanaemr', 'appointmentBooking', ['facilityId': it.facilityId])}" id="view" class="text-primary">View</a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>
