<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    function getVisitUrl(patientId) {
        visit.returnUrl = "${returnUrl}";
        visit.showQuickVisitCreationDialog(patientId);
    }
</script>

<% if(activeVisit) {%>
<span class="btn btn-sm btn-primary">
    <i class="fa fa-h-square"></i>
    Active Visit
    <i class="fa fa fa-clock-o"></i>
    ${activeVisit.startDatetime}
    <a href="javascript:visit.showEndVisitDialog(${activeVisit.id})">| End Visit</a>
</span>
<% } else {%>
<span>
    <a href="javascript:getVisitUrl(${patient.id})" class="btn btn-sm btn-primary text-white">
        <i class="icon-check-in small"></i>${ ui.message("Start Visit") }
    </a>
</span>
<% }%>
