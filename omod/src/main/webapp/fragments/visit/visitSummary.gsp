<div class="col-md-12">
    <div id="visitSummaryCard" class="container-fluid">
        <h2 class="text-primary text-center">${facility}</h2>
        <hr class="divider pt-1 bg-primary"/>
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-primary">Patient's Details</h5>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <li>
                        <div class="col pl-0">Patient Name: ${patientNames}</div>
                        <div class="col pl-0"> Address: [District, City, Ward]
                            <% if(patientAddresses != null) {%>
                                <div class="row">
                                    <div class="col-12 p-0">
                                        <% patientAddresses.each { %>
                                            <span class="text-dark">${it.value ?: ""},</span>
                                        <% } %>
                                    </div>
                                </div>
                            <% } %>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list-unstyled">
                    <li>
                        <div class="col p-0">Age: ${patientAge}</div>
                        <div class="col p-0">Date: ${visitDate}</div>
                        <div class="col p-0">Sex: ${patientGender}</div>
                    </li>
                </ul>
            </div>
        </div>

        <hr class="divider bg-primary"/>

        <% if(allergies) {%>
        <div class="row mt-3">
            <div class="col-md-12">
                ${allergies != null ? "<h5 class='text-primary'>Patient's Allergies</h5>" : "&nbsp;"}
            </div>
            <div class="col-md-12">
                <% allergies.each { %>
                <ul class="list-unstyled">
                    <li>
                        <div class="row">
                            <% if (it.allergen.nonCodedAllergen) { %>
                            <div class="col pl-0 text-danger font-weight-bold">
                                Allergy: ${it.allergen.nonCodedAllergen}
                            </div>
                            <% } else if(it.allergen.codedAllergen) { %>
                            <div class="col pl-0 text-danger font-weight-bold">
                                Allergy: ${it.allergen.codedAllergen?.name}
                            </div>
                            <% } %>
                            <div class="col pl-0 text-danger font-weight-bold">Severity: ${it.severity?.name ?: "Unknown"}</div>
                            <div class="col pl-0 text-danger font-weight-bold">Comments: ${it.comments ?: ""}</div>
                        </div>
                    </li>
                    <hr/>
                </ul>
                <% } %>
            </div>
        </div>
        <% } %>

        <% if (hasConsultationToBeEdited) {%>
        <div class="row mt-3">
            <div class="col-md-12">
                ${hasConsultationToBeEdited ? "<h5 class='text-primary'>Patient's Vitals</h5>" : "&nbsp;"}
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <li>
                        <div class="row p-0">
                            <div class="col p-0">Temperature(C): ${temperature}</div>
                            <div class="col p-0">Weight(Kg): ${weight}</div>
                            <div class="col p-0">Height(cm): ${height}</div>
                            <div class="col p-0">Blood pressure(mmHg): ${systolicBp}&nbsp;/&nbsp;${diastolicBp}</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <li>
                        <div class="row p-0">
                            <div class="col p-0">BMI(Kg/M2): ${bmi}</div>
                            <div class="col p-0">BSA(M2): ${bsa}</div>
                            <div class="col p-0">Pulse(b/m): ${pRate}</div>
                            <div class="col p-0">Respiratory Rate(b/m): ${rRate}</div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <li>
                        <div class="row p-0">
                            <div class="col p-0">Head Circumference(cm): ${headCircumference}</div>
                            <div class="col p-0">Glucose Level(mm/gl) ${glucoseLevel}</div>
                            <div class="col p-0">Oxygen Saturation: ${oxygenSaturation}</div>
                            <% def gender = patient.patient.person.gender %>
                            <% if (gender.equals('F')) {%>
                            <div class="col p-0">Last Menstrual Period: ${lmp}</div>
                            <% } else { %>
                            <div class="col p-0">&nbsp;</div>
                            <% } %>
                        </div>
                    </li>
                    <hr/>
                </ul>
            </div>
        </div>
        <% } %>
        
        <div class="row">
            <div class="col">
                <% if (!conditions.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Patient's Conditions</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% conditions.each { %>
                            <li>
                                <div class="row">
                                    <% if (it.condition.coded) { %>
                                    Condition: <div class="col-6 pl-0">&nbsp;${it.condition.coded ?: it.condition.coded.name}</div>
                                    <% } else { %>
                                    Condition: <div class="col-6 pl-0">&nbsp;${it.condition.nonCoded ?: it.condition.nonCoded.name}</div>
                                    <% } %>
                                    Status: <div class="col pl-0">&nbsp;${it.clinicalStatus}</div>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if (!complaints.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Patient's Complaints</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% complaints.each { complaint -> %>
                            <li>
                                <div class="row">
                                    <% if (complaint.complaint?.valueText) { %>
                                    <div class="col pl-0">
                                        ${complaint.complaint?.valueText}
                                    </div>
                                    <% } %>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if (!lifeStyles.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Patient's Lifestyle Habits</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% lifeStyles.each { lifestyleObject -> %>
                            <li>
                                <div class="row">
                                    <% if (lifestyleObject.lifestyle?.valueText) { %>
                                    <div class="col pl-0">
                                        Habit: ${lifestyleObject.lifestyle?.valueText}
                                    </div>
                                    <% } %>

                                    <% if (lifestyleObject.comment && lifestyleObject.comment?.valueText) { %>
                                    <div class="col pl-0">
                                        Notes: ${lifestyleObject?.comment?.valueText}
                                    </div>
                                    <% } %>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if (physicalExaminations.size() > 0 ) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Physical Examination</h5>
                    </div>

                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% physicalExaminations.each { physicalExam -> %>
                                <li>
                                    <div class="row">
                                        <% physicalExam.each { %>
                                            <% if (it.key != 'groupingObsId') {%>
                                                <div class="text-container pr-3"><b>${it.key.capitalize()}:</b> ${it.value.valueText}  </div>
                                            <% } %>
                                        <% } %>
                                    </div>
                                </li>
                                <hr/>
                            <% } %>
                        </ul>
                    </div>

                </div>
                <% } %>
                <% if (!labTests.isEmpty()) { %>
                <div class="row mt-3">
                    <div class="col-md-12">
                        ${!labTests.isEmpty() ? "<h5 class='text-primary'>Lab Tests & Results</h5>" : "&nbsp;"}
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% labTests.each { %>
                            <li>
                                <div class="row">
                                    <div class="col-4 pl-0">Test: ${it.testName}</div>
                                    <div class="col pl-0">Added on:&nbsp;${ui.format(it.dateAdded)}</div>
                                    <div class="col pl-0">Status:&nbsp;${it.status.toLowerCase()}</div>
                                    <div class="col pl-0">
                                        <% if(it.resultsList) {%>
                                        <h5 class="mb-3 mt-3 pl-3">Test Results</h5>
                                        <% it.resultsList.each {key, value -> %>
                                        <span class="text-primary pl-3">${key}:
                                                        <span class="text-muted">${value}</span>
                                                    </span>
                                        <br />
                                        <%}%>
                                        <%}%>
                                    </div>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if(!patientActivePrograms.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        ${!patientActivePrograms.isEmpty() ? "<h5 class='text-primary'>Patient's Programs</h5>" : "&nbsp;"}
                    </div>
                    <div class="col-md-12">
                        <% patientActivePrograms.each { %>
                        <ul class="list-unstyled">
                            <li>
                                <div class="row">
                                    <div class="col pl-0">Program: ${it?.name}</div>
                                    <div class="col pl-0">Date: ${it.dateCreated}</div>
                                </div>
                            </li>
                            <hr/>
                        </ul>
                        <% } %>
                    </div>
                </div>
                <% } %>
            </div>
            <div class="col">

                <% if (!symptoms.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Patient's Symptoms</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% symptoms.each { symptomObject -> %>
                            <% if (symptomObject.symptom) { %>
                            <li>
                                <div class="row">
                                    <% if (symptomObject.symptom.valueCoded) { %>
                                    <div class="col pl-0">
                                        Symptom: ${ui.encodeHtmlContent(ui.format(symptomObject.symptom.valueCoded))}
                                    </div>
                                    <% } else { %>
                                    <div class="col pl-0">
                                        Symptom: ${ui.encodeHtmlContent(ui.format(symptomObject.symptom?.valueText))}
                                    </div>
                                    <% } %>

                                    <% if (symptomObject.comment && symptomObject.comment?.valueText) { %>
                                    <div class="col pl-0">
                                        Notes: ${symptomObject?.comment?.valueText}
                                    </div>
                                    <% } %>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if (!diets.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Patient's Current Diet</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <% diets.each { dietObject -> %>
                            <% if (dietObject.diet) { %>
                            <li>
                                <div class="row">
                                    <% if (dietObject.diet?.valueText) { %>
                                    <div class="col pl-0">
                                        Diet: ${dietObject?.diet?.valueText}
                                    </div>
                                    <% } %>

                                    <% if (dietObject.comment?.valueText) { %>
                                    <div class="col pl-0">
                                        Notes: ${dietObject?.comment?.valueText}
                                    </div>
                                    <% } %>
                                </div>
                            </li>
                            <hr/>
                            <% } %>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if (dietaryPlan != "") {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Dietary Plan</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <li>
                                ${ui.format(dietaryPlan?.valueText)}
                            </li>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if(!nursingDiagnosis.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        ${!nursingDiagnosis.isEmpty() ? "<h5 class='text-primary'>Nursing Diagnosis</h5>" : "&nbsp;"}
                    </div>
                    <div class="col-md-12">
                        <% nursingDiagnosis.each { %>
                        <ul class="list-unstyled">
                            <li>
                                <div class="row">
                                    <div class="col pl-0">${it?.valueText}</div>
                                </div>
                            </li>
                        </ul>
                        <% } %>
                    </div>
                </div>
                <% } %>

                <% if (nonPharmacologicalPrescription != "") {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5 class="text-primary">Non Pharmacological Prescription</h5>
                    </div>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <li>
                                ${ui.format(nonPharmacologicalPrescription?.valueText)}
                            </li>
                        </ul>
                    </div>
                </div>
                <% } %>

                <% if(!doctorsNotes.isEmpty()) {%>
                <div class="row mt-3">
                    <div class="col-md-12">
                        ${!doctorsNotes.isEmpty() ? "<h5 class='text-primary'>Doctor's Notes</h5>" : "&nbsp;"}
                    </div>
                    <div class="col-md-12">
                        <% doctorsNotes.each { %>
                        <ul class="list-unstyled">
                            <li>
                                <div class="row">
                                    <div class="col pl-0">${it?.valueText}</div>
                                </div>
                            </li>
                        </ul>
                        <% } %>
                    </div>
                </div>
                <% } %>

            </div>
        </div>

        <% if(!pastOperations.isEmpty()) {%>
        <div class="row mt-3">
            <div class="col-md-12">
                ${!pastOperations.isEmpty() ? "<h5 class='text-primary'>Patient's Past Operations</h5>" : "&nbsp;"}
            </div>
            <div class="col-md-12">
                <% pastOperations.each { %>
                <ul class="list-unstyled">
                    <li>
                        <div class="row">
                            <div class="col pl-0">Operation: ${it.pastOperation.valueCoded?.name}</div>
                            <div class="col pl-0">Year: ${it.year?.valueNumeric}</div>
                            <div class="col pl-0">Comments: ${it.comment?.valueText ?: ""}</div>
                        </div>
                    </li>
                    <hr/>
                </ul>
                <% } %>
            </div>
        </div>
        <% } %>
        <% if (hasDiagnoses) { %>
        <div class="row mt-3">
            <div class="col-md-12">
                ${hasDiagnoses ? "<h5 class='text-primary'>Patient's Diagnoses</h5>" : "&nbsp;"}
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <% if (confirmedDiagnoses) {%>
                    <% confirmedDiagnoses.each{ %>
                    <li>
                        <div class="row">
                            <div class="col pl-0">Diagnosis: ${it.diagnosis.specificName?.name ?: it.diagnosis.nonCoded}</div>
                            <div class="col pl-0">Date: ${it.dateCreated}</div>
                            <div class="col pl-0">Certainty: ${it.certainty}</div>
                        </div>
                    </li>
                    <hr/>
                    <% } %>
                    <% } %>

                    <% if (provisionalDiagnoses) {%>
                    <% provisionalDiagnoses.each{ %>
                    <li>
                        <div class="row">
                            <div class="col pl-0">Diagnosis: ${it.diagnosis.specificName?.name ?: it.diagnosis.nonCoded}</div>
                            <div class="col pl-0">Date: ${it.dateCreated}</div>
                            <div class="col pl-0">Certainty: ${it.certainty}</div>
                        </div>
                    </li>
                    <hr/>
                    <% } %>
                    <% } %>
                </ul>
            </div>
        </div>
        <% } %>

        <% if (referralOrder != "") {%>
        <div class="row mt-3">
            <div class="col-md-12">
                <h5 class="text-primary">Patient's Referrals</h5>
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <li>
                        <div class="row">
                            <div class="col pl-0">Department / Referred To: ${receivingDepartment?.valueText ?: ""}</div>
                            <div class="col pl-0">Date: ${referralOrder.dateCreated}</div>
                            <div class="col pl-0">Reason: ${referralOrder.orderReason?.name}</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <% } %>


        <% if (!patientPrescriptions.isEmpty()) { %>
        <div class="row mt-3">
            <div class="col-md-12">
                ${!patientPrescriptions.isEmpty() ? "<h5 class='text-primary'>Patient's Prescriptions</h5>" : "&nbsp;"}
            </div>
            <div class="col-md-12">
                <ul class="list-unstyled">
                    <% patientPrescriptions.each{ %>
                    <li>
                        <div class="row">
                            <div class="col pl-0">Drug: 
                            <% if (it.drug != null) { %>
                                ${it.drug?.displayName}
                            <% } else { %>
                                ${it?.drugNonCoded}
                            <% } %>
                            </div>
                            <div class="col pl-0">Duration: ${it.duration}&nbsp;${it.durationUnits?.displayString}</div>
                            <div class="col pl-0">Dose: ${it.dose}&nbsp;${it.doseUnits?.displayString}</div>
                            <div class="col pl-0">Route: ${it.route?.displayString}</div>
                            <div class="col pl-0">Frequency: ${it.frequency}</div>
                        </div>
                    </li>
                    <hr/>
                    <% } %>
                </ul>
            </div>
        </div>
        <% } %>

        <div class="row">
                <div class="col">
                    <% if(nextAppointmentStartDate != "") {%>
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <li>
                                <h5 class="text-primary">Next Appointment Date:</h5>
                            </li>
                            <li><u style="text-decoration:underline dotted">${nextAppointmentStartDate}</u></li>
                        </ul>
                    </div>
                    <% } %>
                </div>
                <div class="col">
                    <div class="col-md-3">
                        <ul class="list-unstyled mt-2">
                            <li>${creatorNames}</li>
                            <li>Health Practitioner's Signature:</li>
                        </ul>
                    </div>
                    <div class="col-md-9 pl-0 pr-0">
                    <ul class="list-unstyled mt-2">
                        <li class="mt-5">
                            <hr style="border-bottom: 1px dotted #eee;"/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
