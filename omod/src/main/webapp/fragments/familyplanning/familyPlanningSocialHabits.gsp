<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#socialHabitsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });

        jq("#${formUuid}").click(function () {
            windowUtils.displayAsPopup('${ui.pageLink("botswanaemr","familyplanning/socialHabits",[patientId: patient.uuid, visitId: visit.id])}','Social Habits');
        })
    });
</script>

<div class="row mt-3 mb-3">
    <div class="col-6 pl-0">
        <h5 class="text-primary">B. SOCIAL HABITS</h5>
    </div>

    <div class="col-6 pr-0">
        <button class="btn btn-sm btn-primary float-right" id="${formUuid}">
            <i class="fa fa-plus" aria-hidden="true"></i> Add Social Habits
        </button>
    </div>
</div>

<hr class="divider pt-1 mb-5 bg-primary"/>

<div class="table-responsive">
    <% if (socialHabitsEncounters.size() > 0) { %>
    <table id="socialHabitsTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Provider</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <% socialHabitsEncounters.each { %>
        <tr>
            <td>${it.id ?: ""}</td>
            <td>${ui.formatDatePretty(it.encounterDatetime ?: "")}</td>
            <td>${ui.format(it.creator ?: "")}</td>
            <td>
                <button id="viewSocialHabits${it.id}"
                        onclick="windowUtils.displayAsPopup('${ui.pageLink("botswanaemr","familyplanning/socialHabits",[patientId: it.patient.uuid, visitId: it.visit.id, encounterId: it.id])}','Social Habits');"
                        class="btn btn-primary btn-outline border-0 bg-transparent float-right text-primary"> View
                </button>
                <button id="viewActivity${it.id}" objectId="${it.id}" objectType="Encounter" title="SOCIAL HABITS"
                        class="btn btn-primary btn-outline border-0 bg-transparent float-right text-primary view-activity"> Activity Trail
                </button>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <% } else { %>
    <div class="alert alert-sm alert-warning">
        <h5 class="text-danger">No records found!</h5>
    </div>
    <% } %>
</div>
