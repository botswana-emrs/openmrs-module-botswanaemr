<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#abortionsMiscarriageStillBirthsHistoryTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });
    });
</script>

<div class="row mt-0 mb-3 pt-0 pl-0 pr-0">
    <div class="col-md-12 pl-0 pr-0">
        <h5 class="text-primary mt-3 mb-2">ABORTIONS/MISCARRIAGES/STILLBIRTHS DETAILS</h5>
        <hr class="divider pt-1 bg-primary"/>
        <table id="captureAbortionsMiscarriageStillBirthsHistoryTable"
               class="table table-striped table-sm table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>Section</th>
                <th>Year</th>
                <th>Yes/No</th>
                <th>Comments</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <select class="form-control" id="condition" required>
                        <option value="">Select Section</option>
                        <option value="">Section</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" id="year" required>
                        <option value="">Select Year</option>
                        <option value="">Year 1</option>
                        <option value="">Year 2</option>
                    </select>
                </td>
                <td>
                    <select class="form-control" id="status" required>
                        <option value="">Select Yes/No</option>
                        <option value="">Yes</option>
                        <option value="">No</option>
                    </select>
                </td>
                <td>
                    <input id="comments" name="comments" class="form-control" placeholder="Enter Comments"/>
                </td>
                <td>
                    <a href="#" class="text-danger text-center">Clear</a>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="6">
                    <button id="addInfoBtn" name="addInfoBtn" class="btn btn-primary btn-block">
                       Add Abortions / Miscarriages / Stillbirths details
                    </button>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>

<hr class="mt-2"/>

<div class="row">
    <div class="col-md-4 pl-0">
        <div class="input-group">
            <input type="text"
                   class="form-control"
                   id="searchPhrase"
                   name="searchPhrase" placeholder="Search by Section"/>
        </div>
    </div>
    <div class="col-md-4 pl-0">
        <div class="input-group">
            <select class="form-control" id="searchStatus" required>
                <option value="">Select Status</option>
                <option value="">Yes</option>
                <option value="">No</option>
            </select>
        </div>
    </div>
    <div class="col-md-2 pr-0">
        <button id="searchBtn" class="btn btn-primary btn-block">
            <i class="fa fa-search"></i> search
        </button>
    </div>
    <div class="col-md-2 pr-0">
        <button class="btn btn-secondary btn-outline float-right">
            <i class="fa fa-reply fa-1x"></i>
        </button>
    </div>
</div>

<div class="table-responsive">
    <table id="abortionsMiscarriageStillBirthsHistoryTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Section</th>
            <th>Year</th>
            <th>Yes/No</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Any abortions/miscarriages/stillbirths</td>
            <td>Year 1</td>
            <td>Yes</td>
            <td>This is a new issue</td>
        </tr>
        </tbody>
    </table>
</div>


