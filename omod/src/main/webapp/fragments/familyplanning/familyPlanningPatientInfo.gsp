<script type="text/javascript">
    var breadcrumbs = [
        { icon: "icon-home", link: '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/srh/srhDashboard.page'},
        { label: "Family Planning Card"}
    ];
</script>

<div class="row">
    <div class="col pl-0 pr-0">
        <div class="row pl-0">
            <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <div class="card mt-4">
                    <div class="card-body">
                        <h5 class="text-primary">PATIENTS BIO DATA</h5>
                        <hr class="divider pt-1 bg-primary"/>
                        <div class="row">
                            <div class="col pl-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                            <tr>
                                                <th>Identification Type:</th>
                                                <td class="text-left">${identifierType}</td>
                                            </tr>
                                            <tr>
                                                <th>ID number:</th>
                                                <td class="text-danger text-left">${identifierValue}</td>
                                            </tr>
                                            <tr>
                                                <th>First Name:</th>
                                                <td class="text-left">${patient.patient.person.personName.familyName}</td>
                                            </tr>
                                            <tr>
                                                <th>Middle Name:</th>
                                                <td class="text-left">${patient.patient.person.personName.middleName ?: ""}</td>
                                            </tr>
                                            <tr>
                                                <th>Last Name:</th>
                                                <td class="text-left">${patient.patient.person.personName.givenName}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col pr-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                            <tr>
                                                <th>Sex:</th>
                                                <td class="text-left">
                                                    <% def gen = patient.patient.person.gender %>
                                                        <% if(gen.equals('M')) { %>
                                                            Male
                                                        <% } else if(gen.equals('F')) { %>
                                                            Female
                                                        <% } else {%>
                                                            Other
                                                    <% } %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Date of Birth:</th>
                                                <td class="text-left">${patient.patient.person.birthdate.format("dd-MMM-yyyy")}</td>
                                            </tr>
                                            <tr>
                                                <th>Age:</th>
                                                <td class="text-left">${patient.patient.person.age}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-primary mt-5">PATIENTS CONTACT DETAILS</h5>
                        <hr class="divider pt-1 bg-primary"/>

                        <div class="row">
                            <div class="col pl-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                            <tr>
                                                <th>Email:</th>
                                                <td class="text-left">${patient.patient.getAttribute('Email') ?: ''}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col pl-0 pr-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                        <tr>
                                            <th>Contact number:</th>
                                            <td class="text-left text-primary">${patient.patient.getAttribute('Telephone Number') ?: ''}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-primary mt-5">PATIENTS ADDRESS DETAILS</h5>
                        <hr class="divider pt-1 bg-primary"/>

                        <div class="row">
                            <div class="col pl-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                            <tr>
                                                <th>Home address:</th>
                                                <td class="text-left">${patient.patient.person.personAddress?.address1 ?: ''}</td>
                                            </tr>
                                            <tr>
                                                <th>District:</th>
                                                <td class="text-left">${patient.patient.person.personAddress?.address2 ?: ''}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col pl-0 pr-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                        <tr>
                                            <th>City:</th>
                                            <td class="text-left">${patient.patient.person.personAddress?.cityVillage ?: ''}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-primary mt-5">ADDITIONAL DETAILS</h5>
                        <hr class="divider pt-1 bg-primary"/>

                        <div class="row">
                            <div class="col pl-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                            <tr>
                                                <th>Occupation:</th>
                                                <td class="text-left">${patient.patient.getAttribute('Occupation') ?: ''}</td>
                                            </tr>
                                            <tr>
                                                <th>Education level:</th>
                                                <td class="text-left">${patient.patient.getAttribute('Education')?.value ?: ''}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col pl-0 pr-0">
                                <div class="table-responsive">
                                    <table class="table mt-3">
                                        <tbody>
                                        <tr>
                                            <th>Name of employer:</th>
                                            <td class="text-left text-primary">${patient.patient.getAttribute('Name of employer') ?: ''}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <h5 class="text-primary mt-5">CONTACT PERSON: BIO DATA</h5>
                        <hr class="divider pt-1 bg-primary"/>

                        <div class="row">
                            <div class="col pl-0 pr-0">
                                <%  relations.each { %>
                                    <div class="table-responsive">
                                        <table class="table mt-3">
                                            <tbody>
                                                <tr>
                                                    <th>Contact Person Type:</th>
                                                    <td class="text-left">${it.personB.getAttribute('ContactPersonType') ?: ''}</td>
                                                </tr>
                                                <tr>
                                                    <th>Identification Type:</th>
                                                    <td class="text-left">${idType ?: ''}</td>
                                                </tr>
                                                <tr>
                                                    <th>ID number:</th>
                                                    <td class="text-left text-danger">${idValue ?: ''}</td>
                                                </tr>
                                                <tr>
                                                    <th>Full Name:</th>
                                                    <td class="text-left">${it.personB.personName.familyName}&nbsp;${it.personB.personName.givenName}</td>
                                                </tr>
                                                <tr>
                                                    <th>Relationship:</th>
                                                    <td class="text-left">${it.relationshipType.bIsToA}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email:</th>
                                                    <td class="text-left">${it.personB.getAttribute('Email') ?: ''}</td>
                                                </tr>
                                                <tr>
                                                    <th>Contact number:</th>
                                                    <td class="text-left text-primary">${it.personB.getAttribute('Telephone Number') ?: ''}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <% } %>
                            </div>
                        </div>
                        <div class="card-footer text-muted mt-4 pr-1">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
