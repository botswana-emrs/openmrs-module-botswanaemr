<% if (modalTitle != null && modalTitle != "") { %>
     <h5 class="text-primary text-uppercase p-3">${modalTitle}</h5>
<% } %>
<% if (hasEncounter == true) { %>
${ui.includeFragment("htmlformentryui", "htmlform/enterHtmlForm", [
        visit               : encounter?.visit,
        encounter           : encounter,
        patient             : currentPatient,
        returnUrl           : returnUrl,
        definitionUiResource: definitionUiResource ?: ""]
)}
<% } else { %>
${ui.includeFragment('htmlformentryui', 'htmlform/enterHtmlForm', [
        visit               : currentPatientVisit,
        patient             : currentPatient,
        formUuid            : formUuid,
        defaultEncounterDate: defaultEncounterDate,
        returnUrl           : returnUrl]
)}
<% } %>
