<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#sexualStiHistoryTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
        jq('#searchBtn').on('click', () => {
            table.draw();
        });


        jq("#${sexualStiHistoryFormUuid}").click(function () {
            windowUtils.displayAsPopup('${ui.pageLink("botswanaemr","familyplanning/sexualStiHistory", [patientId: patient.uuid, visitId: visit.id])}', 'Add Sexual/STI History');
        })
    });
</script>

<div class="row mt-3 mb-3">
    <div class="col-6 pl-0">
        <h5 class="text-primary">SEXUAL/STI HISTORY</h5>
    </div>

    <div class="col-6 pr-0">
        <button class="btn btn-sm btn-primary float-right" id="${sexualStiHistoryFormUuid}">
            <i class="fa fa-plus" aria-hidden="true"></i> Add Sexual/STI History
        </button>
    </div>
</div>

<hr class="divider pt-1 mb-5 bg-primary"/>

<div id="myIFrameModal"></div>

<div class="table-responsive">
    <% if (sexualStiHistoryEncounters.size() > 0) { %>
    <table id="sexualStiHistoryTable"
           class="table table-striped table-sm table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Provider</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <% sexualStiHistoryEncounters.each { %>
        <tr>
            <td>${it.id ?: ""}</td>
            <td>${ui.formatDatePretty(it.encounterDatetime ?: "")}</td>
            <td>${ui.format(it.creator ?: "")}</td>
            <td>
                <button id="viewSexualStiHistory${it.id}"
                        onclick="windowUtils.displayAsPopup('${ui.pageLink("botswanaemr","familyplanning/sexualStiHistory", [patientId: it.patient.uuid, visitId: it.visit.id, encounterId: it.id])}','Sexual/STI History');"
                        class="btn btn-primary btn-outline border-0 bg-transparent float-right text-primary"> View
                </button>
                <button id="viewActivity${it.id}" objectId="${it.id}" objectType="Encounter" title="SEXUAL/STI HISTORY"
                        class="btn btn-primary btn-outline border-0 bg-transparent float-right text-primary view-activity"> Activity Trail
                </button>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <% } else { %>
    <div class="alert alert-sm alert-warning">
        <h5 class="text-danger">No records found!</h5>
    </div>
    <% } %>
</div>

