<script type="text/javascript">
    jq(function() {
        jq("#deleteProficiencyTestingForm").submit(function(e){
            e.preventDefault();
            var params = {
                'proficiencyTestingId': jq('#proficiencyTestingId').val()
            };

            jq.getJSON('${ui.actionLink('botswanaemr','hts/proficiencyTestsListing','voidProficiencyTesting')}', params)
                .done(function (data) {
                    jq().toastmessage('showNoticeToast', "Proficiency testing deleted successfully");
                    history.back();
                }).fail(function() {
                jq().toastmessage('showErrorToast', "An error occurred while deleting test!");
            })
        });
    });
</script>

<!-- Delete Proficiency Testing -->
<div class="modal fade pt-3 mt-3 pl-4" id="deleteProficiencyTestingModal" tabindex="-1"
     data-controls-modal="deleteProficiencyTestingModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="deleteProficiencyTestingModalTitle">

    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="deleteProficiencyTestingModalTitle">Remove</h4>
                <button type="button" onclick="history.back()" id="editPatientDetails" class="btn btn-sm btn-primary">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="deleteProficiencyTestingForm" method="post">
                    <div>
                        <h5 class="text-muted text-center mb-2">
                            Are you sure you want to remove this record? All fields for this record will be cleared!
                        </h5>
                        <div class="form-group">
                            <input type="hidden" id="proficiencyTestingId" name="proficiencyTestingId" value="${htsProficiencyTesting?.proficiencyTestingId}">
                        </div>
                        <div class="row">
                            <button id="deleteProficiencyTesting" type="submit" class=" btn btn-primary btn-block">${ui.message("Delete")}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
         jQuery("#deleteProficiencyTestingModal").appendTo("body");
    });
</script>
