<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jQuery('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jQuery('#searchPhrase').val() || name.toLowerCase().includes(jQuery('#searchPhrase').val().toLowerCase())
                );
            }
            if (jQuery('#dateCreated').val() !== '') {
                let queueDateCreated = aData[5].trim();
                matchingOptions.push(
                    queueDateCreated === jQuery('#dateCreated').val()
                );
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jQuery.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            dataType: "json",
            global: false,
            async: false,
            data: {
                servicePoint: "${htsServicePoint != null ? htsServicePoint : ''}",
                sourcePageName: "hts/htsAllPatientsPool",
            },
            success: function (data) {
                console.log(data);
                var table = jQuery('#htsPatientPoolTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display') {
                                    if (row.status == 'AWAITING_LAB_TEST') {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting Lab</span>';
                                    } else if (row.status == 'DIAGNOTICS_TEST_COMPLETED') {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Diagnostics Test Complete</span>';
                                    } else if (row.status == 'DIAGNOTICS_TEST_ONGOING') {
                                       return '<i class="fa fa-circle text-primary"></i><span class="text-muted px-0" style="background: none;"> Testing in progress</span>';
                                    } else if (row.status == 'VERIFICATION_TEST_COMPLETED') {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Verification Test Complete</span>';
                                    } else if (row.status == 'VERIFICATION_TEST_ONGOING') {
                                       return '<i class="fa fa-circle text-primary"></i><span class="text-muted px-0" style="background: none;"> Verification in progress</span>';
                                    } else if (row.status == 'AWAITING_DIAGNOSTIC_TEST') {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting Diagnostics Test</span>';
                                    } else if (row.status == 'PRIOR_TEST_COMPLETED') {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Prior Test Completed</span>';
                                    } else if (row.status == 'PRIOR_TEST_ONGOING') {
                                       return '<i class="fa fa-circle text-primary"></i><span class="text-muted px-0" style="background: none;"> Prior Test in progress</span>';
                                    } else if (row.status == 'PENDING') {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
/*
                                    if(row.status == "COMPLETED") {
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Completed</span>';
                                    } else  if (row.status == "TEST_COMPLETED") {
                                        return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Test Complete</span>';
                                    } else if (row.status == "IN_PROGRESS") {
                                       return '<i class="fa fa-circle text-primary"></i><span class="text-muted px-0" style="background: none;"> Testing in progress</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
*/
                                }
                                return data;
                            }
                        },
                        {'data': 'dateCreated'},
                        {'data': null,
                            "render": function(data, type, row, meta) {
                                if(type === 'display') {
                                    returnUrl = urlUtils.encodeUrl('/${ui.contextPath()}/botswanaemr/hts/htsDashboard.page');
                                    if (row.status == 'AWAITING_LAB_TEST') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else if (row.status == 'DIAGNOTICS_TEST_COMPLETED') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">View</a>&nbsp;';
                                    } else if (row.status == 'DIAGNOTICS_TEST_ONGOING') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else if (row.status == 'VERIFICATION_TEST_COMPLETED') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">View</a>&nbsp;';
                                    } else if (row.status == 'VERIFICATION_TEST_ONGOING') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else if (row.status == 'AWAITING_DIAGNOSTIC_TEST') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else if (row.status == 'PRIOR_TEST_COMPLETED') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">View</a>&nbsp;';
                                    } else if (row.status == 'PRIOR_TEST_ONGOING') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else if (row.status == 'PENDING') {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">Test</a>|&nbsp;'+
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0" id="reassign">Reassign</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">Test</a>|&nbsp;'+
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0" id="reassign">Reassign</a>';                                        
                                    }

/*
                                   if(row.status == "COMPLETED") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">View</a>&nbsp;';
                                   } else if (row.status == "TEST_COMPLETED") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">View</a>&nbsp;';
                                   } else if (row.status == "IN_PROGRESS") {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary float-left pl-0 pr-0" id="view">Resume HTS Service</a>|'+
                                            '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0 pr-0" id="reassign">Reassign</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId='+row.patientId+'&returnUrl='+returnUrl+'" class="text-primary p-1" id="view">Test</a>|&nbsp;'+
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'&returnUrl='+returnUrl+'" class="text-primary pl-0" id="reassign">Reassign</a>';
                                    }
*/
                                }

                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jQuery('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jQuery('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jQuery('#htsPatientPool'));
                    table.draw();
                });

                jQuery('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="col-4 pl-0">
            <div class="form-group">
                <input type="text"
                       class="form-control py-2"
                       id="searchPhrase"
                       name="searchPhrase"
                       placeholder="Search by Name, ID"/>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <input type="text"
                       class="form-control py-2"
                       id="dateCreated"
                       name="dateCreated"
                       placeholder="YYYY-MM-DD">
                <script type="text/javascript">
                    jQuery('#dateCreated').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        "setDate": new Date(),
                        dateFormat: "yy-mm-dd",
                        yearRange: "-150:+0",
                        maxDate: 0,
                        "autoclose": true,
                        onSelect: function (data) {
                            jQuery("#dateCreated").trigger('change');
                        }
                    });
                </script>
            </div>
        </div>
        <div class="col-2 pr-0">
            <button type="submit" class="btn btn-primary float-right" id="inquiryBtn" name="inquiryBtn">
                <i class="fa fa-search"></i> search
            </button>
        </div>
        <div class="col-2 pl-0 pr-0">
            <button type="submit" class="btn btn-outline btn-secondary float-right" id="resetBtn" name="resetBtn">
                <i class="fa fa-undo fa-1x"></i>
            </button>
        </div>
    </div>
    <div class="table-responsive">
        <table id="htsPatientPoolTable" class="table table-striped table-sm table-bordered">
            <thead>
                <tr>
                    <th>PIN</th>
                    <th>Name</th>
                    <th>Sex</th>
                    <th>Age</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="htsTableDetails">
            </tbody>
        </table>
    </div>
</div>
