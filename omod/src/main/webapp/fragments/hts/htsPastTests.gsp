<script type="text/javascript">
    jQuery(function () {
        jQuery("#dateCreated").on('change', function() {
            let value = jQuery(this).val().toLowerCase();
            let rows =  jQuery("#htsPastTests tbody tr");
            rows.each(function() {
                if (!value) {
                    jQuery(this).show();
                    return;
                }
                let firstVal = jQuery(this).find("td:first").find("a:first");
                if (firstVal && firstVal.html() !== value) {
                    jQuery(this).hide();
                } else {
                    jQuery(this).show();
                }
            });
        });

        let todayDate = new Date().toISOString().split('T')[0];
        jQuery("#dateCreated").val(todayDate).trigger('change');

        jQuery('#resetBtn').on('click', () => {
            fieldHelper.clearAllFields(jQuery('#allTests'));
        });
    });
</script>
<div class="accordion">
    <div class="panel-group" id="allTests">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title text-left text-info mt-3">HIV testing history</h5>
            </div>
            <div class="panel-body">
                    <div class="row mb-1">
                        <div class="col-4">
                            <div class="form-group">
                                <input type="text"
                                       class="form-control py-2"
                                       id="dateCreated"
                                       name="dateCreated"
                                       placeholder="YYYY-MM-DD">
                                <script type="text/javascript">
                                    jQuery('#dateCreated').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        showButtonPanel: true,
                                        "setDate": new Date(),
                                        dateFormat: "yy-mm-dd",
                                        yearRange: "-150:+0",
                                        maxDate: 0,
                                        "autoclose": true,
                                        onSelect: function (data) {
                                            console.log("data", data)
                                            jQuery("#dateCreated").trigger('change');
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                        <div class="col-2 pl-0 pr-0">
                            <button type="submit" class="btn btn-outline btn-secondary float-right" id="resetBtn" name="resetBtn">
                                <i class="fa fa-undo fa-1x"></i>
                            </button>
                        </div>
                    </div>

                    <table class="table mt-1 mb-1" id="htsPastTests">
                        <thead>
                        <tr>
                            <th class="bg-pale"><h6 class="text-dark text-center">Visit Date</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Registration Number</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Location</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Plan</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Service</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Accept</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Result</h6></th>
                            <th class="bg-pale"><h6 class="text-dark text-center">Record</h6></th>
                        </tr>
                        </thead>
                        <tbody>
                        <% htsTestingEncounters.each { %>
                            <tr class="text-center">
                                <td class="text-primary text-uppercase">
                                    <a href="${ ui.pageLink("botswanaemr", "editEncounter", [encounterId:it.uuid, patientId: patient.id, returnUrl: returnUrl]) }" class="dropdown-item">${it.encounterDatetime.format("yyyy-MM-dd")}</a>
                                </td>
                                <td>${pin}</td>
                                <td>${it.location?.name}</td>
                                <% if (it.encounterType.uuid == newDiagnosticEncounterType) { %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == planConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == reasonForTestConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == acceptedConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == resultsConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                <% } else if (it.encounterType.uuid == verificationEncounterType) { %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == typeConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                    <td>${ui.format('Verification Test')}</td>
                                    <td>${ui.format('Yes')}</td>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == verificationOutcomeConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                <% } else if (it.encounterType.uuid == supplementaryTestEncounterType) { %>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == typeConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                    <td>${ui.format('Supplementary Test')}</td>
                                    <td>${ui.format('Yes')}</td>
                                    <% it.obs.each { obs -> %>
                                    <% if (obs.concept.uuid == supplementaryTestOutcomeConceptUuid) { %>
                                        <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                    <% } %>
                                    <% } %>
                                <% } else { %>
                                    <td>${ui.encodeHtmlContent(ui.format(""))}</td>
                                    <td>${ui.format('Prior Test Documentation')}</td>
                                    <td>${ui.format('Yes')}</td>
                                    <% it.obs.each { obs -> %>
                                        <% if (obs.concept.uuid == priorTestOutcomeConceptUuid) { %>
                                            <td>${ui.encodeHtmlContent(ui.format(obs.valueCoded))}</td>
                                        <% } %>
                                    <% } %>
                                <% } %>
                                <td> <h6></h6></td>
                            </tr>

                        <% } %>
                        </tbody>
                    </table>
                </div>
    </div>
</div>
