<script type="text/javascript">
    let specimenResults = {};
    let ptSpecimenData = [];
    let dtsBuffersData = [];

    function getUrlParameter(param) {
        let pageUrl = window.location.search.substring(1);
        let urlVariables = pageUrl.split('&');
        for (let i = 0; i < urlVariables.length; i++) {
            let paramName = urlVariables[i].split('=');
            if (paramName[0] === param) {
                return paramName[1];
            }
        }
    }

    jQuery(function () {
        let recordedSpecimenResults = ${result};
        let proficiencyTestingId = ${proficiencyTestingId};
        let htsTestingTests = ${htsTestingTests};
        let simplifiedHtsProficiencyTestingBuffers = ${simplifiedHtsProficiencyTestingBuffers};
        let ptSpecimenIdValue;
        let ptSpecimenLotNumberValue;
        let ptSpecimenExpiryDateValue;
        let kitNames = {
            "Determine": "c2ce43cc-0345-40c0-91b2-24155f458d4e",
            "Bioline": "166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            "KHB": "c53f44de-5df2-4810-b99b-7f75e80d1a05",
            "ICT": "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        };
        let repeatKitNames = {
            "Unigold": "c2acb8a1-32f6-456a-b322-d31a1d478c01",
            "1st Response": "570a1f68-0bea-4980-ba9d-d0d7d21e4be9",
            "Bio KIt": "c203885b-7bae-4318-8bb8-4aee96f973cd",
            "Immunoflow": "7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f"
        };
        let kitResults = {
            "Reactive": "1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            "Non Reactive": "1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            "Invalid": "163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        };
        let stockRooms = [];
        const YES = "1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1065";
        const NO = "1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1066";
        const POSITIVE = "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165199";
        const NEGATIVE = "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165200";
        const REACTIVE = "1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165849";
        const NONREACTIVE = "1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165848";
        const INVALID = "163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166812";
        const INDETERMINATE = "1138AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1138";
        const CC_COUPLE = "b92e09dd-8247-4474-9e99-8857998ad3f1" // "166759";
        const COUPLE_WITH_PREGNANT_PARTNER = "726ea800-1d8c-45d5-a543-e3e796d9bf52";  // "166773";
        const COUPLE_HIV_COUNSELLING = "72a81e09-55dd-4d04-aded-70f4e5d4d585"; // "166779";
        const PARTNER_TO_PREGNANT_MOTHER = "cff3a7aa-7deb-4435-bc46-e166c5f3626a"; // "166772";
        const PMTCT_STAGE = "6d7c3e3e-ce9f-4705-a217-2d3b71dd3e6d"; // "166771";
        const RHT = "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166100";
        const VCT = "159940AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166942";
        const YES_HIV_POSITIVE = "11c7d569-81b2-47b6-94a8-5b27c751aeb9";

        jq("#reportSection").hide();
        jq("specimenResultSectionTemplate").hide();
        jq("#ptSpecimenTemplate").hide();
        jq("input[name=ptSpecimenId]").on("change", function(){
            ptSpecimenIdValue = jq(this).val();
        });
        jq("input[name=ptSpecimenLotNumber]").on("change", function(){
            ptSpecimenLotNumberValue = jq(this).val();
        });
        jq("input[name=ptSpecimenExpiryDate]").on("change", function(){
            ptSpecimenExpiryDateValue = jq(this).val();
        });
        stockUtils.fetchHtsServicePoints(jq("#testingPoint"));

        function toSentenceCase(inputString) {
            let result = inputString.replace(/([A-Z])/g, " \$1");
            result = result.charAt(0).toUpperCase() + result.slice(1);
            return result;
        }

        function fetchStockRooms() {
            jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchStockRooms.action')
                .done(function (data) {
                    if (data) {
                        if (data.stockrooms) {
                            for (let i in data.stockrooms) {
                                data.stockrooms[i].label = toSentenceCase(data.stockrooms[i].name);
                            }
                            stockRooms = data.stockrooms;
                        }
                    }
                });
        }
        fetchStockRooms();

        function setupStockRoomAutocomplete(stockRoomInput, stockRoomIdInput) {
            jq(stockRoomInput).autocomplete({
                source: stockRooms,
                minLength: 0,
                select: function (event, ui) {
                    jq("input[name=stockRoomId]:empty").each(function () {
                        if (!jq(this).val()) {
                            jq(this).val(ui.item.uuid);
                        }
                    });
                    jq("input[name=stockRoom]:empty").each(function () {
                        if (!jq(this).val()) {
                            jq(this).val(ui.item.label);
                        }
                    });
                    jq(stockRoomIdInput).val(ui.item.uuid);
                }
            }).blur(function () {
                jq(this).autocomplete('enable');
            }).focus(function () {
                jq(this).autocomplete('search', '');
            });
        }

        function cloneSpecimenResults(rowIndex, specimenVal) {
            let template = jq("#specimenResultSectionTemplate").children().clone(true, true);

            jq('<div/>', {
                'class': 'table-responsive mt-2 pt-2 extraSpecimenResult',
                'id': 'extraSpecimenResult' + rowIndex,
                html: template
            }).hide().appendTo('#extraResults').slideDown('slow');

            let divId = '#extraSpecimenResult' + rowIndex ;
            jq(divId).find("#specimenTitle").html("SPECIMEN: "+ specimenVal).attr("id", "specimenTitle" + rowIndex);

            jq(divId).find("#resultsTable").attr("id", "resultsTable" + rowIndex);
            jq(divId).find("#btnAddRepeatResult").attr("id", "btnAddRepeatResult" + rowIndex);
            jq(divId).find("#kitName").attr("id", "kitName1");
            jq(divId).find("#kitResults").attr("id", "kitResults1");
            jq(divId).find("#interpretation").attr("id", "interpretation"+ rowIndex);
            jq(divId).find("#resultComment").attr("id", "resultComment"+ rowIndex);


            jq(divId).find("#stockRoom").attr("id", "stockRoom" + rowIndex);
            jq(divId).find("#stockRoomId").attr("id", "stockRoomId" + rowIndex);
            setupStockRoomAutocomplete(jq(divId).find("#stockRoom" + rowIndex), jq(divId).find("#stockRoomId" + rowIndex));
            if (rowIndex > 1) {
                jq(divId).find("#stockRoom" + rowIndex).val(jq(divId).find("#stockRoom1").val());
                jq(divId).find("#stockRoomId" + rowIndex).val(jq(divId).find("#stockRoomId1").val());
            }

        }

        function compareResults( a, b ) {
            if ( a.resultNumber < b.resultNumber ){
                return -1;
            }
            if ( a.resultNumber > b.resultNumber ){
                return 1;
            }
            return 0;
        }

        jq("input[name=ptSpecimenId]").change(function (){
            let val = jq(this).val();
            let tr = jq(this).closest('tr');
            let rowIndex = jq("#ptSpecimenBody tr").index(jq(tr));

            if (val) {
                let resultBodyExists = specimenResults.hasOwnProperty(rowIndex);

                specimenResults[rowIndex] = val;
                jq("#specimenTitle" + rowIndex).html("SPECIMEN: " + val)
                if (!resultBodyExists) {
                    cloneSpecimenResults(rowIndex, val);
                }
            } else if(specimenResults[rowIndex]) {
                jq("#extraSpecimenResult" + rowIndex).remove();
                delete specimenResults[rowIndex];
            }
        });

        jq("select[name=ptSpecimenConditions]").change(function (){
            let tr = jq(this).closest('tr');
            let isAcceptable = jq(this).val() === "Acceptable";

            jq(tr).find("#reasonForUnacceptablePtSpecimen").prop('disabled', isAcceptable);
            jq(tr).find("#correctiveActions").prop('disabled', isAcceptable);

        });

        jq("select[name=dtsBufferConditions]").change(function (){
            let tr = jq(this).closest('tr');
            let isAcceptable = jq(this).val() === "Acceptable";

            jq(tr).find("#dtsReasonForUnacceptableBuffer").val("");
            jq(tr).find("#dtsReasonForUnacceptableBuffer").prop('disabled', isAcceptable);
            jq(tr).find("#dtsCorrectiveActions").val("");
            jq(tr).find("#dtsCorrectiveActions").prop('disabled', isAcceptable);

        });

        function addTestRow(index) {
            let rowCount = jq("#resultsTable" + index + " >tbody >tr").length + 1;

            if (rowCount <= 4) {
                let template = jq("#resultTemplate").clone(true, true);
                if (rowCount % 2 === 0) {
                    template = jq("#repeatResultTemplate").clone(true, true);
                }
                jq(template).attr("id", "result");

                jq(template).attr("id", "repeatResult" + rowCount);
                jq(template).find('td:first-of-type').html("t" + rowCount);
                jq(template).find('#kitName').attr("id", "kitName" + rowCount);
                jq(template).find('#kitResults').attr("id", "kitResults" + rowCount);

                jq("#resultsTable" + index + " >tbody").append(template);
            }
        }

        jq('.add-more').click(function (e) {
            e.preventDefault();
            let index = this.id.charAt(this.id.length - 1);
            addTestRow(index);
            addTestRow(index);
        });

        function runResultLogic(resultTable, index, tableIndex) {

            let noOfStrips = 0;
            let finalResults = '';
            let test1Results = resultTable.find("#kitResults1").val() || '';
            let test1RepeatResults = resultTable.find("#kitResults3").val() || '';
            let test2Results = resultTable.find("#kitResults2").val() || '';
            let test2RepeatResults = resultTable.find("#kitResults4").val() || '';

            let t1Outcome = '';
            let t2Outcome = '';
            let t3Outcome = '';

            if (test1Results === NONREACTIVE || test1RepeatResults === NONREACTIVE) {
                t1Outcome = NONREACTIVE;
            } else if (test1Results === REACTIVE || test1RepeatResults === REACTIVE) {
                t1Outcome = REACTIVE;
            } else if (test1Results === INVALID && test1RepeatResults === '') {
                // In the case where a test is invalid, the same test should be repeated with the same test kit.
                //Enable add repeat test button
                jq("#extraSpecimenResult" + tableIndex).find("#btnAddRepeatResult" + tableIndex).prop("disabled", false);

            } else if (test1Results === INVALID && test1RepeatResults === INVALID) {
                // If the test is invalid twice, the user should be redirected to the lab test
                t1Outcome = INVALID;
            }

            if (t1Outcome === NONREACTIVE) {
                finalResults = NEGATIVE;
            } else if (t1Outcome === REACTIVE) {
                // In the case where a test is reactive, perform a second confirmatory test.
                // TODO: Shown in this case by default. Clarify
            } else if (t1Outcome === INVALID) {
                finalResults = INDETERMINATE;
            }

            if (t1Outcome === REACTIVE) {
                if (test2Results === INVALID) {
                    // In the case where a test is invalid, the same test should be repeated with the same test kit.
                    //Enable add repeat test button
                    jq("#extraSpecimenResult" + tableIndex).find("#btnAddRepeatResult" + tableIndex).prop("disabled", false);
                } else if (test2Results === NONREACTIVE || test2RepeatResults === NONREACTIVE) {
                    t2Outcome = NONREACTIVE;
                } else if (test2Results === REACTIVE || test2RepeatResults === REACTIVE) {
                    t2Outcome = REACTIVE;
                } else  if (test2Results === INVALID && test2RepeatResults === INVALID) {
                    // If the test is invalid twice, the user should be redirected to the lab test
                    t2Outcome = INVALID;
                }

                if (t2Outcome === NONREACTIVE) {
                    // If T2 gives discordant results, both T1 and T2 should be repeated with new test devices,
                    // and HIV status will be interpreted as per outcome of repeat test
                    //Enable add repeat test button
                    jq("#extraSpecimenResult" + tableIndex).find("#btnAddRepeatResult" + tableIndex).prop("disabled", false);
                } else if (t2Outcome === REACTIVE) {
                    finalResults = POSITIVE;
                } else if (t2Outcome === INVALID) {
                    finalResults = INDETERMINATE;
                    jq().toastmessage('showNoticeToast', 'Client needs to undertake a Lab test due to repeated invalid tests');
                }

                if (finalResults === '') {
                    if (test1RepeatResults === NONREACTIVE && test2RepeatResults === NONREACTIVE) {
                        t3Outcome = NONREACTIVE;
                    } else if (test1RepeatResults === REACTIVE && test2RepeatResults === REACTIVE) {
                        t3Outcome = REACTIVE;
                    } else if (test1RepeatResults === INVALID || test2RepeatResults === INVALID) {
                        t3Outcome = INVALID;
                    } else if (test1RepeatResults !== '' && test2RepeatResults !== '' && test1RepeatResults !== test2RepeatResults) {
                        t3Outcome = INVALID;
                    }

                    // if the two test continue to give discordant result, Report as HIV status Indeterminate;
                    // and the individual asked to return for resting after 14days or a blood sample is drawn
                    // and sent to the lab for further testing
                    if (t3Outcome === INVALID) {
                    }

                    if (t3Outcome === REACTIVE) {
                        finalResults = POSITIVE;
                    } else if (t3Outcome === NONREACTIVE) {
                        finalResults = NEGATIVE;
                    } else {
                        finalResults = INDETERMINATE;
                    }
                }
            }
        }

        jq(".kitResults").change(function (){
            let val = jq(this).val();
            let index = this.id.charAt(this.id.length - 1);

            let resultTable = jq(this).closest("table");
            let tableIndex = resultTable.attr("id").charAt(resultTable.attr("id").length - 1);

            runResultLogic(resultTable, index, tableIndex);
        });

        function numericToIsoDateFormat(dateValue) {
            let date;

            if (dateValue.includes('/')) {
                const [day, month, year] = dateValue.split('/');
                date =  new Date([month, day, year].join('/'));
            } else {
                let date = new Date(dateValue);
            }

            let dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
                .toISOString()
                .split("T")[0];
            return dateString;
        }

        function fetchHtsKitAvailableBatches(conceptId, batchIdInput, expiryInput, stockroomId) {
            let parameter = {
                'conceptId': conceptId,
                'stockroomId': stockroomId
            };
            let batchNumberToExpiry = {};

            jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsKitAvailableBatches.action', parameter)
                .done(function (data) {
                    if (data.status === 'success') {
                        // populate autocomplete with batches
                        if (data.batches) {
                            let quantity = data.availableQuantity;

                            jq.each(data.batches, function(i, item) {
                                batchNumberToExpiry[item.batchNumber] = item.expiryDate.replaceAll("/", "-");
                                if (data.batchNumbers[i]) {
                                    let val = item.batchNumber + " (" + batchNumberToExpiry[item.batchNumber] + ")";
                                    data.batchNumbers[i] = val;
                                    batchNumberToExpiry[val] = item.expiryDate.replaceAll("/", "-");
                                }
                            })

                            jq(batchIdInput).autocomplete({
                                source: data.batchNumbers,
                                minLength: 0,
                                select: function (event, ui) {
                                    // jQuery(expiryInput).val(numericToIsoDateFormat(batchNumberToExpiry[ui.item.value]));
                                    jq(expiryInput).val(batchNumberToExpiry[ui.item.value]);
                                }
                            }).blur(function () {
                                jq(this).autocomplete('enable');
                            }).focus(function () {
                                    jq(this).autocomplete('search', '');
                                });
                        } else {
                        }
                    } else {
                    }
                });
        }

        jq(".kitName").change(function () {
            let kitVal = jq(this).val();

            if (jq(this).closest('.extraSpecimenResult').find('.stockRoomId').val()) {
                if (kitVal) {
                    let lotNoInput = jq(this).closest('tr').find(".lotNo");
                    let expiryDateInput = jq(this).closest('tr').find(".expiryDate");

                    let stockRoomInput = jq(this).closest('.extraSpecimenResult').find('.stockRoomId');
                    fetchHtsKitAvailableBatches(kitVal, lotNoInput, expiryDateInput, stockRoomInput.val());
                }
            } else {
                jq().toastmessage('showErrorToast', "Select the stock room first");
            }
        });

        function duplicatePtSpecimen() {
            let ptTemplate = jq("#ptSpecimenTemplate").children().clone(true, true);
            ptTemplate.find('input[name="ptSpecimenId"]').val(ptSpecimenIdValue);
            ptTemplate.find('input[name="ptSpecimenLotNumber"]').val(ptSpecimenLotNumberValue);
            ptTemplate.find('input[name="ptSpecimenExpiryDate"]').val(ptSpecimenExpiryDateValue);
            ptTemplate.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });
            var ptRowCount = jq('#ptSpecimensTable >tbody >tr').length;
            jq('<tr/>', {
                'class': 'newPtSpecimen',
                'id': ptRowCount,
                html: ptTemplate
            }).hide().appendTo('#ptSpecimenBody').slideDown();
        }

        duplicatePtSpecimen();
        duplicatePtSpecimen();
        duplicatePtSpecimen();
        duplicatePtSpecimen();
        duplicatePtSpecimen();

        jq(".newPtSpecimen").each(function (index) {
            if (htsTestingTests && htsTestingTests.length > 0) {
                let htsTest = htsTestingTests[index];
                if (htsTest) {
                    jq(this).attr("data-overlay", htsTest.proficiencyTestingTestId)
                    jq(this).find('input[name="ptSpecimenId"]').val(htsTest.specimenId);
                    jq(this).find('input[name="ptSpecimenLotNumber"]').val(htsTest.lotNumber);
                    jq(this).find('input[name="ptSpecimenExpiryDate"]').val(htsTest.expiryDate);
                    jq(this).find('select[name="ptSpecimenConditions"]').val(htsTest.conditionOfSpecimen);
                    jq(this).find('input[name="reasonForUnacceptablePtSpecimen"]').val(htsTest.reasonForUnacceptableSpecimen);
                    jq(this).find('input[name="correctiveActions"]').val(htsTest.correctiveActions);

                    // Generate tests for each specimen
                    if (htsTest.specimenResults && htsTest.specimenResults.length > 0) {
                        cloneSpecimenResults(index+1,htsTest.specimenId);
                        jq.each(htsTest.specimenResults, function (key, specimenResult) {
                            let specimenResultIndex = index+1;


                            jq("#extraSpecimenResult"+ specimenResultIndex).attr("data-overlay", specimenResult.id);
                            jq("#interpretation"+specimenResultIndex).val(specimenResult.interpretation);
                            jq("#resultComment"+specimenResultIndex).val(specimenResult.comment);
                            jq("#extraSpecimenResult"+ specimenResultIndex).find(".stockRoom").val(toSentenceCase(specimenResult.stockRoom));
                            jq("#extraSpecimenResult"+ specimenResultIndex).find(".stockRoomId").val(specimenResult.stockRoomId);

                            specimenResults[specimenResultIndex] = specimenResult.specimenName;

                            specimenResult.results.sort(compareResults);
                            jq.each(specimenResult.results, function (key, result) {
                                if (result.resultNumber && result.resultNumber > 2) {
                                    addTestRow(specimenResultIndex);
                                }
                                let tr = jq("#resultsTable" + specimenResultIndex + " tbody tr:nth-child(" + result.resultNumber + ")");
                                if (result.expiryDate) {
                                    let date = new Date(result.expiryDate).toISOString().split('T')[0];
                                    jq(tr).find('input[name="expiryDate"]').val(date);
                                }
                                jq(tr).attr("data-overlay", result.id);
                                jq(tr).find('td:first-of-type').html("t" + result.resultNumber);
                                jq(tr).find('input[name="lotNo"]').val(result.lotNumber);
                                jq(tr).find('select[name=kitResults]').val(result.kitResults);
                                jq(tr).find('select[name=kitName]').val(result.kitName);
                                jq(tr).attr("resultId", result.id);
                            });
                        });
                    } else {
                        cloneSpecimenResults(index+1,htsTest.specimenId);
                        specimenResults[index+1] = htsTest.specimenId;
                    }
                }
            }
        });

        if (proficiencyTestingId && ${htsProficiencyTesting?.published}) {
            jq("#proficiencyTestingForm :input").prop("disabled", true);
            jq("#cancel").prop("disabled", false);
            jq("#reportSection").show();
        }
        if (getUrlParameter("action") === "report") {
            jq("#proficiencyTestingForm :input").prop("disabled", true);
            jq("#reportSection :input").prop("disabled", false);
            jq("#reportSection").show();
            jq('#buttonSection :button').prop('disabled', false);
            jq("#saveDraftProficiencyTestingFormBtn").prop("disabled", true);
        }

        const searchSystemUsers = function (request, response) {
            jq.getJSON(
                '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/user?v=full&q=' + request.term,
                function (data) {
                    let results = data.results.map(function (user) {
                        let label = user.person && user.person.display ? user.person.display : user.display;
                        let value = user.person.uuid;

                        return {
                            label: label,
                            value: value
                        };
                    });
                    response(results)
                });
        };

        const setupUserAutocomplete = function (fieldIdentifier) {
            let textField = jq(fieldIdentifier);

            textField.autocomplete({
                source: searchSystemUsers,
                // select: selectItem,
                minLength: 4,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(this).attr('data-overlay', ui.item.value);
                    jq(this).val(ui.item.label);
                }
            });
        }

        // Set autocomplete for system users
        setupUserAutocomplete('#receivedBy');
        setupUserAutocomplete('#resultsReceivedBy');
        setupUserAutocomplete('#reportReviewedBy');

    });

    jQuery(function () {
        let dtsBufferIdValue;
        let dtsLotNumberValue;
        let dtsExpiryDateValue;
        let simplifiedHtsProficiencyTestingBuffers = ${simplifiedHtsProficiencyTestingBuffers} || [];

        jq("#dtsBufferTemplate").hide();
        jq("input[name=dtsBufferId]").on("change", function(){
            dtsBufferIdValue  = jq(this).val();
        });
        jq("input[name=dtsLotNumber]").on("change", function(){
            dtsLotNumberValue = jq(this).val();
        });
        jq("input[name=dtsExpiryDate]").on("change", function(){
            dtsExpiryDateValue = jq(this).val();
        });

        function duplicateDtsBuffer() {
            let dtsTemplate = jq("#dtsBufferTemplate").children().clone(true, true);
            dtsTemplate.find('input[name="dtsBufferId"]').val(dtsBufferIdValue);
            dtsTemplate.find('input[name="dtsLotNumber"]').val(dtsLotNumberValue);
            dtsTemplate.find('input[name="dtsExpiryDate"]').val(dtsExpiryDateValue);
            dtsTemplate.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });
            var dtsRowCount = jq('#dtsBufferTable >tbody >tr').length;
            jq('<tr/>', {
                'class': 'newDtsBuffer',
                'id': "dtsBuffer" + dtsRowCount,
                html: dtsTemplate
            }).hide().appendTo('#dtsBufferBody').slideDown('slow');
            return dtsRowCount;
        }
        if(simplifiedHtsProficiencyTestingBuffers.length > 0) {
            jq.each(simplifiedHtsProficiencyTestingBuffers, function (i, buffer) {
                let index = duplicateDtsBuffer();
                jq("#dtsBuffer" + index).attr("data-overlay", buffer.proficiencyTestingBufferId);
                jq("#dtsBuffer" + index).find('input[name="dtsBufferId"]').val(buffer.dtsBufferId);
                jq("#dtsBuffer" + index).find('input[name="dtsLotNumber"]').val(buffer.dtsLotNumber);
                jq("#dtsBuffer" + index).find('input[name="dtsExpiryDate"]').val(buffer.dtsExpiryDate);
                jq("#dtsBuffer" + index).find('select[name="dtsBufferConditions"]').val(buffer.dtsConditionOfBuffer);
                jq("#dtsBuffer" + index).find('input[name="dtsReasonForUnacceptableBuffer"]').val(buffer.dtsReasonForUnacceptableBuffer);
                jq("#dtsBuffer" + index).find('input[name="dtsCorrectiveActions"]').val(buffer.dtsCorrectiveActions);
            })
        } else {
            duplicateDtsBuffer();
        }
        if (getUrlParameter("action") === "report") {
            jq("#dtsBufferBody :input").prop("disabled", true);
        }
    });

    jQuery(function () {
        var table = jQuery('#hivTestResults').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });

    jQuery(function() {
        function saveForm(e, published) {
            jq("#submit").click();
            var ptSpecimenData = [];
            let stockRoomId ;
            jq('#ptSpecimensTable').find('tbody tr:gt(0)').each(function() {
                ptSpecimenId = jq(this).find('input[name="ptSpecimenId"]').val();
                ptSpecimenLotNumber  = jq(this).find('input[name="ptSpecimenLotNumber"]').val();
                ptSpecimenExpiryDate = jq(this).find('input[name="ptSpecimenExpiryDate"]').val();
                specimenConditions   = jq(this).find(('select[name=ptSpecimenConditions]'));
                ptSpecimenConditions = specimenConditions.val();
                reasonForUnacceptablePtSpecimen = jq(this).find('input[name="reasonForUnacceptablePtSpecimen"]').val();
                correctiveActions = jq(this).find('input[name="correctiveActions"]').val();
                let specimenId = jq(this).attr("data-overlay") || '';

                let rowIndex = jq("#ptSpecimenBody tr").index(jq(this));
                let recordedSpecimenResults = [];
                if (ptSpecimenId && ptSpecimenId === specimenResults[rowIndex]) {
                    let divId = "#extraSpecimenResult" + rowIndex;

                    let results = [];
                    jq(divId).find("#resultsTable" + rowIndex + " tbody tr").each(function () {
                        let kitNameInput = jq(this).find('select[name=kitName]');
                        let kitName = kitNameInput.val();
                        let lotNo = jq(this).find('input[name="lotNo"]').val();
                        let expiryDate = jq(this).find('input[name="expiryDate"]').val();
                        let kitResults = jq(this).find('select[name=kitResults]').val();
                        let trIndex = kitNameInput.attr("id").charAt(kitNameInput.attr("id").length - 1);
                        let resultId = jq(this).attr("data-overlay") || '';

                        if (kitName) {
                            results.push(
                                {
                                    "id": resultId,
                                    "kitName": kitName,
                                    "lotNo": lotNo,
                                    "expiryDate": expiryDate,
                                    "kitResults": kitResults,
                                    "resultNumber": trIndex
                                }
                            );
                        }
                    });
                    stockRoomId = jq(divId).find('input[name="stockRoomId"]').val();
                    let interpretation = jq(divId).find('select[name=interpretation]').val();
                    let comment = jq(divId).find('input[name="resultComment"]').val();
                    recordedSpecimenResults.push({
                        "id": jq(divId).attr("data-overlay") || '',
                        "interpretation": interpretation,
                        "comment": comment,
                        "results": results,
                        "stockRoomId": stockRoomId,
                        "quantity": results.length
                    });
                }

                if (ptSpecimenId) {
                    ptSpecimenData.push({
                        "id": specimenId,
                        "ptSpecimenId": ptSpecimenId,
                        "ptSpecimenLotNumber": ptSpecimenLotNumber,
                        "ptSpecimenExpiryDate": ptSpecimenExpiryDate,
                        "ptSpecimenConditions": ptSpecimenConditions,
                        "reasonForUnacceptablePtSpecimen": reasonForUnacceptablePtSpecimen,
                        "correctiveActions": correctiveActions,
                        "specimenResults": recordedSpecimenResults
                    });
                }
            });

            let dtsBufferData = [];
            jq('#dtsBufferTable').find('tbody tr:gt(0)').each(function() {
                dtsBufferId = jq(this).find('input[name="dtsBufferId"]').val();
                dtsLotNumber = jq(this).find('input[name="dtsLotNumber"]').val();
                dtsExpiryDate = jq(this).find('input[name="dtsExpiryDate"]').val();
                dtsConditions = jq(this).find(('select[name=dtsBufferConditions]'));
                dtsBufferConditions = dtsConditions.val();
                dtsReasonForUnacceptableBuffer = jq(this).find('input[name="dtsReasonForUnacceptableBuffer"]').val();
                dtsCorrectiveActions = jq(this).find('input[name="dtsCorrectiveActions"]').val();
                proficiencyTestingBufferId = jq(this).attr("data-overlay") || '';
                dtsBufferData.push({
                    "id": proficiencyTestingBufferId,
                    "dtsBufferId": dtsBufferId,
                    "dtsLotNumber": dtsLotNumber,
                    "dtsExpiryDate": dtsExpiryDate,
                    "dtsConditionOfBuffer": dtsBufferConditions,
                    "dtsReasonForUnacceptableBuffer": dtsReasonForUnacceptableBuffer,
                    "dtsCorrectiveActions": dtsCorrectiveActions
                })
            });

            var params = {
                'proficiencyTestingId': ${proficiencyTestingId},
                'panelId' : jq('#panelId').val(),
                'datePanelReceived' : jq('#datePanelReceived').val(),
                'receivedBy' : jq('#receivedBy').attr("data-overlay"),
                'dateResultsDue' : jq('#dateResultsDue').val(),
                'testingPoint' : jq('#testingPoint').val(),
                'specimenData' : JSON.stringify(ptSpecimenData),
                'dtsBufferData' : JSON.stringify(dtsBufferData),
                'stockRoomUuid' : stockRoomId,
                'published': published
            }
            if (getUrlParameter("action") === "report"){
                let reportData = {
                    "dateTested": jq("#dateTested").val(),
                    "resultsReceivedBy": jq("#resultsReceivedBy").attr("data-overlay"),
                    "performanceScore": jq("#performanceScore").val(),
                    "percentageScore": jq("#percentageScore").val(),
                    "overallComment": jq("#overallComment").val(),
                    "reportReviewedBy": jq("#reportReviewedBy").attr("data-overlay"),
                    "dateReviewed": jq("#dateReviewed").val()
                }
                params["reportData"] = JSON.stringify(reportData);
            }

            jq.getJSON('${ ui.actionLink("botswanaemr","hts/proficiencyTestingForm","saveProficiencyTestingForm")}', params)
                .success(function (data) {
                    jq().toastmessage('showSuccessToast', "Proficiency testing form saved successfully");
                    location.href = '${ui.pageLink("botswanaemr", "hts/proficiencyTestsList")}';
                })

        }

        jq("#saveDialog").dialog({
            modal: true,
            autoOpen: false,
            title: "Publish form?",
            width: 350,
            height: 160,
            buttons: [
                {
                    id: "Publish",
                    text: "Publish",
                    click: function () {
                        saveForm(jq(this), true);
                    }
                },
                {
                    id: "Cancel",
                    text: "Cancel",
                    click: function () {
                        jq(this).dialog('close');
                    }
                }
            ]
        });

        jq("#saveDraftProficiencyTestingFormBtn").on("click", function (e) {
            e.preventDefault();
            jq("#proficiencyTestingForm :input").prop("required", false);
            jq("#requiredSection :input").prop("required", false);
            let valid = jQuery("#proficiencyTestingForm").valid();
            if(valid){
                saveForm(jq(this), false);
            }


        });
        jq('#saveProficiencyTestingFormBtn').on('click', function (e) {
            e.preventDefault();
            jq("#requiredSection :input").prop("required", true);
            jQuery("#proficiencyTestingForm").validate();
            if (jQuery("#proficiencyTestingForm").valid()) {
                jq('#saveDialog').dialog('open');
            }

        });

        jQuery("#ptSpecimensTable tbody").on("click", "a", function () {
            jq(this).parents("tr").find('input').val('');
            jq(this).parents("tr").find('option:selected').prop("selected", false);
        });

        jQuery("#dtsBufferTable tbody").on("click", "a", function () {
            jq(this).parents("tr").find('input').val('');
            jq(this).parents("tr").find('option:selected').prop("selected", false);
        });

        jQuery(".resultsTable tbody").on("click", "a", function () {
            jq(this).parents("tr").find('input').val('');
            jq(this).parents("tr").find('option:selected').prop("selected", false);
        });

        jQuery(document).ready(function () {
            jq("input[id^=date]").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (data) {
                    jq("input[id^=date]").trigger('change');
                    jq(this).focus();
                }
            });
        });
    });
</script>
<style>
    .ui-dialog-title {
        color: #222 !important;
        background-color: transparent !important;
    }
    .ui-widget-content {
        height: auto !important;
        min-height: fit-content;
    }
    form select[disabled] {
        color: #495057;
    }
</style>

<form id="proficiencyTestingForm">
    <div class="card-body">
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="card bp-4 ml-0 mr-0">
                    <div class="row">
                        <div class="col pl-0 pr-0">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="requiredSection">
                        <div class="col pl-0 pr-0">
                            <div class="row pt-4 pb-3">
                                <div class="col pl-0">
                                    <div class="form-group">
                                        <label for="panelId">PT Panel ID:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input id="panelId" class="form-control" name="panelId" value='${htsProficiencyTesting?.panelId ?: ""}'  placeholder="PT Panel Id" required>
                                    </div>
                                </div>
                                <div class="col pr-0">
                                    <div class="form-group">
                                        <label for="datePanelReceived">Date panel received:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="datePanelReceived" class="form-control datepicker" name="datePanelReceived" value='${htsProficiencyTesting?.datePanelReceived ?: ""}' placeholder="Date panel received" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row pb-3">
                                <div class="col pl-0">
                                    <div class="form-group">
                                        <label for="receivedBy">Received by:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="receivedBy" class="form-control" name="receivedBy" value='${htsProficiencyTesting?.receivedByName ?: ""}'  data-overlay='${htsProficiencyTesting?.receivedBy ?: ""}'  placeholder="Received by" required>
                                    </div>
                                </div>
                                <div class="col-6 pr-0">
                                    <div class="form-group">
                                        <label for="dateResultsDue">Results due date:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="dateResultsDue" class="form-control" name="dateResultsDue" value='${htsProficiencyTesting?.resultsDueDate ?: ""}' placeholder="Results due date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row pb-3">
                                <div class="col-6 pl-0">
                                    <div class="form-group">
                                        <label for="testingPoint">Testing point:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="testingPoint" class="form-control" name="testingPoint" value='${htsProficiencyTesting?.testingPoint ?: ""}' placeholder="Testing point" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="col-12 pl-0 pr-0">
                            <h5 class="text-primary text-left">TEST DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="table-responsive mt-3 pt-3 pb-2">
                            <table id="ptSpecimensTable" class="table table-bordered pb-0 mb-0">
                                <thead class="border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale">PT Specimen ID</th>
                                        <th scope="col" class="bg-pale">Lot no.</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Conditions</th>
                                        <th scope="col" class="bg-pale">Reason for Unacceptable PT Specimen(s)</th>
                                        <th scope="col" class="bg-pale">Corrective Actions for Unacceptable PT Specimen(s)</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="ptSpecimenBody">
                                    <tr id="ptSpecimenTemplate">
                                        <td>
                                            <input id="ptSpecimenId" name="ptSpecimenId" type="text" class="form-control" disabled required />
                                        </td>
                                        <td>
                                            <input id="ptSpecimenLotNumber" name="ptSpecimenLotNumber" type="text" class="form-control" disabled required />
                                        </td>
                                        <td>
                                            <input id="ptSpecimenExpiryDate" name="ptSpecimenExpiryDate" type="date" class="form-control" pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" disabled required/>
                                        </td>
                                        <td>
                                            <select id="ptSpecimenConditions" name="ptSpecimenConditions" class="form-control" disabled required>
                                                <option value="">Select one</option>
                                                <option value="Acceptable">Acceptable</option>
                                                <option value="Unacceptable">Unacceptable</option>
                                            </select>
                                        </td>
                                        <td>
                                            <div id="reasonForUnacceptablePtSpecimenInput">
                                                <input id="reasonForUnacceptablePtSpecimen" name="reasonForUnacceptablePtSpecimen" type="text" class="form-control" disabled />
                                            </div>
                                        </td>
                                        <td>
                                            <input id="correctiveActions" name="correctiveActions" type="text" class="form-control" disabled />
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger">Clear</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12 pl-0 pr-0 pb-0 mt-5 pt-5">
                            <h5 class="text-primary text-left">DTS BUFFER</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="table-responsive mt-2 pt-2">
                            <table id="dtsBufferTable" class="table table-bordered">
                                <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale">DTS Buffer ID</th>
                                        <th scope="col" class="bg-pale">Lot no.</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Conditions</th>
                                        <th scope="col" class="bg-pale">Reason for Unacceptable DTS Buffer</th>
                                        <th scope="col" class="bg-pale">Corrective Actions for Unacceptable DTS Buffer</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="dtsBufferBody">
                                    <tr id="dtsBufferTemplate">
                                        <td>
                                            <input id="dtsBufferId" name="dtsBufferId" type="text" class="form-control" disabled required />
                                        </td>
                                        <td>
                                            <input id="dtsLotNumber" name="dtsLotNumber" type="text" class="form-control" disabled required />
                                        </td>
                                        <td>
                                            <input id="dtsExpiryDate" name="dtsExpiryDate" type="date" class="form-control" pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" disabled required />
                                        </td>
                                        <td>
                                            <select id="dtsBufferConditions" name="dtsBufferConditions" class="form-control" disabled required>
                                                <option value="">Select one</option>
                                                <option value="Acceptable">Acceptable</option>
                                                <option value="Unacceptable">Unacceptable</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="dtsReasonForUnacceptableBuffer" name="dtsReasonForUnacceptableBuffer" type="text" class="form-control" disabled />
                                        </td>
                                        <td>
                                            <input id="dtsCorrectiveActions" name="dtsCorrectiveActions" type="text" class="form-control" disabled />
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger">Clear</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-12 pl-0 pr-0 pt-5">
                                <h5 class="text-primary text-left">RESULTS</h5>
                                <hr class="divider pt-1 bg-primary"/>
                            </div>
                            <div class="table-responsive mt-2 pt-2" id="specimenResultSectionTemplate" style="display: none">
                                <h6 id="specimenTitle">SPECIMEN:</h6>

                                <div class="row" id="stockroom-container">
                                    <div class="col pl-0">
                                        <div class="col-6 pl-0">
                                            <div class="form-group">
                                                <label for="stockRoom">Stock Room:
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input id="stockRoom" class="form-control stockRoom" name="stockRoom" placeholder="StockRoom">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col hidden">
                                        <div class="col-6 pr-0">
                                            <div class="form-group">
                                                <label for="stockRoomId">Stock Room ID:
                                                </label>
                                                <input id="stockRoomId" class="form-control stockRoomId" name="stockRoomId" placeholder="stockRoomId">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table id="resultsTable" class="table table-bordered resultsTable">

                                    <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale"></th>
                                        <th scope="col" class="bg-pale">Kit Name</th>
                                        <th scope="col" class="bg-pale">Lot No</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Kit Results</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="resultBody">
                                        <tr id="resultTemplate">
                                            <td>t1</td>
                                            <td>
                                                <select id="kitName" name="kitName" class="form-control kitName" required>
                                                    <option disabled selected></option>
                                                    <%  kitNames.each{ %>
                                                        <option value="${it.value}">${it.key}</option>
                                                    <% } %>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="lotNo" name="lotNo" type="text" class="form-control lotNo" required/>
                                            </td>
                                            <td>
                                                <input id="expiryDate" name="expiryDate" type="date" class="form-control expiryDate"
                                                       pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                            </td>
                                            <td>
                                                <select id="kitResults" name="kitResults" class="form-control kitResults" required>
                                                    <option disabled selected></option>
                                                    <%  kitResults.each{ %>
                                                        <option value="${it.value}">${it.key}</option>
                                                    <% } %>
                                                </select>
                                            </td>
                                            <td>
                                                <a href="#" class="text-danger clear-row">Clear</a>
                                            </td>
                                        </tr>
                                        <tr id="repeatResultTemplate">
                                            <td>t2</td>
                                            <td>
                                                <select id="kitName2" name="kitName" class="form-control kitName" required>
                                                    <option disabled selected></option>
                                                    <%  secondTestKitNames.each{ %>
                                                        <option value="${it.value}">${it.key}</option>
                                                    <% } %>
                                                </select>
                                            </td>
                                            <td>
                                                <input id="lotNo2" name="lotNo" type="text" class="form-control lotNo" required/>
                                            </td>
                                            <td>
                                                <input id="expiryDate2" name="expiryDate" type="date" class="form-control expiryDate"
                                                       pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                            </td>
                                            <td>
                                                <select id="kitResults2" name="kitResults" class="form-control kitResults" required>
                                                    <option disabled selected></option>
                                                    <%  kitResults.each{ %>
                                                        <option value="${it.value}">${it.key}</option>
                                                    <% } %>
                                                </select>
                                            </td>
                                            <td>
                                                <a href="#" class="text-danger clear-row">Clear</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button class="dashed-button rounded add-more hidden" disabled
                                        id="btnAddRepeatResult">+ Add Repeat Test
                                </button>

                                <div class="row pt-4 pb-4">
                                    <div class="col-6 pl-0">
                                        <div class="form-group">
                                            <label for="interpretation">Interpretation:
                                                <span class="text-danger">*</span>
                                            </label>
                                            <select id="interpretation" name="interpretation" class="form-control hivStatus" required>
                                                <option disabled selected></option>
                                                <%  hivStatus.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-6 pr-0">
                                        <div class="form-group">
                                            <label for="resultComment">Comments:
                                            </label>
                                            <input id="resultComment" class="form-control" name="resultComment" placeholder="Comments">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="extraResults" style="min-width: 100%"></div>
                        </div>
                        <span id="reportSection">
                        <div class="col-12 pl-0 pr-0">
                            <h5 class="text-primary text-left">REPORT INFORMATION</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="row pt-4 pb-4">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="dateTested">Date Tested:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="dateTested" class="form-control" value='${htsProficiencyTesting?.dateTested ?: ""}'  name="dateTested" placeholder="Date tested">
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="resultsReceivedBy">Results received by (HTS Provider):
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="resultsReceivedBy" class="form-control" name="resultsReceivedBy" value='${htsProficiencyTesting?.resultsReceivedByName ?: ""}' data-overlay='${htsProficiencyTesting?.resultsReceivedBy ?: ""}' placeholder="Results received by">
                                </div>
                            </div>
                        </div>

                        <div class="row pt-4 pb-4">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="performanceScore">Performance score:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" id="performanceScore" class="form-control" name="performanceScore" value='${htsProficiencyTesting?.performanceScore ?: ""}' placeholder="Performance score">
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="percentageScore">Percentage score:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" id="percentageScore" class="form-control" name="percentageScore" value='${htsProficiencyTesting?.percentageScore ?: ""}' placeholder="Percentage score">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 pl-0 pr-0">
                                <div class="form-group">
                                    <label for="overallComment">Overall comment:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <textarea id="overallComment" class="form-control">${htsProficiencyTesting?.comment ?: ""}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="reportReviewedBy">Report reviewed by:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="reportReviewedBy" class="form-control" name="reportReviewedBy" value='${htsProficiencyTesting?.reportReviewedByName ?: ""}' data-overlay='${htsProficiencyTesting?.reportReviewedBy ?: ""}'
                                     placeholder="Report reviewed by">
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="dateReviewed">Date reviewed:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="dateReviewed" class="form-control" name="dateReviewed" value='${htsProficiencyTesting?.dateReviewed ?: ""}' placeholder="Date reviewed">
                                </div>
                            </div>
                        </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer border-0">
        <div class="row actions px-0">
            <div class="col-12 px-0" id="buttonSection">
                <button type="button" id="cancel" class="btn btn-md btn-dark bg-dark float-right ml-1">
                    Close
                </button>
                <button type="button" id="saveProficiencyTestingFormBtn" class="btn btn-md btn-primary float-right mr-0">
                    Save
                </button>
                <button type="button" id="saveDraftProficiencyTestingFormBtn" class="btn btn-md btn-primary float-right mr-1">
                    Save Draft
                </button>
            </div>
        </div>
    </div>
    <div id="saveDialog" style="display: none; height: auto;" align="center">
        Do you want to publish this form? Forms once published can not be edited.
    </div>
</form>

