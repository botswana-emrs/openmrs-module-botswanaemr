<script type="text/javascript">
    let ptSpecimenData = [];
    let dtsBuffersData = [];
    let specimenResults = {};

    function getUrlParameter(param) {
        let pageUrl = window.location.search.substring(1);
        let urlVariables = pageUrl.split('&');
        for (let i = 0; i < urlVariables.length; i++) {
            let paramName = urlVariables[i].split('=');
            if (paramName[0] === param) {
                return paramName[1];
            }
        }
    }

    jQuery(function () {
        jq(document).ready(function(){
            jq('#ptCondition').on('change', function() {
                if(jq(this).val() == 'Unacceptable') {
                   jq("#reasonForUnacceptableDiv").show();
                   jq("#ptCorrectiveActionsDiv").show();
                } else {
                   jq("#reasonForUnacceptableDiv").hide();
                   jq("#ptCorrectiveActionsDiv").hide();
                }
            });            
            jq('#dtsCondition').on('change', function() {
                if(jq(this).val() == 'Unacceptable') {
                   jq("#reasonForUnacceptableBufferDiv").show();
                   jq("#dtsBufferCorrectiveActionsDiv").show();
                } else {
                   jq("#reasonForUnacceptableBufferDiv").hide();
                   jq("#dtsBufferCorrectiveActionsDiv").hide();
                }
            });
       });
    });

    jQuery(function () {
        let recordedSpecimenResults = ${result};
        const YES = "1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1065";
        const NO = "1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1066";
        const POSITIVE = "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165199";
        const NEGATIVE = "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165200";
        const REACTIVE = "1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165849";
        const NONREACTIVE = "1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "165848";
        const INVALID = "163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166812";
        const INDETERMINATE = "1138AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "1138";
        const CC_COUPLE = "b92e09dd-8247-4474-9e99-8857998ad3f1" // "166759";
        const COUPLE_WITH_PREGNANT_PARTNER = "726ea800-1d8c-45d5-a543-e3e796d9bf52";  // "166773";
        const COUPLE_HIV_COUNSELLING = "72a81e09-55dd-4d04-aded-70f4e5d4d585"; // "166779";
        const PARTNER_TO_PREGNANT_MOTHER = "cff3a7aa-7deb-4435-bc46-e166c5f3626a"; // "166772";
        const PMTCT_STAGE = "6d7c3e3e-ce9f-4705-a217-2d3b71dd3e6d"; // "166771";
        const RHT = "1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166100";
        const VCT = "159940AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"; // "166942";
        const YES_HIV_POSITIVE = "11c7d569-81b2-47b6-94a8-5b27c751aeb9";

        if (!getUrlParameter("action") || !getUrlParameter("action") === "report") {
            jq("#reportSection").hide();
        } else {
            jq("#proficiencyTestingForm :input").prop("disabled", true);
            jq("#reportSection :input").prop("disabled", false);
            jq('#buttonSection :button').prop('disabled', false);
        }


        function cloneSpecimenResults(rowIndex, specimenResultId, specimenId, specimenVal, interpretation ='', comment='') {
            let template = jq("#specimenResultSectionTemplate").children().clone(true, true);

            jq('<div/>', {
                'class': 'table-responsive mt-2 pt-2 extraSpecimenResult',
                'id': 'extraSpecimenResult' + rowIndex,
                'specimenId': specimenId,
                'specimenResultId': specimenResultId,
                html: template
            }).hide().appendTo('#extraResults').slideDown('slow');

            let divId = '#extraSpecimenResult' + rowIndex ;
            jq(divId).find("#specimenTitle").html("SPECIMEN: "+ specimenVal).attr("id", "specimenTitle" + rowIndex);

            jq(divId).find("#resultsTable").attr("id", "resultsTable" + rowIndex);
            jq(divId).find("#btnAddRepeatResult").attr("id", "btnAddRepeatResult" + rowIndex);
            jq(divId).find("#kitName").attr("id", "kitName1");
            jq(divId).find("#kitResults").attr("id", "kitResults1");
            jq(divId).find("#interpretation").attr("id", "interpretation"+ rowIndex).val(interpretation);
            jq(divId).find("#resultComment").attr("id", "resultComment"+ rowIndex).val(comment);
            jq('#extraSpecimenResult' + rowIndex).attr("specimenId", specimenId);
        }

        function compareResults( a, b ) {
            if ( a.resultNumber < b.resultNumber ){
                return -1;
            }
            if ( a.resultNumber > b.resultNumber ){
                return 1;
            }
            return 0;
        }

        jq.each(recordedSpecimenResults, function (key, specimenResult) {
            let specimenResultIndex = key+1;
            cloneSpecimenResults(specimenResultIndex,specimenResult.id, specimenResult.proficiencyTestTestId, specimenResult.specimenName, specimenResult.interpretation, specimenResult.comment);
            specimenResults[specimenResultIndex] = specimenResult.specimenName;

            specimenResult.results.sort(compareResults);
            jq.each(specimenResult.results, function (key, result) {
                if (result.resultNumber && result.resultNumber > 2) {
                    addTestRow(specimenResultIndex);
                }
                let tr = jq("#resultsTable" + specimenResultIndex + " tbody tr:nth-child(" + result.resultNumber + ")");
                if (result.expiryDate) {
                    let date = new Date(result.expiryDate).toISOString().split('T')[0];
                    jq(tr).find('input[name="expiryDate"]').val(date);
                }
                jq(tr).find('td:first-of-type').html("t" + result.resultNumber);
                jq(tr).find('input[name="lotNo"]').val(result.lotNumber);
                jq(tr).find('select[name=kitResults]').val(result.kitResults);
                jq(tr).find('select[name=kitName]').val(result.kitName);
                jq(tr).attr("resultId", result.id);
            });
        });

        function addTestRow(specimenResultIndex) {
            let rowCount = jq("#resultsTable" + specimenResultIndex + " >tbody >tr").length + 1;

            if (rowCount <= 4) {
                let template = jq("#resultTemplate").clone(true, true);
                jq(template).attr("id", "result" + rowCount);

                jq(template).attr("id", "repeatResult" + rowCount);
                jq(template).find('td:first-of-type').html("t" + rowCount);
                jq(template).find('#kitName').attr("id", "kitName" + rowCount);
                jq(template).find('#kitResults').attr("id", "kitResults" + rowCount);

                jq("#resultsTable" + specimenResultIndex + " >tbody").append(template);
            }
        }

        jq('.add-more').click(function (e) {
            e.preventDefault();
            let index = this.id.charAt(this.id.length - 1);
            addTestRow(index);
            addTestRow(index);
        });

        const searchSystemUsers = function (request, response) {
            jq.getJSON(
                '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/user?v=full&q=' + request.term,
                function (data) {
                    let results = data.results.map(function (user) {
                        let label = user.person && user.person.display ? user.person.display : user.display;
                        let value = user.person.uuid;

                        return {
                            label: label,
                            value: value
                        };
                    });
                    response(results)
                });
        };

        const setupUserAutocomplete = function (fieldIdentifier) {
            let textField = jq(fieldIdentifier);

            textField.autocomplete({
                source: searchSystemUsers,
                // select: selectItem,
                minLength: 4,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(this).attr('data-overlay', ui.item.value);
                    jq(this).val(ui.item.label);
                }
            });
        }

        // Set autocomplete for system users
        setupUserAutocomplete('#resultsReceivedBy');
        setupUserAutocomplete('#reportReviewedBy');
    });

    jQuery(function () {
        jq("#dtsBufferTemplate").hide();
        jq("#addDtsBufferBtn").on("click", function (e) {
            e.preventDefault();
            let template = jq("#dtsBufferTemplate").children().clone(true, true);
            template.find('input, select').map(function () {
                jq(this).prop("disabled", false);
            });
            var dtsRowCount = jq('#dtsBufferTable >tbody >tr').length;
            jq('<tr/>', {
                'class': 'newDtsBuffer',
                'id': dtsRowCount,
                html: template
            }).hide().appendTo('#dtsBufferBody').slideDown('slow');
        });
    });

    jQuery(function () {
        var table = jQuery('#resultsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });
    });

    jQuery(function() {
        jq('#saveProficiencyTestingTestBtn').on('click', (e) => {
            var req = {
                'ptId': jq('#ptId').val(),
                'specimenId' : jq('#specimenId').val(),
                'lotNumber' : jq('#lotNumber').val(),
                'expiryDate' : jq('#expiryDate').val(),
                'ptCondition' : jq('#ptCondition').val(),
                'reasonForUnacceptablePt' : jq('#reasonForUnacceptablePt').val(),
                'ptCorrectiveActions' : jq('#ptCorrectiveActions').val()
            }
            jq.getJSON('${ ui.actionLink("botswanaemr","hts/updateProficiencyTestingForm","saveProficiencyTestingTest")}', req).success(function (data) {
                jq().toastmessage('showSuccessToast', "Proficiency test saved successfully");
                location.reload();
            })
        });

        jq('#saveProficiencyTestingBufferBtn').on('click', (e) => {
            var req = {
                'ptDtsId': jq('#ptDtsId').val(),
                'dtsBufferId' : jq('#dtsBufferId').val(),
                'dtsLotNumber' : jq('#dtsLotNumber').val(),
                'dtsExpiryDate' : jq('#dtsExpiryDate').val(),
                'dtsCondition' : jq('#dtsCondition').val(),
                'reasonForUnacceptableDts' : jq('#reasonForUnacceptableDts').val(),
                'dtsBufferCorrectiveActions' : jq('#dtsBufferCorrectiveActions').val()
            }
            jq.getJSON('${ ui.actionLink("botswanaemr","hts/updateProficiencyTestingForm","saveProficiencyTestingBuffer")}', req).success(function (data) {
                jq().toastmessage('showSuccessToast', "Proficiency buffer saved successfully");
                location.reload();
            })
        });
    });

    jQuery(function() {
        jq('#updateProficiencyTestingFormBtn').on('click', (e) => {
            e.preventDefault();
            ptSpecimenData = [];
            jq(this).find('#ptSpecimensTable tbody tr').each(function() {
                proficiencyTestingTestId = jq(this).find('input[name="proficiencyTestingTestId"]').val();
                ptSpecimenId = jq(this).find('input[name="ptSpecimenId"]').val();
                ptSpecimenLotNumber  = jq(this).find('input[name="ptSpecimenLotNumber"]').val();
                ptSpecimenExpiryDate = jq(this).find('input[name="ptSpecimenExpiryDate"]').val();
                specimenConditions   = jq(this).find(('select[name=ptSpecimenConditions]'));
                ptSpecimenConditions = specimenConditions.val();
                reasonForUnacceptablePtSpecimen = jq(this).find('input[name="reasonForUnacceptablePtSpecimen"]').val();
                correctiveActions = jq(this).find('input[name="correctiveActions"]').val();

                let rowIndex = jq("#ptSpecimenBody tr").index(jq(this));
                let ptSpecimenIdentifier = jq(this).attr("ptSpecimenId");

                let extraSpecimenResult = jq(".extraSpecimenResult[specimenid=" + ptSpecimenIdentifier  + "]");
                let recordedSpecimenResults = [];
                if (extraSpecimenResult) {
                    let results = [];
                    jq(extraSpecimenResult).find(".resultsTable tbody tr").each(function () {
                        let resultId = jq(this).attr("resultId") || '';
                        let kitNameInput = jq(this).find('select[name=kitName]');
                        let kitName = kitNameInput.val();
                        let lotNo = jq(this).find('input[name="lotNo"]').val();
                        let expiryDate = jq(this).find('input[name="expiryDate"]').val();
                        let kitResults = jq(this).find('select[name=kitResults]').val();
                        let trIndex = kitNameInput.attr("id").charAt(kitNameInput.attr("id").length - 1);

                        if (kitName) {
                            results.push(
                                {
                                    "id": resultId,
                                    "kitName": kitName,
                                    "lotNo": lotNo,
                                    "expiryDate": expiryDate,
                                    "kitResults": kitResults,
                                    "resultNumber": trIndex
                                }
                            );
                        }
                    });

                    let interpretation = jq(extraSpecimenResult).find('select[name=interpretation]').val();
                    let comment = jq(extraSpecimenResult).find('input[name="resultComment"]').val();
                    let specimenResultId = jq(extraSpecimenResult).attr("specimenResultId") || '';
                    recordedSpecimenResults.push({
                        "interpretation": interpretation,
                        "comment": comment,
                        "results": results,
                        "id": specimenResultId
                    });
                }

                ptSpecimenData.push({
                    "proficiencyTestingTestId": proficiencyTestingTestId,
                    "ptSpecimenId": ptSpecimenId,
                    "ptSpecimenLotNumber": ptSpecimenLotNumber,
                    "ptSpecimenExpiryDate": ptSpecimenExpiryDate,
                    "ptSpecimenConditions": ptSpecimenConditions,
                    "reasonForUnacceptablePtSpecimen": reasonForUnacceptablePtSpecimen,
                    "correctiveActions": correctiveActions,
                    "specimenResults": recordedSpecimenResults
                })
            });

            jq(this).find('#dtsBufferTable tbody tr:gt(0)').each(function() {
                proficiencyTestingBufferId = jq(this).find('input[name="proficiencyTestingBufferId"]').val();
                dtsBufferId = jq(this).find('input[name="dtsBufferId"]').val();
                dtsLotNumber = jq(this).find('input[name="dtsLotNumber"]').val();
                dtsExpiryDate = jq(this).find('input[name="dtsExpiryDate"]').val();
                dtsConditions = jq(this).find(('select[name=dtsConditionOfBuffer]'));
                dtsConditionOfBuffer = dtsConditions.val();
                dtsReasonForUnacceptableBuffer = jq(this).find('input[name="dtsReasonForUnacceptableBuffer"]').val();
                dtsCorrectiveActions = jq(this).find('input[name="dtsCorrectiveActions"]').val();
                dtsBuffersData.push({
                    "proficiencyTestingBufferId": proficiencyTestingBufferId,
                    "dtsBufferId": dtsBufferId,
                    "dtsLotNumber": dtsLotNumber,
                    "dtsExpiryDate": dtsExpiryDate,
                    "dtsConditionOfBuffer": dtsConditionOfBuffer,
                    "dtsReasonForUnacceptableBuffer": dtsReasonForUnacceptableBuffer,
                    "dtsCorrectiveActions": dtsCorrectiveActions
                })
            });
           
            var params = {
                'proficiencyTestingId': jq('#proficiencyTestingId').val(),
                'panelId' : jq('#panelId').val(),
                'datePanelReceived' : jq('#datePanelReceived').val(),
                'receivedBy' : jq('#receivedBy').val(),
                'dateResultsDue' : jq('#dateResultsDue').val(),
                'testingPoint' : jq('#testingPoint').val(),
                'specimenData' : JSON.stringify(ptSpecimenData),
                'dtsBuffersData' : JSON.stringify(dtsBuffersData)
            }

            if (getUrlParameter("action") === "report"){
                let reportData = {
                    "dateTested": jq("#dateTested").val(),
                    "resultsReceivedBy": jq("#resultsReceivedBy").attr("data-overlay"),
                    "performanceScore": jq("#performanceScore").val(),
                    "percentageScore": jq("#percentageScore").val(),
                    "overallComment": jq("#overallComment").val(),
                    "reportReviewedBy": jq("#reportReviewedBy").attr("data-overlay"),
                    "dateReviewed": jq("#dateReviewed").val()
                }
                params["reportData"] = JSON.stringify(reportData);
            }

            jq.getJSON('${ ui.actionLink("botswanaemr","hts/updateProficiencyTestingForm","updateProficiencyTestingForm")}', params)
                .success(function (data) {
                jq().toastmessage('showSuccessToast', "Proficiency testing form updated successfully");
                location.reload();
            });
        });

        jQuery("#ptSpecimensTable tbody").on("click", "button", function(){
            jq(this).parents("tr").remove();
        });
        
        jQuery("#dtsBufferTable tbody").on("click", "a", function () {
            jq(this).parents("tr").remove();
        });

        jQuery(document).ready(function () {
            jq("input[id^=date]").datepicker({
                dateFormat: "yy-mm-dd",
                minDate: new Date(),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                onSelect: function (data) {
                    jq("input[id^=date]").trigger('change');
                }
            });
        });
    });
</script>

<form id="proficiencyTestingForm">
    <div class="card-body">
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="card bp-4 ml-0 mr-0">
                    <div class="row">
                        <div class="col pl-0 pr-0">
                            <div class="form-group">
                                <input type="hidden" id="proficiencyTestingId" name="proficiencyTestingId" class="form-control" value='${htsProficiencyTesting?.proficiencyTestingId ?: ""}' readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col pl-0 pr-0">
                            <div class="row pt-4 pb-3">
                                <div class="col pl-0">
                                    <div class="form-group">
                                        <label for="panelId">PT Panel ID:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input id="panelId" class="form-control" name="panelId" placeholder="PT Panel Id" value='${htsProficiencyTesting?.panelId ?: ""}' required>
                                    </div>
                                </div>
                                <div class="col pr-0">
                                    <div class="form-group">
                                        <label for="datePanelReceived">Date panel received:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="datePanelReceived" class="form-control datepicker" name="datePanelReceived" placeholder="Date panel received" value='${htsProficiencyTesting?.datePanelReceived ?: ""}' required>
                                    </div>
                                </div>
                            </div>
                            <div class="row pb-3">
                                <div class="col pl-0">
                                    <div class="form-group">
                                        <label for="receivedBy">Received by:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="receivedBy" class="form-control" name="receivedBy" placeholder="Received by" value='${htsProficiencyTesting?.receivedBy ?: ""}' required>
                                    </div>
                                </div>
                                <div class="col-6 pr-0">
                                    <div class="form-group">
                                        <label for="dateResultsDue">Results due date:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="dateResultsDue" class="form-control" name="dateResultsDue" placeholder="Results due date" value='${htsProficiencyTesting?.resultsDueDate ?: ""}' required>
                                    </div>
                                </div>
                            </div>
                            <div class="row pb-3">
                                <div class="col-6 pl-0">
                                    <div class="form-group">
                                        <label for="testingPoint">Testing point:
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" id="testingPoint" class="form-control" name="testingPoint" placeholder="Testing point" value='${htsProficiencyTesting?.testingPoint ?: ""}' required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="col-12 pl-0 pr-0">
                            <h5 class="text-primary text-left">TEST DETAILS</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="table-responsive mt-3 pt-3 pb-2">
                            <table id="ptSpecimensTable" class="table table-bordered pb-0 mb-0">
                                <thead class="border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale">PT Specimen ID</th>
                                        <th scope="col" class="bg-pale">Lot no.</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Conditions</th>
                                        <th scope="col" class="bg-pale">Reason for Unacceptable PT Specimen(s)</th>
                                        <th scope="col" class="bg-pale">Corrective Actions for Unacceptable PT Specimen(s)</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="ptSpecimenBody">
                                    <% if(simplifiedHtsProficiencyTestingTests != null) {%>
                                        <% simplifiedHtsProficiencyTestingTests.each { test -> %>
                                            <tr id="ptSpecimenRow-${test?.specimenId}" ptSpecimenId="${test?.proficiencyTestingTestId}">
                                                <td>
                                                    <input type="hidden" id="proficiencyTestingTestId" name="proficiencyTestingTestId" value='${test?.proficiencyTestingTestId ?: ""}' class="form-control" readonly>
                                                    <input id="ptSpecimenId-${test?.specimenId}" name="ptSpecimenId" type="text" class="form-control" value='${test?.specimenId ?: ""}' required />
                                                </td>
                                                <td>
                                                    <input id="ptSpecimenLotNumber-${test?.specimenId}" name="ptSpecimenLotNumber" type="text" class="form-control" value='${test?.lotNumber ?: ""}' required />
                                                </td>
                                                <td>
                                                    <input id="ptSpecimenExpiryDate-${test?.specimenId}" name="ptSpecimenExpiryDate" type="text" class="form-control" value='${test?.expiryDate ?: ""}' placeholder="YYYY-MM-DD" required/>
                                                    <script type="text/javascript">
                                                        jq('#ptSpecimenExpiryDate-${test?.specimenId}').datepicker({
                                                            changeMonth: true,
                                                            changeYear: true,
                                                            showButtonPanel: true,
                                                            dateFormat: "yy-mm-dd",
                                                            onSelect: function (data) {
                                                                jq('#ptSpecimenExpiryDate-${test?.specimenId}').trigger('change');
                                                            }
                                                        });
                                                    </script>
                                                </td>
                                                <td>
                                                    <select id="ptSpecimenConditions-${test?.specimenId}" name="ptSpecimenConditions" class="form-control" required>
                                                        <option <% if (test.conditionOfSpecimen) { %> selected <% } %> value="${test?.conditionOfSpecimen ?: ""}">${test?.conditionOfSpecimen}</option>
                                                        <option value="Acceptable">Acceptable</option>
                                                        <option value="Unacceptable">Unacceptable</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div id="unacceptablePtReason-${test?.specimenId}">
                                                        <input id="reasonForUnacceptablePtSpecimen-${test?.specimenId}" name="reasonForUnacceptablePtSpecimen" type="text" class="form-control" value='${test?.reasonForUnacceptableSpecimen ?: ""}' required />
                                                    </div>
                                                </td>
                                                <td>
                                                    <input id="correctiveActions" name="correctiveActions" type="text" class="form-control" value='${test?.correctiveActions ?: ""}' required>
                                                </td>
                                                <td>
                                                    <button id="btnRowDelete-${test?.specimenId}" type="button" class="btn btn-sm btn-danger border-0 btn-outline text-danger bg-transparent">Clear</button>
                                                </td>
                                            </tr>
                                        <% } %>
                                    <% } %>
                                </tbody>
                            </table>
                            <div class="row mt-3">
                                <button id="addPtSpecimenBtn" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ptAddSpecimenModal">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                                </button>
                            </div>
                        </div>

                        <div class="col-12 pl-0 pr-0 pb-0 mt-5 pt-5">
                            <h5 class="text-primary text-left">DTS BUFFER</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="table-responsive mt-2 pt-2">
                            <table id="dtsBufferTable" class="table table-bordered">
                                <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale">DTS Buffer ID</th>
                                        <th scope="col" class="bg-pale">Lot no.</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Conditions</th>
                                        <th scope="col" class="bg-pale">Reason for Unacceptable DTS Buffer</th>
                                        <th scope="col" class="bg-pale">Corrective Actions for Unacceptable DTS Buffer</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                </thead>
                                <tbody id="dtsBufferBody">
                                    <% if(simplifiedHtsProficiencyTestingBuffers != null) { %>
                                        <% simplifiedHtsProficiencyTestingBuffers.each { buffer -> %>
                                            <tr id="dtsBufferRow-${buffer?.dtsBufferId}">
                                                <td>
                                                    <input type="hidden" id="proficiencyTestingBufferId" name="proficiencyTestingBufferId" value='${buffer?.proficiencyTestingBufferId ?: ""}' class="form-control" readonly>
                                                    <input id="dtsBufferId-${buffer?.dtsBufferId}" name="dtsBufferId" type="text" class="form-control" value='${buffer?.dtsBufferId ?: ""}' required />
                                                </td>
                                                <td>
                                                    <input id="dtsLotNumber-${buffer?.dtsBufferId}" name="dtsLotNumber" type="text" class="form-control" value='${buffer?.dtsLotNumber ?: ""}' required />
                                                </td>
                                                <td>
                                                    <input id="dtsExpiryDate-${buffer?.dtsBufferId}" name="dtsExpiryDate" type="text" class="form-control" value='${buffer?.dtsExpiryDate ?: ""}' placeholder="YYYY-MM-DD" required/>
                                                    <script type="text/javascript">
                                                        jq('#dtsExpiryDate-${buffer?.dtsBufferId}').datepicker({
                                                            changeMonth: true,
                                                            changeYear: true,
                                                            showButtonPanel: true,
                                                            dateFormat: "yy-mm-dd",
                                                            onSelect: function (data) {
                                                                jq('#dtsExpiryDate-${buffer?.dtsBufferId}').trigger('change');
                                                            }
                                                        });
                                                    </script>
                                                </td>
                                                <td>
                                                    <select id="dtsConditionOfBuffer-${buffer?.dtsBufferId}" name="dtsConditionOfBuffer" class="form-control" required>
                                                        <option <% if (buffer.dtsConditionOfBuffer) { %> selected <% } %> value="${buffer?.dtsConditionOfBuffer ?: ""}">${buffer?.dtsConditionOfBuffer}</option>
                                                        <option value="Acceptable">Acceptable</option>
                                                        <option value="Unacceptable">Unacceptable</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div id="unacceptableBufferReason-${buffer?.dtsBufferId}">
                                                        <input id="dtsReasonForUnacceptableBuffer-${buffer?.dtsBufferId}" name="dtsReasonForUnacceptableBuffer" type="text" class="form-control" value='${buffer?.dtsReasonForUnacceptableBuffer ?: ""}' required />
                                                    </div>
                                                </td>
                                                <td>
                                                    <input id="dtsCorrectiveActions" name="dtsCorrectiveActions" type="text" class="form-control" value='${buffer?.dtsCorrectiveActions ?: ""}' required>
                                                </td>
                                                <td>
                                                    <button id="btnDtsRowDelete-${buffer?.dtsBufferId}" type="button" class="btn btn-sm btn-danger border-0 btn-outline text-danger bg-transparent">Clear</button>
                                                </td>
                                            </tr>
                                        <% } %>
                                    <% } %>
                                </tbody>
                            </table>
                            <div class="row mt-3">
                                <button id="addDtsBufferBtn" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#dtsBufferModal">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 pl-0 pr-0 pt-5">
                                <h5 class="text-primary text-left">RESULTS</h5>
                                <hr class="divider pt-1 bg-primary"/>
                            </div>
                            <div class="table-responsive mt-2 pt-2" id="specimenResultSectionTemplate" style="display: none">
                                <table id="resultsTable" class="table table-bordered resultsTable">
                                    <h6 id="specimenTitle">SPECIMEN:</h6>
                                    <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale"></th>
                                        <th scope="col" class="bg-pale">Kit Name</th>
                                        <th scope="col" class="bg-pale">Lot No</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Kit Results</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="resultBody">
                                    <tr id="resultTemplate">
                                        <td>t1</td>
                                        <td>
                                            <select id="kitName" name="kitName" class="form-control kitName" required>
                                                <option disabled selected></option>
                                                <%  kitNames.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="lotNo" name="lotNo" type="text" class="form-control lotNo" required/>
                                        </td>
                                        <td>
                                            <input id="expiryDate" name="expiryDate" type="date" class="form-control expiryDate"
                                                   pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                        </td>
                                        <td>
                                            <select id="kitResults" name="kitResults" class="form-control kitResults" required>
                                                <option disabled selected></option>
                                                <%  kitResults.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger clear-row">Clear</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>t2</td>
                                        <td>
                                            <select id="kitName2" name="kitName" class="form-control kitName" required>
                                                <option disabled selected></option>
                                                <%  secondTestKitNames.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="lotNo2" name="lotNo" type="text" class="form-control lotNo" required/>
                                        </td>
                                        <td>
                                            <input id="expiryDate2" name="expiryDate" type="date" class="form-control expiryDate"
                                                   pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                        </td>
                                        <td>
                                            <select id="kitResults2" name="kitResults" class="form-control kitResults" required>
                                                <option disabled selected></option>
                                                <%  kitResults.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger clear-row">Clear</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <button class="dashed-button rounded add-more" disabled
                                        id="btnAddRepeatResult">+ Add Repeat Test
                                </button>
                                <div class="row pt-4 pb-4">
                                    <div class="col-6 pl-0">
                                        <div class="form-group">
                                            <label for="interpretation">Interpretation:
                                                <span class="text-danger">*</span>
                                            </label>
                                            <select id="interpretation" name="interpretation" class="form-control hivStatus" required>
                                                <option disabled selected></option>
                                                <%  hivStatus.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-6 pr-0">
                                        <div class="form-group">
                                            <label for="resultComment">Comments:
                                            </label>
                                            <input id="resultComment" class="form-control" name="resultComment" placeholder="Comments">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive mt-2 pt-2" id="specimenResultSectionTemplate" style="display: none">
                                <table id="resultsTable" class="table table-bordered resultsTable">
                                    <h6 id="specimenTitle">SPECIMEN:</h6>
                                    <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col" class="bg-pale"></th>
                                        <th scope="col" class="bg-pale">Kit Name</th>
                                        <th scope="col" class="bg-pale">Lot No</th>
                                        <th scope="col" class="bg-pale">Expiry Date</th>
                                        <th scope="col" class="bg-pale">Kit Results</th>
                                        <th scope="col" class="bg-pale">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody id="resultBody">
                                    <tr id="resultTemplate">
                                        <td>t1</td>
                                        <td>
                                            <select id="kitName" name="kitName" class="form-control kitName" required>
                                                <option disabled selected></option>
                                                <%  kitNames.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="lotNo" name="lotNo" type="text" class="form-control lotNo" required/>
                                        </td>
                                        <td>
                                            <input id="expiryDate" name="expiryDate" type="date" class="form-control expiryDate"
                                                   pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                        </td>
                                        <td>
                                            <select id="kitResults" name="kitResults" class="form-control kitResults" required>
                                                <option disabled selected></option>
                                                <%  kitResults.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger clear-row">Clear</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>t2</td>
                                        <td>
                                            <select id="kitName2" name="kitName" class="form-control kitName" required>
                                                <option disabled selected></option>
                                                <%  secondTestKitNames.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="lotNo2" name="lotNo" type="text" class="form-control lotNo" required/>
                                        </td>
                                        <td>
                                            <input id="expiryDate2" name="expiryDate" type="date" class="form-control expiryDate"
                                                   pattern="'\'d{4}-'\'d{2}-'\'d{2}" placeholder="YYYY-MM-DD" required/>
                                        </td>
                                        <td>
                                            <select id="kitResults2" name="kitResults" class="form-control kitResults" required>
                                                <option disabled selected></option>
                                                <%  kitResults.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger clear-row">Clear</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <button class="dashed-button rounded add-more hidden" disabled
                                        id="btnAddRepeatResult">+ Add Repeat Test
                                </button>
                                <div class="row pt-4 pb-4">
                                    <div class="col-6 pl-0">
                                        <div class="form-group">
                                            <label for="interpretation">Interpretation:
                                                <span class="text-danger">*</span>
                                            </label>
                                            <select id="interpretation" name="interpretation" class="form-control hivStatus" required>
                                                <option disabled selected></option>
                                                <%  hivStatus.each{ %>
                                                <option value="${it.value}">${it.key}</option>
                                                <% } %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-6 pr-0">
                                        <div class="form-group">
                                            <label for="resultComment">Comments:
                                            </label>
                                            <input id="resultComment" class="form-control" name="resultComment" placeholder="Comments">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="extraResults" style="min-width: 100%">

                            </div>
                        </div>

                        <span id="reportSection">
                        <div class="col-12 pl-0 pr-0">
                            <h5 class="text-primary text-left">REPORT INFORMATION</h5>
                            <hr class="divider pt-1 bg-primary"/>
                        </div>
                        <div class="row pt-4 pb-4">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="dateTested">Date Tested:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="dateTested" value='${htsProficiencyTesting?.dateTested ?: ""}' class="form-control" name="dateTested" placeholder="Date tested" required>
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="resultsReceivedBy">Results received by (HTS Provider):
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input id="resultsReceivedBy" class="form-control" value='${htsProficiencyTesting?.resultsReceivedByName ?: ""}' data-overlay='${htsProficiencyTesting?.resultsReceivedBy ?: ""}'
                                           name="resultsReceivedBy" placeholder="Results received by" required>
                                </div>
                            </div>
                        </div>

                        <div class="row pt-4 pb-4">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="performanceScore">Performance score:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" id="performanceScore" class="form-control" value='${htsProficiencyTesting?.performanceScore ?: ""}'
                                           name="performanceScore" placeholder="Performance score" required>
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="percentageScore">Percentage score:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="number" id="percentageScore" class="form-control" value='${htsProficiencyTesting?.percentageScore ?: ""}' name="percentageScore" placeholder="Percentage score" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 pl-0 pr-0">
                                <div class="form-group">
                                    <label for="overallComment">Overrall comment:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <textarea id="overallComment" class="form-control" required>${htsProficiencyTesting?.comment ?: ""}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 pl-0">
                                <div class="form-group">
                                    <label for="reportReviewedBy">Report reviewed by:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="reportReviewedBy" value='${htsProficiencyTesting?.reportReviewedByName ?: ""}' data-overlay='${htsProficiencyTesting?.reportReviewedBy ?: ""}'
                                           class="form-control" name="reportReviewedBy" placeholder="Report reviewed by" required>
                                </div>
                            </div>

                            <div class="col-6 pr-0">
                                <div class="form-group">
                                    <label for="dateReviewed">Date reviewed:
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" id="dateReviewed" value='${htsProficiencyTesting?.dateReviewed ?: ""}' class="form-control" name="dateReviewed" placeholder="Date reviewed" required>
                                </div>
                            </div>
                        </div>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-footer border-0" id="buttonSection">
        <div class="row actions px-0">
            <div class="col-12 px-0">
                <button type="button" id="cancel" class="btn btn-md btn-dark bg-dark float-right ml-1">
                    Close
                </button>
                <button type="submit" id="updateProficiencyTestingFormBtn" class="btn btn-md btn-primary float-right mr-0">
                    Save
                </button>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="ptAddSpecimenModal" tabindex="-1"
     data-controls-modal="ptAddSpecimenModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="ptAddSpecimenModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="ptAddSpecimenModalTitle">Add PT Specimen</h4>
                <button type="button" id="closePtSpecimensBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="addPtSpecimensForm">
                    <input type="hidden" class="form-control" id="ptId" name="ptId" value='${htsProficiencyTesting?.proficiencyTestingId ?: ""}' required readonly/>
                    <div class="form-group">
                        <label for="specimenId">Specimen ID:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="specimenId" name="specimenId" placeholder="Specimen ID" required/>
                    </div>
                    <div class="form-group">
                        <label for="lotNumber">Lot#
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="lotNumber" name="lotNumber" placeholder="Lot number" required/>
                    </div>
                    <div class="form-group">
                        <label for="expiryDate">Expiry Date:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="date" class="form-control" id="expiryDate" name="expiryDate" placeholder="Expiry date" required/>
                    </div>
                    <div class="form-group">
                        <label for="ptCondition">Condition of PT Specimen (Acceptable/Unacceptable):
                            <span class="text-danger">*</span>
                        </label>
                        <select  class="custom-select form-control" id="ptCondition" name="ptCondition" required>
                            <option selected value="">Select condition:</option>
                            <option value="Acceptable">Acceptable</option>
                            <option value="Unacceptable">Unacceptable</option>
                        </select>
                    </div>
                    <div id="reasonForUnacceptableDiv" class="form-group" style="display:none;">
                        <label for="reasonForUnacceptablePt">Reason for Unacceptable PT Specimen(s):</label>
                        <input type="text" class="form-control" id="reasonForUnacceptablePt" name="reasonForUnacceptablePt" placeholder="Reason for unacceptable PT" />
                    </div>
                    <div id="ptCorrectiveActionsDiv" class="form-group" style="display:none;">
                        <label for="ptCorrectiveActions">Unacceptable PT Specimen Corrective Actions:</label>
                        <input type="text" class="form-control" id="ptCorrectiveActions" name="ptCorrectiveActions" placeholder="Unacceptable PT Specimen Corrective Actions" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark" data-dismiss="modal"
                                aria-label="Close">Close</button>
                        <button id="saveProficiencyTestingTestBtn" type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  jQuery(function () {
     jQuery("#ptAddSpecimenModal").appendTo("body");
  });
</script>

<div class="modal fade" id="dtsBufferModal" tabindex="-1"
     data-controls-modal="dtsBufferModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="dtsBufferModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="dtsBufferModalTitle">Add DTS Buffer</h4>
                <button type="button" id="closeDtsBufferBtn" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="addDtsBufferForm">
                    <input type="hidden" class="form-control" id="ptDtsId" name="ptDtsId" value='${htsProficiencyTesting?.proficiencyTestingId ?: ""}' required readonly/>
                    <div class="form-group">
                        <label for="dtsBufferId">DTS Buffer ID:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="dtsBufferId" name="dtsBufferId" placeholder="DTS BufferId ID" required/>
                    </div>
                    <div class="form-group">
                        <label for="dtsLotNumber">Lot#
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="dtsLotNumber" name="dtsLotNumber" placeholder="Lot number" required/>
                    </div>
                    <div class="form-group">
                        <label for="dtsExpiryDate">Expiry Date:
                            <span class="text-danger">*</span>
                        </label>
                        <input type="date" class="form-control" id="dtsExpiryDate" name="dtsExpiryDate" placeholder="Expiry date" required/>
                    </div>
                    <div class="form-group">
                        <label for="dtsCondition">Condition of DTS Buffer (Acceptable/Unacceptable):
                            <span class="text-danger">*</span>
                        </label>
                        <select  class="custom-select form-control" id="dtsCondition" name="dtsCondition" required>
                            <option selected value="">Select condition:</option>
                            <option value="Acceptable">Acceptable</option>
                            <option value="Unacceptable">Unacceptable</option>
                        </select>
                    </div>
                    <div id="reasonForUnacceptableBufferDiv" class="form-group" style="display:none;">
                        <label for="reasonForUnacceptableDts">Reason for Unacceptable DTS Buffer(s):</label>
                        <input type="text" class="form-control" id="reasonForUnacceptableDts" name="reasonForUnacceptableDts" placeholder="Reason for unacceptable DTS Buffer" />
                    </div>
                    <div id="dtsBufferCorrectiveActionsDiv" class="form-group" style="display:none;">
                        <label for="dtsBufferCorrectiveActions">Unacceptable DTS Buffer Corrective Actions:</label>
                        <input type="text" class="form-control" id="dtsBufferCorrectiveActions" name="dtsBufferCorrectiveActions" placeholder="Unacceptable DTS Buffer Corrective Actions" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark bg-dark" data-dismiss="modal"
                                aria-label="Close">Close</button>
                        <button id="saveProficiencyTestingBufferBtn" type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
  jQuery(function () {
     jQuery("#dtsBufferModal").appendTo("body");
  });
</script>


