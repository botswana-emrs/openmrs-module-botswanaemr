<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/htsDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'startRegistration', ['returnUrl': patientPoolReturnUrl, 'action': 'self'])}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Registration</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/htsPatientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
     <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/htsAllPatientsPool')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-md fa-1x"></i>
            </div>
            <div class="nav_label">Screening Pool</div>
        </a>
    </li>

    <li>
        <a href="#" class="nav_link"
           data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <div class="nav_icon_container">
                <i class="fa fa-file-text fa-1x"></i>
            </div>

            <div class="nav_label">Stock Management</div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/products')}">
                    <i class="fas fa-file-alt"></i> Products
                </a>

                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/internalRequisitionsPg')}">
                    <i class="fas fa-file-alt"></i> Internal requisitions
                </a>

                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/externalRequisition')}">
                    <i class="fas fa-file-alt"></i> External requisitions
                </a>
<!--                
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/positiveAdjustmentsDashboard')}">
                    <i class="fas fa-file-alt"></i> Positive adjustments
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/negativeAdjustmentsDashboard')}">
                    <i class="fas fa-file-alt"></i> Negative adjustments
                </a>
-->
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/genReports')}">
                    <i class="fas fa-file-alt"></i> Gen 12 Reports
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/directReceipt')}">
                    <i class="fas fa-clipboard-check"></i> Direct Receipt (From Lab)
                </a>
                <a class="dropdown-item" href="${ui.pageLink('botswanaemr', 'stock/stockTake')}">
                    <i class="fas fa-clipboard-check"></i> Stock Take
                </a>
                <a class="dropdown-item" href="${ui.pageLink("botswanaemr", "stock/labReturns")}">
                    <i class="fas fa-trash"></i> Stock Returns
                </a>
                <a class="dropdown-item" href="${ui.pageLink("botswanaemr", "stock/expiredItems")}">
                    <i class="fas fa-file-alt"></i> Expired Items
                </a>

            </div>
        </a>
    </li>

    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/selfTestKit')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-medkit fa-1x"></i>
            </div>
            <div class="nav_label">Self-Test Inventory</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/proficiencyTestsList')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-bar-chart fa-1x"></i>
            </div>
            <div class="nav_label">Proficiency Tests</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'hts/htsReports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'lab/labQualityControl')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-clipboard fa-1x"></i>
            </div>
            <div class="nav_label">Lab Quality Control</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

