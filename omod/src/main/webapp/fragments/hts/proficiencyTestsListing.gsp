<script type="text/javascript">
    jQuery(function () {
        jQuery.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','hts/proficiencyTestsListing','getProficiencyTestListings')}",
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                console.log(data);
                var table = jQuery('#ptListingsTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'panelId'},
                        {'data': 'receivedByName'},
                        {'data': 'datePanelReceived'},
                        {'data': 'testingPoint'},
                        {'data': 'resultsDueDate'},
                        {
                            'data': null,
                            'className': 'dt-center editor-view text-primary',
                            'defaultContent': 'View',
                            'orderable': false,
                            "render": function(data, type, row, meta) {
                                if(type === 'display'){
                                    if(row.proficiencyTestingId != null) {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/proficiencyTestsForm.page?proficiencyTestingId='+row.proficiencyTestingId+'" id="view">View</a>';
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': null,
                            'className': 'dt-center editor-edit text-primary',
                            'defaultContent': 'View',
                            'orderable': false,
                            "render": function(data, type, row, meta) {
                                if(type === 'display'){
                                    if(row.proficiencyTestingId != null) {
                                        if (!row.published) {
                                            return '<a href="/${ui.contextPath()}/botswanaemr/hts/proficiencyTestsForm.page?proficiencyTestingId='+row.proficiencyTestingId+'" id="edit">Edit</a>';
                                        } else {
                                            return '<span>Published</span>';
                                        }
                                    }
                                }
                                return data;
                            }
                        },
                        {
                            'data': null,
                            'className': 'dt-center editor-delete text-danger',
                            'defaultContent': 'Delete',
                            'orderable': false,
                            "render": function(data, type, row, meta) {
                                if(type === 'display'){
                                    if(row.proficiencyTestingId != null) {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/hts/proficiencyTestsForm.page?proficiencyTestingId='+row.proficiencyTestingId+'&action=report" id="edit">Report</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    },
                    order:[]
                });
            }
        });
    });
</script>
<div class="card-body">
    <div class="row">
        <div class="col pl-0 pr-0">
            <div class="card bp-4 ml-0 mr-0">
                <div class="card-header bg-pale border border-info" id="ptSpecimens">
                    <div class="row">
                        <div class="col ml-0 pr-0">
                            <a href="${ui.pageLink('botswanaemr', 'hts/proficiencyTestsForm')}" id="addNewEntryBtn" type="button" class="btn btn-primary float-right text-white">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i> New Entry
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="ptListingsTable" class="table table-bordered">
                        <thead class="bg-pale border border-info">
                        <tr>
                            <th scope="col">PT Panel ID</th>
                            <th scope="col">Received by</th>
                            <th scope="col">Date pane received</th>
                            <th scope="col">Testing point</th>
                            <th scope="col">Results due date</th>
                            <th scope="col" class="bg-transparent border-0">Actions</th>
                            <th scope="col" class="bg-transparent border-0">&nbsp;</th>
                            <th scope="col" class="bg-transparent">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="htsProficiencyListingsBody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>