<script>
    jq(function () {
        var patientObject;
        var currentVisit;
        jq("#quick-program-creation-dialog").dialog({
            autoOpen: false,
            width: 'auto',
            position: ['center', 150],
            modal: true,
        });
        jq("#quick-program-discontinuation-dialog").dialog({
            autoOpen: false,
            width: 'auto',
            position: ['center', 150],
            modal: true,
        });
        // next add the onclick handler
        jq("#enrollInProgram").click(function () {
            jq("#quick-program-creation-dialog").dialog("open");
            return false;
        });
        jq("#discontinue").click(function () {
            jq("#quick-program-discontinuation-dialog").dialog("open");
            return false;
        });
        jq(".ui-dialog-titlebar").hide();
    });
</script>

<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <div class="ml-3 mt-3 lead nav_label">${ui.message("coreapps.clinicianfacing.overallActions")}</div>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'consultation/auxilliaryNurseDashboard?appId=botswanaemr.auxilliaryNurseDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-dashboard fa-1x"></i>
            </div>
            <div class="nav_label">Dashboard</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement?appId=botswanaemr.auxilliaryNurseDashboard')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
                    <% if (user?.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>

</ul>

