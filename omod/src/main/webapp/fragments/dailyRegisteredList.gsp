<script type="text/javascript">
    jQuery(function () {
        var table = jQuery('#registeredPatientsTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            }
        });

        jq('#inquireBtn').on('click', () => {
            jQuery('#registeredPatientsTable').DataTable().draw();

            table.search(jq('#searchPhrase').val()).draw();
            if (jq('#filterGender').val() != null) {
                table.columns(2).search(jq('#filterGender').val()).draw();
            }
        });

        jq('#resetBtn').on('click', () => {
            location.reload();
        });
    });
</script>

<div class="row">
    <div class="col-sm-6 col-md-6 col-lg-6 pl-0 pr-0 pb-6">
        <label>Search:</label>
        <input type="text" class="form-control" id="searchPhrase"
               name="searchPhrase" placeholder="Search by patient ID, Omang or Name">
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6 pr-0 pb-6">
        <label for="filterGender" class="p-r-4">Sex:</label>
         <select class="custom-select form-control border form-inline" id="filterGender">
            <option selected disabled>Select sex</option>
            <option value="M">Male</option>
            <option value="F">Female</option>
        </select>
    </div>
</div>

<div class="row p-t-6">
    <div class="col p-l-0 p-r-0">
        <div class="row">
            <div class="col-auto p-l-0">
                <button type="submit" id="inquireBtn" class="btn btn-sm btn-primary mb-3">
                    ${ui.message("Search")}
                </button>
            </div>

            <div class="col-auto">
                <button type="submit" id="resetBtn" class="btn btn-sm btn-dark bg-dark mb-3 border">
                    ${ui.message("Reset")}
                </button>
            </div>

            <div class="col p-r-0">
                <button
                        type="submit"
                        onclick="redirectToNewRegistrationPage()"
                        class="btn btn-sm btn-primary mb-3 float-right">
                    ${ui.message("Register New Patient")}
                </button>
                <script type="text/javascript">
                    let redirectToNewRegistrationPage = () => {
                        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
                    };
                </script>
            </div>
        </div>
    </div>
</div>
<table id="registeredPatientsTable"
       class="table table-striped table-sm table-bordered" style="width:100%">
    <thead>
    <tr>
        <th>Patient ID</th>
        <th>Name</th>
        <th>Sex</th>
        <th>Age</th>
        <th>RegisteredBy</th>
        <th>Registered At</th>
        <th>Asigned To</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <% todayRegisteredPatientsSummary.each { %>
    <tr>
        <td>${it.identifier}</td>
        <td>${it.name}</td>
        <td>${it.gender}</td>
        <td>${it.age}</td>
        <td>${it.registeredBy}</td>
        <td>${it.registeredDate.replace('Today', '')}</td>
        <td>${it.assignedTo}</td>
        <td>
            <a href="/${ui.contextPath()}/botswanaemr/patientProfile.page?patientId=${it.patientId}"class="color-primary">Profile</a>
            <% if (it.patientQueueId) { %>
             |&nbsp;<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId=${it.patientUuid}&visitId=${it.visitNumber}&patientQueueId=${it.patientQueueId}" class="text-primary pl-0" id="reassign">Reassign</a>
            <% } else { %>
             |&nbsp;<a href="/${ui.contextPath()}/botswanaemr/assignPatient.page?patientId=${it.patientUuid}&visitId=${it.visitNumber}" class="text-primary pl-0" id="reassign">Assign</a>
            <% } %>
        </td>
    </tr>
    <% } %>
    </tbody>
</table>
