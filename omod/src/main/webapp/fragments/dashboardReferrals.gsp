<h5>Referrals</h5>
<div class="clearfix"></div>
<ul class="list-group">
    <li class="list-group-item">
        <% if (!referrals.isEmpty()) {%>
            <%  referrals.each { %>
            <ul class="list-unstyled">
                <li>
                    <div class="col p-0"><small><span>Patient:</span> ${it.patient.personName}</small></div>
                    <% it.encounter.obs.each { %>
                    <% if(it.concept.equals(referringObsConcept)){%>
                    <div class="col p-0"><small><span>Referred From:</span> ${it.comment}</small></div>
                    <% } else if (it.concept.equals(receivingObsConcept)){%>
                    <div class="col p-0"><small><span>Referred to:</span> ${it.comment}</small></div>
                    <% } %>
                    <% } %>
                    <div class="row">
                        <% if(it.fulfillerStatus.toString() == "COMPLETED"){%>
                        <div class="col p-0">
                            <small>
                                <i class="fa fa-circle text-success"></i>
                                <span class="text-muted px-0" style="background: none;"> Completed </span>
                            </small>
                        </div>
                        <% } else { %>
                        <div class="col p-0">
                            <small>
                                <i class="fa fa-circle text-warning"></i>
                                <span class="text-muted px-0" style="background: none;"> Awaiting </span>
                            </small>
                        </div>
                        <% } %>
                        <div class="col p-0">
                            <span class="text-success float-right">
                                <a href="/${ui.contextPath()}/botswanaemr/consultation/consultation.page?patientId=${it.patient.id}&visitId=${it.encounter.visit.id}"
                                   class="text-muted"> View
                                </a>
                            </span>
                        </div>
                    </div>
                    <hr/>
                </li>
            </ul>
        <% } %>
        <br/>
        <% } %>
    </li>
</ul>
