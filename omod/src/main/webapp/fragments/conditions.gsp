<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/lodash.min.js")
    ui.includeJavascript("botswanaemr", "lib/restangular/1.5.2/restangular.min.js")
    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js")
    ui.includeJavascript("coreapps", "conditionlist/models/model.module.js")
    ui.includeJavascript("coreapps", "conditionlist/models/concept.model.js")
    ui.includeJavascript("coreapps", "conditionlist/models/condition.model.js")
    ui.includeJavascript("coreapps", "conditionlist/emr.messages.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("coreapps", "conditionlist/controllers/manageconditions.controller.js")
    ui.includeJavascript("coreapps", "conditionlist/common.functions.js")
    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
    ui.includeCss("coreapps", "conditionlist/conditions.css")
%>
<script>
    const patientUuid = "${patient.patient.uuid}";
    let isProcessing = false;
    let conditionHistoryList = [];

    function getConditions(patientUuid) {
        isProcessing = true;
        const baseUrl = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/emrapi/conditionhistory';

        if (!patientUuid) {
            const urlParams = new URLSearchParams(window.location.search);
            patientUuid = urlParams.get('patientId');
        }

        if (patientUuid) {
            fetch(baseUrl + '?patientUuid=' + patientUuid, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                conditionHistoryList = data;
                console.log('conditionHistoryList', conditionHistoryList);
                populateConditionList(conditionHistoryList);
            })
            .catch(error => {
                console.error('Failed to load conditions data:', error);
            })
            .finally(() => {
                isProcessing = false;
            });
        } else {
            console.error('No patient UUID provided.');
            isProcessing = false;
        }
    }

    //string template
    function populateConditionList(conditionHistoryList) {
        const conditionList = document.getElementById('conditionList');
        const noDataSection = document.getElementById('noData');
        const notesHeader = document.getElementById('notesHeader');

        conditionList.innerHTML = '';

        if (conditionHistoryList.length === 0) {
            noDataSection.style.display = 'block';
        } else {
            noDataSection.style.display = 'none';
            notesHeader.style.display = 'block';

            conditionHistoryList.forEach(conditionHistory => {
                const condition = conditionHistory.conditions[conditionHistory.conditions.length - 1];
                if (condition.status === 'ACTIVE' && !condition.voided) {
                    const listItem = `
                        <li class="conditionStatus">
                            <div class="row">
                                <div class="col-6 zero-padding">
                                    <span class="p normal-text">\${condition.concept.name}</span>
                                </div>
                                <div class="col-6 zero-padding" style="display: \${condition.additionalDetail && condition.additionalDetail !== '' && condition.additionalDetail !== 'null' ? 'block' : 'none'};">
                                    <span class="p normal-text">\${condition.additionalDetail}</span>
                                </div>
                                <div class="col-6 zero-padding" style="display: \${condition.additionalDetail === '' || condition.additionalDetail === null || condition.additionalDetail === 'null' ? 'block' : 'none'};">
                                    <span class="p normal-text">-</span>
                                </div>
                            </div>
                        </li>
                    `;
                    conditionList.innerHTML += listItem;
                }
            });
        }
    }

    jQuery(function () {
        jQuery("#editConditionsModal").appendTo("body");
        
        jQuery('#editConditionsModal').on('show.bs.modal', function () {
            jQuery("#conditionsBody").find("input").prop("disabled", true);
            jQuery('#saveConditions').prop('disabled', false);
        });

        jQuery('#editConditionsModal').on('hide.bs.modal', function () {
            jQuery("#conditionsBody").find("input").prop("disabled", true);
            
        // Remove empty rows
        jQuery("#conditionsBody").find("tr.newCondition").each(function () {
            const inputs = jQuery(this).find("input");
            const isEmpty = inputs.toArray().every(input => jQuery(input).val().trim() === "");
                if (isEmpty) {
                    jQuery(this).remove();
                }
            });
        });

        //async fetch conditions on page load
        getConditions(patientUuid);
    });
</script>

<div class="row">
    <div class="col-12">
        <div class="info-section conditions" id="conditionsSection">
            <div class="info-header">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                        <h5>${ui.message('coreapps.conditionui.conditions')}</h5>
                    </div>

                    <div class="col-6 zero-padding text-left">
                        <h5 class="h5" id="notesHeader" style="display: none;">Notes</h5>
                    </div>
                </div>
            </div>

            <div class="info-body" id="infoBody">
                <ul id="conditionList"></ul>

                <div class="row no-data-section text-center" id="noData"  style="display: none;">
                    <img class=""
                         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

                    <p class="text-center">No data captured</p>
                </div>
            </div>
            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editConditionsModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add comorbidity</button>
            </div>

            <div id="remove-condition-dialog" class="alert alert-info" role="alert"
                 style="display: none; position: absolute; left: 25%; top:30%;">
                <div class="dialog-header">
                    <h4 class="alert-heading">
                        <i class="icon-info-sign"></i> ${ui.message("coreapps.conditionui.removeCondition")}
                    </h4>
                </div>

                <p class="lead">
                    <span id="removeConditionMessage">${ui.message("coreapps.conditionui.removeCondition.message", "")}</span>
                </p>
                <hr>
                <button class="btn btn-sm btn-success float-right" type="submit"
                        ng-click="removeCondition()">${ui.message("general.yes")}</button>
                <button class="btn btn-sm btn-dark bg-dark float-right m-1" ng-click="cancelDeletion()">${ui.message("general.no")}</button>
            </div>
        </div>
    </div>
</div>

<!-- Nodal -->
<div class="modal fade" id="editConditionsModal" tabindex="-1"
     data-controls-modal="editConditionsModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editConditionsTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editConditionsTitle">Edit comorbidities</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "consultation/editConditions", [patientId: patient.patient.uuid]) }
                </div>
            </div>
        </div>
    </div>
</div>
