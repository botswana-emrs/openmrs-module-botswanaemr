<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script>
    let dietPatientId = '${patient.patient.uuid}';
    let dietVisitId = '${visit.uuid}';

    jQuery(function () {
        jQuery("#editDietModal").appendTo("body");

        fetchDietData();
    
        jQuery('#editDietModal').on('show.bs.modal', function () {
            jQuery('#saveDiet').prop('disabled', false);
        });
    });

    function fetchDietData() {
        jq.getJSON('${ui.actionLink("botswanaemr", "diet", "getDietData")}',
        {'patientId': dietPatientId,'visitId': dietVisitId}
        ).done(function (data) {
            renderDietData(data);
        }).fail(function (jqXHR) {
            console.error('Error loading lifestyle data');
        });
    }

    function renderDietData(data) {
        const dietInfoContainer = document.querySelector(".diet-info ul");
        dietInfoContainer.innerHTML = "";

        if (data && data.length > 0) {
            data.forEach(dietObject => {
                const dietValue = dietObject.diet && dietObject.diet.valueText ? dietObject.diet.valueText : "-";
                const dietAmount = dietObject.amount && dietObject.amount.valueNumeric ? dietObject.amount.valueNumeric : "-";
                const dietComment = dietObject.comment && dietObject.comment.valueText ? dietObject.comment.valueText : "-";

                const dietItemHtml = `
                    <li>
                        <div class="row">
                            <!-- Diet Value -->
                            <div class="col-6 zero-padding">
                                <span class="p normal-text">\${dietValue}</span>
                            </div>

                            <!-- Diet Amount -->
                            <div class="col-2 zero-padding">
                                <span class="p normal-text">\${dietAmount}</span>
                            </div>

                            <!-- Diet Comment (or Frequency, based on your data display) -->
                            <div class="col-4 zero-padding">
                                <span class="p normal-text">\${dietComment}</span>
                            </div>
                        </div>
                    </li>
                `;

                dietInfoContainer.insertAdjacentHTML("beforeend", dietItemHtml);
            });
        } else {
            dietInfoContainer.innerHTML = `
                <div class="row no-data-section text-center">
                    <img src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data" />
                    <p class="text-center">No data captured</p>
                </div>
            `;
        }
    }
</script>
<div class="row">
    <div class="col-12">
        <% if (visit == null) {%>
        <div class="col col-sm-12 col-md-12">
            <div class="alert alert-warning text-danger" role="alert">
                <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!</strong>
            </div>
        </div>
        <%} else {%>
        <div class="info-section conditions">
            <div class="info-header">
                <div class="row">
                    <div class="col-6 zero-padding text-left">
                        <h5>${ui.message("Patient's current diet")}</h5>
                    </div>

                    <div class="col-2 zero-padding text-left">
                        <h5 class="h5">Amount</h5>
                    </div>

                    <div class="col-4 zero-padding text-left">
                        <h5 class="h5">Frequency</h5>
                    </div>
                </div>
            </div>

            <div class="info-body diet-info">
                <ul></ul>
            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editDietModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Patient Diet</button>
            </div>

        </div>
        <%}%>
    </div>
</div>
<!-- Nodal -->
<% if (visit != null) {%>
<div class="modal fade" id="editDietModal" tabindex="-1"
     data-controls-modal="editDietModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editDietModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editDietModalTitle">Edit Patient Diet</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "consultation/editDiet", [patientId: patient.patient.uuid, visit: visit]) }
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
