<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script>

    const pastOperationsVisitUuid = "${visit.uuid}";

    function loadOperations() {
        jq.getJSON('${ui.actionLink("botswanaemr", "pastOperations", "getPastOperations")}',
            {'visitUuid': pastOperationsVisitUuid}
        ).done(function (pastOperationObjects) {
            renderPastOperations(pastOperationObjects);
        }).fail(function (jqXHR) {
            console.error('Error loading operations');
        });
    }

    function renderPastOperations(pastOperationObjects) {
        const operationsList = pastOperationObjects.map(function(pastOperationObject) {
            return `
                <li>
                    <div class="row">
                        <div class="col-5 zero-padding">
                            <span class="p normal-text">
                                \${pastOperationObject.pastOperation && 
                                pastOperationObject.pastOperation.name !== "Other" 
                                ? pastOperationObject.pastOperation 
                                : pastOperationObject.pastOperation || "-"}
                            </span>
                        </div>

                        <div class="col-3 zero-padding">
                            <div>
                                <span class="p normal-text">
                                    \${pastOperationObject.year || "-"}
                                </span>
                            </div>
                        </div>

                        <div class="col-4 zero-padding">
                            <div>
                                <span class="p normal-text">
                                    \${pastOperationObject.comment || "-"}
                                </span>
                            </div>
                        </div>
                    </div>
                </li>`;
        }).join('');

        jQuery('.operations-info ul').html(operationsList);

        if (pastOperationObjects.length === 0) {
            jQuery('.no-data-section').show();
        } else {
            jQuery('.no-data-section').hide();
        }
    }

    jQuery(function () {
        jQuery("#editPastOperationsModal").appendTo("body");

        jQuery('#editPastOperationsModal').on('show.bs.modal', function () {
            loadOperations();
        });

        loadOperations();
    });
</script>
<div class="row">
    <% if (visit == null) {%>
    <div class="col col-sm-12 col-md-12">
        <div class="alert alert-warning text-danger" role="alert">
            <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!</strong>
        </div>
    </div>
    <%} else {%>
    <div class="col-12">
        <div class="info-section pastOperations" ng-app="triageDataApp" ng-controller="TriageDataController"
             ng-init="getOperationsData('${visit.uuid}')">
            <div class="info-header">
                <div class="row">
                    <div class="col-5 zero-padding text-left">
                        <h5>${ui.message('Past Operations')}</h5>
                    </div>

                    <div class="col-3 zero-padding text-left">
                        <h5 class="h5" ng-show="operationsObservations.length > 0">Year</h5>
                    </div>

                    <div class="col-4 zero-padding text-left">
                        <h5 class="h5" ng-show="operationsObservations.length > 0">Notes</h5>
                    </div>
                </div>
            </div>

            <div class="info-body operations-info" >
                <ul></ul>

                <div ng-show="operationsObservations.length == 0" class="row no-data-section text-center">
                    <img class=""
                         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

                    <p class="text-center">No data captured</p>
                </div>
            </div>

            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editPastOperationsModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Past Operation</button>
            </div>

        </div>
    </div>
    <!-- Nodal -->
    <div class="modal fade" id="editPastOperationsModal" tabindex="-1"
         data-controls-modal="editPastOperationsModal" data-backdrop="static"
         data-keyboard="false" role="dialog" aria-hidden="true"
         aria-labelledby="editPastOperationsModalTitle">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title text-white" id="editPastOperationsModalTitle">Edit Past Operations</h4>
                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        ${ ui.includeFragment("botswanaemr", "consultation/editPastOperations", [patientId: patient.patient.uuid, visit: visit]) }
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%}%>
</div>

