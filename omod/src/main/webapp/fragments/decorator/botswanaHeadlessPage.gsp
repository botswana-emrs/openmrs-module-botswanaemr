<%
    sessionContext.requireAuthentication()
    def title = config.title ?: ui.message("emr.title")
    def timezoneOffset = -Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis()) / 60000
    def jsTimezone = new java.text.SimpleDateFormat("ZZ").format(new Date());

    // Include bootstrap unless specifically excluded
    def includeBootstrap = config.containsKey('includeBootstrap') ? config.includeBootstrap : true;
	ui.includeFragment("appui", "standardEmrIncludes", [ includeBootstrap: includeBootstrap ])
    if (includeBootstrap) {
        ui.includeJavascript("appui", "popper.min.js")
        ui.includeJavascript("appui", "bootstrap.min.js")
        ui.includeCss("appui", "bootstrap.min.css")
    }
    else {
        ui.includeCss("appui", "no-bootstrap.css")
    }
    ui.includeCss("botswanaemr", "font-awesome.min.css")
    ui.includeCss("botswanaemr", "sidebar.css")
    ui.includeCss("botswanaemr", "helper.css")
    ui.includeCss("botswanaemr", "style.css")
    ui.includeCss("botswanaemr", "main.css")
    ui.includeCss("botswanaemr", "wizard.css")
    ui.includeCss("botswanaemr", "dataTables.css")

    ui.includeCss("botswanaemr", "jquery.dataTables.min.css")
    ui.includeCss("botswanaemr", "buttons.dataTables.min.css")
    ui.includeCss("botswanaemr", "select.dataTables.min.css")

    ui.includeJavascript("botswanaemr", "jquery.lineProgressbar.js")
    ui.includeJavascript("botswanaemr", "jquery.validate.min.js")

    ui.includeJavascript("botswanaemr", "utils.js")

    ui.includeJavascript("botswanaemr", "datatables.min.js")
    ui.includeJavascript("botswanaemr", "pace.min.js")
    ui.includeJavascript("botswanaemr", "tab-loader.js")
    ui.includeJavascript("botswanaemr", "sidebar.js")
    ui.includeJavascript("botswanaemr", "wizard.js")
    ui.includeJavascript("botswanaemr", "jquery.cookie.js")

    ui.includeJavascript("botswanaemr", "dataTables.select.min.js")
    ui.includeJavascript("botswanaemr", "buttons.html5.min.js")
    ui.includeJavascript("botswanaemr", "dataTables.buttons.min.js")
    ui.includeJavascript("botswanaemr", "dwr-util.js")
    ui.includeJavascript("botswanaemr", "vfs_fonts.js")

    ui.includeCss("referenceapplication", "referenceapplication.css", 100)
    ui.includeJavascript("botswanaemr", "highcharts.js")
    ui.includeJavascript("botswanaemr", "botswanaemr-utilis.js")
%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${ title ?: "OpenMRS" }</title>
        <link rel="shortcut icon" type="image/ico" href="/${ ui.contextPath() }/images/openmrs-favicon.ico"/>
        <link rel="icon" type="image/png\" href="/${ ui.contextPath() }/images/openmrs-favicon.png"/>
        <!-- Latest compiled and minified CSS -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${ ui.resourceLinks() }
        <script src="/${ui.contextPath()}/csrfguard" type="text/javascript"></script>

        <script type="text/javascript">
            const OPENMRS_CONTEXT_PATH = '${ ui.contextPath() }';
            const openmrsContextPath = '/' + OPENMRS_CONTEXT_PATH;
            window.sessionContext = window.sessionContext || {
                locale: "${ ui.escapeJs(sessionContext.locale.toString()) }"
            };
            window.translations = window.translations || {};
            window.openmrs = {
                server: {
                    timezone: "${ jsTimezone }",
                    timezoneOffset: ${ timezoneOffset }
                }
            }


            const checkForSession = () => {
                jq.ajax({
                    // url: "/"+OPENMRS_CONTEXT_PATH + "/ws/rest/v1/session?v=ref",
                    url:  '${ ui.actionLink("botswanaemr", "search", "sessionStatus") }',
                    complete: (result) => {
                        result.then(data => {
                            if (data && !data.active) {
                                //redirects to login page.
                                window.location = "/" + OPENMRS_CONTEXT_PATH + "/login.htm";
                            }
                        })
                    }
                })
            }

            jq(document).ready(function() {
                setInterval(checkForSession, 600000);
            });
        </script>
    </head>

    <body>
        <div class="page-container">
            <!-- Page Content Holder -->
            <main id="content" class="main">
            ${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}

                <!-- /# Content: loaded differently for each page that uses this decorator -->
                <div id="content-area" class="mt-0">
                    <%= config.content %>
                </div>
            </main>
        </div>
    </body>
</html>
