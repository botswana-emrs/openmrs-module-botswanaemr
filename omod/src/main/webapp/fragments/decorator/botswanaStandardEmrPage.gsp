<%
    sessionContext.requireAuthentication()
    def title = config.title ?: ui.message("emr.title")
    def timezoneOffset = -Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis()) / 60000
    def jsTimezone = new java.text.SimpleDateFormat("ZZ").format(new Date());

    // Include bootstrap unless specifically excluded
    def includeBootstrap = config.containsKey('includeBootstrap') ? config.includeBootstrap : true;
	ui.includeFragment("appui", "standardEmrIncludes", [ includeBootstrap: includeBootstrap ])
    if (!isScreeningPage) {
        ui.includeJavascript("botswanaemr", "jquery-3.6.0.js")

    }

    if (includeBootstrap) {
        ui.includeJavascript("appui", "popper.min.js")
        ui.includeJavascript("appui", "bootstrap.min.js")
        ui.includeCss("appui", "bootstrap.min.css")
    }
    else {
        ui.includeCss("appui", "no-bootstrap.css")
    }
    ui.includeCss("botswanaemr", "font-awesome.min.css")
    ui.includeCss("botswanaemr", "sidebar.css")
    ui.includeCss("botswanaemr", "helper.css")
    ui.includeCss("botswanaemr", "style.css")
    ui.includeCss("botswanaemr", "main.css")
    ui.includeCss("botswanaemr", "wizard.css")
    ui.includeCss("botswanaemr", "dataTables.css")

    ui.includeCss("botswanaemr", "jquery.dataTables.min.css")
    ui.includeCss("botswanaemr", "buttons.dataTables.min.css")
    ui.includeCss("botswanaemr", "select.dataTables.min.css")

    ui.includeJavascript("botswanaemr", "jquery.lineProgressbar.js")
    ui.includeJavascript("botswanaemr", "jquery.validate.min.js")
    if (!isScreeningPage) {
        ui.includeJavascript("botswanaemr", "jquery-ui-1.13.1.min.js")

    }

    ui.includeJavascript("botswanaemr", "utils.js")
    
    ui.includeJavascript("botswanaemr", "datatables.min.js")
    ui.includeJavascript("botswanaemr", "pace.min.js")
    ui.includeJavascript("botswanaemr", "tab-loader.js")
    ui.includeJavascript("botswanaemr", "sidebar.js")
    ui.includeJavascript("botswanaemr", "wizard.js")
    ui.includeJavascript("botswanaemr", "jquery.cookie.js")
    ui.includeJavascript("botswanaemr", "intlTelInput-jquery.js")

    ui.includeJavascript("botswanaemr", "dataTables.select.min.js")
    ui.includeJavascript("botswanaemr", "buttons.html5.min.js")
    ui.includeJavascript("botswanaemr", "dataTables.buttons.min.js")
    ui.includeJavascript("botswanaemr", "dwr-util.js")
    ui.includeJavascript("botswanaemr", "pdfmake.min.js")
    ui.includeJavascript("botswanaemr", "vfs_fonts.js")


    ui.includeCss("referenceapplication", "referenceapplication.css", 100)
    ui.includeJavascript("botswanaemr", "highcharts.js")
    ui.includeJavascript("botswanaemr", "botswanaemr-utilis.js")
    ui.includeCss("botswanaemr", "intlTelInput.css")
    ui.includeCss("botswanaemr", "icd11/icd11ect-1.6.1.css")

%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${ title ?: "OpenMRS" }</title>
        <link rel="shortcut icon" type="image/ico" href="/${ ui.contextPath() }/images/openmrs-favicon.ico"/>
        <link rel="icon" type="image/png\" href="/${ ui.contextPath() }/images/openmrs-favicon.png"/>
        <!-- Latest compiled and minified CSS -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        ${ ui.resourceLinks() }
        <script src="/${ui.contextPath()}/csrfguard" type="text/javascript"></script>

        <script type="text/javascript">
            const OPENMRS_CONTEXT_PATH = '${ ui.contextPath() }';
            const openmrsContextPath = '/' + OPENMRS_CONTEXT_PATH;
            window.sessionContext = window.sessionContext || {
                locale: "${ ui.escapeJs(sessionContext.locale.toString()) }"
            };
            window.translations = window.translations || {};
            window.openmrs = {
                server: {
                    timezone: "${ jsTimezone }",
                    timezoneOffset: ${ timezoneOffset }
                }
            }

            if(${isAnonymousLocation}){
                window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/selectServicePoint.page";
            }

            const checkForSession = () => {
                jq.ajax({
                    // url: "/"+OPENMRS_CONTEXT_PATH + "/ws/rest/v1/session?v=ref",
                    url:  '${ ui.actionLink("botswanaemr", "search", "sessionStatus") }',
                    complete: (result) => {
                        result.then(data => {
                            if (data && !data.active) {
                                //redirects to login page.
                                window.location = "/" + OPENMRS_CONTEXT_PATH + "/login.htm";
                            }
                        })
                    }
                })
            }

            const getModuleVersionNumber = function () {
                jQuery.ajax({
                    url: '/${ui.contextPath()}/ws/rest/v1/module/botswanaemr?v=custom:(uuid,name,version)',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        var botswanaEmrModule = data;
                        
                        if (botswanaEmrModule) {
                            jQuery('#module-version').text("Module:" + botswanaEmrModule.version);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error('Error fetching module information:', error);
                    }
                });                
            }
            <% if (isSuperUser) { %>
                const getPlatformVersion = function () {
                    jQuery.ajax({
                        url: '/${ui.contextPath()}/ws/rest/v1/systeminformation',
                        type: 'GET',
                        dataType: 'json',
                        success: function(data) {
                            // console.log(data.systemInfo['SystemInfo.title.openmrsInformation']['SystemInfo.OpenMRSInstallation.openmrsVersion']);
                            if (data && data.systemInfo) {
                                let openMRSInstallation = data.systemInfo["SystemInfo.title.openmrsInformation"];
                                var platformVersion = openMRSInstallation['SystemInfo.OpenMRSInstallation.openmrsVersion'];
                                // console.log(platformVersion);
                                jQuery('#platform-version').text("Platform: " + platformVersion);
                            }
                        },
                        error: function(xhr, status, error) {
                            console.error('Error fetching platform version:', error);
                        }
                    });
                }

                jq(document).ready(function() {
                    setInterval(checkForSession, 600000);
                        getModuleVersionNumber();
                        getPlatformVersion();

                    //active tab    
                    var currentUrl = window.location.pathname;
                    var navLinks = jq('.nav_link');
                    navLinks.each(function() {
                        if (jq(this).attr('href').indexOf(currentUrl) !== -1) {
                            jq(this).addClass('active');
                        }
                    });        
                });
            <% } %>
            
        </script>
    </head>

    <body>
        ${ui.includeFragment("botswanaemr", "widget/pageLoadingOverlay")}
        <div class="page-container">
            <!-- Sidebar Holder -->
            <nav class="sidebar bg-black pr-1 nav">
                <% if(registration){ %>
                <ul class="list-unstyled components">
                    <li class="pt-2">
                        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
                    </li>
                    <li>
                        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
                    </li>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'registrationAdminDashboard?appId=botswanaemr.registrationAdminDashboard')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-dashboard fa-1x"></i>
                            </div>
                            <div class="nav_label">Dashboard</div>
                        </a>
                    </li>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'registerPatient')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-plus fa-1x"></i>
                            </div>
                            <div class="nav_label">Patient Registration</div>
                        </a>
                    </li>
                    <% if (!isRegistration) { %>
                        <li>
                            <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
                                <div class="nav_icon_container">
                                    <i class="fa fa-gear fa-1x"></i>
                                </div>
                                <div class="nav_label">Patient Management</div>
                            </a>
                        </li>
                        <!--
                        <li>
                            <a href="${ui.pageLink('botswanaemr', 'appointments/patientFollowup')}" class="nav_link">
                                <div class="nav_icon_container">
                                    <i class="fa icon-phone fa-1x"></i>
                                </div>
                                <div class="nav_label">Patient Followup</div>
                            </a>
                        </li>
                        -->
                    <% } %>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'registrationScreeningPool')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-dashboard fa-1x"></i>
                            </div>
                            <div class="nav_label">Screening Patient Pool</div>
                        </a>
                    </li>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-calendar fa-1x"></i>
                            </div>
                            <div class="nav_label">Appointments Management</div>
                        </a>
                    </li>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'paymentsPortal')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-money fa-1x"></i>
                            </div>
                            <div class="nav_label">Payments Portal</div>
                        </a>
                    </li>
                    <li>
                        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
                            <div class="nav_icon_container">
                                <i class="fa fa-line-chart fa-1x"></i>
                            </div>
                            <div class="nav_label">Reports</div>
                        </a>
                    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
                </ul>
                <% } else if(nurse){%>
                    ${ui.includeFragment("botswanaemr", "nurseAction")}
                <% } else if(doctor){%>
                    ${ui.includeFragment("botswanaemr", "consultation/doctorAction")}
                <% } else if(art){%>
                    ${ui.includeFragment("botswanaemr", "art/artAction")}
                <%} else if(isPatientQueue){%>
                    ${ui.includeFragment("botswanaemr", "patientQueueList")}
                <% } else if(pharmacy){%>
                    ${ui.includeFragment("botswanaemr", "pharmacy/pharmacyAction")}
                <% } else if(stockManagementPortal){%>
                    ${ui.includeFragment("botswanaemr", "stock/stockManagementAction")}
                <% } else if(labServicesPortal){%>
                    ${ui.includeFragment("botswanaemr", "lab/labAction")}
                <% } else if(vmmcPortal){%>
                    ${ui.includeFragment("botswanaemr", "vmmc/vmmcAction")}
                <% } else if(sexualReproductiveHealthPortal){%>
                    ${ui.includeFragment("botswanaemr", "srh/srhAction")}
                <% } else if(tbServicesPortal){ %>
                    ${ui.includeFragment("botswanaemr", "tbservices/tbAction")}
                <% } else if(hivTestingServicesPortal) { %>
                    ${ui.includeFragment("botswanaemr", "hts/htsAction")}
                <%}%>

                <ul class="list-unstyled components version-info">
                    <li id="module-version">
                        <span></span>
                    </li>
                    <li id="platform-version">
                        <span></span>
                    </li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <main id="content" class="main">

                <!-- /# BotswanaEmrHeader -->
                ${ ui.includeFragment("botswanaemr", "botswanaEMRHeader", [ useBootstrap: includeBootstrap, isRegistrationPage: isRegistrationPage ]) }

                <div class="row breadcrumbs-row">
                    <div class="col float-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ul id="breadcrumbs" class="col-12 breadcrumb"></ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# Content: loaded differently for each page that uses this decorator -->
                <div id="content-area" class="mt-0">
                    <%= config.content %>
                </div>

            </main>
        </div>

        <script type="text/javascript">


            jq(function() {
                // Override this method to bypass conflicts brought about by introduction of the
                // WHO toolkit which uses ReactJs
                // emr.updateBreadcrumbs();

                let breadcrumbTemplate = jq('#breadcrumb-template').html();

                jq.each(breadcrumbs, function (index, breadcrumb) {
                    let first = (index === 0);
                    let last = (index === breadcrumbs.length - 1);

                    let itemHtml = jq('<li/>');
                    if (!first) {
                        itemHtml.append('<i class="icon-chevron-right link"></i>');
                    }
                    let a = jq('<a/>');
                    if (!last && breadcrumb.link) {
                        a.attr('href', breadcrumb.link);
                    }

                    if (breadcrumb.icon) {
                        a.append('<i class="'+ breadcrumb.icon + '  small"></i>');
                    }

                    if (breadcrumb.label) {
                        a.append(breadcrumb.label);
                    }

                    if (last || !breadcrumb.link) {
                        itemHtml.append(breadcrumb.label);
                    } else {
                        itemHtml.append(a);
                    }

                    jq('#breadcrumbs').append(itemHtml);
                });

            });
            // global error handler
            jq(document).ajaxError(function(event, jqxhr) {
                emr.redirectOnAuthenticationFailure(jqxhr);
            });
            var featureToggles = {};
            <% featureToggles.getToggleMap().each { %>
                featureToggles["${it.key}"] = ${ Boolean.parseBoolean(it.value)};
            <% } %>
        </script>
        <style>
            .sidebar {
                position: relative;
                min-height: 100vh;
            }

            .version-info {
                position: absolute;
                bottom: 10px;
                left: 10px;
                font-size: 10px;
                color: #888;
            }

            .version-info li {
                margin-bottom: 5px;
            }

            .nav_link.active {
                background-color: #0099cc3d;
                color: #ffffff;
            }

            .nav_link.active .nav_icon_container i {
                color: #ffffff;
            }
        </style>
    </body>
</html>