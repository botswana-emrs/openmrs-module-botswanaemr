<script type="text/javascript">
    let selectedOptions = [];
    let OTHER = "5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    let REACTIVE = '1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    let NONREACTIVE =  '1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    let INVALID =  '163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';

    jQuery.noConflict();
    jq(document).ready(function () {
        let stockRooms = [];
        let testReasons = [];
        let labQualityControlTests = [];

        jq("#incidentAndCorrectiveAction").hide();
        jq("input[type=radio]").click(function () {
            if (jq("input[id='acceptableFail']").is(':checked')) {
                jq("#incidentAndCorrectiveAction").show();
            }
            else {
                jq("#incidentAndCorrectiveAction").hide();
            }
        });

        jq("#dateTested").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            "setDate": new Date(),
            dateFormat: "dd-M-yy",
            maxDate: 0, // Set the maximum date to today
            "autoclose": true
        });

        jq('#reasonForTest').multiselect({
            includeSelectAllOption: true,
            onChange: function (element, checked) {
                let val = jq(element).attr("value");
                if (checked) {
                    selectedOptions.push(val)
                } else {
                    selectedOptions.indexOf(val) !== -1 && selectedOptions.splice(selectedOptions.indexOf(val), 1)
                }
                if (selectedOptions.includes(OTHER)) {
                    jq("#otherSpecifySection").show();
                } else {
                    jq('#otherSpecifySection').hide();
                }

            },
            onSelectAll: function () {
                selectedOptions = [];
                let options = jq('#reasonForTest option:selected');
                jq(options).each(function () {
                    let val = jq(this).attr("value");
                    selectedOptions.push(val)
                });
                jq("#otherSpecifySection").show();
            },
            onDeselectAll: function () {
                selectedOptions = [];
                jq("#otherSpecifySection").hide();
            }
        });

        jq("#testTemplate").hide();

        function toSentenceCase(inputString) {
            let result = inputString.replace(/([A-Z])/g, " \$1");
            result = result.charAt(0).toUpperCase() + result.slice(1);
            return result;
        }

        function fetchStockRooms() {
            jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchStockRooms.action')
                .done(function (data) {

                    if (data.stockrooms) {
                        for (let i in data.stockrooms) {
                            data.stockrooms[i].label = toSentenceCase(data.stockrooms[i].name);
                        }
                        stockRooms = data.stockrooms;
                    }
                    setupStockRoomAutocomplete(jq("#stockRoom0"))
                });
        }
        fetchStockRooms();

        function setupStockRoomAutocomplete(stockRoomInput, stockRoomIdInput) {
            jq(stockRoomInput).autocomplete({
                source: stockRooms,
                minLength: 0,
                select: function (event, ui) {
                    jq("input[name=stockRoomId]:empty").each(function () {
                        if (!jq(this).val()) {
                            jq(this).val(ui.item.uuid);
                        }
                    });
                    jq("input[name=stockRoom]:empty").each(function () {
                        if (!jq(this).val()) {
                            jq(this).val(ui.item.label);
                        }
                    });
                }
            }).blur(function () {
                jq(this).autocomplete('enable');
            }).focus(function () {
                jq(this).autocomplete('search', '');
            });
        }

        function cloneResultsTemplate() {
            let index = jq('.qualityTest').length ;
            let template = jq("#testTemplate").children().clone(true, true);
            jq(template).find('.lotNumber').autocomplete();
            jq(template).find('.lotNumber').autocomplete('destroy');
            jq(template).find("input[name=acceptableOption]").attr("name", "acceptableOption" + index)

            if (index > 0) {
                jq(template).find(".title-header").html("REPEAT TEST " + index);
            }

            jq('<div/>', {
                'class': 'qualityTest',
                'id': 'qualityTest' + index,
                html: template
            }).hide().appendTo('#repeatTestsSection').slideDown('slow');
            let authorizedBy = jq("#qualityTest" + index).find("#authorizedBy");
            setupUserAutocomplete(authorizedBy);

            let divId = "#qualityTest" + index;
            setupStockRoomAutocomplete(jq(divId).find("#stockRoom"), jq(divId).find("#stockRoomId"));
            jq(divId).find("#stockRoom").attr("id", "stockRoom" + index);
            jq(divId).find("#stockRoomId").attr("id", "stockRoomId" + index);
            if (index > 0) {
                jq(divId).find("#stockRoom" + index).val(jq(divId).find("#stockRoom0").val());
                jq(divId).find("#stockRoomId" + index).val(jq(divId).find("#stockRoomId0").val());
            }

            let numStrips = jq("#numOfTestStrips").val();
            jq("#numOfTestStrips").val(Number(numStrips)+4);
            jq(divId).find(".expiryDate").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                "setDate": new Date(),
                dateFormat: "dd-M-yy",
                minDate: 0, // Set the minimum date to today
                "autoclose": true
            });

            return index;
        }

        <!-- TODO: Move this to global location -->
        const searchSystemUsers = function (request, response) {
            jq.getJSON(
                '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/user?v=full&q=' + request.term,
                function (data) {
                    let results = data.results.map(function (user) {
                        let label = user.person && user.person.display ? user.person.display : user.display;
                        let value = user.person.uuid;

                        return {
                            label: label,
                            value: value
                        };
                    });
                    response(results)
                });
        };
        const setupUserAutocomplete = function (fieldIdentifier) {
            let textField = jq(fieldIdentifier);

            textField.autocomplete({
                source: searchSystemUsers,
                // select: selectItem,
                minLength: 4,
                select: function (event, ui) {
                    event.preventDefault();
                    jq(this).attr('data-overlay', ui.item.value);
                    jq(this).val(ui.item.label);
                }
            });
        }

        // Set autocomplete for system users
        setupUserAutocomplete('#operatorName');

        jq("#nameOfTest").autocomplete({
            source: ["Test 1", "Test 2"]
        });

        function toggleOtherVisibility() {
            let selectedOption = jq('#reasonForTest option:selected');
            let otherInputField = jq('#otherSpecifySection');

            if (selectedOption.data('overlay') === '5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') {
                otherInputField.show();
            } else {
                otherInputField.hide();
            }
        }

        jq("#btnAddRepeatTest").on("click", function(e){
            e.preventDefault();
            cloneResultsTemplate();
        });

        const validate = function (numberOfTests , acceptable) {
            if (numberOfTests < 2) {
                jq().toastmessage('showErrorToast', 'Please add atleast 2 Tests before submitting.');
                return false;
            }

            if (acceptable === '583bc8fc-3de9-4988-b271-ee8d0e14c2ce') {
                jq().toastmessage('showErrorToast', 'This test should be repeated.');
                return true;
            }
            return true;
        }

        jq('.kitName').change(function() {
            if (jq(this).closest('.qualityTest').find('.stockRoom').val()) {
                let conceptId = jq(this).find(":selected").data('overlay');
                let selectedKitQuantity = jQuery(this).closest('tr').find('.selectedKitQuantity');
                let batchIdInput = jQuery(this).closest('tr').find('.lotNumber');
                let expiryInput = jQuery(this).closest('tr').find('.expiryDate');
                expiryInput.prop('disabled', true);

                let stockRoomInput = jq(this).closest('.qualityTest').find('.stockRoomId');
                fetchHtsKitAvailableBatches(conceptId, selectedKitQuantity, batchIdInput, expiryInput, stockRoomInput);
            } else {
                jq().toastmessage('showErrorToast', "Select the stock room first");
            }
        });

        jq(".results").each(function() {
            handleSelectChange(this);
        });

        jq(".control-section").each(function() {
            handleSelectInterpretation(this);
        });

        jq(".negative-control-section").change(function() {
            controlLogic(this);
        });

        jq(".positive-control-section").change(function() {
            controlLogic(this);
        });

        stockUtils.fetchHtsServicePoints(jq("#testingPoint"));

        // on edit check from saved data and make the radio checked
        if (jq('#labQualityControlId').val()) {
            let acceptable = jq('#acceptable').val();

            jq("input[name='acceptableOption']").each(function() {
                if (jq(this).attr("data-overlay") === acceptable) {
                    jq(this).prop("checked", true);
                } else {
                    jq("#btnAddRepeatTest").prop("disabled", false);
                }
            });
            testReasons = ${testReasons};
            jq.each(testReasons, function (key, testReason) {
                jq('#reasonForTest').multiselect('select', [testReason.concept]);
                selectedOptions.push(testReason.concept);
                if (testReason.concept === OTHER) {
                    jq("#otherSpecifySection").show();
                }
            });

            labQualityControlTests = ${labQualityControlTests};
            if (labQualityControlTests.length === 0) {
                cloneResultsTemplate();
            } else {
                for (let i = 0; i < labQualityControlTests.length; i++) {
                    if (i % 4 === 0) {
                        let index = cloneResultsTemplate();
                        let qualityTest = labQualityControlTests[i];
                        if (qualityTest.acceptable) {
                        }
                        if (qualityTest.correctiveActionTaken) {
                            jq("#qualityTest" + index).find("#incidentAndCorrectiveAction").show();
                            jq("#qualityTest" + index).find("#correctiveActionTaken").val(qualityTest.correctiveActionTaken);
                        }
                        if (qualityTest.incident) {
                            jq("#qualityTest" + index).find("#incidentAndCorrectiveAction").show();
                            jq("#qualityTest" + index).find("#incident").val(qualityTest.incident);
                        }
                        if (qualityTest.authorizedByName) {
                            jq("#qualityTest" + index).find("#authorizedBy").val(qualityTest.authorizedByName);
                            jq("#qualityTest" + index).find("#authorizedBy").attr("data-overlay", qualityTest.authorizedBy);
                        }
                        jq("#qualityTest" + index).find('.table-bordered tbody tr').each(function (key, elem) {
                            let data = labQualityControlTests[i + key];
                            populateTestRow(jq(this), data);
                        });
                        jq("#qualityTest" + index).find(".positive-control-section").trigger('change');
                        jq("#qualityTest" + index).find(".negative-control-section").trigger('change');
                    }
                }
            }
        } else {
            //Automatically add one test if new form
            cloneResultsTemplate();
        }

        function populateTestRow(row, qualityTest) {
            if (qualityTest) {
                jq(row).attr("data-overlay", qualityTest.id)
                if (qualityTest.stockRoomId) {
                    jq(row).closest('.qualityTest').find('.stockRoomId').val(qualityTest.stockRoomId);
                    jq(row).closest('.qualityTest').find('.stockRoom').val(toSentenceCase(qualityTest.stockRoom));
                }
                jq(row).find(".kitName").val(qualityTest.kitName);
                jq(row).find(".results").val(qualityTest.testResults);
                jq(row).find(".lotNumber").val(qualityTest.lotNumber);
                jq(row).find(".expiryDate").val(qualityTest.testExpiryDate);
                jq(row).find(".kitName").trigger('change');
            }
        }

        function handleSelectChange(selectElement) {
            // Find the hidden option and get its data-overlay value
            let hiddenDataOverlay = jq(selectElement).find("#hiddenOption").attr("data-overlay");

            // Find and select the option with the matching data-overlay value
            let matchingOption = jq(selectElement).find("option[data-overlay='" + hiddenDataOverlay + "']").first();
            matchingOption.prop("selected", true);

            // Check if the matchingOption has a value that should remove d-none
            if (matchingOption.prop("selected") && matchingOption.attr("id") !== "hiddenOption") {
                jq(selectElement).removeClass("d-none");
            }

        }

        function handleSelectInterpretation(selectElement) {
            // Find the hidden option and get its data-overlay value
            let hiddenDataOverlay = jq(selectElement).find(".selectedInterpretation").attr("data-overlay");

            // Find and select the option with the matching data-overlay value
            // jq(selectElement).find(".interpretation option[data-overlay='" + hiddenDataOverlay + "']").prop("selected", true);
        }

        function checkValidity(t1Positive, t2Positive, t1Negative, t2Negative) {
            if ((t1Positive !== "" && t1Positive !== REACTIVE) || (t2Positive !== "" && t2Positive !== REACTIVE)) {
                return false;
            }
            if ((t1Negative !== "" && t1Negative !== NONREACTIVE) || (t2Negative !== "" && t2Negative !== NONREACTIVE)) {
                return false;
            }
            return true;
        }

        function controlLogic(selectElement) {
            let parentSection = jq(selectElement).closest('.qualityTest');
            let t1Positive = jq(parentSection).find('.positive-control-section .test1 .results').val() || '';
            let t2Positive = jq(parentSection).find('.positive-control-section .test2 .results').val() || '';

            let t1Negative = jq(parentSection).find('.negative-control-section .test1 .results').val() || '';
            let t2Negative = jq(parentSection).find('.negative-control-section .test2 .results').val() || '';

            if (checkValidity(t1Positive,t2Positive,t1Negative,t2Negative)) {
                jq(parentSection).find("#acceptableFail").prop("checked", false);
                jq(parentSection).find("#acceptablePass").prop("checked", true);
                jq(parentSection).find("#incidentAndCorrectiveAction").hide();
                if (t1Positive && t2Positive && t1Negative && t2Negative) {
                    let index = jq(parentSection).attr("id").charAt(jq(parentSection).attr("id").length - 1);
                    removeRepeatTest(index);
                }
            } else {
                jq(parentSection).find("#acceptableFail").prop("checked", true);
                jq(parentSection).find("#acceptablePass").prop("checked", false);
                jq(parentSection).find("#incidentAndCorrectiveAction").show();
                jq("#btnAddRepeatTest").prop("disabled", false);
            }
        }

        function removeRepeatTest(index) {
            let toRemove = [];
            jq('.qualityTest').map(function() {
                let testIndex = jq(this).attr("id").charAt(jq(this).attr("id").length - 1);
                if (testIndex > index) {
                    toRemove.push(jq(this));
                }
            });
            jq(toRemove).each(function() {
                jq(this).remove();
            });
        }
        
        function fetchHtsKitAvailableBatches(conceptId, selectedKitQuantity, batchIdInput, expiryInput, stockRoomInput) {
            let parameter = {
                'conceptId': conceptId,
                'stockroomId': stockRoomInput.val()
            };
            let batchNumberToExpiry = {};

            jq.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsKitAvailableBatches.action', parameter)
                .done(function (data) {
                    if (data.status === 'success') {
                        // populate autocomplete with batches
                        if (data.batches) {
                            let quantity = data.availableQuantity;
                            if(selectedKitQuantity.length) {
                                selectedKitQuantity.replaceWith('<input type="hidden" class="kit-quantity" value="' + quantity + '">');
                            } else {
                                jq(".kit-quantity").val(quantity);
                            }

                            jq.each(data.batches, function(i, item) {
                                batchNumberToExpiry[item.batchNumber] = item.expiryDate;
                                if (data.batchNumbers[i]) {
                                    let val = item.batchNumber + " (" + batchNumberToExpiry[item.batchNumber] + ")";
                                    data.batchNumbers[i] = val;
                                    batchNumberToExpiry[val] = item.expiryDate;
                                }
                            })

                            jq(batchIdInput).autocomplete({
                                source: data.batchNumbers,
                                minLength: 0,
                                select: function(event, ui) {
                                    jq(expiryInput).val(batchNumberToExpiry[ui.item.value]);
                                }
                            }).blur(function () {
                                jq(this).autocomplete('enable');
                            }).focus(function () {
                                jq(this).autocomplete('search', '');
                            });
                        }
                    } else {
                        if(selectedKitQuantity.length) {
                            selectedKitQuantity.replaceWith('<input type="hidden" class="kit-quantity" value="">');
                        } else {
                            jq(".kit-quantity").val('');
                        }
                    }
                });
        }

        jq(".kitName").each(function() {
            handleSelectChange(this);
        });

        jq(function () {
            function saveForm(published, isEdit){
                jq('.interpretation').each(function() {
                    let selectedValue = jq(this).find('option:selected').attr('data-overlay');
                    jq(this).closest('.card-body').find('.selectedInterpretation').attr('data-overlay', selectedValue);
                });

                let labQualityDataTests = [];
                let isSetAcceptable = false;
                let isAuthorizedCorrect = true;
                let fail = false;
                jq("#repeatTestsSection .qualityTest").each(function () {
                    let testIndex = jq(this).attr("id").charAt(jq(this).attr("id").length - 1);
                    let acceptable = jq(this).find("input[name='acceptableOption" + testIndex + "']:checked").attr('data-overlay');
                    let incident = jq(this).find("input[name='incident']").val();
                    let correctiveAction = jq(this).find("input[name='correctiveActionTaken']").val();
                    let authorizedBy = jq(this).find("input[name='authorizedBy']").attr('data-overlay');
                    let stockRoomId = jq(this).find('input[name="stockRoomId"]').val();
                    if (!isSetAcceptable) {
                        isSetAcceptable = acceptable && acceptable === "2c6c9444-280b-48b3-9fc6-4872fd95fcb8";
                    }

                    if (published && !authorizedBy) {
                        jq().toastmessage('showErrorToast', "Invalid 'Authorised By' value, please select a valid user");
                        jq(this).find("input[name='authorizedBy']").focus();
                        isAuthorizedCorrect = false;
                    }

                    jq(this).find('.table-bordered tbody tr').each(function(trIndex) {
                        const row = jq(this);
                        const selectedKitName = row.find('.kitName option:selected').data('overlay');
                        const selectedResults = row.find('.results option:selected').data('overlay');
                        if (selectedKitName && selectedResults) {
                            const rowData = {
                                qualityControl: row.find('.quality_control').attr('data-overlay'),
                                nameOfTest: row.find('.nameOfTest').text(),
                                kitName: selectedKitName,
                                lotNumber: row.find('.lotNumber').val(),
                                testExpiryDate: row.find('.expiryDate').val(),
                                testResults: selectedResults,
                                interpretation: '',
                                parent: testIndex,
                                acceptable: acceptable,
                                incident: incident,
                                correctiveAction: correctiveAction,
                                authorizedBy: authorizedBy,
                                id: jq(this).attr("data-overlay"),
                                stockRoomId: stockRoomId
                            };
                            labQualityDataTests.push(rowData);
                        } else if (published) {
                            fail = true;
                        }
                    });
                });

                if (fail) {
                    jq().toastmessage('showErrorToast', "All tests have to be filled before publishing the form");
                    return false;
                }
                if (!isAuthorizedCorrect) {
                    return false;
                }
                let numberOfTests = labQualityDataTests.length;

                let acceptable = isSetAcceptable ? "2c6c9444-280b-48b3-9fc6-4872fd95fcb8" : "583bc8fc-3de9-4988-b271-ee8d0e14c2ce";
                const params = {
                    'labQualityControlId': jq('#labQualityControlId').val(),
                    'reasonForTest': "",
                    'reasonsForTest': JSON.stringify(selectedOptions),
                    'otherReasonForTest': jq('#otherReasonForTest').val(),
                    'operatorName': jq('#operatorName').attr('data-overlay'),
                    'dateTested': jq('#dateTested').val(),
                    labQualityDataTests: JSON.stringify(labQualityDataTests),
                    'numOfTestStrips': numberOfTests,
                    'authorizedBy': jq('#authorizedBy').attr('data-overlay'),
                    'incident': jq('#incident').val(),
                    'correctiveActionTaken': jq('#correctiveActionTaken').val(),
                    'acceptable': acceptable,
                    'testingPoint': jq("#testingPoint").val(),
                    'published': published
                };

                if (validate(numberOfTests, acceptable)) {
                    jq.post('${ui.actionLink("botswanaemr", "lab/laboratoryQualityControlForm", "saveLabQualityControl")}', params, function (data) {
                        let message = published ?  "Lab Quality Control saved successfully!" : "Lab Quality Control draft saved successfully!";
                        jq().toastmessage('showNoticeToast', message);
                        location.href = '${ui.pageLink("botswanaemr", "lab/labQualityControl")}';
                    }, 'json').fail(function (err) {
                        jq().toastmessage('showErrorToast', err.responseText); // Displaying the error response text
                    });
                }
            }
            jq("#savelabQualityControlForm").submit(function (e) {
                e.preventDefault();

                let clickedButton = e.originalEvent.submitter;
                let published = jq(clickedButton).attr("id") === "savelabQualityControl";

                saveForm(published, false);
            });

            jq("#editlabQualityControlForm").submit(function (e) {
                e.preventDefault();

                let clickedButton = e.originalEvent.submitter;
                let published = jq(clickedButton).attr("id") === "publishQualityControl";

                saveForm(published, true);
            });
        });
    });
</script>
<style>
    .dropdown-menu > li > a:hover {
        background-color: white !important;
        background-image:none !important;
    }

    .dropdown-menu > .active > a {
        background-color: white !important;
        background-image:none !important;
    }
</style>

<div id="testTemplate" class="row">
    <div class="row">
        <div class="col pl-0 pr-0">
            <div class="card bp-4 ml-0 mr-0">
                <div class="card-header bg-pale border border-info" id="test">
                    <div class="row">
                        <div class="col">
                            <h4 class="mb-0 pl-0 text-primary float-left title-header">Test</h4>
                        </div>
                    </div>
                </div>
                <div class="row" id="stockroom-container">
                    <div class="col pl-0">
                        <div class="col-6 pl-0">
                            <div class="form-group">
                                <label for="stockRoom">Stock Room:
                                    <span class="text-danger">*</span>
                                </label>
                                <input id="stockRoom" class="form-control stockRoom" name="stockRoom" placeholder="StockRoom">
                            </div>
                        </div>
                    </div>
                    <div class="col hidden">
                        <div class="col-6 pr-0">
                            <div class="form-group">
                                <label for="stockRoomId">Stock Room ID:
                                </label>
                                <input id="stockRoomId" class="form-control stockRoomId" name="stockRoomId" placeholder="stockRoomId">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col m-4 pl-0 pr-0">
                            <h5 class="text-primary">POSITIVE CONTROL</h5>
                            <hr class="divider pt-1 bg-primary"/>

                            <div class="card-body positive-control-section">
                                <table class="table table-bordered">
                                    <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col">Test Name</th>
                                        <th scope="col">Kit Name</th>
                                        <th scope="col">Lot</th>
                                        <th scope="col">Expiry Date</th>
                                        <th scope="col">Results</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="test1">
                                        <input type=hidden class="quality_control" name="quality_control" data-overlay="703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        <input type=hidden class="selectedInterpretation" name="selectedInterpretation" data-overlay=""/>
                                        <th scope="col">
                                            <label class="float-left nameOfTest">Test 1</label>
                                        </th>
                                        <th scope="col">
                                            <select class="form-control kitName">
                                                <option selected disabled>Select Kit Name</option>
                                                <option value="c2ce43cc-0345-40c0-91b2-24155f458d4e" data-overlay="c2ce43cc-0345-40c0-91b2-24155f458d4e">Determine</option>
                                                <option value="166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Bioline</option>
                                                <option value="c53f44de-5df2-4810-b99b-7f75e80d1a05" data-overlay="c53f44de-5df2-4810-b99b-7f75e80d1a05">KHB</option>
                                                <option value="1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">ICT</option>
                                            </select>
                                            <input type="hidden" class="selectedKitQuantity" value=""/>
                                        </th>
                                        <th scope="col">
                                            <input class="form-control lotNumber" name="lotNumber"
                                                   placeholder="Lot Number" />
                                        </th>
                                        <th scope="col">
                                            <input class="form-control expiryDate" name="expiryDate"
                                                   placeholder="Expiry Date" />
                                        </th>
                                        <th scope="col">
                                            <select class="form-control results">
                                                <option selected disabled>Select Result</option>
                                                <option value="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Reactive</option>
                                                <option value="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Non Reactive</option>
                                                <option value="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Invalid</option>
                                            </select>
                                        </th>
                                    </tr>
                                    <tr class="test2">
                                        <input type=hidden class="quality_control" name="quality_control" data-overlay="703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        <input type=hidden class="selectedInterpretation" name="selectedInterpretation" data-overlay=""/>
                                        <th scope="col">
                                            <label class="float-left nameOfTest">Test 2</label>
                                        </th>
                                        <th scope="col">
                                            <select class="form-control kitName">
                                                <option selected disabled>Select Kit Name</option>
                                                <option value="c2acb8a1-32f6-456a-b322-d31a1d478c01" data-overlay="c2acb8a1-32f6-456a-b322-d31a1d478c01">Unigold</option>
                                                <option value="570a1f68-0bea-4980-ba9d-d0d7d21e4be9" data-overlay="570a1f68-0bea-4980-ba9d-d0d7d21e4be9">1st Response</option>
                                                <option value="c203885b-7bae-4318-8bb8-4aee96f973cd" data-overlay="c203885b-7bae-4318-8bb8-4aee96f973cd">Bio KIt</option>
                                                <option value="7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f" data-overlay="7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f">Immunoflow</option>
                                            </select>
                                            <input type="hidden" class="selectedKitQuantity" value=""/>
                                        </th>
                                        <th scope="col">
                                            <input class="form-control lotNumber" name="lotNumber"
                                                   placeholder="Lot Number" />
                                        </th>
                                        <th scope="col">
                                            <input class="form-control expiryDate" name="expiryDate"
                                                   placeholder="Expiry Date" />
                                        </th>
                                        <th scope="col">
                                            <select class="form-control results">
                                                <option selected disabled>Select Result</option>
                                                <option value="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Reactive</option>
                                                <option value="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Non Reactive</option>
                                                <option value="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Invalid</option>
                                            </select>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m-4 pl-0 pr-0">
                            <h5 class="text-primary">NEGATIVE CONTROL</h5>
                            <hr class="divider pt-1 bg-primary"/>
                            <div class="card-body negative-control-section">
                                <table class="table table-bordered">
                                    <thead class="bg-pale border border-info">
                                    <tr>
                                        <th scope="col">Test Name</th>
                                        <th scope="col">Kit Name</th>
                                        <th scope="col">Lot</th>
                                        <th scope="col">Expiry Date</th>
                                        <th scope="col">Results</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="test1">
                                        <input type=hidden class="quality_control" name="quality_control" data-overlay="664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        <input type=hidden class="selectedInterpretation" name="interpretation" data-overlay=""/>
                                        <th scope="col">
                                            <label class="float-left nameOfTest">Test 1</label>
                                        </th>
                                        <th scope="col">
                                            <select class="form-control kitName">
                                                <option selected disabled>Select Kit Name</option>
                                                <option value="c2ce43cc-0345-40c0-91b2-24155f458d4e" data-overlay="c2ce43cc-0345-40c0-91b2-24155f458d4e">Determine</option>
                                                <option value="166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Bioline</option>
                                                <option value="c53f44de-5df2-4810-b99b-7f75e80d1a05" data-overlay="c53f44de-5df2-4810-b99b-7f75e80d1a05">KHB</option>
                                                <option value="1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">ICT</option>
                                            </select>
                                            <input type="hidden" class="selectedKitQuantity" value=""/>
                                        </th>
                                        <th scope="col">
                                            <input class="form-control lotNumber" name="lotNumber"
                                                   placeholder="Lot Number">
                                        </th>
                                        <th scope="col">
                                            <input class="form-control expiryDate" name="expiryDate"
                                                   placeholder="Expiry Date">
                                        </th>
                                        <th scope="col">
                                            <select class="form-control results">
                                                <option selected disabled>Select Result</option>
                                                <option value="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Reactive</option>
                                                <option value="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Non Reactive</option>
                                                <option value="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Invalid</option>
                                            </select>
                                        </th>
                                    </tr>
                                    <tr class="test2">
                                        <input type=hidden class="quality_control" name="quality_control" data-overlay="664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        <input type=hidden class="selectedInterpretation" name="selectedInterpretation" data-overlay=""/>
                                        <th scope="col">
                                            <label class="float-left nameOfTest">Test 2</label>
                                        </th>
                                        <th scope="col">
                                            <select class="form-control kitName">
                                                <option selected disabled>Select Kit Name</option>
                                                <option value="c2acb8a1-32f6-456a-b322-d31a1d478c01" data-overlay="c2acb8a1-32f6-456a-b322-d31a1d478c01">Unigold</option>
                                                <option value="570a1f68-0bea-4980-ba9d-d0d7d21e4be9" data-overlay="570a1f68-0bea-4980-ba9d-d0d7d21e4be9">1st Response</option>
                                                <option value="c203885b-7bae-4318-8bb8-4aee96f973cd" data-overlay="c203885b-7bae-4318-8bb8-4aee96f973cd">Bio KIt</option>
                                                <option value="7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f" data-overlay="7fb6eaec-6cd8-4cd4-9e38-3f05c58c881f">Immunoflow</option>
                                            </select>
                                            <input type="hidden" class="selectedKitQuantity" value=""/>
                                        </th>
                                        <th scope="col">
                                            <input class="form-control lotNumber" name="lotNumber"
                                                   placeholder="Lot Number">
                                        </th>
                                        <th scope="col">
                                            <input class="form-control expiryDate" name="expiryDate"
                                                   placeholder="Expiry Date">
                                        </th>
                                        <th scope="col">
                                            <select class="form-control results">
                                                <option selected disabled>Select Result</option>
                                                <option value="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Reactive</option>
                                                <option value="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Non Reactive</option>
                                                <option value="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="163611AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Invalid</option>
                                            </select>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col pl-0 pr-0">
            <div class="row">
                <div class="col-6 pl-0">
                    <div class="form-group">
                        <label for="acceptable">Acceptable
                            <span class="text-danger">*</span>
                        </label>
                        <div class="row">
                            <div class="form-check">
                                <div class="col">
                                    <input type="radio" id="acceptablePass" class="form-check-input"
                                           name="acceptableOption" value="Positive" data-overlay="2c6c9444-280b-48b3-9fc6-4872fd95fcb8">
                                    <label for="positive" class="pl-4">Pass</label>
                                </div>
                            </div>

                            <div class="form-check">
                                <div class="col">
                                    <input type="radio" id="acceptableFail" class="form-check-input"
                                           name="acceptableOption" value="Negative" data-overlay="583bc8fc-3de9-4988-b271-ee8d0e14c2ce">
                                    <label for="negative" class="pl-4">Fail</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="incidentAndCorrectiveAction" class="row pt-4 pb-4">
                <div class="col pl-0">
                    <div class="form-group">
                        <label for="incident">Incident
                            <span class="text-danger">*</span>
                        </label>
                        <input id="incident" class="form-control" name="incident" placeholder="Incident">
                    </div>
                </div>
                <div class="col pr-0">
                    <div class="form-group">
                        <label for="correctiveActionTaken">Corrective action Taken
                            <span class="text-danger">*</span>
                        </label>
                        <input id="correctiveActionTaken" class="form-control" name="correctiveActionTaken"
                               placeholder="Corrective Action Taken">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col pl-0 pr-0">
            <div class="row pt-4 pb-4">
                <div class="col pr-0">
                    <div class="form-group">
                        <label for="authorizedBy">Authorized by
                            <span class="text-danger">*</span>
                        </label>
                        <input id="authorizedBy" class="form-control" name="authorizedBy" data-overlay=""
                               placeholder="Authorized by">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% if (labQualityControl == null) { %>
    <div class="card-body">
    <form method="post" id="savelabQualityControlForm">
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="row pt-4 pb-4">
                    <div class="col pl-0">
                        <div class="form-group">
                            <label for="reasonForTest">Reason for Test
                                <span class="text-danger">*</span>
                            </label>
                            <select id="reasonForTest" multiple class="form-control">
                                <option value="21057f3f-7805-4e27-959b-fc48843116db" data-overlay="21057f3f-7805-4e27-959b-fc48843116db">Weekly test</option>
                                <option value="c25d0c0d-bdc3-466c-9d90-af59b1a5fb98" data-overlay="c25d0c0d-bdc3-466c-9d90-af59b1a5fb98">New lot</option>
                                <option value="306dcd53-be24-4e31-be06-ff22d4cb77d8" data-overlay="306dcd53-be24-4e31-be06-ff22d4cb77d8">New shipment</option>
                                <option value="8c31e5f3-2b83-40c6-8a2e-c3b527bdbaf1" data-overlay="8c31e5f3-2b83-40c6-8a2e-c3b527bdbaf1">New tester</option>
                                <option value="799d9f56-32e7-4ab2-b6ad-fd7aaf31e81d" data-overlay="799d9f56-32e7-4ab2-b6ad-fd7aaf31e81d">Suspicious results</option>
                                <option value="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Temperature</option>
                                <option value="5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="col pr-0" id="otherSpecifySection">
                        <div class="form-group">
                            <label for="otherReasonForTest">Other Specify</label>
                            <input id="otherReasonForTest" class="form-control" name="otherSpecify" data-overlay="e24181a9-74bd-428d-8587-4cc3fa1b312b"
                                   placeholder="Other Specify">
                        </div>
                    </div>
                </div>

                <div class="row pt-4 pb-4">
                    <div class="col pl-0">
                        <div class="form-group">
                            <label for="operatorName">Operator Name
                                <span class="text-danger">*</span>
                            </label>
                            <input id="operatorName" class="form-control" name="operatorName" data-overlay=""
                                   placeholder="Operator Name">
                        </div>
                    </div>
                    <div class="col pr-0">
                        <div class="form-group">
                            <label for="dateTested">Date tested
                                <span class="text-danger">*</span>
                            </label>
                            <input id="dateTested" class="form-control" name="dateTested" placeholder="Date tested">
                        </div>
                    </div>
                </div>

                <div class="row pt-4 pb-4">
                    <div class="col-6 pl-0">
                        <div class="form-group">
                            <label for="testingPoint">Testing Point
                                <span class="text-danger">*</span>
                            </label>
                            <input id="testingPoint" class="form-control" name="testingPoint" data-overlay=""
                                   placeholder="Testing Point">
                        </div>
                    </div>
                </div>

                <div id="repeatTestsSection"></div>
                <div class="row mt-4">
                    <button class="dashed-button rounded add-more" disabled
                        id="btnAddRepeatTest">+ Add Repeat Test
                    </button>
                </div>
                <div class="row mt-4">
                    <div class="col pl-0 pr-0">
                        <div class="row pt-4 pb-4">
                            <div class="col pl-0">
                                <div class="form-group">
                                    <label for="numOfTestStrips">Number of test strips used
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input name="integer" id="numOfTestStrips" class="form-control" name="numOfTestStrips"
                                           placeholder="Number of test strips used">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer bg-pale border border-info">
            <div class="row actions px-0">
                <div class="col-12 px-0">
                    <button type="button" id="cancel" class="btn btn-sm btn-dark bg-dark float-right ml-1">
                        Close
                    </button>
                    <button type="submit" id="savelabQualityControl" class="btn btn-sm btn-primary float-right mr-0">
                        Save and Publish
                    </button>
                    <button type="submit" id="saveDraftLabQualityControl" class="btn btn-sm btn-primary float-right mr-1">
                        Save Draft
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<% } else { %>
    <div class="card-body">
    <form method="post" id="editlabQualityControlForm">
        <div class="row">
            <div class="col pl-0 pr-0">
                <div class="row pt-4 pb-4">
                    <div class="col pl-0">
                        <input type=hidden id="labQualityControlId" name="labQualityControlId" value="${labQualityControlId}"/>
                        <div class="form-group">
                            <label for="reasonForTest">Reason for Test
                                <span class="text-danger">*</span>
                            </label>
                            <select id="reasonForTest" multiple class="form-control" >
                                <option value="21057f3f-7805-4e27-959b-fc48843116db" data-overlay="21057f3f-7805-4e27-959b-fc48843116db">Weekly test</option>
                                <option value="c25d0c0d-bdc3-466c-9d90-af59b1a5fb98" data-overlay="c25d0c0d-bdc3-466c-9d90-af59b1a5fb98">New lot</option>
                                <option value="306dcd53-be24-4e31-be06-ff22d4cb77d8" data-overlay="306dcd53-be24-4e31-be06-ff22d4cb77d8">New shipment</option>
                                <option value="8c31e5f3-2b83-40c6-8a2e-c3b527bdbaf1" data-overlay="8c31e5f3-2b83-40c6-8a2e-c3b527bdbaf1">New tester</option>
                                <option value="799d9f56-32e7-4ab2-b6ad-fd7aaf31e81d" data-overlay="799d9f56-32e7-4ab2-b6ad-fd7aaf31e81d">Suspicious results</option>
                                <option value="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Temperature</option>
                                <option value="5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" data-overlay="5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="col pr-0" id="otherSpecifySection">
                        <div class="form-group">
                            <label for="otherReasonForTest">Other Specify</label>
                            <input id="otherReasonForTest" class="form-control" name="otherSpecify" placeholder="Other Specify"
                                   value="${labQualityControl.otherReasonForTest ?: ''}">
                        </div>
                    </div>
            </div>

            <div class="row pt-4 pb-4">
                <div class="col pl-0">
                    <div class="form-group">
                        <label for="operatorName">Operator Name
                            <span class="text-danger">*</span>
                        </label>
                        <input id="operatorName" class="form-control" name="operatorName" data-overlay="${labQualityControl.operator.getUuid() ?: ''}"
                               value="${labQualityControl.operator.getPersonName().getFullName() ?: ''}"
                               placeholder="Operator Name">
                    </div>
                </div>
                <div class="col pr-0">
                    <div class="form-group">
                        <label for="dateTested">Date tested
                            <span class="text-danger">*</span>
                        </label>
                        <input id="dateTested" class="form-control" name="dateTested" placeholder="Date tested" data-overlay="e24181a9-74bd-428d-8587-4cc3fa1b312b"
                               value="${labQualityControlDateTested ?: ''}">
                    </div>
                </div>
            </div>

                <div class="row pt-4 pb-4">
                    <div class="col-6 pl-0">
                        <div class="form-group">
                            <label for="testingPoint">Testing Point
                                <span class="text-danger">*</span>
                            </label>
                            <input id="testingPoint" class="form-control" name="testingPoint" data-overlay=""
                                   placeholder="Testing Point" value="${labQualityControl.testingPoint ?: ''}">
                        </div>
                    </div>
                </div>

            <div id="repeatTestsSection"></div>
            <div class="row mt-4">
                <button class="dashed-button rounded add-more" disabled
                    id="btnAddRepeatTest">+ Add Repeat Test
                </button>
            </div>
                <div class="row mt-4">
                    <div class="col pl-0 pr-0">
                        <div class="row pt-4 pb-4">
                            <div class="col pl-0">
                                <div class="form-group">
                                    <label for="numOfTestStrips">Number of test strips used
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input name="integer" id="numOfTestStrips" class="form-control" name="numOfTestStrips" value="0"
                                           placeholder="Number of test strips used">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer bg-pale border border-info">
            <div class="row actions px-0">
                <div class="col-12 px-0">
                    <button type="button" id="cancel" class="btn btn-sm btn-dark bg-dark float-right ml-1">
                        Close
                    </button>
                    <button type="submit" id="editlabQualityControl" class="btn btn-sm btn-primary float-right mr-0">
                        Update
                    </button>
                    <button type="submit" id="publishQualityControl" class="btn btn-sm btn-primary float-right mr-1">
                        Publish
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<% } %>
