<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jQuery('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jQuery('#searchPhrase').val() || name.toLowerCase().includes(jQuery('#searchPhrase').val().toLowerCase())
                );
            }
            if (jQuery('#dateCreated').val() !== '') {
                let queueDateCreated = aData[5].trim();
                matchingOptions.push(
                    queueDateCreated === jQuery('#dateCreated').val()
                );
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    let getSearchDateValue = function() {
        let searchDate = jQuery('#dateCreated').datepicker('getDate'); 
        return jQuery.datepicker.formatDate('yy-mm-dd', searchDate);
    };

    jQuery(function () {
        const table = jQuery('#laboratoryTestOrdersPoolTable').DataTable({
            dom: 'rtp',
            searching: true,
            "oLanguage": {
                "oPaginate": {
                    "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                }
            },
            serverMethod: "get",
            processing: true,
            serverSide: true,
            ajax: {
                url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoomWithExtendedSearch')}",
                data: function (d) {
                    d.servicePoint = "${currentServicePoint != null ? currentServicePoint : ''}";
                    d['search[date]'] = getSearchDateValue();
                },
            },
            columns: [
                {'data': 'pin'},
                {'data': 'patientNames'},
                {'data': 'gender'},
                {'data': 'age'},
                {
                    'data': 'status',
                    "render": function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row.status === "COMPLETED") {
                                return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Completed</span>';
                            } else if (row.status === "IN_PROGRESS") {
                                return '<i class="fa fa-circle text-primary"></i><span class="text-muted px-0" style="background: none;"> Testing in progress</span>';
                            } else {
                                return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                            }
                        }
                        return data;
                    }
                },
                {'data': 'dateCreated'},
                {'data': null}
            ],
            columnDefs: [
                {
                    targets: 6,
                    data: null,
                    defaultContent: '',
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row.status !== "COMPLETED") {
                                return '<a href="/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestOrders.page?patientId=' + row.patientId + '&returnUrl=/'+ OPENMRS_CONTEXT_PATH +'/botswanaemr/lab/addLaboratoryTestOrdersPool.page" class="text-primary p-1" id="view">Add Lab Test</a>&nbsp;';
                            }
                        }
                    }
                }
            ]

        });

        jQuery('#inquiryBtn').on('click', () => {
            // if extend search is checked then we need to search for all patients
            if (jQuery('#extendSearch').is(':checked')) {
                // jQuery('#dateCreated').val('');
                //table.ajax.url('${ui.actionLink("botswanaemr","search","getPatientsByAdvancedSearch")}');
                //table.ajax.data({nameOrUniqueId: jq("searchPhrase")});
                table.load();
            }
            table.search(jq("#searchPhrase").val()).draw();
        });

        jQuery('#resetBtn').on('click', () => {
            fieldHelper.clearAllFields(jQuery('#htsPatientPool'));
            table.draw();
        });
        /*
        jQuery('#searchPhrase').on( 'keyup', function () {
            let timeout;
            let delay = 1000;
            let searchText = this.value;
            if (searchText.length >= 3) {
                if(timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function() {
                    table.draw();
                }, delay);
            }
        });
        */
    });
</script>

<div class="card">
    <div class="row mb-5">
        <div class="col-4 pl-0">
            <div class="form-group">
                <input type="text"
                       class="form-control py-2"
                       id="searchPhrase"
                       name="searchPhrase"
                       placeholder="Search by Name, ID"/>
            </div>
        </div>
        <div class="col-4">
            <div class="form-group">
                <input type="text"
                       class="form-control py-2"
                       id="dateCreated"
                       name="dateCreated"
                       placeholder="YYYY-MM-DD">
                <script type="text/javascript">
                    jQuery('#dateCreated').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        "setDate": new Date(),
                        dateFormat: "yy-mm-dd",
                        yearRange: "-150:+0",
                        maxDate: 0,
                        "autoclose": true,
                        onSelect: function (data) {
                            jQuery("#dateCreated").trigger('change');
                        }
                    });
                </script>
            </div>
        </div>

        <div class="col-4 pr-0">
            <div class="row">
                <div class="col">
                    <div class="form-group row hidden">
                        <label for="extendSearch">Extend Search</label><input class="form-check-input form-check-inline"
                                                                              type="checkbox" name="extendSearch"
                                                                              id="extendSearch"/>
                    </div>
                </div>

                <div class="col">
                    <div class="form-group row">
                        <button type="button" class="btn btn-primary float-right" id="inquiryBtn" name="inquiryBtn">
                            <i class="fa fa-search"></i> search
                        </button>
                    </div>
                </div>

                <div class="col">
                    <div class="form-group">
                        <button type="button" class="btn btn-outline btn-secondary float-right" id="resetBtn" name="resetBtn">
                            <i class="fa fa-undo fa-1x"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="table-responsive">
        <table id="laboratoryTestOrdersPoolTable" class="table table-striped table-sm table-bordered">
            <thead>
            <tr>
                <th>PIN</th>
                <th>Name</th>
                <th>Sex</th>
                <th>Age</th>
                <th>Status</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
