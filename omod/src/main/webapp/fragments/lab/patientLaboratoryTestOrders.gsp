<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jQuery('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jQuery('#searchPhrase').val() || name.toLowerCase().includes(jQuery('#searchPhrase').val().toLowerCase())
                );
            }

            if (jq('#labOrderStatus').find(":selected").val() !== '') {
                let testStatus = aData[2].trim();
                matchingOptions.push(testStatus === jq('#labOrderStatus').find(":selected").text());
            }

            if (matchingOptions.length === 0) {
                return  true;
            }

            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );


    jQuery(function () {
        let patientUuid = "${patient.uuid}";

        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','lab/patientLaboratoryTestOrders','getPatientLaboratoryTestOrders')}&patientId=" + patientUuid,
            dataType: "json",
            global: false,
            async: false,
            success: function (data) {
                console.log(data);
                var table = jQuery('#patientLabTestOrdersListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "COMPLETED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;">Completed</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;">Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': 'requestedBy'},
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status !== null) {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/lab/laboratoryOrders.page?encounterId='+row.encounterId+'" class="text-primary pl-4" id="view">View</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#searchButton').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#laboratoryOrders'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col pr-0">
        <a  href="/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestOrdersPool.page"
            class="btn btn-sm btn-primary float-right mb-3 text-light">Add Request</a>
    </div>
</div>

<div class="card-header pr-1">
    <div class="row mt-3 mb-3 pr-1">
        <div class="col pl-0">
            <input id="searchPhrase"
                   type="text"
                   name="searchPhrase"
                   class="form-control"
                   value=""
                   placeholder="${ui.message('Patient Name')}"
            />
        </div>
        <div class="col">
            <select id="labOrderStatus"
                    class="form-control"
                    placeholder="${ui.message('Select status')}">
                <option value="">Status</option>
                <option value="Awaiting"> Awaiting</option>
                <option value="Completed"> Completed</option>
            </select>
        </div>
        <div class="col">
            <button id="searchButton"
                    type="submit"
                    class="btn btn-primary float-right">
                    <i class="fa fa-search"></i>
                    ${ui.message("Search")}
            </button>
        </div>
        <div class="col pr-0">
            <button id="resetBtn"
                    type="reset"
                    class="btn btn-dark bg-dark float-right">
                    <i class="fa fa-undo"></i>
                    ${ui.message("Reset")}
            </button>
        </div>
    </div>
</div>

<div class="card-body mt-5">
    <div class="table-responsive">
        <table id="patientLabTestOrdersListTable"
               class="table table-striped table-sm table-bordered">
            <thead>
                <tr>
                    <th>Patient ID</th>
                    <th>Patient Names</th>
                    <th>Status</th>
                    <th>Requested By</th>
                    <th class="text-right border-right-0">Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>