<ul class="list-unstyled components">
    <li class="pt-2">
        <img class="mx-auto d-block" src="${ui.resourceLink('botswanaemr', 'images/moh-logo-01.png')}" width="55" height="55"/>
    </li>
    <li>
        <span class="ml-3 h4 nav_label">BotswanaEMR</span>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'patientManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Patient Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'lab/groupedLaboratoryOrders')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-user-plus fa-1x"></i>
            </div>
            <div class="nav_label">Lab Orders</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'lab/laboratoryResults')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-gear fa-1x"></i>
            </div>
            <div class="nav_label">Lab Results</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'appointments/appointmentsManagement')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-calendar fa-1x"></i>
            </div>
            <div class="nav_label">Appointments Management</div>
        </a>
    </li>
    <li>
        <a href="${ui.pageLink('botswanaemr', 'reports')}" class="nav_link">
            <div class="nav_icon_container">
                <i class="fa fa-line-chart fa-1x"></i>
            </div>
            <div class="nav_label">Reports</div>
        </a>
    </li>
                    <% if (user.isSuperUser()) { %>
                        ${ui.includeFragment("botswanaemr", "widget/adminNav")}
                    <% } %>
</ul>

