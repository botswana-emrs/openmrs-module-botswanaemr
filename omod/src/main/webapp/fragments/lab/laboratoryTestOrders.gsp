<script type="text/javascript">
    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let name = aData[0].trim();
                matchingOptions.push(
                    name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }

            if (jq('#testName').val() !== '') {
                let name = aData[1].trim();
                matchingOptions.push(
                    name.toLowerCase().includes(jq('#testName').val().toLowerCase())
                );
            }

            if (jq('#labTestStatus').find(":selected").val() !== '') {
                let testStatus = aData[2].trim();
                matchingOptions.push(testStatus === jq('#labTestStatus').find(":selected").text());
            }

            if (jq('#testDate').val() !== '') {
                let queueDateCreated = aData[4].trim();
                matchingOptions.push(
                    queueDateCreated === jQuery('#testDate').val()
                );
            }
 
            if (matchingOptions.length === 0) {
                return  true;
            }
            
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','lab/laboratoryTestOrders','getLaboratoryTests')}",
            dataType: "json",
            global: false,
            async: false,
            data: {
                encounterId: "${encounterId != null ? encounterId : ''}",
            },
            success: function (data) {
                console.log(data);
                var table = jQuery('#laboratoryTestOrdersListTable').DataTable({
                    data: data,
                    columns: [
                        {'data': 'patientNames'},
                        {'data': 'testName'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "COMPLETED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;">Completed</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;">Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': 'requestedBy'},
                        {'data': 'dateAdded'},
                        {'data': null,
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "COMPLETED") {
                                        return '<a href="#" class="text-primary pl-0" id="void">Void</a>&nbsp;|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestResults.page?patientId='+row.patientId+'&labTestId='+row.testId+'" class="text-primary pl-0 float-right" id="view">View</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestResults.page?patientId='+row.patientId+'&labTestId='+row.testId+'" class="text-primary pl-0 float-left" id="add">Add Results</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    }
                });

                jq('#searchButton').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#laboratoryOrders'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col pr-0">
        <a  href="/${ui.contextPath()}/botswanaemr/lab/addLaboratoryTestOrdersPool.page"
            class="btn btn-sm btn-primary float-right mb-3 text-light">Add Request</a>
    </div>
</div>

<div class="card-header pr-1">
    <div class="row mt-3 mb-3 pr-1">
        <div class="col pl-0">
            <input id="searchPhrase"
                   type="text"
                   name="searchPhrase"
                   class="form-control"
                   value=""
                   placeholder="${ui.message('Patient Name')}"
            />
        </div>
        <div class="col">
            <input id="testName"
                   type="text"
                   name="testName"
                   class="form-control"
                   placeholder="${ui.message('Test Name')}"
            />
        </div>
        <div class="col">
            <input type="text"
               class="form-control py-2"
               id="testDate"
               name="testDate"
               placeholder="YYYY-MM-DD">
            <script type="text/javascript">
                jQuery('#testDate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    "setDate": new Date(),
                    dateFormat: "yy-mm-dd",
                    yearRange: "-150:+0",
                    maxDate: 0,
                    "autoclose": true,
                    onSelect: function (data) {
                        jQuery("#testDate").trigger('change');
                    }
                });
            </script>
        </div>
        <div class="col">
            <select id="labTestStatus"
                    class="form-control"
                    placeholder="${ui.message('Select status')}">
                <option value="">Status</option>
                <option value="Awaiting"> Awaiting</option>
                <option value="Completed"> Completed</option>
            </select>
        </div>
        <div class="col">
            <button id="searchButton"
                    type="submit"
                    class="btn btn-primary float-right">
                    <i class="fa fa-search"></i>
                    ${ui.message("Search")}
            </button>
        </div>
        <div class="col pr-0">
            <button id="resetBtn"
                    type="reset"
                    class="btn btn-dark bg-dark float-right">
                    <i class="fa fa-undo"></i>
                    ${ui.message("Reset")}
            </button>
        </div>
    </div>
</div>

<div class="card-body mt-5">
    <div class="table-responsive">
        <table id="laboratoryTestOrdersListTable"
               class="table table-striped table-sm table-bordered">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Lab Test</th>
                    <th>Status</th>
                    <th>Requested By</th>
                    <th>Date Requested</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>