
<style>
    .col {
        margin-bottom: 5px;
    }
</style>
<% if (allowedWidgets.size() > 0) { %>
<div class="card px-0">
    <div class="row">

        <% if (allowedWidgets.contains('1') && hivStatus) { %>
        <div class="col">
            <span class="badge badge-pill
                <% if(hivStatus.toLowerCase().contains('positive')) { %>  badge-warning <% } else { %>  badge-success <% } %>">
                HIV Status
                -
                ${hivStatus}
            </span>
        </div>
        <% } %>

        <% if (allowedWidgets.contains('2') && firstMissed) { %>
        <div class="col">
            <span class="badge badge-pill badge-warning">
                Missed 48 hour review
            </span>
        </div>
        <% } %>

        <% if (allowedWidgets.contains('3') && secondMissed) { %>
        <div class="col">
            <span class="badge badge-pill badge-warning">
                Missed 7 days review
            </span>
        </div>
        <% } %>

        <% if (allowedWidgets.contains('4') && thirdMissed) { %>
        <div class="col">
            <span class="badge badge-pill badge-warning">
                Missed 42 days review
            </span>
        </div>
        <% } %>

        <% if (allowedWidgets.contains('5')) { %>
        <div class="col">
            <span class="badge badge-pill badge-success">
                Post-operations Completed -
                ${completed} / 3
            </span>
        </div>
        <% } %>
    </div>
</div>
<% } %>
