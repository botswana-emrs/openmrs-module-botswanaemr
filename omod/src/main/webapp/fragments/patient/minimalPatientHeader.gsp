<%

    def replaceVisitIdInUrl(String url, String newVisitId) {
        // Extract query string from URL
        String queryString = url.substring(url.indexOf("?") + 1)

        Map<String, String> queryParams = [:]
        String[] params = queryString.split("&")
        String returnUrl = null

        params.each { param ->
            if (param.isEmpty()) return // Skip empty parameters

            // Split key-value pair
            String[] keyValue = param.split("=", 2)
            String key = keyValue[0]
            String value = keyValue.length > 1 ? keyValue[1] : ""

            // Decode the value if needed
            if (key == "returnUrl") {
                value = URLDecoder.decode(value, "UTF-8")
				// if visitId is present in returnUrl, replace it with new visitId, if not present, add it
				if (value.contains("visitId=")) {
					value = value.replaceAll("visitId=\\d+", "visitId=" + newVisitId);
				} else {
					value += "&visitId=" + newVisitId;
				}

				// Replace visitId in returnUrl
				returnUrl = URLEncoder.encode(value, "UTF-8");

            }

            queryParams[key] = value
        }

        // Remove old visitId and returnUrl from query parameters
        queryParams.remove("visitId")
        queryParams.remove("returnUrl")

        // Add new visitId and returnUrl to query parameters
        queryParams["visitId"] = URLEncoder.encode(newVisitId, "UTF-8")
        if (returnUrl != null) {
            queryParams["returnUrl"] = returnUrl
        }

        // Reconstruct the URL with new query parameters
        String newQuery = queryParams.collect { key, value -> "${key}=${value}" }.join("&")

        URI uri = new URI(url)
        URI newUri = new URI(
            uri.scheme,
            uri.userInfo,
            uri.host,
            uri.port,
            uri.path,
            newQuery,
            uri.fragment
        )

        return newUri.toString()
    }    

%>
<script>
    jq(function () {
        jq("#savePastVisit").click(function () {
            jq.getJSON('${ ui.actionLink("botswanaemr", "visit/patientVisit", "createPastVisit") }', {
                patient: jq("#patient").val(),
                pastVisitDate: jq("#pastVisitDate").val(),
            }).success(function (data) {
                jq().toastmessage('showSuccessToast', "Past Visit created successfully");
                let url = '${replaceVisitIdInUrl(ui.thisUrl(), 'placeholderVisitId')}';
                url = url.replace('placeholderVisitId', data['visitId']);
                window.location = url;
            });
        });
    });
</script>
<style>
    table.table td.small-td {
        width: 20%;
        white-space: nowrap;
    }

    table.table td.large-td {
        width: 80%;
    }
    .text-normal {
        color: #868e96 !important;
    }
</style>

<div class="card px-0 pb-0">
    <div class="row">
        <div class="col col-md-3 text-right">
            <h5 class="text-primary text-left">BIO DATA</h5>
        </div>
        <div class="col col-md-3">
            ${ ui.includeFragment("botswanaemr", "widget/cancelAndGoBack") }
        </div>
        <div class="col text-right">
            <% if (activePatientVisit) { %>
            <span class="badge badge-pill badge-success">
                <i class="fa fa-h-square"></i>
                Active Visit
                <i class="fa fa fa-clock-o"></i>
                ${activePatientVisit.startDatetime}
            </span>
            <% } %>

            <div class="btn-group">
                <button type="button"
                        class="btn btn-sm btn-primary dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false">
                    <i class="fa fa-external-link"></i> Past Visits
                </button>

                <div class="dropdown-menu dropdown-menu-right">
                    <% if (patientVisits) { %>
                    <% patientVisits.each { %>
                    <a href="${replaceVisitIdInUrl(ui.thisUrl(), it.uuid)}"
                       class="dropdown-item">${it.visitType.name} ${it.startDatetime} --> ${it.stopDatetime}</a>
                    <% } %>
                    <% } %>
                    <button style="margin:10px;" type="button" class="btn btn-success right" data-toggle="modal"
                            data-target="#quick-past-visit-creation-dialog">Add Past Visit</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <hr class="divider pt-1 bg-primary"/>    
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table class="table mt-3 mb-3">
                <tbody>
                    <tr>
                        <td class="small-td">NAME: </td>
                        <td class="text-left">
                            <span class="text-normal">${patient.person.personName.givenName}&nbsp;${patient.person.personName.familyName}</span>
                            <span class="text-capitalize font-weight-bolder text-normal">(${patient.gender})</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="small-td">AGE: </td>
                        <td class="text-primary text-left">
                            <span class="text-normal">${patient.person.age} Years (${patient.person.birthdate?.format("dd-MMM-yyyy")}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col">
            <table class="table mt-3">
                <tbody>
                <tr>
                    <td class="small-td">PATIENT ID: </td>
                    <td class="text-primary text-left">
                        <span class="text-normal">${pin}</span>
                    </td>
                </tr>
                <% if (showHivStatus) { %>
                    <tr>
                        <td class="small-td">HIV STATUS: </td>
                        <td class="text-primary text-left">
                            <span class="text-normal">${showHivStatus}</span>
                        </td>
                    </tr>
                    <% if (showHivStatus.toLowerCase() == "negative") { %>
                    <tr>
                        <td class="small-td">DATE OF TEST: </td>
                        <td class="text-primary text-left">
                            <span class="text-normal">${dateTested?.format("dd-MMM-yyyy")}</span>
                        </td>
                    </tr>
                    <% } %>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#quick-past-visit-creation-dialog").appendTo("body");
    });
</script>

<div class="modal fade" id="quick-past-visit-creation-dialog" tabindex="-1"
     data-controls-modal="beginPastVisitModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="beginPastVisitModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="beginPastVisitModalTitle">Add Past Visit</h4>
                <button type="button" id="enroll" class="btn btn-sm btn-primary" data-dismiss="modal"
                        aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>

            <div class="modal-body">
                <input type="hidden" id="patient" name="patient" value="${patient.id}"/>
                <br/>
                Visit Date:
                <input type="text" class="form-control datepicker" required id="pastVisitDate" name="pastVisitDate"
                       placeholder="yyyy-mm-dd"/> <br/>
                <script type="text/javascript">
                    jq('#pastVisitDate').datepicker({
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        "setDate": new Date(),
                        dateFormat: "yy-mm-dd",
                        yearRange: "-150:+0",
                        maxDate: 0,
                        "autoclose": true,
                        onSelect: function (data) {
                            jq("#pastVisitDate").trigger('change');
                        }
                    });
                </script>
                <br/>
                <hr/>
                <button class="cancel" data-dismiss="modal">${ui.message("Close")}</button>
                <button class="confirm right" id="savePastVisit">${ui.message("Save")}
                    <i class="icon-spinner icon-spin icon-2x" style="display: none; margin-left: 10px;"></i>
                </button>
            </div>
        </div>
    </div>
</div>
