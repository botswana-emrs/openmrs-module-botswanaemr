<script type="text/javascript">
    // Declare selectedPartners array at the global scope
    var selectedPartners = [];

    jq(function () {
        jq("#searchPartner-btn").click(function() {
            jq(".patient-search").show();
            var searchResult;
            jQuery.ajax({
              type: "GET",
              url: '${ui.actionLink("botswanaemr", "findPatient", "findPatients")}',
              dataType: "json",
              global: false,
              async: false,
              data: {
                q:  jq("#find-patient_id").val(),
              },
              success: function (data) {
                searchResult = data;
                let list = "";
                jq.each(data.results, function(index, item) {
                    let  patientUrl = "href = /${ui.contextPath()}/botswanaemr/hts/htsClientProfile.page?patientId=" + item.uuid;
                    let identifier = (item.identifiers && item.identifiers.length > 0)
                        ? item.identifiers.map(id => id.identifierType.name.replace("OpenMRS ID", "Patient ID") + "&nbsp;&nbsp;" + id.identifier).join("&nbsp;&nbsp;&nbsp;&nbsp;")
                        : '';

                    list += "<li class='patient-search-result list-group-item d-flex justify-content-between align-items-center'>" +
                        "<span class='preferred-details'>" + identifier + "<span class='mr-4'></span>" + item.person.preferredName.display + "<span class='mr-4'></span>" + item.person.age + " years </span>" +
                        "<a  " + patientUrl + " target='_blank' class='badge badge-primary badge-pill text-white px-1 py-1'>" +
                        "${ui.message('View profile >>')}</a>" +
                        "<button class='add-partner-button btn btn-success btn-sm ml-2' data-patient-details='" + identifier +"&nbsp;&nbsp;&nbsp;"+ item.person.preferredName.display +"&nbsp;&nbsp;&nbsp;"+ item.person.age +
                        " years '>Add Partner</button>" +
                        "</li>";
                    });
                    jq(".patient-search").html(list);

                    // Handle adding partners
                    jq(".add-partner-button").click(function(e) {
                        e.preventDefault();
                        jq("#selected-partners").show();
                        let patientName = jq(this).data("patient-details");

                        // Check if the partner is not already selected
                        if (!selectedPartners.includes(patientName)) {
                            selectedPartners.push(patientName);

                            updateSelectedPartnersList();
                            // Remove existing search results container
                            jq(".patient-search").hide();
                            jq("#find-patient_id").val("");
                        }
                    });
                }
            });
            return searchResult;
        });

        jq(document).on('click', '.delete-partner', function() {
            let partnerName = jq(this).closest('li').text().trim();
            jq(this).closest('li').remove();
            // Update selectedPartners array
            selectedPartners = selectedPartners.filter(partner => partner.trim() !== partnerName);
        });

    });

    // Function to update the selected partners list
    function updateSelectedPartnersList() {
        let selectedList = "<ul>";
        for (var i = 0; i < selectedPartners.length; i++) {
            // You may fetch partner details and display them here based on patientUuid
            selectedList += "<li class='list-group-item d-flex justify-content-between align-items-center'>" + selectedPartners[i] + "<span class='delete-partner badge badge-danger badge-pill ml-3'><i class='fas fa-times'></i></span></li>";
        }
        selectedList += "</ul>";

        // Update the selected partners container
        jq("#selected-partners").html(selectedList);
    }
</script>

     <div class="input-group  pl-0 mt-3" id="searchPartner">
             <input type="text" class="outline my-0 py-1" id="find-patient_id" placeholder="Search by patient ID, Omang or Name" autocomplete="new-password">
             <div class="input-group-append" id="searchPartner-btn">
                 <button class="btn btn-md btn-primary" type='button'>
                     <i class="fa fa-search text-white" aria-hidden="true"></i>
                 </button>
             </div>
     </div>

    <ul id="d-flex justify-content-between align-items-center" class="patient-search list-group border-0"></ul>

    <div class="border p-3 mt-4" id="selected-partners" style="display: none">
        <!-- Selected partners will be displayed here -->
    </div>