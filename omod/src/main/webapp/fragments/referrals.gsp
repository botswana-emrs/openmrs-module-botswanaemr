<script>
    let referralParams = new URLSearchParams(window.location.search);
    
    jq(function() {
        getReferralData();
    
        jQuery('#makeReferralModal').on('show.bs.modal', function () {
            jQuery("#makePatientReferralForm")[0].reset();
        });
    
        jQuery('#makeReferralModal').on('hide.bs.modal', function () {
            jQuery("#makePatientReferralForm")[0].reset();
        });

        jQuery('#editReferralModal').on('hide.bs.modal', function () {
            jQuery("#editPatientReferralForm")[0].reset();
        });
    });
    
    function getReferralData() {
        jQuery.getJSON('${ui.actionLink("botswanaemr", "referrals/makeReferral", "getReferralData")}', 
            { 'patientId': referralParams.get('patientId')}
        ).done(function(data) {
            renderReferralData(data);
        }).fail(function(jqXHR) {
            console.error('Error loading referral data');
        });
    }
    
    function renderReferralData(data) {
        let referralHtml = '';

        data.patientReferrals.forEach(referral => {
            const referringDepartment = referral.referringDepartment || "";
            const facility = referral.facility || "";
            const referredTo = referral.referredTo || "";
            const receivingFacility = referral.receivingFacility || "";
            const reason = referral.reason || "";
            const referredBy = referral.referredBy || "";
            const referredDate = referral.referredDate ? new Date(referral.referredDate).toLocaleDateString('en-GB') : "";
            const status = referral.status || "";
            const notes = referral.notes || "";
        
            const statusIndicatorClass = status.toUpperCase() === 'COMPLETED' ? 'text-success' : 'text-warning';
            const statusText = status.toUpperCase() === 'COMPLETED' ? 'Completed' : 'Awaiting';
        
            const noReferralData = Object.values(referral).every(value => value === "");
        
            if (noReferralData) {
                referralHtml += `
                    <h5 class="pl-3">Referral</h5>
                    <div class="row no-data-section text-center">
                        <img class="" src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>
                        <p class="text-center">No data captured. Add data by clicking "Add" below</p>
                    </div>
                `;
            } else {
                referralHtml += `
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-sm btn-dark bg-dark float-right" data-toggle="modal" data-target="#editReferralModal"> Edit </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="row">
                                <div class="col">
                                    <ul class="list-unstyled">
                                        <li><span>Referring facility:</span> \${facility}</li>
                                        <li><span>Department referring:</span> \${referringDepartment}</li>
                                        <li><span>Reason:</span> \${reason}</li>
                                    </ul>
                                </div>
                                <div class="col">
                                    <ul class="list-unstyled">
                                        <li><span>Receiving facility:</span> \${receivingFacility}</li>
                                        <li><span>Referred by:</span> \${referredBy}</li>
                                        <li><span>Referred date:</span> \${referredDate}</li>
                                        <li>
                                            <input id="statusHolder" type="hidden" value="\${status}"/>
                                            <span>Status:</span>
                                            <i id="statusIndicator" class="fa fa-circle \${statusIndicatorClass}"></i>
                                            <span id="statusText" class="text-muted px-0" style="background: none;">\${statusText}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-12">
                                    <span>Notes:</span> \${notes}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br></br>
                `;
            }
        });
        
        referralHtml += `
            <div class="row">
                <div class="col">
                    <button type="button" class="dashed-button rounded" data-toggle="modal" data-target="#makeReferralModal">
                        <i class="fa fa-plus"></i> Add a referral
                    </button>
                </div>
            </div>
        `;
        
        document.getElementById('referralContainer').innerHTML = referralHtml;
    }
</script>    
<div id="referralContainer">
    <h5 class="pl-3">Referral</h5>
</div>
<!-- Add Referral Modal -->
<div class="modal fade" id="makeReferralModal" tabindex="-1"
    data-controls-modal="makeReferralModal" data-backdrop="static"
    data-keyboard="false" role="dialog" aria-hidden="true"
    aria-labelledby="referralModalTitle">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable"
        role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white"
                    id="referralModalTitle">Patient's Referral Details</h4>
                <button type="button" id="closeModal" class="btn btn-sm btn-primary"
                    data-dismiss="modal" aria-label="Close">
                <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ui.includeFragment("botswanaemr", "referrals/makeReferral", [patientId: patient.patient.id])}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery("#makeReferralModal").appendTo("body");
    });
</script>