<script type="text/javascript">
    jQuery('body').on('click', 'a.add-vital,a.view-screening', function(e) {
        e.preventDefault();
        clearActiveTabCookie();
        window.location.href = e.target.href;
    });

    function clearActiveTabCookie(e) {
        webUtils.clearCookie('activeTab', "/");
    }

    jQuery.fn.dataTable.ext.search.push(
        function( oSettings, aData, iDataIndex ) {
            let matchingOptions = [];
            if (jq('#searchPhrase').val() !== '') {
                let pin  = aData[0].trim();
                let name = aData[1].trim();
                matchingOptions.push(
                    pin === jq('#searchPhrase').val() || name.toLowerCase().includes(jq('#searchPhrase').val().toLowerCase())
                );
            }
            if (jq('#screenedBy').find(":selected").val() !== '') {
                let screenedBy = aData[4].trim();
                matchingOptions.push(screenedBy === jq('#screenedBy').find(":selected").text());
            }
            if (matchingOptions.length === 0) {
                return  true;
            }
            let matchingStatus = matchingOptions.reduce(function (overallStatus, currentStatus){
                return overallStatus && currentStatus;
            });
            return matchingStatus;
        }
    );

    jQuery(function () {
        jq.ajax({
            type: "GET",
            url: "${ui.actionLink('botswanaemr','patientQueueList','getPatientQueueListByQueueRoom')}",
            dataType: "json",
            global: false,
            async: true,
            data: {
                servicePoint: "${nurseServicePoint != null ? nurseServicePoint : ''}",
                sourcePageName: "consultation/auxilliaryNurseDashboard"
            },
            success: function (data) {
                // console.log(data);

                var awaitingData = data.filter(function(item) {
                    return item.status === "AWAITING";
                });

                var awaitingScreeningListSize = awaitingData.length;

                jQuery("#awaitingScreeningListSize").find('h5').text(awaitingScreeningListSize);

                var table = jQuery('#patientQueueListTable').DataTable({
                    data: data,
                    bAutoWidth: false,
                    columns: [
                        {'data': 'pin'},
                        {'data': 'patientNames'},
                        {'data': 'gender'},
                        {'data': 'age'},
                        {'data': 'providerNames'},
                        {'data': 'recentQueueLocation',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                       return row.recentQueueLocation;
                                    } else {
                                       return '';
                                    }
                                }
                                return data;
                            }                        
                        },
                        {'data': 'dateCreated'},
                        {'data': 'status',
                            "render": function(data, type, row, meta){
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                       return '<i class="fa fa-circle text-success"></i><span class="text-muted px-0" style="background: none;"> Screened</span>';
                                    } else {
                                       return '<i class="fa fa-circle text-warning"></i><span class="text-muted px-0" style="background: none;"> Awaiting</span>';
                                    }
                                }
                                return data;
                            }
                        },
                        {'data': null,
                            "render": function(data, type, row, meta){
                                var formUuid = '${tbScreeningFormUuid}';
                                if(type === 'display'){
                                    if(row.status == "TREATED"){
                                        let url = '<a href="/${ui.contextPath()}/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'" class="text-primary float-left pl-3 view-screening" id="view">View</a>';
                                        if (row.recentQueueLocation == 'Screening') {
                                            url = url + ' |&nbsp;<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0" id="reassign">Reassign</a>';
                                        }
                                        return url;
                                    }
                                    let auxilliaryNurseDashboardUrl = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/consultation/auxilliaryNurseDashboard.page?appId=botswanaemr.auxilliaryNurseDashboard';
                                    if(row.hasTbScreeningEncounter == true) {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/editEncounter.page?patientId='+row.patientId+'&encounterId='+row.encounterIdentifier+'&formUuid='+formUuid+'&returnUrl='+row.returnUrl+'" class="text-info p-1" id="edit">Screen</a>|&nbsp;'+
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0" id="reassign">Reassign</a>|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&returnUrl='+auxilliaryNurseDashboardUrl+'" class="text-primary pl-0 add-vital" id="addVital">Add Vitals</a>|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/consultation/printVitals.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&returnUrl='+auxilliaryNurseDashboardUrl+'" class="text-primary pl-0 print-vital" id="printVitals">Print Vitals</a>';
                                    } else {
                                        return '<a href="/${ui.contextPath()}/botswanaemr/enterForm.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&formUuid='+formUuid+'&returnUrl='+row.returnUrl+'" class="text-primary p-1" id="screen">Screen</a>|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/patientQueueManager.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&patientQueueId='+row.patientQueueId+'" class="text-primary pl-0" id="reassign">Reassign</a>|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/consultation/nursesExaminationPatientPool.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&returnUrl='+auxilliaryNurseDashboardUrl+'" class="text-primary pl-0 add-vital" id="addVital">Add Vitals</a>|&nbsp;' +
                                               '<a href="/${ui.contextPath()}/botswanaemr/consultation/printVitals.page?patientId='+row.patientId+'&visitId='+row.visitNumber+'&returnUrl='+auxilliaryNurseDashboardUrl+'" class="text-primary pl-0 print-vital" id="printVitals">Print Vitals</a>';
                                    }
                                }
                                return data;
                            }
                        }
                    ],
                    dom: 'rtp',
                    searching: true,
                    "oLanguage": {
                        "oPaginate": {
                            "sNext": '<i class="fa fa-chevron-right py-1" ></i>',
                            "sPrevious": '<i class="fa fa-chevron-left py-1" ></i>'
                        }
                    },
                    order: [[7, 'asc'], [6, 'asc']],
                });
                jq('#inquiryBtn').on('click', () => {
                    table.draw();
                });

                jq('#resetBtn').on('click', () => {
                    fieldHelper.clearAllFields(jq('#screeningPool'));
                    table.draw();
                });

                jq('#searchPhrase').on( 'keyup', function () {
                    let timeout;
                    let delay = 1000;
                    let searchText = this.value;
                    if (searchText.length >= 3) {
                        if(timeout) {
                            clearTimeout(timeout);
                        }
                        timeout = setTimeout(function() {
                           table.draw();
                        }, delay);
                    }
                });
            }
        });
    });
</script>

<div class="row">
    <div class="col-sm-5 col-md-5 col-lg-5 pl-0 pr-0 pb-5">
        <label>Search</label>
        <input id="searchPhrase"
               type="text"
               name="searchPhrase"
               class="form-control"
               value=""
               placeholder="${ui.message('Search by Patient ID or Name')}"
        />
    </div>
    <div class="col-sm-4 col-md-4 col-lg-4 pr-0 pb-5">
        <label>Screened by</label>
        <select id="screenedBy"
                class="form-control"
                placeholder="${ui.message('Select nurse ')}">
            <option value="">Select One</option>
            <% providerList.each { provider-> %>
            <% if (provider != null && provider?.person != null && provider?.person?.personName != null) { %>
            <option value="<%=provider.uuid %>"><%=provider.person.personName %></option>
            <% } %>
            <% } %>
        </select>
    </div>
    <div class="col-sm-3 col-md-3 col-lg-3 mt-3 pr-0">
        <label>&nbsp;</label>
        <button id="inquiryBtn"
                type="submit"
                class="btn btn-primary mt-3">
                ${ui.message("Search")}
        </button>
        <button id="resetBtn"
                type="reset"
                class="btn btn-dark bg-dark mt-3 float-right">
                ${ui.message("Reset")}
        </button>
        <script type="text/javascript">
            let redirectToNewRegistrationPage = () => {
                window.location = "/"+ OPENMRS_CONTEXT_PATH + "/botswanaemr/registerPatient.page";
            };
        </script>
    </div>
</div>

<div class="card-header">
     <span id="awaitingScreeningListSize">Awaiting screening: <h5 class="badge badge-pill badge-primary"></h5></span>
</div>
<table id="patientQueueListTable"
       class="table table-striped table-sm table-bordered">
    <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Sex</th>
            <th>Age</th>
            <th>Screened By</th>
            <th>Assigned To</th>
            <th>Date/Time</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
</table>
