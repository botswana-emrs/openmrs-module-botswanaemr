<%
    ui.includeJavascript("uicommons", "angular.min.js")
    ui.includeJavascript("uicommons", "angular-ui/ui-bootstrap-tpls-0.11.2.min.js")
    ui.includeJavascript("allergyui", "allergy.js")
    ui.includeJavascript("uicommons", "angular-resource.min.js")
    ui.includeJavascript("uicommons", "angular-common.js")

    ui.includeJavascript("coreapps", "conditionlist/restful-services/restful-service.js");

    ui.includeJavascript("botswanaemr", "managetriage.controller.js")
%>
<script>
    jQuery(function () {
        jQuery("#editSymptomsModal").appendTo("body");
    });
</script>
<div class="row">
    <div class="col-12">
        <% if (visit == null) {%>
        <div class="col col-sm-12 col-md-12">
            <div class="alert alert-warning text-danger" role="alert">
                <strong><i class="fa fa-warning"></i> No active visit found. Please start a patient visit to proceed!</strong>
            </div>
        </div>
        <%} else {%>
        <div class="info-section symptoms" ng-app="triageDataApp" ng-controller="TriageDataController"
             ng-init="getSymptomsData('${visit.uuid}')">
            <div class="info-header">
                <div class="row">
                    <div class="col-4 zero-padding text-left">
                        <h5>${ui.message('Symptoms')}</h5>
                    </div>

                    <div class="col-4 zero-padding text-left">
                        <h5 class="h5"  ng-show="symtomsObservations.length > 0">Notes</h5>
                    </div>

                    <div class="col-2 zero-padding text-left">
                        <h5 class="h5" ng-show="symtomsObservations.length > 0" >Duration</h5>
                    </div>

                    <div class="col-2 zero-padding text-left">
                        <h5 class="h5" ng-show="symtomsObservations.length > 0" >Unit</h5>
                    </div>
                </div>
            </div>

            <div class="info-body">
                <ul>
                    <li ng-repeat="symptom in symtomsObservations">
                        <div class="row">


                            <div class="col-4 zero-padding ">
                                <div ng-if="symptom.value.name">
                                    <span class="p normal-text">{{symptom.value.name.name}}</span>
                                </div>

                                <div ng-if="!symptom.value.name">
                                    <span class="p normal-text">{{symptom.value}}</span>
                                </div>
                            </div>

                            <div class="col-4 zero-padding ">
                                <div ng-show="symptom.comment && symptom.comment !==''">
                                    <span class="p normal-text">{{symptom.comment}}</span>
                                </div>

                                <div ng-show="!symptom.comment">
                                    <span class="p normal-text">-</span>
                                </div>
                            </div>

                            <div class="col-2 zero-padding ">
                                <div ng-show="symptom.symptomsDuration && symptom.symptomsDuration !==''">
                                    <span class="p normal-text">{{symptom.symptomsDuration}}</span>
                                </div>

                                <div ng-show="!symptom.symptomsDuration">
                                    <span class="p normal-text">-</span>
                                </div>
                            </div>

                            <div class="col-2 zero-padding ">
                                <div ng-show="symptom.symptomsDurationUnit && symptom.symptomsDurationUnit.name">
                                    <span class="p normal-text">{{symptom.symptomsDurationUnit.name.display}}</span>
                                </div>

                                <div ng-show="!symptom.symptomsDurationUnit">
                                    <span class="p normal-text">-</span>
                                </div>
                            </div>

                        </div>
                    </li>
                </ul>

                <div ng-show="symtomsObservations.length == 0" class="row no-data-section text-center">
                    <img class=""
                         src="${ui.resourceLink('botswanaemr', 'images/no-task.svg')}" alt="no data"/>

                    <p class="text-center">No data captured</p>
                </div>
            </div>
            <div class="row col col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                <button class="dashed-button rounded"
                        id="btnPhysicalExam" data-toggle="modal" data-target="#editSymptomsModal"
                <% if(!isConsultationActive) { %> disabled <% } %> >+ Add Presenting Complaint</button>
            </div>

        </div>
        <%}%>
    </div>
</div>

<!-- Nodal -->
<div class="modal fade" id="editSymptomsModal" tabindex="-1"
     data-controls-modal="editSymptomsModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="editSymptomsModalTitle">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title text-white" id="editSymptomsTitle">Edit Symptoms</h4>
                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="col">
                    ${ ui.includeFragment("botswanaemr", "consultation/editSymptoms", [patientId: patient.patient.uuid, visit: visit]) }
                </div>
            </div>
        </div>
    </div>
</div>
