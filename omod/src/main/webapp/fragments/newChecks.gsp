<%
    ui.includeJavascript("botswanaemr", "utils/vitals-utils.js")
%>
<style>
.text-separator {
    margin: auto 0.5rem;
}
</style>
<script type="text/javascript">
    jq(function () {
        let newChecksForm = \$("#newChecksForm");
        if (newChecksForm.length > 0) {
            newChecksForm.validate({
                rules: {
                    sbp: {
                        digits: true,
                        max: 250,
                        min: 0
                    },
                    dbp: {
                        digits: true,
                        max: 150,
                        min: 0
                    },
                    temperature: {
                        number: true,
                        max: 43,
                        min: 25
                    },
                    weight: {
                        number: true,
                        max: 250,
                        min: 0
                    },
                    height: {
                        number: true,
                        max: 272,
                        min: 10
                    },
                    bmi: {
                        number: true
                    },
                    bsa: {
                        number: true
                    },
                    rrate: {
                        number: true,
                        max: 99,
                        min: 0
                    },
                    pulse: {
                        number: true,
                        max: 230,
                        min: 0
                    },
                    hcircumference: {
                        number: true
                    },
                    glucoseLevel: {
                        number: true
                    },
                    oxygenSaturation: {
                        number: true,
                        max: 100,
                        min: 50,
                    },
                }
            });
        }

        function updateBmi() {
            let wt = jq("#weight").val();
            let ht = jq("#height").val();

            let bmi = calculateBmi(wt, ht);

            if (bmi != null && !isNaN(bmi)) {
                jq("#bmi").val(bmi.toFixed(1));
            } else {
                jq("#bmi").val('');
            }
        }

        function updateBsa() {
            let wt = jq("#weight").val();
            let ht = jq("#height").val();

            let bsa = calculateBsa(wt, ht);

            if (bsa != null && !isNaN(bsa)) {
                jq("#bsa").val(bsa.toFixed(2));
            } else {
                jq("#bsa").val('');
            }
        }

        jq("#weight, #height").change(function (e) {
            updateBmi();
            updateBsa()
        });

        jq("#newChecksForm").submit(function (e) {
            e.preventDefault();
            let valid = \$("#newChecksForm").valid();
            if (!valid) {
                return false;
            }
            var vitals = {
                'patientId': ${activePatient.patientId},
                'patientQueueId': ${patientQueueId},
                'temperature': jq('#temperature').val(),
                'sbp': jq('#sbp').val(),
                'dbp': jq('#dbp').val(),
                'weight': jq('#weight').val(),
                'height': jq('#height').val(),
                'bmi': jq('#bmi').val(),
                'bsa': jq('#bsa').val(),
                'rRate': jq('#rRate').val(),
                'pulse': jq('#pulse').val(),
                'hCircumference': jq('#hCircumference').val(),
                'glucoseLevel': jq('#glucoseLevel').val(),
                'oxygenSaturation': jq('#oxygenSaturation').val(),
                'lmnp': jq('#lmnp').val(),
                'pregnancyKnown': jq('#pregnancyKnown').val(),
                'conscious': jq('#conscious').val(),
                'responsive': jq('#responsive').val(),
                'vitals': false
            };

            jq.getJSON('${ ui.actionLink("botswanaemr", "newChecks", "captureVitals")}', vitals)
                .success(function (data) {
                    jq().toastmessage('showNoticeToast', "The vitals have been captured");
                    showLoadingOverlay();
                    location.reload();
                })

        });

    });
</script>

<form method="post" id="newChecksForm">
    <div class="row">
        <label>Please fill in the details below to capture the patients vitals</label>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="temperature">Temperature(C)</label>
                <input
                        id="temperature"
                        name="temperature"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <label>Blood pressure (mmHg)</label>
        </div>

        <div class="row">
            <div class="col-md-3 mr-0 pr-0">
                <div class="form-group">
                    <input
                            id="sbp"
                            name="sbp"
                            type="text"
                            value=""
                            digits
                            class="form-control"/>
                </div>
            </div>

            <div class="text-separator">
                <span style="position: relative;top: -0.5rem; font-size: 2rem">/</span>
            </div>

            <div class="col-md-3 ml-0 pl-0">
                <input
                        id="dbp"
                        name="dbp"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="pulse">Pulse(b/m)</label>
                <input  id="pulse"
                        name="pulse"
                        type="text"
                        value=""
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="weight">Weight(Kg)</label>
                <input
                        id="weight"
                        name="weight"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="height">Height(cm)</label>
                <input
                        id="height"
                        name="height"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="bmi">BMI(Kg/M2)</label>
                <input  id="bmi"
                        name="bmi"
                        type="text"
                        value=""
                        digits
                        readonly="readonly"
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="bsa">BSA(M2)</label>
                <input  id="bsa"
                        name="bsa"
                        type="text"
                        value=""
                        digits
                        readonly="readonly"
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="rRate">Respiratory Rate(b/m)</label>
                <input  id="rRate"
                        name="rRate"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="hCircumference">Head Circumference(cm)</label>
                <input  id="hCircumference"
                        name="hCircumference"
                        type="text"
                        value=""
                        digits
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="glucoseLevel">Glucose Level(mm/gl)</label>
                <input  id="glucoseLevel"
                        name="glucoseLevel"
                        type="text"
                        value=""
                        class="form-control"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="oxygenSaturation">Oxygen Saturation</label>
                <input  id="oxygenSaturation"
                        name="oxygenSaturation"
                        type="text"
                        value=""
                        class="form-control"/>
            </div>
        </div>
    </div>

    <% if (patient.gender == 'F') { %>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="lmnp">LNMP</label>
                <input  id="lmnp"
                        name="lmnp"
                        type="text"
                        value=""
                        class="form-control"/>
            </div>
        </div>
    </div>
    <% } %>

    <% if (patient.gender == 'F') { %>
    <div>
        <div class="col">
            <div class="form-group">
                <label for="pregnancyKnown">Are you pregnant?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="pregnancyKnown"
                        name="pregnancyKnown">
                    <option disabled selected value="">Select Status</option>
                    <option value="1065">Yes</option>
                    <option value="1066">No</option>
                    <option value="1067">Unknown</option>
                </select>
            </div>
        </div>
    </div>
    <% } %>

    <div>
        <div class="col">
            <div class="form-group">
                <label for="conscious">Conscious?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="conscious"
                        name="conscious">
                    <option selected disabled value="">Select Status</option>
                    <option value="1065">Yes</option>
                    <option value="1066">No</option>
                </select>
                <input type="hidden" value="${consciousObsId}" id="consciousObsId" name="consciousObsId">
            </div>
        </div>
    </div>

    <div>
        <div class="col">
            <div class="form-group">
                <label for="responsive">Responsive?</label>
                <select type="text"
                        class="form-control custom-select"
                        id="responsive"
                        name="responsive">
                    <option selected disabled value="">Select Status</option>
                    <option value="1065">Yes</option>
                    <option value="1066">No</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row button-section-right pr-3">
        <button type="button" class="btn btn-sm btn-dark bg-dark float-right" data-dismiss="modal">Close</button>
        <button id="saveVitals" type="submit" class="btn btn-sm btn-primary float-right ml-1" value="saveVitals"
                name="save">${ui.message("Save")}
        </button>
    </div>
</form>
