<script>
    jq(function() {
        jq("#makePatientReferralForm").submit(function(e){
            e.preventDefault();
            var params = {
                'patientId': jq('#patientId').val(),
                'providerId' : jq('#providersId').find(":selected").val(),
                'receivingDepartmentId' : jq('#receivingDepartmentId').find(":selected").val(),
                'receivingFacilityId' : jq('#receivingFacilityId').find(":selected").val(),
                'referringDepartmentId' : jq('#referringDepartmentId').find(":selected").val(),
                'referringFacility': jq('#referringFacility').val(),
                'referralNotes': jq('#referralNotes').val(),
                'reason': jq('#reason').val()
            };

            jq.getJSON('${ ui.actionLink("botswanaemr","referrals/makeReferral", "makePatientReferral")}', params)
                .success(function (data) {
                showLoadingOverlay();
                jq().toastmessage('showSuccessToast', "Patient referred successfully");
                getReferralData();
                jQuery('#makeReferralModal').modal('toggle');
                jQuery("#makePatientReferralForm")[0].reset();
                hideLoadingOverlay();
            })
        });
    });
</script>
<form method="post" id="makePatientReferralForm">
    <input type="hidden" id="patientId" name="patientId" value="${patient.id}" class="form-control"/>
    <input type="hidden" id="referringFacility" name="referringFacility" value="${hospital}" class="form-control"/>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="referringDepartmentId"> Department referring:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="referringDepartmentId" name="referringDepartmentId" required>
                    <option value="">Select one</option>
                    <%  referringDepartments.each{ %>
                    <option value="${it.locationId}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>

            <div class="form-group">
                <label for="receivingFacilityId"> Receiving facility:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="receivingFacilityId" name="receivingFacilityId" required>
                    <option value="">Select one</option>
                    <%  receivingFacilities.each{ %>
                    <option value="${it.locationId}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>
        </div>

        <div class="col">
            <div class="form-group">
                <label for="receivingDepartmentId"> Department receiving:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="receivingDepartmentId" name="receivingDepartmentId" required>
                    <option value="">Select one</option>
                    <%  receivingDepartments.each { %>
                    <option value="${it.locationId}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>

            <div class="form-group">
                <label for="providerId"> Provider referred to: </label>
                <select class="form-control custom-select" id="providerId" name="providerId">
                    <option value="">Select one</option>
                    <%  providers.grep{!it.name.equals(sessionContext.currentProvider.name) && it.name != null}.each { %>
                    <option value="${it.providerId}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label for="reason"> Reason
                    <span class="text-danger">*</span>
                </label>
                <script>
                    jq(document).ready(function() {
                        var diagnoses = ${diagnosisConceptNames}
                        jq( "#reason" ).autocomplete({
                          source: diagnoses
                        });
                    });
                </script>
                <input class="form-control" id="reason" name="reason" required value="${reason ?: ''}">
            </div>

            <div class="form-group">
                <label for="referralNotes">Notes: </label>
                <textarea class="form-control" rows="5"  id="referralNotes" name="referralNotes"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <button id="saveBtn" type="submit" class="btn btn-sm btn-primary float-right ml-1">Save</button>
            <button id="closeBtn" type="button" class="btn btn-sm btn-dark bg-dark float-right ml-1" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>





