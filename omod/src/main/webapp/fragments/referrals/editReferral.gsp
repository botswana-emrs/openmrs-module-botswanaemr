<script>
    jq(function() {
        jq("#editPatientReferralForm").submit(function(e){
            e.preventDefault();
            var params = {
                'patientId': jq('#patientId').val(),
                'providerId' : jq('#providerId').find(":selected").val(),
                'referralId': jq('#referralId').val(),
                'referralEncounterUuid': jq('#referralEncounterUuid').val(),
                'status' : jq('#status').find(":selected").val(),
                'receivingDepartmentName' : jq('#receivingDepartmentName').find(":selected").val(),
                'receivingFacilityName' : jq('#receivingFacilityName').find(":selected").val(),
                'referringDepartmentName' : jq('#referringDepartmentName').find(":selected").val(),
                'referralNotes': jq('#referralNotes').val(),
                'reason': jq('#reason').val()
            };

            jq.getJSON('${ ui.actionLink("botswanaemr","referrals/editReferral", "editPatientReferral")}', params)
                .success(function (data) {
                showLoadingOverlay();
                jq().toastmessage('showSuccessToast', "Patient's referral updated successfully");
                getReferralData();
                jq("#editPatientReferralForm")[0].reset();
                jQuery('#editReferralModal').modal('toggle');
                hideLoadingOverlay();
            })
        });
    });
</script>
<form method="post" id="editPatientReferralForm">
    <input type="hidden" id="patientId" name="patientId" value="${patient.id}" class="form-control"/>
    <input type="hidden" id="referralId" name="referralId" value="${referralId}" class="form-control"/>
    <input type="hidden" id="referralEncounterUuid" name="referralEncounterUuid" value="${referralEncounter.uuid}" class="form-control"/>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="referringDepartmentName"> Department referring:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="referringDepartmentName" name="referringDepartmentName" required>
                    <option value="${referringDepartment?.valueText ?: ''}">
                        ${referringDepartment?.valueText ?: 'Select one'}
                    </option>
                    <%  referringDepartments.each{ %>
                    <option value="${it.name}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>

            <div class="form-group">
                <label for="receivingFacilityName"> Receiving facility:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="receivingFacilityName" name="receivingFacilityName" required>
                    <option value="${receivingFacility?.comment ?: ''}">
                        ${receivingFacility?.comment ?: 'Select one'}
                    </option>
                    <%  receivingFacilities.each{ %>
                    <option value="${it.name}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>
        </div>

        <div class="col">
            <div class="form-group">
                <label for="receivingDepartmentName"> Department receiving:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="receivingDepartmentName" name="receivingDepartmentName" required>
                    <option value="${receivingFacility?.valueText ?: ''}">
                        ${receivingFacility?.valueText ?: 'Select one'}
                    </option>
                    <%  receivingDepartments.each { %>
                    <option value="${it.name}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>

            <div class="form-group">
                <label for="status"> Status:
                    <span class="text-danger">*</span>
                </label>
                <select class="form-control custom-select" id="status" name="status" required>
                    <option value="${status ?: ''}">
                        <% if(status.equals("IN_PROGRESS")) { %> println("IN PROGRESS") <% } else { %> ${status} <% } %>
                    </option>
                    <option value="RECEIVED">RECEIVED</option>
                    <option value="COMPLETED">COMPLETED</option>
                </select>
            </div>
        </div>
        
        <div class="col-12">
            <div class="form-group">
                <label for="providerId"> Provider referred to: </label>
                <select class="form-control custom-select" id="providerId" name="providerId">
                    <%  providers.grep{!it.name.equals(sessionContext.currentProvider.name) && it.name != null}.each { %>
                    <option value="${it.providerId}">
                        ${it.name}
                    </option>
                    <% } %>
                </select>
            </div>

            <div class="form-group">
                <label for="reason"> Reason
                    <span class="text-danger">*</span>
                </label>
                <script>
                    jq(document).ready(function() {
                        var diagnoses = ${diagnosisConceptNames}
                        jq( "#reason" ).autocomplete({
                          source: diagnoses
                        });
                    });
                </script>
                <input class="form-control" id="reason" name="reason" required value="${reason ?: ''}">
            </div>

            <div class="form-group">
                <label for="referralNotes">Notes: </label>
                <textarea class="form-control" rows="5"  id="referralNotes" name="referralNotes">${notes ?: ""}</textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <button id="saveBtn" type="submit" class="btn btn-sm btn-primary float-right ml-1">Save</button>
            <button id="closeBtn" type="button" class="btn btn-sm btn-dark bg-dark float-right ml-1" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>
