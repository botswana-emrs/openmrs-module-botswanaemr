<%
    def addContextPath = {
        if (!it)
            return null
        if (it.startsWith("/")) {
            it = "/" + org.openmrs.ui.framework.WebConstants.CONTEXT_PATH + it
        }
        return it
    }
    def logoIconUrl = addContextPath(configSettings?."logo-icon-url") ?: ui.resourceLink("botswanaemr", "images/moh-logo-01.png")
    def logoLinkUrl = addContextPath(configSettings?."logo-link-url") ?: "/${org.openmrs.ui.framework.WebConstants.CONTEXT_PATH}"
    def multipleServicePoints = (servicePoints.size > 1);
    def enableUserAccountExt = userAccountMenuItems.size > 0;
%>
<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    jq( document ).ready(function() {
        jq('[data-toggle=search-form]').click(function() {
            jq('.search-form-wrapper').toggleClass('open');
            jq('html').toggleClass('search-form-open');
          });
          jq('[data-toggle=search-form-close]').click(function() {
            jq('.search-form-wrapper').removeClass('open');
            jq('html').removeClass('search-form-open');
          });
        jq('.search-form-wrapper .search').keypress(function( event ) {
          if(jq(this).val() == "Search") jq(this).val("");
        });

        jq('.search-close').click(function(event) {
          jq('.search-form-wrapper').removeClass('open');
          jq('html').removeClass('search-form-open');
        });

        function getWindowHash(){
            return window.location.hash;
        }
        
        let inactivityTimeout;
        let reminderTimeout;
        const reminderElement = document.getElementById("inactivityModal");
        const stayLoggedInButton = document.getElementById("stay-logged-in");
        const logOutButton = document.getElementById("log-out");

        function resetInactivityTimer() {
            clearTimeout(inactivityTimeout);
            clearTimeout(reminderTimeout);
            hideReminder();
            startInactivityTimer();
        }

        function startInactivityTimer() {
            // reminder after 5 minutes of inactivity
            inactivityTimeout = setTimeout(showReminder, 5 * 60 * 1000);
        }

        function showReminder() {
            jQuery('#inactivityModal').modal({show: true, backdrop: 'static', keyboard: false});
            reminderTimeout = setTimeout(logOutUser, 2 * 60 * 1000);
        }

        function hideReminder() {
            jQuery('#inactivityModal').modal('hide');
        }

        function logOutUser() {
            window.location.href = "/${contextPath}/ms/logout";
        }

        stayLoggedInButton.addEventListener("click", function() {
            resetInactivityTimer();
            hideReminder();
        });

        logOutButton.addEventListener("click", logOutUser);

        ["mousemove", "keydown", "click", "scroll"].forEach(event => {
            document.addEventListener(event, function() {
                if (reminderElement.style.display !== "block") {
                    resetInactivityTimer();
                }
            });
        });

        startInactivityTimer();
    });
</script>

<script type="text/javascript">
    var sessionLocationModel = {
      id: ko.observable(),
      text: ko.observable()
    };
    jq(function () {
      ko.applyBindings(sessionLocationModel, jq('.change-location').get(0));
      sessionLocationModel.id(${ servicePointVisited?.locationId });
      sessionLocationModel.text("${ ui.format(servicePointVisited) }");
      // we only want to activate the functionality to change location if there are actually multiple login locations
      <% if (multipleServicePoints) { %>
      jq(".change-location a").click(function () {
        jq('#session-location').show();
        jq(".change-location a i:nth-child(3)").removeClass("icon-caret-down");
        jq(".change-location a i:nth-child(3)").addClass("icon-caret-up");
      });
      jq('#session-location').mouseleave(function () {
        jq('#session-location').hide();
        jq(".change-location a i:nth-child(3)").addClass("icon-caret-down");
        jq(".change-location a i:nth-child(3)").removeClass("icon-caret-up");
      });
      jq("#session-location ul.select li").click(function (event) {
        var element = jq(event.target);
        var locationId = element.attr("locationId");
        var locationUuid = element.attr("locationUuid");
        var locationName = element.attr("locationName");
        var data = {locationId: locationId};
        jq("#spinner").show();
        jq.post(emr.fragmentActionLink("botswanaemr", "botswanaEMRHeader", "setCurrentServiceDeliveryLocation", data), function (data) {
          sessionLocationModel.id(locationId);
          sessionLocationModel.text(locationName);
          jq('#selected-location').attr("location-uuid", locationUuid);
          jq('#session-location li').removeClass('selected');
          element.addClass('selected');
          jq("#spinner").hide();
          jq(document).trigger("sessionLocationChanged");
            locationUuid = data.locationUuid;
            if (locationUuid === '${artServicesUuid}') {
                // ART Services
                location.href = '${ui.pageLink("botswanaemr", "art/artDashboard")}';
            } else if (locationUuid === '${tbServicesUuid}') {
                // TB Services
                location.href = '${ui.pageLink("botswanaemr", "tbservices/tbServicesDashboard")}';
            } else if (locationUuid === '${hivTestingServicesPortal}') {
                // HIV Testing Services
                location.href = '${ui.pageLink("botswanaemr", "hts/htsDashboard")}';
            } else if (locationUuid === '${registrationPageUuid}') {
                // Registration page
                location.href = '${ui.pageLink("botswanaemr", "registrationAdminDashboard?appId=botswanaemr.registrationAdminDashboard")}';
            } else if (locationUuid === '${pharmacyPortalUuid}') {
                // Pharmacy Portal
                location.href = '${ui.pageLink("botswanaemr", "pharmacy/pharmacyAllPrescriptions")}';
            } else if (locationUuid === '${doctorsPortalUuid}') {
                // Doctors
                location.href = '${ui.pageLink("botswanaemr", "consultation/doctorsPatientPoolDashboard?appId=botswanaemr.auxilliaryNurseDashboard")}';
            } else if (locationUuid === '${nursePortalUuid}') {
                // Nurse
                location.href = '${ui.pageLink("botswanaemr", "consultation/auxilliaryNurseDashboard?appId=botswanaemr.auxilliaryNurseDashboard")}';
            } else if (locationUuid === '${vmmcPortalUuid}') {
                // VMMC Dashboard
                location.href = '${ui.pageLink("botswanaemr", "vmmc/vmmcDashboard?appId=botswanaemr.vmmcDashboard")}';
            } else if (locationUuid === '${stockManagementPortalUuid}') {
                // Stock Management Portal Dashboard
                location.href = '${ui.pageLink("botswanaemr", "stock/stockManagementLandingPg")}';
            } else if (locationUuid === '${labServicesPortalUuid}') {
                // Lab Services Portal Dashboard
                location.href = '${ui.pageLink('botswanaemr', '/lab/groupedLaboratoryOrders')}';
            } else if (locationUuid === '${sexualReproductiveHealthPortalUuid}') {
                // Family Planning Portal Dashboard
                location.href = '${ui.pageLink("botswanaemr", "srh/srhDashboard")}';
            } else {
                location.reload()
            }
        })
        jq('#session-location').hide();
        jq(".change-location a i:nth-child(3)").addClass("icon-caret-down");
        jq(".change-location a i:nth-child(3)").removeClass("icon-caret-up");
      });
      <% if (enableUserAccountExt) { %>
      jq('.identifier').hover(
          function () {
            jq('.appui-toggle').show();
            jq('.appui-icon-caret-down').hide();
          },
          function () {
            jq('.appui-toggle').hide();
            jq('.appui-icon-caret-down').show();
          }
      );
      jq('.identifier').css('cursor', 'pointer');
      <% } %>
      <% } %>
    });
</script>
<header class="header">
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-primary">
        <div class="container-fluid">
            <div class="col pl-0">
                <button class="btn btn-sm btn-primary px-0 py-0"
                        type="button"
                        id="sidebarCollapse">
                    <i class="fa fa-bars fa-2x"></i>
                </button>
            </div>
            <div class="col">
                <h4 class="lead text-white justify-content-center">
                    ${hospital}
                </h4>
            </div>

            <button class="btn btn-sm btn-primary px-0 py-0 d-lg-none ml-auto"
                   type="button"
                   data-toggle="collapse"
                   data-target="#navbarSupportedContent"
                   aria-controls="navbarSupportedContent"
                   aria-expanded="false"
                   aria-label="Toggle navigation">
               <i class="fas fa fa-bars fa-2x"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <% if (context.authenticated) { %>
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-link">
                            <% if(administrativeActions.size() != 0 &&
                                  context.getAuthenticatedUser().hasRole("Administrator", false)) {%>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell text-white pl-2"></i>
                                    <span class="label label-danger text-white px-1 py-1">${administrativeActions.size()}</span>
                                </a>

                                <ul class="dropdown-menu">
                                    <h4 class="px-4 py-2 text-success">
                                        You have <% (administrativeActions.size() == 1) ? println("1 pending action") : println("${administrativeActions.size()} pending actions") %>
                                    </h4>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <% administrativeActions.each {%>
                                                <div class="col">
                                                    <li class="list-group-item py-2">
                                                        <a href="${it.getUrl()}" class="py-0">
                                                            <span class="text-primary">
                                                                <i class="fa fa-tasks"></i>&nbsp;${it.getUrl()}
                                                            </span>
                                                        </a>
                                                    </li>
                                                </div>
                                            <% } %>
                                        </ul>
                                    </li>
                                </ul>
                            <% } else if(alertNotifications.size() != 0) {%>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell text-white pl-2"></i>
                                    <span class="label label-danger text-white px-1 py-1">${alertNotifications.size()}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <h4 class="px-4 py-2 text-success">
                                        You have <% (alertNotifications.size() == 1) ? println("1 notification") : println("${alertNotifications.size()} notifications") %>
                                    </h4>
                                    <li>
                                        <ul class="list-group">
                                            <% alertNotifications.each {%>
                                                <div class="col">
                                                    <li class="list-group-item py-1">
                                                        <span class="text-primary">
                                                            <i class="fa fa-info-circle text-primary"></i>
                                                            <span class="text-muted">
                                                                <span class="text-danger">Message</span> ${it.getText()} sent on
                                                                <span class="text-danger">${it.getDateCreated().format("dd-MMM-yyyy")}</span> at
                                                                <span class="text-danger">${it.getDateCreated().format("HH:mm")}</span> by
                                                            </span> ${it.getCreator()}
                                                        </span>
                                                    </li>
                                                </div>
                                            <% } %>
                                        </ul>
                                    </li>
                                </ul>
                            <% } else { %>
                                <li class="nav-link">
                                    <i class="fa fa-bell text-white"></i>
                                    <span class="label label-danger text-white">0</span>
                                </li>
                            <% } %>
                        </li>
                        <li class="nav-item">
                           <a onclick="getWindowHash();return false;"
                              class="search-form-trigger btn btn-primary pl-2"
                              data-toggle="search-form">
                                 <i class="fa fa-search" aria-hidden="true"></i>
                           </a>
                        </li>
                        <li class="identifier">
                            <i class="icon-user small"></i>
                            ${context.authenticatedUser.username ?: context.authenticatedUser.systemId}
                            <% if (enableUserAccountExt) { %>
                            <i class="icon-caret-down appui-icon-caret-down link"></i>
                            <i class="icon-caret-up link appui-toggle" style="display: none;"></i>
                            <ul id="user-account-menu" class="appui-toggle">
                                <% userAccountMenuItems.each { menuItem -> %>
                                <li>
                                    <a id="" href="/${contextPath}/${menuItem.url}">
                                        ${ui.message(menuItem.label)}
                                    </a>
                                </li>
                                <% } %>
                            </ul>
                            <% } %>
                        </li>
                        <li class="change-location">
                            <a href="javascript:void(0);">
                                <i class="icon-map-marker small"></i>
                                <span id="selected-location" data-bind="text: text" location-uuid="${ servicePointVisited ? servicePointVisited.uuid : '' }"></span>
                                <% if (multipleServicePoints) { %>
                                <i class="icon-caret-down link"></i>
                                <% } %>
                            </a>
                        </li>
                        <li class="logout">
                            <a href="/${contextPath}/ms/logout">
                                 <i class="icon-signout small"></i>
                                ${ui.message("emr.logout")}
                            </a>
                        </li>
                    </ul>

                    <div id="session-location">
                        <div id="spinner" style="position:absolute; display:none">
                            <img src="${ui.resourceLink('uicommons', 'images/spinner.gif')}">
                        </div>
                        <ul class="select">
                            <%  servicePoints.sort { ui.format(it) }.each {
                                def selected = (it == servicePointVisited) ? "selected" : ""
                            %>
                            <li class="${selected}" locationId="${it.id}" locationName="${ui.format(it)}">${ui.format(it)}</li>
                            <% } %>
                        </ul>
                    </div>
                <% } %>
            </div>
        </div>
    </nav>
    <div id="search-form-wrapper" class="search-form-wrapper">
        <div class="row justify-content-center">
            <div class="col col-sm-12 col-md-10 col-lg-10">
                ${ ui.includeFragment("botswanaemr", "findPatient",[isRegistrationPage: isRegistrationPage, action: action, returnUrl: returnUrl])}
            </div>
        </div>
    </div>
</header>
<div class="modal fade" tabindex="-1"
     data-controls-modal="inactivityModal" data-backdrop="static"
     data-keyboard="false" role="dialog" aria-hidden="true"
     aria-labelledby="inactivityWarning" id="inactivityModal" style="display:none;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inactivityWarning">Inactivity Warning</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>You have been inactive for 5 minutes. You will be logged out in 2 minutes unless you confirm.</p>
                <button type="button" class="btn btn-success float-right" id="stay-logged-in">Stay Logged In</button>
                <button type="button" class="btn btn-danger float-right" id="log-out">Log Out</button>
            </div>
        </div>
    </div> 
</div>     