<!--
    This Source Code Form is subject to the terms of the Mozilla Public License,
    v. 2.0. If a copy of the MPL was not distributed with this file, You can
    obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
    the terms of the Healthcare Disclaimer located at http://openmrs.org/license.

    Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
    graphic logo is a trademark of OpenMRS Inc.
-->
<htmlform formUuid="62176b46-14f0-454c-9324-5eaa2d7a1435"
    formName="Partner characteristic and HIV Testing form"
    formEncounterType="662614c3-8c4f-4f6f-8759-190b24c86349"
    formVersion="1.0"
    formDescription="Partner characteristic and HIV Testing form"
    formAddMetadata="yes"
    formUILocation="patientDashboard.visitActions"
    formIcon="icon-file"
    formDisplayStyle="Standard"
    formLabel="Partner characteristic and HIV Testing form">

        <!--@formatter:off-->
        <script type="text/javascript">
            jq(document.body).ready(() => {

                const HIV_OFF_ART = '<lookup expression="fn.getConcept('214d3bb8-af42-4cb3-9784-ee5ebba6a4eb').id"/>';
                const HIV_ON_ART  = '<lookup expression="fn.getConcept('163561AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA').id"/>';

                jq("#lastSexDateContainer input[type='text']").datepicker({ dateFormat: 'mm/yy' });


                getField('no-testing-plan.value').change(function() {
                    if (getValue('no-testing-plan.value') !== '') {
                        getField("testing-plan.value").prop('disabled', true);
                    } else {
                        getField("testing-plan.value").prop('disabled', false);
                    }
                });

                getField('partnerStatus.value').change(function() {
                    let selectedValue = jq(this).val();

                    if (selectedValue == HIV_OFF_ART || selectedValue == HIV_ON_ART) {
                        jq('#partnerNotificationSection').hide();
                    } else {
                        jq('#partnerNotificationSection').show();
                    }
                });

                beforeSubmit.push(function() {
                    showLoadingOverlay();
                    return true;
                });

                // the contactRecordName from the browser url queryString
                let pnsContactName = urlUtils.urlParam('pnsContactName');

                if (pnsContactName !== null &amp;&amp; pnsContactName !== '') {
                    let partnerContactName = pnsContactName.replaceAll('__', ' ');
                    getField('pnsContactRecordName.value').val(partnerContactName);
                }
            });
        </script>
        <style>
            .form-section{
                display:grid;
                grid-template-columns: 1fr;
                grid-template-rows: 1fr 1fr 1fr
                width: 100%;
            }
            .dashed-button {
                margin-bottom: 1rem;
                min-width: 100%;
                border-style: dashed;
                background: transparent;
            }
            .text-separator {
                margin-top: auto;
                margin-bottom: auto;
                margin-right: 1rem;
                margin-left: 1rem;
            }
            .required {
                color: #fff;
                background-color: #fff;
            }
        </style>
    <!--@formatter:on-->
    <uiInclude provider="botswanaemr" javascript="botswanaemr-common.js"/>
    <div class="row hidden">
        <encounterProvider id="tbSelectCurrentUser" default="currentUser" />
        <encounterDate showTime="true" default="now" />
        <encounterLocation default="SessionAttribute:emrContext.sessionLocationId" />
        <!-- This is the Obs Name for the contact being screened -->
        <obs conceptId="5ac051bb-b94c-497f-9c08-cc28c0e6369b" id="pnsContactRecordName"/>
    </div>
    <div class="content-section">
        <div class="container-fluid center-content-container">

            <fieldset class="form-section">
                <div class="form-group col text-primary">
                    <div class="col">
                        <label>PARTNER CHARACTERISTICS</label>
                    </div>
                    <div class="border-top border-primary my-3"></div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Date of last sex(MM/YYYY):
                                <span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="col-md-8" id="lastSexDateContainer">
                            <obs required="true" conceptId="577e7811-f87c-411b-b25f-c389ef41991c"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="col">
                            <label for="tested-client">Age:</label>
                        </div>
                        <div class="col-md-8">
                            <obs required="true" id="tested-client"
                                conceptId="167172AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
                            </obs>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Sex:
                                <span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="col">
                            <obs class="form-inline" required="true"
                                conceptId="787c9023-a485-401e-874b-463a8640bf8d"
                                answerConceptIds="1534AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, 1535AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, 166415AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                answerLabels="Male, Female, Trans"  />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Living together:
                                <span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="col">
                            <obs class="form-inline" required="true"
                                conceptId="b8d424a5-1985-4ff0-9509-80e460e7944d"
                                answerConceptIds="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, 1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                answerLabels="Yes, No"  />
                        </div>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-section">
                <div class="form-group col text-primary">
                    <div class="col">
                        <label>PARTNER CONTACT INFORMATION</label>
                    </div>
                    <div class="border-top border-primary my-3"></div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label for="tested-client">Contact phone:</label>
                        </div>
                        <div class="col-md-8">
                            <obs id="tested-client" conceptId="159635AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
                            </obs>
                        </div>
                    </div>
                    <div class="col">
                        <div class="col">
                            <label for="tested-client">Address:</label>
                        </div>
                        <div class="col-md-8">
                            <obs id="tested-client"
                                conceptId="162725AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
                            </obs>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Place of employment:</label>
                        </div>
                        <div class="col">
                            <obs class="form-inline"
                                conceptId="ec66fad3-4f0e-4d7c-a617-79e2484d86d6" />
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="form-section">
                <div class="form-group col text-primary">
                    <div class="col">
                        <label>PARTNER TESTING AND HIV STATUS</label>
                    </div>
                    <div class="border-top border-primary my-3"></div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Partner testing and HIV status:
                                <span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="col" id="partnerStatusSection">
                            <obs required="true" id="partnerStatus"
                                conceptId="9845d27a-d398-4bc1-9e9b-027e2baa313c"
                                answerConceptIds="664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, 214d3bb8-af42-4cb3-9784-ee5ebba6a4eb, 163561AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, dd4dff2a-5776-4b7e-b7b9-e200fee98814, 1067AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                answerLabels="Recently tested HIV- ,Known prior HIV+ off ART ,Known HIV+ on ART, Never tested,Unknown">
                                <controls>
                                    <when value="163561AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" thenDisplay="#follow-up-question,#artFacility"/>
                                </controls>
                            </obs>
                        </div>
                    </div>
                    <div class="col" id="follow-up-question">
                        <div class="col">
                            <label>Unique ART No:</label>
                        </div>
                        <div class="col">
                            <obs class="form-inline"
                                conceptId="2f8df9bf-610a-4fe1-a399-35d1c6bebb89"/>
                        </div>
                    </div>
                </div>
                <div class="form-group row" id="artFacility">
                    <div class="col">
                        <div class="col">
                            <label>Facility receiving ART(if on ART)</label>
                        </div>
                        <div class="col">
                            <obs
                                conceptId="162724AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" />
                        </div>
                    </div>
                </div>
            </fieldset>


            <fieldset class="form-section" id="partnerNotificationSection">
                <div class="form-group col text-primary">
                    <div class="col">
                        <label>Partner Notification and Testing Plan</label>
                    </div>
                    <div class="border-top border-primary my-3"></div>
                </div>
                <div class="form-group row">
                    <div class="col">
                        <div class="col">
                            <label>Testing plan:
                                <span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="col">
                            <obs id="testing-plan"
                                 conceptId="29e12d1a-a8cc-41a5-945f-67c61899eaf5"
                                 answerConceptIds="a99af370-b975-4604-9ac3-2b4f3e2f6c7e,3cec1bec-b663-4b72-9c87-afc5008cf82b,4d0819df-1f99-4a60-bec7-9e24ab4cf609,5149b16c-74ff-4cf1-bde8-0fc1d21d1360,0f5e752a-1d9c-40d9-877c-53c78e56b576,2f6d0e8e-2b11-4244-94f5-311fa911fb3a,c809667c-e690-4c29-a0d2-48e7c566746d,161557AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,728afc1b-f5bf-44c3-9daa-668b34e4c52d"
                                 answerLabels="Patient will give a referral slip to the partner,Patient will bring partner to clinic,Patient will bring partner for couples Counselling and testing,Patient and HCW will call partner together,Patient will tell partner but wants HCW to then call the partner to arrange testing,HCW will notify partner by phone,Refer to community testing,Give patient HIV self-test,No testing plan">
                                <controls>
                                    <when value="728afc1b-f5bf-44c3-9daa-668b34e4c52d" thenDisplay="#no-testing-plan"/>
                                </controls>
                            </obs>
                        </div>
                    </div>
                    <div class="col" id="no-testing-plan">
                        <div class="col">
                            <label>Reasons</label>
                        </div>
                        <div class="col">
                            <obs class="form-inline" id="no-testing-plan"
                                 conceptId="728afc1b-f5bf-44c3-9daa-668b34e4c52d"
                                 answerConceptIds="159AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,5566AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,4d18d47c-0abe-46d6-a1aa-60cd3ebf63a7,fd704ab8-8fca-4623-8a04-6aacc86e4a53,664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                 answerLabels="Partner deceased,Partner known HIV+,No partner contact information,Fear of violence,Recently tested HIV-,Other (Specify)"
                            ><controls>
                                <when value="5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" thenDisplay="#no-testing-other"/>
                                <when value="fd704ab8-8fca-4623-8a04-6aacc86e4a53" thenDisplay="#refer-social-worker"/>
                            </controls> </obs>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col" id="no-testing-other">
                        <div class="col">
                            <label>Specify</label>
                        </div>
                        <div class="col">
                            <obs
                                conceptId="94775d6e-b4e5-4f6d-bbdd-f8d4b71305b0" />
                        </div>
                    </div>
                    <div class="col" id="refer-social-worker">
                        <div class="col">
                            <label>Have they been referred  to social work services?</label>
                        </div>
                        <div class="col">
                            <obs class="form-inline"
                                 conceptId="020886b6-be61-49ea-9c5d-e59d34ae1041"
                                 answerConceptIds="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, 1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                 answerLabels="Yes, No"  />
                        </div>
                    </div>
                </div>
            </fieldset>


            <div class="row actions mt-5 mb-3 px-0">
                <div class="col-12 px-0">
                    <button type="button" id="cancel" class="btn btn-sm btn-dark bg-dark float-right ml-1">
                        Close
                    </button>
                    <button type="submit" id="submit" class="btn btn-sm btn-primary float-right mr-0">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <redirectOnSave url="/botswanaemr/hts/pnsClientProfile.page?patientId={{patient.id}}"/>
</htmlform>
