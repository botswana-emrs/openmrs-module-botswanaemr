<!--
    This Source Code Form is subject to the terms of the Mozilla Public License,
    v. 2.0. If a copy of the MPL was not distributed with this file, You can
    obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
    the terms of the Healthcare Disclaimer located at http://openmrs.org/license.

    Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
    graphic logo is a trademark of OpenMRS Inc.
-->
<htmlform formUuid="a6072c4a-cd6a-11ed-afa1-0242ac120002"
          formName="Antenatal Enrolment form"
          formEncounterType="c1a4cb24-cd6a-11ed-afa1-0242ac120002"
          formVersion="1.0"
          formDescription="Antenatal Enrolment form"
          formAddMetadata="yes"
          formUILocation="patientDashboard.visitActions"
          formIcon="icon-file"
          formDisplayStyle="Standard"
          formLabel="Antenatal Enrolment form">

    <!--@formatter:off-->
    <script type="text/javascript">
        jq(document.body).ready(() => {
            jq("#labour-duration-hours").attr("size", "10");
            jq("#labour-duration-minutes").attr("size", "10");
            jq("#para").on( "change", function() {
                if (getValue('para.value') > 0 ) {
                    jq("#multipleBirthType").show();
                } else {
                    jq("#multipleBirthType").hide();
                }
            });
            let firstRow = jq('table.multi tbody tr:first-of-type');
            let otherRows = jq('table.multi tbody tr:not(:first-of-type)');

            let notEmptyRows = otherRows.filter(function(idx, element) {
                return jq(element).find('td input').val() != '';
            })
            <ifMode mode="ENTER">
                otherRows.hide(); //Hide all rows apart from first row if adding new form
            </ifMode>
            <ifMode mode="EDIT">
                otherRows.hide(); //Hide all rows apart from first row if adding new form
                notEmptyRows.show();
            </ifMode>

            function addMore(targetId) {
                let table = jq("#" + targetId).parent();

                let thisRow = table.find('tr.main:visible:last');
                let nextRow = table.find('tr.main:hidden:first');

                if (nextRow.length &gt; 0) {
                    nextRow.show('slow');
                }

            }

            jq('.add-more').click(function (e) {
                e.preventDefault();
                addMore(this.id);
            });

            jq('.remove').click(function (e) {
                let closestTr = jq(this).closest('tr');
                closestTr.hide('slow');
                fieldHelper.clearAllFields(closestTr);
            });

            jq('#cancel-form').click(function (e) {
                if (window.parent) {
                    // Trigger the click event on the parent button
                    jq(window.parent.document).find('#close-button').click();
                }
            });

            beforeSubmit.push(function () {
                if ((getValue('gravida.value') == '')) {
                    getField('gravida.error').html('<p class="text-danger">Gravida value should not be empty.</p>').show();
                    jq().toastmessage('showErrorToast', 'The Gravida value should not be empty.');
                    return false;
                }
                var calculatedGravidaVal = 0;
                calculatedGravidaVal += parseInt(getValue('para.value'));
                calculatedGravidaVal += parseInt(getValue('ab.value'));
                calculatedGravidaVal += parseInt(getValue('ectopic.value'));
                if (getValue('gravida.value') > 1 &amp;&amp; getValue('gravida.value') == calculatedGravidaVal) {
                    getField('gravida.error').html('<p class="text-danger">Gravida value should be 1 or should not equal to the sum of parity, ab and ectopic values.</p>').show();
                    jq().toastmessage('showErrorToast', 'Gravida value should be 1 or should not equal to the sum of parity, ab and ectopic values.');
                    return false;
                }
                let requiredFields = jq('span.required').parent().nextAll('div').find('select,input:text');
                requiredFields.push(jq('span.required').siblings().children('select,input:text'));
                let emptyRequiredFields = requiredFields.filter(function () {
                    return jq(this).val() === '';
                });
                if (emptyRequiredFields.length > 0) {
                    jq().toastmessage('showErrorToast', 'Some required fields have missing values');
                    return false;
                }
                let errorFields = jq('span.field-error:visible');
                if (errorFields.length > 0) {
                    jq().toastmessage('showErrorToast', 'Some fields have validation errors');
                    return false;
                }
                showLoadingOverlay();
                return true;
            });
        
            fetchFacilities();

            function fetchFacilities() {
                let parameter = {};
                jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchFacilities.action', parameter)
                .done(function(data) {
                    if (data.status === 'success') {
                        if (data.locations) {
                            var facilities = data.locations;
                            getField('facility.value').autocomplete({
                                source: facilities,
                                change: function(event, ui) {
                                    if (!ui.item) {
                                    jq(".concept").val("");
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    </script>
    <style>
        .form-section{
            display:grid;
            grid-template-columns: 1fr;
            grid-template-rows: 1fr 1fr 1fr
            width: 100%;
        }

        .dashed-button {
            margin-bottom: 1rem;
            min-width: 100%;
            border-style: dashed;
            background: transparent;
        }

        .text-separator {
            margin-top: auto;
            margin-bottom: auto;
            margin-right: 1rem;
            margin-left: 1rem;
        }

        input[type="number"] {
            width: 25px !important;
            min-width: 25px !important;
        }
        select {
            min-width: 25px !important;
        }
        select:focus {
            min-width: 25px;
            width: auto;
        }
        .pl-10,
        .px-10 {
            padding-left: 4rem !important;
        }
    </style>
    <!--@formatter:on-->
    <uiInclude provider="botswanaemr" javascript="botswanaemr-common.js"/>
    <div class="row hidden">
        <encounterProvider id="tbSelectCurrentUser" default="currentUser"/>
        <encounterDate showTime="true" default="now"/>
        <encounterLocation default="SessionAttribute:emrContext.sessionLocationId"/>
    </div>
    <div class="content-section">
        <div class="container-fluid">
            <fieldset class="form-section">
                <div class="row pl-0 pr-0">
                    <div class="col-12 pl-0 pr-0 text-primary">
                        <div class="col pl-0">
                            <h5 class="text-primary float-left">ANTENATAL FORM</h5>
                            <button type="button" id="clientBtn" class="btn btn-primary float-right mr-0"
                                    onclick="redirectToClientProfilePage()">Client Profile
                            </button>
                            <script type="text/javascript">
                                let redirectToClientProfilePage = () => {
                                    window.location = "/" + OPENMRS_CONTEXT_PATH +
                                    "/botswanaemr/srh/srhClientProfile.page?patientId=<lookup expression="patient.patientId"/>";
                                };
                            </script>
                        </div>
                    </div>
                    <div class="col-12 pl-0 pr-0">
                        <div class="border-top border-3 my-3">
                        </div>
                    </div>
                </div>
                <div class="col-12 pl-0 pr-0 mt-5">
                    <h5 class="text-primary float-left mt-0 mb-3">PAST OBSTESTRIC HISTORY</h5>
                    <hr class="divider pt-1 bg-primary mt-4"/>
                </div>
                <div class="row">
                    <div class="col pl-0 pr-0">
                        <div class="form-group">
                            <label>Gravida :
                                <span class="required">*</span>
                            </label>
                            <obs id="gravida" conceptId="5624AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Para :
                                <span class="required">*</span>
                            </label>
                            <obs id="para" 
                                 conceptId="1053AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Ab :
                                <span class="required">*</span>
                            </label>
                            <obs id="ab" conceptId="1823AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label>Ectopics :
                                <span class="required">*</span>
                            </label>
                            <obs id="ectopic" conceptId="4af1d038-05e7-4972-ba0f-bf4dce6b0582"/>
                        </div>
                    </div>
                </div>
                <div id="multipleBirthType" class="row my-3" style="display: none;">
                    <div class="col pl-0 pr-0">
                        <div class="form-group">
                            <label for="typeOfMultipleBirth">Type of multiple birth</label>
                            <obs id="typeOfMultipleBirth"
                                 conceptId="a37ea16f-6c91-4be7-8c61-d63abf8bc550"
                                 answerConceptIds="151089AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,159915AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,529dc8db-f700-4c96-b052-c35920071e8a"
                                 answerLabels="Twin,Triplet,Quadraple">
                            </obs>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive" style="margin-top:10px">
                    <table class="table table-striped multi table-borderless" id="hivTestResults">
                        <thead>
                            <th scope="col">Date</th>
                            <th scope="col">Place</th>
                            <th scope="col">Gestation (WKS)</th>
                            <th scope="col">Duration of Labour (hrs|mins)</th>
                            <th scope="col">Method of Delivery</th>
                            <th scope="col">Fetal Outcome</th>
                            <th scope="col">Sex</th>
                            <th scope="col">Bwt(g)</th>
                            <th scope="col">Maternal Complications</th>
                            <th scope="col">Fetal Complications</th>
                        </thead>
                        <tbody>
                            <repeat>
                                <template>

                                    <tr class="main">
                                        <td>
                                            <obs id="date" conceptId="ccef7286-fec7-4a39-98e8-4e6da674128c"
                                                 allowFutureDates="false"/>
                                        </td>
                                        <td>
                                            <obs id="place"
                                                 conceptId="4cd09f72-a3f8-49b6-b1e9-91ec576ef485"/>
                                        </td>
                                        <td>
                                            <obs id="gestation"
                                                 conceptId="1438AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        </td>
                                        <td class="duration">
                                            <div class="row">
                                                <div class="form-inline">
                                                    <div class="col-2 pl-0">
                                                        <obs id="labour-duration-hours"
                                                             conceptId="c29be4f9-ea65-4a12-995b-35fda06a2a1f"/>
                                                    </div>
                                                    <div class="col-4"> </div>
                                                    <div class="col-2">
                                                        <obs id="labour-duration-minutes"
                                                             conceptId="c29be4f9-ea65-4a12-995b-35fda06a2a1f"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="pl-10">
                                            <obs id="delivery-method"
                                                 conceptId="5630AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        </td>
                                        <td>
                                            <obs id="fatal-outcome"
                                                 conceptId="159917AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                 answerConceptIds="151849AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,159916AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,135436AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                 answerLabels="Live birth, 	Fresh stillbirth, Macerated birth"/>
                                        </td>
                                        <td>
                                            <obs class="form-inline"
                                                 conceptId="1533AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                 answerConceptIds="1535AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,1534AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                 answerLabels="Male, Female"/>
                                        </td>
                                        <td>
                                            <obs id="bwt"
                                                 conceptId="af47037e-f3d8-4e2d-be5c-c13c4e42d99b"/>
                                        </td>
                                        <td>
                                            <obs id="maternalC"
                                                 conceptId="c73a9636-5b4a-4f94-a3c5-efe9682cb4e1"/>
                                        </td>
                                        <td>
                                            <obs id="fetalC"
                                                 conceptId="71ec4c2a-6cbe-493d-b43e-305c06714f63"/>
                                        </td>
                                    </tr>

                                </template>
                                <render id="1"/>
                                <render id="2"/>
                                <render id="3"/>
                                <render id="4"/>
                            </repeat>
                        </tbody>
                    </table>
                    <button class="dashed-button rounded add-more"
                            id="btnAdd1">+ Add
                    </button>
                </div>
            </fieldset>
            <fieldset class="form-section">
                <div class="col-12 pl-0 pr-0">
                    <h5 class="text-primary float-left mt-0 mb-3">ADMISSIONS DURING PREGNANCY</h5>
                    <hr class="divider pt-1 bg-primary mt-4"/>
                </div>
                <div class="card-body table-responsive" style="margin-top:10px">
                    <table class="table table-striped multi table-borderless" id="hivTestResults">
                        <thead>
                            <th scope="col">Health Facility</th>
                            <th scope="col">Date of Admission</th>
                            <th scope="col">Date of Discharge</th>
                            <th scope="col">Hospital Number</th>
                            <th scope="col">Diagnosis</th>
                            <th scope="col">Treatment at</th>
                            <th scope="col">Follow up at</th>
                        </thead>
                        <tbody>
                            <repeat>
                                <template>

                                    <tr class="main">
                                        <td>
                                            <obs id="facility" class="facility-name" conceptId="162724AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                        </td>
                                        <td>
                                            <obs id="admission-date" conceptId="ecb39085-d350-4bea-af1d-433520402fb5"
                                                 allowFutureDates="false"/>
                                        </td>
                                        <td>
                                            <obs id="discharge-date" conceptId="421756e0-4a36-4a11-b490-38d320a50e22"
                                                 allowFutureDates="true"/>
                                        </td>
                                        <td>
                                            <obs id="hospital-number" conceptId="89367b5f-b73a-4b84-95b5-2b4b627ad1b6"/>
                                        </td>
                                        <td>
                                            <obs id="diagnosis" conceptId="bbfe0413-63b7-49ed-812d-d92a1ca57bae"/>
                                        </td>
                                        <td>
                                            <obs id="treatment" conceptId="83785a9a-576b-4639-b9e9-0faeee96398b"/>
                                        </td>
                                        <td>
                                            <obs id="follow-up" conceptId="e6b0d88c-c3a4-49f8-aece-9fa7b28bdf87"/>
                                        </td>
                                    </tr>

                                </template>
                                <render id="1"/>
                                <render id="2"/>
                                <render id="3"/>
                                <render id="4"/>
                            </repeat>
                        </tbody>
                    </table>
                    <button class="dashed-button rounded add-more"
                            id="btnAdd2">+ Add
                    </button>
                </div>
            </fieldset>
            <div class="row actions mt-5 mb-3 px-0">
                <div class="col-12 px-0">
                    <button type="button" id="cancel-form" class="btn btn-sm btn-dark bg-dark float-right ml-1">
                        Close
                    </button>
                    <button type="submit" id="submit" class="btn btn-sm btn-primary float-right mr-0">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <redirectOnSave url="/botswanaemr/srh/obstetricProfile.page?patientId={{patient.id}}"/>
</htmlform>
