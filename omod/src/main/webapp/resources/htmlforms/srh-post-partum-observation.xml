<!--
    This Source Code Form is subject to the terms of the Mozilla Public License,
    v. 2.0. If a copy of the MPL was not distributed with this file, You can
    obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
    the terms of the Healthcare Disclaimer located at http://openmrs.org/license.

    Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
    graphic logo is a trademark of OpenMRS Inc.
-->
<htmlform
        formUuid="d44bbc94-e8cc-40d1-948e-8cd8cbddeedb"
        formName="Postpartum Observation Form"
        formEncounterType="86e70c36-25f8-411f-a5aa-1ab4d5734740"
        formVersion="1.0"
        formDescription="Postpartum Observation Form"
        formAddMetadata="yes"
        formUILocation="patientDashboard.visitActions"
        formIcon="icon-file"
        formDisplayStyle="Standard"
        formLabel="Postpartum Observation  Form">
    <div style="display:none">
        Enrollment date:
        <encounterDate showTime="true" default="now"/>
        Enrolled by:
        <encounterProvider default="currentUser"/>
        Enrolled at:
        <encounterLocation default="SessionAttribute:emrContext.sessionLocationId"/>
    </div>

    <script type="text/javascript">
        jq(document).ready(function () {
        let firstRow = jq('table.multi tbody tr:first-of-type');
        let otherRows = jq('table.multi tbody tr:not(:first-of-type)');
        let notEmptyRows = otherRows.filter(function(idx, element) {
        return jq(element).find('td input').val() != '';
        })
        <ifMode mode="ENTER">
            otherRows.hide(); //Hide all rows apart from first row if adding new form
        </ifMode>

        <ifMode mode="EDIT">
            otherRows.hide(); //Hide all rows apart from first row if adding new form
            notEmptyRows.show();
        </ifMode>

        const LOW_APICAL_PULSE = "Apical pulse is too low.";
        const NORMAL_APICAL_PULSE = "Apical pulse is normal.";
        const HIGH_APICAL_PULSE = "Apical pulse is too high.";

        jq("#treatment-provided-section").hide();

        function addMore(targetId) {
        let table = jq("#" + targetId).parent();

        let thisRow = table.find('tr.main:visible:last');
        let nextRow = table.find('tr.main:hidden:first');

        if (nextRow.length &gt; 0) {
        nextRow.show('slow');
        }
        };

        jq('.add-more').click(function (e) {
        e.preventDefault();
        addMore(this.id)

        });

        jq('.apicalPulseSection').change(function () {
            let enteredValue = jq(this).find('input').val();
            let apicalPulseMessage = jq(this).find('.apicalPulseMessage');

            if (enteredValue &lt; 120) {
                apicalPulseMessage.text(LOW_APICAL_PULSE);
            } else if (enteredValue > 150) {
                apicalPulseMessage.text(HIGH_APICAL_PULSE);
            } else {
                apicalPulseMessage.text(NORMAL_APICAL_PULSE);
            }
            checkAllApicalPulseSections();
        });

        function checkAllApicalPulseSections() {
            let allNormal = false;
            jq('.apicalPulseMessage').each(function() {
                let messageText = jq.trim(jq(this).text());
                if (messageText === NORMAL_APICAL_PULSE) {
                    allNormal = true;
                    return false; // exit the loop
                }
            });

            if (allNormal) {
                jq("#treatment-provided-section").show();
            } else {
                jq("#treatment-provided-section").hide();
            }
        }


        jq('.remove').click(function (e) {
        let closestTr = jq(this).closest('tr');
        closestTr.hide('slow');
        fieldHelper.clearAllFields(closestTr);

        });

        });

        jq('.autoCompleteText').on('keyup', function() {
        onBlurAutocomplete(this)
        jq(this).siblings('.error').hide();
        if (jq(this).siblings('.autoCompleteHidden')[0].value == 'ERROR') {
        jq(this).siblings('.error').html('Select valid value').show();
        }
        });

    </script>
    <uiInclude provider="botswanaemr" javascript="hiv-screening.js"/>
    <uiInclude provider="botswanaemr" javascript="botswanaemr-common.js"/>
    <uiInclude provider="botswanaemr" javascript="srh-forms.js"/>

    <style>
        .content-section {
        width: 100% !important;
        }

        input[type="radio"] {
        width: 1em;
        height: 1em;
        margin-top: 0.25em;
        vertical-align: top;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid rgba(0,0,0,.25);
        border-radius: 50%;
        }

        input[type="radio"] + label {
        margin-top: 0px;
        }

        input[type="checkbox"] {
        width: 1em;
        height: 1em;
        margin-top: 0.25em;
        vertical-align: top;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid rgba(0,0,0,.25);
        border-radius: 50%;
        }

        input[type="checkbox"] + label {
        margin-top: 0px;
        }

        form fieldset, .form fieldset {
        min-width: 95% !important;
        }

        span.required {
        position: absolute;
        left: 0;
        top: 25%;
        }

        form select {
        min-width: 100%;
        }

        .title-section {
        margin-top: 2rem;
        }

        .inline-section {
        display: inline-flex;
        align-items: center;
        }

        .inline-section > label {
        margin-right: 1.5rem;
        }

        .top-space {
        margin-top: 10px;
        }

        .fill-secion {
        min-width: 100% !important;
        }

        .fill-secion > input {
        min-width: 100% !important;
        }

        .sub-element {
        margin-left: 0;
        width: 90% !important
        }

        .content-left {
        justify-content: left !important;
        }

        .table {
        overflow-x: scroll;
        display: inline-block;
        }

    </style>
    <div class="card mt-4">
        <div class="content-section">
            <div class="container-fluid">
                <div class="row">
                    <h5 class="pl-0">Postpartum Observation Form</h5>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="selectedCurrentUser">Provider Name:</label>
                            <encounterProvider required="true" id="selectedCurrentUser" type="autocomplete" default="currentUser"/>
                        </div>
                    </div>
                </div>

                <div class="title-section">
                    <div class="row">
                        <h6 class="pl-0 text-primary">MOTHER</h6>
                    </div>
                    <hr class="separator-light"/>
                </div>

                <div class="form-section">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="col-12">
                                <div style="margin-top:10px">
                                    <table class="table multi table-borderless"
                                           id="motherSectionSection">
                                        <thead>
                                            <th class="text-center" scope="col">DATE</th>
                                            <th class="text-center" scope="col">TIME</th>
                                            <th class="text-center" scope="col">T</th>
                                            <th class="text-center" scope="col">R</th>
                                            <th class="text-center" scope="col">P</th>
                                            <th class="text-center" scope="col">BP</th>
                                            <th class="text-center" scope="col">VAGINAL BLEEDING</th>
                                            <th class="text-center" scope="col">POSITION OF THE UTERUS</th>
                                            <th class="text-center" scope="col">CONSISTENCY OF THE UTERUS</th>
                                            <th class="text-center" scope="col">HEIGHT OF FUNDUS(cm)</th>
                                            <th class="text-center" cope="col">URINE OUTPUT(ml)</th>
                                            <th class="text-center" scope="col">COMMENTS</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <repeat>
                                                <template>
                                                    <tr class="main" id="{n}">
                                                        <obsgroup
                                                                groupingConceptId="476b4258-2e61-476b-b8a6-3992dcacd9aa">
                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="d59cffd1-eb90-4e76-87ed-e04672ecea0e"
                                                                         allowTime="false"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row timeOnly">
                                                                    <obs conceptId="3fd05451-ea4d-41cc-b26c-9a4c96add027"
                                                                         allowTime="true"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         style="text"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="5242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         style="text"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="5087AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         style="text"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row col-12">
                                                                    <div class="col form-inline">
                                                                        <div class="col px-0">
                                                                            <obs id="systolic"
                                                                                 conceptId="5085AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                        </div>
                                                                        <div class="text-center text-separator text-primary text-bolder px-4">
                                                                            /
                                                                        </div>
                                                                        <div class="col px-0">
                                                                            <obs id="diastolic"
                                                                                 conceptId="5086AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="147232AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         answerConceptIds="1498AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,1499AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,1500AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         answerLabels="Mild,Moderate,Severe"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="99e20be2-7d10-4ccf-b145-8b4c2d0eaf45"
                                                                         answerConceptIds="f6072ae4-8c7b-45b0-830e-c3f4045743b5,72dbf326-de82-484a-a413-36b2cee806c8"
                                                                         answerLabels="Mid-length,lateral"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="203280bb-0dc5-4b90-bc08-f094c6df1bf1"
                                                                         answerConceptIds="f63ab0a0-1b53-48a4-9632-db2169e8d26a,4b6a7699-302e-4ef7-b1bc-ae0d02b9dd6e,1f97f36a-d090-4aeb-8548-be70b3a5bb11"
                                                                         answerLabels="Bulky Uterus,Firm uterus,Well contracted"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="c93f1fec-c0dd-4197-b8c2-947f2e97c75b"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="161929AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="60e7c4d7-2e64-40f7-9f75-b26c1c94d9ea"
                                                                         style="textarea"/>
                                                                </div>
                                                            </td>

                                                            <td class="col-md-3">
                                                                <div class="row">
                                                                    <div class="col-md-12 remove">
                                                                        <a>Delete</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </obsgroup>
                                                    </tr>
                                                </template>
                                                <render n="1"/>
                                                <render n="2"/>
                                                <render n="3"/>
                                                <render n="4"/>
                                                <render n="5"/>
                                                <render n="6"/>
                                                <render n="7"/>
                                                <render n="8"/>
                                                <render n="9"/>
                                                <render n="10"/>
                                            </repeat>
                                        </tbody>
                                    </table>
                                    <button class="dashed-button rounded add-more"
                                            id="btnMotherSection">+ Add
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="title-section" style="margin-top: 5rem">
                        <div class="row">
                            <h6 class="pl-0 text-primary">BABY</h6>
                        </div>
                        <hr class="separator-light"/>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="col-12">
                                <div style="margin-top:10px">
                                    <table class="table multi table-borderless"
                                           id="babySection">
                                        <thead>
                                            <th class="text-center" scope="col">DATE</th>
                                            <th class="text-center" scope="col">TIME</th>
                                            <th class="text-center" scope="col">APICAL PULSE</th>
                                            <th class="text-center" scope="col">R</th>
                                            <th class="text-center" scope="col">T</th>
                                            <th class="text-center" scope="col">CORD</th>
                                            <th class="text-center" scope="col">FEEDING</th>
                                            <th class="text-center" scope="col">ELIMINATION</th>
                                            <th class="text-center" scope="col">COMMENTS</th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <repeat>
                                                <template>
                                                    <tr class="main" id="{n}">
                                                        <obsgroup
                                                                groupingConceptId="c8c504db-075a-405f-b9e9-271a0d78af2e">
                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="d59cffd1-eb90-4e76-87ed-e04672ecea0e"
                                                                         allowTime="false"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row timeOnly">
                                                                    <obs conceptId="3fd05451-ea4d-41cc-b26c-9a4c96add027"
                                                                         allowTime="true"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row apicalPulseSection">
                                                                    <obs id="apicalPulse" conceptId="d458507f-2517-4338-a6c7-6b78b1511ecc"/>
                                                                    <label class="form-text text-danger apicalPulseMessage"></label>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="5242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         style="text"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs conceptId="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         style="text"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs id="cord" conceptId="1dedebef-e9be-461c-bb2e-a99cd1588f7b"
                                                                         answerConceptIds="147241AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,f1655825-1401-4e54-9f3f-6357d0ea8fb6"
                                                                         answerLabels="Bleeding,In place"
                                                                         />
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs id="feeding" conceptId="b1efa920-35d4-4f3c-ba81-357bd0b5819f"
                                                                         answerConceptIds="1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                         answerLabels="Yes, No"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs id="elimination" conceptId="aa6a0076-0f88-4375-b35f-99251f42f49e"
                                                                         style="textarea"/>
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="row">
                                                                    <obs id="comments" conceptId="60e7c4d7-2e64-40f7-9f75-b26c1c94d9ea"
                                                                         style="textarea"/>
                                                                </div>
                                                            </td>

                                                            <td class="col-md-3">
                                                                <div class="row">
                                                                    <div class="col-md-12 remove">
                                                                        <a>Delete</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </obsgroup>
                                                    </tr>
                                                </template>
                                                <render n="1"/>
                                                <render n="2"/>
                                                <render n="3"/>
                                                <render n="4"/>
                                                <render n="5"/>
                                                <render n="6"/>
                                                <render n="7"/>
                                                <render n="8"/>
                                                <render n="9"/>
                                                <render n="10"/>
                                            </repeat>
                                        </tbody>
                                    </table>
                                    <button class="dashed-button rounded add-more"
                                            id="btnBabySection">+ Add
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row" id="treatment-provided-section">
                        <div class="row m-t-10" id="medication">
                            <div class="col-12">
                                <label>Prescribe Medication</label>
                            </div>
                            <div class="col-12 ">
                                <div class="card" style="margin-top:10px">
                                    <div class="card-body">
                                        <table class="table table-striped multi table-borderless" id="tbPatientMedication">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Medication</th>
                                                    <th scope="col">Dosage</th>
                                                    <th scope="col">Unit</th>
                                                    <th scope="col">Route</th>
                                                    <th scope="col">Notes</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <repeat>
                                                    <template>
                                                        <tr class="main">
                                                            <obsgroup
                                                                    groupingConceptId="163711AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA">
                                                                <td>
                                                                    <div class="row">
                                                                        <obs conceptId="1282AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                             answerDrugs="true"/>
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <div class="row">
                                                                        <obs conceptId="160856AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <div class="row">
                                                                        <obs conceptId="1519AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                             answerConceptSetIds="162384AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <div class="row">
                                                                        <obs conceptId="c7e89cec-973f-47b7-9b87-09d718351968"
                                                                             answerConceptIds="160240AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,160242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,160243AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,160245AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,160241AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,160241AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,161253AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162391AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162392AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162393AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162824AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162385AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162386AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162387AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162388AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162389AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,162390AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
                                                                        />
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <div class="row" id="medicationNotes">
                                                                        <obs conceptId="165095AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                                                    </div>
                                                                </td>

                                                                <td>
                                                                    <div class="row">
                                                                        <div class="remove">
                                                                            <a>Delete</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </obsgroup>
                                                        </tr>
                                                    </template>
                                                    <render n="1"/>
                                                    <render n="2"/>
                                                    <render n="3"/>
                                                    <render n="4"/>
                                                    <render n="5"/>
                                                    <render n="6"/>
                                                </repeat>
                                            </tbody>
                                        </table>
                                        <button class="dashed-button rounded add-more"
                                                id="btnAddMedication">+ Add
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row actions mt-5 mb-3">
                <div class="col-sm-6">
                    <button type="button" id="cancel" class="btn btn-sm cancel right">
                        CANCEL
                    </button>
                </div>
                <div class="col-sm-6">
                    <button type="submit" id="submit" class="btn btn-sm confirm left">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
    <redirectOnSave url="/botswanaemr/srh/obstetricProfile.page?patientId={{patient.id}}"/>
</htmlform>
