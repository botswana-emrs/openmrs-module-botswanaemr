<!--
    This Source Code Form is subject to the terms of the Mozilla Public License,
    v. 2.0. If a copy of the MPL was not distributed with this file, You can
    obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
    the terms of the Healthcare Disclaimer located at http://openmrs.org/license.

    Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
    graphic logo is a trademark of OpenMRS Inc.
-->
<htmlform formUuid="7db944e0-f90c-40fc-adf0-979eb0ee6725"
          formName="Partograph and Other Observations Form"
          formEncounterType="025d3595-c93b-43dc-8796-177e1b00fb87"
          formVersion="1.0"
          formDescription="Partograph and Other Observations Form"
          formAddMetadata="yes"
          formUILocation="patientDashboard.visitActions"
          formIcon="icon-file"
          formDisplayStyle="Standard"
          formLabel="Partograph and Other Observations">

    <div style="display:none">
        Enrollment date:
        <encounterDate showTime="true" default="now"/>
        Enrolled by:
        <encounterProvider default="currentUser"/>
        Enrolled at:
        <encounterLocation default="SessionAttribute:emrContext.sessionLocationId"/>
    </div>

    <script type="text/javascript">
        jq(document).ready(function () {
            let firstRow = jq('table.multi tbody tr:first-of-type');
            let otherRows = jq('table.multi tbody tr:not(:first-of-type)');
            let notEmptyRows = otherRows.filter(function(idx, element) {
                return jq(element).find('td input').val() != '';
            })
            <ifMode mode="ENTER">
                otherRows.hide(); //Hide all rows apart from first row if adding new form
            </ifMode>

            <ifMode mode="EDIT">
                otherRows.hide(); //Hide all rows apart from first row if adding new form
                notEmptyRows.show();
            </ifMode>

            function addMore(targetId) {
                let table = jq("#" + targetId).parent();
                let thisRow = table.find('tr.main:visible:last');
                let nextRow = table.find('tr.main:hidden:first');
                if (nextRow.length &gt; 0) {
                    nextRow.show('slow');
                }
            };

            jq('.add-more').click(function (e) {
                e.preventDefault();
                addMore(this.id)
            });

            jq('.remove').click(function (e) {
                let closestTr = jq(this).closest('tr');
                closestTr.hide('slow');
                fieldHelper.clearAllFields(closestTr);
            });
        });

        jq('.autoCompleteText').on('keyup', function() {
            onBlurAutocomplete(this)
            jq(this).siblings('.error').hide();
            if (jq(this).siblings('.autoCompleteHidden')[0].value == 'ERROR') {
                jq(this).siblings('.error').html('Select valid value').show();
            }
        })
    </script>
    <uiInclude provider="botswanaemr" javascript="hiv-screening.js"/>
    <uiInclude provider="botswanaemr" javascript="botswanaemr-common.js"/>
    <style>
        .content-section {
            width: 100% !important;
        }

        input[type="radio"] {
            width: 1em;
            height: 1em;
            margin-top: 0.25em;
            vertical-align: top;
            background-color: #fff;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            border: 1px solid rgba(0,0,0,.25);
            border-radius: 50%;
        }

        input[type="radio"] + label {
            margin-top: 0px;
        }

        input[type="checkbox"] {
            width: 1em;
            height: 1em;
            margin-top: 0.25em;
            vertical-align: top;
            background-color: #fff;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain;
            border: 1px solid rgba(0,0,0,.25);
            border-radius: 50%;
        }

        input[type="checkbox"] + label {
            margin-top: 0px;
        }

        form fieldset, .form fieldset {
            min-width: 95% !important;
        }

        span.required {
            position: absolute;
            left: 0;
            top: 25%;
        }

        form select {
            min-width: 100%;
        }

        .title-section {
            margin-top: 2rem;
        }

        .inline-section {
            display: inline-flex;
            align-items: center;
        }

        .inline-section > label {
            margin-right: 1.5rem;
        }

        .top-space {
            margin-top: 10px;
        }

        .fill-secion {
            min-width: 100% !important;
        }

        .fill-secion > input {
            min-width: 100% !important;
        }

        .sub-element {
            margin-left: 0;
            width: 90% !important
        }

        .required {
            color: #fff;
            background-color: #fff;
        }

        input[type="text"] {
            width: 100% !important;
        }

        input[type="text"].hasDatepicker {
            min-width: 30% !important;
            width: 30% !important;
        }

        select {
            min-width: 25px !important;
            display: inline-block !important;
            padding-left: 5px !important;
        }

        select:focus {
            min-width: 25px;
            width: auto;
        }

        textarea {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
        }
        
        form fieldset, .form fieldset {
            min-width: 100% !important;
        }
    </style>
    <div class="card mt-4">
        <div class="content-section">
            <div class="container-fluid">
                <div class="row">
                    <h5 class="text-primary">PARTOGRAPH AND OTHER OBSERVATIONS</h5>
                </div>
                <div class="title-section">
                    <hr class="divider pt-1 bg-primary mb-4"/>
                </div>
                <fieldset class="form-section">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Place:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="4cd09f72-a3f8-49b6-b1e9-91ec576ef485" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Date of Admission:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="ecb39085-d350-4bea-af1d-433520402fb5" required="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col pl-0 pr-0">
                            <div class="col">
                                <label>Time of Admission:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col form-inline">
                                <obs conceptId="91521b58-3342-45de-ab76-55208c5b149c" required="true"/>
                            </div>
                        </div>
                        <div class="col pl-0 pr-0">
                            <div class="col">
                                <label>Time membranes raptured:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col form-inline">
                                <obs conceptId="f35782c0-c25a-4b0b-8e2d-35e7e8fc25a4" required="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 pl-0 pr-0">
                            <div class="col-12">
                                <label>Special Instructions:</label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="0ad5d91d-bf1b-4b10-9479-84b9f74531fd"
                                     style="textarea"/>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive" style="margin-top:10px">
                        <table class="table table-bordered multi">
                            <thead>
                                <th scope="col">FULL NAME</th>
                                <th scope="col">LABOUR AND DELIVERY NOTES</th>
                                <th scope="col">Actions</th>
                            </thead>
                            <tbody>
                                <repeat>
                                    <template>
                                        <tr class="main" id="{n}">
                                            <td>
                                                <obs conceptId="28a51fba-8cde-4064-9b61-2522e552931f"/>
                                            </td>
                                            <td>
                                                <obs conceptId="eff470bd-9a74-4e43-b070-6a12578f6c99"
                                                     style="textarea"/>
                                            </td>
                                            <td class="col-sm-1">
                                                <div class="row">
                                                    <div class="col remove text-danger">
                                                        <a>Delete</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </template>
                                    <render n="1"/>
                                    <render n="2"/>
                                    <render n="3"/>
                                    <render n="4"/>
                                    <render n="5"/>
                                    <render n="6"/>
                                    <render n="7"/>
                                    <render n="8"/>
                                    <render n="9"/>
                                    <render n="10"/>
                                </repeat>
                            </tbody>
                        </table>
                        <button class="dashed-button rounded add-more"
                                id="btnAddDiagnosis">+ Add
                        </button>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Pulse:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="5087AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Contractions per 10 minutes:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="d989c325-d9cc-4d6a-bfac-d208f206aafc" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Temp:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Fetal Heart Rate (bpm):
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="7a0b503c-a6f9-4884-ab06-e2015c468061" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Cervix size (in cm):
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="5dd4da91-2108-4027-88d9-ef377a63b3de" required="true"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Descent of Head (-4 to 3):
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="166528AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" required="true"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>BP:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <div class="col pl-0 form-inline">
                                    <div class="col px-0">
                                        <obs id="systolic" conceptId="5085AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                    </div>
                                    <div class="text-center text-separator text-primary text-bolder px-2">/</div>
                                    <div class="col px-0">
                                        <obs id="diastolic" conceptId="5086AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Urine:
                                    <span class="text-danger">*</span>
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="de2c9127-3913-4e24-99a3-812f2188e2f5" required="true" style="text"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 pl-0 pr-0">
                            <div class="col-8">
                                <label>Blood Sugar:
                                </label>
                            </div>
                            <div class="col-12">
                                <obs conceptId="887AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" required="false"/>
                            </div>
                        </div>
                    </div>

                    <uiInclude provider="botswanaemr" fragment="srh/partograph"/>

                </fieldset>
            </div>
            
            <div class="row actions mt-5 mb-3 pr-3">
                <div class="col-12 px-0">
                    <button class="btn btn-sm btn-dark bg-dark float-right ml-1" id="cancel" type="button">
                        Close
                    </button>
                    <button class="btn btn-sm btn-primary float-right mr-0" id="submit" type="submit">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    <redirectOnSave url="/botswanaemr/srh/obstetricProfile.page?patientId={{patient.id}}"/>
</htmlform>
