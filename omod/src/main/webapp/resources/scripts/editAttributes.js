jq = jQuery;
function validateBirthNumber(birthNumber) {
    return String(birthNumber)
        .match(/^[a-zA-Z][a-zA-Z]\d\d\/[0-9]+\/\d\d\d\d$/);
}