jq = jQuery;

let currentTab = 0; // Current tab is set to be the first tab (0)

function toSentenceCase(inputString) {
    let result = inputString.replace(/([A-Z])/g, " $1");
    result = result.charAt(0).toUpperCase() + result.slice(1);
    return result;
}

function hideAllTabs() {
    let all = jq(".form-tab").map(function (index, e) {
        jq(this).hide();
    });
}

function showTab(n) {
    jq('.step-nav a.active').removeClass('active');

    let all = jq(".form-tab").map(function (index, e) {
        if (n === index) {
            jq(this).show();
        }
    });

    if (n === 0) {
        jq("#prevBtn").hide();
    } else {
        jq("#prevBtn").show();
    }

    if (n === (jq(".form-tab").length - 1)) {
        jq("#nextBtn").html("Complete screening and Triage");
    } else {
        jq("#nextBtn").html("Next Step");
    }
    const active = n + 1;
    jq("#step-" + active).addClass("active");
}

function nextPrev(n) {
    // Hide the current tab:
    jq(".form-tab").map(function (index, e) {

        if (index === currentTab) {
            jq(this).hide();
        }
    });
    let final = currentTab === (jq(".form-tab").length - 1);

    if (n > 0 && currentTab < (jq(".form-tab").length - 1)) {
        currentTab = currentTab + n;
    } else if (n < 0 && currentTab < (jq(".form-tab").length)) {
        currentTab = currentTab + n;
    }

    showTab(currentTab);

    if (currentTab >= (jq(".form-tab").length - 1) && final) {
        jq("#screening-submit").trigger("click");
        showLoadingOverlay();
    } else if (currentTab >= (jq(".form-tab").length - 1) && !final) {
        confirmDetails();
    }
}

const genericColourCode = {
    extremelyLow: 'red',
    abnormal: 'red',
    low: 'orange',
    normal: 'green',
    high: 'orange',
    extremelyHigh: "red",
    obese: "red",
    underWeight: 'red',
    overWeight: 'orange',
    unrecordable: 'blue',
};

let validateUtils = {
    bmiValidation: function (value, age) {
        if (value <= 18.5) {
            return "underWeight";
        } else if (value <= 24.9) {
            return "normal";
        } else if (value <= 29.9) {
            return "overWeight";
        } else {
            return "obese";
        }
    },
    respiratoryValidation: function (value, age) {
        if (age <= 2) {
            if (value <= 19) {
                return "extremelyLow";
            } else if (value <= 25) {
                return "low";
            } else if (value <= 39) {
                return "normal";
            } else if (value <= 49) {
                return "high";
            } else if (value >= 50) {
                return "extremelyHigh";
            }
        } else if (age <= 12) {
            if (value <= 14) {
                return "extremelyLow";
            } else if (value <= 16) {
                return "low";
            } else if (value <= 21) {
                return "normal";
            } else if (value <= 26) {
                return "high";
            } else if (value >= 27) {
                return "extremelyHigh";
            }
        } else if (age > 18) {
            if (value <= 8) {
                return "extremelyLow";
            } else if (value <= 14) {
                return "normal";
            } else if (value <= 20) {
                return "high";
            } else if (value <= 29) {
                return "extremelyHigh";
            } else if (value >= 30) {
                return "abnormal";
            }
        }
    },
    heartRateValidation: function (value, age) {
        if (age <= 2) {
            if (value <= 69) {
                return "extremelyLow";
            } else if (value <= 79) {
                return "low";
            } else if (value <= 130) {
                return "normal";
            } else if (value <= 159) {
                return "high";
            } else if (value >= 160) {
                return "extremelyHigh";
            }
        } else if (age <= 12) {
            if (value <= 59) {
                return "extremelyLow";
            } else if (value <= 79) {
                return "low";
            } else if (value <= 99) {
                return "normal";
            } else if (value <= 129) {
                return "high";
            } else if (value >= 130) {
                return "extremelyHigh";
            }
        } else if (age > 18) {
            if (value <= 4) {
                return "extremelyLow";
            } else if (value <= 50) {
                return "low";
            } else if (value <= 100) {
                return "normal";
            } else if (value <= 110) {
                return "high";
            } else if (value <= 129) {
                return "extremelyHigh";
            } else if (value >= 130) {
                return "abnormal";
            }
        }
    },
    systolicValidation: function (value, age) {
        if (age <=12) {
            if (value < 96) {
                return "low";
            } else if (value <= 130) {
                return "normal";
            } else {
                return "high";
            }
        } else {
            if (value < 90) {
                return "low";
            } else if (value <= 140) {
                return "normal";
            } else {
                return "high";
            }
        }
    },
    diastolicValidation: function (value, age) {
        if (age <=12) {
            if (value < 55) {
                return "low";
            } else if (value <= 62) {
                return "normal";
            } else {
                return "high";
            }
        }else {
            if (value < 60) {
                return "low";
            } else if (value <= 90) {
                return "normal";
            } else {
                return "high";
            }
        }
    },
    temperatureValidation: function (value, age) {
        if (age >= 16) {
            if (value <= 35) {
                return "extremelyLow";
            } else if (value <= 38.4) {
                return "normal";
            } else {
                return "extremelyHigh";
            }
        } else {
            if (value <= 35) {
                return "extremelyLow";
            } else if (value <= 37.9) {
                return "normal";
            } else {
                return "extremelyHigh";
            }
        }
    },
    glucoseLevelValidation: function (value, age) {
        if (value < 4) {
            return "low";
        } else if(value <= 11 ){
            return "normal";
        }else if(value <= 25 ){
            return "high";
        }else {
            return "extremelyHigh";
        }
    },
    oxygenSaturationValidation: function (value, age) {
        if (value < 90) {
            return "extremelyLow";
        } else if( value < 95) {
            return "low";
        } else if(value <=100 ){
            return "normal";
        }
    }
}

let getColorCode = function (name, value) {
    let status = '';
    if (name.toLowerCase().includes("body mass") || name.toLowerCase().includes("bmi")) {
        status = validateUtils.bmiValidation(value, activePatientAge);
    } else if (name.toLowerCase().includes("respiratory")) {
        status = validateUtils.respiratoryValidation(value, activePatientAge);
    } else if (name.toLowerCase().includes("pulse")) {
        status = validateUtils.heartRateValidation(value, activePatientAge);
    } else if (name.toLowerCase().includes("systolic")) {
        status = validateUtils.systolicValidation(value, activePatientAge);
    }else if (name.toLowerCase().includes("diastolic")) {
        status = validateUtils.diastolicValidation(value, activePatientAge);
    } else if (name.toLowerCase().includes("temperature")) {
        status = validateUtils.temperatureValidation(value, activePatientAge);
    }else if (name.toLowerCase().includes("rbs")) {
        status = validateUtils.glucoseLevelValidation(value, activePatientAge);
    }else if (name.toLowerCase().includes("oxygen")) {
        status = validateUtils.oxygenSaturationValidation(value, activePatientAge);
    }

    return genericColourCode[status] || 'inherit';
}

function getTableData() {
    let tableData = {};
    const tableIds = [
        "tbPatientConditions",
        "tbPatientAllergies",
        "tbFamilyHistory",
        "tbPatientOperations",
        "tbPatientLifestyle",
        "tbPatientDiet",
        "tbPatientSymptoms",
        "tbPatientMedication"
    ];

    tableIds.map(function (targetId) {
        let arr = [];
        let trs = jq("#" + targetId).find("tbody>tr");

        trs.each(function () {
            if (!jq(this)[0].hidden) {
                let td = jq(this).find("td:first");
                let input = td.find("input:first");

                input.val() !== "" ? arr.push(input.val()) : ''; //put elements into array
                if(targetId === "tbPatientMedication"){
                    let notesTd = jq(this).find("#medicationNotes");
                    let notesInput = notesTd.find("input:first");
                    notesInput.val() !== "" ? arr.push("Note : " + notesInput.val()) : '';
                }
            }

        });
        tableData[targetId] = arr;
    })

    return tableData;
}

function getVitalsData() {
    let arr = []
    jq("#numericalVitals :input").each(function () {
        if (jq(this).val() !== "" && jq(this).val() !== null) {

            let row = jq(this).parent().closest('.row');
            let label = row.find('label');


            let labelText = label.html();
            if(labelText.includes("blood pressure")){
                return;
            }
            let val = labelText + " : " + "<span class='bg-transparent' style='color:"+ getColorCode(labelText, jq(this).val()) +"'>" + jq(this).val()+ "</span>";

            if (jq(this).attr("type") === "radio") {
                return;
            } else if (jq(this).attr("type") === undefined) {
                let selected = jq(this).find('option:selected');
                if(selected === undefined){
                    return;
                }else {
                    val = labelText + " : " + selected.html();
                }
            }
            arr.push(val);
        }
    });

    let systolic = htmlForm.getValueIfLegal('systolic.value');
    let diastolic = htmlForm.getValueIfLegal('diastolic.value');

    if (systolic && diastolic){
        arr.unshift("Blood pressure  (mmHg) : " +  "<span class='bg-transparent' style='color:"+ getColorCode("systolic", systolic) +"'>" + systolic + "</span>" + "/" + "<span class='bg-transparent' style='color:"+ getColorCode("diastolic", diastolic) +"'>" + diastolic + "</span>");
    }

    return arr;
}

function getFormData() {
    let formData = {};
    let tableData = getTableData();

    tableData["tbPatientConditions"].length > 0 ? formData["conditions"] = tableData["tbPatientConditions"] : '';
    tableData["tbPatientAllergies"].length > 0 ? formData["allergies"] = tableData["tbPatientAllergies"] : '';
    tableData["tbFamilyHistory"].length > 0 ? formData["familyMedicalHistory"] = tableData["tbFamilyHistory"] : '';
    tableData["tbPatientOperations"].length > 0 ? formData["pastOperations"] = tableData["tbPatientOperations"] : '';
    tableData["tbPatientLifestyle"].length > 0 ? formData["lifestyleHabits"] = tableData["tbPatientLifestyle"] : '';
    tableData["tbPatientDiet"].length > 0 ? formData["currentDiet"] = tableData["tbPatientDiet"] : '';

    tableData["tbPatientSymptoms"].length > 0 ? formData["symptoms"] = tableData["tbPatientSymptoms"] : '';
    let vitalsData = getVitalsData();

    vitalsData.length > 0 ? formData["vitals"] = vitalsData : '';
    tableData["tbPatientMedication"].length > 0 ? formData["immediateMedication"] = tableData["tbPatientMedication"] : '';

    return formData;

}

function confirmDetails() {
    jq("#confirmDetails").empty();
    let formData = getFormData();

    for (let key in formData) {
        let uniqueValues = new Set(formData[key].map(value => value.trim()));
        let appendHtml = "<h6 class=\"h6\" style=\"margin-top:2rem\">" + toSentenceCase(key) + "</h6>";

        uniqueValues.forEach(value => {
            appendHtml += "<p class=\"p-margin\">" + value + "</p>";
        });

        if (uniqueValues.size > 0) {
            jq('#confirmDetails').append(appendHtml);
        }
    }
}


jq(function () {
    // Display the current tab
    jq(document).ready(function () {
        showTab(currentTab);
        jq("#saveOverlay").hide();
    });

    const hash = window.location.hash;
});

function showOverlay(){
        showLoadingOverlay();
}
