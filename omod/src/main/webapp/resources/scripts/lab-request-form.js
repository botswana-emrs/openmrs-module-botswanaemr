jq(document.body).ready(function () {

    jq('[id^="reTreatmentOptions"]').hide()
    jq('[id^="lpaTestsOptions"]').hide()
    jq('[id^="dstTestsOptions"]').hide()
    jq('[id^="otherMonths"]').hide()
    jq('[id^="reasonForRejectingSpecimen"]').hide()


    jq('#patientTreatmentCategory').change(function () {
        if ((getValue('patientTreatmentCategory.value') === "165275")) {
            jq('[id^="reTreatmentOptions"]').show()
        } else {
            jq('[id^="reTreatmentOptions"]').hide()
        }
    });

    jq('#testRequired').change(function () {
        if ((getValue('testRequired.value') === "165205")) {
            jq('[id^="lpaTestsOptions"]').show()
        } else {
            jq('[id^="lpaTestsOptions"]').hide()

            if (getValue('testRequired.value') === "165254") {
                jq('[id^="dstTestsOptions"]').show()
            } else {
                jq('[id^="dstTestsOptions"]').hide()
            }
        }
    });


    jq('#specimenCollectionPeriod').change(function () {
        if ((getValue('specimenCollectionPeriod.value') === "165174")) {
            jq('[id^="otherMonths"]').show()
        } else {
            jq('[id^="otherMonths"]').hide()
        }
    });

    jq('#rejectedSpecimen').change(function () {
        if ((getValue('rejectedSpecimen.value') === "1065")) {
            jq('[id^="reasonForRejectingSpecimen"]').show()
        } else {
            jq('[id^="reasonForRejectingSpecimen"]').hide()
        }
    });
});