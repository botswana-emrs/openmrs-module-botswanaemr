var app = angular.module("personAttributesApp",
    ['app.restfulServices', 'app.models', 'app.commonFunctionsFactory']);

app.controller("PersonAttributesController", PersonAttributesController);
PersonAttributesController.$inject = ['$scope', 'RestfulService', 'CommonFunctions'];

function PersonAttributesController($scope, RestfulService, CommonFunctions) {
    $scope.nokIndex = 0;
    $scope.nokAttributeEdit = [];
    $scope.personNokAttributes = $scope.personNokAttributes || [];
    $scope.idNumber = '';
    $scope.idType = $scope.idType || '';
    $scope.replaceRegex = /[A-Z]{3}-\d{1}-/;

    let val = {
        "value": '',
        "name": '',
        "uuid": '',
    };
    $scope.extraContact = $scope.extraContact || val;
    $scope.email = val;
    $scope.contact = $scope.email || val;
    $scope.relationship = $scope.relationship || val;
    $scope.fullName = $scope.fullName || val;
    $scope.idNumber = $scope.idNumber || val;
    $scope.idType = $scope.idType || val;

    $scope.getPersonProfile = function (patientUuid) {
        $scope.isProcessing = true;
        $scope.patientUuid = patientUuid;
        RestfulService.setBaseUrl('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/patientprofile');

        if ($scope.patientUuid !== null && $scope.patientUuid !== undefined) {
            RestfulService.get(patientUuid, {}, function (data) {
                if (data.patient) {
                    let personAttributes = data.patient.person.attributes;

                    // Get NOk attributes only
                    personAttributes = personAttributes.filter(function (attribute) {
                        return attribute.attributeType.display.startsWith("NOK-");
                    });
                    let filteredAttributes = {};
                    angular.forEach(personAttributes, function (attribute, index) {
                        let numIndex = attribute.attributeType.display.split("-")[1].charAt(0);
                        if (!filteredAttributes[numIndex]) {
                            filteredAttributes[numIndex] = [];
                        }
                        filteredAttributes[numIndex].push(attribute);
                    })
                    $scope.personNokAttributes = filteredAttributes;
                }

            }, function (error) {
                $scope.isProcessing = false;
            });
        }
    }

    $scope.setAttributeToEdit = function (data) {
        $scope.nokAttributeEdit = data;

        angular.forEach($scope.nokAttributeEdit, function (attribute) {
            let val = {
                "value": attribute.value,
                "name": attribute.attributeType.display,
                "uuid": attribute.uuid
            };

            if (attribute.attributeType.display.includes('ID-Type')) {
                $scope.idType = val;
            }
            if (attribute.attributeType.display.includes('ID-Number')) {
                $scope.idNumber = val;
            }
            if (attribute.attributeType.display.includes('Fullname')) {
                $scope.fullName = val;
            }
            if (attribute.attributeType.display.includes('Relationship')) {
                $scope.relationship = val;
            }
            if (attribute.attributeType.display.includes('Contact')
                && !attribute.attributeType.display.includes('Extra-Contact')) {
                $scope.contact = val;
            }
            if (attribute.attributeType.display.includes('Email')) {
                $scope.email = val;
            }
            if (attribute.attributeType.display.includes('Extra-Contact')) {
                $scope.extraContact = val;
            }
        });
    }

    function resetVariables(){
        let val = {
            "value": '',
            "name": '',
            "uuid": '',
        };
        $scope.extraContact = val;
        $scope.email = val;
        $scope.contact = val;
        $scope.relationship = val;
        $scope.fullName = val;
        $scope.idNumber = val;
        $scope.idType = val;
    }

    $scope.addNextOfKin = function () {
        resetVariables();
    }

    $scope.getNokAttributeValue = function (attributeType) {
        angular.forEach($scope.nokAttributeEdit, function (attribute) {
            if (attribute.attributeType.display.includes(attributeType)) {
                return attribute.value;
            }
        });
        return '';
    }

}