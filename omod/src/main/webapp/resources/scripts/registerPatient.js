jq = jQuery;

let patientIdType = "nationalIdNumber";
let nokIdType = { "nokIdType": "nationalIdNumber" };
let contactPersonType = { "contactPersonType": "contactPersonType" };
let nokPositionIndex = 0;
let genderValid = true;
let registered = false;
let nokRelationShips = {};
let namesSet = false;
let genderSet = false;
let nokIdValidation = [];
let extraPhoneInstances = {};
let extraNokPhoneInstances = {};
const defaultCountryVal = "Botswana";
const defaultCityAddressField = "cityVillage";
const cities = [
    'Serowe', 'Selibe Phikwe', 'Mahalapye', 'Palapye', 'Letlhakane', 'Tonota', 'Bobonong', 'Tutume', 'Mmadinare', 'Shoshong', 'Orapa', 'Nata', 'Lerala', 'Rakops', 'Gweta', 'Sefophe', 'Dukwi', 'Maitengwe', 'Mathangwane', 'Sefhare', 'Borolong', 'Mopipi', 'Chadibe', 'Moiyabana', 'Maunatlala', 'Tsetsebjwe', 'Ramokgonami', 'Mandunyane', 'Shashe-Mooke', 'Lecheng', 'Sebina', 'Nkange', 'Serule', 'Sowa', 'Shashe/Semotswane', 'Tumasera', 'Mookane', 'Thabala', 'Chadibe', 'Lesenepole/Matolwane', 'Radisele', 'Molalatau', 'Mmashoro', 'Kasane', 'Kazungula', 'Ghanzi', 'Charles Hill', 'Tshabong', 'Kang', 'Hukuntsi', 'Werda', 'Mochudi', 'Bokaa', 'Oodi', 'Rasesa', 'Morwa', 'Modipane', 'Artisia', 'Molepolole', 'Mogoditshane', 'Thamaga', 'Mmopane', 'Gabane', 'Kopong', 'Letlhakeng', 'Metsimotlhabe', 'Lentsweletau', 'Mmankgodi', 'Kumakwane', 'Sojwe', 'Takatokwane', 'Salajwe', 'Khudumelapye', 'Matshelagabedi', 'Matsiloje', 'Francistown', 'Tati Siding', 'Masunga', 'Maun', 'Gumare', 'Shakawe', 'Etsha 6', 'Sehithwa', 'Seronga', 'Nokaneng', 'Tsao', 'Gaborone', 'Tlokweng', 'Ramotswa', 'Lobatse', 'Otse', 'Ramotswa station/Taung', 'Kanye', 'Moshupa', 'Jwaneng', 'Molapowabojang', 'Mmathethe', 'Good Hope', 'Lotlhakane', 'Manyana', 'Pitsane Siding', 'Khakhea', 'Digwana'
]
let idTypeValid = true;
let emergencyPatientDob = "1950-01-01";
cities.sort((a, b) => a.localeCompare(b));

function resetPatientIdError() {
    jq("#patientIdError").text('');
    jq("#patientIdError").hide();
}

function resetPatientIdErrorAdvancedSearch() {
    jq("#patientIdErrorAdvanced").text('');
    jq("#patientIdErrorAdvanced").hide();
}

function resetNokIdError(nokIndex) {
    jq("#nokIdError" + nokIndex).hide();
    jq("#nokIdError" + nokIndex).text('');
    resetNokIdValidation(nokIndex);
}

function resetNokIdValidation(nokIndex) {
    if (nokIdValidation.includes(nokIndex)) {
        const index = nokIdValidation.indexOf(nokIndex);
        if (index > -1) {
            nokIdValidation.splice(index, 1);
        }
    }
}

function setIdText() {
    let text = "";
    if (patientIdType === "nationalIdNumber") {
        text = "ID Number"
    } else if (patientIdType === "passportNumber") {
        text = "Passport Number"
    } else if (patientIdType === "birthCertificateNumber") {
        text = "Birth Certificate Number"
    } else if (patientIdType === "noIdentification") {
        text = "No Identification"
        jq("#patientId").attr("placeholder", "");
    }
    jq("#idNumberLabel").html(text);
    jq("idNumberLabelAdvanced").html(text);
    if (patientIdType !== "noIdentification") {
        jq("#patientId").attr("placeholder", "Enter the patient's " + text);
    }
}

function setIdTextAdvanced() {
    let text = "";
    if (patientIdType === "nationalIdNumber") {
        text = "ID Number"
    } else if (patientIdType === "passportNumber") {
        text = "Passport Number"
    } else if (patientIdType === "birthCertificateNumber") {
        text = "Birth Certificate Number"
    } else if (patientIdType === "noIdentification") {
        text = "No Identification"
        jq('#patientIdAdvanced').attr('placeholder', "");
    }
    jq("idNumberLabelAdvanced").html(text);
    if (patientIdType !== "noIdentification") {
        jq('#patientIdAdvanced').attr('placeholder', "Enter the patient's" + text);
    }
}

function selectAdvancedIdTypeIdentification(targetId) {
    console.log("Button clicked:", targetId); // Check which button was clicked
    console.log("Current patientIdType before update:", patientIdType);
    const targetControl = jq('#' + targetId);
    if (targetControl.has('outline-button-active')) {
        targetControl.removeClass('outline-button-active');
        patientIdType = "";
        targetId = "";
    }

    if (targetId === "btnAdvancedId" && patientIdType !== "nationalIdNumber") {
        jq("#btnAdvancedId").addClass("outline-button-active");
        jq("#btnAdvancedPassport").removeClass("outline-button-active");
        jq("#btnAdvancedBirthCertificate").removeClass("outline-button-active");
        patientIdType = "nationalIdNumber";
        resetPatientIdErrorAdvancedSearch();
    } else if (targetId === "btnAdvancedPassport" && patientIdType !== "passportNumber") {
        jq("#btnAdvancedPassport").addClass("outline-button-active");
        jq("#btnAdvancedId").removeClass("outline-button-active");
        jq("#btnAdvancedBirthCertificate").removeClass("outline-button-active");
        patientIdType = "passportNumber";
        genderValid = true;
        resetPatientIdErrorAdvancedSearch();
    } else if (targetId === "btnAdvancedBirthCertificate" && patientIdType !== "birthCertificateNumber") {
        jq("#btnAdvancedBirthCertificate").addClass("outline-button-active");
        jq("#btnAdvancedId").removeClass("outline-button-active");
        jq("#btnAdvancedPassport").removeClass("outline-button-active");
        patientIdType = "birthCertificateNumber"
        genderValid = true;
        resetPatientIdErrorAdvancedSearch();


    }

    setIdTextAdvanced();
    if (patientIdType == "") {
        jq("#patientIdAdvanced").prop("type", "text");
        jq("#patientIdAdvanced").removeAttr("minlength");
        jq("#patientIdAdvanced").removeAttr("maxlength");
    } else {
        if (patientIdType !== "passportNumber") {
            jq("#patientIdAdvanced").prop("type", "number");
            jq("#patientIdAdvanced").attr("minlength", "9");
            jq("#patientIdAdvanced").attr("maxlength", "9");
        } else {
            jq("#patientIdAdvanced").prop("type", "text");
            jq("#patientIdAdvanced").removeAttr("minlength");
            jq("#patientIdAdvanced").removeAttr("maxlength");
        }

        if (patientIdType !== "noIdentification") {
            jq("#patientIdAdvanced").attr("required", true);
            jq("#address2").attr("required", true);
            jq("#cityVillage").attr("required", true);
            jq("#phoneNumber").attr("required", true);
            jq(".temp-label").show();
            jq("#patientIdAdvanced").prop("disabled", false);
        } else {
            jq("#patientIdAdvanced").attr("required", false);
            jq("#address2").attr("required", false);
            jq("#cityVillage").attr("required", false);
            jq("#phoneNumber").attr("required", false);
            jq(".temp-label").hide();
            jq("#patientIdAdvanced").prop("disabled", true);
        }
    }
    // patientIdType === "" ? jq('#idTypeErrorAdvanced').show() : jq('#idTypeErrorAdvanced').hide();
    idTypeValid = patientIdType !== "";

}

function selectIdTypeIdentification(targetId) {
    let citizenType = jq('input[name="citizenType"]:checked').attr("data-attr");
    let age = jq("#age").val();
    if (targetId === "btnId" && patientIdType !== "nationalIdNumber" && citizenType === "Citizen") {
        jq("#btnId").addClass("outline-button-active");
        jq("#btnPassport").removeClass("outline-button-active");
        jq("#btnBirthCertificate").removeClass("outline-button-active");
        jq("#noIdentification").removeClass("outline-button-active");
        patientIdType = "nationalIdNumber";
        resetPatientIdError();
    } else if (targetId === "btnPassport" && patientIdType !== "passportNumber" && citizenType === "Non-citizen") {
        jq("#btnPassport").addClass("outline-button-active");
        jq("#btnId").removeClass("outline-button-active");
        jq("#btnBirthCertificate").removeClass("outline-button-active");
        jq("#noIdentification").removeClass("outline-button-active");
        patientIdType = "passportNumber";
        genderValid = true;
        resetPatientIdError();
    } else if (targetId === "btnBirthCertificate" && patientIdType !== "birthCertificateNumber" && citizenType === "Citizen") {
        if (age && age > 16) {
            jq().toastmessage('showErrorToast', 'Birth certificate only allowed for Patients 16 years or below.');
        } else {
            jq("#btnBirthCertificate").addClass("outline-button-active");
            jq("#btnId").removeClass("outline-button-active");
            jq("#btnPassport").removeClass("outline-button-active");
            jq("#noIdentification").removeClass("outline-button-active");
            patientIdType = "birthCertificateNumber"
            genderValid = true;
            resetPatientIdError();
        }
    } else if (targetId === "noIdentification" && patientIdType !== "noIdentification") {
        jq("#noIdentification").addClass("outline-button-active");
        jq("#btnId").removeClass("outline-button-active");
        jq("#btnPassport").removeClass("outline-button-active");
        jq("#btnBirthCertificate").removeClass("outline-button-active");
        patientIdType = "noIdentification"
        genderValid = true;
        resetPatientIdError();
    }

    setIdText();
    if (patientIdType !== "passportNumber") {
        jq("#patientId").prop("type", "number");
        jq("#patientId").attr("minlength", "9");
        jq("#patientId").attr("maxlength", "9");
    } else {
        jq("#patientId").prop("type", "text");
        jq("#patientId").removeAttr("minlength");
        jq("#patientId").removeAttr("maxlength");
    }

    if (patientIdType !== "noIdentification") {
        jq("#patientId").attr("required", true);
        jq("#address2").attr("required", true);
        jq("#cityVillage").attr("required", true);
        jq("#phoneNumber").attr("required", true);
        jq(".temp-label").show();
        jq("#patientId").prop("disabled", false);
    } else {
        jq("#patientId").attr("required", false);
        jq("#address2").attr("required", false);
        jq("#cityVillage").attr("required", false);
        jq("#phoneNumber").attr("required", false);
        jq(".temp-label").hide();
        jq("#patientId").prop("disabled", true);
    }
    patientIdType === "" ? jq('#idTypeError').show() : jq('#idTypeError').hide();
    idTypeValid = patientIdType !== "";

}

function setNokIdText(nokIndex) {
    let text = "ID number";
    if (nokIdType["nokIdType" + nokIndex] === "passportNumber") {
        text = "Passport Number"
    } else if (nokIdType["nokIdType" + nokIndex] === "birthCertificateNumber") {
        text = "Birth Certificate Number"
    }
    jq("#nokIdNumberLabel" + nokIndex).html(text);
}

function selectNokIdTypeIdentification(targetId) {
    const nokIndex = isNaN(targetId.charAt(targetId.length - 1)) ? '' : targetId.charAt(targetId.length - 1);
    if (targetId.startsWith("nokBtnId") && nokIdType["nokIdType" + nokIndex] !== "nationalIdNumber") {
        jq("#nokBtnId" + nokIndex).addClass("outline-button-active");
        jq("#nokBtnPassport" + nokIndex).removeClass("outline-button-active");
        jq("#nokBtnBirthCertificate" + nokIndex).removeClass("outline-button-active");
        nokIdType["nokIdType" + nokIndex] = "nationalIdNumber";
        resetNokIdError(nokIndex);
    } else if (targetId.startsWith("nokBtnPassport") && nokIdType["nokIdType" + nokIndex] !== "passportNumber") {
        jq("#nokBtnPassport" + nokIndex).addClass("outline-button-active");
        jq("#nokBtnId" + nokIndex).removeClass("outline-button-active");
        jq("#nokBtnBirthCertificate" + nokIndex).removeClass("outline-button-active");
        nokIdType["nokIdType" + nokIndex] = "passportNumber";
        resetNokIdError(nokIndex);
    } else if (targetId.startsWith("nokBtnBirthCertificate") && nokIdType["nokIdType" + nokIndex] !== "birthCertificateNumber") {
        jq("#nokBtnBirthCertificate" + nokIndex).addClass("outline-button-active");
        jq("#nokBtnId" + nokIndex).removeClass("outline-button-active");
        jq("#nokBtnPassport" + nokIndex).removeClass("outline-button-active");
        nokIdType["nokIdType" + nokIndex] = "birthCertificateNumber";
        resetNokIdError(nokIndex);
    } else {
        jq("#nokBtnId").removeClass("outline-button-active");
        jq("#nokBtnPassport").removeClass("outline-button-active");
        jq("#nokBtnBirthCertificate").removeClass("outline-button-active");
        delete nokIdType["nokIdType" + nokIndex];
        resetNokIdError(nokIndex);
    }
    setNokIdText(nokIndex);

    if (nokIdType["nokIdType" + nokIndex] !== "passportNumber") {
        jq("#nokIdNumber" + nokIndex).prop("type", "number");
        jq("#nokIdNumber" + nokIndex).attr("minlength", "9");
    } else {
        jq("#nokIdNumber" + nokIndex).prop("type", "text");
        jq("#nokIdNumber" + nokIndex).removeAttr("minlength");
    }
    nokIdType["nokIdType" + nokIndex] === "" ? jq('#nokIdTypeError' + nokIndex).show() : jq('#nokIdTypeError' + nokIndex).hide();

}

jq(document).ready(function() {
    jq("#patientIdError").hide();
    jq("#patientSpinner").hide();
    jq("#nokIdError").hide();
    jq("#nokSpinner").hide();
    jq('#idTypeError').hide();
    jq("#nokIdTypeError").hide();
    jq("#genderError").hide();
    jq("#dateOfBirthError").hide();
    jq("#patientEmailError").hide();
    jq("#nextOfKinEmailError").hide();
    jq("#nameError").hide();
    jq("#otherEmploymentSectorGrp").hide();
    jq("#otherDistrict").hide();
    jq("#otherDistrict2").hide();
    jq("#otherNokRelationship").hide();
    jq("#fgCountry").hide();
    jq("#existing").hide();
    jq("#existingAdvancedSearch").hide();
    jq("#patientIdErrorAdvanced").hide();
    jq('#btnPassport').hide();
    jq("#advancedPatientSearchSpinner").hide();
    if (jq("#affiliation").val() !== 'Force Officer') {
        jq("#fgForceNumber").hide();
        jq("#fgRank").hide();
    }

    const PATIENT_REGISTRATION_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/registration";
    const OPENCR_CREATE_RECORD_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/botswanaemr/registry/savePatientInRegistry";
    const GP_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/systemsetting";

    const SEARCH_REGISTRY_TEXT = "Searching registry..."
    const SEARCH_LOCAL_DATABASE_TEXT = "Searching local database..."
    const ID_NOT_FOUND = "ID not found in registry. Register patient manually.";
    const NOK_ID_NOT_FOUND = "ID not found in registry. Register next of kin manually.";
    const ID_LOCAL_SEARCH_FOUND = "The Patient is already registered in the local database, register patient as an existing patient";
    const INVALID_BIRTH_NUMBER = "Invalid Birth Number. Should be in the format XX00/00000000/0000"
    const INVALID_BIRTH_NUMBER_YEAR = "Birth year can not be more than the current year";
    const INVALID_ID_NUMBER = "Invalid ID number. The fifth number ID should be 1 or 2";
    const MAX_PATIENT_CONTACTS = 5;
    const MAX_NEXT_OF_KINS = 5;
    let offline = false;
    let emergency = false;

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    jq("#btnId").addClass("outline-button-active");
    jq("#btnAdvancedId").addClass("outline-button-active");
    jq("#nokBtnId").addClass("outline-button-active");

    const todayDate = new Date();
    jq("#dateOfBirth").attr({
        "max": todayDate.toISOString().split('T')[0],
    });

    // TODO : Pull relationship list from openmrs api. Add child rel check by gender
    const relationships = [
        "Spouse", "Sibling", "Mother", "Father", "Child", "Aunt", "Uncle", "Niece", "Nephew"
    ]

    // TODO : Pull occupation list from concepts
    const occupations = [
        "Chief Executive, Senior Official, and Legislator",
        "Administrative and Commercial Manager",
        "Production and Specialised Services Manager",
        "Hospitality, Retail, and Other Services Manager",
        "Science and Engineering Professional",
        "Health Professional",
        "Teaching Professional",
        "Business and Administration Professional",
        "Information and Communications Technology Professional",
        "Legal, Social, and Cultural Professional",
        "Science and Engineering Associate Professional",
        "Health Associate Professional",
        "Business and Administration Associate Professional",
        "Legal, Social, Cultural, and Related Associate Professional",
        "Information and Communications Technician",
        "Primary, Pre-Primary, Vocational, and Technical Education Teacher",
        "General and Keyboard Clerk",
        "Customer Services Clerk",
        "Numerical and Material Recording Clerk",
        "Other Clerical Support Worker",
        "Personal Service Worker",
        "Sales Worker",
        "Personal Care Worker",
        "Protective Services Worker",
        "Market Oriented Skilled Agricultural, Forestry, and Fishery Worker",
        "Market-Oriented Skilled Forestry, Fishery, and Hunting Worker",
        "Subsistence Farmer, Hunter, Fisher, Trapper, and Gatherer",
        "Building and Related Trades Worker (Excluding Electrician)",
        "Metal, Machinery, and Related Trades Worker",
        "Handicraft and Printing Worker",
        "Electrical and Electronics Trades Worker",
        "Food Processing, Woodworking, Garment, and Other Craft and Related Trades Worker",
        "Stationary Plant and Machine Operator",
        "Plant and Machine Assembler",
        "Driver and Mobile Machine/Plant Operator",
        "Cleaner and Helper",
        "Agricultural, Fishery, and Forestry Labourer",
        "Labourer in Mining, Construction, Manufacturing, and Transport",
        "Food Preparation Assistant",
        "Street and Related Sales and Services Elementary Worker",
        "Refuse Worker and Other Elementary Occupation Worker",
        "Commissioned Armed Forces Officer",
        "Non-Commissioned Armed Forces Officer",
        "Armed Forces Occupation Worker",
        "Self-Employed"
    ];
    const otherOccupation = "Other Occupation";
    const filteredOccupations = occupations.filter(occupation => occupation !== otherOccupation);

    filteredOccupations.sort((a, b) => a.localeCompare(b));
    filteredOccupations.push(otherOccupation);

    $('#occupation').empty();
    $('#occupation').html('<option disabled selected>Select the patient\'s occupation</option>');
    filteredOccupations.forEach(occupation => {
        $('#occupation').append($('<option>', {
            value: occupation,
            text: occupation
        }));
    });

    $('#occupation').on('change', function() {
        if ($(this).val() === "Other Occupation") {
            $('#otherOccupation').show();
            $("#otherOccupation").show().prop('required', true);
        } else {
            $('#otherOccupation').hide().val('');
            $("#otherOccupation").hide().prop('required', false);
        }
    });

    function getDistrictAddresses() {
        const url = '/' + OPENMRS_CONTEXT_PATH + '/module/addresshierarchy/ajax/getChildAddressHierarchyEntries.form';
        let params = {
            searchString: defaultCountryVal
        }

        return jq.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            data: params,
            success: function(data) {
                jq.each(data, function(key, value) {
                    var newOption = jq('<option>', {
                        value: value.name,
                        text: value.name
                    });
                    jq('#address2').append(newOption);
                    jq('#currentDistrict').append(newOption.clone());
                    jq('#nokAddress2').append(jq('<option>', {
                        value: value.name,
                        text: value.name
                    }));
                    jq('#nokDistrict').append(jq('<option>', {
                        value: value.name,
                        text: value.name
                    }));
                });
            },
            error: function(response) {
                console.log("response");
            }
        });
    }

    getDistrictAddresses();

    function setCityAutocomplete(element, districtElemId) {
        element.autocomplete({
            source: function(request, response) {
                jq.ajax({
                    url: '/' + OPENMRS_CONTEXT_PATH + '/module/addresshierarchy/ajax/getPossibleAddressHierarchyEntriesWithParents.form',
                    type: "GET",
                    dataType: "json",
                    data: {
                        searchString: request.term,
                        addressField: defaultCityAddressField,
                        limit: 20
                    },
                    success: function(data) {
                        let district = jq("#" + districtElemId).val();
                        let results = [];
                        jq.each(data, function(key, value) {
                            let result = {
                                label: value.name,
                                value: value.name,
                                parent: value.parent.name
                            };
                            if (!district) {
                                results.push(result);
                            } else if (district === value.parent.name) {
                                results.push(result);
                            }
                        });
                        response(results);
                    },
                    error: function(response) {
                        console.log("response");
                    }
                });
            },
            select: function(event, ui) {
                if (!jq("#" + districtElemId).val()) {
                    jq("#" + districtElemId).val(ui.item.parent);
                }
            },
        });
    }

    setCityAutocomplete(jq("#cityVillage"), "address2");
    setCityAutocomplete(jq("#currentCityVillage"), "currentDistrict");

    jq("#nokCityVillage").autocomplete({
        source: cities
    });

    setCityAutocomplete(jq("#nokCityVillage"), "nokAddress2");

    jq("#address2").on("change", function() {
        jq("#cityVillage").val('');
        if (this.value.toLowerCase() === "other") {
            jq("#otherDistrict").show().prop('required', true);
        } else {
            jq("#otherDistrict").val("");
            jq("#otherDistrict").hide().prop('required', false);
        }
    });

    jq("#currentDistrict").on("change", function() {
        jq("#cityVillage").val('');
        if (this.value.toLowerCase() === "other") {
            jq("#otherDistrict2").show().prop('required', true);
        } else {
            jq("#otherDistrict2").val("");
            jq("#otherDistrict2").hide().prop('required', false);
        }
    });

    jq("#affiliation").on("change", function() {
        if (jq("#affiliation").val() === 'Force Officer') {
            jq("#fgForceNumber").show();
            jq("#fgRank").show();
        } else {
            jq("#fgForceNumber").hide();
            jq("#fgRank").hide();
        }
    });

    jq("#occupation").on('change', function() {
        const exceptions = ['unemployed', 'retired'];
        if (exceptions.includes(this.value.toLowerCase())) {
            jq("#employer").val('');
            jq("#employmentSector").val('');
            jq("#otherEmploymentSector").val('');
            jq("#employer").prop("disabled", true);
            jq("#employmentSector").prop("disabled", true);
            jq("#otherEmploymentSector").prop("disabled", true);
        } else {
            jq("#employer").prop("disabled", false);
            jq("#employmentSector").prop("disabled", false);
            jq("#otherEmploymentSector").prop("disabled", false);
            if (this.value.toLowerCase().includes('student')) {
                jq("#employer").prop("disabled", true);
                jq("#nameOfEmployer").prop("disabled", true);
                jq("#employmentSector").prop("disabled", true);
            }
        }
    });

    jq("#country").autocomplete({
        source: function(request, response) {
            let data = searchCountry(request.term);
            response(data);
        },
    });

    jq('#employmentSector').on('change', function() {
        if (this.value.toLowerCase() === '5622') {
            jq("#otherEmploymentSectorGrp").show();
            jq("#otherEmploymentSector").prop('required', true);
            enableIfDisabled("otherEmploymentSector");
        } else {
            jq("#otherEmploymentSector").val("");
            jq("#otherEmploymentSectorGrp").hide();
            jq("#otherEmploymentSector").prop('required', false);
            clearAndDisable("otherEmploymentSector");
        }
    });

    jq('input[name="patientType"]').on("change", function() {
        let todayDate = new Date();
        if (this.value.toLowerCase().includes("child")) {
            todayDate.setFullYear(todayDate.getFullYear() - 10);
        } else {
            todayDate.setFullYear(todayDate.getFullYear() - 50);
        }
        emergencyPatientDob = numericToIsoDateFormat(todayDate);
    });

    extraPhoneInstances["phoneNumber"] = jQuery("#phoneNumber")
    extraPhoneInstances["phoneNumber"].intlTelInput({
        initialCountry: "bw",
        hiddenInput: "phoneNumberCode"
    });

    jq("#phoneNumber").on("countrychange", function() {
        let code = extraPhoneInstances["phoneNumber"].intlTelInput('getSelectedCountryData').dialCode;
        if (code === '267') {
            jq("#phoneNumber").attr("maxlength", "8");
        } else {
            jq("#phoneNumber").attr("maxlength", "15");
        }
    });

    extraNokPhoneInstances["nokContact"] = jQuery("#nokContact")
    extraNokPhoneInstances["nokContact"].intlTelInput({
        initialCountry: "bw",
        hiddenInput: "nokContactCode"
    });

    jq("#nokContact").on("countrychange", function() {
        let code = extraNokPhoneInstances["nokContact"].intlTelInput('getSelectedCountryData').dialCode;
        if (code === '267') {
            jq("#nokContact").attr("maxlength", "8");
        } else {
            jq("#nokContact").attr("maxlength", "15");
        }
    });

    jq("body").on("change", "select[name=relationship]", function() {
        if (this.value.toLowerCase() === "other") {
            jq(this).siblings("input[name=otherNokRelationship]").show().prop('required', true);
        } else {
            jq(this).siblings("input[name=otherNokRelationship]").val("");
            jq(this).siblings("input[name=otherNokRelationship]").hide().prop('required', false);
        }
    });

    function getTelNumberWithCode(elemId) {
        // console.log("extraPhoneInstances", extraPhoneInstances);
        let val = jq("#" + elemId).val();
        if (val) {
            let code = extraPhoneInstances[elemId].intlTelInput('getSelectedCountryData').dialCode;
            val = code + " " + val.replace(/\s/g, '');
            return val;
        }
        return "";
    }

    function getNokTelNumberWithCode(elemId) {
        let val = jq("#" + elemId).val();
        if (val) {
            let code = extraNokPhoneInstances[elemId].intlTelInput('getSelectedCountryData').dialCode;
            val = code + " " + val.replace(/\s/g, '');
            return val;
        }
        return "";
    }

    function toSentenceCase(inputString) {
        let result = inputString.replace(/([A-Z])/g, " $1");
        result = result.charAt(0).toUpperCase() + result.slice(1);
        return result;
    }

    // Gets a date value in numeric format and converts it to the "yyyy-MM-dd" format
    function numericToIsoDateFormat(dateValue) {
        let date = new Date(dateValue);

        // convert the date value t retrieved date format from the cookie value "defaultDateDormat" 
        let defaultDateFormat = webUtils.getCookieValue("defaultDateFormat");
        if (!defaultDateFormat) {
            defaultDateFormat = "yyyy-MM-dd";
        }
        let dateString = dateUtils.formatDate(date, defaultDateFormat);

        return dateString;
    }

    function validateEmail(email) {
        return email.trim().length === 0 || String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
    }

    function validateBirthNumber(birthNumber) {
        return String(birthNumber)
            .match(/^[a-zA-Z][a-zA-Z]\d\d\/[0-9]+\/\d\d\d\d$/);
    }

    function validateAgeWithBirthCertificate() {
        let age = jq("#age").val();
        if (patientIdType === "birthCertificateNumber" && age > 16) {
            patientIdType = "";
            jq("#btnBirthCertificate").removeClass("outline-button-active");
            jq('#idTypeError').show();
            idTypeValid = patientIdType !== "";
            return false;
        }

        return true;
    }

    function validateBirthYearWithCertificate(idValue) {
        return true;
        if (patientIdType === "birthCertificateNumber" && idValue !== "") {
            if (jq('#dateOfBirth').val() !== "") {
                let dob = new Date(jq('#dateOfBirth').val());
                let year = dob.getFullYear().toString();

                let extractedYear = idValue.substring(idValue.length - 4, idValue.length);
                if (extractedYear !== year) {
                    jq("#dateOfBirthError").text("Birth Year should match birth certificate year");
                    jq("#dateOfBirthError").show();
                    return false;
                } else {
                    jq("#dateOfBirthError").hide();
                }
            }
        }
        return true;
    }

    function clearAndDisable(elemId) {
        jq("#" + elemId).val("").prop("disabled", true);
    }

    function enableIfDisabled(elemId) {
        if (jq("#" + elemId).prop("disabled")) {
            jq("#" + elemId).prop("disabled", false);
        }
    }

    jq("#email").on("keyup", function() {
        const email = jq("#email").val();
        if (validateEmail(email)) {
            jq("#patientEmailError").hide();
        } else {
            jq("#patientEmailError").show();
        }
    });

    jq("#phoneNumber").on("keyup", function() {
        if (this.value.length > 0) {
            jq("#phoneNumberNa").hide();
            jq("#phoneNumberNaLabel").hide();

        } else {
            jq("#phoneNumberNa").show();
            jq("#phoneNumberNaLabel").show();
        }
    });

    jq("#phoneNumberNa").on("change", function() {
        if (this.checked) {
            jq("#address4").prop("required", true);
            jq("#phoneNumber").prop("required", false);
        } else {
            jq("#address4").prop("required", false);
            jq("#phoneNumber").prop("required", true);
        }
    });

    jq(".nokContactPersonType").on('click', 'input[type="checkbox"]', function() {
        let nokId = jq(this).attr('id');
        const nokIndex = isNaN(nokId.charAt(nokId.length - 1)) ? '' : nokId.charAt(nokId.length - 1);
        jq("#nextOfKinType" + nokIndex).not(this).prop('checked', false);
        jq("#contactPersonType" + nokIndex).not(this).prop('checked', false);
        jq("#treatmentBuddyType" + nokIndex).not(this).prop('checked', false);
        jq("#unspecifiedPersonType" + nokIndex).not(this).prop('checked', false);

        let selectedVal = '';
        jq('#nokContactPersonType' + nokIndex + ' input[type=checkbox]').each(function() {
            if (jq(this).is(":checked")) {
                selectedVal = jq(this).attr("id");
                if (nokIndex) {
                    selectedVal = selectedVal.slice(0, -1);
                }
                contactPersonType["contactPersonType" + nokIndex] = selectedVal;
            }
        });
        if (!selectedVal) {
            delete contactPersonType["contactPersonType" + nokIndex];
        }
    });

    jq('input[name="citizenType"').on("change", function() {
        let citizenType = jq('input[name="citizenType"]:checked').attr("data-attr");
        if (citizenType === "Non-citizen" && patientIdType !== "passportNumber" ||
            (citizenType === "Citizen" && patientIdType === "passportNumber")) {
            jq("#btnId").removeClass("outline-button-active");
            jq("#btnPassport").removeClass("outline-button-active");
            jq("#btnBirthCertificate").removeClass("outline-button-active");
            patientIdType = "";
            genderValid = true;
            resetPatientIdError();
        }

        if (citizenType === "Non-citizen") {
            jq("#fgCountry").show();
            jq('#btnPassport').show();
            jq("#btnId").hide();
            jq("#btnBirthCertificate").hide();

            //Patient info page
            jq("#passportNumberLabel").show();
            jq("#nationalIdLabel").hide();
            jq("#birthCertificateNumberLabel").hide();
        } else {
            jq("#fgCountry").hide();
            jq('#btnPassport').hide();
            jq("#btnId").show();
            jq("#btnBirthCertificate").show();

            //Patient info page
            jq("#passportNumberLabel").hide();
            jq("#nationalIdLabel").show();
            jq("#birthCertificateNumberLabel").show();
        }
    });

    function registerEmergencyPatient() {
        let array = jq("#name").val().split(" ");
        let givenName = "";
        let familyName = "";
        let middleName = "";
        let patientType = "";

        if (array.length < 2) {
            jq("#nameError").text("Enter First name and Last name");
            jq("#nameError").show();
        } else {
            jq("#nameError").hide();

            if (array.length === 2) {
                givenName = array[0];
                familyName = array[1];
                middleName = "";
            } else if (array.length > 2) {
                givenName = array[0];
                familyName = array[array.length - 1];
                middleName = array[1]
            }

            // patientIdType = jq('input[name="patientType"]:checked').val();

            let registrationData = {
                "patientType": "Emergency",
                "idType": patientIdType,
                "citizenType": jq('input[name="citizenType"]:checked').attr("data-attr"),
                "idNumber": jq("#patientId").val(),
                "emergencyPatientType": jq('input[name="patientType"]:checked').attr("data-attr"),
                "givenName": givenName,
                "middleName": middleName,
                "familyName": familyName,
                "gender": jq("#emergencyGender").val(),
                "dob": emergencyPatientDob,
                "dobEstimated": true,
                "email": "",
                "contactNumber": "",
                "occupation": "",
                "affiliation": "",
                "forceNumber": "",
                "rank": "",
                "maritalStatus": "",
                "education": "",
                "employmentSector": "",
                "otherEmploymentSector": "",
                "employerName": "",
                "country": "",
                "address2": "",
                "cityVillage": "",
                "address4": "",
                "address5": "",
                "address6": "",
                "address7": "",
                "patientFacilityLocation": jq("#facilityLocationField").val(),
                "nok": [{
                    "nokIdNumber": "",
                    "nokFullName": "",
                    "nokRelationship": "",
                    "nokContact": "",
                    "nokEmail": ""
                }]
            }
            submitPatientRegistration(registrationData, null);
        }
    }

    function registerQuickPatient() {
        let array = jq("#name").val().split(" ");
        let givenName = "";
        let familyName = "";
        let middleName = "";

        if (array.length < 2) {
            jq("#nameError").text("Enter First name and Last name");
            jq("#nameError").show();
        } else {
            jq("#nameError").hide();

            if (array.length === 2) {
                givenName = array[0];
                familyName = array[1];
                middleName = "";
            } else if (array.length > 2) {
                givenName = array[0];
                familyName = array[array.length - 1];
                middleName = array[1]
            }

            // patientIdType = jq('input[name="patientType"]:checked').val();

            // Retrieve the default date format from a saved cookie
            let defaultDateFormat = webUtils.getCookieValue('defaultDateFormat');
            if (!defaultDateFormat) {
                defaultDateFormat = "yyyy-MM-dd";
            }

            let dob = dateUtils.formatDate(jq("#dateOfBirth").datepicker('getDate'), defaultDateFormat);

            let registrationData = {
                "patientType": "Quick",
                "idType": patientIdType,
                "citizenType": jq('input[name="citizenType"]:checked').attr("data-attr"),
                "idNumber": jq("#patientId").val(),
                "emergencyPatientType": jq('input[name="patientType"]:checked').attr("data-attr"),
                "givenName": givenName,
                "middleName": middleName,
                "familyName": familyName,
                "gender": jq("#gender").val(),
                "dob": dob,
                "dobEstimated": true,
                "email": "",
                "contactNumber": "",
                "occupation": "",
                "affiliation": "",
                "forceNumber": "",
                "rank": "",
                "maritalStatus": "",
                "education": "",
                "employmentSector": "",
                "otherEmploymentSector": "",
                "employerName": "",
                "country": "",
                "address2": "",
                "cityVillage": "",
                "address4": "",
                "address5": "",
                "address6": "",
                "address7": "",
                "patientFacilityLocation": jq("#facilityLocationField").val(),
                "nok": [{
                    "nokIdNumber": "",
                    "nokFullName": "",
                    "nokRelationship": "",
                    "nokContact": "",
                    "nokEmail": ""
                }]
            }
            let returnUrl = jq("#returnUrl").val();
            submitPatientRegistration(registrationData, returnUrl, "quick");
        }
    }

    // Get Next of kin details from both main and extra nok inputs
    function getNOkDetails() {
        let nokData = [];
        nokData.push({
            "idType": nokIdType["nokIdType"],
            "nokIdNumber": jq("#nokIdNumber").val(),
            "nokFullName": jq("#nokFullName").val(),
            "nokRelationship": jq("#nokRelationship").val(),
            "otherNokRelationship": jq("#otherNokRelationship").val(),
            "nokContact": getNokTelNumberWithCode("nokContact"),
            "extraNokContact": getNokTelNumberWithCode("extraNokContact1_"),
            "thirdContact": getNokTelNumberWithCode("extraNokContact2_"),
            "fourthContact": getNokTelNumberWithCode("extraNokContact3_"),
            "fifthContact": getNokTelNumberWithCode("extraNokContact4_"),
            "nokEmail": jq("#nokEmail").val(),
            "contactPersonType": contactPersonType["contactPersonType"],
            "district": jq("#nokAddress2").val(),
            "city": jq("#nokCityVillage").val(),
        });
        if (nokRelationShips['0']) {
            nokRelationShips['0'].relationship = jq("#nokRelationship").val();
        }
        jq('#extraNoks').children().map(function() {
            let singleNokData = {}
                // Iterate through extra nok inputs and build json object
            jq(this).find('input, select').map(function() {
                let baseInputId = this.id.slice(0, -1);

                const nokIndex = this.id.charAt(this.id.length - 1);
                let elemId = jq(this).attr('id');
                if (baseInputId === "nokContact") {
                    singleNokData["nokContact"] = getNokTelNumberWithCode(elemId);
                } else if (baseInputId.startsWith("extraNokContact1")) {
                    singleNokData["extraNokContact"] = getNokTelNumberWithCode(elemId);
                } else if (baseInputId.startsWith("extraNokContact2")) {
                    singleNokData["thirdContact"] = getNokTelNumberWithCode(elemId);
                } else if (baseInputId.startsWith("extraNokContact3")) {
                    singleNokData["fourthContact"] = getNokTelNumberWithCode(elemId);
                } else if (baseInputId.startsWith("extraNokContact4")) {
                    singleNokData["fifthContact"] = getNokTelNumberWithCode(elemId);
                } else if (this.name.startsWith("nokContactPersonType")) {
                    singleNokData["contactPersonType"] = contactPersonType["contactPersonType" + nokIndex];
                } else if (this.name.startsWith("nokHomeAddress")) {
                    singleNokData["homeAddress"] = jq(this).val();
                } else if (this.name.startsWith("nokAddress2")) {
                    singleNokData["district"] = jq(this).val();
                } else if (this.name.startsWith("nokCityVillage")) {
                    singleNokData["city"] = jq(this).val();
                } else {
                    if (baseInputId) {
                        singleNokData[baseInputId] = jq(this).val();
                    } else {
                        console.log("this.name.", this.name);
                    }
                }

                if (this.name.startsWith("idNumber")) {
                    singleNokData["idType"] = nokIdType["nokIdType" + nokIndex];
                }
                if (this.name.startsWith("relationship") && nokRelationShips[nokIndex]) {
                    nokRelationShips[nokIndex].relationship = jq(this).val();
                }
            });
            nokData.push(singleNokData);
        });
        return nokData;
    }

    function registerRegularPatient() {
        const nokData = getNOkDetails();
        let relationShips = [];

        for (let key in nokRelationShips) {
            relationShips.push(nokRelationShips[key]);
        }

        let defaultDateFormat = webUtils.getCookieValue('defaultDateFormat');
        if (!defaultDateFormat) {
            defaultDateFormat = "yyyy-MM-dd";
        }

        let dob = jq("#dateOfBirth").val();

        let registrationData = {
            "patientType": "Regular",
            "idType": patientIdType,
            "citizenType": jq('input[name="citizenType"]:checked').attr("data-attr"),
            "idNumber": jq("#patientId").val(),
            "givenName": jq("#givenName").val(),
            "middleName": jq("#middleName").val(),
            "familyName": jq("#familyName").val(),
            "gender": jq("#gender").val(),
            "dob": dob,
            "dobEstimated": jq("#dobEstimate").prop('checked') === true,
            "email": jq("#email").val(),
            "contactNumber": getTelNumberWithCode("phoneNumber"),
            "numberType": jq("#numberType").val(),
            "occupation": jq("#occupation").val(),
            "otherOccupation": jq("#otherOccupation").val(),
            "affiliation": jq("#affiliation").val(),
            "forceNumber": jq("#forceNumber").val(),
            "rank": jq("#rank").val(),
            "maritalStatus": jq("#maritalStatus").val(),
            "education": jq("#education").val(),
            "employmentSector": jq("#employmentSector").val(),
            "otherEmploymentSector": jq("#otherEmploymentSector").val(),
            "employerName": jq("#employer").val(),
            "country": jq("#country").val(),
            "address2": jq("#address2").val(),
            "cityVillage": jq("#cityVillage").val(),
            "address4": jq("#address4").val(),
            "address5": jq("#address5").val(),
            "address6": jq("#otherDistrict").val(),
            "address7": jq("#address7").val(),
            "patientFacilityLocation": jq("#facilityLocationField").val(),
            "nok": nokData,
            "altContactNumber": getTelNumberWithCode("phoneNumber1"),
            "altContactNumber2": getTelNumberWithCode("phoneNumber2"),
            "altContactNumber3": getTelNumberWithCode("phoneNumber3"),
            "altContactNumber4": getTelNumberWithCode("phoneNumber4"),
            "altContactNumber5": getTelNumberWithCode("phoneNumber5"),
            "altNumberType1": jq("#extraNumberType1").val(),
            "altNumberType2": jq("#extraNumberType2").val(),
            "altNumberType3": jq("#extraNumberType3").val(),
            "altNumberType4": jq("#extraNumberType4").val(),
            "altNumberType5": jq("#extraNumberType5").val(),
            "relationShips": relationShips
        }
        submitPatientRegistration(registrationData, null);
    }

    function getLocationFromUrl() {
        let urlParams = new URLSearchParams(window.location.search);
        let location = urlParams.get('location');
        return location ? decodeURIComponent(location) : null;
    }

    function submitPatientRegistration(registrationData, returnUrl, action) {
        let settings = {
            "url": PATIENT_REGISTRATION_URL,
            "method": "POST",
            "timeout": 8000,
            "headers": {
                "Content-Type": "application/json",
            },
            "data": JSON.stringify(registrationData),
        };

        // Show loading overlay
        showLoadingOverlay();

        $.ajax(settings).done(function(response) {
            if (response.status && response.status.indexOf('200') > -1) {
                let patientUuid = response.response.Patient.uuid;

                let location = getLocationFromUrl();

                // console.log('location', location);
                formUtils.clearCachedData('formPatientDetails');

                // if mpiSearch is enabled, create record in CR
                let mpiSearchEnabled = webUtils.getCookieValue('mpiSearchEnabled');
                if (mpiSearchEnabled) {
                    //Async call to openCR to create new record.
                    $.ajax({
                        url: OPENCR_CREATE_RECORD_URL,
                        method: "POST",
                        timeout: 8000,
                        headers: {
                            "Content-Type": "application/json",
                        },
                        data: JSON.stringify({
                            patientUuid: patientUuid,
                            location: location
                        }),
                    }).done(function(openCrResponse) {
                        console.log('openCrResponse', openCrResponse);
                        if (openCrResponse.status && (openCrResponse.status === '201 CREATED')) {
                            console.log("Record posted to CR successfully");
                        } else if (openCrResponse.results && openCrResponse.status === 'BAD REQUEST') {
                            console.log(alert("Error in OpenCR: " + openCrResponse.results));
                        }
                    }).fail(function(openCrResponse) {
                        console.log("Failed to create record in OpenCR: " + openCrResponse.results);
                    });
                }

                if (registrationData.patientType === 'Regular') {
                    window.location.href = "capturePayment.page?patientId=" + patientUuid;
                } else {
                    captureDelayedPayment(patientUuid).then(function(result) {
                        if (returnUrl !== undefined && returnUrl !== null && returnUrl !== "") {
                            window.location.href = "assignPatient.page?patientId=" + patientUuid + "&startVisit=true&returnUrl=" + returnUrl + "&action=" + action;
                        } else {
                            window.location.href = "assignPatient.page?patientId=" + patientUuid + "&startVisit=true";
                        }
                    });
                }
            } else if (response.message) {
                console.log("Error: " + response.message);
            } else {
                // Display error
                console.log("Error: " + response.response);
            }
        }).fail(function(response) {
            console.log("Failed: " + response.statusText);
            // Toast message
            hideLoadingOverlay();
            jq().toastmessage('showErrorToast', 'Something went wrong Failed to register patient');
        });
    }

    $('#btnPrevious').on('click', function() {
        jq(".sw-btn-prev").trigger("click");
        jq("#btnNext").text("Next step");
        jq('#content').animate({ scrollTop: 0 }, "fast");
    })

    jq('#btnNext').on('click', function() {
        const hash = window.location.hash;

        const email = jq("#email").val();
        let emailValidation = validateEmail(email);
        if (hash.endsWith('-3') && emailValidation) {
            registerRegularPatient();
        } else if (hash.endsWith('-1') && $("#formPatientDetails").valid() && emailValidation && genderValid && validateBirthYearWithCertificate(jq("#patientId").val()) && !registered && idTypeValid &&
            validateAgeWithBirthCertificate()) {
            jq(".sw-btn-next").trigger("click");
            jq("#btnNext").text("Next step");
            jq('#content').animate({ scrollTop: 0 }, "fast");
        } else if (hash.endsWith('-2') && $("#formNOK").valid() && emailValidation && nokIdValidation.length === 0) {
            jq(".sw-btn-next").trigger("click");
            jq("#btnNext").text("Save & Register");
            setNokDetails();
            jq('#content').animate({ scrollTop: 0 }, "fast");
        }
        if (!idTypeValid) {
            jq().toastmessage('showWarningToast', 'Select patient identification type.');
        }
    })

    jq("#formPatientDetails :input").change(function() {
        let inputs = jq('#formPatientDetails :input');
        let formData = {};

        formData["identificationType"] = toSentenceCase(patientIdType);
        inputs.each(function() {
            if (jq(this).val() !== "" && jq(this).val() !== null) {
                if (jq(this).attr("id") === "education") {
                    formData[this.name] = jq("#education option:selected").text();
                } else if (jq(this).attr("id") === "employmentSector") {
                    formData[this.name] = jq("#employmentSector option:selected").text();
                } else {
                    formData[this.name] = jq(this).val();
                }

            }
        });
        formData["citizenType"] = jq('input[name="citizenType"]:checked').attr("data-attr");
        jq('#generalDetails').empty();
        for (const val in formData) {
            jq('#generalDetails').append('<p>Patient\'s ' + toSentenceCase(val) + ':   ' + '<strong>' + formData[val] + '</strong></p>')
        }
    });

    function captureDelayedPayment(patientId) {
        const url = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/capturePayment/postDelayedPayment.action';
        let params = {
            patientId: patientId
        }

        return $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: params,
            success: function(response) {
                return response;
            },
            error: function(response) {
                return response
            }
        });
    }

    function setNokDetails() {
        jq('#nokDetails').empty();
        let nokDetails = getNOkDetails();
        let filteredNokDetails = {};
        for (let i in nokDetails) {
            let rel = nokDetails[i].nokRelationship;
            if (rel) {
                if (!filteredNokDetails[rel]) {
                    filteredNokDetails[rel] = [];
                }
                filteredNokDetails[rel].push(nokDetails[i]);
            }

        }

        for (let key in filteredNokDetails) {
            jq('#nokDetails').append('<p><strong>Contact person: ' + key + '</strong></p>');
            filteredNokDetails[key].map(function(nokDetail) {
                for (let dataKey in nokDetail) {
                    if (nokDetail.hasOwnProperty(dataKey) && nokDetail[dataKey]) {
                        let key = dataKey.replace(/(^nok)/gi, "");
                        if (key === "contactPersonType") {
                            let val = toSentenceCase(nokDetail[dataKey]).replace("Type", '');
                            jq('#nokDetails').append('<p class="nok-group">Contact person Type : ' + '<strong>' + val + '</strong></p>');
                        } else {
                            jq('#nokDetails').append('<p class="nok-group">Contact person ' + toSentenceCase(key) + ': ' + '<strong>' + nokDetail[dataKey] + '</strong></p>');
                        }
                    }
                }
                jq('#nokDetails').append('<hr class="separator nok-group"">');
            });
        }
    }

    function setInputVal(id, val) {
        $(id).val(val);
        $(id).prop("disabled", true);
        $(id).valid();
    }

    function extractNames(fullName) {
        let array = fullName.split(" ");
        console.log("namesSet", namesSet);
        if (!namesSet) {
            if (array.length === 1) {
                setInputVal("#givenName", array[0]);
            } else if (array.length === 2) {
                setInputVal("#givenName", array[0]);
                setInputVal("#familyName", array[1]);
            } else if (array.length > 2) {
                setInputVal("#givenName", array[0]);
                setInputVal("#familyName", array[array.length - 1]);
                setInputVal("#middleName", array[1]);
            }
        }
    }

    function setInputValEditable(id, val) {
        $(id).val(val);
        $(id).prop("disabled", false);
        $(id).valid();
    }

    function resetInputs() {
        jq("#formPatientDetails :input").prop("disabled", false);
        if (!namesSet) {
            jq('#givenName').val("");
            jq('#middleName').val("");
            jq('#familyName').val("");
        }
        if (!genderSet) { jq('#gender').val(""); }
        jq('#age').val("");
        jq('#dateOfBirth').val("");
    }

    function resetNokInputs(nokIndex) {
        jq("#nokFullName" + nokIndex).prop("disabled", false).val("");
        jq("#nokEmail" + nokIndex).prop("disabled", false).val("");
        jq("#nokContact" + nokIndex).prop("disabled", false).val("");
    }

    function validateGender() {
        if (jq("#patientId").val().length >= 5) {
            const genderVal = jq("#patientId").val().charAt(4);
            const gender = jq("#gender").val();
            if (["nationalIdNumber", "birthCertificateNumber"].includes(patientIdType)) {
                if (genderVal < 1 || genderVal > 2) {
                    jq("#genderError").text("Invalid ID number. The fifth number ID should be 1 or 2").show();
                    jq("#patientIdError").text("Invalid ID number. The fifth number ID should be 1 or 2").show();
                    genderValid = false;
                    return;
                }
                if (gender) {
                    if (genderVal === '1' && gender !== "Male") {
                        jq("#genderError").text("Gender should be Male. Please check if ID number is correct.").show();
                        genderValid = false;
                    } else if (genderVal === '2' && gender !== "Female") {
                        jq("#genderError").text("Gender should be Female. Please check if ID number is correct.").show();
                        genderValid = false;
                    } else {
                        genderValid = true;
                        jq("#genderError").hide();
                        jq("#patientIdError").hide();
                    }
                } else {
                    genderValid = true;
                    jq("#genderError").hide();
                    jq("#patientIdError").hide();
                }
            } else {
                genderValid = true;
                jq("#genderError").hide();
                jq("#patientIdError").hide();
            }
        } else {
            genderValid = true;
            jq("#genderError").hide();
            jq("#patientIdError").hide();
        }
    }

    function setGender() {
        const genderVal = jq("#patientId").val().charAt(4);
        if (genderVal === '1') {
            jq("#gender").val('Male');
            genderSet = true;
        } else if (genderVal === '2') {
            jq("#gender").val('Female');
            genderSet = true;
        } else {
            jq("#gender").val('');
            genderSet = false;
        }
    }

    function checkLocalRegistrationStatus(idType, idValue) {
        const url = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/getPatientsByIdentifier.action';
        let params = {
            idType: idType,
            idValue: idValue
        }
        jq("#patientSpinner").show();
        jq("#patientIdError").text(SEARCH_LOCAL_DATABASE_TEXT);
        jq("#patientIdError").show();

        return $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            data: params,
            success: function(data) {
                jq("#patientIdError").hide();
                if (data.patientId > 0) {
                    jq("#patientIdError").text(ID_LOCAL_SEARCH_FOUND);
                    jq("#patientIdError").show();
                    jq("#formPatientDetails :input:not(.default-input)").attr("disabled", true);
                    jq("#existing").show();

                    let existingPatientUrl = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/confirmPatient.page?patientId=' + data.patientUuid + '&action=' + '&returnUrl=';
                    jq("#existing").attr("href", existingPatientUrl)
                } else {
                    jq("#formPatientDetails :input:not(.default-input)").attr("disabled", false);
                    if (!namesSet) {
                        jq("#formPatientDetails :input:not(.default-input)").val("");
                    } else {
                        jq("#formPatientDetails :input:not(.default-input,.name-input)").val("");
                    }
                    jq("#existing").attr("href", "#")
                }
                jq("#patientSpinner").hide();
            },
            error: function(response) {
                jq("#patientSpinner").hide();
            }
        });
    }

    jq("#patientId").on("keyup", function() {
        let idValue = jq(this).val();
        jq("#existing").hide();
        jq("#patientIdError").hide();

        if (patientIdType !== "" && idValue.length >= 8) {
            let getRegisteredStatus = checkLocalRegistrationStatus(patientIdType, idValue);

            getRegisteredStatus.then(function(data) {
                    registered = data.patientId > 0;
                    if (!registered) {
                        validateGender();
                        if (genderValid) {
                            setGender();
                            jq("#patientSpinner").show();
                            jq("#patientIdError").show();
                            jq("#patientIdError").text(SEARCH_REGISTRY_TEXT);
                            const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/' + patientIdType + '/' + idValue;

                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: "json",
                                timeout: 15000,
                                success: function(response) {
                                    if (response.status.indexOf('200') > -1) {
                                        offline = false;
                                        jq("#patientIdError").hide();
                                        if (response.response.length > 0) {
                                            const patientDetails = response.response[0];

                                            if (patientDetails.fullName !== "") {
                                                extractNames(patientDetails.fullName);
                                            }
                                            if (patientDetails.gender !== "") {
                                                setInputVal("#gender", patientDetails.gender);
                                            }
                                            if (patientDetails.age !== "") {
                                                setInputVal("#age", patientDetails.age);
                                            }
                                            if (patientDetails.dateOfBirth !== "") {
                                                if ($.isNumeric(patientDetails.dateOfBirth)) {
                                                    setInputVal("#dateOfBirth", numericToIsoDateFormat(patientDetails.dateOfBirth));
                                                } else {
                                                    setInputVal("#dateOfBirth", patientDetails.dateOfBirth);
                                                }
                                            }
                                        }
                                    } else {
                                        offline = true;
                                        resetInputs();
                                        jq("#patientIdError").text(ID_NOT_FOUND);
                                        jq("#patientIdError").show();
                                        validateGender();
                                    }

                                    jq("#patientSpinner").hide();
                                },
                                error: function(response) {
                                    resetInputs();
                                    jq("#patientIdError").text(ID_NOT_FOUND);
                                    jq("#patientIdError").show();
                                    jq("#patientSpinner").hide();
                                }
                            });
                        }
                    }
                })
                .fail(function(error) {
                    console.log(error);
                });

        } else if (patientIdType === "") {
            jq('#idTypeError').show();
        }
    });

    // if numberType is MOBILE, restrict phoneNumber to 7 digits otherwise 8 digits
    jq("#numberType").on("change", function() {
        if (jq("#numberType").val() === "Mobile") {
            jq("#phoneNumber").attr("maxlength", "8");
        } else {
            jq("#phoneNumber").attr("maxlength", "7");
        }
    });

    //advanced search
    $(document).ready(function() {
        $("#advancedPatientSearch").on("submit", function(event) {
            event.preventDefault();

            let idValue = $("#patientIdAdvanced").val();
            let firstName = $("#advancedGivenName").val();
            let lastName = $("#advancedFamilyName").val();
            let dateOfBirth = $("#advancedDateOfBirth").val();
            let gender = $("#advancedGender").val();

            const payload = {
                identifierType: patientIdType,
                idNumber: idValue || "",
                firstName: firstName || "",
                lastName: lastName || "",
                dateOfBirth: dateOfBirth || "",
                gender: gender || " "
            };
            const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/getPatientByAdvancedSearch';
            $("#advancedPatientSearchSpinner").show();
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=UTF-8",
                data: JSON.stringify(payload),
                success: function(response) {
                    $("#patientDataTable tbody").empty();
                    $("#advancedPatientSearchSpinner").hide();
                    if (response.status === "404 NOT_FOUND") {
                        //patient not found in the openCR:
                        $("#patientIdErrorAdvanced").show();
                        $("#existingAdvancedSearch").show();
                        jq().toastmessage('showWarningToast', 'Patient not found in Client Registry.');
                    } else {
                        response.response.forEach(function(patientData) {
                            let [firstName, lastName] = (patientData.fullName || 'N/A').split(' ', 2);

                            var tableRow = `
                                 <tr>
                                     <td>${patientData.idNumber || 'N/A'}</td>
                                     <td>${firstName || 'N/A'}</td>
                                     <td>${lastName || 'N/A'}</td>
                                     <td>${new Date(patientData.dateOfBirth).toLocaleDateString() || 'N/A'}</td>
                                     <td>${patientData.gender || 'N/A'}</td>
                                     <td><a href="#" class="btn btn-primary text-white select-patient" data-idnumber="${patientData.idNumber}" data-firstname="${firstName}" data-lastname="${lastName}" data-dob="${patientData.dateOfBirth}" data-gender="${patientData.gender}" data-dismiss="modal">Select</a></td>
       
                                 </tr>
                             `;
                            $("#patientDataTable tbody").append(tableRow);
                        });

                        $("#patientDataTableContainer").show();
                        $('html, body').animate({
                            scrollTop: $("#patientDataTableContainer").offset().top
                        }, 1000);
                    }
                },
                error: function(xhr, status, error) {
                    $("#advancedPatientSearchSpinner").hide();
                    console.warn('Failed to communicate with the client registry');
                    jq().toastmessage('showErrorToast', 'Failed to communicate with client registry. Please try again.');
                }
            });
        });

        // Repopulate form data when the page loads
        //formUtils.repopulateFormData('formPatientDetails');
        //clear cached data
        //formUtils.clearCachedData('formPatientDetails');

        $('#permanentResidentialAddress').on('change', function() {
            if ($(this).is(':checked')) {
                $('#permanentResidential').slideDown();
            } else {
                $('#permanentResidential').slideUp();
            }
        });

        $('#currentResidentialAddress').on('change', function() {
            if ($(this).is(':checked')) {
                $('#currentResidential').slideDown();

                const permanentAddress = $('#permanentAddressDetails').val();
                if (permanentAddress) {
                    $('#currentAddressDetails').val(permanentAddress);
                }
            } else {
                $('#currentResidential').slideUp();
            }
        });
    });

    //populate form fields with data from openCR for enrichment
    $(document).on('click', '.select-patient', function() {
        var idNumber = $(this).data('idnumber');
        var firstName = $(this).data('firstname');
        var lastName = $(this).data('lastname');
        var dob = $(this).data('dob');
        var gender = $(this).data('gender');

        $('#patientId').val(idNumber);
        $('#givenName').val(firstName);
        $('#familyName').val(lastName);
        $('#dateOfBirth').val(new Date(dob).toISOString().split('T')[0]);
        $('#gender').val(gender);
    });


    jq("#gender").change(function() {
        if (offline) {
            validateGender();
        }
    });

    let searchParam = getUrlParameter('searchParam');

    if (searchParam.length > 0) {
        if (jq.isNumeric(searchParam)) {
            jq("#patientId").val(searchParam);
            jq("#patientId").trigger('keyup');
        } else {
            // Split the text into words
            let words = searchParam.split(' ');
            if (words.length > 0) jq('#givenName').val(words[0]);
            if (words.length === 3) jq('#middleName').val(words[1]);
            if (words.length > 1) jq('#familyName').val(words[words.length - 1]);
            namesSet = words.length > 0;
        }
    }

    function ageValidations() {
        let age = jq("#age").val();
        if (age <= 16) {
            clearAndDisable("occupation");
            clearAndDisable("employer");
            clearAndDisable("nameOfEmployer");
            clearAndDisable("employmentSector");
            clearAndDisable("otherEmploymentSector");
            clearAndDisable("maritalStatus");
        } else {
            enableIfDisabled("occupation");
            enableIfDisabled("employer");
            enableIfDisabled("nameOfEmployer");
            enableIfDisabled("employmentSector");
            enableIfDisabled("otherEmploymentSector");
            enableIfDisabled("maritalStatus");
        }
    }

    function diffInYearsMonthsDays(dateFrom, dateTo) {
        var from = {
            d: dateFrom.getDate(),
            m: dateFrom.getMonth(),
            y: dateFrom.getFullYear()
        };

        var to = {
            d: dateTo.getDate(),
            m: dateTo.getMonth(),
            y: dateTo.getFullYear()
        };

        var age = {
            d: 0,
            m: 0,
            y: 0
        };

        var daysFebruary = (from.y % 4 === 0 && from.y % 100 !== 0) || from.y % 400 === 0 ? 29 : 28;
        var daysInMonths = [31, daysFebruary, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        age.y = to.y - from.y;
        age.m = to.m - from.m;

        if (from.m > to.m) {
            age.y = age.y - 1;
            age.m = to.m - from.m + 12;
        }
        age.d = to.d - from.d;

        if (from.d > to.d) {
            age.m = age.m - 1;

            if (from.m == to.m) {
                age.y = age.y - 1;
                age.m = age.m + 12;
            }
            age.d = to.d - from.d + daysInMonths[parseInt(from.m)];
        }
        return {
            days: age.d,
            months: age.m,
            years: age.y
        };
    }

    jq("#dateOfBirth").change(function() {
        let dob = jq(this).datepicker('getDate');
        if (dob !== "") {
            dob = new Date(dob);

            let ageDiff = diffInYearsMonthsDays(dob, new Date());

            jq("#age").val(ageDiff.years);
            jq('#ageMonth').val(ageDiff.months);
            jq('#ageWeeks').val(Math.trunc(ageDiff.days / 7));

            ageValidations();
        }

        const patientIdVal = jq("#patientId").val();
        validateBirthYearWithCertificate(patientIdVal);
    });


    jq('#age, #ageMonth, #ageWeeks').change(function() {
        let ageYears = jq('#age').val();
        let ageMonths = jq('#ageMonth').val();
        let ageWeeks = jq('#ageWeeks').val();
        if (!ageYears) {
            jq('#age').val(0)
        }

        let today = new Date();
        today.setFullYear(today.getFullYear() - ageYears)

        if (jq('#dobEstimate').is(':checked')) {
            // If the user estimates the age, we will set the date of birth to the first day of the month
            if (!ageMonths) {
                today.setFullYear(today.getFullYear() - 1);
                today.setMonth(12);
                today.setDate(1);
            } else {
                today.setDate(1);
                if (ageMonths) today.setMonth(today.getMonth() - ageMonths);
            }
        } else {
            if (ageMonths) today.setMonth(today.getMonth() - ageMonths);
        }
        if (ageWeeks) today.setDate(today.getDate() - (ageWeeks * 7));

        jq("#dateOfBirth").datepicker('setDate', today);
        jq("#dobEstimate").prop('checked', true);

        const patientIdVal = jq("#patientId").val();
        validateBirthYearWithCertificate(patientIdVal);

        ageValidations();
    });

    jq('#age').keypress(function(e) {
        jq("#dobEstimate").prop('checked', true);
    });

    function searchNokIdLocally(idType, idValue, nokIndex) {

        jq("#nokSpinner" + nokIndex).show();
        jq("#nokIdError" + nokIndex).text(SEARCH_LOCAL_DATABASE_TEXT);
        jq("#nokIdError" + nokIndex).show();

        const url = '/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/getPatientsByIdentifier.action';
        let params = {
            idType: idType,
            idValue: idValue
        }

        return $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            data: params
        });
    }

    function searchNokInRegistry(nokIndex, idValue, idType) {
        jq("#nokSpinner" + nokIndex).show();
        jq("#nokIdError" + nokIndex).text(SEARCH_REGISTRY_TEXT);
        jq("#nokIdError" + nokIndex).show();

        const url = '/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/registry/' + idType + '/' + idValue;

        $.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            timeout: 5000,
            success: function(response) {
                if (response.status.indexOf('200') > -1) {
                    jq("#nokIdError" + nokIndex).hide();
                    if (response.response.length > 0) {
                        const nokDetails = response.response[0];

                        if (nokDetails.fullName !== "") {
                            setInputVal("#nokFullName" + nokIndex, nokDetails.fullName);
                        }
                    }
                } else {
                    resetNokInputs(nokIndex);
                    jq("#nokIdError" + nokIndex).text(NOK_ID_NOT_FOUND);
                    jq("#nokIdError" + nokIndex).show();
                }
                jq("#nokSpinner" + nokIndex).hide();
            },
            error: function(response) {
                resetNokInputs(nokIndex);
                jq("#nokIdError" + nokIndex).text(NOK_ID_NOT_FOUND);
                jq("#nokIdError" + nokIndex).show();
                jq("#nokSpinner" + nokIndex).hide();
            }
        });
    }

    function validateNokIdNumber(nokIndex, idValue) {
        if (idValue.length > 4) {
            if (nokIdType["nokIdType" + nokIndex] !== "passportNumber") {
                const genderVal = idValue.charAt(4);
                const isValid = !(genderVal < 1 || genderVal > 2);

                if (!isValid) {
                    jq("#nokIdError" + nokIndex).text(INVALID_ID_NUMBER);
                    jq("#nokIdError" + nokIndex).show();
                    if (!nokIdValidation.includes(nokIndex)) {
                        nokIdValidation.push(nokIndex);
                    }
                } else {
                    resetNokIdError(nokIndex);
                }
                return isValid;
            } else {
                resetNokIdError(nokIndex);
                return true;
            }
        }
        resetNokIdError(nokIndex);
        return true;
    }

    jq("#nokIdNumber").on("keyup", function(e) {
        const nokId = e.target.id;
        const nokIndex = isNaN(nokId.charAt(nokId.length - 1)) ? '' : nokId.charAt(nokId.length - 1);
        let idValue = jq(this).val();
        let valid = validateNokIdNumber(nokIndex, idValue);

        if (valid && nokIdType["nokIdType" + nokIndex] !== "" && $(this).val().length >= 9) {
            let getRegisteredStatus = searchNokIdLocally(nokIdType["nokIdType" + nokIndex], idValue, nokIndex);

            getRegisteredStatus.then(function(data) {
                if (data.patientId > 0) {
                    if (nokIndex === '') {
                        nokRelationShips['0'] = {
                            "patientId": data.patientId
                        }
                    } else {
                        nokRelationShips[nokIndex] = {
                            "patientId": data.patientId
                        }
                    }
                    // Local patient found
                    jq("#nokIdError" + nokIndex).hide();
                    if (data.fullName !== "") {
                        setInputVal("#nokFullName" + nokIndex, data.fullName);
                    }
                    if (data.email !== "") {
                        setInputValEditable("#nokEmail" + nokIndex, data.email);
                    }
                    if (data.phoneNumber !== "") {
                        if (data.phoneNumber.startsWith("267")) {
                            let strippedNumber = data.phoneNumber.replace(/^267/, "");
                            strippedNumber = strippedNumber.slice(-8);
                            setInputValEditable("#nokContact" + nokIndex, strippedNumber);
                        } else {
                            let strippedNumber = data.phoneNumber.slice(-8);
                            setInputValEditable("#nokContact" + nokIndex, strippedNumber);
                        }
                    }

                    if (data.nokAddress1 !== "") {
                        jq("#nokCityVillage").val(data.nokAddress2);
                    }

                    if (data.nokCity !== "") {
                        jq("#nokCityVillage").val(data.nokCity);
                    }
                    if (data.nokAddress2 !== "") {
                        if (jq("#nokAddress2 option[value='" + data.nokAddress2 + "']").length === 0) {
                            jq("#nokAddress2").append(
                                "<option value='" + data.nokAddress2 + "'>" + data.nokAddress2 + "</option>"
                            );
                        }
                        jq("#nokAddress2").val(data.nokAddress2);
                    }

                    jq("#nokSpinner" + nokIndex).hide();
                } else {
                    // Local ID not found.Search in registry
                    searchNokInRegistry(nokIndex, idValue, nokIdType["nokIdType" + nokIndex]);
                }
            }).fail(function(error) {
                // Error on local search. Search in registry
                searchNokInRegistry(nokIndex, idValue, nokIdType["nokIdType" + nokIndex]);
            });

        } else if (nokIdType["nokIdType" + nokIndex] === "") {
            jq('#nokIdTypeError' + nokIndex).show();
        }
    });

    jq("#btnAddContact").on("click", function(e) {
        e.preventDefault();
        let len = jq('.extraContact').length + 1;


        if (len < MAX_PATIENT_CONTACTS) {
            let contactIds = [];
            $(".extraContact").find("input").each(function() { contactIds.push(this.id); });
            contactIds.sort((a, b) => a.localeCompare(b));

            let contactIndex = '';
            for (let i = 1; i < 6; i++) {
                let temp = "phoneNumber" + i;
                if (!contactIds.includes(temp)) {
                    contactIndex = i;
                    break;
                }
            }

            let template = jq(".contact-template").children().clone(true, true);

            template.find('[name=extraPhone]')[0].name = "phoneNumber" + contactIndex;
            template.find('[id=extraPhone]')[0].id = "phoneNumber" + contactIndex;

            template.find('[id=extraNumberType]')[0].id = "extraNumberType" + contactIndex;
            template.find('[name=extraNumberType]')[0].name = "extraNumberType" + contactIndex;

            template.find('[id=extraDelete]')[0].id = "deletePhone" + contactIndex;
            jq(template.find('[id=extraPhoneError]')[0]).attr("for", "phoneNumber" + contactIndex);
            template.find('[id=extraPhoneError]')[0].id = "phoneNumber" + contactIndex + "-error";


            jq('<div/>', {
                'class': 'extraContact',
                'id': 'extraContact',
                html: template
            }).hide().appendTo('#extraContacts').slideDown('slow');

            extraPhoneInstances["phoneNumber" + contactIndex] = jQuery("#phoneNumber" + contactIndex);
            extraPhoneInstances["phoneNumber" + contactIndex].intlTelInput({
                initialCountry: "bw",
                hiddenInput: "phoneNumber" + contactIndex + "Code",
            });

            jq("#phoneNumber" + contactIndex).on("countrychange", function() {
                let code = extraPhoneInstances["phoneNumber" + contactIndex].intlTelInput('getSelectedCountryData').dialCode;
                if (code === '267') {
                    jq("#phoneNumber" + contactIndex).attr("maxlength", "8");
                } else {
                    jq("#phoneNumber" + contactIndex).attr("maxlength", "15");
                }
            });
        }
    });

    jq('#extraContacts').on("click", function(e) {
        const element = e.target.nodeName;

        // Delete icon clicked
        if (element.toLowerCase() === "i") {
            jq(e.target).closest('#extraContact').remove();
            jq("#formPatientDetails :input").trigger("change");
        }
    });

    jq(".btn-address").on("click", function(e) {
        e.preventDefault();
        let targetId = jq(this).attr('id');
        const nokIndex = isNaN(targetId.charAt(targetId.length - 1)) ? '' : targetId.charAt(targetId.length - 1);
        jq("#nokAddress2" + nokIndex).val(jq("#address2").val());
        jq("#nokCityVillage" + nokIndex).val(jq("#cityVillage").val());
    });

    jq("#btnAddNok").on("click", function(e) {
        e.preventDefault();
        const len = jq('.extraNok').length + 1;
        nokPositionIndex = nokPositionIndex + 1;

        if (len < MAX_NEXT_OF_KINS) {
            let template = jq("#nexOfKin").children().clone(true, true);

            template.find('[id=nokIdError]').map(function() {
                jq(this).attr("id", this.id + nokPositionIndex).hide()
            });
            template.find('input').val('');
            template.find('input, select').map(function() {
                jq(this).attr("id", this.id + nokPositionIndex).attr("name", this.name + nokPositionIndex);
            });
            template.find('[id=nokSpinner]')[0].id = "nokSpinner" + nokPositionIndex;
            template.find('[id=nokIdNumberLabel]')[0].id = "nokIdNumberLabel" + nokPositionIndex;
            template.find('[id=nokIdTypeError]')[0].id = "nokIdTypeError" + nokPositionIndex;
            template.find('[id=nokBtnId]')[0].id = "nokBtnId" + nokPositionIndex;
            template.find('[id=nokBtnPassport]')[0].id = "nokBtnPassport" + nokPositionIndex;
            // template.find('[id=nokBtnBirthCertificate]')[0].id = "nokBtnBirthCertificate" + nokPositionIndex;
            template.find('[id=nextOfKinEmailError]')[0].id = "nextOfKinEmailError" + nokPositionIndex;
            template.find('[id=btnAddress]')[0].id = "btnAddress" + nokPositionIndex;
            template.map(function(key, element) {
                if (jq(element).attr('id') === "extraNokContact") {
                    jq(template[key]).empty();
                }
                if (jq(element).attr('id') === "extraNokContact" || jq(element).attr('id') === "btnAddNokContact") {
                    template[key].id = jq(element).attr('id') + nokPositionIndex;
                }
                if (jq(element).attr('id') === "nokContactPersonType") {
                    template[key].id = jq(element).attr('id') + nokPositionIndex;
                }
            });

            const extraNokHeader = "<hr class=\"separator\"/>\n" +
                " <div class=\"text-icon-div\" id=\"nok-header\">\n" +
                "   <h6 class=\"h6\"><strong>Additional contact person </strong></h6>\n" +
                "   <button class=\"mini-button rounded\" id=\"btnRemoveNok" + nokPositionIndex + "\" '>Remove</button>\n" +
                " </div>";

            jq('<div/>', {
                'class': 'extraNok',
                'id': 'extraNok',
                html: template
            }).hide().appendTo('#extraNoks').prepend(extraNokHeader).slideDown('slow');

            // Select ID number by default
            nokIdType["nokIdType" + nokPositionIndex] = "nationalIdNumber";

            jq("#nokContact" + nokPositionIndex).attr("maxlength", "8");
            extraNokPhoneInstances["nokContact" + nokPositionIndex] = jQuery("#nokContact" + nokPositionIndex);
            extraNokPhoneInstances["nokContact" + nokPositionIndex].intlTelInput({
                initialCountry: "bw",
                hiddenInput: "nokContact" + nokPositionIndex + "Code",
            });

            jq("#nokContact" + nokPositionIndex).on("countrychange", function() {
                let code = extraNokPhoneInstances["nokContact" + nokPositionIndex].intlTelInput('getSelectedCountryData').dialCode;
                if (code === '267') {
                    jq("#nokContact" + nokPositionIndex).attr("maxlength", "8");
                } else {
                    jq("#nokContact" + nokPositionIndex).attr("maxlength", "15");
                }
            });
        }
    });

    jq(".add-nok-contact").on("click", function(e) {
        e.preventDefault();
        let buttonId = jq(this).attr('id');

        let extraNokContact = jq("#" + buttonId).prev();

        const nokId = extraNokContact.attr('id');

        let len = extraNokContact[0].children.length + 1;
        const nokIndex = isNaN(nokId.charAt(nokId.length - 1)) ? '' : nokId.charAt(nokId.length - 1);

        if (len < MAX_PATIENT_CONTACTS) {
            let template = jq(".nok-contact-template").children().clone(true, true);
            let trueIndex = len + "_" + nokIndex;

            template.find('[name=extraNokContact]')[0].name = "extraNokContact" + trueIndex;
            template.find('[id=extraNokContact]')[0].id = "extraNokContact" + trueIndex;
            template.find('[id=extraNokContactDelete]')[0].id = "extraNokContactDelete" + trueIndex;
            jq(template.find('[id=extraNokContactError]')[0]).attr("for", "extraNokContact" + trueIndex);
            template.find('[id=extraNokContactError]')[0].id = "extraNokContactError" + trueIndex + "-error";

            jq('<div/>', {
                'class': 'extraNokContact',
                'id': 'extraNokContact',
                html: template
            }).hide().appendTo('#extraNokContact' + nokIndex).slideDown('slow');

            extraNokPhoneInstances["extraNokContact" + trueIndex] = jQuery("#extraNokContact" + trueIndex);
            extraNokPhoneInstances["extraNokContact" + trueIndex].intlTelInput({
                initialCountry: "bw",
                hiddenInput: "extraNokContact" + trueIndex + "Code",
            });

            jq("#extraNokContact" + trueIndex).on("countrychange", function() {
                let code = extraNokPhoneInstances["extraNokContact" + trueIndex].intlTelInput('getSelectedCountryData').dialCode;
                if (code === '267') {
                    jq("#extraNokContact" + trueIndex).attr("maxlength", "8");
                } else {
                    jq("#extraNokContact" + trueIndex).attr("maxlength", "15");
                }
            });
        }
    });

    jq('.delete-extra-contact').on("click", function(e) {
        const element = e.target.nodeName;

        // Delete icon clicked
        if (element.toLowerCase() === "i") {
            jq(e.target).closest('#extraNokContact').remove();
            jq("#formPatientDetails :input").trigger("change");
        }
    });

    jq("#nokEmail").on("keyup", function(e) {
        const extraNokEmailId = e.target.id;
        const nokIndex = isNaN(extraNokEmailId.charAt(extraNokEmailId.length - 1)) ? '' : extraNokEmailId.charAt(extraNokEmailId.length - 1);
        let email = $(this).val();
        if ((email) && (!validateEmail(email))) {
            jq("#nextOfKinEmailError" + nokIndex).show();
        } else {
            jq("#nextOfKinEmailError" + nokIndex).hide();
        }
    });

    jq("#extraNoks").on("click", function(e) {

        const element = e.target.nodeName;

        if (element.toLowerCase() === "button" && e.target.id.startsWith("btnRemoveNok")) {
            let nokIndex = e.target.id.charAt(e.target.id.length - 1);
            e.preventDefault();
            jq(e.target).closest('#extraNok').remove();
            delete nokIdType["nokIdType" + nokIndex];
            jq("#formNOK :input").trigger("change");

        }
    });

    jq("#emergencyPatientRegistrationForm").on("submit", function(e) {
        e.preventDefault();
        registerEmergencyPatient();
    });

    jq("#quickPatientRegistrationForm").on("submit", function(e) {
        e.preventDefault();
        formUtils.saveFormData('formPatientDetails');
        registerQuickPatient();
    });

    jq("input, select, textarea").on("input change blur", function() {
        formUtils.saveFormData('formPatientDetails');
    });

    jq(window).on("beforeunload", function() {
        formUtils.saveFormData('formPatientDetails');
    });


    validatePatientForm();

    function validatePatientForm() {
        let formNok = $("#formNOK");
        if (formNok.length > 0) {
            formNok.validate({
                rules: {
                    'nokContactPersonType': {
                        required: function() {
                            return $("input[name='nokContactPersonType']:checked").length === 0;
                        }
                    }
                },
                messages: {
                    'nokContactPersonType': {
                        required: "Please select at least one contact person type."
                    }
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "nokContactPersonType") {
                        element.closest('.form-group').find('.error-message').html(error).show();
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        }

        let formPatientDetails = $("#formPatientDetails");
        if (formPatientDetails.length) {
            formPatientDetails.validate({
                rules: {
                    nokContact: {
                        required: true,
                        digits: true,
                        maxlength: 8,
                        minlength: 7
                    },
                    email: {
                        email: true
                    },
                    nokEmail: {
                        email: true
                    },
                    phoneNumber: {
                        digits: true,
                        maxlength: 15,
                        minlength: 7
                    },
                    extraPhone: {
                        required: true,
                        digits: true,
                        maxlength: 15,
                        minlength: 7
                    },
                    maritalStatus: {
                        required: true
                    },
                }
            });
        }
    }

    function getQueryParam(param) {
        let urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(param);
    }

    //date of birth format to DD/MM/YYYY
    jq("#dateOfBirth").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+nn",
        maxDate: new Date(),
    });

    webUtils.fetchDefaultDateFormat();

    // fetch the OpenMRS global property "botswanaemr.mpiSearchEnabled" via rest and assign its value to the 'mpiSearchEnabled' cookie
    function fetchMpiSearchEnabled() {
        jq.ajax({
            url: GP_URL + "/botswanaemr.mpiSearchEnabled",
            type: "GET",
            success: function(data) {
                let mpiSearchEnabled = data.value;
                webUtils.setCookie('mpiSearchEnabled', mpiSearchEnabled, 30);
            },
            error: function(xhr, status, error) {
                webUtils.setCookie('mpiSearchEnabled', 'false', 30);
            }
        });
    }

    fetchMpiSearchEnabled();

});