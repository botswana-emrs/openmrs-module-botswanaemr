const genericColourCodes = {
    extremelyLow: 'red',
    abnormal: 'red',
    low: 'orange',
    normal: 'green',
    high: 'orange',
    extremelyHigh: "red",
    obese: "red",
    underWeight: 'red',
    overWeight: 'orange',
    unrecordable: 'blue',
};

const altColorCodes = {
    extremelyLow: 'red',
    low: 'red',
    normal: 'green',
    high: 'red',
    extremelyHigh: "red",
}

let validationUtils = {
    bmiValidation: function (value, age) {
        if (value <= 18.5) {
            return "underWeight";
        } else if (value <= 24.9) {
            return "normal";
        } else if (value <= 29.9) {
            return "overWeight";
        } else {
            return "obese";
        }
    },
    respiratoryValidation: function (value, age) {
        if (age <= 1) {
            if (value <= 19) {
                return "low";
            } else if (value <= 25) {
                return "low";
            } else if (value <= 49) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 2) {
            if (value <= 19) {
                return "low";
            } else if (value <= 25) {
                return "low";
            } else if (value <= 39) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 5) {
            if (value <= 19) {
                return "low";
            } else if (value <= 25) {
                return "low";
            } else if (value <= 39) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 8) {
            if (value <= 19) {
                return "low";
            } else if (value <= 25) {
                return "low";
            } else if (value <= 29) {
                return "normal";
            } else {
                return "high";
            }
        } else {
            if (value <= 8) {
                return "low";
            } else if (value <= 14) {
                return "normal";
            } else if (value <= 20) {
                return "high";
            } else if (value <= 29) {
                return "high";
            } else {
                return "abnormal";
            }
        }
    },
    heartRateValidation: function (value, age) {
        if (age <= 2) {
            if (value <= 69) {
                return "extremelyLow";
            } else if (value <= 79) {
                return "low";
            } else if (value <= 130) {
                return "normal";
            } else if (value <= 159) {
                return "high";
            } else if (value >= 160) {
                return "extremelyHigh";
            }
        } else if (age <= 12) {
            if (value <= 59) {
                return "extremelyLow";
            } else if (value <= 79) {
                return "low";
            } else if (value <= 99) {
                return "normal";
            } else if (value <= 129) {
                return "high";
            } else if (value >= 130) {
                return "extremelyHigh";
            }
        } else if (age > 18) {
            if (value <= 4) {
                return "extremelyLow";
            } else if (value <= 50) {
                return "low";
            } else if (value <= 100) {
                return "normal";
            } else if (value <= 110) {
                return "high";
            } else if (value <= 129) {
                return "extremelyHigh";
            } else if (value >= 130) {
                return "abnormal";
            }
        }
    },
    systolicValidation: function (value, age) {
        if (age <= 0.5) {
            if (value < 87) {
                return "low";
            } else if (value <= 105) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 2) {
            if (value < 95) {
                return "low";
            } else if (value <= 105) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 7) {
            if (value < 97) {
                return "low";
            } else if (value <= 112) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 15) {
            if (value < 112) {
                return "low";
            } else if (value <= 128) {
                return "normal";
            } else {
                return "high";
            }
        } else {
            if (value < 90) {
                return "low";
            } else if (value <= 140) {
                return "normal";
            } else {
                return "high";
            }
        }
    },
    diastolicValidation: function (value, age) {
        if (age <= 0.5) {
            if (value < 53) {
                return "low";
            } else if (value <= 66) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 2) {
            if (value < 53) {
                return "low";
            } else if (value <= 66) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 7) {
            if (value < 57) {
                return "low";
            } else if (value <= 71) {
                return "normal";
            } else {
                return "high";
            }
        } else if (age <= 15) {
            if (value < 66) {
                return "low";
            } else if (value <= 80) {
                return "normal";
            } else {
                return "high";
            }
        } else {
            if (value < 60) {
                return "low";
            } else if (value <= 90) {
                return "normal";
            } else {
                return "high";
            }
        }
    },
    temperatureValidation: function (value, age) {
        if (age >= 16) {
            if (value <= 35) {
                return "extremelyLow";
            } else if (value <= 38.4) {
                return "normal";
            } else {
                return "extremelyHigh";
            }
        } else {
            if (value <= 35) {
                return "extremelyLow";
            } else if (value <= 37.9) {
                return "normal";
            } else {
                return "extremelyHigh";
            }
        }
    },
    glucoseLevelValidation: function (value, age) {
        if (value < 4) {
            return "low";
        } else if(value <= 11 ){
            return "normal";
        }else if(value <= 25 ){
            return "high";
        }else {
            return "extremelyHigh";
        }
    },
    oxygenSaturationValidation: function (value, age) {
        if (value < 90) {
            return "extremelyLow";
        } else if( value < 95) {
            return "low";
        } else if(value <=100 ){
            return "normal";
        }
    }
}

jq(document).ready(function () {
    // Element is a div with label and input
    function elementValidation(element) {
        let label = jq(element).find("label:first");

        let name = jq(label).text();

        let input = jq(element).find("input:first");
        let inputVal = jq(input).val();

        let status = validateVitals(name, inputVal);

        if (status && genericColourCodes[status]) {
            let color = genericColourCodes[status];
            if (name.toLowerCase().includes("rbs")) {
                color = altColorCodes[status];
            }

            jq(input).css("border-color", color);
            jq(input).parent().find(".validated").map(function () {
                jq(this).remove();
            });
            jq(input).parent().remove('.validated');
            if (status !== "normal") {
                jq(input).parent().append("<h6 class='validated' style='color:" + color + "'>" + toSentenceCase(status) + "</h6>");
            }
        } else {
            jq(input).parent().find(".validated").map(function () {
                jq(this).remove();
            });
        }
    }

    function validateOnLoad() {
        jq(".vital").map(function () {
            elementValidation(this);
        });
    }

    validateOnLoad();

    function toSentenceCase(inputString) {
        let result = inputString.replace(/([A-Z])/g, " $1");
        result = result.charAt(0).toUpperCase() + result.slice(1);
        return result;
    }

    function validateVitals(name, inputVal) {
        let status = '';

        if(inputVal) {
            if (name.toLowerCase().includes("body mass") || name.toLowerCase().includes("bmi")) {
                status = validationUtils.bmiValidation(inputVal, activePatientAge);
            } else if (name.toLowerCase().includes("respiratory")) {
                status = validationUtils.respiratoryValidation(inputVal, activePatientAge);
            } else if (name.toLowerCase().includes("pulse")) {
                status = validationUtils.heartRateValidation(inputVal, activePatientAge);
            } else if (name.toLowerCase().includes("systolic")) {
                status = validationUtils.systolicValidation(inputVal, activePatientAge);
            }else if (name.toLowerCase().includes("diastolic")) {
                status = validationUtils.diastolicValidation(inputVal, activePatientAge);
            } else if (name.toLowerCase().includes("temperature")) {
                status = validationUtils.temperatureValidation(inputVal, activePatientAge);
            }else if (name.toLowerCase().includes("rbs")) {
                status = validationUtils.glucoseLevelValidation(inputVal, activePatientAge);
            }else if (name.toLowerCase().includes("oxygen")) {
                status = validationUtils.oxygenSaturationValidation(inputVal, activePatientAge);
            }
        }
        return status;
    }

    // vital class must be assigned to the parent div and must have a label with input
    jq('.vital').on('change', function () {
        elementValidation(this);
    });
});

