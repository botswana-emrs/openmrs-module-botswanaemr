jQuery(document).ready(function (jq) {
    let sidebarState = jq.cookie('sidebar_state');

    if (sidebarState === 'collapsed') {
        jq('.sidebar').toggleClass('nav_collapsed');
    }

    jq('#sidebarCollapse').on('click', function () {
        jq('.sidebar').toggleClass('nav_collapsed');
        sidebarState = jq('.sidebar').hasClass('nav_collapsed') ? 'collapsed' : 'not_collapsed';
        jq.cookie('sidebar_state', sidebarState);
    });
});
