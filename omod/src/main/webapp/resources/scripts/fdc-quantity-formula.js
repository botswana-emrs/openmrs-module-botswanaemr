function calculateQuantityBasedOnWeight(weight) {
    if (weight < 4) {
        return 0.0;
    } else if (weight >= 4 && weight <= 7) {
        return 1.0;
    } else if (weight >= 8 && weight <= 11) {
        return 2.0;
    } else if (weight >= 12 && weight <= 15) {
        return 3.0;
    } else if (weight >= 16 && weight <= 24) {
        return 4.0;
    } else {
        return 0.0;
    }
}

function calculateQuantityBasedOnWeightAndAge(weight, age) {
    if (age >= 14 || weight >= 25) {
        if (weight >= 25 && weight <= 39) {
            return 2.0;
        }

        if (weight >= 40 && weight <= 54) {
            return 3.0;
        }

        if (weight >= 55 && weight <= 70) {
            return 4.0;
        }

        if (weight > 70) {
            return 5.0;
        }
    } else {
        if (weight < 4) {
            return 0.5;
        }

        if (weight >= 4 && weight <= 7) {
            return 1.0;
        }

        if (weight >= 8 && weight <= 11) {
            return 2.0;
        }

        if (weight >= 12 && weight <= 15) {
            return 3.0;
        }

        if (weight >= 16 && weight <= 24) {
            return 4.0;
        }
    }
    return 0.0;
}

