jq(document).ready(function () {
    function dateOnly() {
        jq(".timeOnly").each(function (elem) {
            let parent = jq(this)[0];
            let previous = null;
            let textToRemove = [];
            jq(parent).contents().each(function(){
                if (previous !== null && previous.nodeType === 3) {
                    if (jq(this).prop("nodeName").toLowerCase() === 'script') {
                        textToRemove.push(previous);
                    }
                }
                previous = this;
            });
            textToRemove.forEach(function (text) {
                jq(text).remove();
            });

            jq(parent).children().each(function () {
                if (jq(this).hasClass("hasDatepicker")){
                    jq(this).datepicker('setDate', new Date());
                    jq(this).hide();
                }
            });
        });
    }

    dateOnly();

    jq('#btnsApgar button').each(function () {
        jq(this).click(function () {
            let val = jq(this).html();
            jq('#btnsApgar button').removeClass("active")
            if (val.startsWith('10')) {
                setValue('apgari.value', 10);
            } else if (val.startsWith('5')) {
                setValue('apgari.value', 5);
            } else {
                setValue('apgari.value', 1);
            }
            jq(this).addClass("active");
        });

    });

    jq('#btnsApgarii button').each(function () {
        jq(this).click(function () {
            let val = jq(this).html();
            jq('#btnsApgarii button').removeClass("active")
            if (val.startsWith('10')) {
                setValue('apgarii.value', 10);
            } else if (val.startsWith('5')) {
                setValue('apgarii.value', 5);
            } else {
                setValue('apgarii.value', 1);
            }
            jq(this).addClass("active");
        });

    });
});