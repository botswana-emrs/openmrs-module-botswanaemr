jq = jQuery;

jq(function () {
    jq('#queuePatient').prop('disabled', true);

    jq('#queueRoom').on('change', function () {
        if (this.value) {
            jq('#queuePatient').prop('disabled', false);
        } else {
            jq('#queuePatient').prop('disabled', true);
        }
    });

    function retrievePatientId() {
        let params = new URLSearchParams(window.location.search);
        return params.get("patientId");
    }

    jq("#amount").on("keyup change", function () {
        if (jq(this).val() !== "" && jq("input[name='paymentRadio']").is(":checked") === true) {
            jq('#btnCapturePayment').prop('disabled', false);
        } else {
            jq('#btnCapturePayment').prop('disabled', true);
        }
    });

    jq("input[name='paymentRadio']").on("change", function () {
        if (jq(this).val() !== "" && jq("#amount").val() !== "" && jq("input[name='paymentRadio']").is(":checked") === true) {
            jq('#btnCapturePayment').prop('disabled', false);
        } else {
            jq('#btnCapturePayment').prop('disabled', true);
        }
    });

    jq("#assignPatient").change(function () {
        jq('#btnAssignPatient').prop('disabled', false);
    });

    jq('#capturePayment').submit(function (e) {
        //e.preventDefault();
        //let patientId = retrievePatientId();
        //window.location.href = "assignPatient.page?patientId=" + patientId;

    });
});
