let stringUtils = {
    toSentenceCase: function (inputString) {
        let result = inputString.replace(/([A-Z])/g, " $1");
        result = result.charAt(0).toUpperCase() + result.slice(1);
        return result;
    }
}

function validateEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
}

Date.prototype.toDateInputValue = (function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
});

let windowUtils = {
    displayAsPopup: function (pageUrl, popupTitle) {
        let popupWindow;
        if (!popupWindow || popupWindow.closed) {
            let iframe = jq('<iframe/>', {
                'src': pageUrl,
                'width': '100%',
                'height': '100%'
            });//.appendTo('#myIFrameModal');
            let popupWidth = screen.availWidth;
            let popupHeight = screen.availHeight;
            popupWindow = window.open('', 'myPopup', 'width=' + popupWidth + ',height=' + popupHeight + '');
            if (popupTitle === undefined) {
                popupTitle = "";
            }
            let htmlContent = `
                <html>
                    <head>
                        <title>${popupTitle}</title> <!-- Custom title -->
                    </head>
                    <body>${iframe[0].outerHTML}</body>
                </html>`;

            popupWindow.document.write(htmlContent);

            popupWindow.onunload = function () {
                showLoadingOverlay();
                window.location.reload();
            }
        } else {
            popupWindow.focus();
        }
    }
}

const dateUtils = {
    formatDate(date, formatStr) {
        const tokens = {
            // Day tokens
            d: date.getDate(),
            dd: this.twoDigits(date.getDate()),
            // Month tokens
            M: date.getMonth() + 1, // Month is zero-based
            MM: this.twoDigits(date.getMonth() + 1),
            MMM: [
                "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ][date.getMonth()],
            MMMM: [
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
            ][date.getMonth()],
            // Year tokens
            yyyy: date.getFullYear(),
            yy: String(date.getFullYear()).slice(-2)
        };

        return formatStr.replace(/([yMd]+)/g, (match) => tokens[match]);
    },

    twoDigits(num) {
        return num < 10 ? "0" + num : String(num);
    }
};

// Example usage:
//const today = new Date();
//const formattedDate = DateUtils.formatDate(today, "dd-MM-yyyy");
//console.log(formattedDate); // Output: 24-02-2024 (based on your current date)

let urlUtils = {
    urlParam: function (param) {
        let pageUrl = window.location.search.substring(1);
        let urlVariables = pageUrl.split('&');
        for (let i = 0; i < urlVariables.length; i++) {
            let paramName = urlVariables[i].split('=');
            if (paramName[0] === param) {
                return paramName[1];
            }
        }
    },
    redirectToReturnUrl: function () {
         let returnUrl = urlUtils.urlParam('returnUrl');
            if (returnUrl) {

                // Decoding the returnUrl at first try
                let decodedUrl = decodeURIComponent(returnUrl);

                // Check if the URL still contains encoded characters and decode again if necessary
                while (decodedUrl !== returnUrl) {
                    returnUrl = decodedUrl;
                    decodedUrl = decodeURIComponent(returnUrl);
                    console.log("returnUrl after further decode: ", decodedUrl);
                }

                // Perform the final redirection
                if (decodedUrl.startsWith("/" + OPENMRS_CONTEXT_PATH)) {
                    location.href = decodedUrl;
                } else {
                    location.href = "/" + OPENMRS_CONTEXT_PATH + decodedUrl;
                }
            } else {
                window.history.back();
            }
    },
    encodeUrl: function (url) {
        return encodeURIComponent(url);
    }
}

// front-end caching mechanism

const formUtils = {
    // clear cached data
    clearCachedData: function (formId) {
        const form = document.getElementById(formId);
        if (form) {
            const formName = form.getAttribute('data-form-name');
            if (formName) {
                localStorage.removeItem(formName);
                console.log(`Cleared cached data for form: ${formName}`);
            } else {
                console.error("Form name attribute is not set for form with id: " + formId);
            }
        } else {
            console.error("Form with id: " + formId + " not found");
        }
    },

    // save form data
    saveFormData: function (formId) {
        try {
            const form = document.getElementById(formId);
            if (form) {
                const formName = form.getAttribute('data-form-name');
                if (!formName) {
                    console.error("Form name attribute is not set for form with id: " + formId);
                    return;
                }

                const formElements = form.querySelectorAll("input, select, textarea");
                const formData = {};

                formElements.forEach(el => {
                    const id = el.id || el.name;
                    const value = el.value;
                    if (id && value !== undefined) {
                        formData[id] = {
                            value: value,
                            timestamp: Date.now()
                        };
                    }
                });

                // cache under respective form name
                localStorage.setItem(formName, JSON.stringify(formData));  
                console.log(`Saved data for form: ${formName}`);
            }
        } catch (error) {
            jq().toastmessage('showWarningToast', 'Failed to save form data. Please try again.');
        }
    },

    // form data repopulating
    repopulateFormData: function (formId) {
        try {
            // set expiry time
            const expirationTime = 30 * 60 * 1000;
            const form = document.getElementById(formId);
            if (form) {
                const formName = form.getAttribute('data-form-name');
                if (!formName) {
                    console.error("Form name attribute is not set for form with id: " + formId);
                    return;
                }

                const cachedFormData = localStorage.getItem(formName);
                if (cachedFormData) {
                    const formData = JSON.parse(cachedFormData);
                    const formElements = form.querySelectorAll("input, select, textarea");

                    formElements.forEach(el => {
                        const id = el.id || el.name;
                        if (id && formData[id]) {
                            const data = formData[id];
                            if (Date.now() - data.timestamp < expirationTime) {
                                // repopulate form field
                                el.value = data.value;
                                //console.log(`Repopulated data for: ${id} in form: ${formName}`);
                            } else {
                                //console.log(`Field: ${id}, Cached data expired`);
                                // remove expired data
                                delete formData[id];
                            }
                        }
                    });
                    
                    localStorage.setItem(formName, JSON.stringify(formData));
                } else {
                    //console.log(`No cached data found for form: ${formName}`);
                }
            }
        } catch (error) {
            jq().toastmessage('showWarningToast', 'Failed to repopulate form data. Please reload the page.');
        }
    }
};

let toggleGraph = function () {
    jq('.collapsible-button').click(function() {
        var content = jq('.collapsible .collapsible-content');
        content.toggle();
    });
}

const webUtils = {
    getCookieValue: function (cookieName) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            let cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                let cookie = cookies[i].trim();
                if (cookie.substring(0, cookieName.length + 1) === (cookieName + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(cookieName.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    },
    setCookie: function (cookieName, cookieValue, expirationDays) {
        let date = new Date();
        date.setTime(date.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + date.toUTCString();
        document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
    },
    fetchDefaultDateFormat: function () {
        const GP_URL = "/" + OPENMRS_CONTEXT_PATH + "/ws/rest/v1/systemsetting";
        jq.ajax({
            url: GP_URL + "/botswanaemr.defaultDateFormat",
            type: "GET",
            success: function (data) {
                let defaultDateFormat = data.value;
                webUtils.setCookie('defaultDateFormat', defaultDateFormat, 30);
            }
        });
    },
    clearCookie: function (cookieName, path = '/') {
        const pastDate = new Date(0);
        document.cookie = cookieName + "=; expires=" + pastDate.toUTCString() + "; path=" + path;
    }
}

const tbUtils = {
    fetchTbRegimensAutocomplete: (fieldObject) => {
        jq.getJSON({
            url: '/' + OPENMRS_CONTEXT_PATH + '/' + 'botswanaemr/art/manageRegimen/getTbRegimens.action',
            success: function (data) {
                let regimens = [];
                data.forEach(function (item) {
                    regimens.push({
                        'value': item.uuid,
                        'label': item.regimenName
                    });
                });
                fieldObject.autocomplete({
                    source: regimens,
                    minLength: 2,
                    select: function (event, ui) {
                        fieldObject.val(ui.item.label);
                        fieldObject.attr('data-uuid', ui.item.value);
                        return false;
                    }
                });
            },
            fail: function (xhr, status, error) {
                console.error('Error fetching regimens', error);
            }
        });
    },
    chooseFdcDrugBasedOnAgeAndWeight: (treatmentDrugField, selectedTreatmentPhase, age, weight) => {
        let initialDrugs = [
        'Rifampicin 75 mg/Isoniazid 50mg/Pyrazinamide 150 mg (paediatric)',
        'Rifampicin 150mg/Isoniazid 75mg/Pyrazinamide 400mg/Ethambutol 275mg'];

        let continuationDrugs = [
        'Rifampicin 150 mg/Isoniazid 75 mg/ Ethambutol 275 mg',
        'Rifampicin 75mg/Isoniazid 50mg (paediatric)'];

        let adultDrugs = [
        'Rifampicin 150mg/Isoniazid 75mg/Pyrazinamide 400mg/Ethambutol 275mg',
        'Rifampicin 150 mg/Isoniazid 75 mg/ Ethambutol 275 mg'];

        let childDrugs = [
        'Rifampicin 75 mg/Isoniazid 50mg/Pyrazinamide 150 mg (paediatric)',
        'Rifampicin 75mg/Isoniazid 50mg (paediatric)'];

        let initialAdultDrugs = initialDrugs.filter(function(drug) {
            return adultDrugs.includes(drug);
        });

        let initialChildDrugs = initialDrugs.filter(function(drug) {
            return childDrugs.includes(drug);
        });

        let continuationAdultDrugs = continuationDrugs.filter(function(drug) {
            return adultDrugs.includes(drug);
        });

        let continuationChildDrugs = continuationDrugs.filter(function(drug) {
            return childDrugs.includes(drug);
        });

        let enableInitialChildDrugsOnly = () => {
            treatmentDrugField.find("option").each(function() {
                if (initialChildDrugs.includes(jq(this).text().trim())) {
                    jq(this).removeAttr('disabled');
                }
            });
        }

        let enableInitialAdultDrugsOnly = () => {
            treatmentDrugField.find("option").each(function() {
                if (initialAdultDrugs.includes(jq(this).text().trim())) {
                    jq(this).removeAttr('disabled');
                }
            });
        }

        let enableContinuationAdultDrugsOnly = () => {
            treatmentDrugField.find("option").each(function() {
                if (continuationAdultDrugs.includes(jq(this).text().trim())) {
                    jq(this).removeAttr('disabled');
                }
            });
        }

        let enableContinuationChildDrugsOnly = () => {
            treatmentDrugField.find("option").each(function() {
                if (continuationChildDrugs.includes(jq(this).text().trim())) {
                    jq(this).removeAttr('disabled');
                }
            });
        }

        let validationMessage = [];

        if (age == '') {
            validationMessage.push('Age is required');
        }

        if (weight == '') {
            validationMessage.push('Weight is required');
        }

        if (selectedTreatmentPhase == '') {
            validationMessage.push('Treatment phase is required');
        }

        if (validationMessage.length > 0) {

        let errorMessage = '';
            validationMessage.forEach(function(msg) {
            errorMessage += msg + '</br>'
        });

        jq().toastmessage('showErrorToast', errorMessage);
            return false;
        }

        // Check patient's treatment phase, age and weight
        treatmentDrugField.val('');
        treatmentDrugField.find("option").attr('disabled', true);

        if (selectedTreatmentPhase == 'Initial') {
            if (age < 14) {
                enableInitialChildDrugsOnly();
            } else if (age > 14 || weight > 25) {
                enableInitialAdultDrugsOnly();
            }
        } else if (selectedTreatmentPhase == 'Continuation') {
            if (age < 14) {
                enableContinuationChildDrugsOnly();
            } else if (age >= 14 || weight > 25) {
                enableContinuationAdultDrugsOnly();
            }
        }

        // always enable the other option
        treatmentDrugField.find("option").each(function() {
            if (jq(this).text().trim() === 'Other') {
                jq(this).removeAttr('disabled');
            }
        });
    }
};

const collectionUtils = {
    reverseMap : function (originalMap) {
        const reversedMap = new Map();
        for (const [key, value] of originalMap) {
            reversedMap.set(value, key);
        }
        return reversedMap;
    }
};