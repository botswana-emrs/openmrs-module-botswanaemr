jq(document).ready(function () {
    function calculateBmi(weight, height) {
        let bmi = null;
        if (weight && height) {
            bmi = weight / ((height / 100) * (height / 100));
        }
        return bmi;
    }

    function calculateBsa(weight, height) {
        let bsa = null;
        if (weight && height) {
            bsa = Math.sqrt((height * weight) / 3600);
        }
        return bsa;
    }

    function addMore(targetId) {
        let table = jq("#" + targetId).parent();

        let thisRow = table.find('tr.main:visible:last');
        let nextRow = table.find('tr.main:hidden:first');

        if (nextRow.length > 0) {
            nextRow.show('slow');
        }
    }

    function updateBmi() {
        let wt = htmlForm.getValueIfLegal('weight.value');
        let ht = htmlForm.getValueIfLegal('height.value');

        let bmi = calculateBmi(wt, ht);

        if (bmi != null && !isNaN(bmi)) {
            setValue('bmi.value', bmi.toFixed(1));
        } else {
            setValue('bmi.value', '');
        }

        // Trigger validation
        let bmiInput = getField('bmi.value');
        let bmiId = jq(bmiInput).attr("id");
        jq("#" + bmiId).trigger("change");
    }

    function updateBsa() {
        let wt = htmlForm.getValueIfLegal('weight.value');
        let ht = htmlForm.getValueIfLegal('height.value');

        let bsa = calculateBsa(wt, ht);

        if (bsa != null && !isNaN(bsa)) {
            setValue('bsa.value', bsa.toFixed(2));
        } else {
            setValue('bsa.value', '');
        }
    }

    function setAutocomplete(element) {
        element.autocomplete({
            source: function (request, response) {
                jq.getJSON(icd11SearchUrl, {
                    q: request.term
                }).success(function (data) {
                    var results = [];
                    for (var i in data) {
                        var result = {
                            label: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text(),
                            value: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text()
                        };
                        results.push(result);
                    }
                    response(results);
                });
            },
            minLength: 3,
            select: function (event, ui) {
                event.preventDefault();
                jq(element).val(ui.item.label);
                jq(element).attr('data-overlay', ui.item.value);
            },
        });
    }

    jq('.diagnosis-section input').focus(function () {
        setAutocomplete(jq(this));
    });

    jq("#weight, #height").change(function (e) {
        updateBmi();
        updateBsa();
    });

    jq(function () {
        getField('year.value').change(function () {
            // window.alert('Weight is now ' + getValue('weight.value'))
        });
    });

    jq('#yearSection input').each(function () {
        jq(this).change(function () {
            let currentYear = new Date().getFullYear();
            if (jq(this).parent().attr("id") === "year" &&
                jq(this).val()) {
                if (jq(this).val() > currentYear) {
                    jq(this).parent().find(".invalid-year").map(function () {
                        jq(this).remove();
                    });

                    jq(this).parent().append("<label class=\"text-danger invalid-year\">Year can not be in the future</label>");
                    return;
                }
                if (activePatientAge && jq(this).val() < (currentYear - activePatientAge )) {
                    jq(this).parent().find(".invalid-year").map(function () {
                        jq(this).remove();
                    });

                    jq(this).parent().append("<label class=\"text-danger invalid-year\">Year can not be less than patient's birth year.</label>");
                    return;
                }

            }

            jq(this).parent().find(".invalid-year").map(function () {
                jq(this).remove();
            });
        });
    });

    function setPlaceHolder(input){
        if (input) {
            jq(input).attr("placeholder", "Start typing to get hints ...");

        }
    }

    // setPlaceHolder(getField('conditionsInput.value'));
    setPlaceHolder(getField('allergiesInput.value'));
    setPlaceHolder(getField('familyConditionsInput.value'));
    setPlaceHolder(getField('pastOperationsInput.value'));

});
