function calculateBmi(weight, height) {
        let bmi = null;
        if (weight && height) {
            bmi = weight / ((height / 100) * (height / 100));
        }
        return bmi;
    }

function calculateBsa(weight, height) {
    let bsa = null;
    if (weight && height) {
        bsa = Math.sqrt((height * weight) / 3600);
    }
    return bsa;
}