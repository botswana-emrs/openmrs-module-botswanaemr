/*
 *This is a helper object that contains functions to perform basic stock related functions on a form
 *
 *@param: selector string or JQuery object
 */
const stockUtils = {
    fetchHtsKitAvailableBatches: function (conceptId, selectedKitQuantity, batchIdInput, expiryInput, stockRoomInput) {
        let parameter = {
            'conceptId': conceptId,
            'stockroomId': stockRoomInput.val()
        };
        let batchNumberToExpiry = {};
        let batchNumberToQty = {};

        jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsKitAvailableBatches.action', parameter)
            .done(function (data) {
                if (data.status === 'success') {
                    // Clear the batch input
                    batchIdInput.val('');
                    // Clear the expiry input
                    expiryInput.val('');
                    // Clear the quantity input
                    selectedKitQuantity.val('');
                    // populate autocomplete with batches
                    if (data.batches) {
                        let quantity = data.availableQuantity;
                        if (selectedKitQuantity.length) {
                            //selectedKitQuantity.val(quantity);
                            selectedKitQuantity.addClass('kit-quantity');
                            // selectedKitQuantity.replaceWith('&lt;input type="hidden" class="kit-quantity" value="' + quantity + '">');
                        } else {
                            jQuery(".kit-quantity").val(quantity);
                        }

                        jQuery.each(data.batches, function (i, item) {
                            batchNumberToExpiry[item.batchNumber] = item.expiryDate;
                            batchNumberToQty[item.batchNumber] = item.quantity;
                            if (data.batchNumbers[i]) {
                                let val = item.batchNumber + " (" + batchNumberToExpiry[item.batchNumber] + ")";
                                data.batchNumbers[i] = val;
                                batchNumberToExpiry[val] = item.expiryDate;
                                batchNumberToQty[val] = item.quantity;
                            }
                        })

                        batchIdInput.autocomplete({
                            source: data.batchNumbers,
                            minLength: 0,
                            select: function (event, ui) {
                                jQuery(selectedKitQuantity).val(batchNumberToQty[ui.item.value]);
                                jQuery(expiryInput).datepicker('setDate', new Date(batchNumberToExpiry[ui.item.value]));
                                jQuery(expiryInput).datepicker('option', 'minDate', new Date(batchNumberToExpiry[ui.item.value]));
                                jQuery(expiryInput).datepicker('option', 'maxDate', new Date(batchNumberToExpiry[ui.item.value]));
                                // trigger change event on expiry input
                                jQuery(batchIdInput).trigger('change');
                            }
                        }).on('focus', function () {
                            jQuery(this).keydown();
                        });
                    } else {
                        batchIdInput.autocomplete();
                        batchIdInput.autocomplete('destroy');
                    }
                    batchIdInput.focus();
                } else {
                    if (selectedKitQuantity.length) {
                        selectedKitQuantity.val(0);
                        selectedKitQuantity.addClass('kit-quantity');
                    } else {
                        jQuery(".kit-quantity").val('');
                    }
                }
            });
    },
    getHtsStockrooms: function (stockRoomNameInput, stockRoomIdInput) {
        jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsStockRooms.action')
            .done(function (data) {
                if (data) {
                    if (data.stockrooms) {
                        var results = [];

                        for (var i in data.stockrooms) {
                            var index = parseInt(i);
                            if (data.stockrooms[i].uuid == null || data.stockrooms[i].uuid == undefined) {
                                continue;
                            }
                            results.push(
                                {
                                    'value': data.stockrooms[i].uuid,
                                    'label': data.stockrooms[i].name
                                });
                        }

                        stockRoomNameInput.autocomplete({
                            source: results,
                            minLength: 0,
                            select: function (event, ui) {
                                event.preventDefault();
                                stockRoomIdInput.val(ui.item.value);
                                jq(this).val(ui.item.label);
                            }
                        }).on('focus', function () {
                            jQuery(this).keydown();
                        });
                    }
                }
            });
    },
    getDispensingStockrooms: function (stockRoomNameInput, stockRoomIdInput) {
        jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchDispensingStockRooms.action')
            .done(function (data) {
                if (data) {
                    if (data.stockrooms) {
                        var results = [];

                        for (var i in data.stockrooms) {
                            var index = parseInt(i);
                            if (data.stockrooms[i].uuid == null || data.stockrooms[i].uuid == undefined) {
                                continue;
                            }
                            results.push(
                                {
                                    'value': data.stockrooms[i].uuid,
                                    'label': data.stockrooms[i].name
                                });
                        }

                        stockRoomNameInput.autocomplete({
                            source: results,
                            minLength: 0,
                            select: function (event, ui) {
                                event.preventDefault();
                                stockRoomIdInput.val(ui.item.value);
                                jq(this).val(ui.item.label);
                            }
                        }).on('focus', function () {
                            jQuery(this).keydown();
                        });
                    }
                }
            });
    },
    fetchHtsServicePoints: function (servicePointInput) {
        jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchHtsServicePoints.action')
            .done(function (data) {
                if (data.status === 'success') {
                    // set autocomplete with service points on the service-point input
                    servicePointInput.autocomplete({
                        source: data.servicePoints.map((item) => item.name),
                        minLength: 0
                    }).on('focus', function () {
                        jQuery(this).keydown();
                    });
                }
            });
    },
    fetchItemByDrug: function (drugId, stockRoomInput, batchIdInput) {
         let parameter = {
            'drugId': drugId,
            'stockRoomUuid': stockRoomInput.val()
         };
         jQuery.getJSON('/' + OPENMRS_CONTEXT_PATH + '/botswanaemr/search/fetchAvailableBatchesByDrugUuid.action', parameter)
         .done(function (data) {
            if (data.status === 'success') {
                // populate autocomplete with batches
                if (data.batches) {
                    batchIdInput.autocomplete({
                        source: data.batchNumbers,
                        minLength: 0,
                        select: function (event, ui) {
                            // trigger change event on expiry input
                            jQuery(this).trigger('change');
                        }
                    }).on('focus', function () {
                        jQuery(this).keydown();
                    });
                } else {
                    batchIdInput.autocomplete();
                    batchIdInput.autocomplete('destroy');
                }
                batchIdInput.focus();
            }
         });
    }
};

