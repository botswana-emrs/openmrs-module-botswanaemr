jq(document.body).ready(() => {

    const currentUser = getValue("tbSelectCurrentUser.value");

    const valueNotSet = (value) => {
        return value === null || isNaN(value) || value === "";
    };

    jq("#tbAdministeredBy").change(() => {
        if (getValue("tbAdministeredBy.value") !== "1574AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA") {
            setValue("selectedCurrentUser.value", currentUser)
        }
    });

    jq("#submit").on("click", (event) => {
        const selectedUser = getValue("selectedCurrentUser.value");
        if (valueNotSet(selectedUser)) {
            setValue("selectedCurrentUser.value", currentUser)
        }
    });

});
