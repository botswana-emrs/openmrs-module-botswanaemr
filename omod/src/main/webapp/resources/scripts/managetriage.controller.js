var app = angular.module("triageDataApp",
    ['app.restfulServices', 'app.models', 'app.commonFunctionsFactory']);

app.controller("TriageDataController", TriageDataController);
TriageDataController.$inject = ['$scope', 'RestfulService', 'CommonFunctions'];

function TriageDataController($scope, RestfulService, CommonFunctions) {
    var self = this;
    $scope.conditions = $scope.conditions || [];
    $scope.vitalsObs = $scope.vitalsObs || [];
    $scope.conditionListToBeRemoved = null;
    $scope.tabs = ["ACTIVE", "INACTIVE"];
    $scope.conditionHistoryList = $scope.conditionHistoryList || [];
    $scope.symtomsObservations = $scope.symtomsObservations || [];
    $scope.groupObs = $scope.groupObs || $scope.groupObs;
    $scope.lifestyleObservations = $scope.lifestyleObservations || [];
    $scope.operationsObservations = $scope.operationsObservations || [];
    $scope.dietObservations = $scope.dietObservations || [];
    $scope.diagnosisObservations = $scope.dietObservations || [];
    $scope.isProcessing = false;
    $scope.isConditionsProcessing = false;
    $scope.complaintsObservations = $scope.complaintsObservations || [];

    const symptomsGroupingConcept = '1727AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
    const symptomsConceptUuid = "161602AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const symptomsDurationUuid = "1731AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const symptomsDurationUnitUuid = "1732AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

    const lifestyleGroupingConcept = '9a42abff-1199-4727-9c4d-c9968f0719cc';
    const lifestyleConceptUuid = "88e4e67f-773e-43ce-80c8-29b01ab272e1";

    const complaintGroupingConcept = 'd8d64e4c-5315-4ef3-8c25-a86b2ec0121c';
    const complaintConceptUuid = "160531AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

    const notesConcept = "165095AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const operationsGroupingConcept = "160714AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const operationsConceptUuid = "1651AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const operationsNotesConceptUuid = "160716AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

    const dietGroupingConcept = "bb8a8f81-7b21-4ca0-946f-f4daf78d5be8";
    const dietConceptUuid = "a9a8f9a1-1e2e-49cb-a405-a6f507ead6b4";

    const diagnosisGroupingConcept = "bb8a8f81-7b21-4ca0-946f-f4daf78d5be8"; //To be updated
    const diagnosisConceptUuid = "a9a8f9a1-1e2e-49cb-a405-a6f507ead6b4"; // To be updated

    const relativeConceptUuid = "164352AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const yearConceptUuid = "164393AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const amountConceptUuid = "162524AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const frequencyConceptUuid = "ade20a36-2bb2-4006-b274-891b3d824e5b";
    const vitalsGroupingConcept = "1114AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    const vitalsConcepts = ['5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5086AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5089AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5090AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
        '1342AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '980AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5087AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '5314AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA']

    function removeEmptyObs(obsArr) {
        return obsArr.filter(function (obs) {
            return obs.value;
        });
    }

    function extractVitalObs() {
        $scope.vitalsObs = [];
        $scope.bloodPressure = {};
        let vitalObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === vitalsGroupingConcept;
        });

        angular.forEach(vitalObs, function (obs, index) {
            $scope.vitalsObs.push(...obs.groupMembers.filter(function (vital) {
                if (vital.concept.name.display.includes("Systolic")) {
                    $scope.bloodPressure.systolic = vital;
                } else if (vital.concept.name.display.includes("Diastolic")) {
                    $scope.bloodPressure.diastolic = vital;
                }
                return vital.value !== undefined;
            }));
        });
    }

    function getGroupingObs(data) {
        $scope.groupObs = [];
        data.encounters = data.encounters.filter(function (encounter) {
            return encounter.encounterType !== undefined && encounter.encounterType.uuid === "7A4C7F96-F49D-43BA-BB51-D34C0B4C2F6B";
        });

        angular.forEach(data.encounters, function (encounter, index) {
            let groupingObs = encounter.obs.filter(function (observation) {
                return observation.groupMembers !== null;
            });
            $scope.groupObs.push(...groupingObs);
        });
    }

    function getPrimaryObs(obs, primaryConcept) {
        let primaryIndex;
        let primaryObs = null;
        angular.forEach(obs, function (observation, index) {
            if (observation.concept.uuid === primaryConcept) {
                if (primaryConcept === operationsConceptUuid) {
                    if (observation.value && observation.value.name.name === "Other" && observation.comment) {
                        observation.value.name.name = observation.comment;
                    }
                }
                primaryObs = observation;
                primaryIndex = index;
            }
        });

        if (primaryObs) {
            obs.splice(primaryIndex, 1);
            angular.forEach(obs, function (observation, index) {
                if (observation.concept.uuid === notesConcept && observation.value) {
                    primaryObs.comment = observation.value;
                } else if (observation.concept.uuid === operationsNotesConceptUuid && observation.value) {
                    primaryObs.comment = observation.value;
                } else if (observation.concept.uuid === relativeConceptUuid && observation.value) {
                    primaryObs.relative = observation.value;
                } else if (observation.concept.uuid === yearConceptUuid && observation.value) {
                    primaryObs.year = observation.value;
                } else if (observation.concept.uuid === amountConceptUuid && observation.value) {
                    primaryObs.amount = observation.value;
                } else if (observation.concept.uuid === frequencyConceptUuid && observation.value) {
                    primaryObs.frequency = observation.value;
                } else if (observation.concept.uuid === symptomsDurationUnitUuid && observation.value) {
                    primaryObs.symptomsDurationUnit = observation.value;
                } else if (observation.concept.uuid === symptomsDurationUuid && observation.value) {
                    primaryObs.symptomsDuration = observation.value;
                }
            });
        }

        return primaryObs;
    }

    function populateObs(obsGroups, primaryConcept) {
        let populatedObs = [];
        angular.forEach(obsGroups, function (obsGroup, index) {
            let primaryObs = getPrimaryObs(obsGroup.groupMembers, primaryConcept);
            populatedObs.push(primaryObs);

        });
        return populatedObs;
    }

    function extractSymptoms() {
        let symptomObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === symptomsGroupingConcept;
        });

        $scope.symtomsObservations = populateObs(symptomObs, symptomsConceptUuid);
    }

    function extractOperations() {
        let operationsObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === operationsGroupingConcept;
        });
        $scope.operationsObservations = populateObs(operationsObs, operationsConceptUuid);
    }

    function extractLifeStyle() {
        let lifestyleObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === lifestyleGroupingConcept;
        });

        $scope.lifestyleObservations = populateObs(lifestyleObs, lifestyleConceptUuid);
    }

    function extractComplaints() {
        let complaintObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === complaintGroupingConcept;
        });

        $scope.complaintsObservations = populateObs(complaintObs, complaintConceptUuid);
    }

    function extractDiet() {
        let dietObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === dietGroupingConcept;
        });

        $scope.dietObservations = populateObs(dietObs, dietConceptUuid);
    }

    self.getTriageData = self.getTriageData || function (visitUuid) {
        $scope.isProcessing = true;
        $scope.visitUuid = visitUuid;
        RestfulService.setBaseUrl('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/visit');

        let customParam = "custom:(uuid,visitId,visitType,patient,encounters:(uuid,encounterType,voided,orders:(uuid,orderType,voided,concept:(uuid,set,name)),obs:(uuid,comment,value,concept,obsDatetime,groupMembers:(uuid,comment,concept:(uuid,name),obsDatetime,value:(uuid,name),groupMembers:(uuid,concept:(uuid,name),value:(uuid,name),groupMembers:(uuid,concept:(uuid,name),value:(uuid,name)))))))"

        $scope.vitalsObs = [];
        if ($scope.visitUuid !== null && $scope.visitUuid !== undefined) {
            RestfulService.get(visitUuid, {
                'v': customParam
            }, function (data) {
                getGroupingObs(data);
                extractVitalObs();
                extractSymptoms();
                extractLifeStyle();
                extractOperations();
                extractDiet();
                extractComplaints();

            }, function (error) {
                $scope.isProcessing = false;
            });
        }
    }

    self.getConditions = self.getConditions || function (patientUuid) {
        $scope.isConditionsProcessing = true;
        RestfulService.setBaseUrl('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/emrapi');
        if (patientUuid === null || patientUuid === undefined) {
            $scope.patientUuid = CommonFunctions.extractUrlArgs(window.location.search)['patientId'];
        } else {
            $scope.patientUuid = patientUuid;
        }

        if ($scope.patientUuid !== null && $scope.patientUuid !== undefined) {
            RestfulService.get('conditionhistory', {"patientUuid": $scope.patientUuid}, function (data) {
                $scope.conditionHistoryList = data;
            }, function (error) {
                $scope.isConditionsProcessing = false;
            });
        }
    }

    self.getVitalsObsData = function (visitUuid) {
        if (!$scope.isProcessing && $scope.vitalsObs.length === 0) {
            self.getTriageData(visitUuid);
        } else {
            // extractVitalObs();
        }
    }

    self.getConditionsData = function (patientUuid) {
        if (!$scope.isProcessing && $scope.conditionHistoryList.length === 0) {
            self.getConditions(patientUuid);
        }
    }

    $scope.getSymptomsData = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.symtomsObservations.length === 0) {
            extractSymptoms();
        }
    }

    $scope.getOperationsData = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.operationsObservations.length === 0) {
            extractOperations();
        }
    }

    $scope.getLifeSyleData = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.lifestyleObservations.length === 0) {
            extractLifeStyle();
        }
    }

    $scope.getComplaintsData = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.complaintsObservations.length === 0) {
            extractComplaints();
        }
    }

    $scope.getDietData = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.dietObservations.length === 0) {
            extractDiet();
        }
    }

    function extractDiagnosis() {
        let diagnosisObs = $scope.groupObs.filter(function (observation) {
            return observation.concept.uuid === diagnosisGroupingConcept;
        });

        $scope.dietObservations = populateObs(diagnosisObs, dietConceptUuid);
    }

    $scope.getDiagnosis = function (visitUuid) {
        if (!$scope.isProcessing && (!$scope.groupObs || $scope.groupObs.length === 0)) {
            self.getTriageData(visitUuid);
        } else if ($scope.diagnosisObservations.length === 0) {
            extractDiagnosis();
        }
    }

    $scope.getIndex = function (obj) {
        if (obj) {
            let index = $scope.vitalsObs.findIndex(object => {
                return object.uuid === obj.uuid;
            });
            return index;
        } else {
            return 0;
        }
    }

    $scope.init = function (patientUuid, visitUuid) {
        self.getVitalsObsData(visitUuid);
        self.getConditionsData(patientUuid);
    }


    $scope.getVitalsObsData = self.getVitalsObsData;
    $scope.getConditions = self.getConditions;
    $scope.getConditionsData = self.getConditionsData

    $scope.nokIndex = 0;
    $scope.nokAttributeEdit = [];
    $scope.personNokAttributes = $scope.personNokAttributes || [];
    $scope.idNumber = '';
    $scope.replaceRegex = /[A-Z]{3}-\d{1}-/;

    let val = {
        "value": '',
        "name": '',
        "uuid": '',
    };
    $scope.contact1 = $scope.contact1 || val;
    $scope.contact2 = $scope.contact2 || val;
    $scope.contact3 = $scope.contact3 || val;
    $scope.contact4 = $scope.contact4 || val;
    $scope.contact5 = $scope.contact5 || val;
    $scope.email = val || val;
    $scope.relationship = $scope.relationship || val;
    $scope.givenName = $scope.givenName || val;
    $scope.middleName = $scope.middleName || val;
    $scope.familyName = $scope.familyName || val;
    $scope.idNumber = $scope.idNumber || val;
    $scope.idType = $scope.idType || val;
    $scope.contactType = $scope.contactType || val;
    $scope.district = $scope.district || val;
    $scope.city = $scope.city || val;
    $scope.landmark = $scope.landmark || val;
    $scope.maxNok = false;

    $scope.toSentenceCase = function (text) {
        const result = text.replace(/([A-Z])/g, " $1");
        return result.charAt(0).toUpperCase() + result.slice(1);
    }

    $scope.getPatientNextOfKins = function (patientId) {
        $scope.isProcessing = true;
        $scope.patientId = patientId;

        RestfulService.setBaseUrl('/' + OPENMRS_CONTEXT_PATH + '/ws/rest/v1/botswanaemr/nokRelationship/');

        if ($scope.patientId !== null && $scope.patientId !== undefined) {
            RestfulService.get(patientId, {}, function (data) {
                if (data.length > 0) {
                    $scope.personNokAttributes = data;
                }
            }, function (error) {
                $scope.isProcessing = false;
            });
        }
    }

    $scope.setAttributeToEdit = function (data) {
        $scope.maxNok = false;
        $scope.nokAttributeEdit = data;
        $scope.nokUuid = data.personB.uuid;

        for (let key in data) {
            if (['givenName', 'middleName', 'familyName', 'district', 'city', 'landmark'].includes(key)) {
                let val = {
                    "value": data[key],
                    "name": key,
                    "uuid": data[key]
                };
                if (key.includes('givenName')) {
                    $scope.givenName = val;
                }
                if (key.includes('middleName')) {
                    $scope.middleName = val;
                }
                if (key.includes('familyName')) {
                    $scope.familyName = val;
                }
                if (key.includes('district')) {
                    $scope.district = val;
                }
                if (key.includes('city')) {
                    $scope.city = val;
                }
                if (key.includes('landmark')) {
                    $scope.landmark = val;
                }
            } else {
                let val = {
                    "value": data[key].value,
                    "name": key,
                    "uuid": data[key].uuid
                };
                if (key.includes('contactType')) {
                    $scope.contactType = val;
                }
                if (key.includes('idType')) {
                    $scope.idType = val;
                }
                if (key.includes('idNumber')) {
                    $scope.idNumber = val;
                }
                if (key.includes('relationShip')) {
                    $scope.relationship = val;
                }
                if (key.includes('email')) {
                    $scope.email = val;
                }
                if (key.includes('contact1')) {
                    $scope.code1 = getTelephoneCode(val.value);
                    val.value = getTelephoneVal(val.value);
                    $scope.contact1 = val;
                }
                if (key.includes('contact2')) {
                    $scope.code2 = getTelephoneCode(val.value);
                    val.value = getTelephoneVal(val.value);
                    $scope.contact2 = val;
                }
                if (key.includes('contact3')) {
                    $scope.code3 = getTelephoneCode(val.value);
                    val.value = getTelephoneVal(val.value);
                    $scope.contact3 = val;
                }
                if (key.includes('contact4')) {
                    $scope.code4 = getTelephoneCode(val.value);
                    val.value = getTelephoneVal(val.value);
                    $scope.contact4 = val;
                }
                if (key.includes('contact5')) {
                    $scope.code5 = getTelephoneCode(val.value);
                    val.value = getTelephoneVal(val.value);
                    $scope.contact5 = val;
                }
            }
        }
    }

    function getTelephoneVal(val) {
        if (val) {
            const result = val.trim().split(" ");
            if (result.length > 1) {
                return result[1];
            }
        }
        return val;
    }

    function getTelephoneCode(val){
        if(val) {
            const result = val.trim().split(" ");
            if (result.length > 1) {
                return result[0];
            }
        }
        return "";
    }

    function resetVariables() {
        let val = {
            "value": '',
            "name": '',
            "uuid": '',
        };
        $scope.contac1 = val;
        $scope.contact2 = val;
        $scope.contact3 = val;
        $scope.contact4 = val;
        $scope.code1 = "";
        $scope.code2 = "";
        $scope.code3 = "";
        $scope.code4 = "";
        $scope.code5 = "";
        $scope.email = val;
        $scope.contact5 = val;
        $scope.relationship = val;
        $scope.givenName = val;
        $scope.middleName = val;
        $scope.familyName = val;
        $scope.idNumber = val;
        $scope.idType = val;
        $scope.contactType = val;
        $scope.district = val;
        $scope.city = val;
        $scope.landmark = val;
    }

    $scope.addNextOfKin = function () {
        resetVariables();
        let index = 1;
        if ($scope.personNokAttributes.length > 0) {
            index = $scope.personNokAttributes.length + 1;
        }

        if (index <= 5) {
            $scope.maxNok = false;

            $scope.contact1 = {
                "value": '',
                "name": "contact1",
                "uuid": '',
            };
            $scope.contact2 = {
                "value": '',
                "name": "contact2",
                "uuid": '',
            };
            $scope.contact3 = {
                "value": '',
                "name": "contact3",
                "uuid": '',
            };
            $scope.contact4 = {
                "value": '',
                "name": "contact4",
                "uuid": '',
            };
            $scope.email = {
                "value": '',
                "name": "email",
                "uuid": '',
            };
            $scope.contact5 = {
                "value": '',
                "name": "contact5",
                "uuid": '',
            };
            $scope.relationship = {
                "value": '',
                "name": "relationShip",
                "uuid": '',
            };
            $scope.givenName = {
                "value": '',
                "name": "givenName",
                "uuid": '',
            };
            $scope.middleName = {
                "value": '',
                "name": "middleName",
                "uuid": '',
            };
            $scope.familyName = {
                "value": '',
                "name": "familyName",
                "uuid": '',
            };
            $scope.idNumber = {
                "value": '',
                "name": "idNumber",
                "uuid": '',
            };
            $scope.idType = {
                "value": '',
                "name": "idType",
                "uuid": '',
            };
            $scope.contactType = {
                "value": '',
                "name": "idType",
                "uuid": '',
            }
            $scope.district = {
                "value": '',
                "name": "district",
                "uuid": '',
            }
            $scope.city = {
                "value": '',
                "name": "city",
                "uuid": '',
            }
            $scope.landmark = {
                "value": '',
                "name": "landmark",
                "uuid": '',
            }
            $scope.code1 = "";
            $scope.code2 = "";
            $scope.code3 = "";
            $scope.code4 = "";
            $scope.code5 = "";
        } else {
            $scope.maxNok = true;
        }

    }

    $scope.getNokAttributeValue = function (attributeType) {
        angular.forEach($scope.nokAttributeEdit, function (attribute) {
            if (attribute.attributeType.display.includes(attributeType)) {
                return attribute.value;
            }
        });
        return '';
    }

    const genericColourCodes = {
        extremelyLow: 'red',
        abnormal: 'red',
        low: 'orange',
        normal: 'green',
        high: 'orange',
        extremelyHigh: "red",
        obese: "red",
        underWeight: 'red',
        overWeight: 'orange',
        unrecordable: 'blue',
    };

    let validationUtils = {
        bmiValidation: function (value, age) {
            if (value <= 18.5) {
                return "underWeight";
            } else if (value <= 24.9) {
                return "normal";
            } else if (value <= 29.9) {
                return "overWeight";
            } else {
                return "obese";
            }
        },
        respiratoryValidation: function (value, age) {
            if (age <= 2) {
                if (value <= 19) {
                    return "extremelyLow";
                } else if (value <= 25) {
                    return "low";
                } else if (value <= 39) {
                    return "normal";
                } else if (value <= 49) {
                    return "high";
                } else if (value >= 50) {
                    return "extremelyHigh";
                }
            } else if (age <= 12) {
                if (value <= 14) {
                    return "extremelyLow";
                } else if (value <= 16) {
                    return "low";
                } else if (value <= 21) {
                    return "normal";
                } else if (value <= 26) {
                    return "high";
                } else if (value >= 27) {
                    return "extremelyHigh";
                }
            } else if (age > 18) {
                if (value <= 8) {
                    return "extremelyLow";
                } else if (value <= 14) {
                    return "normal";
                } else if (value <= 20) {
                    return "high";
                } else if (value <= 29) {
                    return "extremelyHigh";
                } else if (value >= 30) {
                    return "abnormal";
                }
            }
        },
        heartRateValidation: function (value, age) {
            if (age <= 2) {
                if (value <= 69) {
                    return "extremelyLow";
                } else if (value <= 79) {
                    return "low";
                } else if (value <= 130) {
                    return "normal";
                } else if (value <= 159) {
                    return "high";
                } else if (value >= 160) {
                    return "extremelyHigh";
                }
            } else if (age <= 12) {
                if (value <= 59) {
                    return "extremelyLow";
                } else if (value <= 79) {
                    return "low";
                } else if (value <= 99) {
                    return "normal";
                } else if (value <= 129) {
                    return "high";
                } else if (value >= 130) {
                    return "extremelyHigh";
                }
            } else if (age > 18) {
                if (value <= 4) {
                    return "extremelyLow";
                } else if (value <= 50) {
                    return "low";
                } else if (value <= 100) {
                    return "normal";
                } else if (value <= 110) {
                    return "high";
                } else if (value <= 129) {
                    return "extremelyHigh";
                } else if (value >= 130) {
                    return "abnormal";
                }
            }
        },
        systolicValidation: function (value) {
            if (value <= 70) {
                return "unrecordable";
            } else if (value <= 80) {
                return "extremelyLow";
            } else if (value <= 100) {
                return "low";
            } else if (value <= 199) {
                return "normal";
            } else {
                return "extremelyHigh";
            }
        },
        diastolicValidation: function (value, age) {
            if (age <=12) {
                if (value < 55) {
                    return "low";
                } else if (value <= 62) {
                    return "normal";
                } else {
                    return "high";
                }
            }else {
                if (value < 60) {
                    return "low";
                } else if (value <= 90) {
                    return "normal";
                } else {
                    return "high";
                }
            }
        },
        temperatureValidation: function (value, age) {
            if (age >= 18) {
                if (value <= 35) {
                    return "extremelyLow";
                } else if (value <= 38.4) {
                    return "normal";
                } else {
                    return "extremelyHigh";
                }
            }
        },
        oxygenSaturationValidation: function (value, age) {
            if (value < 90) {
                return "extremelyLow";
            } else if( value < 95) {
                return "low";
            } else if(value <=100 ){
                return "normal";
            }
        }

    }

    $scope.getColorCode = function (name, value, age) {
        let status = '';
        if (name.toLowerCase().includes("body mass")) {
            status = validationUtils.bmiValidation(value, age);
        } else if (name.toLowerCase().includes("respiratory")) {
            status = validationUtils.respiratoryValidation(value, age);
        } else if (name.toLowerCase().includes("pulse")) {
            status = validationUtils.heartRateValidation(value, age);
        } else if (name.toLowerCase().includes("systolic")) {
            status = validationUtils.systolicValidation(value, age);
        } else if (name.toLowerCase().includes("temperature")) {
            status = validationUtils.temperatureValidation(value, age);
        } else if (name.toLowerCase().includes("oxygen")) {
            status = validationUtils.oxygenSaturationValidation(value, age);
        } else if (name.toLowerCase().includes("diastolic")) {
            status = validationUtils.diastolicValidation(value, age);
        }

        return genericColourCodes[status];
    }
}
