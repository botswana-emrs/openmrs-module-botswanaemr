/*
 *This is a helper object that contains functions to perform basic functions on a form field
 *
 *@param: selector string or JQuery object
 */
var fieldHelper = {
    $jqObj: function () {
        return {};
    },
    disable: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.attr('disabled', true).fadeTo(250, 0.25);


    },
    enable: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.removeAttr('disabled').fadeTo(250, 1);
    },
    makeReadonly: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.attr('readonly', true).fadeTo(250, 0.25);
    },
    removeReadonly: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.removeAttr('readonly').fadeTo(250, 1)
    },
    clear: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }
        if (this.$jqObj.is('input[type=text], select')) {
            this.$jqObj.val('');
        } else if (this.$jqObj.is('input[type="radio"], input[type="checkbox"]')) {
            this.$jqObj.removeAttr('checked');
        }


    },
    clearAllFields: function (args) {
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.find('input[type="text"], select').val('').change();


        this.$jqObj.find('input[type="radio"], input[type="checkbox"]').removeAttr('checked');
    },
    customizeDateTimePickerWidget: function(args) {
        //Remove the colon(:) in the date time picker
        if (args instanceof jQuery || args instanceof jq) {
            this.$jqObj = args;
        } else if (typeof args === 'string') {
            this.$jqObj = jq(args);
        }


        this.$jqObj.contents().filter(function() {
          return this.nodeType === 3;
        }).remove();




        // Insert label for time just before the timepicker field
        var $timeLabel = jq('<label/>').html('Time');
        $('.hfe-hours').before($timeLabel);
    }
};

const artUtils = {
    setServiceTypeData: function setServiceTypeData(patientId, visitNumber, hivStatus, patientIdSelector, visitIdSelector, serviceTypeSelector, modalSelector) {
        jq(patientIdSelector).val(patientId);
        jq(patientIdSelector).attr("data-hivStatus", hivStatus);
        jq(visitIdSelector).val(visitNumber);
        jq(serviceTypeSelector + " option").each(function () {
            let optionText = jq(this).html().toLowerCase();
            jq(this).hide();

            if (hivStatus === "positive") {
                if (optionText.includes("art")) {
                    jq(this).show();
                }
            } else {
                if (!optionText.includes("art")) {
                    jq(this).show();
                }
            }
        });

        jQuery(modalSelector).modal('show');
    }
};
