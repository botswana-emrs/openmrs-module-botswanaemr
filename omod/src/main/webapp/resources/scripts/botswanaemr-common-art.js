jq(document.body).ready(function () {
    htmlForm.cancel = function() {
        let url = new URL(window.location);
        let params = new URLSearchParams(url.search);
        let patientId = params.get('patientId');
        window.location = "/" + OPENMRS_CONTEXT_PATH + "/botswanaemr/programs/programs.page?patientId=" + patientId + "&programId=e5cc020a-8e22-11e8-b630-0242ac130002";
    };
});
