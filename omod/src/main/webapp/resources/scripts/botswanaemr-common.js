jq(document.body).ready(function () {
    jq('button#cancel').on('click', function(e){
        e.preventDefault();
        urlUtils.redirectToReturnUrl();
    });
});

const validateRequiredFields = function () {
    // Validate required fields inputs text and select
    let requiredFields = jq('span.required').siblings('select,input:text');
    requiredFields.push(jq('span.required').siblings().children('select,input:text'));
    let emptyRequiredFields = requiredFields.filter(function () {
        return jq(this).is(":visible") && jq(this).val() === '';
    });

    if (emptyRequiredFields.length > 0) {
        emptyRequiredFields.get(0).focus();
        jq().toastmessage('showErrorToast', 'Some required fields have missing values');
        emptyRequiredFields.first().focus();
        jq('html, body').animate({
            scrollTop: emptyRequiredFields.first().offset().top
        }, 1000);
        return false;
    }
    // Validate required fields radio buttons
    let requiredRadioFields = jq('span.required').siblings('input:radio').parent();
    let emptyRequiredRadioFields = requiredRadioFields.filter(function () {
        return jq(this).children('input:radio').is(":visible") && jq(this).children('input:radio').not(":checked").length === jq(this).children('input:radio').length;
    });

    if (emptyRequiredRadioFields.length > 0) {
        emptyRequiredRadioFields.first().focus();
        jq('html, body').animate({
            scrollTop: emptyRequiredRadioFields.first().offset().top
        }, 1000);
        jq().toastmessage('showErrorToast', 'Some required fields have missing values');
        return false;
    }

    let errorFields = jq('span.field-error:visible');
    if (errorFields.length > 0) {
        jq().toastmessage('showErrorToast', 'Some fields have validation errors');
        return false;
    }

    return true;
};

function requiredFieldsFilledTable(testRow) {
    let requiredFieldsFilled = true;

    // Check if each field for row are filled (includes the hidden class="selectedKitQuantity") to avoid saving custom lot no.
    testRow.find('input, select').each(function() {
        if (jq(this).val().trim() === '') {
            requiredFieldsFilled = false;
            return false;
        }
    });
    return requiredFieldsFilled;
}
