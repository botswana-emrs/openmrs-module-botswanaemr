jq(document.body).ready(() => {

    const valueNotSet = (value) => {
        return value === null || isNaN(value) || value === "";
    };

    jq("#submit").on("click", (event) => {});

    const validateFields = (targetFields) => {
        for (let targetField of targetFields) {
            if (getValue(targetField + ".value") === "" || isNaN(getValue(targetField + ".value"))) {
                jq().toastmessage('showErrorToast', "There were errors during validation." + " Correct them and submit again");
                return false;
            }
        }
        return true;
    }
});

