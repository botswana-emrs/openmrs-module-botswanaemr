//jq(document).ready(function () {
    function calculateBmi(weight, height) {
        let bmi = null;
        if (weight && height) {
            bmi = weight / ((height / 100) * (height / 100));
        }
        return bmi;
    }

    function updateBmi() {
        let wt = htmlForm.getValueIfLegal('weight.value');
        let ht = htmlForm.getValueIfLegal('height.value');

        let bmi = calculateBmi(wt, ht);

        if (bmi != null && !isNaN(bmi)) {
            setValue('bmi.value', bmi.toFixed(1));
        } else {
            setValue('bmi.value', '');
        }

        // Trigger validation
        let bmiInput = getField('bmi.value');
        let bmiId = jq(bmiInput).attr("id");
        jq("#" + bmiId).trigger("change");
    }

    jq("#weight, #height").change(function (e) {
        updateBmi();
    });

    function setAutocomplete(element) {
        element.autocomplete({
            source: function (request, response) {
                jq.getJSON(icd11SearchUrl, {
                    q: request.term
                }).success(function (data) {
                    var results = [];
                    for (var i in data) {
                        var result = {
                            label: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text(),
                            value: data[i].theCode + ' - ' + jq(jq.parseHTML(data[i].title)).text()
                        };
                        results.push(result);
                    }
                    response(results);
                });
            },
            minLength: 3,
            select: function (event, ui) {
                event.preventDefault();
                jq(element).val(ui.item.label);
                jq(element).attr('data-overlay', ui.item.value);
            },
        });
    }

    jq('.diagnosis-section input').focus(function () {
        setAutocomplete(jq(this));
    });

//})