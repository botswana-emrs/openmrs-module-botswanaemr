jq(document.body).ready(function () {
    jq('[id^=tbContactPhoneNumber] input[type=text]').attr('minlength', 7);

    var allTables,
    allRows,
    otherRows,
    firstRow,
    thisRow,
    nextRow,
    hiddenRows,
    lastVisibleRow;

    var showAddButtonForLastVisibleRow = function(container) {
        lastVisibleRow = container.find('tr:visible:last');
        lastVisibleRow.find('.add').show();
    };

    firstRow = jq('table.multi tbody tr:first-of-type');
    otherRows = jq('table.multi tbody tr:not(:first-of-type)');
    otherRows.hide(); //Hide all rows apart from first row if adding new form

    let nonEmptyRows = otherRows.find('input[type=text]').filter(function (){
        return this.value.length > 0
    }).parents('tr');
    nonEmptyRows.show();
    nonEmptyRows.find('.remove').hide();
    firstRow.find('.remove').hide(); //Hide the remove button from the first row

    jq('.add').click(function(e) { //Handle add button event
        e.preventDefault();
        thisRow = jq(this).parents('tr:first');
        nextRow = thisRow.next();

        if (nextRow.length > 0) {
            nextRow.show('slow');
            thisRow.find('.add').hide();
        }
        showAddButtonForLastVisibleRow(thisRow.parents('table:first'));
    });

    jq("#removeBtn").on("click", function(confirm) {
        jQuery("deleteModal").modal('show');
    });

    //.detach() keeps all jQuery data associated with the removed elements.
    // This method is useful when removed elements are to
    // be reinserted into the DOM at a later time.
    jq("#tb-contacts").on('click', '#deleteBtn', function () {
        jq(this).closest('tr').detach();
        jQuery("#deleteModal").modal('hide');
    });

    jq("#closeBtn").on("click", function() {
        jQuery("#deleteModal").modal('hide');
    });

    jq('#tbPositiveContact').change(function () {
        if ((getValue('tbPositiveContact.value') == 1065)) {
            jq('[id^="tbContactDiv"]').show()
        } else {
            jq('[id^="tbContactDiv"]').hide()
        }
    });

    //On page load, hide presumed section
    jq('[id^="presumedSectionDiv"]').hide();

    let isCoughYes = getValue('cough.value') == 1065;
    let isFeverYes = getValue('fever.value') == 1065;
    let isWeightLossYes = getValue('weightLoss.value') == 1065;
    let isNightSweatYes = getValue('nightSweat.value') == 1065;
    let isEnlargedLymphNodesYes = getValue('enlargedLymphNodes.value') == 1065;

    const conditionalHidePresumedSection = () => {
        if(isCoughYes || isFeverYes || isEnlargedLymphNodesYes || isNightSweatYes || isWeightLossYes) {
            jq('[id^="presumedSectionDiv"]').show();
        } else  {
            jq('[id^="presumedSectionDiv"]').hide();
        }
    }

    jq('#cough').change(() => {
        if ((getValue('cough.value') == 1065)) {
            isCoughYes = true;
            conditionalHidePresumedSection();
        } else {
            isCoughYes = false;
            conditionalHidePresumedSection();
        }
    });

    jq('#fever').change(() => {
        if ((getValue('fever.value') == 1065)) {
            isFeverYes = true;
            conditionalHidePresumedSection();
        } else {
            isFeverYes = false;
            conditionalHidePresumedSection();
        }
    });

    jq('#weightLoss').change(() => {
        if ((getValue('weightLoss.value') == 1065)) {
            isWeightLossYes = true;
            conditionalHidePresumedSection();
        } else {
            isWeightLossYes = false;
            conditionalHidePresumedSection();
        }
    });

    jq('#nightSweat').change(() => {
        if ((getValue('nightSweat.value') == 1065)) {
            isNightSweatYes = true;
            conditionalHidePresumedSection();
        } else {
            isNightSweatYes = false;
            conditionalHidePresumedSection();
        }
    });

    jq('#enlargedLymphNodes').change(() => {
        if ((getValue('enlargedLymphNodes.value') == 1065)) {
            isEnlargedLymphNodesYes = true;
            conditionalHidePresumedSection();
        } else {
            isEnlargedLymphNodesYes = false;
            conditionalHidePresumedSection();
        }
    });

    conditionalHidePresumedSection();
});
