jQuery(document).ready(function (jq) {
   // let activeTabId = jq.cookie('activeTab');
    let activeTabId = webUtils.getCookieValue('activeTab');

    if(activeTabId){
        if (jq('.nav').find('#' + activeTabId).length > 0) {

            // get the active tab
            let activeTab = jq('#' + activeTabId);
            // reset tab actions for siblings only
            activeTab.parent().find('.nav-link').removeClass('active').prop('aria-selected', false);
            activeTab.parent().siblings().find('.nav-link').removeClass('active').prop('aria-selected', false);

            // Get the corresponding target tabs
            let activeTabTarget = jq('#' + activeTabId.replace('-tab', ''));
            activeTabTarget.removeClass('show').removeClass('active');
            activeTabTarget.siblings().removeClass('show').removeClass('active');

            activeTab.toggleClass('active').prop('aria-selected', true);
            activeTabTarget.toggleClass('show').toggleClass('active');

        }
    } else {
        // find the first tab and make it active
        let firstTab = jq('.nav.nav-pills').find('.nav-link').first();
        // reset tab actions for siblings only
        firstTab.parent().find('.nav-link').removeClass('active').prop('aria-selected', false);
        firstTab.parent().siblings().find('.nav-link').removeClass('active').prop('aria-selected', false);

        // Get the corresponding target tabs
        let firstTabTarget = jq(firstTab.attr('href'));
        firstTabTarget.removeClass('show').removeClass('active');
        firstTabTarget.siblings().removeClass('show').removeClass('active');

        // make the first tab active
        firstTab.addClass('active').prop('aria-selected', true);
        jq(firstTab.attr('href')).addClass('show').addClass('active');

    }

    jq('a[data-toggle="pill"]').on('click', function(e) {
        activeTab = jq(e.target).prop('id');
        webUtils.setCookie('activeTab', activeTab, 60);
        // jq.cookie('activeTab', activeTab);
    });
});
