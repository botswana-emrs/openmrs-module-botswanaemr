(function( htmlForm, jq, undefined) {

    var beforeValidation = new Array();
    var beforeSubmit = new Array(); 		
    var propertyAccessorInfo = new Array();

    var whenObsHasValueThenDisplaySection = { };

    var tryingToSubmit = false;

    var returnUrl = '';

    var successFunction = function(result) {
        if (result.goToUrl) {
            emr.navigateTo({ applicationUrl: result.goToUrl });
        } else {
            goToReturnUrl(result.encounterId);
        }
    }

    var disableSubmitButton = function() {
        jq('.submitButton.confirm').attr('disabled', 'disabled');
        jq('.submitButton.confirm').addClass("disabled");
        if (tryingToSubmit) {
            jq('.submitButton.confirm .icon-spin').css('display', 'inline-block');
        }
    }

    var enableSubmitButton = function() {
        jq('.submitButton.confirm').removeAttr('disabled', 'disabled');
        jq('.submitButton.confirm').removeClass("disabled");
        jq('.submitButton.confirm .icon-spin').css('display', 'none');
    }

    var submitButtonIsDisabled = function() {
        return jq(".submitButton.confirm").is(":disabled");
    }

    var changeTimeWidgetClientTimeZone = function (clientDateTime) {
        getField('encounterDate.value').datepicker('setDate', clientDateTime)
        jq("#encounterDate").find(".hfe-hours").val(clientDateTime.getHours()).change();
        jq("#encounterDate").find(".hfe-minutes").val(clientDateTime.getMinutes()).change();
        jq("#encounterDate").find(".hfe-seconds").val(clientDateTime.getSeconds()).change();
    }

    var extractDate = function (date) {
        return moment(date.split('T')[0]).toDate()
    }

    var findAndHighlightErrors = function() {
        var containError = false
        var ary = jq(".autoCompleteHidden");
        jq.each(ary, function(index, value){
            if(value.value == "ERROR"){
                if(!containError){
                    alert("Autocomplete answer not valid");
                    var id = value.id;
                    id = id.substring(0,id.length-4);
                    jq("#"+id).focus();
                }
                containError=true;
            }
        });
        return containError;
    }

    var checkIfLoggedInAndErrorsCallback = function(isLoggedIn) {
        var state_beforeValidation=true;
        if (!isLoggedIn) {
            showAuthenticateDialog();
        }else{
            if (beforeValidation.length > 0){
                for (var i=0, l = beforeValidation.length; i < l; i++){
                    if (state_beforeValidation){
                        var fncn=beforeValidation[i];
                        state_beforeValidation=fncn.call(htmlForm);
                    }
                    else{
                        i=l;
                    }
                }
            }

            if (state_beforeValidation) {
                var anyErrors = findAndHighlightErrors();

                if (anyErrors) {
                    tryingToSubmit = false;
                    return;
                } else {
                    doSubmitHtmlForm();
                }
            }
            else {
                tryingToSubmit = false;
            }
        }
    }

    var showAuthenticateDialog = function() {
        jq('#passwordPopup').show();
        tryingToSubmit = false;
    }

    var goToReturnUrl = function(encounterId) {
        if (returnUrl) {
            // if returnUrl is encoded, decode it
             let decodedUrl = decodeURIComponent(returnUrl);

            // Check if the URL still contains encoded characters and decode again if necessary
            while (decodedUrl !== returnUrl) {
               returnUrl = decodedUrl;
               decodedUrl = decodeURIComponent(returnUrl);
            }

            if (encounterId) {
              var encounterParameter =  "encounterId=" + encounterId;
              var index = returnUrl.indexOf('?');
              if (index == -1) {
                  decodedUrl =  decodedUrl + '?' + encounterParameter;
              } else {
                  decodedUrl =  decodedUrl.substring(0,index+1) + encounterParameter + '&' +  decodedUrl.substring(index+1);
              }
            }
            location.href = decodedUrl;
        }
        else {
            if (typeof(parent) !== 'undefined') {
                parent.location.reload();
            } else {
                location.reload();
            }
        }
    }

    var doSubmitHtmlForm = function() {
        var state_beforeSubmit=true;
        if (beforeSubmit.length > 0){
            for (var i=0, l = beforeSubmit.length; i < l; i++){
                if (state_beforeSubmit){
                    var fncn=beforeSubmit[i];
                    state_beforeSubmit=fncn();
                }
                else{
                    i=l;
                }
            }
        }

        if (state_beforeSubmit && !submitButtonIsDisabled()){
            disableSubmitButton();
            var form = jq('#htmlform');
            var formData = false;
            if (window.FormData) {
                formData = new FormData(form[0]);
            }
			jq(".error", form).text(""); //clear errors

            jq.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: formData ? formData : form.serialize(),
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (result) {
                    if (result.success) {
                        tryingToSubmit = false;
                        successFunction(result);
                    }
                    else {
                        enableSubmitButton();
                        tryingToSubmit = false;
                        for (key in result.errors) {
                            showError(key, result.errors[key]);
                        }
                        for (key in result.errors) {
                            jq(document).scrollTop(jq('#' + key).offset().top - 100);
                            break;
                        }

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    emr.errorAlert('Unexpected error, please contact your System Administrator: ' + textStatus);
                }
            });
        }
        else {
            tryingToSubmit = false;
        }
    };

    htmlForm.submitHtmlForm = function()  {
        if (!tryingToSubmit) {
            tryingToSubmit = true;
            jq.getJSON(emr.fragmentActionLink('botswanaemr', 'enterHtmlForm', 'checkIfLoggedIn'), function(result) {
                checkIfLoggedInAndErrorsCallback(result.isLoggedIn);
            });
        }
    };

    htmlForm.loginThenSubmitHtmlForm = function() {
        jq('#passwordPopup').hide();
        var username = jq('#passwordPopupUsername').val();
        var password = jq('#passwordPopupPassword').val();
        jq('#passwordPopupUsername').val('');
        jq('#passwordPopupPassword').val('');
        jq.getJSON(emr.fragmentActionLink('botswanaemr', 'enterHtmlForm', 'authenticate', { user: username, pass: password }), submitHtmlForm);
    };

    htmlForm.cancel = function() {
        goToReturnUrl();
    };

    htmlForm.getValueIfLegal = function(idAndProperty) {
        var jqField = getField(idAndProperty);
        if (jqField && jqField.hasClass('illegalValue')) {
            return null;
        }
        return getValue(idAndProperty);
    };

    htmlForm.getPropertyAccessorInfo = function() {
        return propertyAccessorInfo;
    };

    htmlForm.getBeforeSubmit = function() {
        return beforeSubmit;
    };

    htmlForm.getBeforeValidation = function() {
        return beforeValidation;
    };

    htmlForm.setReturnUrl = function(url) {
        returnUrl = url;
    };

    htmlForm.getReturnUrl = function() {
        return returnUrl;
    };

    htmlForm.setSuccessFunction = function(fn) {
        successFunction = fn;
    };

    htmlForm.setEncounterStartDateRange = function(date , handletimezones) {
        if (getField('encounterDate.value')) {
            if (handletimezones) {
                var startDateWithClientTimezone = new Date(date)
                getField('encounterDate.value').datepicker('option', 'minDate', startDateWithClientTimezone)
            }else{
                getField('encounterDate.value').datepicker('option', 'minDate',  extractDate(date));
            }
        }
    };

    htmlForm.setEncounterStopDateRange = function(date, handletimezones) {
        if(jq("#encounterDate").find(".hfe-timezone")){
            jq("#encounterDate").find(".hfe-timezone").val(Intl.DateTimeFormat().resolvedOptions().timeZone)
        }
        if (getField('encounterDate.value')) {
            if (handletimezones) {
                var stopDateWithClientTimezone = new Date(date)
                getField('encounterDate.value').datepicker('option', 'maxDate', stopDateWithClientTimezone > new Date ? new Date : stopDateWithClientTimezone)
            }else{
                getField('encounterDate.value').datepicker('option', 'maxDate', extractDate(date));
            }
        }
    };

    htmlForm.setEncounterDate = function(date) {
        if (getField('encounterDate.value')) {
            getField('encounterDate.value').datepicker('setDate',  extractDate(date));
        }
    };

    htmlForm.adjustEncounterDatetimeWithTimezone = function(setDateTime ) {
        if (jq("#encounterDate").find(".hfe-timezone").length) {
            var dateWithClientTimeZone =  new Date(setDateTime)
            changeTimeWidgetClientTimeZone(dateWithClientTimeZone);
        };
    };

    htmlForm.disableEncounterDateManualEntry = function() {
        if (getField('encounterDate.value')) {
            getField('encounterDate.value').attr( 'readOnly' , 'true' );
        }
    };

    htmlForm.showDiv = function(id) {
        var div = document.getElementById(id);
        if ( div ) { div.style.display = ""; }
    };

    htmlForm.hideDiv = function(id) {
        var div = document.getElementById(id);
        if ( div ) { div.style.display = "none"; }
    }

    htmlForm.disableSubmitButton = function() {
        disableSubmitButton();
    }

    htmlForm.enableSubmitButton = function() {
        if (!tryingToSubmit) {
            enableSubmitButton();
        }
    }


}( window.htmlForm = window.htmlForm || {}, jQuery ));
window['$j'] = window['$j'] || jQuery;