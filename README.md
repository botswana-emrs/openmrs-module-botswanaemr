# README #

Configuration module for the BostwanaEMR distribution

### What is this repository for? ###

This module provides the following configuration meta-data:

* [ ] Look and feel customizations
* [ ] Patient identifiers
* [ ] Custom forms

### How do I get set up? ###

* Summary

1. Ensure that you have installed the necessary prerequisites before you start 
i.e. `Java 8`, `Maven 3.6.2`

   * Install the other prerequisites if you want to set up using the old way `MySQL 5.7`, `TomCat 7`. Jump to step 2: below for the recommended method
   
   * Download OpenMRS Reference Application 2.12.2 modules from: https://sourceforge.net/projects/openmrs/files/releases/OpenMRS_Reference_Application_2.12.2/referenceapplication-addons-2.12.2.zip/download

   * Unpack this zip file and copy all modules into the {OPENMRS_HOME}/modules folder

   * Download the OpenMRS Platform 2.5.0 from source

2. Alternatively, use [OpenMRS-SDK](https://wiki.openmrs.org/display/docs/OpenMRS+SDK) to install RefApp 2.12.2

3. Download the following modules and install/upgrade the existing module:
   
   * htmlformentry-4.3.0 from https://addons.openmrs.org/show/org.openmrs.module.htmlformentry
   
   * initializer-2.1.0: clone, build and deploy from here https://github.com/mekomsolutions/openmrs-module-initializer
   
   * patientqueueing-1.2.0 clone, build and deploy from here https://github.com/openmrs/openmrs-module-queue

4. Copy the `configurations` folder to the root folder (i.e. folder containing `openmrs-server.properties` file)

5. Download, build and install this module to integrate with the above packages

6. Run the integrated package as a normal OpenMRS reference Application
   using https://wiki.openmrs.org/display/docs/Reference+Application+2.11.0 as a sample guide

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### How to format the code and apply license headers ###

To ensure consistent code style and proper license headers, follow these instructions:

#### Formatting the code ####

1. Open a terminal or command prompt.
2. Navigate to the root directory of the project where the `pom.xml` file is located.
3. Run the following command to format the code:

   ```shell
   mvn formatter:format
   ```

4. After the formatting is complete, review the changes to ensure they meet your expectations.

#### Applying license headers ####

1. In the same terminal or command prompt, ensure you're still in the project's root directory.
2. Run the following command to apply license headers:

   ```shell
   mvn com.mycila:license-maven-plugin:format
   ```

3. This command will add or update license headers in all applicable files.
4. Review the changes made to ensure the license headers are correctly applied.

After completing both steps, commit the formatted files and updated license headers to your version control system if satisfied with the results.

Note: Make sure you have Maven installed and properly configured in your system path before running these commands.
