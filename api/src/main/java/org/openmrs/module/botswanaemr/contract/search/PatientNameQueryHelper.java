/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang.StringUtils.isEmpty;

public class PatientNameQueryHelper {
	
	private String name;
	
	private boolean includeIdentifierSearch;
	
	public static final String BY_NAME_PARTS = " concat_ws(' ',coalesce(given_name), coalesce(middle_name), coalesce(family_name)) like ";
	
	public static final String IDENTIFIER_PARTS = " pi.identifier like ";
	
	public PatientNameQueryHelper(String name, boolean includeIdentifierSearch) {
		this.name = name;
		this.includeIdentifierSearch = includeIdentifierSearch;
	}
	
	public String appendToWhereClause(String where) {
		WildCardParameter nameParameter = WildCardParameter.create(name);
		String nameSearchCondition = getNameSearchCondition(nameParameter);
		
		if (includeIdentifierSearch) {
			nameSearchCondition += getIdentifierSearchCondition(nameParameter);
		}
		where = isEmpty(nameSearchCondition) ? where : combine(where, "and", enclose(nameSearchCondition));
		return where;
	}
	
	private String getNameSearchCondition(WildCardParameter wildCardParameter) {
		if (wildCardParameter.isEmpty())
			return "";
		String query_by_name_parts = "";
		for (String part : wildCardParameter.getParts()) {
			if (!"".equals(query_by_name_parts)) {
				query_by_name_parts += " and " + BY_NAME_PARTS + " '" + part + "'";
			} else {
				query_by_name_parts += BY_NAME_PARTS + " '" + part + "'";
			}
		}
		return query_by_name_parts;
	}
	
	private String getIdentifierSearchCondition(WildCardParameter wildCardParameter) {
		if (wildCardParameter.isEmpty())
			return "";
		String query_by_name_parts = "";
		for (String part : wildCardParameter.getParts()) {
			if (!"".equals(query_by_name_parts)) {
				query_by_name_parts += " or " + IDENTIFIER_PARTS + " '" + part + "'";
			} else {
				if (StringUtils.isEmpty(query_by_name_parts)) {
					query_by_name_parts += " or ";
				}
				query_by_name_parts += IDENTIFIER_PARTS + " '" + part + "'";
			}
		}
		return query_by_name_parts;
	}
	
	private String combine(String query, String operator, String condition) {
		return String.format("%s %s %s", query, operator, condition);
	}
	
	private String enclose(String value) {
		return String.format("(%s)", value);
	}
}
