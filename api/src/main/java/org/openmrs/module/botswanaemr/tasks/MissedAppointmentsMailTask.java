/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.tasks;

import lombok.SneakyThrows;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.scheduler.tasks.AbstractTask;
import org.openmrs.ui.framework.SimpleObject;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;

public class MissedAppointmentsMailTask extends AbstractTask {
	
	private static final String PROP_EMR_SERVER = "botswanaemr.patientFollowupUrl";
	
	AppointmentService appointmentService = Context.getService(AppointmentService.class);
	
	@SneakyThrows
	@Override
	public void execute() {
		BotswanaEmrService service = Context.getService(BotswanaEmrService.class);
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", LocalDate.now().minusWeeks(1).toString());
		params.put("endDate", LocalDate.now().toString());
		params.put("signature", "Powered By OpenMRS");
		params.put("missedAppointmentsLink",
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.patientFollowupUrl",
		        "http://localhost:8600/openmrs/botswanaemr/appointments/patientFollowup.page"));
		params.put("clinicDays", getDailyAppointmentSummaryCount());
		String subject = "Patient Appointment Summary";
		
		String body = Context.getService(BotswanaEmrService.class).getFreemarkerTemplateContent("missedAppointments.ftlh",
		    params);
		service.getBotswanaEmrEmailReportConfigs("botswanaemr.tb.missed.appointment.register", null).forEach(config -> {
			String cccList = config.getCccList();
			service.sendGeneralEmail(cccList, subject, body);
			config.setLastEmailSent(new Date());
			service.saveBotswanaEmrEmailReportConfig(config);
			
		});
		
	}
	
	public String generateLink(String globalProperty) {
		String emrServerUrl = Context.getAdministrationService().getGlobalProperty(globalProperty);
		
		if ((emrServerUrl == null) || "".equals(emrServerUrl)) {
			emrServerUrl = "";
		}
		return new MessageFormat(emrServerUrl).format(new Object[] {});
	}
	
	public List<SimpleObject> getDailyAppointmentSummaryCount() {
		List<SimpleObject> days = new ArrayList<>();
		SimpleObject so = new SimpleObject();
		so.put("date", BotswanaEmrUtils.getStartOfDay());
		so.put("scheduled", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.SCHEDULED).size());
		so.put("completed", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.COMPLETED).size());
		so.put("pending", (long) appointmentService.getAppointmentsByConstraints(BotswanaEmrUtils.getStartOfDay(),
		    BotswanaEmrUtils.getEndOfDay(), null, null, null, Appointment.AppointmentStatus.MISSED).size());
		days.add(so);
		
		return days;
	}
}
