/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Culture and DST reports summary
 *
 * @author corneliouzbett
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@NoArgsConstructor
public class CultureDstReport extends BaseSummary implements Serializable, Summary {
	
	//Domain specific constants
	private final static String DATE = "166848AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String DRUG = "1193AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String RESISTANCE = "159984AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private Encounter encounter;
	
	private String date;
	
	private String drug;
	
	private String resistance;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		CultureDstReport dstReport = new CultureDstReport();
		dstReport.setEncounter(encounter);
		observations.forEach(obs -> {
			if (obs.getConcept().getUuid().equals(DATE)) {
				dstReport.setDate(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(DRUG)) {
				dstReport.setDrug(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(RESISTANCE)) {
				dstReport.setResistance(getValueCoded(obs));
			} else {
				log.debug("No available Culture & DST Report for this encounter");
			}
		});
		
		return dstReport;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return Arrays.asList(getConcept(DATE), getConcept(DRUG), getConcept(RESISTANCE));
	}
	
}
