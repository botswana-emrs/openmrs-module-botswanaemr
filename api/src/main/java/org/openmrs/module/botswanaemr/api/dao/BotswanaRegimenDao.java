/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao;

import org.openmrs.Concept;
import org.openmrs.api.APIException;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;

import java.util.List;

public interface BotswanaRegimenDao {
	
	//Regimen lines DAO methods
	public List<RegimenLine> getAllRegimenLine() throws DAOException;
	
	public RegimenLine getRegimenLine(Integer EhrAppointmentTypeId) throws DAOException;
	
	public RegimenLine getRegimenLineByUuid(String uuid) throws DAOException;
	
	public RegimenLine saveRegimenLine(RegimenLine regimenLine) throws DAOException;
	
	public RegimenLine retireRegimenLine(RegimenLine regimenLine, String reason) throws DAOException;
	
	public RegimenLine unretireRegimenLine(RegimenLine regimenLine) throws DAOException;
	
	public void purgeRegimenLine(RegimenLine regimenLine) throws DAOException;
	
	//Regimen DAO methods
	public Regimen saveRegimen(Regimen regimen) throws DAOException;
	
	public Regimen getRegimen(Integer regimenId) throws DAOException;
	
	public Regimen getRegimenByRegimenLine(RegimenLine regimenLine) throws DAOException;
	
	public List<Regimen> getAllRegimen(boolean includeVoided) throws DAOException;
	
	public Regimen updateRegimen(Regimen regimen) throws DAOException;
	
	public Regimen voidRegimen(Regimen regimen, String voidReason) throws DAOException;
	
	//regimen component DAO
	public RegimenComponent saveRegimenComponent(RegimenComponent regimenComponent) throws DAOException;
	
	public RegimenComponent getRegimenComponentById(Integer regimenComponentId) throws DAOException;
	
	public List<RegimenComponent> getAllRegimenComponent(boolean includeVoided) throws DAOException;
	
	public RegimenComponent getRegimenComponentByRegimen(Regimen regimen) throws DAOException;
	
	public List<RegimenComponent> getAllRegimenComponentByRegimen(Regimen regimen) throws DAOException;
	
	public RegimenComponent updateRegimenComponent(RegimenComponent regimenComponent) throws DAOException;
	
	public RegimenComponent voidRegimenComponent(RegimenComponent regimenComponent, String voidReason) throws DAOException;
	
	RegimenComponentDrug saveRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) throws DAOException;
	
	RegimenComponentDrug getRegimenComponentDrugById(Integer regimenComponentDrugId) throws DAOException;
	
	List<RegimenComponentDrug> getAllRegimenComponentDrugs(boolean includeVoided) throws DAOException;
	
	RegimenComponentDrug updateRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) throws DAOException;
	
	RegimenComponentDrug voidRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug, String voidReason)
	        throws DAOException;
	
	Regimen getRegimenByConcept(Concept regimenConcept);
	
	List<RegimenComponentDrug> getRegimenComponentDrugByRegimenConcept(Concept regimenConcept);
	
	List<RegimenComponentDrug> getRegimenComponentDrugByRegimenComponent(RegimenComponent regimenComponent);
	
	List<RegimenComponent> getRegimenComponentsByConcept(Concept regimenConcept);
}
