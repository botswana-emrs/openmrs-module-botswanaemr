/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao;

import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.APIException;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.botswanaemr.lab.Lab;
import org.openmrs.module.botswanaemr.lab.LabTest;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface LabDAO {
	
	/**
	 * LAB
	 */
	public Lab saveLab(Lab lab) throws DAOException;
	
	public Lab getLabByName(String name) throws DAOException;
	
	public List<Lab> getAllLab() throws DAOException;
	
	public List<Lab> getAllActiveLab() throws DAOException;
	
	public Lab getLabByRole(Role role) throws DAOException;
	
	public List<Lab> getLabByRoles(List<Role> roles) throws DAOException;
	
	public Lab getLabById(Integer labId) throws DAOException;
	
	public void deleteLab(Lab lab) throws DAOException;
	
	/**
	 * LAB TEST
	 */
	
	public LabTest saveLabTest(LabTest labTest) throws DAOException;
	
	public LabTest getLabTestById(Integer labTestId) throws DAOException;
	
	public LabTest getLabTestByOrder(Order order) throws DAOException;
	
	public LabTest getLabTestBySampleNumber(String sampleNumber) throws DAOException;
	
	public List<LabTest> getLatestLabTestByDate(Date today, Date nextDay, Lab lab) throws DAOException;
	
	public void deleteLabTest(LabTest labtest) throws DAOException;
	
	public List<LabTest> getLatestLabTestByDateOnly(Date date) throws DAOException, ParseException;
	
	/**
	 * Orders
	 */
	public List<LabTest> getLaboratoryTestsByDateAndPatient(Date date, Patient patient) throws DAOException, ParseException;
	
	public List<Order> getOrders(Patient patient, Date date, Concept concept) throws DAOException, ParseException;
	
	public LabTest getLaboratoryTest(Encounter encounter) throws APIException;
	
	public List<LabTest> getAllLabTests() throws DAOException;
	
	public List<LabTest> getLaboratoryTestsByDiscontinuedDate(Date date, Set<Concept> tests, List<Patient> patients)
	        throws ParseException, DAOException;
	
	public List<LabTest> getLaboratoryTestsByPatient(Patient patient) throws DAOException;
	
	List<LabTest> getAllCompletedLabTestOrders(@NotNull Patient patient);
	
	public List<Concept> searchConceptsByNameAndClass(String text, ConceptClass clazz) throws DAOException;
	
	public List<Concept> searchConceptsByNameAndClass(String text, ConceptClass clazz, ConceptClass clazz2)
	        throws DAOException;
	
	public List<LabTest> getLaboratoryTestListByEncounter(Encounter encounter) throws DAOException;
	
}
