/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.inventory;

public class InventoryConstants {
	
	public static final String HEADER_NAME = "Name";
	
	public static final String HEADER_CODE = "Code";
	
	public static final String HEADER_CONCEPT = "Concept";
	
	public static final String HEADER_DESCRIPTION = "Description";
	
	public static final String HEADER_UNIT_ISSUE = "Unit of Issue";
	
	public static final String HEADER_UNIT_MEASURE = "unit of measure";
	
	public static final String HEADER_DEPARTMENT = "Department";
	
	public static final String HEADER_SECTION = "Program / Section";
	
	public static final String HEADER_CATEGORY = "Category";
	
	public static final String HEADER_STATUS = "status";
	
	public static final String HEADER_MIN_STOCK = "minimumStockLevel";
	
	public static final String HEADER_MAX_STOCK = "maximumStockLevel";
	
	public static final String HEADER_CLASS = "Class";
	
	public static final String HEADER_SUB_CLASS = "Sub-class";
	
	public static final String HEADER_DRUG = "Drug/Concept";
	
	public static final String DEFAULT_DEPARTMENT = "N/A";
}
