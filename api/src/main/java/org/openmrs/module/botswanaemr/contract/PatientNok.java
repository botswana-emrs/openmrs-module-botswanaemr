/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

import java.io.Serializable;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class PatientNok implements Serializable {
	
	AttributeObject idType;
	
	AttributeObject contactType;
	
	AttributeObject idNumber;
	
	String givenName;
	
	String middleName;
	
	String familyName;
	
	String district;
	
	String city;
	
	AttributeObject relationShip;
	
	AttributeObject email;
	
	AttributeObject contact1;
	
	AttributeObject contact2;
	
	AttributeObject contact3;
	
	AttributeObject contact4;
	
	AttributeObject contact5;
	
	NokPerson personA;
	
	NokPerson personB;
	
	public @Data static class NokPerson {
		
		String uuid;
		
		String name;
	}
	
	public @Data static class AttributeObject {
		
		String uuid;
		
		String value;
	}
}
