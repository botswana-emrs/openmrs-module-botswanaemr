/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.search.annotations.Field;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Concept;
import org.openmrs.Person;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;

@Data
@Entity
@Table(name = "hts_lab_quality_control_test")
public class LabQualityControlTest extends BaseOpenmrsObject {
	
	@Column(name = "type_of_test")
	private String typeOfTest;
	
	@Column(name = "lot_number")
	private String lotNumber;
	
	@Column(name = "expiry_date")
	private Date expiryDate;
	
	@ManyToOne
	@JoinColumn(name = "interpretation", foreignKey = @ForeignKey(name = "hts_lab_quality_control_test_concept_FK_interpretation"))
	private Concept interpretation;
	
	@ManyToOne
	@JoinColumn(name = "kit_name", foreignKey = @ForeignKey(name = "hts_lab_quality_control_test_concept_FK_kit_name"))
	private Concept kitName;
	
	@ManyToOne
	@JoinColumn(name = "quality_control", foreignKey = @ForeignKey(name = "hts_lab_quality_control_test_concept_FK_quality_control"))
	private Concept qualityControl;
	
	public LabQualityControl getLabQualityControl() {
		return labQualityControl;
	}
	
	@ManyToOne
	@JoinColumn(name = "test_result", foreignKey = @ForeignKey(name = "hts_lab_quality_control_test_concept_FK_test_result"))
	private Concept testResult;
	
	@Column(name = "incident")
	private String incident;
	
	@Column(name = "corrective_action_taken")
	private String correctiveActionTaken;
	
	@ManyToOne
	@JoinColumn(name = "acceptable", foreignKey = @ForeignKey(name = "fk_acceptable_reference"))
	private Concept acceptable;
	
	@ManyToOne
	@JoinColumn(name = "authorized_by")
	private Person authorizedBy;
	
	@ManyToOne
	@JoinColumn(name = "stock_room_id")
	private Stockroom stockroom;
	
	@Column(name = "parent_id")
	@Field
	private Integer parent = 0;
	
	public void setLabQualityControl(LabQualityControl labQualityControl) {
		this.labQualityControl = labQualityControl;
	}
	
	@ManyToOne
	@JoinColumn(name = "lqc_id")
	private LabQualityControl labQualityControl;
	
	@Id
	@GeneratedValue
	@Column(name = "lqc_test_id")
	private Integer id;
	
	@Override
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 40;
	}
	
}
