/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.regimen;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Concept;
import org.openmrs.Drug;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "bt_regimen_component_drug")
@BatchSize(size = 25)
public class RegimenComponentDrug extends BaseOpenmrsData {
	
	@Getter
	@Setter
	@Id
	@GeneratedValue
	@Column(name = "regimen_component_drug_id")
	private Integer regimenComponentDrugId;
	
	@OneToMany
	@ManyToOne
	@JoinColumn(name = "regimen_id", foreignKey = @ForeignKey(name = "bt_regimen_component_drug_regimen_FK"))
	private Regimen regimen;
	
	@ManyToOne
	@JoinColumn(name = "regimen_component_id", foreignKey = @ForeignKey(name = "bt_regimen_component_drug_regimen_component_FK"))
	private RegimenComponent regimenComponent;
	
	@ManyToOne
	@JoinColumn(name = "drug_id", foreignKey = @ForeignKey(name = "bt_regimen_component_drug_drug_FK"))
	private Drug drug;
	
	private Double dose;
	
	@ManyToOne
	@JoinColumn(name = "units_id", foreignKey = @ForeignKey(name = "bt_regimen_component_drug_units_FK"))
	private Concept units;
	
	@ManyToOne
	@JoinColumn(name = "frequency_id", foreignKey = @ForeignKey(name = "bt_regimen_component_drug_frequency_FK"))
	private Concept frequency;
	
	private String strength;
	
	@Override
	public Integer getId() {
		return getRegimenComponentDrugId();
	}
	
	@Override
	public void setId(Integer id) {
		setRegimenComponentDrugId(id);
	}
	
}
