/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.Data;

@Data
public class SimplifiedStockTake {
	
	private Integer id;
	
	private Integer stockRoomId;
	
	private Integer itemStockQuantity;
	
	private Integer enteredQuantityInStore;
	
	private String operationDate;
	
	private String expiryDate;
	
	private String batchNumber;
	
	private String todayDate;
	
	private String officer;
	
	private String status;
	
	private String itemCode;
	
	private String itemName;
	
	private String itemStockUuid;
	
	private String comments;
	
	private String variance;
	
	private String varianceReason;
}
