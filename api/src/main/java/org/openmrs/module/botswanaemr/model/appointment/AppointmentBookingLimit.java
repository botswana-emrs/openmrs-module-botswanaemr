/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.appointment;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "appointment_booking_limit")
@Data
public class AppointmentBookingLimit extends BaseOpenmrsData {
	
	private static final String DAY_MON = "Monday";
	
	private static final String DAY_TUE = "Tuesday";
	
	private static final String DAY_WED = "Wednesday";
	
	private static final String DAY_THU = "Thursday";
	
	private static final String DAY_FRI = "Friday";
	
	private static final String DAY_SAT = "Saturday";
	
	private static final String DAY_SUN = "Sunday";
	
	@ManyToOne
	@JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "appointment_booking_limit_location_FK"))
	private Location location;
	
	@Column(name = "day", nullable = false)
	private String day;
	
	@Column(name = "booking_limit", nullable = false)
	private int limit;
	
	@Column(name = "comment")
	private String comment;
	
	@Id
	@GeneratedValue
	@Column(name = "appointment_booking_id")
	private Integer id;
	
	@Override
	public Integer getId() {
		return null;
	}
	
	@Override
	public void setId(Integer id) {
		
	}
	
	@Override
	public int hashCode() {
		return 40;
	}
}
