/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.Data;
import org.openmrs.Order;
import org.openmrs.Patient;

@Data
public class SimplifiedLabOrder {
	
	private String pin;
	
	private String status;
	
	private String patientNames;
	
	private String requestedBy;
	
	private String sampleDrawDate;
	
	private String orderingFacility;
	
	private Patient patient;
	
	private Order order;
	
	private Integer encounterId;
}
