/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.regimen;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class SimplifiedRegimenComponentDrug {
	
	private Integer id;
	
	private Integer regimenId;
	
	private Integer regimenComponentId;
	
	private Integer drug;
	
	private String drugUuid;
	
	private String drugName;
	
	private Double dose;
	
	private Integer unitsId;
	
	private String units;
	
	private String unitsUuid;
	
	private Integer frequencyId;
	
	private String frequency;
	
	private String frequencyUuid;
	
	private String strength;
	
}
