/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.Encounter;
import org.openmrs.Patient;
import org.openmrs.User;

import java.util.Date;

public class CaseLinkage {
	
	private Integer caseLinkageId;
	
	public Integer getCaseLinkageId() {
		return caseLinkageId;
	}
	
	public void setCaseLinkageId(Integer caseLinkageId) {
		this.caseLinkageId = caseLinkageId;
	}
	
	public Encounter getCurrentEncounter() {
		return currentEncounter;
	}
	
	public void setCurrentEncounter(Encounter currentEncounter) {
		this.currentEncounter = currentEncounter;
	}
	
	public Encounter getLinkedEncounter() {
		return linkedEncounter;
	}
	
	public void setLinkedEncounter(Encounter linkedEncounter) {
		this.linkedEncounter = linkedEncounter;
	}
	
	private Encounter currentEncounter;
	
	private Encounter linkedEncounter;
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	private Patient patient;
	
	public User getCreator() {
		return creator;
	}
	
	public void setCreator(User creator) {
		this.creator = creator;
	}
	
	private User creator;
	
	public Date getLinkedOn() {
		return linkedOn;
	}
	
	public void setLinkedOn(Date linkedOn) {
		this.linkedOn = linkedOn;
	}
	
	private Date linkedOn;
	
	public Cases getCases() {
		return cases;
	}
	
	public void setCases(Cases cases) {
		this.cases = cases;
	}
	
	private Cases cases;
	
}
