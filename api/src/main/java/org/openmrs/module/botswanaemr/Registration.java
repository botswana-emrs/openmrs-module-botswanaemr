/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr;

import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseChangeableOpenmrsData;
import org.openmrs.Location;
import org.openmrs.Patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "registration")
@BatchSize(size = 25)
public class Registration extends BaseChangeableOpenmrsData {
	
	public static final long serialVersionUID = 2L;
	
	// Fields
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "registration_id")
	private Integer registrationId;
	
	@Column(name = "registration_datetime", nullable = false, length = 19)
	private Date registrationDatetime;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "patient_id")
	private Patient patient;
	
	@ManyToOne
	@JoinColumn(name = "location_id")
	private Location location;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "payment_method")
	private PaymentMethod paymentMethod;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "service_provider")
	private ServiceProvider serviceProvider;
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public double getBillableAmount() {
		return billableAmount;
	}
	
	public void setBillableAmount(double billableAmount) {
		this.billableAmount = billableAmount;
	}
	
	@Column(name = "billable_amount")
	private double billableAmount;
	
	public double getAmountPaid() {
		return amountPaid;
	}
	
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	
	@Column(name = "payment_amount")
	private double amountPaid;
	
	public String getPaymentCode() {
		return paymentCode;
	}
	
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	
	@Column(name = "payment_code")
	private String paymentCode;
	
	//////////////////////////////////
	// Constructors
	
	/** default constructor */
	public Registration() {
	}
	
	/**
	 * @param registrationId <strong>Should</strong> set payment id
	 */
	public Registration(Integer registrationId) {
		this.registrationId = registrationId;
	}
	
	// Property accessors
	
	/**
	 * @return Returns the registrationDatetime.
	 */
	public Date getRegistrationDatetime() {
		return registrationDatetime;
	}
	
	/**
	 * @param registrationDatetime The registrationDatetime to set.
	 */
	public void setRegistrationDatetime(Date registrationDatetime) {
		this.registrationDatetime = registrationDatetime;
	}
	
	/**
	 * @return Returns the registrationId.
	 */
	public Integer getRegistrationId() {
		return registrationId;
	}
	
	/**
	 * @param registrationId The registrationId to set.
	 */
	public void setRegistrationId(Integer registrationId) {
		this.registrationId = registrationId;
	}
	
	/**
	 * @return Returns the paymentMethod.
	 */
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	
	/**
	 * @param paymentMethod The paymentMethod to set.
	 */
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	/**
	 * @return Returns the serviceProvider.
	 */
	public ServiceProvider getServiceProvider() {
		return serviceProvider;
	}
	
	/**
	 * @param serviceProvider The serviceProvider to set.
	 */
	public void setServiceProvider(ServiceProvider serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	
	/**
	 * @return Returns the location.
	 */
	public Location getLocation() {
		return location;
	}
	
	/**
	 * @param location The location to set.
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	
	/**
	 * @return Returns the patient.
	 */
	public Patient getPatient() {
		return patient;
	}
	
	/**
	 * @param patient The patient to set.
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	@Override
	public Integer getId() {
		return registrationId;
	}
	
	@Override
	public void setId(Integer registrationId) {
		this.registrationId = registrationId;
	}
	
	/**
	 * @see java.lang.Object#toString() <strong>Should</strong> not fail with empty object
	 */
	@Override
	public String toString() {
		String ret = "";
		ret += registrationId == null ? "(no ID) " : registrationId.toString() + " ";
		ret += this.getRegistrationDatetime() == null ? "(no Date) " : this.getRegistrationDatetime().toString() + " ";
		ret += this.getPaymentMethod() == null ? "(no Type) " : this.getPaymentMethod().getName() + " ";
		ret += this.getLocation() == null ? "(no Location) " : this.getLocation().getName() + " ";
		ret += this.getPatient() == null ? "(no Patient) " : this.getPatient().getPatientId().toString() + " ";
		ret += this.getServiceProvider() == null ? "(no Service Provider) " : this.getServiceProvider().getName() + " ";
		return "Payment: [" + ret + "]";
	}
}
