/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

public class SimplifiedPatient {
	
	public Integer getPatientId() {
		return patientId;
	}
	
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	
	private Integer patientId;
	
	private String identifier;
	
	public String getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getAge() {
		return age;
	}
	
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getRegisteredBy() {
		return registeredBy;
	}
	
	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}
	
	public String getRegisteredDate() {
		return registeredDate;
	}
	
	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public void setVisitNumber(String visitNumber) {
		this.visitNumber = visitNumber;
	}
	
	public String getVisitNumber() {
		return visitNumber;
	}
	
	public String getAssignedTo() {
		return assignedTo;
	}
	
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	
	public String getPatientUuid() {
		return patientUuid;
	}
	
	public void setPatientUuid(String patientUuid) {
		this.patientUuid = patientUuid;
	}
	
	public Integer getId() {
		return this.patientQueueId;
	}
	
	public void setId(Integer integer) {
		this.patientQueueId = integer;
	}
	
	private String name;
	
	private String gender;
	
	private String age;
	
	private String registeredBy;
	
	private String registeredDate;
	
	private String status;
	
	private String dateOfBirth;
	
	private String visitNumber;
	
	private String assignedTo;
	
	private String patientUuid;
	
	private Integer patientQueueId;
}
