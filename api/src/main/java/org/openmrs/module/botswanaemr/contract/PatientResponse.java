/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract;

import lombok.Getter;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PatientResponse {
	
	@Getter
	private String uuid;
	
	private Date birthDate;
	
	private String extraIdentifiers;
	
	@Getter
	private int patientId;
	
	@Getter
	private Date deathDate;
	
	@Getter
	private String identifier;
	
	private String addressFieldValue;
	
	@Getter
	private String givenName;
	
	@Getter
	private String middleName;
	
	@Getter
	private String familyName;
	
	private String name;
	
	@Getter
	private String gender;
	
	private String registeredDate;
	
	private String activeVisitUuid;
	
	private String customAttribute;
	
	private Object patientProgramAttributeValue;
	
	private Boolean hasBeenAdmitted;
	
	@Getter
	private Boolean dead;
	
	@Getter
	private int visitNumber;
	
	private String status;
	
	@Getter
	private Date dateCreated;
	
	private String dateOfBirth;
	
	@lombok.Setter
	@lombok.Getter
	private String hivStatus;
	
	@lombok.Setter
	@lombok.Getter
	private Date dateLastTested;
	
	@lombok.Setter
	@lombok.Getter
	private Date lastVisitDate;
	
	@lombok.Setter
	@lombok.Getter
	private String reasonForTest;
	
	public String getAge() {
		if (birthDate == null)
			return null;
		
		// Use default end date as today.
		Calendar today = Calendar.getInstance();
		
		// If date given is after date of death then use date of death as end date
		if (getDeathDate() != null && today.getTime().after(getDeathDate())) {
			today.setTime(getDeathDate());
		}
		
		Calendar bday = Calendar.getInstance();
		bday.setTime(birthDate);
		
		int age = today.get(Calendar.YEAR) - bday.get(Calendar.YEAR);
		
		// Adjust age when today's date is before the person's birthday
		int todaysMonth = today.get(Calendar.MONTH);
		int bdayMonth = bday.get(Calendar.MONTH);
		int todaysDay = today.get(Calendar.DAY_OF_MONTH);
		int bdayDay = bday.get(Calendar.DAY_OF_MONTH);
		
		if (todaysMonth < bdayMonth) {
			age--;
		} else if (todaysMonth == bdayMonth && todaysDay < bdayDay) {
			// we're only comparing on month and day, not minutes, etc
			age--;
		}
		
		return Integer.toString(age);
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getAddressFieldValue() {
		return addressFieldValue;
	}
	
	public void setAddressFieldValue(String addressFieldValue) {
		this.addressFieldValue = addressFieldValue;
	}
	
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getRegisteredDate() {
		return BotswanaEmrUtils.formatDateWithoutTime(this.dateCreated, "dd-MM-yyyy");
	}
	
	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}
	
	public String getActiveVisitUuid() {
		return activeVisitUuid;
	}
	
	public void setActiveVisitUuid(String activeVisitUuid) {
		this.activeVisitUuid = activeVisitUuid;
	}
	
	public String getCustomAttribute() {
		return customAttribute;
	}
	
	public void setCustomAttribute(String customAttribute) {
		this.customAttribute = customAttribute;
	}
	
	public Object getPatientProgramAttributeValue() {
		return patientProgramAttributeValue;
	}
	
	public void setPatientProgramAttributeValue(Object patientProgramAttributeValue) {
		this.patientProgramAttributeValue = patientProgramAttributeValue;
	}
	
	public Boolean getHasBeenAdmitted() {
		return hasBeenAdmitted;
	}
	
	public void setHasBeenAdmitted(Boolean hasBeenAdmitted) {
		this.hasBeenAdmitted = hasBeenAdmitted;
	}
	
	public String getExtraIdentifiers() {
		return extraIdentifiers;
	}
	
	public void setExtraIdentifiers(String extraIdentifiers) {
		this.extraIdentifiers = extraIdentifiers;
	}
	
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	
	public void setDead(Boolean dead) {
		this.dead = dead;
	}
	
	public void setVisitNumber(int visitNumber) {
		this.visitNumber = visitNumber;
	}
	
	public String getName() {
		return BotswanaEmrUtils.formatPersonName(this.givenName, this.middleName, this.familyName);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getStatus() {
		return this.dead ? "Expired" : "Active";
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateOfBirth() {
		return BotswanaEmrUtils.formatDateWithoutTime(this.birthDate, "dd-MM-yyyy");
	}
	
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	/**
	 * Used to serialize Java.util.Date, which is not a common JSON type, so we have to create a custom
	 * serialize method;
	 */
	@Component
	public static class JsonDateSerializer extends JsonSerializer<Date> {
		
		private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		
		@Override
		public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException {
			String formattedDate = dateFormat.format(date);
			gen.writeString(formattedDate);
		}
	}
}
