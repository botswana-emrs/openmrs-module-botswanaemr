/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;

public class PatientProgramQueryHelper {
	
	private String programUuid;
	
	public PatientProgramQueryHelper(String uuid) {
		this.programUuid = uuid;
	}
	
	public String appendToJoinClause(String join) {
		if (StringUtils.isEmpty(this.programUuid)) {
			return join;
		}
		
		return join += " INNER JOIN patient_program pp on pp.patient_id = p.person_id\n"
		        + "            AND pp.voided=FALSE\n"
		        + "            AND pp.program_id=(SELECT pm.program_id FROM program pm WHERE pm.uuid='" + this.programUuid
		        + "') \n";
	}
}
