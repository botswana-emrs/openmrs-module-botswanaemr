/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import lombok.Data;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "hts_test_result")
public class HtsTestResult extends BaseOpenmrsData {
	
	@Column(name = "lot_number")
	private String lotNumber;
	
	@Column(name = "expiry_date")
	private Date expiryDate;
	
	@Column(name = "kit_name")
	private String kitName;
	
	@Column(name = "kit_results")
	private String kitResults;
	
	@Column(name = "result_number")
	private String resultNumber;
	
	@ManyToOne
	@JoinColumn(name = "specimen_result_id")
	private HtsSpecimenResult htsSpecimenResult;
	
	@Id
	@GeneratedValue
	@Column(name = "result_id")
	private Integer id;
	
	@Override
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getLotNumber() {
		return lotNumber;
	}
	
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getKitName() {
		return kitName;
	}
	
	public void setKitName(String kitName) {
		this.kitName = kitName;
	}
	
	public HtsSpecimenResult getHtsSpecimenResult() {
		return htsSpecimenResult;
	}
	
	public void setHtsSpecimenResult(HtsSpecimenResult htsSpecimenResult) {
		this.htsSpecimenResult = htsSpecimenResult;
	}
	
	public String getKitResults() {
		return kitResults;
	}
	
	public void setKitResults(String kitResults) {
		this.kitResults = kitResults;
	}
	
	public String getResultNumber() {
		return resultNumber;
	}
	
	public void setResultNumber(String resultNumber) {
		this.resultNumber = resultNumber;
	}
}
