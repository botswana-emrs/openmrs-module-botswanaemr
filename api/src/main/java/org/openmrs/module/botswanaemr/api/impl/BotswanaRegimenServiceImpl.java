/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.openmrs.Concept;
import org.openmrs.api.APIException;
import org.openmrs.module.botswanaemr.api.BotswanaRegimenService;
import org.openmrs.module.botswanaemr.api.dao.BotswanaRegimenDao;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;

import java.util.List;

/**
 * Implementations of business logic methods for
 * {@link org.openmrs.module.botswanaemr.api.BotswanaRegimenService}
 */
public class BotswanaRegimenServiceImpl implements BotswanaRegimenService {
	
	public BotswanaRegimenDao getBotswanaRegimenDao() {
		return botswanaRegimenDao;
	}
	
	public void setBotswanaRegimenDao(BotswanaRegimenDao botswanaRegimenDao) {
		this.botswanaRegimenDao = botswanaRegimenDao;
	}
	
	private BotswanaRegimenDao botswanaRegimenDao;
	
	@Override
	public List<RegimenLine> getAllRegimenLine() throws APIException {
		return botswanaRegimenDao.getAllRegimenLine();
	}
	
	@Override
	public RegimenLine getRegimenLine(Integer ehrAppointmentTypeId) throws APIException {
		return botswanaRegimenDao.getRegimenLine(ehrAppointmentTypeId);
	}
	
	@Override
	public RegimenLine getRegimenLineByUuid(String uuid) throws APIException {
		return botswanaRegimenDao.getRegimenLineByUuid(uuid);
	}
	
	@Override
	public RegimenLine saveRegimenLine(RegimenLine regimenLine) throws APIException {
		return botswanaRegimenDao.saveRegimenLine(regimenLine);
	}
	
	@Override
	public RegimenLine retireRegimenLine(RegimenLine regimenLine, String reason) throws APIException {
		return botswanaRegimenDao.retireRegimenLine(regimenLine, reason);
	}
	
	@Override
	public RegimenLine unretireRegimenLine(RegimenLine regimenLine) throws APIException {
		return botswanaRegimenDao.unretireRegimenLine(regimenLine);
	}
	
	@Override
	public void purgeRegimenLine(RegimenLine regimenLine) throws APIException {
		getBotswanaRegimenDao().purgeRegimenLine(regimenLine);
	}
	
	@Override
	public Regimen saveRegimen(Regimen regimen) throws APIException {
		return botswanaRegimenDao.saveRegimen(regimen);
	}
	
	@Override
	public Regimen getRegimen(Integer regimenId) throws APIException {
		return botswanaRegimenDao.getRegimen(regimenId);
	}
	
	@Override
	public Regimen getRegimenByRegimenLine(RegimenLine regimenLine) throws APIException {
		return botswanaRegimenDao.getRegimenByRegimenLine(regimenLine);
	}
	
	@Override
	public List<Regimen> getAllRegimen(boolean includeVoided) throws APIException {
		return botswanaRegimenDao.getAllRegimen(includeVoided);
	}
	
	@Override
	public Regimen updateRegimen(Regimen regimen) throws APIException {
		return botswanaRegimenDao.updateRegimen(regimen);
	}
	
	@Override
	public Regimen voidRegimen(Regimen regimen, String voidReason) throws APIException {
		return getBotswanaRegimenDao().voidRegimen(regimen, voidReason);
	}
	
	@Override
	public RegimenComponent saveRegimenComponent(RegimenComponent regimenComponent) throws APIException {
		RegimenComponent savedRegimenComponent = botswanaRegimenDao.saveRegimenComponent(regimenComponent);
		if (!savedRegimenComponent.getRegimenComponentDrugs().isEmpty()) {
			for (RegimenComponentDrug regimenComponentDrug : savedRegimenComponent.getRegimenComponentDrugs()) {
				regimenComponentDrug.setRegimenComponent(savedRegimenComponent);
				botswanaRegimenDao.saveRegimenComponentDrug(regimenComponentDrug);
			}
		}
		return savedRegimenComponent;
	}
	
	@Override
	public RegimenComponent getRegimenComponentById(Integer regimenComponentId) throws APIException {
		return botswanaRegimenDao.getRegimenComponentById(regimenComponentId);
	}
	
	@Override
	public List<RegimenComponent> getRegimenComponentsByRegimen(Integer regimenId) throws APIException {
		Regimen regimen = botswanaRegimenDao.getRegimen(regimenId);
		return botswanaRegimenDao.getAllRegimenComponentByRegimen(regimen);
	}
	
	@Override
	public List<RegimenComponent> getRegimenComponentsByRegimenConcept(Concept regimenConcept) throws APIException {
		return botswanaRegimenDao.getRegimenComponentsByConcept(regimenConcept);
	}
	
	@Override
	public List<RegimenComponent> getAllRegimenComponent(boolean includeVoided) throws APIException {
		return botswanaRegimenDao.getAllRegimenComponent(includeVoided);
	}
	
	@Override
	public RegimenComponent getRegimenComponentByRegimen(Regimen regimen) throws APIException {
		return botswanaRegimenDao.getRegimenComponentByRegimen(regimen);
	}
	
	@Override
	public List<RegimenComponent> getAllRegimenComponentByRegimen(Regimen regimen) throws APIException {
		return botswanaRegimenDao.getAllRegimenComponentByRegimen(regimen);
	}
	
	@Override
	public RegimenComponent updateRegimenComponent(RegimenComponent regimenComponent) throws APIException {
		return botswanaRegimenDao.updateRegimenComponent(regimenComponent);
	}
	
	@Override
	public RegimenComponent voidRegimenComponent(RegimenComponent regimenComponent, String voidReason) throws APIException {
		return botswanaRegimenDao.voidRegimenComponent(regimenComponent, voidReason);
	}
	
	@Override
	public RegimenComponentDrug saveRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) {
		return botswanaRegimenDao.saveRegimenComponentDrug(regimenComponentDrug);
	}
	
	@Override
	public RegimenComponentDrug getRegimenComponentDrug(Integer id) {
		return botswanaRegimenDao.getRegimenComponentDrugById(id);
	}
	
	@Override
	public List<RegimenComponentDrug> getAllRegimenComponentDrugs(boolean includeVoided) {
		return botswanaRegimenDao.getAllRegimenComponentDrugs(includeVoided);
	}
	
	// Get regimenComponentDrug by regimenComponent
	@Override
	public List<RegimenComponentDrug> getRegimenComponentDrugsByRegimenComponent(RegimenComponent regimenComponent) {
		return botswanaRegimenDao.getRegimenComponentDrugByRegimenComponent(regimenComponent);
	}
	
	// Get regimenComponentDrug by regimenConcept
	@Override
	public List<RegimenComponentDrug> getRegimenComponentDrugsByRegimenConcept(Concept regimenConcept) {
		return botswanaRegimenDao.getRegimenComponentDrugByRegimenConcept(regimenConcept);
	}
	
	@Override
	public RegimenComponentDrug updateRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) {
		return botswanaRegimenDao.updateRegimenComponentDrug(regimenComponentDrug);
	}
	
	@Override
	public void voidRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug, String voidReason) {
		botswanaRegimenDao.voidRegimenComponentDrug(regimenComponentDrug, voidReason);
	}
	
	@Override
	public Regimen getRegimenByConcept(Concept regimenConcept) {
		return botswanaRegimenDao.getRegimenByConcept(regimenConcept);
		
	}
}
