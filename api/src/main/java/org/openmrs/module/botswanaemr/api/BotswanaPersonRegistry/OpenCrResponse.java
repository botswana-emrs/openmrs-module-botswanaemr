/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry;

import lombok.Data;
import org.hl7.fhir.r4.model.ResourceType;

import java.util.List;

@Data
public class OpenCrResponse {
	
	private String status;
	
	private ResourceType resourceType;
	
	private String id;
	
	private Meta meta;
	
	private String type;
	
	private int total;
	
	private List<Entry> entry;
	
	@Data
	public static class Meta {
		
		private String lastUpdated;
	}
	
	@Data
	public static class Entry {
		
		private String entryId;
		
		private String entryValue;
	}
}
