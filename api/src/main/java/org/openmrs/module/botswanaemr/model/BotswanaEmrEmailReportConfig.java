/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.Location;

import javax.persistence.*;
import java.util.Date;

/**
 * A configuration that defines the email template for email report notification
 */

@Entity
@Table(name = "botswana_emr_email_report_config")
public class BotswanaEmrEmailReportConfig {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "email_config_id")
	private Integer emailConfigId;
	
	@Column(name = "mail_from")
	String from;
	
	@Access(AccessType.FIELD)
	@Column(name = "mail_to")
	String mailTo;
	
	@Column(name = "subject")
	String subject;
	
	@Column(name = "mail_content")
	String mailContent;
	
	@Column(name = "add_output_to_body")
	Boolean addOutputToBody;
	
	@Column(name = "add_output_as_attachment")
	Boolean addOutputAsAttachment;
	
	@Column(name = "attachment_name")
	
	String attachmentName;
	
	@Column(name = "mailReportId")
	String mailReportId;
	
	@Column(name = "report_name")
	String reportName;
	
	@ManyToOne
	@JoinColumn(name = "facilityId")
	Location facilityId;
	
	@Column(name = "ccc_list")
	String cccList;
	
	@Column(name = "bccc_list")
	String bccList;
	
	@Column(name = "date_last_email_sent")
	Date lastEmailSent;
	
	public Integer getEmailConfigId() {
		return emailConfigId;
	}
	
	public void setEmailConfigId(Integer emailConfigId) {
		this.emailConfigId = emailConfigId;
	}
	
	public String getFrom() {
		return from;
	}
	
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getMailTo() {
		return mailTo;
	}
	
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getMailContent() {
		return mailContent;
	}
	
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	
	public Boolean getAddOutputToBody() {
		return addOutputToBody;
	}
	
	public void setAddOutputToBody(Boolean addOutputToBody) {
		this.addOutputToBody = addOutputToBody;
	}
	
	public Boolean getAddOutputAsAttachment() {
		return addOutputAsAttachment;
	}
	
	public void setAddOutputAsAttachment(Boolean addOutputAsAttachment) {
		this.addOutputAsAttachment = addOutputAsAttachment;
	}
	
	public String getAttachmentName() {
		return attachmentName;
	}
	
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	
	public String getMailReportId() {
		return mailReportId;
	}
	
	public void setMailReportId(String reportId) {
		this.mailReportId = reportId;
	}
	
	public Location getFacilityId() {
		return facilityId;
	}
	
	public void setFacilityId(Location facilityId) {
		this.facilityId = facilityId;
	}
	
	public Date getLastEmailSent() {
		return lastEmailSent;
	}
	
	public void setLastEmailSent(Date lastEmailSent) {
		this.lastEmailSent = lastEmailSent;
	}
	
	public String getReportName() {
		return reportName;
	}
	
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	public String getCccList() {
		return cccList;
	}
	
	public void setCccList(String allowedUserGroups) {
		this.cccList = allowedUserGroups;
	}
	
	public String getBccList() {
		return bccList;
	}
	
	public void setBccList(String bccList) {
		this.bccList = bccList;
	}
}
