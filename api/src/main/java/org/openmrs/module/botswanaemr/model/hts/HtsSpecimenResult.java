/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import lombok.Data;
import lombok.ToString;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "hts_specimen_result")
public class HtsSpecimenResult extends BaseOpenmrsObject {
	
	@Column(name = "interpretation")
	private String interpretation;
	
	@Column(name = "comment")
	private String comment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pt_test_id")
	private HtsProficiencyTestingTest htsProficiencyTestingTest;
	
	@OneToMany(mappedBy = "htsSpecimenResult", cascade = { CascadeType.ALL })
	@ToString.Exclude
	private Set<HtsTestResult> results = new HashSet<>();
	
	@ManyToOne
	@JoinColumn(name = "stock_room_id")
	private Stockroom stockroom;
	
	@Id
	@GeneratedValue
	@Column(name = "specimen_result_id")
	private Integer id;
	
	@Override
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getInterpretation() {
		return interpretation;
	}
	
	public void setInterpretation(String interpretation) {
		this.interpretation = interpretation;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public HtsProficiencyTestingTest getHtsProficiencyTestingTest() {
		return htsProficiencyTestingTest;
	}
	
	public void setHtsProficiencyTestingTest(HtsProficiencyTestingTest htsProficiencyTestingTest) {
		this.htsProficiencyTestingTest = htsProficiencyTestingTest;
	}
	
	public Set<HtsTestResult> getResults() {
		return results;
	}
	
	public void setResults(Set<HtsTestResult> results) {
		this.results = results;
	}
	
	public Stockroom getStockroom() {
		return stockroom;
	}
	
	public void setStockroom(Stockroom stockroom) {
		this.stockroom = stockroom;
	}
}
