/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import java.util.List;

import lombok.Data;
import org.openmrs.ui.framework.SimpleObject;

@Data
public class StockCardSimplifier {
	
	Double averageMonthlyConsumption;
	
	Double minimumStockLevel;
	
	Double maximumStockLevel;
	
	Double emergencyOrderPoint;
	
	List<TransactionSimplifier> transactionSimplifierList;
	
	public static SimpleObject simplify(StockCardSimplifier stockCardSimplifier) {
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("averageMonthlyConsumption", stockCardSimplifier.getAverageMonthlyConsumption());
		simpleObject.put("minimumStockLevel", stockCardSimplifier.getMinimumStockLevel());
		simpleObject.put("maximumStockLevel", stockCardSimplifier.getMaximumStockLevel());
		simpleObject.put("emergencyOrderPoint", stockCardSimplifier.getEmergencyOrderPoint());
		simpleObject.put("transactionSimplifierList", stockCardSimplifier.getTransactionSimplifierList());
		
		return simpleObject;
	}
}
