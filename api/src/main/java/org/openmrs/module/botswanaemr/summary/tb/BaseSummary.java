/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import org.openmrs.Concept;
import org.openmrs.Obs;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.utilities.DateUtils;

import javax.validation.constraints.NotNull;

public class BaseSummary {
	
	protected Concept getConcept(@NotNull String conceptUuid) {
		return Context.getConceptService().getConceptByUuid(conceptUuid);
	}
	
	protected String getValueCoded(@NotNull Obs obs) {
		return obs.getValueCoded() != null ? obs.getValueCoded().getDisplayString() : " ";
	}
	
	protected String getValueDate(@NotNull Obs obs) {
		return obs.getValueDate() != null ? DateUtils.getStringDate(obs.getValueDate(), "dd-MM-yyyy") : " ";
	}
	
	protected String getValueText(@NotNull Obs obs) {
		return obs.getValueText() != null ? obs.getValueText() : " ";
	}
	
	protected Double getValueNumeric(@NotNull Obs obs) {
		return obs.getValueNumeric() != null ? obs.getValueNumeric() : 0.0;
	}
}
