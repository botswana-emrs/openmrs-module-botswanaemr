/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.openmrs.BaseOpenmrsData;
import org.openmrs.DrugOrder;
import org.openmrs.Order;
import org.openmrs.api.EncounterService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;

public class PharmacyUtils {
	
	public static boolean hasRefillsDone(Integer orderId) {
		boolean refillsDone = false;
		if (orderId != null) {
			DrugOrder drugOrder = (DrugOrder) Context.getOrderService().getOrder(orderId);
			Set<Order> orderListRelatedToThisOrder = Context.getEncounterService()
			        .getEncounter(drugOrder.getEncounter().getEncounterId()).getOrders();
			for (Order order : orderListRelatedToThisOrder) {
				// check if the order is a drug order
				if (order instanceof DrugOrder) {
					DrugOrder ordersFromEncounter = (DrugOrder) order;
					// check if numRefills is null
					if (drugOrder.getNumRefills() != null && ordersFromEncounter.getNumRefills() != null
					        && !drugOrder.getNumRefills().equals(ordersFromEncounter.getNumRefills())
					        && drugOrder.getConcept().equals(ordersFromEncounter.getConcept())) {
						refillsDone = true;
					}
				}
			}
			
		}
		return refillsDone;
	}
	
	/**
	 * Calculates the number of refills done for a given drug order. This method retrieves the drug
	 * order associated with the given orderId and checks all related orders from the same encounter. It
	 * counts the number of orders that have a different number of refills but the same concept as the
	 * original drug order, and where the number of refills is greater than 0 and the action is not
	 * DISCONTINUE. Note: If the orderId is null, the method returns 0.
	 *
	 * @param orderId the ID of the drug order to check for refills
	 * @return the number of refills done for the specified drug order
	 */
	public static Integer refillsDone(Integer orderId) {
		List<Order> orderList = new ArrayList<>();
		if (orderId != null) {
			DrugOrder drugOrder = (DrugOrder) Context.getOrderService().getOrder(orderId);
			Set<Order> orderListRelatedToThisOrder = Context.getEncounterService()
			        .getEncounter(drugOrder.getEncounter().getEncounterId()).getOrders();
			for (Order order : orderListRelatedToThisOrder) {
				DrugOrder ordersFromEncounter = Context.getService(BotswanaEmrService.class)
				        .getDrugOrderById(order.getOrderId());
				if (ordersFromEncounter.getNumRefills() != null && ordersFromEncounter.getNumRefills() > 0
				        && ordersFromEncounter.getAction() != Order.Action.DISCONTINUE) {
					if (!drugOrder.getNumRefills().equals(ordersFromEncounter.getNumRefills())
					        && drugOrder.getConcept().equals(ordersFromEncounter.getConcept())) {
						orderList.add(order);
					}
				}
			}
		}
		return orderList.size();
	}
	
	public static Integer getTotalRefills(Integer orderId) {
		List<Order> orderList = new ArrayList<>();
		int refills = 0;
		if (orderId != null) {
			DrugOrder drugOrder = (DrugOrder) Context.getOrderService().getOrder(orderId);
			Set<Order> orderListRelatedToThisOrder = Context.getEncounterService()
			        .getEncounter(drugOrder.getEncounter().getEncounterId()).getOrders();
			for (Order order : orderListRelatedToThisOrder) {
				DrugOrder orderToDrugOrder = Context.getService(BotswanaEmrService.class)
				        .getDrugOrderById(order.getOrderId());
				if (drugOrder.getConcept().equals(orderToDrugOrder.getConcept())) {
					orderList.add(order);
				}
			}
			
		}
		List<Order> orderedList = orderList.stream().sorted(Comparator.comparing(BaseOpenmrsData::getDateCreated))
		        .collect(Collectors.toList());
		DrugOrder firstOrderEver = Context.getService(BotswanaEmrService.class)
		        .getDrugOrderById(orderedList.get(0).getOrderId());
		if (firstOrderEver != null && firstOrderEver.getNumRefills() != null) {
			refills = firstOrderEver.getNumRefills();
		}
		return refills;
	}
	
	public static Integer getTheCorrectRefillCount(int currentIndex, int totalCountIndex) {
		return totalCountIndex - currentIndex;
	}
	
	public static Comparator<Order> getOrdersComparator() {
		return Comparator.comparing(BaseOpenmrsData::getDateCreated);
	}
	
	public static List<DrugOrder> getGroupedDrugOrders(List<Order> orderListOriginal, EncounterService encounterService,
	        Integer encounterId) {
		List<Order> orderListCopy = new ArrayList<>(encounterService.getEncounter(encounterId).getOrders());
		Map<String, List<DrugOrder>> groupedDrugOrders = new HashMap<>();
		List<DrugOrder> orderListResultant;
		for (Order orderOriginal : orderListOriginal) {
			if (orderOriginal instanceof DrugOrder) {
				DrugOrder original = (DrugOrder) orderOriginal;
				orderListResultant = new ArrayList<>();
				for (Order orderCopy : orderListCopy) {
					DrugOrder copy = (DrugOrder) orderCopy;
					if (original.getConcept().equals(copy.getConcept())) {
						orderListResultant.add(copy);
					}
				}
				groupedDrugOrders.put(original.getConcept().getDisplayString(), orderListResultant);
			}
		}
		
		List<DrugOrder> uniqueOrders = new ArrayList<>();
		List<Order> getOrdersFromSet = new ArrayList<>();
		for (Map.Entry<String, List<DrugOrder>> entry : groupedDrugOrders.entrySet()) {
			getOrdersFromSet.addAll(entry.getValue().stream().sorted(getOrdersComparator()).collect(Collectors.toList()));
			
			uniqueOrders.add((DrugOrder) getOrdersFromSet.get(getOrdersFromSet.size() - 1));
		}
		
		return uniqueOrders;
	}
}
