/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import java.util.List;
import java.util.Set;

import org.openmrs.Location;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.api.dao.HtsLabQualityControlDao;

import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;
import org.openmrs.module.botswanaemr.model.hts.LabQualityTestReason;
import org.springframework.stereotype.Component;

@Component
public class HtsLabQualityControlServiceImpl extends BaseOpenmrsService implements HtsLabControlService {
	
	public HtsLabQualityControlDao getLabQualityControlDao() {
		return labQualityControlDao;
	}
	
	public void setLabQualityControlDao(HtsLabQualityControlDao labQualityControlDao) {
		this.labQualityControlDao = labQualityControlDao;
	}
	
	private HtsLabQualityControlDao labQualityControlDao;
	
	@Override
	public List<LabQualityControl> getList() {
		return labQualityControlDao.getAll(false);
	}

	@Override
	public List<LabQualityControl> getList(Location location) {
		return labQualityControlDao.getAll(location, false);
	}
	
	@Override
	public LabQualityControl getLabQualityControl(int id) {
		return (LabQualityControl) labQualityControlDao.getById(id);
	}
	
	@Override
	public LabQualityControlTest getLabQualityControlTest(int id) {
		return labQualityControlDao.getByLabQualityControlTestId(id);
	}
	
	@Override
	public void saveLabQualityControl(LabQualityControl labQualityControl) {
		labQualityControlDao.saveOrUpdate(labQualityControl);
		if (!labQualityControl.getHtsTest().isEmpty()) {
			for (LabQualityControlTest labQualityControltest : labQualityControl.getHtsTest()) {
				labQualityControltest.setLabQualityControl(labQualityControl);
				if (labQualityControltest.getParent() > 0) {
					labQualityControltest.setParent(labQualityControl.getHtsTest().stream().findFirst().get().getId());
				}
				labQualityControlDao.saveOrUpdate(labQualityControltest);
			}
		}
		
		for (LabQualityTestReason qualityTestReason : labQualityControl.getReasons()) {
			qualityTestReason.setLabQualityControl(labQualityControl);
			labQualityControlDao.saveOrUpdate(qualityTestReason);
		}
	}
	
	@Override
	public void updateLabQualityControl(LabQualityControl labQualityControl) {
		labQualityControlDao.saveOrUpdate(labQualityControl);
	}
	
	@Override
	public void voidLabQualityControl(LabQualityControl labQualityControl) {
		labQualityControlDao.delete(labQualityControl);
		
	}
	
	@Override
	public List<LabQualityControlTest> getLabQualityControlTests(LabQualityControl labQualityControl) {
		return labQualityControlDao.getAll(false);
	}
	
	@Override
	public LabQualityControlTest saveOrUpdateLabQualityControlTest(LabQualityControlTest labQualityControlTest) {
		return (LabQualityControlTest) labQualityControlDao.saveOrUpdate(labQualityControlTest);
	}
	
	@Override
	public void voidLabQualityControlTest(LabQualityControlTest labQualityControlTest) {
		labQualityControlDao.delete(labQualityControlTest);
	}
	
	@Override
	public void addLabQualityControlTests(Set<LabQualityControlTest> labQualityControlTestSet) {
		
	}
	
	@Override
	public List<LabQualityControl> getLabQualityControlsThisWeekByTestingPoints(List<String> testingPoints) {
		return labQualityControlDao.getLabQualityControlsThisWeekByTestingPoints(testingPoints);
	}
	
}
