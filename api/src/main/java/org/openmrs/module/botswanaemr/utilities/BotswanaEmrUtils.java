/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import static org.openmrs.ConditionVerificationStatus.PROVISIONAL;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.ConceptAnswer;
import org.openmrs.ConceptMapType;
import org.openmrs.ConceptSource;
import org.openmrs.Condition;
import org.openmrs.ConditionVerificationStatus;
import org.openmrs.Diagnosis;
import org.openmrs.Drug;
import org.openmrs.DrugOrder;
import org.openmrs.DrugReferenceMap;
import org.openmrs.Encounter;
import org.openmrs.EncounterRole;
import org.openmrs.EncounterType;
import org.openmrs.Form;
import org.openmrs.Location;
import org.openmrs.LocationAttribute;
import org.openmrs.LocationAttributeType;
import org.openmrs.PersonAttributeType;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Order.FulfillerStatus;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.PersonAddress;
import org.openmrs.PersonName;
import org.openmrs.Privilege;
import org.openmrs.Program;
import org.openmrs.Provider;
import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.Visit;
import org.openmrs.VisitType;
import org.openmrs.VisitAttribute;
import org.openmrs.VisitAttributeType;
import org.openmrs.CareSetting;
import org.openmrs.api.APIException;
import org.openmrs.api.DiagnosisService;
import org.openmrs.api.EncounterService;
import org.openmrs.api.FormService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.OrderService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.ProviderService;
import org.openmrs.api.UserService;
import org.openmrs.api.VisitService;
import org.openmrs.api.OrderContext;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.model.DrugObsData;
import org.openmrs.module.botswanaemr.model.DrugOrderSimplifier;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.AppointmentBlock;
import org.openmrs.module.appointmentscheduling.AppointmentType;
import org.openmrs.module.appointmentscheduling.TimeSlot;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.AgeCategory;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.CaseLinkage;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.MedicalHistorySimplifier;
import org.openmrs.module.botswanaemr.model.SimplifiedLabTest;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.SimplifiedProficiencyTesting;
import org.openmrs.module.botswanaemr.model.SimplifiedDrug;
import org.openmrs.module.botswanaemr.utils.Programs;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.emrapi.visit.VisitDomainWrapper;
import org.openmrs.module.htmlformentry.HtmlForm;
import org.openmrs.module.htmlformentry.HtmlFormEntryService;
import org.openmrs.module.idgen.IdentifierSource;
import org.openmrs.module.idgen.service.IdentifierSourceService;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.module.reporting.common.DateUtil;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.parameter.EncounterSearchCriteriaBuilder;
import org.openmrs.parameter.OrderSearchCriteria;
import org.openmrs.parameter.OrderSearchCriteriaBuilder;
import org.openmrs.ui.framework.Model;
import org.openmrs.ui.framework.SimpleObject;
import org.openmrs.ui.framework.annotation.FragmentParam;
import org.openmrs.ui.framework.annotation.SpringBean;
import org.openmrs.ui.framework.fragment.FragmentModel;
import org.openmrs.ui.framework.page.PageModel;
import org.openmrs.util.OpenmrsClassLoader;
import org.openmrs.util.OpenmrsUtil;

@Slf4j
public class BotswanaEmrUtils {
	
	public static final String HIV_ADULT_FOLLOW_UP_ENCOUNTER_TYPE = "f402f44a-d010-11ec-9d64-0242ac120002";
	
	public static final String HIV_PAEDS_FOLLOW_UP_ENCOUNTER_TYPE = "4c329b3a-c4a3-11ec-9d64-0242ac120002";
	
	public static final String ART_ADHERENCE_ENCOUNTER_TYPE = "5e421412-d084-11ec-9d64-0242ac120002";
	
	public static final String ADHERENCE_FORM_UUID = "4310ea10-d084-11ec-9d64-0242ac120002";
	
	private static final OrderService orderService = Context.getService(OrderService.class);
	
	private static final EncounterService encounterService = Context.getService(EncounterService.class);
	
	private static final LabService labService = Context.getService(LabService.class);
	
	private static final AppointmentService appointmentService = Context.getService(AppointmentService.class);
	
	private static final DiagnosisService diagnosisService = Context.getService(DiagnosisService.class);
	
	private static final ObsService obsService = Context.getService(ObsService.class);
	
	public static final Integer LIMIT_FETCH_SIZE = Integer
	        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
	
	public static final Integer MIN_AGE_FOR_REG_FEE_PAYMENT = Integer
	        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.minAgeForRegistrationFee"));
	
	public static final Integer MAX_AGE_FOR_REG_FEE_PAYMENT = Integer
	        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.maxAgeForRegistrationFee"));
	
	public static final Integer DEFAULT_BILLABLE_AMOUNT_FOR_LOCALS = Integer
	        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.defaultBillableAmountForLocals"));
	
	public static final Integer DEFAULT_BILLABLE_AMOUNT_FOR_FOREIGNERS = Integer
	        .valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.defaultBillableAmountForForeigners"));
	
	private static List<Obs> symptomsObsGrouping;
	
	public static void setPatientIdentifierAttributes(Model model, Patient patient) {
		/* EnumMaps are ordered collections, and they maintain the
		 * natural order of their keys ( the natural order of keys
		 * means the order in which enum constants are declared in
		 * the enum types)
		 *
		 * The firstNonNull method leverages that behaviour and returns the very
		 * first non-null value in the natural order of NIN, PPN, BCN and OID,
		 * otherwise null.
		 */
		EnumMap<IDENTIFIER, PatientIdentifier> patientIdentifiers = new EnumMap<>(IDENTIFIER.class);
		patientIdentifiers.put(IDENTIFIER.NIN, patient.getPatientIdentifier(OMANG_IDENTIFIER_NAME));
		patientIdentifiers.put(IDENTIFIER.PPN, patient.getPatientIdentifier(PASSPORT_IDENTIFIER_NAME));
		patientIdentifiers.put(IDENTIFIER.BCN, patient.getPatientIdentifier(BIRTH_CERT_IDENTIFIER_NAME));
		patientIdentifiers.put(IDENTIFIER.TID, patient.getPatientIdentifier(TEMPORARY_ID_IDENTIFIER_NAME));
		// patientIdentifiers.put(IDENTIFIER.OID, patient.getPatientIdentifier(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME));
		
		for (Map.Entry<IDENTIFIER, PatientIdentifier> patientIdentifier : patientIdentifiers.entrySet()) {
			if (ObjectUtils.firstNonNull(patientIdentifier.getValue() != null)) {
				model.addAttribute("identifierType", patientIdentifier.getKey().getIdentificationType());
				model.addAttribute("identifierValue", patientIdentifier.getValue());
				break;
			}
			model.addAttribute("identifierType", "");
			model.addAttribute("identifierValue", "");
		}
	}
	
	public static String generatePatientTemporaryId(Location location) {
		String identifier = "";
		IdentifierSource source = Context.getService(IdentifierSourceService.class)
		        .getIdentifierSourceByUuid(TEMPORARY_ID_IDGEN_SOURCE_UUID);
		if (source != null) {
			if (location != null) {
				for (LocationAttribute localAttribute : location.getActiveAttributes()) {
					if (localAttribute.getAttributeType().getUuid().equals(MASTER_FACILITY_CODE_UUID)) {
						identifier = Context.getService(IdentifierSourceService.class).generateIdentifier(source, "temp id");
						int position = localAttribute.getValueReference().length();
						identifier = identifier.substring(0, position) + "-" + identifier.substring(position);
						break;
					}
				}
			}
		}
		return identifier;
	}
	
	public static Visit getActiveVisit(@NotNull Patient patient) {
		List<Visit> visitList = Context.getVisitService().getVisitsByPatient(patient, false, false);
		if (visitList.size() > 0) {
			return visitList.get(0);
		}
		return null;
	}
	
	public static Location getLocation(String location) {
		Location location1 = null;
		if (StringUtils.isNotEmpty(location)) {
			location1 = Context.getLocationService().getLocationByUuid(location);
			location1 = location1 == null ? Context.getLocationService().getLocation(location) : location1;
			location1 = location1 == null ? Context.getLocationService().getLocation(Integer.valueOf(location)) : location1;
		}
		
		return location1;
	}
	
	public static String formatDateWithTime(Date date, String format) {
		
		if (StringUtils.isEmpty(format)) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		return formatter.format(date);
	}
	
	public static String formatDateWithoutTime(Date date, String format) {
		
		Format formatter = new SimpleDateFormat(format);
		
		return formatter.format(date);
	}
	
	public static Date formatDateFromString(String date, String format) throws ParseException {
		return new SimpleDateFormat(format).parse(date);
	}
	
	public static Date formatDateFromStringWithTime(String date) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm a").parse(date);
	}
	
	public static Date formatFullDateFromStringWithTime(String date) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(date);
	}
	
	/**
	 * Calculates the days since the given date
	 *
	 * @param date1 the date
	 * @param date2 the date2
	 * @return the number of days
	 */
	public static int unitsSince(Date date1, Date date2, String type) {
		int value = 0;
		DateTime d1 = new DateTime(date1.getTime());
		DateTime d2 = new DateTime(date2.getTime());
		if (type.equals("days")) {
			value = Math.abs(Days.daysBetween(d1, d2).getDays());
		} else if (type.equals("hours")) {
			value = Math.abs(Hours.hoursBetween(d1, d2).getHours());
		} else if (type.equals("minutes")) {
			value = Math.abs(Minutes.minutesBetween(d1, d2).getMinutes());
		} else if (type.equals("seconds")) {
			value = Math.abs(Seconds.secondsBetween(d1, d2).getSeconds());
		} else if (type.equals("years")) {
			value = Math.abs(Years.yearsBetween(d1, d2).getYears());
		} else if (type.equals("weeks")) {
			value = Math.abs(Weeks.weeksBetween(d1, d2).getWeeks());
		} else if (type.equals("months")) {
			value = Math.abs(Months.monthsBetween(d1, d2).getMonths());
		}
		return value;
	}
	
	/**
	 * Formats a person name
	 *
	 * @param name the name
	 * @return the string value
	 * @should format voided person as empty string
	 */
	public static String formatPersonName(PersonName name) {
		if (name != null) {
			List<String> items = new ArrayList<String>();
			
			if (name.getFamilyName() != null) {
				items.add(name.getFamilyName() + " ");
			}
			if (name.getGivenName() != null) {
				items.add(name.getGivenName());
			}
			if (name.getMiddleName() != null) {
				items.add(name.getMiddleName());
			}
			return OpenmrsUtil.join(items, " ");
		}
		return "";
	}
	
	/**
	 * Formats a person name
	 *
	 * @param givenName the given name
	 * @param middleName the middle name
	 * @param familyName the family name
	 * @return the string value
	 * @should format voided person as empty string
	 */
	public static String formatPersonName(String givenName, String middleName, String familyName) {
		List<String> items = new ArrayList<String>();
		
		if (StringUtils.isNotEmpty(familyName)) {
			items.add(familyName + " ");
		}
		if (StringUtils.isNotEmpty(givenName)) {
			items.add(givenName + " ");
		}
		if (StringUtils.isNotEmpty(middleName)) {
			items.add(middleName + " ");
		}
		return OpenmrsUtil.join(items, " ");
	}
	
	/**
	 * Formats a person creator
	 *
	 * @param person the name
	 * @return the string value
	 * @should format voided person as empty string
	 */
	public static String formatPersonCreator(Person person) {
		if (person != null) {
			List<String> items = new ArrayList<String>();
			
			if (person.getCreator() != null) {
				if (person.getCreator().equals(Context.getAuthenticatedUser())) {
					items.add("You");
				} else {
					if (person.getCreator().getFamilyName() != null) {
						items.add(person.getCreator().getFamilyName() + " ");
					}
					if (person.getCreator().getGivenName() != null) {
						items.add(person.getCreator().getGivenName());
					}
				}
			}
			return OpenmrsUtil.join(items, " ");
		}
		return "";
	}
	
	/**
	 * Formats a person
	 *
	 * @param person the name
	 * @return the string value
	 * @should format voided person as empty string
	 */
	public static String formatPerson(Person person) {
		if (person != null && Context.getAuthenticatedUser() != null) {
			List<String> items = new ArrayList<String>();
			
			if (person.equals(Context.getAuthenticatedUser().getPerson())) {
				items.add("You");
			} else {
				if (person.getFamilyName() != null) {
					items.add(person.getFamilyName() + " ");
				}
				if (person.getGivenName() != null) {
					items.add(person.getGivenName());
				}
			}
			return OpenmrsUtil.join(items, " ");
		}
		return "";
	}
	
	/**
	 * Formats a person
	 *
	 * @param person the name
	 * @return the string value
	 * @should format voided person as empty string
	 */
	public static String formatPersonFull(Person person) {
		if (person != null && Context.getAuthenticatedUser() != null) {
			List<String> items = new ArrayList<String>();
			
			if (person.getGivenName() != null) {
				items.add(person.getGivenName());
			}
			if (person.getFamilyName() != null) {
				items.add(person.getFamilyName());
			}
			return OpenmrsUtil.join(items, " ");
		}
		return "";
	}
	
	/**
	 * Returns the text to be shown in activity trail based on the encounter type
	 *
	 * @param encounterTypeUuid the encounter type uuid
	 * @return the string value
	 */
	public static String formatEncounterTypeText(String encounterTypeUuid) {
		HashMap<String, String> encounterTypeMap = new HashMap<String, String>();
		
		encounterTypeMap.put(TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID, "completed the screening of");
		encounterTypeMap.put(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID, "accepted to treat");
		encounterTypeMap.put(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID, "accepted to treat");
		encounterTypeMap.put(TB_SCREENING_ENCOUNTER_TYPE_UUID, "completed the screening of");
		encounterTypeMap.put(TB_ENROLMENT_ENCOUNTER_TYPE_UUID, "completed the enrollment of");
		encounterTypeMap.put(LAB_REQUEST_FORM_UUID, "requested lab tests for");
		encounterTypeMap.put(LAB_RESULT_ENCOUNTER_TYPE_UUID, "posted lab results for");
		encounterTypeMap.put(XRAY_RESULT_ENCOUNTER_TYPE, "posted xray results for");
		encounterTypeMap.put(DRUG_TRACKING_ENCOUNTER_TYPE, "completed dot form for");
		
		return encounterTypeMap.getOrDefault(encounterTypeUuid, "");
	}
	
	public static String formatDurationSinceRegistration(Person person) {
		
		return getDurationSince(person.getDateCreated());
	}
	
	public static String formatDurationSinceRegistration(Registration registration) {
		
		return getDurationSince(registration.getDateCreated());
	}
	
	public static String getDurationSince(Date dateCreated) {
		String duration = "";
		
		if (unitsSince(new Date(), dateCreated, "seconds") > 0 && unitsSince(new Date(), dateCreated, "seconds") < 60) {
			duration = unitsSince(new Date(), dateCreated, "seconds") + " seconds ago";
		} else if (unitsSince(new Date(), dateCreated, "minutes") > 0
		        && unitsSince(new Date(), dateCreated, "minutes") < 60) {
			duration = unitsSince(new Date(), dateCreated, "minutes") + " minutes ago";
		} else if (unitsSince(new Date(), dateCreated, "hours") > 0 && unitsSince(new Date(), dateCreated, "hours") < 24) {
			duration = unitsSince(new Date(), dateCreated, "hours") + " hours ago";
		} else if (unitsSince(new Date(), dateCreated, "days") > 0 && unitsSince(new Date(), dateCreated, "days") < 7) {
			duration = unitsSince(new Date(), dateCreated, "days") + " days ago";
		} else if (unitsSince(new Date(), dateCreated, "weeks") > 0 && unitsSince(new Date(), dateCreated, "weeks") < 4) {
			duration = unitsSince(new Date(), dateCreated, "weeks") + " weeks ago";
		} else if (unitsSince(new Date(), dateCreated, "months") > 0 && unitsSince(new Date(), dateCreated, "months") < 12) {
			duration = unitsSince(new Date(), dateCreated, "months") + " months ago";
		} else if (unitsSince(new Date(), dateCreated, "years") > 0) {
			duration = unitsSince(new Date(), dateCreated, "years") + " years ago";
		}
		
		return duration;
	}
	
	public static String formatGender(Person person) {
		
		String gender = "";
		if (person != null) {
			if (person.getGender().equals("M")) {
				gender = "Male";
			} else if (person.getGender().equals("F")) {
				gender = "Female";
			}
		}
		return gender;
	}
	
	public static String formatPatientIdentifier(Patient patient) {
		String identifier = "";
		if (patient != null && patient.getIdentifiers() != null) {
			for (PatientIdentifier patientIdentifier : patient.getIdentifiers()) {
				identifier = patientIdentifier.getIdentifier();
			}
		}
		return identifier;
	}
	
	public static String readAttributeValue(Map<String, String> obj, String fieldName) {
		return obj.get(fieldName);
		
	}
	
	public static String extractFamilyName(String fullname) {
		String[] splitName = fullname.split(" ");
		if (splitName.length > 0) {
			return splitName[splitName.length - 1];
		}
		return "";
	}
	
	public static String extractGivenName(String fullname) {
		String[] splitName = fullname.split(" ");
		if (splitName.length > 1) {
			return splitName[0];
		}
		return "";
	}
	
	public static String extractMiddleName(String fullname) {
		String[] splitName = fullname.split(" ");
		if (splitName.length > 2) {
			return splitName[1];
		}
		return "";
	}
	
	/**
	 * @return the Concept that matches the passed uuid, name, source:code mapping, or primary key id
	 */
	public static Concept getConcept(String lookup) {
		Concept concept = Context.getConceptService().getConceptByUuid(lookup);
		if (concept == null) {
			concept = Context.getConceptService().getConceptByName(lookup);
		}
		if (concept == null) {
			try {
				String[] split = lookup.split("\\:");
				if (split.length == 2) {
					concept = Context.getConceptService().getConceptByMapping(split[1], split[0]);
				}
			}
			catch (Exception ignored) {}
		}
		if (concept == null) {
			try {
				concept = Context.getConceptService().getConcept(Integer.parseInt(lookup));
			}
			catch (Exception e) {
				//Don't break app flow because of not found concept
				// throw new RuntimeException(e.toString());
				log.error("Unable to find concept with ID {} ", lookup, e);
			}
		}
		return concept;
	}
	
	public static Provider getProvider(Person person) {
		Provider provider = null;
		ProviderService providerService = Context.getProviderService();
		List<Provider> providerList = new ArrayList<Provider>(providerService.getProvidersByPerson(person));
		if (providerList.size() > 0) {
			provider = providerList.get(0);
		}
		return provider;
	}
	
	public static String getOpenMrsId(Patient patient) {
		String openMRSId = "";
		PatientIdentifier patientIdentifier = patient.getPatientIdentifier(OPENMRS_ID_IDENTIFIER_NAME);
		if (patientIdentifier != null) {
			openMRSId = patientIdentifier.getIdentifier();
		}
		return openMRSId;
	}
	
	public static EncounterRole getDefaultEncounterRole() {
		return Context.getEncounterService().getEncounterRoleByUuid(DEFAULT_ENCOUNTER_ROLE_UUID);
	}
	
	/**
	 * Create a new encounter
	 *
	 * @param patient
	 * @param encounterType
	 * @param location
	 * @return
	 */
	public static Encounter createEncounter(Patient patient, EncounterType encounterType, Location location, Visit visit) {
		return createEncounter(patient, encounterType, location, null, visit);
	}
	
	/**
	 * Create a new encounter
	 *
	 * @param patient
	 * @param encounterType
	 * @param location
	 * @param form
	 * @return
	 */
	public static Encounter createEncounter(Patient patient, EncounterType encounterType, Location location, Form form,
	        Visit visit) {
		// create encounter
		Encounter encounter = null;
		if (visit == null) {
			visit = getPatientActiveVisit(patient, location, true);
		} else {
			if (visit.getPatient() == null) {
				visit = Context.getVisitService().getVisit(visit.getVisitId());
			}
			if (visit.getPatient() != patient) {
				visit = getPatientActiveVisit(patient, location, true);
			}
		}
		
		if (visit.getStopDatetime() == null || isClosedVisitLegibleForEdit(patient)) {
			encounter = new Encounter();
			encounter.setEncounterType(encounterType);
			encounter.setCreator(Context.getAuthenticatedUser());
			encounter.setProvider(getDefaultEncounterRole(), getProvider(Context.getAuthenticatedUser().getPerson()));
			encounter.setPatient(patient);
			encounter.setLocation(location);
			
			if (visit.getStopDatetime() != null) {
				DateTime dateTime = new DateTime(visit.getStopDatetime());
				encounter.setEncounterDatetime(dateTime.minusMinutes(5).toDate());
			} else {
				encounter.setEncounterDatetime(new Date());
			}
			
			encounter.setVisit(visit);
			if (form != null) {
				encounter.setForm(form);
			}
		}
		return encounter;
	}
	
	public static Encounter persistEncounter(Patient patient, EncounterType encounterTypeByUuid, Location sessionLocation,
	        Visit visit) {
		Encounter encounter = createEncounter(patient, encounterTypeByUuid, sessionLocation, visit);
		if (encounter != null) {
			encounter = Context.getEncounterService().saveEncounter(encounter);
		}
		return encounter;
	}
	
	public static Encounter getOrCreateEncounter(Patient patient, EncounterType consultationEncounterType, Visit visit,
	        Location location) {
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, location, null, null, null,
		        null, Collections.singletonList(consultationEncounterType), null, null, Collections.singletonList(visit),
		        false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		Encounter encounter;
		if (encounters.isEmpty()) {
			encounter = persistEncounter(patient, consultationEncounterType, location, visit);
		} else {
			encounter = encounters.get(0);
		}
		
		return encounter;
	}
	
	public static Visit getPatientActiveVisit(Patient patient, Location location, boolean ensureActive) {
		Visit currentVisit = null;
		AdtService adtService = Context.getService(AdtService.class);
		if (ensureActive) {
			currentVisit = adtService.ensureActiveVisit(patient, location);
		} else {
			VisitDomainWrapper wrapper = adtService.getActiveVisit(patient, location);
			currentVisit = wrapper == null ? null : wrapper.getVisit();
		}
		return currentVisit;
	}
	
	public static Visit getLastPatientVisit(Patient p, VisitType visitType) {
		Visit visit = null;
		
		if (p != null) {
			List<Visit> visitList = Context.getVisitService().getVisitsByPatient(p, true, false);
			if (visitType != null) {
				visitList = visitList.stream()
				        .filter(x -> x.getVisitType().getVisitTypeId().equals(visitType.getVisitTypeId()))
				        .collect(Collectors.toList());
			}
			if (visitList.size() > 0) {
				visitList.sort((o1, o2) -> {
					if (o1.getStartDatetime() == null || o2.getStartDatetime() == null) {
						return 0;
					}
					return o1.getStartDatetime().compareTo(o2.getStartDatetime());
				});
				visit = visitList.get(visitList.size() - 1);
			}
		}
		return visit;
	}
	
	public static Obs creatObs(Encounter encounter, Concept question) {
		Obs newObs = new Obs();
		newObs.setPerson(encounter.getPatient());
		newObs.setLocation(encounter.getLocation());
		newObs.setCreator(encounter.getCreator());
		newObs.setDateCreated(encounter.getDateCreated());
		newObs.setEncounter(encounter);
		newObs.setConcept(question);
		newObs.setObsDatetime(encounter.getEncounterDatetime());
		return newObs;
	}
	
	public static void startConsultation(Patient patient, String option, String description,
	        PatientQueueingService patientQueueingService, EncounterService encounterService, Location location,
	        Cases existingCase, BotswanaEmrService botswanaEmrService, OrderService orderService, Order referral,
	        Visit currentVisit, User currentUser) {
		EncounterType consultationsNewEncounterType = encounterService
		        .getEncounterTypeByUuid(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID);
		EncounterType consultationsExistingEncounterType = encounterService
		        .getEncounterTypeByUuid(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID);
		
		Encounter consultationsNewEncounter = null;
		Encounter consultationsExistingEncounter = null;
		PatientQueue patientQueue = activeConsultationQueue(patient);
		
		if (patientQueue == null) {
			patientQueue = createOrUpdatePatientQueue(patient, location, null, currentVisit, currentUser,
			    patientQueueingService);
		}
		if (referral != null) {
			orderService.updateOrderFulfillerStatus(referral, FulfillerStatus.IN_PROGRESS, "Referral in progress");
		}
		if (consultationsNewEncounterType != null) {
			consultationsNewEncounter = createEncounter(patient, consultationsNewEncounterType, location, null);
		}
		if (consultationsExistingEncounterType != null) {
			consultationsExistingEncounter = createEncounter(patient, consultationsExistingEncounterType, location, null);
		}
		if (patientQueue != null && patientQueue.getStatus() != null
		        && patientQueue.getStatus().equals(PatientQueue.Status.PENDING)) {
			Diagnosis diagnosis = null;
			Cases newCase = null;
			Visit visit = null;
			String caseId = "";
			if (consultationsNewEncounter != null && option.equals("new")) {
				diagnosis = setDiagnosisObject(patient, description, consultationsNewEncounter);
				//create the case here
				newCase = createCase(description, botswanaEmrService, patient);
				//save the encounter
				encounterService.saveEncounter(consultationsNewEncounter);
				//link the saved encounter, case and the case encounter
				createCaseEncounter(newCase, consultationsNewEncounter, botswanaEmrService, patient);
				//change the queue status to picked
				updatePatientQueue(patientQueueingService, consultationsNewEncounter, patientQueue);
				
				visit = consultationsNewEncounter.getVisit();
				caseId = String.valueOf(newCase.getCaseId());
				
			}
			if (consultationsExistingEncounter != null && option.equals("existing") && existingCase != null) {
				//do what is related to the existing case
				if (StringUtils.isNotBlank(description)) {
					diagnosis = setDiagnosisObject(patient, description, consultationsExistingEncounter);
				}
				//save encounter
				encounterService.saveEncounter(consultationsExistingEncounter);
				//save an existing case
				createCaseEncounter(existingCase, consultationsExistingEncounter, botswanaEmrService, patient);
				//save linked case
				CaseLinkage caseLinkage = new CaseLinkage();
				caseLinkage.setPatient(patient);
				caseLinkage.setCreator(Context.getAuthenticatedUser());
				caseLinkage.setCurrentEncounter(consultationsExistingEncounter);
				caseLinkage.setCases(existingCase);
				caseLinkage.setLinkedOn(consultationsExistingEncounter.getDateCreated());
				
				botswanaEmrService.saveCaseLinkage(caseLinkage);
				//change the queue status to picked
				updatePatientQueue(patientQueueingService, consultationsExistingEncounter, patientQueue);
				
				visit = consultationsExistingEncounter.getVisit();
				caseId = String.valueOf(existingCase.getCaseId());
			}
			//update diagnosis
			if (diagnosis != null) {
				DiagnosisService diagnosisService = Context.getDiagnosisService();
				diagnosisService.save(diagnosis);
			}
			
			VisitAttributeType visitAttributeType = Context.getVisitService()
			        .getVisitAttributeTypeByUuid(CASE_ID_VISIT_ATTRIBUTE);
			if (visitAttributeType != null) {
				VisitAttribute visitAttribute = new VisitAttribute();
				//				visitAttribute.setVisit(visit);
				visitAttribute.setAttributeType(visitAttributeType);
				visitAttribute.setValue(caseId.toString());
				Objects.requireNonNull(visit).setAttribute(visitAttribute);
				Context.getVisitService().saveVisit(visit);
			}
		}
	}
	
	public static PatientQueue createOrUpdatePatientQueue(Patient patient, Location visitLocation, Location queueRoom,
	        Visit visit, User user, PatientQueueingService patientQueueingService) {
		PatientQueue patientQueue = null;
		
		if (queueRoom != null) {
			PatientQueue incompletePatientQueue = getPatientQueue(patient, queueRoom);
			if (incompletePatientQueue != null) {
				patientQueue = incompletePatientQueue;
			}
		}
		
		if (patientQueue == null) {
			patientQueue = new PatientQueue();
		}
		
		patientQueue.setLocationFrom(visitLocation);
		patientQueue.setPatient(patient);
		patientQueue.setLocationTo(queueRoom);
		patientQueue.setStatus(PatientQueue.Status.PENDING);
		patientQueue.setCreator(user);
		patientQueue.setDateCreated(new Date());
		patientQueueingService.assignVisitNumberForToday(patientQueue);
		if (visit != null) {
			patientQueue.setPriorityComment(BotswanaEmrUtils.getPatientSeverityLevel(visit));
		}
		
		if (queueRoom != null) {
			patientQueue.setQueueRoom(queueRoom);
		}
		
		// Save and return the patient queue
		return patientQueueingService.savePatientQue(patientQueue);
	}
	
	public static PatientQueue getPatientQueue(Patient patient, Location location) {
		return Context.getService(PatientQueueingService.class).getIncompletePatientQueue(patient, location);
	}
	
	private static void updatePatientQueue(PatientQueueingService patientQueueingService,
	        Encounter consultationsNewEncounter, PatientQueue patientQueue) {
		patientQueue.setStatus(PatientQueue.Status.PICKED);
		patientQueue.setDatePicked(consultationsNewEncounter.getDateCreated());
		patientQueue.setEncounter(consultationsNewEncounter);
		patientQueue.setDateCreated(consultationsNewEncounter.getDateCreated());
		patientQueue.setDatePicked(consultationsNewEncounter.getDateCreated());
		//update the queue
		patientQueueingService.savePatientQue(patientQueue);
	}
	
	private static Diagnosis setDiagnosisObject(Patient patient, String description, Encounter encounter) {
		Diagnosis diagnosis = null;
		if (StringUtils.isNotBlank(description)) {
			diagnosis = new Diagnosis();
			diagnosis.setPatient(patient);
			diagnosis.setEncounter(encounter);
			diagnosis.setDiagnosis(new CodedOrFreeText(null, null, description));
			diagnosis.setCertainty(PROVISIONAL);
			diagnosis.setRank(200);
		}
		return diagnosis;
	}
	
	public static void endConsultation(PatientQueueingService patientQueueingService, Patient patient, Location location) {
		PatientQueue patientQueue = patientQueueingService.getIncompletePatientQueue(patient, location);
		patientQueueingService.completePatientQueue(patientQueue);
	}
	
	public static EncounterType getEncounterType(String uuid) {
		return Context.getEncounterService().getEncounterTypeByUuid(uuid);
	}
	
	public static List<Encounter> sortEncountersByEncounterDatetime(List<Encounter> encounters) {
		Collections.sort(encounters, new Comparator<Encounter>() {
			
			@Override
			public int compare(Encounter a, Encounter b) {
				return b.getEncounterDatetime().compareTo(a.getEncounterDatetime());
			}
		});
		return encounters;
	}
	
	public static List<Concept> getVitalsConcepts() {
		return Arrays.asList(getConcept(WEIGHT), getConcept(HEIGHT), getConcept(BMI), getConcept(BSA),
		    getConcept(RESPIRATORY_RATE), getConcept(PULSE), getConcept(HEAD_CIRCUMFERENCE), getConcept(GLUCOSE_LEVEL),
		    getConcept(OXYGEN_SATURATION), getConcept(LMP), getConcept(PREGNANCY_STATUS), getConcept(CONSCIOUS),
		    getConcept(RESPONSIVE), getConcept(PUEPERIUM_PULSE), getConcept(DIASTOLIC_BP), getConcept(SYSTOLIC_BP));
	}
	
	public static List<Concept> getGestationTrendsConcepts() {
		return Arrays.asList(getConcept(FUNDAL_HEIGHT_CONCEPT_UUID), getConcept(MATERNAL_WEIGHT_CONCEPT_UUID),
		    getConcept(FOETAL_WEIGHT_CONCEPT_UUID), getConcept(GESTATION_WEEK_CONCEPT_UUID));
	}
	
	public static List<Concept> getPartographConcepts() {
		return Arrays.asList(getConcept(PULSE), getConcept(RESPIRATORY_RATE), getConcept(CONTRACTIONS_PER_TEN_MINUTES),
		    getConcept(DIASTOLIC_BP), getConcept(SYSTOLIC_BP), getConcept(TEMPERATURE), getConcept(URINE),
		    getConcept(FETAL_HEART_RATE), getConcept(CERVIX_SIZE), getConcept(DECENT_OF_THE_HEAD));
	}
	
	public static String getConceptName(Concept concept) {
		return concept.getDisplayString();
	}
	
	public static String getNumericValue(Obs obs) {
		return obs.getValueNumeric().toString();
	}
	
	public static List<Order> getAllOrders(List<Patient> patients) {
		List<Order> orders = new ArrayList<>();
		for (Patient patient : patients) {
			orders.addAll(orderService.getAllOrdersByPatient(patient));
		}
		return orders;
	}
	
	public static Obs getLatestObs(Visit currentPatientVisit, String severityConceptUuid) {
		ObsService obsService = Context.getObsService();
		List<Obs> obsList = obsService.getObservations(
		    Collections.singletonList(currentPatientVisit.getPatient().getPerson()),
		    currentPatientVisit.getNonVoidedEncounters(), Collections.singletonList(getConcept(severityConceptUuid)), null,
		    null, null, null, 1, null, currentPatientVisit.getStartDatetime(), null, false);
		Obs obs = null;
		if (!obsList.isEmpty()) {
			obs = obsList.get(0);
		}
		
		return obs;
	}
	
	public static Obs getLatestObs(Person person, List<Encounter> encounters, String conceptUuid, Date fromDate,
	        Date toDate) {
		ObsService obsService = Context.getObsService();
		List<Obs> obsList = obsService.getObservations(Collections.singletonList(person), encounters,
		    Collections.singletonList(getConcept(conceptUuid)), null, null, null, null, 1, null, fromDate, toDate, false);
		Obs obs = null;
		if (!obsList.isEmpty()) {
			obs = obsList.get(0);
		}
		
		return obs;
	}
	
	public static boolean onArt(Patient patient) {
		PersonAttributeType currentRegimen;
		currentRegimen = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.ART_REGIMEN_ATTRIBUTE_TYPE_UUID);
		return patient.getAttribute(currentRegimen) != null && patient.getAttribute(currentRegimen).getValue() != null;
	}
	
	public static AppointmentBlock getAppointmentBlock(Date startDate, Date endDate, Provider provider, Location location,
	        AppointmentType type) {
		Set<AppointmentType> appointmentTypeSet = new HashSet<>();
		appointmentTypeSet.add(type);
		
		AppointmentBlock appointmentBlock = new AppointmentBlock();
		appointmentBlock.setStartDate(startDate);
		appointmentBlock.setEndDate(endDate);
		appointmentBlock.setProvider(provider);
		appointmentBlock.setLocation(location);
		appointmentBlock.setTypes(appointmentTypeSet);
		appointmentBlock.setCreator(Context.getAuthenticatedUser());
		appointmentBlock.setDateCreated(new Date());
		return appointmentBlock;
	}
	
	public static TimeSlot getAppointmentTimeSlot(Date startDate, Date endDate, Provider provider, Location location,
	        AppointmentType type) {
		TimeSlot timeSlot = new TimeSlot();
		timeSlot.setStartDate(startDate);
		timeSlot.setEndDate(endDate);
		timeSlot.setCreator(Context.getAuthenticatedUser());
		timeSlot.setDateCreated(new Date());
		
		//Check if appointment block is already created
		AppointmentBlock appointmentBlock = Context.getService(AppointmentService.class)
		        .getAppointmentBlocks(startDate, endDate, String.valueOf(location.getLocationId()), provider, type).stream()
		        .findFirst().orElse(null);
		if (appointmentBlock == null) {
			appointmentBlock = getAppointmentBlock(startDate, endDate, provider, location, type);
		}
		timeSlot.setAppointmentBlock(appointmentBlock);
		
		return timeSlot;
	}
	
	public static void setSingleActiveReferralOrder(FragmentModel fragmentModel, Patient patient, Visit activeVisit) {
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		Comparator<Order> orderDateComparator = Comparator.comparing(Order::getDateCreated);
		OrderType referralOrderType = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		
		Optional<Order> referral = getReferral(patient, orderDateComparator, referralOrderType, activeVisit);
		
		if (referral.isPresent()) {
			Optional<Obs> referringDepartmentObs = getCurrentObs(obsDateComparator, referral.get(),
			    REFERRING_DEPARTMENT_CONCEPT_UUID);
			Optional<Obs> receivingDepartmentObs = getCurrentObs(obsDateComparator, referral.get(),
			    RECEIVING_DEPARTMENT_CONCEPT_UUID);
			fragmentModel.addAttribute("referralId", referral.get().getOrderId());
			fragmentModel.addAttribute("referralEncounter", referral.get().getEncounter());
			if (referringDepartmentObs.isPresent()) {
				fragmentModel.addAttribute("referringDepartment", referringDepartmentObs.get());
			} else {
				fragmentModel.addAttribute("referringDepartment", "");
			}
			fragmentModel.addAttribute("facility", referral.get().getEncounter().getLocation());
			fragmentModel.addAttribute("referredTo", referral.get().getOrderer().getName());
			fragmentModel.addAttribute("reason",
			    referral.get().getOrderReason() != null ? referral.get().getOrderReason().getName()
			            : referral.get().getOrderReasonNonCoded() != null ? referral.get().getOrderReasonNonCoded() : "");
			if (receivingDepartmentObs.isPresent()) {
				fragmentModel.addAttribute("receivingFacility", receivingDepartmentObs.get());
			} else {
				fragmentModel.addAttribute("receivingFacility", "");
			}
			fragmentModel.addAttribute("referredBy", referral.get().getCreator().getPersonName());
			fragmentModel.addAttribute("referredDate", referral.get().getDateCreated());
			fragmentModel.addAttribute("status", referral.get().getFulfillerStatus());
			fragmentModel.addAttribute("notes", referral.get().getCommentToFulfiller());
		} else {
			fragmentModel.addAttribute("referralId", "");
			fragmentModel.addAttribute("referralEncounter", "");
			fragmentModel.addAttribute("referringDepartment", "");
			fragmentModel.addAttribute("facility", "");
			fragmentModel.addAttribute("referredTo", "");
			fragmentModel.addAttribute("reason", "");
			fragmentModel.addAttribute("receivingFacility", "");
			fragmentModel.addAttribute("referredBy", "");
			fragmentModel.addAttribute("referredDate", "");
			fragmentModel.addAttribute("status", "");
			fragmentModel.addAttribute("notes", "");
		}
	}
	
	public static void getArtVisitTypes(FragmentModel fragmentModel, Visit visit) {
		fragmentModel.addAttribute("currentVisitType",
		    visit != null && visit.getVisitType() != null ? visit.getVisitType().getUuid() : "");
		fragmentModel.addAttribute("prepVisitType", BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID);
		fragmentModel.addAttribute("prepFollowUpVisitType", BotswanaEmrConstants.PREP_FOLLOW_UP_VISIT_TYPE_UUID);
		fragmentModel.addAttribute("pepVisitType", BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID);
		fragmentModel.addAttribute("pepFollowUpVisitType", BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID);
		fragmentModel.addAttribute("artInitiationVisitType", BotswanaEmrConstants.ART_INITIATION_VISIT_TYPE_UUID);
		fragmentModel.addAttribute("artFollowUpVisitType", BotswanaEmrConstants.ART_FOLLOW_UP_VISIT_TYPE_UUID);
	}
	
	public static Optional<Order> getReferral(Patient patient, Comparator<Order> orderDateComparator, OrderType orderType,
	        Visit visit) {
		return orderService.getAllOrdersByPatient(patient).stream().filter(order -> order.getOrderType().equals(orderType))
		        .filter(order -> order.getEncounter().getVisit().equals(visit)).max(orderDateComparator);
	}
	
	public static Optional<Obs> getCurrentObs(Comparator<Obs> obsDateComparator, Order referral, String obsUuid) {
		return referral.getEncounter().getObs().stream().filter(obs -> obs.getConcept().getUuid().equals(obsUuid))
		        .max(obsDateComparator);
	}
	
	public static AppointmentType getDefaultAppointmentType() {
		AppointmentService service = Context.getService(AppointmentService.class);
		AppointmentType appointmentType = service
		        .getAppointmentTypeByUuid(BotswanaEmrConstants.GENERAL_CLINICAL_REVIEW_APPOINTMENT_TYPE);
		if (appointmentType == null) {
			appointmentType = createDefaultAppointmentType(service);
		}
		return appointmentType;
	}
	
	private static AppointmentType createDefaultAppointmentType(AppointmentService service) {
		AppointmentType existingAppointmentType = service.getAppointmentTypeByUuid(REGULAR_FOLLOW_UP_APPOINTMENT_TYPE);
		if (existingAppointmentType != null) {
			return existingAppointmentType;
		}
		
		AppointmentType appointmentType = new AppointmentType();
		appointmentType.setUuid(REGULAR_FOLLOW_UP_APPOINTMENT_TYPE);
		appointmentType.setName("Regular follow up appointment");
		appointmentType.setDescription("Regular follow up appointment type");
		appointmentType.setDuration(10);
		
		return service.saveAppointmentType(appointmentType);
	}
	
	public static Calendar getCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}
	
	public static Date getDateToday() {
		Calendar calendar = Calendar.getInstance();
		calendar.clear(Calendar.HOUR);
		calendar.clear(Calendar.MINUTE);
		calendar.clear(Calendar.SECOND);
		return calendar.getTime();
	}
	
	public static Date getStartOfDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMinimum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getMinimum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getMinimum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMinimum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
	
	public static Date getStartOfYesterday() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1); // set to yesterday
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMinimum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getMinimum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getMinimum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMinimum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
	
	public static Date getEndOfDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
	
	public static Date getEndOfYesterday() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1); // set to yesterday
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getMaximum(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendar.getMaximum(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, calendar.getMaximum(Calendar.SECOND));
		calendar.set(Calendar.MILLISECOND, calendar.getMaximum(Calendar.MILLISECOND));
		return calendar.getTime();
	}
	
	public static Date getStartTimeOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static Date getEndTimeOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}
	
	public static EncounterSearchCriteria getEncounterSearchCriteriaPatientTypesAndCurrentVisit(Patient patient,
	        List<EncounterType> types, Visit currentVisit) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .setVisits(Arrays.asList(currentVisit)).createEncounterSearchCriteria();
		
	}
	
	public static List<Obs> getLabOrderResultsIfCompleteSet(Integer labTestOrderId, LabService labService) {
		
		LabTest labTest = labService.getLabTestById(labTestOrderId);
		List<Obs> resultsObsList = new ArrayList<Obs>();
		if (labTest != null && labTest.getStatus().equals("COMPLETED")) {
			Encounter resultsEncounter = labTest.getEncounterResult();
			Obs overalObsGroup = null;
			
			if (resultsEncounter != null) {
				for (Obs obs : resultsEncounter.getAllObs()) {
					if (obs.getConcept().equals(getConcept(TB_TEST_TYPE))
					        && obs.getValueCoded().equals(labTest.getConcept())) {
						overalObsGroup = obs.getObsGroup();
						break;
					}
				}
			}
			if (overalObsGroup != null) {
				for (Obs obsResults : resultsEncounter.getAllObs()) {
					if (obsResults.getObsGroup() != null && obsResults.getObsGroup().equals(overalObsGroup)) {
						resultsObsList.add(obsResults);
					}
					
				}
			}
		}
		return resultsObsList;
	}
	
	public static Map<String, String> getLabResultsKeyMap(List<Obs> obsList) {
		Map<String, String> returnedMap = new HashMap<String, String>();
		if (obsList != null && obsList.size() > 0) {
			for (Obs obs : obsList) {
				returnedMap.put(obs.getConcept().getDisplayString(), getObsValueString(obs));
			}
		}
		return returnedMap;
	}
	
	public static String getObsValueString(Obs obs) {
		String value = "";
		if (obs.getValueCoded() != null) {
			value = obs.getValueCoded().getDisplayString();
		} else if (obs.getValueNumeric() != null) {
			value = String.valueOf(obs.getValueNumeric());
		} else if (obs.getValueText() != null) {
			value = obs.getValueText();
		} else if (obs.getValueDatetime() != null) {
			value = formatDateWithTime(obs.getValueDatetime(), null);
		}
		return value;
	}
	
	public static List<Encounter> getConsultationPatientEncounters(Patient patient, Integer limit) {
		EncounterType newConsultationEncounterType = getEncounterType(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID);
		EncounterType existingConsultationEncounterType = getEncounterType(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID);
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		List<Encounter> consultationPatientEncounters = botswanaEmrService
		        .getEncounterLimits(getEncounterSearchCriteriaUsingEncounterTypes(patient,
		            Arrays.asList(newConsultationEncounterType, existingConsultationEncounterType)), limit);
		
		return consultationPatientEncounters;
	}
	
	public static List<MedicalHistorySimplifier> simplifiedMedicalHistory(List<Encounter> consultationPatientEncounters) {
		List<MedicalHistorySimplifier> simplifier = new ArrayList<>();
		for (Encounter consultationEncounter : consultationPatientEncounters) {
			MedicalHistorySimplifier medicalHistorySimplifier = new MedicalHistorySimplifier();
			medicalHistorySimplifier.setPatientId(consultationEncounter.getPatient().getPatientId().toString());
			Cases associatedCase = getAssociatedCase(consultationEncounter);
			medicalHistorySimplifier.setCaseId(associatedCase != null ? associatedCase.getCaseId().toString() : "");
			medicalHistorySimplifier.setCaseDescription(associatedCase != null ? associatedCase.getDescription() : "");
			medicalHistorySimplifier.setEncounterDate(consultationEncounter.getEncounterDatetime());
			medicalHistorySimplifier.setCaseStatus(associatedCase != null ? associatedCase.getCaseStatus().toString() : "");
			medicalHistorySimplifier.setVisitId(String.valueOf(consultationEncounter.getVisit().getId()));
			medicalHistorySimplifier.setFacility(String.valueOf(consultationEncounter.getLocation().getName()));
			
			medicalHistorySimplifier.setName(formatPersonName(consultationEncounter.getPatient().getPersonName()));
			medicalHistorySimplifier.setGender(formatGender(consultationEncounter.getPatient()));
			medicalHistorySimplifier.setCreator(BotswanaEmrUtils.formatPerson(
			    new ArrayList<>(consultationEncounter.getEncounterProviders()).get(0).getProvider().getPerson()));
			medicalHistorySimplifier.setDuration(formatDurationSinceRegistration(consultationEncounter.getPatient()));
			medicalHistorySimplifier
			        .setTypeText(formatEncounterTypeText(consultationEncounter.getEncounterType().getUuid()));
			
			simplifier.add(medicalHistorySimplifier);
		}
		
		return simplifier;
	}
	
	private static Cases getAssociatedCase(Encounter consultationEncounter) {
		CaseEncounter caseEncounter = Context.getService(BotswanaEmrService.class)
		        .getCaseByEncounterId(consultationEncounter);
		if (caseEncounter != null) {
			return caseEncounter.getCases();
		}
		
		return null;
	}
	
	public static EncounterSearchCriteria getEncounterSearchCriteriaUsingEncounterTypes(Patient patient,
	        List<EncounterType> types) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(types).setPatient(patient).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		
	}
	
	public static List<EncounterType> getConsultationEncounterTypes() {
		EncounterType newConsultationEncounterType = getEncounterType(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID);
		EncounterType existingConsultationEncounterType = getEncounterType(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID);
		List<EncounterType> consultationEncounterTypes = Arrays.asList(newConsultationEncounterType,
		    existingConsultationEncounterType);
		return consultationEncounterTypes;
	}
	
	public static Cases createCase(String description, BotswanaEmrService botswanaEmrService, Patient patient) {
		Cases cases = new Cases();
		cases.setDescription(description);
		cases.setCaseStatus(Cases.CaseStatus.OPEN);
		cases.setCreator(Context.getAuthenticatedUser().getUserId());
		cases.setDateCreated(new Date());
		cases.setRetired(false);
		cases.setPatient(patient);
		//save the cases
		return botswanaEmrService.saveCase(cases);
	}
	
	public static void createCaseEncounter(Cases cases, Encounter encounter, BotswanaEmrService botswanaEmrService,
	        Patient patient) {
		CaseEncounter caseEncounter = new CaseEncounter();
		caseEncounter.setEncounter(encounter);
		caseEncounter.setCaseId(cases);
		caseEncounter.setPatient(patient);
		caseEncounter.setCreator(Context.getAuthenticatedUser());
		caseEncounter.setDateCreated(new Date());
		//save the case encounter
		botswanaEmrService.saveCaseEncounter(caseEncounter);
	}
	
	public static List<CaseEncounter> sortCaseEncounterByDateCreated(List<CaseEncounter> caseEncounters) {
		Collections.sort(caseEncounters, new Comparator<CaseEncounter>() {
			
			@Override
			public int compare(CaseEncounter a, CaseEncounter b) {
				return a.getDateCreated().compareTo(b.getDateCreated());
			}
		});
		return caseEncounters;
	}
	
	public static String getPatientSeverityLevel(Visit currentPatientVisit) {
		Obs severityLevelObs = getLatestObs(currentPatientVisit, SEVERITY_CONCEPT_UUID);
		if (severityLevelObs != null) {
			return severityLevelObs.getValueCoded().getName().getName();
		}
		
		return "";
	}
	
	public static List<Drug> filterDOTDrugs() {
		String dsTBDrugStringUuid = Context.getAdministrationService().getGlobalProperty(DS_TB_DRUGS, "");
		List<String> dsTbDrugUuids = Arrays.asList(dsTBDrugStringUuid.split(","));
		
		List<Drug> allDrugs = Context.getConceptService().getAllDrugs();
		allDrugs.removeIf(drug -> dsTbDrugUuids.contains(drug.getConcept().getUuid()));
		return allDrugs.stream().filter(drug -> dsTbDrugUuids.contains(drug.getConcept().getUuid()))
		        .collect(Collectors.toList());
	}
	
	public static Program getProgramByIdOrUuid(Object programId) {
		ProgramWorkflowService service = Context.getProgramWorkflowService();
		Program program = null;
		if (programId instanceof String) {
			program = service.getProgramByUuid(String.valueOf(programId));
		} else if (programId instanceof Integer) {
			program = service.getProgram((Integer) programId);
		}
		
		return program;
	}
	
	public static List<Order> getDrugOrders(Date startDate, Date endDate, Order.FulfillerStatus fulfillerStatus,
	        Location location) {
		OrderSearchCriteriaBuilder orderSearchCriteriaBuilder = new OrderSearchCriteriaBuilder();
		orderSearchCriteriaBuilder.setActivatedOnOrAfterDate(startDate);
		orderSearchCriteriaBuilder.setActivatedOnOrBeforeDate(endDate);
		orderSearchCriteriaBuilder.setFulfillerStatus(fulfillerStatus);
		orderSearchCriteriaBuilder
		        .setOrderTypes(Arrays.asList(Context.getOrderService().getOrderTypeByUuid(DRUG_ORDER_TYPE_UUID)));
		OrderSearchCriteria orderSearchCriteria = orderSearchCriteriaBuilder.build();
		
		List<Order> orders = Context.getOrderService().getOrders(orderSearchCriteria);
		
		if (location != null) {
			orders = orders.stream().filter(o -> o.getEncounter() != null && o.getEncounter().getLocation() != null
			        && o.getEncounter().getLocation().equals(location)).collect(Collectors.toList());
		}
		
		return orders;
	}
	
	public static List<String> getDate() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		List<String> days = new ArrayList<String>();
		int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
		now.add(Calendar.DAY_OF_MONTH, delta);
		for (int i = 0; i < 7; i++) {
			days.add(format.format(now.getTime()));
			now.add(Calendar.DAY_OF_MONTH, 1);
		}
		return days;
	}
	
	public static Date parseDateFromString(String dateString) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
	}
	
	public static List<Encounter> getFilledEncounterForPatientPerTheCurrentVisit(Visit currentVisit, Patient patient,
	        @SpringBean("encounterService") EncounterService encounterService,
	        @SpringBean("visitService") VisitService visitService, List<String> listOfFormsToExcludeUuids) {
		List<Encounter> encounterList = encounterService.getEncountersByPatient(patient);
		List<Encounter> neededEncounters = new ArrayList<Encounter>();
		for (Encounter encounter : encounterList) {
			if (encounter.getForm() != null) {
				if (currentVisit != null && encounter.getVisit().equals(currentVisit)
				        && !listOfFormsToExcludeUuids.contains(encounter.getForm().getUuid())) {
					neededEncounters.add(encounter);
				}
			}
		}
		return neededEncounters;
	}
	
	public static PatientProgram getLastDiscontinuedPatientProgram(Patient patient, Program program) {
		List<PatientProgram> allPatientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, program,
		    null, null, null, null, false);
		
		PatientProgram activePatientProgramPicked = null;
		for (PatientProgram patientProgram : allPatientPrograms) {
			if (patientProgram.getActive()) {
				activePatientProgramPicked = patientProgram;
				break;
			}
		}
		return activePatientProgramPicked;
		
	}
	
	public static EncounterSearchCriteria getEncounterTypeSearchCriteria(Patient patient,
	        List<EncounterType> encounterTypes) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(encounterTypes).setPatient(patient)
		        .setIncludeVoided(false).createEncounterSearchCriteria();
		
	}
	
	public static List<HtmlForm> getHtsEncounterForms(HtmlFormEntryService htmlFormEntryService, FormService formService) {
		
		Form htsRegisterHtmlForm = formService.getFormByUuid(HTS_DIAGNOSTICS_TEST_FORM_UUID);
		Form htsVerificationHtmlForm = formService.getFormByUuid(HTS_VERIFICATION_FORM_UUID);
		Form htsSupplementaryTestHtmlForm = formService.getFormByUuid(HTS_SUPPLEMENTARY_TEST_FORM_UUID);
		Form htsPnsIndexHtmlForm = formService.getFormByUuid(HTS_PNS_INDEX_FORM_UUID);
		Form htsChildIndexInformationHtmlForm = formService.getFormByUuid(HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID);
		
		Form htsSexContactInformationHtmlForm = formService.getFormByUuid(HTS_IPV_SCREENING_FORM_UUID);
		Form htsPartnerCharacteristicForm = formService.getFormByUuid(HTS_PARTNER_CHARACTERISTIC_AND_HIV_FORM_UUID);
		Form htsContactForm = formService.getFormByUuid(HTS_CONTACTS_FORM_UUID);
		Form htsReferralsAndOutcomeForm = formService.getFormByUuid(HTS_REFERRALS_AND_OUTCOME_FORM_UUID);
		
		Form htsPostTestHtmlForm = formService.getFormByUuid(HTS_POST_TEST_FORM_UUID);
		Form htsSelfTestDistributionHtmlForm = formService.getFormByUuid(HTS_SELF_TEST_DISTRIBUTION_FORM_UUID);
		
		Form htsHIVTestReferralHtmlForm = formService.getFormByUuid(HTS_HIV_TEST_REFERRAL_FORM_UUID);
		Form htsDocumentPriorTestHtmlForm = formService.getFormByUuid(HTS_DOCUMENT_PRIOR_TEST_FORM_UUID);
		
		/*
				TODO: Update this segment once the forms have been added
				Form htsPnsSexContactHtmlForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_PNS_SEX_CONTACT_FORM_UUID);
				Form htsPnsChildrenOfIndexHtmlForm = formService.getFormByUuid(BotswanaEmrConstants.HTS_PNS_CHILDREN_OF_INDEX_FORM_UUID);
		*/
		HtmlForm htsRegisterForm = htmlFormEntryService.getHtmlFormByForm(htsRegisterHtmlForm);
		HtmlForm htsVerificationForm = htmlFormEntryService.getHtmlFormByForm(htsVerificationHtmlForm);
		HtmlForm htsSupplementaryTestForm = htmlFormEntryService.getHtmlFormByForm(htsSupplementaryTestHtmlForm);
		HtmlForm htsPnsIndexForm = htmlFormEntryService.getHtmlFormByForm(htsPnsIndexHtmlForm);
		HtmlForm htsChildIndexInformationForm = htmlFormEntryService.getHtmlFormByForm(htsChildIndexInformationHtmlForm);
		
		HtmlForm htsSexContactInformationForm = htmlFormEntryService.getHtmlFormByForm(htsSexContactInformationHtmlForm);
		HtmlForm htsPartnerCharacteristicHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsPartnerCharacteristicForm);
		HtmlForm htsContactHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsContactForm);
		HtmlForm htsReferralsAndOutcomeHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsReferralsAndOutcomeForm);
		HtmlForm htsPostTestForm = htmlFormEntryService.getHtmlFormByForm(htsPostTestHtmlForm);
		HtmlForm htsSelfTestDistributionForm = htmlFormEntryService.getHtmlFormByForm(htsSelfTestDistributionHtmlForm);
		HtmlForm htsHIVTestReferralForm = htmlFormEntryService.getHtmlFormByForm(htsHIVTestReferralHtmlForm);
		HtmlForm htsDocumentPriorTestForm = htmlFormEntryService.getHtmlFormByForm(htsDocumentPriorTestHtmlForm);
		
		List<HtmlForm> htsEncounterForms = new ArrayList<>();
		if (htsRegisterForm != null) {
			htsEncounterForms.add(htsRegisterForm);
		}
		
		if (htsVerificationForm != null) {
			htsEncounterForms.add(htsVerificationForm);
		}
		
		if (htsSupplementaryTestForm != null) {
			htsEncounterForms.add(htsSupplementaryTestForm);
		}
		
		if (htsPnsIndexForm != null) {
			htsEncounterForms.add(htsPnsIndexForm);
		}
		
		if (htsChildIndexInformationForm != null) {
			htsEncounterForms.add(htsChildIndexInformationForm);
		}
		
		if (htsSexContactInformationForm != null) {
			htsEncounterForms.add(htsSexContactInformationForm);
		}
		
		if (htsPartnerCharacteristicHtmlForm != null) {
			htsEncounterForms.add(htsPartnerCharacteristicHtmlForm);
		}
		
		if (htsContactHtmlForm != null) {
			htsEncounterForms.add(htsContactHtmlForm);
		}
		
		if (htsReferralsAndOutcomeHtmlForm != null) {
			htsEncounterForms.add(htsReferralsAndOutcomeHtmlForm);
		}
		
		if (htsPostTestForm != null) {
			htsEncounterForms.add(htsPostTestForm);
		}
		
		if (htsSelfTestDistributionForm != null) {
			htsEncounterForms.add(htsSelfTestDistributionForm);
		}
		
		if (htsHIVTestReferralForm != null) {
			htsEncounterForms.add(htsHIVTestReferralForm);
		}
		
		if (htsDocumentPriorTestForm != null) {
			htsEncounterForms.add(htsDocumentPriorTestForm);
		}
		return htsEncounterForms;
	}
	
	public static List<HtmlForm> getPnsEncounterForms(HtmlFormEntryService htmlFormEntryService, FormService formService) {
		
		Form htsChildIndexInformationHtmlForm = formService.getFormByUuid(HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID);
		
		Form htsSexContactInformationHtmlForm = formService.getFormByUuid(HTS_IPV_SCREENING_FORM_UUID);
		Form htsPartnerCharacteristicForm = formService.getFormByUuid(HTS_PARTNER_CHARACTERISTIC_AND_HIV_FORM_UUID);
		Form htsContactForm = formService.getFormByUuid(HTS_CONTACTS_FORM_UUID);
		Form htsReferralsAndOutcomeForm = formService.getFormByUuid(HTS_REFERRALS_AND_OUTCOME_FORM_UUID);
		
		HtmlForm htsChildIndexInformationForm = htmlFormEntryService.getHtmlFormByForm(htsChildIndexInformationHtmlForm);
		
		HtmlForm htsSexContactInformationForm = htmlFormEntryService.getHtmlFormByForm(htsSexContactInformationHtmlForm);
		HtmlForm htsPartnerCharacteristicHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsPartnerCharacteristicForm);
		HtmlForm htsContactHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsContactForm);
		HtmlForm htsReferralsAndOutcomeHtmlForm = htmlFormEntryService.getHtmlFormByForm(htsReferralsAndOutcomeForm);
		
		List<HtmlForm> htsPnsEncounterForms = new ArrayList<>();
		
		if (htsChildIndexInformationForm != null) {
			htsPnsEncounterForms.add(htsChildIndexInformationForm);
		}
		
		if (htsSexContactInformationForm != null) {
			htsPnsEncounterForms.add(htsSexContactInformationForm);
		}
		
		if (htsPartnerCharacteristicHtmlForm != null) {
			htsPnsEncounterForms.add(htsPartnerCharacteristicHtmlForm);
		}
		
		if (htsContactHtmlForm != null) {
			htsPnsEncounterForms.add(htsContactHtmlForm);
		}
		
		if (htsReferralsAndOutcomeHtmlForm != null) {
			htsPnsEncounterForms.add(htsReferralsAndOutcomeHtmlForm);
		}
		
		return htsPnsEncounterForms;
	}
	
	public static List<HtmlForm> getHivEncounterForms(Patient patient, HtmlFormEntryService htmlFormEntryService,
	        FormService formService) {
		HtmlForm prepForm = htmlFormEntryService.getHtmlFormByForm(formService.getFormByUuid(ART_PREP_FORM_UUID));
		HtmlForm pepForm = htmlFormEntryService.getHtmlFormByForm(formService.getFormByUuid(PEP_INITIATION_FORM_UUID));
		
		HtmlForm transferOutForm = htmlFormEntryService
		        .getHtmlFormByForm(formService.getFormByUuid(ART_TRANSFER_OUT_FORM_UUID));
		
		HtmlForm adherenceHtmlForm;
		Form adherenceForm;
		boolean patientIsAdult = patient.getAge() >= 14;
		
		if (patientIsAdult) {
			adherenceForm = formService.getFormByUuid(ADULT_ADHERENCE_ASSESSMENT_FORM_UUID);
		} else {
			adherenceForm = formService.getFormByUuid(PAEDS_ADHERENCE_ASSESSMENT_FORM_UUID);
		}
		adherenceHtmlForm = htmlFormEntryService.getHtmlFormByForm(adherenceForm);
		
		HtmlForm hivInitialHtmlForm;
		Form hivInitialForm;
		if (patientIsAdult) {
			hivInitialForm = formService.getFormByUuid(HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID);
		} else {
			hivInitialForm = formService.getFormByUuid(HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID);
		}
		hivInitialHtmlForm = htmlFormEntryService.getHtmlFormByForm(hivInitialForm);
		
		HtmlForm hivFollowUpHtmlForm;
		Form hivFollowupForm;
		if (patientIsAdult) {
			hivFollowupForm = formService.getFormByUuid(ADULT_FOLLOW_UP_CONSULTATION_FORM_UUID);
		} else {
			hivFollowupForm = formService.getFormByUuid(PAEDS_FOLLOW_UP_CONSULTATION_FORM_UUID);
		}
		hivFollowUpHtmlForm = htmlFormEntryService.getHtmlFormByForm(hivFollowupForm);
		
		//HtmlForm physicalExamHtmlForm;
		//Form physicalExamForm = formService.getFormByUuid(PHYSICAL_EXAM_FORM_UUID);
		//physicalExamHtmlForm = htmlFormEntryService.getHtmlFormByForm(physicalExamForm);
		
		List<HtmlForm> forms = new ArrayList<>(Arrays.asList(transferOutForm, hivInitialHtmlForm, hivFollowUpHtmlForm, //physicalExamHtmlForm,
		    adherenceHtmlForm));
		
		Concept patientHivStatus = getPatientHivStatus(patient);
		if (patientHivStatus == null || !patientHivStatus.getUuid().equals(HIV_STATUS_POSITIVE)) {
			forms.add(0, prepForm);
			forms.add(0, pepForm);
		}
		
		return forms;
	}
	
	public static List<HtmlForm> getVmmcEncounterForms(HtmlFormEntryService htmlFormEntryService, FormService formService) {
		Form vmmcInitialScreeningForm = formService.getFormByUuid(VMMC_INITIAL_SCREENING_FORM_UUID);
		Form vmmcOperationForm = formService.getFormByUuid(VMMC_OPERATION_FORM_UUID);
		Form vmmcPostOperationForm = formService.getFormByUuid(VMMC_POST_OPERATION_FORM_UUID);
		Form vmmcCounsellingForm = formService.getFormByUuid(VMMC_COUNSELLING_FORM_UUID);
		Form vmmcConsentForm = formService.getFormByUuid(VMMC_CONSENT_FORM_UUID);
		Form vmmcBookingForm = formService.getFormByUuid(VMMC_BOOKING_FORM_UUID);
		HtmlForm vmmcInitialScreeningHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcInitialScreeningForm);
		HtmlForm vmmcOperationHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcOperationForm);
		HtmlForm vmmcPostOperationHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcPostOperationForm);
		HtmlForm vmmcCounsellingHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcCounsellingForm);
		HtmlForm vmmcConsentHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcConsentForm);
		HtmlForm vmmcBookingHtmlForm = htmlFormEntryService.getHtmlFormByForm(vmmcBookingForm);
		
		List<HtmlForm> vmmcForms = new ArrayList<>();
		
		if (vmmcInitialScreeningHtmlForm != null) {
			vmmcForms.add(vmmcInitialScreeningHtmlForm);
		}
		if (vmmcOperationHtmlForm != null) {
			vmmcForms.add(vmmcOperationHtmlForm);
		}
		if (vmmcPostOperationHtmlForm != null) {
			vmmcForms.add(vmmcPostOperationHtmlForm);
		}
		if (vmmcCounsellingHtmlForm != null) {
			vmmcForms.add(vmmcCounsellingHtmlForm);
		}
		if (vmmcConsentHtmlForm != null) {
			vmmcForms.add(vmmcConsentHtmlForm);
		}
		if (vmmcBookingHtmlForm != null) {
			vmmcForms.add(vmmcBookingHtmlForm);
		}
		return vmmcForms;
	}
	
	public static List<HtmlForm> getSrhObstetricEncounterForms(HtmlFormEntryService htmlFormEntryService,
	        FormService formService, Concept srhPatientTypeObs) {
		Form vitalsForm = formService.getFormByUuid(VITALS_FORM_UUID);
		Form ancAntenantalEnrolmentForm = formService.getFormByUuid(ANC_ANTENATAL_ENROLMENT_FORM_UUID);
		Form ancAntenantalHistoryForm = formService.getFormByUuid(ANC_ANTENATAL_HISTORY_FORM_UUID);
		Form tetanusDiptheriaTrackingForm = formService.getFormByUuid(TETANUS_DIPTHERIA_TRACKING_FORM_UUID);
		Form ancNewRisksAfterBookingForm = formService.getFormByUuid(ANC_NEW_RISKS_AFTER_BOOKING_FORM_UUID);
		Form ancFoetalGrowthAndMaternityWeightForm = formService
		        .getFormByUuid(ANC_FOETAL_GROWTH_AND_MATERNITY_WEIGHT_FORM_UUID);
		Form labourDeliveryForm = formService.getFormByUuid(LABOUR_DELIVERY_FORM_UUID);
		Form partographObservationForm = formService.getFormByUuid(PARTOGRAPH_OBSERVATION_FORM_UUID);
		Form postPartumHomeVisitForm = formService.getFormByUuid(POST_PARTUM_HOME_VISIT_FORM_UUID);
		//  Partograph Graphs
		Form postPartumObservationForm = formService.getFormByUuid(POST_PARTUM_OBSERVATION_FORM_UUID);
		Form deliverySummaryForm = formService.getFormByUuid(DELIVERY_SUMMARY_FORM_UUID);
		Form puerperiumForm = formService.getFormByUuid(PUERPERIUM_FORM_UUID);
		Form srhDischargeSummaryForm = formService.getFormByUuid(SRH_DISCHARGE_SUMMARY_FORM_UUID);
		Form pregnancySummaryForm = formService.getFormByUuid(PREGNANCY_SUMMARY_FORM_UUID);
		Form admissionAndPregnancyTerminationForm = formService.getFormByUuid(ADMISSION_AND_PREGNANCY_TERMINATION_FORM_UUID);
		Form domiciliaryForm = formService.getFormByUuid(DOMICILIARY_FORM_UUID);
		Form postnatalPartographForm = formService.getFormByUuid(POSTNATAL_PARTOGRAPH_FORM_UUID);
		Form cohortRegistryForm = formService.getFormByUuid(COHORT_REGISTRY_FORM_UUID);
		Form srhEnrolmentForm = formService.getFormByUuid(SRH_ENROLMENT_FORM_UUID);
		
		HtmlForm ancAntenantalEnrolmentHtmlForm = htmlFormEntryService.getHtmlFormByForm(ancAntenantalEnrolmentForm);
		HtmlForm ancAntenantalHistoryHtmlForm = htmlFormEntryService.getHtmlFormByForm(ancAntenantalHistoryForm);
		HtmlForm tetanusDiptheriaTrackingHtmlForm = htmlFormEntryService.getHtmlFormByForm(tetanusDiptheriaTrackingForm);
		HtmlForm ancNewRisksAfterBookingHtmlForm = htmlFormEntryService.getHtmlFormByForm(ancNewRisksAfterBookingForm);
		HtmlForm ancFoetalGrowthAndMaternityWeightHtmlForm = htmlFormEntryService
		        .getHtmlFormByForm(ancFoetalGrowthAndMaternityWeightForm);
		HtmlForm labourDeliveryHtmlForm = htmlFormEntryService.getHtmlFormByForm(labourDeliveryForm);
		HtmlForm partographObservationHtmlForm = htmlFormEntryService.getHtmlFormByForm(partographObservationForm);
		HtmlForm postPartumHomeVisitHtmlForm = htmlFormEntryService.getHtmlFormByForm(postPartumHomeVisitForm);
		HtmlForm postPartumObservationHtmlForm = htmlFormEntryService.getHtmlFormByForm(postPartumObservationForm);
		HtmlForm deliverySummaryHtmlForm = htmlFormEntryService.getHtmlFormByForm(deliverySummaryForm);
		HtmlForm puerperiumHtmlForm = htmlFormEntryService.getHtmlFormByForm(puerperiumForm);
		HtmlForm srhDischargeSummaryHtmlForm = htmlFormEntryService.getHtmlFormByForm(srhDischargeSummaryForm);
		HtmlForm pregnancySummaryHtmlForm = htmlFormEntryService.getHtmlFormByForm(pregnancySummaryForm);
		HtmlForm admissionAndPregnancyTerminationHtmlForm = htmlFormEntryService
		        .getHtmlFormByForm(admissionAndPregnancyTerminationForm);
		HtmlForm domiciliaryHtmlForm = htmlFormEntryService.getHtmlFormByForm(domiciliaryForm);
		HtmlForm postnatalPartographHtmlForm = htmlFormEntryService.getHtmlFormByForm(postnatalPartographForm);
		HtmlForm cohortRegistryHtmlForm = htmlFormEntryService.getHtmlFormByForm(cohortRegistryForm);
		HtmlForm vitalsHtmlForm = htmlFormEntryService.getHtmlFormByForm(vitalsForm);
		HtmlForm srhEnrolmentHtmlForm = htmlFormEntryService.getHtmlFormByForm(srhEnrolmentForm);
		
		List<HtmlForm> srhObstetricForms = new ArrayList<>();
		if (BotswanaEmrConstants.ANC_BOOKER_CONCEPT_UUID.equals(srhPatientTypeObs.getUuid())) {
			addIfNotNull(srhObstetricForms, ancAntenantalEnrolmentHtmlForm);
			addIfNotNull(srhObstetricForms, ancAntenantalHistoryHtmlForm);
			addIfNotNull(srhObstetricForms, ancNewRisksAfterBookingHtmlForm);
			addIfNotNull(srhObstetricForms, ancFoetalGrowthAndMaternityWeightHtmlForm);
			addIfNotNull(srhObstetricForms, deliverySummaryHtmlForm);
		} else if (BotswanaEmrConstants.ANC_RECURRENT_CONCEPT_UUID.equals(srhPatientTypeObs.getUuid())) {
			addIfNotNull(srhObstetricForms, vitalsHtmlForm);
			addIfNotNull(srhObstetricForms, ancNewRisksAfterBookingHtmlForm);
			addIfNotNull(srhObstetricForms, ancFoetalGrowthAndMaternityWeightHtmlForm);
			addIfNotNull(srhObstetricForms, deliverySummaryHtmlForm);
		} else if (BotswanaEmrConstants.BBA_BOOKER_CONCEPT_UUID.equals(srhPatientTypeObs.getUuid())) {
			addIfNotNull(srhObstetricForms, deliverySummaryHtmlForm);
			addIfNotNull(srhObstetricForms, postPartumObservationHtmlForm);
			addIfNotNull(srhObstetricForms, puerperiumHtmlForm);
			addIfNotNull(srhObstetricForms, srhDischargeSummaryHtmlForm);
		} else if (BotswanaEmrConstants.NON_BBA_BOOKER_CONCEPT_UUID.equals(srhPatientTypeObs.getUuid())) {
			addIfNotNull(srhObstetricForms, labourDeliveryHtmlForm);
			addIfNotNull(srhObstetricForms, deliverySummaryHtmlForm);
			addIfNotNull(srhObstetricForms, postPartumObservationHtmlForm);
			addIfNotNull(srhObstetricForms, puerperiumHtmlForm);
			addIfNotNull(srhObstetricForms, srhDischargeSummaryHtmlForm);
		} else {
			// Add all forms
			addIfNotNull(srhObstetricForms, vitalsHtmlForm);
			addIfNotNull(srhObstetricForms, ancAntenantalEnrolmentHtmlForm);
			addIfNotNull(srhObstetricForms, ancAntenantalHistoryHtmlForm);
			addIfNotNull(srhObstetricForms, tetanusDiptheriaTrackingHtmlForm);
			addIfNotNull(srhObstetricForms, ancNewRisksAfterBookingHtmlForm);
			addIfNotNull(srhObstetricForms, ancFoetalGrowthAndMaternityWeightHtmlForm);
			addIfNotNull(srhObstetricForms, labourDeliveryHtmlForm);
			addIfNotNull(srhObstetricForms, partographObservationHtmlForm);
			addIfNotNull(srhObstetricForms, postPartumHomeVisitHtmlForm);
			addIfNotNull(srhObstetricForms, postPartumObservationHtmlForm);
			addIfNotNull(srhObstetricForms, deliverySummaryHtmlForm);
			addIfNotNull(srhObstetricForms, puerperiumHtmlForm);
			addIfNotNull(srhObstetricForms, srhDischargeSummaryHtmlForm);
			addIfNotNull(srhObstetricForms, pregnancySummaryHtmlForm);
			addIfNotNull(srhObstetricForms, admissionAndPregnancyTerminationHtmlForm);
			addIfNotNull(srhObstetricForms, domiciliaryHtmlForm);
			addIfNotNull(srhObstetricForms, postnatalPartographHtmlForm);
			addIfNotNull(srhObstetricForms, cohortRegistryHtmlForm);
			addIfNotNull(srhObstetricForms, srhEnrolmentHtmlForm);
		}
		
		return srhObstetricForms;
	}
	
	private static void addIfNotNull(List<HtmlForm> formList, HtmlForm form) {
		if (form != null) {
			formList.add(form);
		}
	}
	
	public static List<Concept> getStrengthUnits() {
		// TODO: Replace Context.getOrderService().getDrugDosingUnits() with a more specific method that returns only strength units relevant to Botswana
		// Add sachet to the list of strength units
		List<Concept> strengthUnits = new ArrayList<>(Context.getOrderService().getDrugDosingUnits());
		Concept sachetConcept = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.SACHET_CONCEPT_UUID);
		if (sachetConcept != null) {
			strengthUnits.add(sachetConcept);
		}
		return strengthUnits;
	}
	
	public static List<ConceptAnswer> getConceptAnswers(Concept concept) {
		List<ConceptAnswer> answerConcepts = null;
		if (concept != null) {
			answerConcepts = new ArrayList<>(concept.getAnswers());
			
			Collections.sort(answerConcepts, new Comparator<ConceptAnswer>() {
				
				public int compare(ConceptAnswer o1, ConceptAnswer o2) {
					Object obj1 = o1.getAnswerConcept().getName().getName();
					
					Object obj2 = o2.getAnswerConcept().getName().getName();
					
					if (o1.getAnswerConcept().getName().getName().equals(o2.getAnswerConcept().getName().getName()))
						return 0;
					return obj1.toString().compareToIgnoreCase(obj2.toString());
				}
			});
		}
		return answerConcepts;
	}
	
	public static List<Encounter> getPrescriptionEncounters(List<Patient> patientList, Date startDate, Date endDate,
	        Location facility) {
		
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, facility, startDate, endDate,
		        null, null, Collections.singletonList(getEncounterType(DRUG_ORDER_ENCOUNTER_TYPE)), null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		List<Encounter> resultantEncounter = encounters.stream().filter(e -> patientList.contains(e.getPatient()))
		        .collect(Collectors.toList());
		
		return resultantEncounter;
	}
	
	public static List<Encounter> getPrescriptionEncounters(Patient patient, Date startDate, Date endDate,
	        Location facility) {
		
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, facility, startDate, endDate,
		        null, null, Collections.singletonList(getEncounterType(DRUG_ORDER_ENCOUNTER_TYPE)), null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		return encounters;
	}
	
	public static boolean isRegistrationClerk(User user) {
		boolean isRegistrationClerkOnly = true;
		
		if (user.isSuperUser()) {
			isRegistrationClerkOnly = false;
		} else {
			List<String> roles = Arrays.asList(ROLE_DOCTOR, ROLE_NURSE, ROLE_HOSPITAL_ADMIN, ROLE_SYSTEM_ADMIN);
			
			for (Role role : user.getAllRoles()) {
				if (roles.contains(role.getName())) {
					isRegistrationClerkOnly = false;
					break;
				}
			}
		}
		return isRegistrationClerkOnly;
	}
	
	public static List<Encounter> getVmmcEncounters(List<PatientResponse> patientList, Date startDate, Date endDate,
	        Location facility) {
		EncounterType vmmcEncounterType = getEncounterType(VMMC_OPERATION_ENCOUNTER_TYPE);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, facility, startDate, endDate,
		        null, null, Collections.singletonList(vmmcEncounterType), null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		List<Encounter> vmmcEncounter = encounters.stream().filter(e -> {
			return !patientList.isEmpty()
			        && patientList.stream().anyMatch(pl -> pl.getPatientId() == e.getPatient().getId());
		}).collect(Collectors.toList());
		
		return vmmcEncounter;
	}
	
	public static List<Encounter> getHtsEncounters(List<Patient> patientList, Date startDate, Date endDate,
	        Location facility) {
		//EncounterType htsEncounterType = getEncounterType("b0754e64-ea6c-11ec-8fea-0242ac120002");
		EncounterType htsDailytEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID);
		List<EncounterType> htsEncounterTypes = new ArrayList<>();
		htsEncounterTypes.add(htsDailytEncounterType);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, facility, startDate, endDate,
		        null, null, htsEncounterTypes, null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		List<Encounter> htsEncounter = encounters.stream().filter(e -> {
			return !patientList.isEmpty() && patientList.contains(e.getPatient());
		}).collect(Collectors.toList());
		
		return htsEncounter;
	}
	
	public static List<Encounter> getAllHtsPatients(Date start, Date end, Integer limit, Location location) {
		//To be replace with the correct uuids
		EncounterType htsDailyEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID);
		List<EncounterType> htsEncounterTypes = new ArrayList<>();
		htsEncounterTypes.add(htsDailyEncounterType);
		
		List<Encounter> htsEncounters = Context.getService(BotswanaEmrService.class).getEncountersList(start, end,
		    htsEncounterTypes, limit, location, null);
		if (htsEncounters != null && !htsEncounters.isEmpty()) {
			htsEncounters = htsEncounters.stream().filter(r -> r.getPatient().getAttribute("LocationAttribute") != null)
			        .filter(r -> r.getPatient().getAttribute("LocationAttribute").getValue().equals(location.getUuid()))
			        .collect(Collectors.toList());
		}
		return htsEncounters;
	}
	
	public static List<Encounter> getVmmcPostEncounters(List<Patient> patientList, Date startDate, Date endDate,
	        Location facility) {
		EncounterType vmmcEncounterType = getEncounterType(VMMC_POST_OPERATION_ENCOUNTER_TYPE);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, facility, startDate, endDate,
		        null, null, Collections.singletonList(vmmcEncounterType), null, null, null, false);
		
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		List<Encounter> vmmcPostEncounter = new ArrayList<>();
		
		if (!patientList.isEmpty()) {
			for (Patient patient : patientList) {
				for (Encounter encounter : encounters) {
					if (patient.equals(encounter.getPatient())) {
						vmmcPostEncounter.add(encounter);
					}
				}
			}
		}
		return vmmcPostEncounter;
	}
	
	public static List<Encounter> getVmmcPostEncounters(Patient patient, Date startDate, Date endDate, Location facility) {
		EncounterType vmmcEncounterType = getEncounterType(VMMC_POST_OPERATION_ENCOUNTER_TYPE);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, facility, startDate, endDate,
		        null, null, Collections.singletonList(vmmcEncounterType), null, null, null, false);
		
		return Context.getEncounterService().getEncounters(encounterSearchCriteria);
	}
	
	public static PatientProgram createPatientProgram(Patient patient, String programUuid) {
		PatientProgram program = new PatientProgram();
		program.setPatient(patient);
		program.setProgram(Context.getProgramWorkflowService().getProgramByUuid(programUuid));
		program.setDateEnrolled(new Date());
		return program;
	}
	
	public static Set<Patient> getPatientsPrescriptionEncounters(Date startDate, Date endDate, Location facility) {
		Set<Patient> uniquePatients = new HashSet<>();
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, facility, startDate, endDate,
		        null, null, Collections.singletonList(getEncounterType(DRUG_ORDER_ENCOUNTER_TYPE)), null, null, null, false);
		List<Encounter> encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		if (!encounters.isEmpty()) {
			uniquePatients = encounters.stream().map(Encounter::getPatient).collect(Collectors.toSet());
		}
		return uniquePatients;
	}
	
	public static EnumMap<EducationLevel, ConceptAnswer> getRequiredEducationLevels() {
		Concept educationLevelConcept = getConcept(EDUCATION_LEVEL_CONCEPT_UUID);
		List<ConceptAnswer> answerConcepts = getConceptAnswers(educationLevelConcept);
		Predicate<ConceptAnswer> prePrimaryEducation = getEducationLevel("Preschool education level");
		Predicate<ConceptAnswer> primaryEducation = getEducationLevel("Primary school education");
		Predicate<ConceptAnswer> secondaryEducation = getEducationLevel("Secondary school education");
		Predicate<ConceptAnswer> tertiaryEducation = getEducationLevel("Tertiary education complete");
		Predicate<ConceptAnswer> nonFormalEducation = getEducationLevel("No formal education");
		Predicate<ConceptAnswer> uneducated = getEducationLevel("None");
		Predicate<ConceptAnswer> educationLevelPredicate = prePrimaryEducation.or(primaryEducation).or(secondaryEducation)
		        .or(tertiaryEducation).or(nonFormalEducation).or(uneducated);
		Set<ConceptAnswer> conceptAnswers = answerConcepts.stream().filter(educationLevelPredicate)
		        .collect(Collectors.toSet());
		EnumMap<EducationLevel, ConceptAnswer> educationLevelConceptAnswers = new EnumMap<>(EducationLevel.class);
		educationLevelConceptAnswers.put(EducationLevel.PRE_PRIMARY, getAnswerConcept(conceptAnswers, prePrimaryEducation));
		educationLevelConceptAnswers.put(EducationLevel.PRIMARY_EDUCATION,
		    getAnswerConcept(conceptAnswers, primaryEducation));
		educationLevelConceptAnswers.put(EducationLevel.SECONDARY_EDUCATION,
		    getAnswerConcept(conceptAnswers, secondaryEducation));
		educationLevelConceptAnswers.put(EducationLevel.TERTIARY_EDUCATION,
		    getAnswerConcept(conceptAnswers, tertiaryEducation));
		educationLevelConceptAnswers.put(EducationLevel.NON_FORMAL_EDUCATION,
		    getAnswerConcept(conceptAnswers, nonFormalEducation));
		educationLevelConceptAnswers.put(EducationLevel.UNEDUCATED, getAnswerConcept(conceptAnswers, uneducated));
		return educationLevelConceptAnswers;
	}
	
	private static Predicate<ConceptAnswer> getEducationLevel(String level) {
		return conceptAnswer -> conceptAnswer.getAnswerConcept().getName().getName().equals(level);
	}
	
	public static ConceptAnswer getAnswerConcept(Set<ConceptAnswer> conceptAnswers,
	        Predicate<ConceptAnswer> educationLevelPredicate) {
		ConceptAnswer conceptAnswer = new ConceptAnswer();
		for (ConceptAnswer concept : conceptAnswers) {
			if (educationLevelPredicate.test(concept)) {
				conceptAnswer = concept;
			}
		}
		return conceptAnswer;
	}
	
	public static List<Obs> getTbQuestionsObservations(List<Person> persons) {
		ObsService obsService = Context.getObsService();
		return obsService.getObservations(persons, null, Programs.tbScreeningQuestions(), null, null, null, null, null, null,
		    null, null, false);
	}
	
	public static List<Obs> getTbQuestionsObservations(List<Person> persons, Visit visit) {
		ObsService obsService = Context.getObsService();
		return obsService.getObservations(persons, visit.getNonVoidedEncounters(), Programs.tbScreeningQuestions(), null,
		    null, null, null, null, null, null, null, false);
	}
	
	public static Boolean isConsultationActive(Patient patient) {
		return activeConsultationQueue(patient) != null || isClosedVisitLegibleForEdit(patient);
	}
	
	private static Boolean isClosedVisitLegibleForEdit(Patient patient) {
		String numHoursAllowed = Context.getAdministrationService()
		        .getGlobalProperty("numberOfHoursToAllowEditAfterVisitClose", "24");
		VisitType VisitType = Context.getVisitService().getVisitTypeByUuid(FACILITY_VISIT_VISIT_TYPE_UUID);
		Visit lastPatientVisit = getLastPatientVisit(patient, VisitType);
		return lastPatientVisit != null && DateUtils.getHoursInBetween(new Date(),
		    lastPatientVisit.getStopDatetime()) < Integer.parseInt(numHoursAllowed);
	}
	
	public static PatientQueue activeConsultationQueue(Patient patient) {
		Location consultationServiceDeliveryPoint = Context.getLocationService().getLocationByUuid(DOCTORS_PORTAL_UUID);
		PatientIdentifier omangIdentifier = patient.getPatientIdentifier(OMANG_IDENTIFIER_NAME);
		String search_identifier_value = omangIdentifier == null ? null : omangIdentifier.getIdentifier();
		if (search_identifier_value == null) {
			PatientIdentifier bcnIdentifier = patient.getPatientIdentifier(BIRTH_CERT_IDENTIFIER_NAME);
			search_identifier_value = bcnIdentifier == null ? null : bcnIdentifier.getIdentifier();
			if (search_identifier_value == null) {
				PatientIdentifier ppnIdentifier = patient.getPatientIdentifier(PASSPORT_IDENTIFIER_NAME);
				search_identifier_value = ppnIdentifier == null ? null : ppnIdentifier.getIdentifier();
				if (search_identifier_value == null) {
					PatientIdentifier tempIdentifier = patient.getPatientIdentifier(TEMPORARY_ID_IDENTIFIER_NAME);
					search_identifier_value = tempIdentifier == null ? null : tempIdentifier.getIdentifier();
					if (search_identifier_value == null) {
						search_identifier_value = patient.getPatientIdentifier(OPENMRS_ID_IDENTIFIER_NAME).getIdentifier();
					}
				}
			}
		}
		List<PatientQueue> patientQueueList = Context.getService(PatientQueueingService.class)
		        .getPatientQueueListBySearchParams(search_identifier_value, null, null, consultationServiceDeliveryPoint,
		            null, null);
		
		return patientQueueList.stream().filter(q -> !q.getStatus().equals(PatientQueue.Status.COMPLETED))
		        .max(Comparator.comparing(BaseOpenmrsData::getDateCreated)).orElse(null);
	}
	
	public static List<SimpleObject> getSimplePastOperationsObjects(Set<Obs> groupingObs) {
		List<SimpleObject> pastOperationsObjects = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(PAST_OPERATION_NOTES_CONCEPT)) {
					simpleObject.put("comment", groupMember);
				} else if (groupMember.getConcept().getUuid().equals(PAST_OPERATION_YEAR_CONCEPT)) {
					simpleObject.put("year", groupMember);
				} else {
					simpleObject.put("pastOperation", groupMember);
				}
			}
			if (!simpleObject.isEmpty()) {
				pastOperationsObjects.add(simpleObject);
			}
		}
		return pastOperationsObjects;
	}
	
	public static Set<Obs> getPastOperationsObs(Visit visit) {
		Set<Obs> groupingObs = new HashSet<>();
		for (Encounter encounter : visit.getNonVoidedEncounters()) {
			if (encounter.getEncounterType().getUuid().equals(TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID)) {
				List<Obs> operationsGroupingObs = encounter.getAllObs(false).stream()
				        .filter(e -> e.getConcept().getUuid().equals(PAST_OPERATIONS_GROUPING_CONCEPT))
				        .collect(Collectors.toList());
				groupingObs.addAll(operationsGroupingObs);
			}
		}
		return groupingObs;
	}
	
	public static List<Encounter> getAllConsultations(Date start, Date end, Integer limit, Location location) {
		return getAllConsultations(start, end, limit, location, null);
	}
	
	public static List<Encounter> getAllConsultations(Date start, Date end, Integer limit, Location location,
	        PagingInfo pagingInfo) {
		EncounterType newDoctorsConsultationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID);
		EncounterType existingConsultationEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID);
		List<EncounterType> consultationEncounterTypes = new ArrayList<>();
		consultationEncounterTypes.add(newDoctorsConsultationEncounterType);
		consultationEncounterTypes.add(existingConsultationEncounterType);
		
		return Context.getService(BotswanaEmrService.class).getEncountersList(start, end, consultationEncounterTypes, limit,
		    location, null, pagingInfo);
	}
	
	public static List<Concept> getAdministrationRouteConcepts() {
		String drugRoutesConceptUuid = Context.getAdministrationService()
		        .getGlobalProperty(GLOBAL_PROPERTY_ORDER_DRUG_ROUTES_CONCEPT_UUID);
		if (drugRoutesConceptUuid == null) {
			drugRoutesConceptUuid = ADMINISTRATION_ROUTES_CONCEPT_UUID;
		}
		
		List<Concept> administrationRouteConcepts = new ArrayList<>(
		        Context.getConceptService().getConceptByUuid(drugRoutesConceptUuid).getSetMembers());
		
		if (administrationRouteConcepts.isEmpty()) {
			administrationRouteConcepts = Arrays.asList(
			    Context.getConceptService().getConceptByUuid(ORAL_ROUTE_CONCEPT_UUID),
			    Context.getConceptService().getConceptByUuid(INJECTABLE_ROUTE_CONCEPT_UUID));
		}
		
		Concept topicalRoute = getConcept(TOPICAL_ROUTE_CONCEPT_UUID);
		if (topicalRoute != null) {
			administrationRouteConcepts.add(topicalRoute);
		}
		
		return administrationRouteConcepts;
	}
	
	public static void getVisitSummaryInformation(Patient patient, PageModel model, Visit visit) {
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		EncounterType labOrderEncounterType = encounterService.getEncounterTypeByUuid(LAB_ORDER_ENCOUNTER_TYPE_UUID);
		OrderType drugOrderType = orderService.getOrderTypeByUuid(DRUG_ORDER_TYPE_UUID);
		OrderType referralOrderType = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		Comparator<Order> orderDateComparator = Comparator.comparing(Order::getDateCreated);
		
		List<Encounter> patientEncounters = visit.getNonVoidedEncounters().stream()
		        .filter(e -> e.getEncounterDatetime() != null).collect(Collectors.toList());
		
		List<DrugOrder> patientDrugOrders = new ArrayList<>();
		List<SimplifiedLabTest> labTests = new ArrayList<>();
		
		if (!patientEncounters.isEmpty()) {
			patientEncounters.sort(Comparator.comparing(BaseOpenmrsData::getDateCreated));
			for (Encounter patientEncounter : patientEncounters) {
				Set<Order> drugOrders = patientEncounter.getOrders().stream()
				        .filter(order -> order.getPatient().equals(patient))
				        .filter(order -> order.getOrderType().equals(drugOrderType))
				        .filter(order -> order.getFulfillerStatus() != Order.FulfillerStatus.COMPLETED)
				        .collect(Collectors.toSet());
				DrugOrder drugOrder;
				for (Order order : drugOrders) {
					if (order instanceof DrugOrder) {
						drugOrder = (DrugOrder) order;
						patientDrugOrders.add(drugOrder);
					}
				}
				
				Set<Encounter> labEncounters = patientEncounters.stream()
				        .filter(enc -> enc.getEncounterType().equals(labOrderEncounterType)).collect(Collectors.toSet());
				for (Encounter labEncounter : labEncounters) {
					labTests = simplifiedLabTestList(labService.getLaboratoryTestListByEncounter(labEncounter), labService);
				}
			}
		}
		
		List<Condition> conditions = Context.getConditionService().getActiveConditions(patient);
		if (!conditions.isEmpty()) {
			model.addAttribute("conditions", conditions);
		} else {
			model.addAttribute("conditions", Collections.EMPTY_LIST);
		}
		
		Optional<Order> referralOrder = orderService.getAllOrdersByPatient(patient).stream()
		        .filter(order -> order.getOrderType().equals(referralOrderType))
		        .filter(order -> order.getEncounter().getVisit().equals(visit)).filter(Order::isActive)
		        .max(orderDateComparator);
		
		if (referralOrder.isPresent()) {
			Optional<Obs> receivingDepartmentObs = getCurrentObs(obsDateComparator, referralOrder.get(),
			    RECEIVING_DEPARTMENT_CONCEPT_UUID);
			receivingDepartmentObs.ifPresent(obs -> model.addAttribute("receivingDepartment", obs));
			model.addAttribute("referralOrder", referralOrder.get());
		} else {
			model.addAttribute("referralOrder", "");
		}
		
		model.addAttribute("artServicesUuid", ART_SERVICES_PORTAL_UUID);
		model.addAttribute("pharmacyPortalUuid", PHARMACY_PORTAL_UUID);
		model.addAttribute("registrationPageUuid", PATIENT_REGISTRATION_PORTAL_UUID);
		model.addAttribute("doctorsPortalUuid", DOCTORS_PORTAL_UUID);
		model.addAttribute("nursePortalUuid", NURSING_PORTAL_UUID);
		model.addAttribute("vmmcPortalUuid", VMMC_PORTAL_UUID);
		model.addAttribute("sexualReproductiveHealthPortalUuid", SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID);
		model.addAttribute("patientNames", formatPersonName(patient.getPersonName()));
		model.addAttribute("patientAge", patient.getAge());
		model.addAttribute("patientGender", patient.getGender());
		
		if (!patientDrugOrders.isEmpty()) {
			model.addAttribute("patientPrescriptions", patientDrugOrders);
		} else {
			model.addAttribute("patientPrescriptions", Collections.EMPTY_LIST);
		}
		
		if (!labTests.isEmpty()) {
			model.addAttribute("labTests", labTests);
		} else {
			model.addAttribute("labTests", Collections.EMPTY_LIST);
		}
		
		List<Program> patientActivePrograms = new ArrayList<>();
		List<PatientProgram> patientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, null, null,
		    null, null, null, false);
		for (PatientProgram patientProgram : patientPrograms) {
			if (patientProgram.getDateCompleted() == null) {
				patientActivePrograms.add(patientProgram.getProgram());
			}
		}
		
		if (!patientActivePrograms.isEmpty()) {
			model.addAttribute("patientActivePrograms", patientActivePrograms);
		} else {
			model.addAttribute("patientActivePrograms", Collections.EMPTY_LIST);
		}
		
		Comparator<Appointment> appointmentComparator = Comparator.comparing(Appointment::getDateCreated);
		Appointment nextAppointment = appointmentService.getAppointmentsOfPatient(patient).stream()
		        .max(appointmentComparator).orElse(null);
		if (nextAppointment != null) {
			model.addAttribute("nextAppointmentStartDate", nextAppointment.getTimeSlot().getStartDate());
		} else {
			model.addAttribute("nextAppointmentStartDate", "");
		}
		
		List<EncounterType> consultationEncounterTypes = getConsultationEncounterTypes();
		List<Diagnosis> diagnoses = diagnosisService.getDiagnosesByVisit(visit, false, false).stream()
		        .filter(d -> d.getVoided().equals(false))
		        .filter(d -> !consultationEncounterTypes.contains(d.getEncounter().getEncounterType()))
		        .collect(Collectors.toList());
		model.addAttribute("hasDiagnoses", diagnoses.size() > 0);
		model.addAttribute("provisionalDiagnoses",
		    diagnoses.stream().filter(d -> d.getCertainty().equals(PROVISIONAL)).collect(Collectors.toList()));
		model.addAttribute("confirmedDiagnoses", diagnoses.stream()
		        .filter(d -> d.getCertainty().equals(ConditionVerificationStatus.CONFIRMED)).collect(Collectors.toList()));
		
		Set<Obs> doctorsNotes = new HashSet<>();
		Comparator<Obs> obsDatetimeComparator = Comparator.comparing(Obs::getObsDatetime);
		
		List<Obs> objectiveNotes = getObservations(patient, getConcept(OBJECTIVE_ADDITIONAL_NOTES_CONCEPT_UUID), visit)
		        .stream().filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(objectiveNotes)) {
			doctorsNotes.addAll(objectiveNotes);
		}
		
		List<Obs> assessmentNotes = getObservations(patient, getConcept(ASSESSMENT_ADDITIONAL_NOTES_CONCEPT_UUID), visit)
		        .stream().filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(assessmentNotes)) {
			doctorsNotes.addAll(assessmentNotes);
		}
		
		if (!doctorsNotes.isEmpty()) {
			model.addAttribute("doctorsNotes", doctorsNotes);
		} else {
			model.addAttribute("doctorsNotes", Collections.EMPTY_LIST);
		}
		
		List<Obs> nursingDiagnosis = getObservations(patient, getConcept(NURSING_DIAGNOSIS_CONCEPT_UUID), visit).stream()
		        .filter(obs -> obs.getEncounter().getVisit().equals(visit)).collect(Collectors.toList());
		
		model.addAttribute("nursingDiagnosis", nursingDiagnosis);
		
		Set<Obs> symptomsObsGroupings = new HashSet<>();
		List<Obs> observations = getObservations(patient, getConcept(SYMPTOMS_GROUPING_CONCEPT_UUID), visit);
		List<Obs> symptomsObsGrouping = observations != null ? observations.stream()
		        .filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList()) : new ArrayList<>();
		if (!symptomsObsGrouping.isEmpty()) {
			symptomsObsGroupings.addAll(symptomsObsGrouping);
		}
		
		List<SimpleObject> symptoms = getSimplifiedSymptomObjects(symptomsObsGroupings);
		if (!symptoms.isEmpty()) {
			model.addAttribute("symptoms", symptoms);
		} else {
			model.addAttribute("symptoms", Collections.EMPTY_LIST);
		}
		
		Set<Obs> lifeStyleObsGroupings = new HashSet<>();
		List<Obs> observations1 = getObservations(patient, getConcept(LIFESTYLE_GROUPING_CONCEPT_UUID), visit);
		List<Obs> lifeStyleObsGrouping = observations1 != null ? observations1.stream()
		        .filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList()) : new ArrayList<>();
		if (!lifeStyleObsGrouping.isEmpty()) {
			lifeStyleObsGroupings.addAll(lifeStyleObsGrouping);
		}
		
		List<SimpleObject> lifeStyles = getSimplifiedLifeStyleObjects(lifeStyleObsGroupings);
		if (!lifeStyles.isEmpty()) {
			model.addAttribute("lifeStyles", lifeStyles);
		} else {
			model.addAttribute("lifeStyles", Collections.EMPTY_LIST);
		}
		
		Integer limitFetch = Integer.valueOf(Context.getAdministrationService().getGlobalProperty("botswanaemr.fetchSize"));
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		List<Obs> complaintGroupingObs = botswanaEmrService.getObservation(patient,
		    encounterService.getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PATIENT_COMPLAINT_GROUPING_CONCEPT_UUID), visit.getLocation(),
		    limitFetch);
		Set<Obs> complaintGroupingObsSet = new HashSet<Obs>(complaintGroupingObs);
		List<SimpleObject> complaintObjects = getSimplifiedComplaintsObjects(complaintGroupingObsSet);
		model.addAttribute("complaints", complaintObjects);
		
		Set<Obs> dietObsGroupings = new HashSet<>();
		List<Obs> observations2 = getObservations(patient, getConcept(DIET_GROUPING_CONCEPT), visit);
		List<Obs> dietObsGrouping = observations2 != null ? observations2.stream()
		        .filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList()) : new ArrayList<>(
		
		);
		if (!dietObsGrouping.isEmpty()) {
			dietObsGroupings.addAll(dietObsGrouping);
		}
		
		List<SimpleObject> diets = getSimplifiedDietObjects(dietObsGroupings);
		if (!diets.isEmpty()) {
			model.addAttribute("diets", diets);
		} else {
			model.addAttribute("diets", Collections.EMPTY_LIST);
		}
		
		List<Obs> physicalExamGroupingObs = botswanaEmrService.getObservation(patient.getPerson(), visit,
		    BotswanaEmrUtils.getConcept(BotswanaEmrConstants.PHYSICAL_EXAM_GROUPING_CONCEPT), visit.getLocation(),
		    limitFetch);
		List<SimpleObject> physicalExamObs = new ArrayList<>();
		for (Obs obs : physicalExamGroupingObs) {
			physicalExamObs.add(BotswanaEmrUtils.getPhysicalExamObs(obs));
		}
		model.addAttribute("physicalExaminations", physicalExamObs);
		
		List<Obs> observations4 = getObservations(patient, getConcept(DIETARY_PLAN_CONCEPT_UUID), visit);
		List<Obs> dietaryPlans = observations4 != null ? observations4.stream()
		        .filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList()) : new ArrayList<>();
		Optional<Obs> dietaryPlan = Optional.empty();
		if (CollectionUtils.isNotEmpty(dietaryPlans)) {
			dietaryPlan = dietaryPlans.stream().max(obsDatetimeComparator);
		}
		if (dietaryPlan.isPresent()) {
			model.addAttribute("dietaryPlan", dietaryPlan.get());
		} else {
			model.addAttribute("dietaryPlan", "");
		}
		
		List<Obs> observations5 = getObservations(patient, getConcept(NON_PHARMA_PLAN_CONCEPT_UUID), visit);
		List<Obs> nonPharmacologicalPrescriptions = observations5 != null ? observations5.stream()
		        .filter(obs -> obs.getEncounter() != null && obs.getEncounter().getVisit().equals(visit))
		        .collect(Collectors.toList()) : new ArrayList<>();
		Optional<Obs> nonPharmacologicalPrescription = Optional.empty();
		if (CollectionUtils.isNotEmpty(nonPharmacologicalPrescriptions)) {
			nonPharmacologicalPrescription = nonPharmacologicalPrescriptions.stream().max(obsDatetimeComparator);
		}
		if (nonPharmacologicalPrescription.isPresent()) {
			model.addAttribute("nonPharmacologicalPrescription", nonPharmacologicalPrescription.get());
		} else {
			model.addAttribute("nonPharmacologicalPrescription", "");
		}
		
		Set<Obs> groupingObs = getPastOperationsObs(visit);
		List<SimpleObject> pastOperationsObjects = getSimplePastOperationsObjects(groupingObs);
		if (!pastOperationsObjects.isEmpty()) {
			model.addAttribute("pastOperations", pastOperationsObjects);
		} else {
			model.addAttribute("pastOperations", Collections.EMPTY_LIST);
		}
	}
	
	public static SimpleObject getPhysicalExamObs(Obs groupObs) {
		SimpleObject simpleObject = new SimpleObject();
		for (Obs obs : groupObs.getGroupMembers(false)) {
			switch (obs.getConcept().getUuid()) {
				
				case BotswanaEmrConstants.PHYSICAL_EXAMINATION_CONCEPT_UUID:
					simpleObject.put("general", obs);
					break;
				case BotswanaEmrConstants.HEENT_CONCEPT_UUID:
					simpleObject.put("heent", obs);
					break;
				case BotswanaEmrConstants.CHEST_EXAMINATION_CONCEPT_UUID:
					simpleObject.put("respiratory", obs);
					break;
				case BotswanaEmrConstants.CARDIOVASCULAR_CONCEPT_UUID:
					simpleObject.put("cardiovascular", obs);
					break;
				case BotswanaEmrConstants.SKIN_CONCEPT_UUID:
					simpleObject.put("skin", obs);
					break;
				case BotswanaEmrConstants.ENDOCRINE_CONCEPT_UUID:
					simpleObject.put("endocrine", obs);
					break;
				case BotswanaEmrConstants.GI_CONCEPT_UUID:
					simpleObject.put("gi", obs);
					break;
				case BotswanaEmrConstants.GU_WOMEN_HEALTH_CONCEPT_UUID:
					simpleObject.put("womenHealth", obs);
					break;
				case BotswanaEmrConstants.CNS_CONCEPT_UUID:
					simpleObject.put("cns", obs);
					break;
				case BotswanaEmrConstants.GROWTH_DEVELOPMENT_CONCEPT_UUID:
					simpleObject.put("growth", obs);
					break;
				case BotswanaEmrConstants.URO_GENITAL_CONCEPT_ID:
					simpleObject.put("uroGenital", obs);
					break;
				case BotswanaEmrConstants.RADIOLOGY_CONCEPT_UUID:
					simpleObject.put("radiology", obs);
					break;
				case BotswanaEmrConstants.ECG_CONCEPT_UUID:
					simpleObject.put("ecg", obs);
					break;
				case BotswanaEmrConstants.ULTRASOUND_CONCEPT_UUID:
					simpleObject.put("ultrasound", obs);
					break;
				default:
					break;
			}
		}
		simpleObject.put("groupingObsId", groupObs.getObsId());
		return simpleObject;
	}
	
	public static List<Obs> getObservations(Patient patient, Concept concept, Visit visit) {
		if (visit != null) {
			return obsService.getObservations(Collections.singletonList(patient), null, Collections.singletonList(concept),
			    null, null, null, Collections.singletonList("id desc"), null, null, visit.getStartDatetime(), null, false,
			    null);
		} else {
			return obsService.getObservations(Collections.singletonList(patient), null, Collections.singletonList(concept),
			    null, null, null, Collections.singletonList("id desc"), null, null, null, null, false, null);
		}
	}
	
	public static List<Obs> getObservations(Patient patient, Concept concept) {
		return getObservations(patient, concept, null);
	}
	
	public static PersonAddress getPersonAddress(Set<PersonAddress> personAddresses) {
		return personAddresses.stream().findFirst().orElse(null);
	}
	
	public static List<SimplifiedLabTest> simplifiedLabTestList(List<LabTest> labTests, LabService labService) {
		List<SimplifiedLabTest> allTests = new ArrayList<>();
		SimplifiedLabTest simplifiedLabTest;
		if (labTests != null && labTests.size() > 0) {
			for (LabTest labTest : labTests) {
				simplifiedLabTest = new SimplifiedLabTest();
				simplifiedLabTest.setTestName(labTest.getConcept().getDisplayString());
				simplifiedLabTest.setStatus(labTest.getStatus());
				simplifiedLabTest.setDateAdded(formatDateWithTime(labTest.getAcceptDate(), "dd/MM/yyyy HH:mm"));
				simplifiedLabTest.setResultsList(
				    getLabResultsKeyMap(getLabOrderResultsIfCompleteSet(labTest.getLabTestId(), labService)));
				allTests.add(simplifiedLabTest);
			}
		}
		return allTests;
	}
	
	public static List<SimpleObject> getSimplifiedSymptomObjects(Set<Obs> groupingObs) {
		List<SimpleObject> symptomObjects = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(PATIENT_NOTE_CONCEPT)) {
					simpleObject.put("comment", groupMember);
				} else if (groupMember.getConcept().getUuid().equals(SYMPTOM_DURATION_CONCEPT)) {
					simpleObject.put("duration", groupMember);
				} else if (groupMember.getConcept().getUuid().equals(DURATION_UNITS_CONCEPT)) {
					simpleObject.put("durationUnit", groupMember);
				} else {
					simpleObject.put("symptom", groupMember);
				}
			}
			if (!simpleObject.isEmpty()) {
				symptomObjects.add(simpleObject);
			}
		}
		return symptomObjects;
	}
	
	public static List<SimpleObject> getSimplifiedLifeStyleObjects(Set<Obs> groupingObs) {
		List<SimpleObject> lifestyleObjects = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(PATIENT_NOTE_CONCEPT)) {
					simpleObject.put("comment", groupMember);
				} else {
					simpleObject.put("lifestyle", groupMember);
				}
			}
			if (!simpleObject.isEmpty()) {
				lifestyleObjects.add(simpleObject);
			}
		}
		return lifestyleObjects;
	}
	
	public static List<SimpleObject> getSimplifiedComplaintsObjects(Set<Obs> groupingObs) {
		List<SimpleObject> lifestyleObjects = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(PATIENT_NOTE_CONCEPT)) {
					simpleObject.put("comment", groupMember);
				} else {
					simpleObject.put("complaint", groupMember);
				}
			}
			if (!simpleObject.isEmpty()) {
				lifestyleObjects.add(simpleObject);
			}
		}
		return lifestyleObjects;
	}
	
	public static List<SimpleObject> getSimplifiedDietObjects(Set<Obs> groupingObs) {
		List<SimpleObject> dietObjects = new ArrayList<>();
		for (Obs obs : groupingObs) {
			SimpleObject simpleObject = new SimpleObject();
			for (Obs groupMember : obs.getGroupMembers()) {
				if (groupMember.getConcept().getUuid().equals(DIET_NOTES_CONCEPT)) {
					simpleObject.put("comment", groupMember);
				} else if (groupMember.getConcept().getUuid().equals(DIET_AMOUNT_CONCEPT)) {
					simpleObject.put("amount", groupMember);
				} else {
					simpleObject.put("diet", groupMember);
				}
			}
			if (!simpleObject.isEmpty()) {
				dietObjects.add(simpleObject);
			}
		}
		return dietObjects;
	}
	
	public static OrderSearchCriteria getOrderSearchCriteria(OrderType orderType) {
		OrderSearchCriteriaBuilder orderSearchCriteriaBuilder = new OrderSearchCriteriaBuilder();
		return orderSearchCriteriaBuilder.setOrderTypes(Collections.singletonList(orderType))
		        .setActivatedOnOrAfterDate(BotswanaEmrUtils.getDateToday()).build();
	}
	
	public static List<Location> getLocationsWithMFLCode(String facilityLocationMFlCode) {
		LocationService ls = Context.getLocationService();
		LocationAttributeType codeAttrType = ls.getLocationAttributeTypeByUuid(MASTER_FACILITY_CODE_UUID);
		return ls.getLocations(null, null, Collections.singletonMap(codeAttrType, facilityLocationMFlCode), false, null, 1);
	}
	
	// Get facility Mfl code
	public static String getFacilityMflCode(Location location) {
		LocationAttributeType codeAttrType = Context.getLocationService()
		        .getLocationAttributeTypeByUuid(MASTER_FACILITY_CODE_UUID);
		Set<LocationAttribute> locationAttributes = location.getAttributes();
		LocationAttribute locationAttribute = locationAttributes.stream()
		        .filter(attr -> attr.getAttributeType().equals(codeAttrType)).findFirst().orElse(null);
		
		return locationAttribute == null ? "" : locationAttribute.getValueReference();
	}
	
	public static EncounterSearchCriteria getTbEncounterSearchCriteria(Patient patient, EncounterType encounterType) {
		return new EncounterSearchCriteriaBuilder().setEncounterTypes(Collections.singletonList(encounterType))
		        .setPatient(patient).setIncludeVoided(false).createEncounterSearchCriteria();
		
	}
	
	public static List<Encounter> getHtsEncounters(Patient patient, EncounterService encounterService,
	        List<Visit> currentPatientVisit, List<HtmlForm> htsEncounterForms) {
		List<Form> htsForms = htsEncounterForms.stream().map(HtmlForm::getForm).collect(Collectors.toList());
		
		return encounterService.getEncounters(new EncounterSearchCriteria(patient, null, null, null, null, htsForms, null,
		        null, null, currentPatientVisit, false));
	}
	
	public static List<Encounter> getHtsEncounters(Patient patient, EncounterService encounterService,
	        List<HtmlForm> htsEncounterForms) {
		return getHtsEncounters(patient, encounterService, null, htsEncounterForms);
	}
	
	public static List<Form> completedForms(Visit currentPatientVisit) {
		List<Form> completedForms = new ArrayList<>();
		if (currentPatientVisit != null) {
			List<Encounter> encounterList = currentPatientVisit.getNonVoidedEncounters();
			for (Encounter encounter : encounterList) {
				completedForms.add(encounter.getForm());
			}
		}
		return completedForms;
	}
	
	public static List<HtmlForm> completedHtmlForms(Visit currentPatientVisit,
	        @SpringBean("htmlFormEntryService") HtmlFormEntryService htmlFormEntryService) {
		List<HtmlForm> completedHtmlForms = new ArrayList<>();
		for (Form form : completedForms(currentPatientVisit)) {
			completedHtmlForms.add(htmlFormEntryService.getHtmlFormByForm(form));
		}
		return completedHtmlForms;
	}
	
	public static List<Encounter> getEncounters(EncounterType encounterType, Location location, Date fromDate, Date toDate) {
		List<Encounter> encounters = new ArrayList<>();
		
		if (encounterType != null) {
			EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(null, location, fromDate, toDate,
			        null, null, Collections.singletonList(encounterType), null, null, null, false);
			encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		}
		
		return encounters;
	}
	
	public static List<Encounter> getEncountersByPatient(Patient patient, EncounterType encounterType, Location location,
	        Date fromDate, Date toDate) {
		List<Encounter> encounters = new ArrayList<>();
		
		if (patient != null && encounterType != null) {
			EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, location, fromDate,
			        toDate, null, null, Collections.singletonList(encounterType), null, null, null, false);
			encounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		}
		
		return encounters;
	}
	
	private static Date getDateFromString(String dateString) throws ParseException {
		return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
	}
	
	public static List<Registration> getPatientsSeenOnDay(String dateString, Location location) throws ParseException {
		List<Registration> paymentList = Context.getService(BotswanaEmrService.class)
		        .getPatientsRegisteredOnDate(getDateFromString(dateString), getDateFromString(dateString), location, null);
		if (paymentList == null) {
			paymentList = new ArrayList<>();
		}
		
		return paymentList;
	}
	
	public static List<Registration> getPatientsSeenOnDay(Date date, Location location) throws ParseException {
		List<Registration> paymentList = Context.getService(BotswanaEmrService.class).getPatientsRegisteredOnDate(date, date,
		    location, null);
		if (paymentList == null) {
			paymentList = new ArrayList<>();
		}
		
		return paymentList;
	}
	
	public static List<Patient> getPatientsScreenedOnDay(Date dateString, EncounterType encounterType, Location location) {
		List<Patient> patientList = new ArrayList<>();
		List<Encounter> encountersList = Context.getService(BotswanaEmrService.class).getScreeningsDoneOnDate(dateString,
		    dateString, encounterType, null, location);
		if (encountersList != null) {
			encountersList = encountersList.stream().filter(e -> e.getForm() != null).collect(Collectors.toList());
		}
		patientList = encountersList.stream().map(Encounter::getPatient).collect(Collectors.toList());
		return patientList;
	}
	
	public static List<Patient> getPatientsConsultedOnDay(Date date, Location location) {
		List<Patient> patientList = new ArrayList<>();
		List<Encounter> consultationEncountersList = BotswanaEmrUtils.getAllConsultations(date, date, null, location);
		if (consultationEncountersList != null && consultationEncountersList.size() > 0) {
			patientList = consultationEncountersList.stream().map(Encounter::getPatient).collect(Collectors.toList());
		}
		return patientList;
	}
	
	public static List<Patient> getPatientsEnrolledOnDay(Date startDate, Date endDate, Location location,
	        Location currentLocation) {
		List<Patient> patientList = new ArrayList<>();
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		List<PatientQueue> patientQueueList;
		patientQueueList = patientQueueingService.getPatientQueueList(null, startDate, endDate, location, currentLocation,
		    null, null);
		
		if (patientQueueList != null && patientQueueList.size() > 0) {
			patientList = patientQueueList.stream().map(PatientQueue::getPatient).collect(Collectors.toList());
		}
		return patientList;
	}
	
	/**
	 * convert a resource path to a file into a string containing the file's contents
	 *
	 * @param filename resource path to the file
	 * @return the contents of the file in a String
	 * @throws IOException
	 */
	public static String getResourceAsString(String filename) throws IOException {
		InputStream resource = OpenmrsClassLoader.getInstance().getResourceAsStream(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(resource));
		StringBuilder sb = new StringBuilder();
		String line = null;
		
		while ((line = reader.readLine()) != null)
			sb.append(line).append("\n");
		
		reader.close();
		return sb.toString();
	}
	
	public static Encounter getDrugOrderEncounter(@FragmentParam(value = "patientId", required = false) Patient patient,
	        @FragmentParam(value = "visitId", required = false) Visit visit, Location location) {
		Encounter encounter;
		if (visit == null) {
			visit = BotswanaEmrUtils.getPatientActiveVisit(patient, location, true);
		}
		
		List<Encounter> drugOrderEncounters = getEncountersByPatient(patient,
		    Context.getEncounterService().getEncounterTypeByUuid(DRUG_ORDER_ENCOUNTER_TYPE), visit.getLocation(),
		    visit.getStartDatetime(), visit.getStopDatetime());
		if (drugOrderEncounters.isEmpty()) {
			encounter = persistEncounter(patient,
			    Context.getEncounterService().getEncounterTypeByUuid(DRUG_ORDER_ENCOUNTER_TYPE), location, visit);
		} else {
			drugOrderEncounters = sortEncountersByEncounterDatetime(drugOrderEncounters);
			encounter = drugOrderEncounters.get(0);
		}
		return encounter;
	}
	
	public static SimplifiedLabTest getSingleSimplifiedLabTest(LabTest labTest, LabService labService) {
		SimplifiedLabTest simplifiedLabTest = null;
		if (labTest != null) {
			simplifiedLabTest = new SimplifiedLabTest();
			simplifiedLabTest.setTestId(labTest.getLabTestId());
			simplifiedLabTest.setTestName(labTest.getConcept().getDisplayString());
			simplifiedLabTest.setStatus(labTest.getStatus());
			simplifiedLabTest.setPatientId(labTest.getPatient().getPatientId());
			simplifiedLabTest.setPatientNames(formatPersonName(labTest.getPatient().getPersonName()));
			simplifiedLabTest.setDateAdded(formatDateWithoutTime(labTest.getAcceptDate(), "yyyy-MM-dd"));
			simplifiedLabTest.setRequestedBy(formatPersonName(labTest.getCreator().getPersonName()));
			simplifiedLabTest.setResultsList(getLabResultsMap(getAllLabOrderResultsObs(labTest.getLabTestId(), labService)));
		}
		return simplifiedLabTest;
	}
	
	public static List<SimplifiedLabTest> getSimplifiedLabTests(List<LabTest> labTests, LabService labService) {
		List<SimplifiedLabTest> laboratoryTests = new ArrayList<>();
		SimplifiedLabTest simplifiedLabTest;
		if (!labTests.isEmpty()) {
			for (LabTest labTest : labTests) {
				simplifiedLabTest = new SimplifiedLabTest();
				simplifiedLabTest.setTestId(labTest.getLabTestId());
				simplifiedLabTest.setTestName(labTest.getConcept().getDisplayString());
				simplifiedLabTest.setStatus(labTest.getStatus());
				simplifiedLabTest.setPatientId(labTest.getPatient().getPatientId());
				simplifiedLabTest.setPatientNames(formatPersonName(labTest.getPatient().getPersonName()));
				simplifiedLabTest.setDateAdded(formatDateWithoutTime(labTest.getAcceptDate(), "yyyy-MM-dd"));
				simplifiedLabTest.setRequestedBy(formatPersonName(labTest.getCreator().getPersonName()));
				simplifiedLabTest
				        .setResultsList(getLabResultsMap(getAllLabOrderResultsObs(labTest.getLabTestId(), labService)));
				laboratoryTests.add(simplifiedLabTest);
			}
		}
		return laboratoryTests;
	}
	
	public static List<Obs> getAllLabOrderResultsObs(Integer labTestOrderId, LabService labService) {
		
		List<String> labResultsObsUuids = new ArrayList<>();
		labResultsObsUuids.add(LAB_RESULTS_TEXT);
		labResultsObsUuids.add(LAB_TEST_INTERPRETATION);
		labResultsObsUuids.add(DATE_CONCEPT_UUID);
		
		LabTest labTest = labService.getLabTestById(labTestOrderId);
		List<Obs> resultsObsList = new ArrayList<>();
		if (labTest != null && labTest.getStatus().equals("COMPLETED")) {
			Encounter resultsEncounter = labTest.getEncounter();
			if (resultsEncounter != null) {
				for (Obs obs : resultsEncounter.getAllObs()) {
					if (labResultsObsUuids.contains(obs.getConcept().getUuid())) {
						resultsObsList.add(obs);
					}
				}
			}
		}
		return resultsObsList;
	}
	
	public static Map<String, String> getLabResultsMap(List<Obs> obsList) {
		Map<String, String> obsMap = new HashMap<>();
		if (obsList != null && obsList.size() > 0) {
			for (Obs obs : obsList) {
				obsMap.put(obs.getComment(), obs.getValueText() == null ? "" : obs.getValueText());
			}
		}
		return obsMap;
	}
	
	public static void hasRegistryEncounter(PageModel pageModal, Visit visit, EncounterType registryEncounterType) {
		boolean hasRegistryEncounter = false;
		if (visit != null) {
			Set<Encounter> encounters = visit.getEncounters();
			Comparator<Encounter> encounterDateTimeComparator = Comparator.comparing(Encounter::getEncounterDatetime);
			Optional<Encounter> registryEncounter = encounters.stream()
			        .filter(encounter -> encounter.getEncounterType().equals(registryEncounterType))
			        .max(encounterDateTimeComparator);
			if (registryEncounter.isPresent()) {
				hasRegistryEncounter = true;
				pageModal.addAttribute("encounter", registryEncounter.get());
			}
			pageModal.addAttribute("hasRegistryEncounter", hasRegistryEncounter);
			pageModal.addAttribute("visitId", visit.getVisitId());
			pageModal.addAttribute("visit", visit);
		}
	}
	
	public static void hasEncounter(PageModel pageModal, Visit visit, EncounterType encounterType) {
		boolean hasEncounter = false;
		if (visit != null) {
			Set<Encounter> encounters = visit.getEncounters();
			Comparator<Encounter> encounterDateTimeComparator = Comparator.comparing(Encounter::getEncounterDatetime);
			Optional<Encounter> encounter = encounters.stream().filter(enc -> enc.getEncounterType().equals(encounterType))
			        .max(encounterDateTimeComparator);
			if (encounter.isPresent()) {
				hasEncounter = true;
				pageModal.addAttribute("encounter", encounter.get());
			}
			pageModal.addAttribute("hasEncounter", hasEncounter);
			pageModal.addAttribute("visitId", visit.getVisitId());
			pageModal.addAttribute("visit", visit);
		}
	}
	
	public static String[] getAgeBounds(String rangeString) {
		String[] ageRange = rangeString.split("-");
		//			int lowerBound = Integer.parseInt(ageRange[0]);
		//			int upperBound = Integer.parseInt(ageRange[1]);
		return ageRange;
		
	}
	
	public static List<AgeCategory> getDefaultAgeCategories() {
		List<AgeCategory> ageCategoyList = new ArrayList<>();
		ageCategoyList.add(new AgeCategory("1-4", "Below 5", 0, 4));
		ageCategoyList.add(new AgeCategory("5-9", "Over 4 under 10", 5, 9));
		ageCategoyList.add(new AgeCategory("10-14", "Over 9 under 15", 10, 14));
		ageCategoyList.add(new AgeCategory("15-24", "Over 14 under 25", 15, 24));
		ageCategoyList.add(new AgeCategory("25-54", "Over 24 under 55", 25, 54));
		ageCategoyList.add(new AgeCategory("55-64", "Over 54 under 65", 55, 64));
		ageCategoyList.add(new AgeCategory(">=65", "Over 64", 65, 100));
		
		return ageCategoyList;
	}
	
	public static boolean hasHivTestObs(Encounter e) {
		return e.getAllObs().stream().anyMatch(o -> o.getConcept().getUuid().equals(TEST_ORDERED_CONCEPT_UUID)
		        && o.getValueCoded().getUuid().equals(HIV_TEST_CONCEPT_UUID));
	}
	
	public static boolean containsAssociatedEncounter(Encounter e, String htsEncounterUuid) {
		return e.getAllObs().stream().anyMatch(
		    o -> o.getConcept().getUuid().equals(ENCOUNTER_NOTE_CONCEPT_UUID) && o.getValueText().equals(htsEncounterUuid));
	}
	
	public static Encounter getLabOrderEncounter(Patient patient) {
		Encounter labOrderEncounter = null;
		Date today = DateUtil.getStartOfDay(new Date());
		Date threeMonthsAgo = DateUtil.getStartOfMonth(today, -3);
		try {
			EncounterSearchCriteria labEncounterSearchCriteria = new EncounterSearchCriteria(patient, null, threeMonthsAgo,
			        null, null, null, Collections.singletonList(getEncounterType(LAB_REQUEST_ENCOUNTER_TYPE)), null, null,
			        null, false);
			EncounterService encounterService = Context.getEncounterService();
			List<Encounter> labEncounters = encounterService.getEncounters(labEncounterSearchCriteria);
			labEncounters = labEncounters.stream().filter(BotswanaEmrUtils::hasHivTestObs).collect(Collectors.toList());
			// invole the array is null or empty check
			if (labEncounters.isEmpty()) {
				return null;
			}
			
			// Get the latest lab order encounter with active orders
			for (Encounter labEncounter : labEncounters) {
				if (labEncounter.getOrders() == null || labEncounter.getOrders().isEmpty()) {
					continue;
				}
				if (labEncounter.getOrders().stream().anyMatch(o -> o.isActive())) {
					labOrderEncounter = labEncounter;
					break;
				}
			}
		}
		catch (APIException e) {
			log.error("Error occurred while checking for lab orders", e);
		}
		return labOrderEncounter;
	}
	
	public static SimplifiedProficiencyTesting getSimplifiedProficiencyTesting(HtsProficiencyTesting htsProficiencyTesting) {
		SimplifiedProficiencyTesting simplifiedProficiencyTesting = new SimplifiedProficiencyTesting();
		simplifiedProficiencyTesting.setProficiencyTestingId(htsProficiencyTesting.getId());
		simplifiedProficiencyTesting.setPanelId(htsProficiencyTesting.getPanelId());
		if (htsProficiencyTesting.getReceivedBy() != null) {
			simplifiedProficiencyTesting.setReceivedBy(htsProficiencyTesting.getReceivedBy().getUuid());
			simplifiedProficiencyTesting
			        .setReceivedByName(htsProficiencyTesting.getReceivedBy().getPersonName().getFullName());
		}
		if (htsProficiencyTesting.getDatePanelReceived() != null)
			simplifiedProficiencyTesting.setDatePanelReceived(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTesting.getDatePanelReceived(), "yyyy-MM-dd"));
		simplifiedProficiencyTesting.setTestingPoint(htsProficiencyTesting.getTestingPoint());
		if (htsProficiencyTesting.getResultsDueDate() != null)
			simplifiedProficiencyTesting.setResultsDueDate(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTesting.getResultsDueDate(), "yyyy-MM-dd"));
		if (htsProficiencyTesting.getDateTested() != null)
			simplifiedProficiencyTesting.setDateTested(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTesting.getDateTested(), "yyyy-MM-dd"));
		if (htsProficiencyTesting.getDateReviewed() != null)
			simplifiedProficiencyTesting.setDateReviewed(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTesting.getDateReviewed(), "yyyy-MM-dd"));
		simplifiedProficiencyTesting.setPercentageScore(htsProficiencyTesting.getPercentageScore());
		simplifiedProficiencyTesting.setPerformanceScore(htsProficiencyTesting.getPerformanceScore());
		if (htsProficiencyTesting.getReportReviewedBy() != null) {
			simplifiedProficiencyTesting.setReportReviewedBy(htsProficiencyTesting.getReportReviewedBy().getUuid());
			simplifiedProficiencyTesting
			        .setReportReviewedByName(htsProficiencyTesting.getReportReviewedBy().getPersonName().getFullName());
		}
		if (htsProficiencyTesting.getResultsReceivedBy() != null) {
			simplifiedProficiencyTesting
			        .setResultsReceivedByName(htsProficiencyTesting.getResultsReceivedBy().getPersonName().getFullName());
			simplifiedProficiencyTesting.setResultsReceivedBy(htsProficiencyTesting.getResultsReceivedBy().getUuid());
		}
		
		simplifiedProficiencyTesting.setComment(htsProficiencyTesting.getComment());
		simplifiedProficiencyTesting.setPublished(htsProficiencyTesting.getPublished());
		return simplifiedProficiencyTesting;
	}
	
	public static Obs getLatestObs(Patient patient, EncounterType encounterType, Concept concept) {
		// Get latest obs of a given concept and encounter type
		
		EncounterService encounterService = Context.getEncounterService();
		EncounterSearchCriteriaBuilder encounterSearchCriteriaBuilder = new EncounterSearchCriteriaBuilder();
		EncounterSearchCriteria encounterSearchCriteria = encounterSearchCriteriaBuilder.setPatient(patient)
		        .setEncounterTypes(Collections.singletonList(encounterType)).setIncludeVoided(false)
		        .createEncounterSearchCriteria();
		List<Encounter> encounters = encounterService.getEncounters(encounterSearchCriteria);
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		List<Obs> obsList = new ArrayList<>();
		for (Encounter encounter : encounters) {
			obsList.addAll(encounter.getAllObs());
		}
		Optional<Obs> latestObs = obsList.stream().filter(obs -> obs.getConcept().equals(concept)).max(obsDateComparator);
		return latestObs.orElse(null);
	}
	
	public static Concept getPatientHivStatus(Patient patient) {
		Obs hivStatusObs = getPatientHivStatusObs(patient);
		Concept hivStatus = null;
		if (hivStatusObs != null) {
			hivStatus = hivStatusObs.getValueCoded();
		}
		return hivStatus;
	}
	
	public static Obs getPatientHivStatusObs(Patient patient) {
		List<Concept> hivStatusConcepts = Arrays.asList(
		    Context.getConceptService().getConceptByUuid(HTS_PRIOR_TEST_STATUS_CONCEPT_UUID),
		    Context.getConceptService().getConceptByUuid(HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID),
		    Context.getConceptService().getConceptByUuid(HIV_STATUS_RESULT_CONCEPT_UUID));
		List<EncounterType> hivTestingEncounterTypes = Arrays.asList(
		    Context.getEncounterService().getEncounterTypeByUuid(HTS_PRIOR_TEST_ENCOUNTER_TYPE_UUID),
		    Context.getEncounterService().getEncounterTypeByUuid(HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID),
		    Context.getEncounterService().getEncounterTypeByUuid(HTS_VERIFICATION_ENCOUNTER_TYPE_UUID));
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, null, null, null, null,
		        hivTestingEncounterTypes, null, null, null, false);
		
		List<Encounter> hivTestingEncounters = Context.getEncounterService().getEncounters(encounterSearchCriteria);
		
		List<Obs> obsList = Context.getObsService().getObservations(Collections.singletonList(patient.getPerson()),
		    hivTestingEncounters, hivStatusConcepts, null, null, null, null, 1, null, null, null, false);
		
		Obs hivStatusObs = null;
		if (obsList != null && !obsList.isEmpty()) {
			hivStatusObs = obsList.get(0);
		}
		return hivStatusObs;
	}
	
	public static String getHivStatus(Patient patient) {
		Concept clientHivStatus = getPatientHivStatus(patient);
		
		if (clientHivStatus == null) {
			return "unknown";
		}
		if (clientHivStatus.getUuid().equals(HIV_STATUS_POSITIVE)) {
			return "positive";
		}
		return "negative";
	}
	
	public static boolean isHivPositive(Patient patient) {
		boolean status = false;
		Concept clientHivStatus = getPatientHivStatus(patient);
		if (clientHivStatus != null && clientHivStatus.getUuid().equals(HIV_STATUS_POSITIVE)) {
			status = true;
		}
		return status;
	}
	
	public static void createAppointment(Patient patient, Location location, Date nextAppointmentDate,
	        String appointmentReason) {
		Appointment appointment = new Appointment();
		AppointmentType defaultAppointmentType = getDefaultAppointmentType();
		
		appointment.setDateCreated(new Date());
		appointment.setPatient(patient);
		appointment.setCreator(Context.getAuthenticatedUser());
		appointment.setReason(appointmentReason);
		if (defaultAppointmentType != null) {
			appointment.setAppointmentType(defaultAppointmentType);
			
			Date startDate = getStartTimeOfDay(nextAppointmentDate);
			Date endDate = getEndTimeOfDay(nextAppointmentDate);
			
			Provider provider = getProvider(Context.getAuthenticatedUser().getPerson());
			TimeSlot appointmentTimeSlot = getAppointmentTimeSlot(startDate, endDate, provider, location,
			    defaultAppointmentType);
			
			AppointmentService appointmentService = Context.getService(AppointmentService.class);
			appointmentService.saveAppointmentBlock(appointmentTimeSlot.getAppointmentBlock());
			appointmentService.saveTimeSlot(appointmentTimeSlot);
			appointment.setTimeSlot(appointmentTimeSlot);
			appointment.setStatus(Appointment.AppointmentStatus.SCHEDULED);
			
			appointmentService.saveAppointment(appointment);
		}
	}
	
	public static boolean isEnrolledInProgram(Patient patient, String programUuid) {
		List<PatientProgram> patientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, null, null,
		    null, null, null, false);
		boolean isEnrolled = false;
		for (PatientProgram programIn : patientPrograms) {
			if (programIn.getDateCompleted() == null && programIn.getProgram().getUuid().equals(programUuid)) {
				isEnrolled = true;
				break;
			}
		}
		return isEnrolled;
	}
	
	public static int getCompletedReviews(Encounter postOpEcounter, Patient patient) {
		int count = 0;
		if (postOpEcounter == null) {
			return count;
		} else {
			Obs obs = BotswanaEmrUtils.getLatestObs(patient.getPerson(), Collections.singletonList(postOpEcounter),
			    BotswanaEmrConstants.SCM_REVIEW_DATE_CONCEPT_UUID, patient.getDateCreated(), new Date());
			if (obs != null) {
				switch (obs.getValueCoded().getUuid()) {
					case BotswanaEmrConstants.REVIEW_DATE_3:
						count = 3;
						break;
					case BotswanaEmrConstants.REVIEW_DATE_2:
						count = 2;
						break;
					case BotswanaEmrConstants.REVIEW_DATE_1:
						count = 1;
						break;
				}
			}
		}
		return count;
	}
	
	public static void saveDrugOrderFromDrugObs(Set<Obs> drugObs, Patient patient, Boolean isStatDose) {
		Set<DrugObsData> drugObsDataSet = findDrugObsWithGrouping(drugObs);
		OrderService os = Context.getOrderService();
		Concept orderFrequencyConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.DEFAULT_MEDICATION_FREQUENCY);
		User user = Context.getAuthenticatedUser();
		Provider provider = Context.getProviderService().getProvidersByPerson(user.getPerson()).iterator().next();
		OrderType orderType = os.getOrderTypeByName("Drug order");
		CareSetting careSetting = os.getCareSettingByName(BotswanaEmrConstants.DEFAULT_CARE_SETTING);
		List<Order> activeDrugOrders = os.getActiveOrders(patient, orderType, careSetting, new Date());
		
		for (DrugObsData drugObsData : drugObsDataSet) {
			if (drugObsData.drugObs == null
			        || drugExists(activeDrugOrders, drugObsData.drugObs.getValueDrug().getConcept().getConceptId(),
			            drugObsData.drugObs.getEncounter().getEncounterId())) {
				continue;
			}
			
			DrugOrder drugOrder = new DrugOrder();
			drugOrder.setPatient(patient);
			drugOrder.setEncounter(drugObsData.drugObs.getEncounter());
			
			Drug drug = drugObsData.drugObs.getValueDrug();
			
			if (drug == null && orderType == null) {
				continue;
			}
			drugOrder.setDrug(drug);
			
			if (drugObsData.doseObs != null) {
				drugOrder.setDose(drugObsData.doseObs.getValueNumeric());
				drugOrder.setQuantity(drugObsData.doseObs.getValueNumeric());
			}
			if (drugObsData.doseUnit != null) {
				drugOrder.setDoseUnits(drugObsData.doseUnit.getValueCoded());
				drugOrder.setQuantityUnits(drugObsData.doseUnit.getValueCoded());
			}
			
			if (drugObsData.routeObs != null) {
				drugOrder.setRoute(drugObsData.routeObs.getValueCoded());
			}
			
			drugOrder.setFrequency(os.getOrderFrequencyByConcept(orderFrequencyConcept));
			
			drugOrder.setOrderer(provider);
			drugOrder.setCareSetting(careSetting);
			drugOrder.setNumRefills(0);
			
			if (isStatDose) {
				drugOrder.setUrgency(Order.Urgency.STAT);
				drugOrder.setOrderReasonNonCoded("Stat dose");
				drugOrder.setDateActivated(drugOrder.getEncounter().getEncounterDatetime());
				drugOrder.setAutoExpireDate(
				    org.apache.commons.lang.time.DateUtils.addMinutes(drugOrder.getDateActivated(), 5));
				drugOrder.setFulfillerStatus(FulfillerStatus.COMPLETED);
				drugOrder.setFulfillerComment("Stat dose");
			}
			
			OrderContext orderContext = new OrderContext();
			orderContext.setOrderType(orderType);
			Context.getOrderService().saveOrder(drugOrder, orderContext);
			
		}
	}
	
	private static Set<DrugObsData> findDrugObsWithGrouping(Set<Obs> groupingObs) {
		Set<DrugObsData> drugObsDataSet = new HashSet<DrugObsData>();
		for (Obs obs : groupingObs) {
			Set<Obs> childObs = obs.getGroupMembers();
			DrugObsData drugObsData = new DrugObsData();
			drugObsData.setObs(childObs);
			drugObsDataSet.add(drugObsData);
		}
		
		return drugObsDataSet;
	}
	
	private static boolean drugExists(List<Order> drugOrders, int conceptId, int encounterId) {
		for (Order order : drugOrders) {
			if (order.getConcept().getId() == conceptId && order.getEncounter().getEncounterId() == encounterId) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean getIsPepVisitType(Visit currentPatientVisit) {
		VisitType visitType = currentPatientVisit.getVisitType();
		return visitType != null && visitType.getUuid().equals(BotswanaEmrConstants.PEP_INITIATION_VISIT_TYPE_UUID)
		        || visitType.getUuid().equals(BotswanaEmrConstants.PEP_FOLLOW_UP_VISIT_TYPE_UUID);
	}
	
	public static boolean getIsPrepVisitType(Visit currentPatientVisit) {
		VisitType visitType = currentPatientVisit.getVisitType();
		return visitType.getUuid().equals(BotswanaEmrConstants.PREP_INITIATION_VISIT_TYPE_UUID)
		        || visitType.getUuid().equals(BotswanaEmrConstants.PREP_FOLLOW_UP_VISIT_TYPE_UUID);
	}
	
	public static boolean getIsArtVisitType(Visit currentPatientVisit) {
		VisitType visitType = currentPatientVisit.getVisitType();
		return visitType.getUuid().equals(BotswanaEmrConstants.ART_INITIATION_VISIT_TYPE_UUID)
		        || visitType.getUuid().equals(BotswanaEmrConstants.ART_FOLLOW_UP_VISIT_TYPE_UUID);
	}
	
	public static final ArrayList<Location> getFacilitiesList() {
		LocationService locationService = Context.getLocationService();
		return new ArrayList<>(locationService
		        .getLocationsByTag(locationService.getLocationTagByName(BotswanaEmrConstants.VISIT_LOCATION_TAG_NAME)));
	}
	
	public static final ArrayList<Location> getLabOrderingSites() {
		LocationService locationService = Context.getLocationService();
		return new ArrayList<>(locationService
		        .getLocationsByTag(locationService.getLocationTagByName(BotswanaEmrConstants.LAB_ORDER_SITE_TAG_NAME)));
	}
	
	public static final String determinePrescriptionStatus(Set<Order> encounterOrders) {
		String prescriptionStatus = "";
		if (encounterOrders.stream().allMatch(o -> o.getFulfillerStatus() == null)) {
			prescriptionStatus = "Awaiting";
		} else if (encounterOrders.stream().allMatch(
		    o -> o.getFulfillerStatus() != null && o.getFulfillerStatus().equals(Order.FulfillerStatus.COMPLETED))) {
			prescriptionStatus = "Completed";
		} else if (encounterOrders.stream().anyMatch(
		    o -> o.getFulfillerStatus() != null && o.getFulfillerStatus().equals(Order.FulfillerStatus.IN_PROGRESS))) {
			prescriptionStatus = "In progress";
		} else {
			if (encounterOrders.stream().anyMatch(
			    o -> o.getFulfillerStatus() != null && o.getFulfillerStatus().equals(Order.FulfillerStatus.EXCEPTION))) {
				prescriptionStatus = "Partially completed";
			}
			
			if (encounterOrders.stream().anyMatch(o -> o.getFulfillerStatus() == null)
			        && encounterOrders.stream().anyMatch(o -> o.getFulfillerStatus() != null)) {
				prescriptionStatus = "Partially filled";
			}
		}
		return prescriptionStatus;
	}
	
	public static void purgePrivilegeFromRole(String role_name, String privilege_name) {
		UserService userService = Context.getUserService();
		Role role = userService.getRole(role_name);
		if (role != null) {
			Privilege privilege = userService.getPrivilege(privilege_name);
			if (privilege != null) {
				role.removePrivilege(privilege);
				userService.saveRole(role);
			}
		}
	}
	
	public static String fetchAndformatDrugName(Drug drug) {
		String drugName = "";
		// Get BOTSMEDLIST mapping source
		ConceptSource botsmedList = Context.getConceptService().getConceptSourceByName("BOTSMEDLIST");
		// Get SAME-AS mapping type
		ConceptMapType sameAs = Context.getConceptService().getConceptMapTypeByName("SAME-AS");
		DrugReferenceMap botsMedsDrugReferenceMap = drug.getDrugReferenceMaps().stream().filter(
		    d -> d.getConceptReferenceTerm().getConceptSource().equals(botsmedList) && d.getConceptMapType().equals(sameAs))
		        .findFirst().orElse(null);
		if (botsMedsDrugReferenceMap != null) {
			drugName = botsMedsDrugReferenceMap.getConceptReferenceTerm().getCode();
		} else {
			// Given a drugname in this format "Benzoic Acid Comp Ointment BP, 500g", remove the ", 500g"
			// and return the drugname as "Benzoic Acid Comp Ointment BP"
			drugName = drug.getName().split(", ")[0];
		}
		
		return drugName;
	}
	
	public static List<SimplifiedDrug> getSimplifiedDrugsList() {
		List<SimplifiedDrug> simplifiedDrugs = new ArrayList<>();
		List<Drug> drugs = Context.getConceptService().getAllDrugs(false).stream().filter(d -> d.getConcept() != null)
		        .collect(Collectors.toList());
		
		// Get the drug name from the BOTSMEDLIST mapping source
		// if drug name already exists in the simplifiedDrugs list, then don't add it
		drugs.forEach(drug -> {
			String drugName = fetchAndformatDrugName(drug);
			if (!simplifiedDrugs.stream().anyMatch(sd -> sd.getName().equals(drugName))) {
				simplifiedDrugs.add(new SimplifiedDrug(drug.getUuid(), drugName));
			}
		});
		
		return simplifiedDrugs;
	}
	
	public static boolean isTbPresumptive(Patient patient, Visit lastPatientVisit) {
		boolean isPresumptive = false;
		Obs hivObs = null;
		if (lastPatientVisit != null) {
			Set<Obs> tbQuestionsWithAnswerYes = new HashSet<>();
			List<Obs> tbScreeningResultsObservations = getTbQuestionsObservations(Collections.singletonList(patient),
			    lastPatientVisit);
			if (tbScreeningResultsObservations != null && !tbScreeningResultsObservations.isEmpty()) {
				for (Obs obs : tbScreeningResultsObservations) {
					final Integer YES_ANSWER_CONCEPT_ID = BotswanaEmrConstants.YES_CONCEPT_ID;
					if (Objects.equals(obs.getValueCoded().getConceptId(), YES_ANSWER_CONCEPT_ID)) {
						tbQuestionsWithAnswerYes.add(obs);
					}
					if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.HIV_STATUS_QUESTION)) {
						hivObs = obs;
					}
				}
				// if hiv positive and has one or more yes answers, then patient is presumptive otherwise if hiv negative or unkno and has two or more yes answers, then patient is presumptive 
				if (hivObs != null && hivObs.getValueCoded().getUuid().equals(BotswanaEmrConstants.POSITIVE)) {
					if (tbQuestionsWithAnswerYes.size() >= 1) {
						isPresumptive = true;
					}
				} else {
					if (tbQuestionsWithAnswerYes.size() >= 2) {
						isPresumptive = true;
					}
				}
				
			}
		}
		
		return isPresumptive;
	}
	
	public static List<Concept> getQuantityUnits() {
		Concept dispensingUnitsConcept = Context.getConceptService()
		        .getConceptByUuid(BotswanaEmrConstants.DISPENSING_UNITS_CONCEPT_UUID);
		if (dispensingUnitsConcept == null) {
			return new ArrayList<>();
		}
		return new ArrayList<>(dispensingUnitsConcept.getSetMembers());
	}
	
	public static List<DrugOrderSimplifier> getActivePatientDrugOrders(Patient patient) {
		// Initialize list to store simplified drug orders
		List<DrugOrderSimplifier> drugOrderSimplifierList = new ArrayList<>();
		
		// Retrieve all active drug orders for the current patient
		OrderSearchCriteriaBuilder orderSearchCriteriaBuilder = new OrderSearchCriteriaBuilder();
		orderSearchCriteriaBuilder.setPatient(patient);
		OrderType orderType = Context.getOrderService().getOrderTypeByUuid(BotswanaEmrConstants.DRUG_ORDER_TYPE_UUID);
		orderSearchCriteriaBuilder.setOrderTypes(Collections.singletonList(orderType));
		orderSearchCriteriaBuilder.setExcludeCanceledAndExpired(true);
		orderSearchCriteriaBuilder.setExcludeDiscontinueOrders(true);
		orderSearchCriteriaBuilder.setIsStopped(false);
		
		OrderSearchCriteria orderSearchCriteria = orderSearchCriteriaBuilder.build();
		List<Order> drugOrders = Context.getOrderService().getOrders(orderSearchCriteria);
		
		// Simplify and format each drug order
		for (Order order : drugOrders) {
			try {
				DrugOrder drugOrder = (DrugOrder) order;
				// Simplify the drug order
				DrugOrderSimplifier drugOrderSimplifier = DrugOrderSimplifier.simplify(drugOrder);
				// Fetch and format the drug name
				drugOrderSimplifier.setDrug(fetchAndformatDrugName(drugOrder.getDrug()));
				
				// Add the simplified drug order to the list
				drugOrderSimplifierList.add(drugOrderSimplifier);
			}
			catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		
		// Return the list of simplified drug orders
		return drugOrderSimplifierList;
	}
	
	public static String currentRegimen(Patient patient) {
		PersonAttributeType currentRegimen;
		currentRegimen = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.TB_PATIENT_REGIMEN_UUID);
		
		if (patient.getAttribute(currentRegimen) != null && patient.getAttribute(currentRegimen).getValue() != null) {
			return patient.getAttribute(currentRegimen).getValue();
		}
		return null;
	}
	
	public static boolean onTreatment(Patient patient) {
		PersonAttributeType onTreatment;
		onTreatment = Context.getPersonService().getPersonAttributeTypeByUuid(BotswanaEmrConstants.TB_TREATMENT_STATUS);
		return patient.getAttribute(onTreatment) != null && patient.getAttribute(onTreatment).getValue() != null;
	}
	
	public static SimpleObject addReferralDetails(SimpleObject response, Patient patient, Visit activeVisit) {
		Comparator<Obs> obsDateComparator = Comparator.comparing(Obs::getObsDatetime);
		Comparator<Order> orderDateComparator = Comparator.comparing(Order::getDateCreated);
		OrderType referralOrderType = orderService.getOrderTypeByUuid(REFERRAL_ORDER_TYPE_UUID);
		
		List<Order> referrals = getReferrals(patient, orderDateComparator, referralOrderType, activeVisit);
		List<Map<String, Object>> patientReferrals = new ArrayList<>();
		
		if (!referrals.isEmpty()) {
			for (Order referral : referrals) {
				Optional<Obs> referringDepartmentObs = getCurrentObs(obsDateComparator, referral, REFERRING_DEPARTMENT_CONCEPT_UUID);
				Optional<Obs> receivingDepartmentObs = getCurrentObs(obsDateComparator, referral, RECEIVING_DEPARTMENT_CONCEPT_UUID);
				
				String reason = "";
				if (referral.getOrderReason() != null) {
					reason = String.valueOf(referral.getOrderReason().getName());
				} else {
					reason = referral.getOrderReasonNonCoded();
				}
				
				Map<String, Object> referralData = new HashMap<>();
				referralData.put("id", referral.getOrderId());
				referralData.put("encounter", referral.getEncounter().getEncounterId());
				referralData.put("referringDepartment", referringDepartmentObs.map(Obs::getValueText).orElse(""));
				referralData.put("facility", referral.getEncounter().getLocation().getName());
				referralData.put("referredTo", referral.getOrderer().getName());
				referralData.put("reason", reason);
				referralData.put("receivingFacility", receivingDepartmentObs.map(Obs::getComment).orElse(""));
				referralData.put("referredBy", formatPersonName(referral.getCreator().getPersonName()));
				referralData.put("referredDate", referral.getDateCreated());
				referralData.put("status", referral.getFulfillerStatus());
				referralData.put("notes", referral.getCommentToFulfiller());
				referralData.put("uuid", referral.getUuid());
				
				patientReferrals.add(referralData);
			}
		} else {
			Map<String, Object> referralPlaceholder = new HashMap<>();
			referralPlaceholder.put("id", "");
			referralPlaceholder.put("encounter", "");
			referralPlaceholder.put("referringDepartment", "");
			referralPlaceholder.put("facility", "");
			referralPlaceholder.put("referredTo", "");
			referralPlaceholder.put("reason", "");
			referralPlaceholder.put("receivingFacility", "");
			referralPlaceholder.put("referredBy", "");
			referralPlaceholder.put("referredDate", "");
			referralPlaceholder.put("status", "");
			referralPlaceholder.put("notes", "");
			referralPlaceholder.put("uuid", "");
			patientReferrals.add(referralPlaceholder);
		}
		
		response.put("patientReferrals", patientReferrals);
		return response;
	}
	
	public static List<Order> getReferrals(Patient patient, Comparator<Order> orderDateComparator, OrderType orderType, Visit visit) {
		return orderService.getAllOrdersByPatient(patient).stream()
				.filter(order -> order.getOrderType().equals(orderType))
				.filter(order -> order.getEncounter().getVisit().equals(visit))
				.sorted(orderDateComparator)
				.collect(Collectors.toList());
	}
	
	public static void getVitalsSummaryInformation(PageModel model, Visit visit) {
		model.addAttribute("temperature", "");
		model.addAttribute("weight", "");
		model.addAttribute("height", "");
		model.addAttribute("systolicBp", "");
		model.addAttribute("diastolicBp", "");
		model.addAttribute("bmi", "");
		model.addAttribute("bsa", "");
		model.addAttribute("pRate", "");
		model.addAttribute("rRate", "");
		model.addAttribute("glucoseLevel", "");
		model.addAttribute("lmp", "");
		
		if (visit == null) {
			return;
		}
		
		for (Encounter encounter : visit.getEncounters()) {
			Set<Obs> obsSet = encounter.getAllObs();
			if (obsSet != null) {
				for (Obs obs : obsSet) {
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.SYSTOLIC_BP))
					        && obs.getValueNumeric() != null) {
						model.addAttribute("systolicBp", obs.getValueNumeric().intValue());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.DIASTOLIC_BP))
					        && obs.getValueNumeric() != null) {
						model.addAttribute("diastolicBp", obs.getValueNumeric().intValue());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.TEMPERATURE))
					        && obs.getValueNumeric() != null) {
						model.addAttribute("temperature", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.WEIGHT)) && obs.getValueNumeric() != null) {
						model.addAttribute("weight", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.HEIGHT)) && obs.getValueNumeric() != null) {
						model.addAttribute("height", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.BMI)) && obs.getValueNumeric() != null) {
						model.addAttribute("bmi", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.BSA)) && obs.getValueNumeric() != null) {
						model.addAttribute("bsa", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.PULSE)) && obs.getValueNumeric() != null) {
						model.addAttribute("pRate", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.RESPIRATORY_RATE))
					        && obs.getValueNumeric() != null) {
						model.addAttribute("rRate", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.GLUCOSE_LEVEL))
					        && obs.getValueNumeric() != null) {
						model.addAttribute("glucoseLevel", obs.getValueNumeric());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.LMP)) && obs.getValueDatetime() != null) {
						model.addAttribute("lmp", obs.getValueDatetime());
					}
				}
			}
		}
	}
	
	public static void loadModelWithVitals(PageModel model, Encounter encounter) {
		String temperature = "";
		String temperatureObsId = "";
		String systolicBp = "";
		String systolicBpObsId = "";
		String diastolicBp = "";
		String diastolicBpObsId = "";
		String weight = "";
		String weightObsId = "";
		String height = "";
		String heightObsId = "";
		String bmi = "";
		String bmiObsId = "";
		String bsa = "";
		String bsaObsId = "";
		String rRate = "";
		String rRateObsId = "";
		String pRate = "";
		String pRateObsId = "";
		String headCircumference = "";
		String headCircumferenceObsId = "";
		String glucoseLevel = "";
		String glucoseLevelObsId = "";
		String oxygenSaturation = "";
		String oxygenSaturationObsId = "";
		String lmp = "";
		String lmpObsId = "";
		String pregnancyKnown = "";
		String pregnancyKnownObsId = "";
		String conscious = "";
		String consciousObsId = "";
		String responsive = "";
		String responsiveObsId = "";
		
		if (encounter != null) {
			//loop through all and pick the ones with vitals recorded
			Set<Obs> obsSet = encounter.getAllObs();
			//Loop through all the obs and find those that are related to vitals
			if (obsSet != null) {
				for (Obs obs : obsSet) {
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.TEMPERATURE))
					        && obs.getValueNumeric() != null) {
						temperature = String.valueOf(obs.getValueNumeric());
						temperatureObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.SYSTOLIC_BP))
					        && obs.getValueNumeric() != null) {
						systolicBp = String.valueOf(obs.getValueNumeric().intValue());
						systolicBpObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.DIASTOLIC_BP))
					        && obs.getValueNumeric() != null) {
						diastolicBp = String.valueOf(obs.getValueNumeric().intValue());
						diastolicBpObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.WEIGHT)) && obs.getValueNumeric() != null) {
						weight = String.valueOf(obs.getValueNumeric());
						weightObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.HEIGHT)) && obs.getValueNumeric() != null) {
						height = String.valueOf(obs.getValueNumeric());
						heightObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.BMI)) && obs.getValueNumeric() != null) {
						bmi = String.valueOf(obs.getValueNumeric());
						bmiObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.BSA)) && obs.getValueNumeric() != null) {
						bsa = String.valueOf(obs.getValueNumeric());
						bsaObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.RESPIRATORY_RATE))
					        && obs.getValueNumeric() != null) {
						rRate = String.valueOf(obs.getValueNumeric());
						rRateObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.PULSE)) && obs.getValueNumeric() != null) {
						pRate = String.valueOf(obs.getValueNumeric());
						pRateObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.HEAD_CIRCUMFERENCE))
					        && obs.getValueNumeric() != null) {
						headCircumference = String.valueOf(obs.getValueNumeric());
						headCircumferenceObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.GLUCOSE_LEVEL))
					        && obs.getValueNumeric() != null) {
						glucoseLevel = String.valueOf(obs.getValueNumeric());
						glucoseLevelObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.OXYGEN_SATURATION))
					        && obs.getValueNumeric() != null) {
						oxygenSaturation = String.valueOf(obs.getValueNumeric());
						oxygenSaturationObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.LMP)) && obs.getValueDate() != null) {
						lmp = String.valueOf(obs.getValueDate());
						lmpObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.PREGNANCY_STATUS))
					        && obs.getValueCoded() != null) {
						pregnancyKnown = String.valueOf(obs.getValueCoded().getId());
						pregnancyKnownObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.CONSCIOUS)) && obs.getValueCoded() != null) {
						conscious = String.valueOf(obs.getValueCoded().getId());
						consciousObsId = String.valueOf(obs.getObsId());
					}
					if (obs.getConcept().equals(getConcept(BotswanaEmrConstants.RESPONSIVE))
					        && obs.getValueCoded() != null) {
						responsive = String.valueOf(obs.getValueCoded().getId());
						responsiveObsId = String.valueOf(obs.getObsId());
					}
				}
			}
		}
		model.addAttribute("temperature", temperature);
		model.addAttribute("temperatureObsId", temperatureObsId);
		model.addAttribute("systolicBp", systolicBp);
		model.addAttribute("systolicBpObsId", systolicBpObsId);
		model.addAttribute("diastolicBp", diastolicBp);
		model.addAttribute("diastolicBpObsId", diastolicBpObsId);
		model.addAttribute("weight", weight);
		model.addAttribute("weightObsId", weightObsId);
		model.addAttribute("height", height);
		model.addAttribute("heightObsId", heightObsId);
		model.addAttribute("bmi", bmi);
		model.addAttribute("bmiObsId", bmiObsId);
		model.addAttribute("bsa", bsa);
		model.addAttribute("bsaObsId", bsaObsId);
		model.addAttribute("rRate", rRate);
		model.addAttribute("rRateObsId", rRateObsId);
		model.addAttribute("pRate", pRate);
		model.addAttribute("pRateObsId", pRateObsId);
		model.addAttribute("headCircumference", headCircumference);
		model.addAttribute("headCircumferenceObsId", headCircumferenceObsId);
		model.addAttribute("glucoseLevel", glucoseLevel);
		model.addAttribute("glucoseLevelObsId", glucoseLevelObsId);
		model.addAttribute("oxygenSaturation", oxygenSaturation);
		model.addAttribute("oxygenSaturationObsId", oxygenSaturationObsId);
		model.addAttribute("lmp", lmp);
		model.addAttribute("lmpObsId", lmpObsId);
		model.addAttribute("pregnancyKnown", pregnancyKnown);
		model.addAttribute("pregnancyKnownObsId", pregnancyKnownObsId);
		model.addAttribute("conscious", conscious);
		model.addAttribute("consciousObsId", consciousObsId);
		model.addAttribute("responsive", responsive);
		model.addAttribute("responsiveObsId", responsiveObsId);
		
	}
}
