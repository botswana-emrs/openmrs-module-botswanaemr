/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.lab;

import org.openmrs.EncounterType;
import org.openmrs.OrderType;
import org.openmrs.PatientIdentifierType;
import org.openmrs.Role;

import java.io.Serializable;
import java.util.Date;

public class Lab implements Serializable {
	
	private static final long serialVersionUID = 716116064672914345L;
	
	private Integer labId;
	
	private String name;
	
	private String description;
	
	private OrderType labOrderType;
	
	private EncounterType labTestEncounterType;
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public Integer getLabId() {
		return labId;
	}
	
	public void setLabId(Integer labId) {
		this.labId = labId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public OrderType getLabOrderType() {
		return labOrderType;
	}
	
	public void setLabOrderType(OrderType labOrderType) {
		this.labOrderType = labOrderType;
	}
	
	public EncounterType getLabTestEncounterType() {
		return labTestEncounterType;
	}
	
	public void setLabTestEncounterType(EncounterType labTestEncounterType) {
		this.labTestEncounterType = labTestEncounterType;
	}
	
	public PatientIdentifierType getPatientIdentifierType() {
		return patientIdentifierType;
	}
	
	public void setPatientIdentifierType(PatientIdentifierType patientIdentifierType) {
		this.patientIdentifierType = patientIdentifierType;
	}
	
	public Boolean getRetired() {
		return retired;
	}
	
	public void setRetired(Boolean retired) {
		this.retired = retired;
	}
	
	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}
	
	public Date getRetiredDate() {
		return retiredDate;
	}
	
	public void setRetiredDate(Date retiredDate) {
		this.retiredDate = retiredDate;
	}
	
	private PatientIdentifierType patientIdentifierType;
	
	private Boolean retired = false;
	
	private Role role;
	
	private Date retiredDate;
}
