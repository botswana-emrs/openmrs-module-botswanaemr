/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import static org.hibernate.criterion.Restrictions.eq;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.APIException;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.dao.LabDAO;
import org.openmrs.module.botswanaemr.lab.Lab;
import org.openmrs.module.botswanaemr.lab.LabTest;

public class HibernateLabDAO implements LabDAO {
	
	/**
	 * Hibernate session factory
	 */
	private SessionFactory sessionFactory;
	
	/**
	 * Set session factory
	 *
	 * @param sessionFactory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public List<Lab> getAllActiveLab() throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		criteria.add(eq("retired", false));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Lab> getAllLab() throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		return criteria.list();
	}
	
	public Lab getLabById(Integer labId) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		criteria.add(eq("labId", labId));
		return (Lab) criteria.uniqueResult();
	}
	
	public Lab getLabByName(String name) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		criteria.add(eq("name", name));
		return (Lab) criteria.uniqueResult();
	}
	
	public LabTest getLabTestById(Integer labTestId) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("labTestId", labTestId));
		return (LabTest) criteria.uniqueResult();
	}
	
	public LabTest getLabTestByOrder(Order order) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("order", order));
		return (LabTest) criteria.uniqueResult();
	}
	
	public LabTest getLabTestBySampleNumber(String sampleNumber) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("sampleNumber", sampleNumber));
		return (LabTest) criteria.uniqueResult();
	}
	
	public Lab saveLab(Lab lab) throws DAOException {
		return (Lab) sessionFactory.getCurrentSession().merge(lab);
	}
	
	public LabTest saveLabTest(LabTest labTest) throws DAOException {
		return (LabTest) sessionFactory.getCurrentSession().merge(labTest);
	}
	
	public void deleteLab(Lab lab) throws DAOException {
		sessionFactory.getCurrentSession().delete(lab);
	}
	
	public Lab getLabByRole(Role role) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		criteria.add(eq("role", role));
		return (Lab) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Lab> getLabByRoles(List<Role> roles) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Lab.class);
		criteria.add(Restrictions.in("role", roles));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<LabTest> getLatestLabTestByDate(Date today, Date nextDay, Lab lab) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("lab", lab));
		criteria.add(Restrictions.ge("acceptDate", new java.sql.Date(today.getTime())));
		criteria.add(Restrictions.lt("acceptDate", new java.sql.Date(nextDay.getTime())));
		return criteria.list();
	}
	
	public void deleteLabTest(LabTest labtest) throws DAOException {
		sessionFactory.getCurrentSession().delete(labtest);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LabTest> getLatestLabTestByDateOnly(Date date) throws DAOException, ParseException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = dateFormat.format(date) + " 00:00:00";
		String endDate = dateFormat.format(date) + " 23:59:59";
		
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		criteria.add(
		    Restrictions.between("acceptDate", dateTimeFormatter.parse(startDate), dateTimeFormatter.parse(endDate)));
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LabTest> getLaboratoryTestsByDateAndPatient(Date date, Patient patient) throws DAOException, ParseException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		Criteria orderCriteria = criteria.createCriteria("order");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = sdf.format(date) + " 00:00:00";
		String endDate = sdf.format(date) + " 23:59:59";
		
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		orderCriteria.add(
		    Restrictions.between("dateStopped", dateTimeFormatter.parse(startDate), dateTimeFormatter.parse(endDate)));
		criteria.add(eq("patient", patient));
		return criteria.list();
	}
	
	@Override
	public List<Order> getOrders(Patient patient, Date date, Concept concept) throws DAOException, ParseException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Order.class);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = sdf.format(date) + " 00:00:00";
		String endDate = sdf.format(date) + " 23:59:59";
		
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		criteria.add(
		    Restrictions.between("dateActivated", dateTimeFormatter.parse(startDate), dateTimeFormatter.parse(endDate)));
		criteria.add(eq("patient", patient));
		criteria.add(eq("concept", concept));
		return criteria.list();
	}
	
	@Override
	public LabTest getLaboratoryTest(Encounter encounter) throws APIException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("encounter", encounter));
		return (LabTest) criteria.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LabTest> getAllLabTests() throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LabTest> getLaboratoryTestsByDiscontinuedDate(Date date, Set<Concept> tests, List<Patient> patients)
	        throws DAOException, ParseException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(Restrictions.in("concept", tests));
		if (!CollectionUtils.isEmpty(patients)) {
			criteria.add(Restrictions.in("patient", patients));
		}
		Criteria orderCriteria = criteria.createCriteria("order");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = sdf.format(date) + " 00:00:00";
		String endDate = sdf.format(date) + " 23:59:59";
		
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		orderCriteria.add(
		    Restrictions.between("dateStopped", dateTimeFormatter.parse(startDate), dateTimeFormatter.parse(endDate)));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<LabTest> getLaboratoryTestsByPatient(Patient patient) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("patient", patient));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LabTest> getAllCompletedLabTestOrders(@NotNull Patient patient) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		//only completed orders
		criteria.add(eq("status", BotswanaEmrConstants.labStatus.COMPLETED.name()).ignoreCase());
		criteria.add(eq("patient", patient));
		
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Concept> searchConceptsByNameAndClass(String text, ConceptClass clazz) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Concept.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("retired", false));
		criteria.add(Restrictions.eq("conceptClass", clazz));
		if (StringUtils.isNotBlank(text)) {
			criteria.createAlias("names", "names");
			criteria.add(Restrictions.like("names.name", text, MatchMode.ANYWHERE));
		}
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Concept> searchConceptsByNameAndClass(String text, ConceptClass clazz, ConceptClass clazz2)
	        throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Concept.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("retired", false));
		criteria.add(Restrictions.or(Restrictions.eq("conceptClass", clazz), Restrictions.eq("conceptClass", clazz2)));
		if (StringUtils.isNotBlank(text)) {
			criteria.createAlias("names", "names");
			criteria.add(Restrictions.like("names.name", text, MatchMode.ANYWHERE));
		}
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LabTest> getLaboratoryTestListByEncounter(Encounter encounter) throws DAOException {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabTest.class);
		criteria.add(eq("encounter", encounter));
		return criteria.list();
	}
}
