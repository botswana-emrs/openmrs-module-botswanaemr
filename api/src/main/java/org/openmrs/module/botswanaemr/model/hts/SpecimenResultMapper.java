/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;

public class SpecimenResultMapper {
	
	public static List<SimplifiedHtsProficiencyTestingTest> simplifiedHtsProficiencyTestingTest(
	        HtsProficiencyTesting htsProficiencyTesting) {
		List<SimplifiedHtsProficiencyTestingTest> resultsList = new ArrayList<>();
		
		return resultsList;
	}
	
	public static List<SimplifiedHtsProficiencyTestingTest> getSimplifiedProficiencyTestingTests(
	        List<HtsProficiencyTestingTest> htsProficiencyTestingTests) {
		List<SimplifiedHtsProficiencyTestingTest> simplifiedHtsProficiencyTestingTests = new ArrayList<>();
		for (HtsProficiencyTestingTest htsProficiencyTestingTest : htsProficiencyTestingTests) {
			SimplifiedHtsProficiencyTestingTest simplifiedHtsProficiencyTestingTest = getSimpleHtsProficiencyTestingTest(
			    htsProficiencyTestingTest);
			
			List<SimplifiedHtsSpecimenResult> specimenResultList = simplifySpecimenResults(
			    Collections.singletonList(htsProficiencyTestingTest));
			simplifiedHtsProficiencyTestingTest.getSpecimenResults().addAll(specimenResultList);
			
			simplifiedHtsProficiencyTestingTests.add(simplifiedHtsProficiencyTestingTest);
		}
		return simplifiedHtsProficiencyTestingTests;
	}
	
	public static List<SimplifiedHtsSpecimenResult> simplifySpecimenResults(
	        List<HtsProficiencyTestingTest> proficiencyTestingTests) {
		List<SimplifiedHtsSpecimenResult> resultList = new ArrayList<>();
		for (HtsProficiencyTestingTest proficiencyTestingTest : proficiencyTestingTests) {
			List<HtsSpecimenResult> specimenResultList = proficiencyTestingTest.getSpecimenResults().stream()
			        .sorted(Comparator.comparing(HtsSpecimenResult::getId)).collect(Collectors.toList());
			for (HtsSpecimenResult specimenResult : specimenResultList) {
				SimplifiedHtsSpecimenResult simplifiedHtsSpecimenResult = getSimplifiedHtsSpecimenResult(specimenResult,
				    proficiencyTestingTest);
				
				resultList.add(simplifiedHtsSpecimenResult);
			}
		}
		return resultList;
	}
	
	public static SimplifiedHtsProficiencyTestingTest getSimpleHtsProficiencyTestingTest(
	        HtsProficiencyTestingTest htsProficiencyTestingTest) {
		SimplifiedHtsProficiencyTestingTest simplifiedHtsProficiencyTestingTest = new SimplifiedHtsProficiencyTestingTest();
		simplifiedHtsProficiencyTestingTest.setProficiencyTestingTestId(htsProficiencyTestingTest.getId());
		simplifiedHtsProficiencyTestingTest.setId(htsProficiencyTestingTest.getId());
		simplifiedHtsProficiencyTestingTest.setSpecimenId(htsProficiencyTestingTest.getSpecimenId());
		simplifiedHtsProficiencyTestingTest.setLotNumber(htsProficiencyTestingTest.getLotNumber());
		if (htsProficiencyTestingTest.getExpiryDate() != null) {
			simplifiedHtsProficiencyTestingTest.setExpiryDate(
			    BotswanaEmrUtils.formatDateWithoutTime(htsProficiencyTestingTest.getExpiryDate(), "yyyy-MM-dd"));
		}
		simplifiedHtsProficiencyTestingTest.setConditionOfSpecimen(htsProficiencyTestingTest.getConditionOfSpecimen());
		simplifiedHtsProficiencyTestingTest
		        .setReasonForUnacceptableSpecimen(htsProficiencyTestingTest.getReasonForUnacceptableSpecimen());
		simplifiedHtsProficiencyTestingTest.setCorrectiveActions(htsProficiencyTestingTest.getCorrectiveActions());
		return simplifiedHtsProficiencyTestingTest;
	}
	
	private static SimplifiedHtsSpecimenResult getSimplifiedHtsSpecimenResult(HtsSpecimenResult specimenResult,
	        HtsProficiencyTestingTest proficiencyTestingTest) {
		SimplifiedHtsSpecimenResult simplifiedHtsSpecimenResult = new SimplifiedHtsSpecimenResult();
		simplifiedHtsSpecimenResult.setComment(specimenResult.getComment());
		simplifiedHtsSpecimenResult.setInterpretation(specimenResult.getInterpretation());
		simplifiedHtsSpecimenResult.setId(specimenResult.getId());
		simplifiedHtsSpecimenResult.setProficiencyTestTestId(proficiencyTestingTest.getId());
		simplifiedHtsSpecimenResult.setSpecimenName(proficiencyTestingTest.getSpecimenId());
		if (specimenResult.getStockroom() != null) {
			simplifiedHtsSpecimenResult.setStockRoom(specimenResult.getStockroom().getName());
			simplifiedHtsSpecimenResult.setStockRoomId(specimenResult.getStockroom().getUuid());
		}
		
		for (HtsTestResult testResult : specimenResult.getResults()) {
			SimplifiedHtsTestResult simplifiedHtsTestResult = new SimplifiedHtsTestResult();
			simplifiedHtsTestResult.setKitResults(testResult.getKitResults());
			simplifiedHtsTestResult.setResultNumber(testResult.getResultNumber());
			simplifiedHtsTestResult.setExpiryDate(testResult.getExpiryDate());
			simplifiedHtsTestResult.setKitName(testResult.getKitName());
			simplifiedHtsTestResult.setLotNumber(testResult.getLotNumber());
			simplifiedHtsTestResult.setId(testResult.getId());
			simplifiedHtsTestResult.setSpecimenResultId(specimenResult.getId());
			
			simplifiedHtsSpecimenResult.getResults().add(simplifiedHtsTestResult);
		}
		
		return simplifiedHtsSpecimenResult;
	}
}
