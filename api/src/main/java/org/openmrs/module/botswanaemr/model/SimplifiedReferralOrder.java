package org.openmrs.module.botswanaemr.model;

import lombok.Data;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.module.patientqueueing.model.PatientQueue;

@Data
public class SimplifiedReferralOrder {
	
	private Order order;
	
	private PatientQueue patientQueue;
}
