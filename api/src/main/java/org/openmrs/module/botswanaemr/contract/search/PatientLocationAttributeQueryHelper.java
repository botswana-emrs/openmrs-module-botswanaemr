/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

public class PatientLocationAttributeQueryHelper {
	
	public String appendToJoinClause(String join) {
		join += "INNER JOIN person_attribute pa on p.person_id = pa.person_id\n"
		        + "            AND pa.value = :locationUuidParam \n"
		        + "         INNER JOIN person_attribute_type pat ON pat.person_attribute_type_id = pa.person_attribute_type_id\n"
		        + "            AND pat.name ='LocationAttribute' \n";
		return join;
	}
}
