/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang.StringEscapeUtils;

public class WildCardParameter {
	
	private final String[] parts;
	
	public WildCardParameter(String[] parts) {
		this.parts = parts;
	}
	
	public static WildCardParameter create(String value) {
		if (value == null || "".equals(value)) {
			return new WildCardParameter(new String[0]);
		}
		String[] splitName = value.split(" ");
		for (int i = 0; i < splitName.length; i++) {
			splitName[i] = "%" + StringEscapeUtils.escapeSql(splitName[i]) + "%";
		}
		return new WildCardParameter(splitName);
	}
	
	public String[] getParts() {
		return parts;
	}
	
	public boolean isEmpty() {
		return parts.length == 0;
	}
}
