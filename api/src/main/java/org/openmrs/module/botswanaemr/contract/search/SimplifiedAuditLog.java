/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.openmrs.api.context.Context;
import org.openmrs.module.auditlog.AuditLog;
import org.openmrs.module.auditlog.util.AuditLogConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;

import java.io.Serializable;

@Data
public class SimplifiedAuditLog {
	
	private static final String DAEMON_USER_UUID = "A4F30A1B-5EB9-11DF-A648-37A07F9C90FB";
	
	private Integer auditLogId;
	
	private String classname;
	
	private String simpleClassname;
	
	private Serializable identifier;
	
	private String action;
	
	private String userDetails = "";
	
	private String dateCreatedString;
	
	public SimplifiedAuditLog() {
	}
	
	public SimplifiedAuditLog(AuditLog auditLog) throws ClassNotFoundException {
		auditLogId = auditLog.getAuditLogId();
		classname = auditLog.getType();
		simpleClassname = Class.forName(auditLog.getType()).getSimpleName();
		//If it is a nested class, use the simple name of the nested class
		if (simpleClassname.indexOf("$") > -1) {
			simpleClassname = simpleClassname.substring(simpleClassname.indexOf("$") + 1);
		}
		identifier = auditLog.getIdentifier();
		action = auditLog.getAction().toString();
		if (auditLog.getUser() != null) {
			if (auditLog.getUser().getUuid().equals(DAEMON_USER_UUID)) {
				userDetails = Context.getMessageSourceService().getMessage(AuditLogConstants.MODULE_ID + ".systemChange");
			} else {
				if (auditLog.getUser().getPersonName() != null) {
					userDetails = auditLog.getUser().getPersonName().getFullName();
				}
				if (StringUtils.isNotBlank(auditLog.getUser().getUsername())) {
					userDetails = userDetails + "(" + auditLog.getUser().getUsername() + ")";
				}
			}
		}
		
		dateCreatedString = BotswanaEmrUtils.formatDateWithoutTime(auditLog.getDateCreated(), "dd-MM-yyyy HH:mm");
	}
}
