/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.ToString;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;

@Data
@Entity
@Table(name = "hts_proficiency_testing_test")
public class HtsProficiencyTestingTest extends BaseOpenmrsObject {
	
	@Column(name = "specimen_id")
	private String specimenId;
	
	@Column(name = "lot_number")
	private String lotNumber;
	
	@Column(name = "expiry_date")
	private Date expiryDate;
	
	@Column(name = "condition_of_specimen")
	private String conditionOfSpecimen;
	
	@Column(name = "reason_for_unacceptable_specimen")
	private String reasonForUnacceptableSpecimen;
	
	@Column(name = "corrective_actions")
	private String correctiveActions;
	
	@OneToMany(mappedBy = "htsProficiencyTestingTest", cascade = { CascadeType.ALL })
	@ToString.Exclude
	private Set<HtsSpecimenResult> specimenResults = new HashSet<>();;
	
	public HtsProficiencyTesting getHtsProficiencyTesting() {
		return htsProficiencyTesting;
	}
	
	public void setHtsProficiencyTesting(HtsProficiencyTesting htsProficiencyTesting) {
		this.htsProficiencyTesting = htsProficiencyTesting;
	}
	
	@ManyToOne
	@JoinColumn(name = "pt_id")
	private HtsProficiencyTesting htsProficiencyTesting;
	
	@ManyToOne
	@JoinColumn(name = "stock_room_id")
	private Stockroom stockroom;
	
	@Id
	@GeneratedValue
	@Column(name = "pt_test_id")
	private Integer id;
	
	@Override
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 41;
	}
}
