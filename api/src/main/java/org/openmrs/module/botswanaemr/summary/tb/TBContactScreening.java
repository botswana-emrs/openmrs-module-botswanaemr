/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;

import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@NoArgsConstructor
public class TBContactScreening extends BaseSummary implements Summary, Serializable {
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		return null;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return null;
	}
}
