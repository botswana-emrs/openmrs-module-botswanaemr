/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract;

import org.openmrs.Person;
import org.openmrs.PersonAddress;
import org.openmrs.PersonAttributeType;
import org.openmrs.Relationship;
import org.openmrs.api.context.Context;
import org.openmrs.attribute.AttributeType;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

public class NokMapper {
	
	public static PatientNok mapToResponse(Relationship relationship, Person person) {
		PatientNok patientNok = new PatientNok();
		Person nextOfKinN;
		
		PatientNok.NokPerson nokPerson = new PatientNok.NokPerson();
		nokPerson.setName(String.format("%s %s %s", person.getFamilyName(), person.getMiddleName(), person.getFamilyName()));
		nokPerson.setUuid(person.getUuid());
		patientNok.setPersonA(nokPerson);
		
		if (relationship.getPersonA() == person) {
			nokPerson = new PatientNok.NokPerson();
			nokPerson.setName(String.format("%s %s %s", relationship.getPersonB().getFamilyName(),
			    relationship.getPersonB().getMiddleName(), relationship.getPersonB().getFamilyName()));
			nokPerson.setUuid(relationship.getPersonB().getUuid());
			patientNok.setPersonB(nokPerson);
			nextOfKinN = relationship.getPersonB();
		} else {
			nokPerson = new PatientNok.NokPerson();
			nokPerson.setName(String.format("%s %s %s", relationship.getPersonA().getFamilyName(),
			    relationship.getPersonA().getMiddleName(), relationship.getPersonA().getFamilyName()));
			nokPerson.setUuid(relationship.getPersonA().getUuid());
			patientNok.setPersonB(nokPerson);
			nextOfKinN = relationship.getPersonA();
		}
		patientNok.setIdType(getPersonAttribute(nextOfKinN, NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID));
		patientNok.setContactType(getPersonAttribute(nextOfKinN, NOK_CONTACT_TYPE_TYPE_UUID));
		patientNok.setIdNumber(getPersonAttribute(nextOfKinN, NOK_ID_ATTRIBUTE_TYPE_UUID));
		patientNok.setGivenName(nextOfKinN.getGivenName());
		patientNok.setMiddleName(nextOfKinN.getMiddleName());
		patientNok.setFamilyName(nextOfKinN.getFamilyName());
		patientNok.setRelationShip(getPersonAttribute(nextOfKinN, NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID));
		patientNok.setEmail(getPersonAttribute(nextOfKinN, NOK_EMAIL_ATTRIBUTE_TYPE_UUID));
		patientNok.setContact1(getPersonAttribute(nextOfKinN, NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID));
		patientNok.setContact2(getPersonAttribute(nextOfKinN, NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID));
		patientNok.setContact3(getPersonAttribute(nextOfKinN, NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID));
		patientNok.setContact4(getPersonAttribute(nextOfKinN, NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID));
		patientNok.setContact5(getPersonAttribute(nextOfKinN, NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID));
		PersonAddress personAddress = nextOfKinN.getPersonAddress();
		if (personAddress != null) {
			patientNok.setDistrict(personAddress.getAddress2());
			patientNok.setCity(personAddress.getCityVillage());
		}
		
		return patientNok;
	}
	
	private static PatientNok.AttributeObject getPersonAttribute(Person person, String attributeTypeUuid) {
		PersonAttributeType attributeType = Context.getPersonService().getPersonAttributeTypeByUuid(attributeTypeUuid);
		if (attributeType != null && person.getAttribute(attributeType) != null) {
			PatientNok.AttributeObject attributeObject = new PatientNok.AttributeObject();
			attributeObject.setValue(person.getAttribute(attributeType).getValue());
			attributeObject.setUuid(person.getAttribute(attributeType).getUuid());
			return attributeObject;
		}
		return new PatientNok.AttributeObject();
	}
}
