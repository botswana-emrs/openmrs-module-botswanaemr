/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.APIException;
import org.openmrs.api.OpenmrsService;
import org.openmrs.module.botswanaemr.lab.Lab;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Transactional
public interface LabService extends OpenmrsService {
	
	/**
	 * LAB Objects fetch and insertion in DB
	 */
	public Lab saveLab(Lab lab) throws APIException;
	
	public Lab getLabByName(String name) throws APIException;
	
	public List<Lab> getAllLab() throws APIException;
	
	public List<Lab> getAllActiveLabs() throws APIException;
	
	public Lab getLabByRole(Role role) throws APIException;
	
	public List<Lab> getLabByRoles(List<Role> roles) throws APIException;
	
	public Lab getLabById(Integer labId) throws APIException;
	
	public void deleteLab(Lab lab) throws APIException;
	
	/**
	 * LAB TEST
	 */
	
	public LabTest saveLabTest(LabTest labTest) throws APIException;
	
	public LabTest getLabTestById(Integer labTestId) throws APIException;
	
	public LabTest getLabTestByOrder(Order order) throws APIException;
	
	public LabTest getLabTestBySampleNumber(String sampleNumber) throws APIException;
	
	public List<LabTest> getLatestLabTestByLabAndDate(Lab lab, Date date) throws APIException;
	
	public List<LabTest> getLatestLabTestByDateOnly(Date date) throws APIException, ParseException;
	
	public String getNextSampleNumber(Lab lab, Date date) throws APIException;
	
	public void deleteLabTest(LabTest labtest) throws APIException;
	
	public void deleteLabTestByOrder(Order order) throws APIException;
	
	/**
	 * Orders
	 */
	
	public List<LabTest> getCompletedLaboratoryTests(Date date, String phrase, Set<Concept> allowableTests)
	        throws APIException, ParseException;
	
	public List<LabTest> getLaboratoryTestsByDateAndPatientOnGivenDay(Date date, Patient patient)
	        throws APIException, ParseException;
	
	public List<Order> getOrders(Patient patient, Date date, Concept concept) throws APIException, ParseException;
	
	public LabTest getLaboratoryTest(Encounter encounter) throws APIException;
	
	public List<LabTest> getAllLabTests() throws APIException;
	
	public void saveOrderAndLabTest(Set<Concept> savedSetOrders, Encounter encounter) throws APIException;
	
	List<LabTest> getLaboratoryTestsByPatient(Patient patient) throws APIException;
	
	List<LabTest> getAllCompletedLabTestOrders(@NotNull Patient patient);
	
	public List<Concept> searchLabTest(String text) throws APIException;
	
	public List<LabTest> getLaboratoryTestListByEncounter(Encounter encounter) throws APIException;
}
