/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.openmrs.BaseOpenmrsObject;
import org.openmrs.api.OpenmrsService;
import org.openmrs.module.botswanaemr.model.hts.DtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface HtsProficiencyTestingService extends OpenmrsService {
	
	// Lab Quality Control
	List<HtsProficiencyTesting> getList();
	
	HtsProficiencyTesting getHtsProficiencyTesting(int id);
	
	BaseOpenmrsObject getById(Serializable id, String className);
	
	void saveHtsProficiencyTesting(HtsProficiencyTesting ProficiencyTesting);
	
	void updateHtsProficiencyTesting(HtsProficiencyTesting ProficiencyTesting);
	
	void voidHtsProficiencyTesting(HtsProficiencyTesting ProficiencyTesting);
	
	// Lab Quality Control Test
	List<HtsProficiencyTestingTest> getHtsProficiencyTestingTests(HtsProficiencyTesting ProficiencyTesting);
	
	HtsProficiencyTestingTest saveOrUpdateHtsProficiencyTestingTest(HtsProficiencyTestingTest ProficiencyTestingTest);
	
	void voidHtsProficiencyTestingTest(HtsProficiencyTestingTest ProficiencyTestingTest);
	
	void addHtsProficiencyTestingTests(Set<HtsProficiencyTestingTest> ProficiencyTestingTestSet);
	
	List<DtsProficiencyTestingBuffer> getDtsProficiencyTestingBuffer(HtsProficiencyTesting proficiencyTesting);
	
	DtsProficiencyTestingBuffer saveOrUpdateDtsProficiencyTestingBuffer(
	        DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer);
	
	void voidDtsProficiencyTestingBuffer(DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer);
	
	void addDtsProficiencyTestingBuffer(Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffers);
}
