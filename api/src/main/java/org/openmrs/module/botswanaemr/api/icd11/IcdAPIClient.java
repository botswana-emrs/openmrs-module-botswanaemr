/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.icd11;

import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openmrs.module.botswanaemr.contract.Icd11ResponseObject;

public class IcdAPIClient {
	
	private static IcdAPIClient icdApiClient;
	
	private final OkHttpClient client;
	
	@Setter
	@Getter
	private String url;
	
	public static IcdAPIClient getInstance() {
		if (icdApiClient == null) {
			icdApiClient = new IcdAPIClient();
		}
		
		return icdApiClient;
	}
	
	public IcdAPIClient() {
		client = new OkHttpClient().newBuilder().build();
	}
	
	public String getAccessToken() throws Exception {
		return getToken();
	}
	
	@Setter
	private String accessToken;
	
	public String getReleaseId() {
		return releaseId;
	}
	
	public void setReleaseId(String releaseId) {
		this.releaseId = releaseId;
	}
	
	private String releaseId;
	
	public static final String WHO_ICD_BASE_URL = "https://id.who.int/icd";
	
	private final String TOKEN_REQUEST_ENDPOINT = "https://icdaccessmanagement.who.int/connect/token";
	
	private final String CLIENT_ID = "3b5062e4-b33c-4160-beb0-c7f644efb8ea_c0dc37b1-291c-4d3e-b762-64405edd98ee";
	
	private final String CLIENT_SECRET = "ZQDcvb4s0LEABWEQQCTsg5C0cNGZsVGgxHvt7RR1usI=";
	
	private final String SCOPE = "icdapi_access";
	
	private final String GRANT_TYPE = "client_credentials";
	
	public static void main(String[] args) throws Exception {
		
		String uri = WHO_ICD_BASE_URL + "/entity";
		
		IcdAPIClient api = new IcdAPIClient();
		// String token = api.getToken();
		// System.out.println("URI Response JSON : \n" + api.getURI(token, uri));
		// parse JSON response
		// JSONObject jsonObj = new JSONObject(api.getURI(token, uri));
		// String releaseId = jsonObj.getString("releaseId");
		List<Icd11ResponseObject> jsonResponse = api.searchTerm("Malaria");
		// System.out.println(jsonResponse.size());
		
	}
	
	// get the OAUTH2 token
	private String getToken() throws Exception {
		
		// System.out.println("Getting token...");
		
		MediaType mediaType = MediaType.parse("text/plain");
		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("client_id", CLIENT_ID)
		        .addFormDataPart("client_secret", CLIENT_SECRET).addFormDataPart("scope", SCOPE)
		        .addFormDataPart("grant_type", GRANT_TYPE).build();
		Request request = new Request.Builder().url(TOKEN_REQUEST_ENDPOINT).method("POST", body).build();
		Response response = client.newCall(request).execute();
		
		ResponseBody responseBody = requireNonNull(response.body());
		InputStream outputStream = responseBody.byteStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(outputStream));
		String inputLine;
		StringBuilder responseBuffer = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			responseBuffer.append(inputLine);
		}
		in.close();
		
		// parse JSON response
		JSONObject jsonObj = new JSONObject(responseBuffer.toString());
		return jsonObj.getString("access_token");
	}
	
	// access ICD API
	private String getURI(String token, String uri) throws Exception {
		
		// System.out.println("Getting URI...");
		
		URL url = new URL(uri);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		
		// HTTP header fields to set
		con.setRequestProperty("Authorization", "Bearer " + token);
		con.setRequestProperty("Accept", "application/json");
		con.setRequestProperty("Accept-Language", "en");
		con.setRequestProperty("API-Version", "v2");
		
		// response
		int responseCode = con.getResponseCode();
		// System.out.println("URI Response Code : " + responseCode + "\n");
		
		if (responseCode == 401) {
			this.accessToken = getToken();
			getURI(this.accessToken, uri);
		}
		
		if (responseCode == 500) {
			throw new Exception("Error" + con.getResponseMessage());
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuilder response = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		return response.toString();
	}
	
	public List<Icd11ResponseObject> searchTerm(String term) throws Exception {
		if (this.accessToken == null) {
			this.setAccessToken(getToken());
		}
		
		JSONObject jsonObj = new JSONObject(this.getURI(this.accessToken, WHO_ICD_BASE_URL + "/entity"));
		if (this.releaseId == null) {
			this.releaseId = jsonObj.getString("releaseId");
		}
		
		MediaType mediaType = MediaType.parse("text/plain");
		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("flatResults", "true")
		        .addFormDataPart("includePostcoordination", "false").build();
		Request request = new Request.Builder()
		        .url(WHO_ICD_BASE_URL + String.format("/release/11/%s/mms/search?q=%s", this.releaseId, term + "%"))
		        .method("POST", body).addHeader("api-version", "v2").addHeader("Accept-Language", "en")
		        .addHeader("Authorization", "Bearer " + this.accessToken).build();
		Response response = client.newCall(request).execute();
		
		ResponseBody responseBody = requireNonNull(response.body());
		InputStream outputStream = responseBody.byteStream();
		BufferedReader in = new BufferedReader(new InputStreamReader(outputStream));
		String inputLine;
		StringBuilder responseBuffer = new StringBuilder();
		while ((inputLine = in.readLine()) != null) {
			responseBuffer.append(inputLine);
		}
		in.close();
		
		JSONObject jsonObject = new JSONObject(responseBuffer.toString());
		
		JSONArray jsonArray = jsonObject.getJSONArray("destinationEntities");
		
		List<Icd11ResponseObject> icd11ResponseObjectList = new ArrayList<>();
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
			Icd11ResponseObject icd11ResponseObject = new Icd11ResponseObject();
			icd11ResponseObject.setId(jsonObject1.getString("id"));
			icd11ResponseObject.setTitle(jsonObject1.getString("title"));
			icd11ResponseObject.setTheCode(jsonObject1.getString("theCode"));
			icd11ResponseObject.setPostcoordinationAvailability(jsonObject1.getString("postcoordinationAvailability"));
			icd11ResponseObject.setChapter(jsonObject1.getString("chapter"));
			icd11ResponseObject.setScore(jsonObject1.getString("score"));
			icd11ResponseObject.setTitleIsTopScore(jsonObject1.getString("titleIsTopScore"));
			icd11ResponseObjectList.add(icd11ResponseObject);
		}
		
		return icd11ResponseObjectList;
		
	}
}
