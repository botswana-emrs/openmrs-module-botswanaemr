/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.Data;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;

import java.util.Date;

@Data
public class SimplifiedItemStockDetail {
	
	private String itemName;
	
	private Date expiryDate;
	
	private Integer quantity;
	
	private Integer id;
	
	private String stockRoom;
	
	private String batchNumber;
	
	private Integer stockRoomId;
	
	public static SimplifiedItemStockDetail simplify(ItemStockDetail itemStockDetail) {
		SimplifiedItemStockDetail simplifiedItemStockDetail = new SimplifiedItemStockDetail();
		simplifiedItemStockDetail.setItemName(itemStockDetail.getItemStock().getItem().getName());
		
		simplifiedItemStockDetail.setExpiryDate(itemStockDetail.getExpiration());
		simplifiedItemStockDetail.setQuantity(itemStockDetail.getQuantity());
		StockOperation batchOperation = itemStockDetail.getBatchOperation();
		simplifiedItemStockDetail.setBatchNumber(batchOperation == null ? "" : batchOperation.getOperationNumber());
		simplifiedItemStockDetail.setId(itemStockDetail.getId());
		simplifiedItemStockDetail.setStockRoom(itemStockDetail.getStockroom().getName());
		
		return simplifiedItemStockDetail;
	}
}
