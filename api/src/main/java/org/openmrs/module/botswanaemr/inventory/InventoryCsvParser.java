/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.inventory;

import org.apache.commons.lang.StringUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.initializer.Domain;
import org.openmrs.module.initializer.api.BaseLineProcessor;
import org.openmrs.module.initializer.api.CsvLine;
import org.openmrs.module.initializer.api.CsvParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InventoryCsvParser extends CsvParser<Item, BaseLineProcessor<Item>> {
	
	private IItemDataService itemDataService;
	
	@Autowired
	public InventoryCsvParser(InventoryLineProcessor lineProcessor) {
		super(lineProcessor);
		itemDataService = BotswanaInventoryContext.getItemDataService();
	}
	
	@Override
	protected boolean shouldFill(Item instance, CsvLine csvLine) {
		return super.shouldFill(instance, csvLine);
	}
	
	@Override
	public Item bootstrap(CsvLine csvLine) throws IllegalArgumentException {
		String uuid = csvLine.getUuid();
		String code = getItemCode(csvLine);
		
		Item item;
		item = itemDataService.getByUuid(uuid);
		
		if (item == null && !StringUtils.isEmpty(code)) {
			item = itemDataService.getItemByCode(code);
		}
		
		if (item == null) {
			item = new Item();
			if (!StringUtils.isEmpty(uuid)) {
				item.setUuid(uuid);
				item.setHasExpiration(true);
			}
		}
		
		return item;
	}
	
	@Override
	public Item save(Item item) {
		return itemDataService.save(item);
	}
	
	@Override
	public Domain getDomain() {
		return null;
	}
	
	private String getItemCode(CsvLine line) {
		return line.get(InventoryConstants.HEADER_CODE, true);
	}
	
}
