/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.regimen;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Concept;
import org.openmrs.User;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "bt_regimen_component")
@BatchSize(size = 25)
public class RegimenComponent extends BaseOpenmrsData {
	
	@Id
	@GeneratedValue
	@Column(name = "regimen_component_id")
	private Integer regimenComponentId;
	
	@Column(name = "component_label")
	private String componentLabel;
	
	@ManyToOne
	@JoinColumn(name = "regimen_id")
	private Regimen regimen;
	
	@OneToMany(mappedBy = "regimenComponent")
	@ToString.Exclude
	private List<RegimenComponentDrug> regimenComponentDrugs;
	
	@Column(name = "uuid", unique = true, length = 38)
	private String uuid;
	
	@Column(name = "date_created", nullable = false)
	private Date dateCreated;
	
	@Column(name = "date_changed")
	private Date dateChanged;
	
	@Column(name = "date_voided")
	private Date dateVoided;
	
	@Column(name = "voided", nullable = false)
	private Boolean voided;
	
	@Column(name = "void_reason", length = 255)
	private String voidReason;
	
	@ManyToOne
	@JoinColumn(name = "creator_id")
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "changed_by")
	private User changedBy;
	
	@ManyToOne
	@JoinColumn(name = "voided_by")
	private User voidedBy;
	
	public RegimenComponent(Regimen regimen, String regimenComponentLabel,
	    List<RegimenComponentDrug> regimenComponentDrugs) {
		this.regimen = regimen;
		this.componentLabel = regimenComponentLabel;
		this.regimenComponentDrugs = regimenComponentDrugs;
	}
	
	/**
	 * Checks if this component is complete
	 * 
	 * @return true if complete
	 */
	public boolean isComplete() {
		return regimen != null && componentLabel != null;
	}
	
	@Override
	public Integer getId() {
		return getRegimenComponentId();
	}
	
	@Override
	public void setId(Integer id) {
		setRegimenComponentId(id);
	}
}
