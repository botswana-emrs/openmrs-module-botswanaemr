/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;

public class PatientHtsTestingPointsQueryHelper {
	
	private static final String HTS_TESTING_SITE_CONCEPT_UUID = "41d2d3ca-bf7b-4689-8e87-08fe9270d18b";
	
	private String testingLocation;
	
	public PatientHtsTestingPointsQueryHelper(String testingLocation) {
		this.testingLocation = testingLocation;
	}
	
	public String appendToJoinClause(String join) {
		if (StringUtils.isEmpty(this.testingLocation)) {
			return join;
		}
		
		String encounterString = join
		        + (" INNER JOIN encounter e on e.patient_id = p.person_id\n" + "            AND e.voided=0\n"
		                + "            AND e.encounter_type = SELECT encounter_type_id FROM encounter_type "
		                + " WHERE e.uuid IN ('" + BotswanaEmrConstants.HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID + "','"
		                + BotswanaEmrConstants.HTS_PRIOR_TEST_ENCOUNTER_TYPE_UUID + "','"
		                + BotswanaEmrConstants.HTS_VERIFICATION_ENCOUNTER_TYPE_UUID + "')\n");
		
		return encounterString + (" INNER JOIN obs o on o.person_id = p.person_id\n"
		        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
		        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
		        + HTS_TESTING_SITE_CONCEPT_UUID + "'" + "            AND o.value_text='" + this.testingLocation + "') \n");
	}
	
}
