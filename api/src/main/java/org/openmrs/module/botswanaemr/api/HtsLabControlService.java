/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import java.util.List;
import java.util.Set;

import org.openmrs.Location;
import org.openmrs.api.OpenmrsService;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface HtsLabControlService extends OpenmrsService {
	
	// Lab Quality Control
	List<LabQualityControl> getList();

	List<LabQualityControl> getList(Location location);
	
	LabQualityControl getLabQualityControl(int id);
	
	LabQualityControlTest getLabQualityControlTest(int id);
	
	void saveLabQualityControl(LabQualityControl labQualityControl);
	
	void updateLabQualityControl(LabQualityControl labQualityControl);
	
	void voidLabQualityControl(LabQualityControl labQualityControl);
	
	// Lab Quality Control Test
	List<LabQualityControlTest> getLabQualityControlTests(LabQualityControl labQualityControl);
	
	LabQualityControlTest saveOrUpdateLabQualityControlTest(LabQualityControlTest labQualityControlTest);
	
	void voidLabQualityControlTest(LabQualityControlTest labQualityControlTest);
	
	void addLabQualityControlTests(Set<LabQualityControlTest> labQualityControlTestSet);
	
	List<LabQualityControl> getLabQualityControlsThisWeekByTestingPoints(List<String> testingPoints);
}
