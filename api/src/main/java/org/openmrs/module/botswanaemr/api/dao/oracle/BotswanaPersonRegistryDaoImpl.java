/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.oracle;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.pool.OracleDataSource;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;
import org.openmrs.module.botswanaemr.api.dao.BotswanaPersonRegistryDao;

import java.sql.*;
import java.time.ZoneId;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

public class BotswanaPersonRegistryDaoImpl implements BotswanaPersonRegistryDao {
	
	// TODO: Move these parameters to a global configuration
	final static String DB_WALLET_LOCATION = "/opt/Wallet_DBPerson";
	
	final static String DB_URL = "jdbc:oracle:thin:@dbperson_medium?TNS_ADMIN=" + DB_WALLET_LOCATION;
	
	final static String DB_USER = "ADMIN";
	
	final static String DB_PASSWORD = "ETk7Lc8@Fgn55h@";
	
	final static String CONN_FACTORY_CLASS_NAME = "oracle.jdbc.pool.OracleDataSource";
	
	private static OracleConnection oracleConnection;
	
	public static OracleConnection getConnection() throws SQLException {
		if (oracleConnection == null) {
			Properties info = new Properties();
			info.put(OracleConnection.CONNECTION_PROPERTY_USER_NAME, DB_USER);
			info.put(OracleConnection.CONNECTION_PROPERTY_PASSWORD, DB_PASSWORD);
			info.put(OracleConnection.CONNECTION_PROPERTY_DEFAULT_ROW_PREFETCH, "20");
			
			OracleDataSource ods = new OracleDataSource();
			ods.setURL(DB_URL);
			ods.setConnectionProperties(info);
			
			// With AutoCloseable, the connection is closed automatically.
			oracleConnection = (OracleConnection) ods.getConnection();
			// Get the JDBC driver name and version
			DatabaseMetaData dbmd = oracleConnection.getMetaData();
			//System.out.println("Driver Name: " + dbmd.getDriverName());
			//System.out.println("Driver Version: " + dbmd.getDriverVersion());
			// Print some connection properties
			// System.out.println("Default Row Prefetch Value is: " +
			//		oracleConnection.getDefaultRowPrefetch());
			//System.out.println("Database Username is: " + oracleConnection.getUserName());
			//System.out.println();
			
		}
		
		return oracleConnection;
	}
	
	@Override
	public List<BotswanaRegistryPerson> getPersonsById(String personId) throws SQLException, ParseException {
		OracleConnection oracleConnection = getConnection();
		
		PreparedStatement statement = oracleConnection
		        .prepareStatement("SELECT ID_NUMBER, FULLNAME, DOB, GENDER FROM PERSON WHERE ID_NUMBER = ?");
		statement.setString(1, personId);
		ResultSet resultSet = statement.executeQuery();
		
		BotswanaRegistryPerson person = new BotswanaRegistryPerson();
		while (resultSet.next()) {
			// System.out.println(resultSet.getString(1) + " "
			// 		+ resultSet.getString(3) + " ");
			
			Date dob = getDOB(resultSet.getString(3));
			Integer age = calculateAge(dob, new Date());
			person.setAge(age);
			person.setIdNumber(resultSet.getString(1));
			person.setFullName(resultSet.getString(2));
			person.setDateOfBirth(dob);
			person.setGender(resultSet.getString(4));
			
		}
		if (person.getIdNumber() != null) {
			return Collections.singletonList(person);
		}
		
		throw new SQLDataException("Record not found");
	}
	
	private Integer calculateAge(Date birthDate, Date currentDate) {
		if ((birthDate != null) && (currentDate != null)) {
			int ageInYears = Period.between(getLocalDate(birthDate), getLocalDate(currentDate)).getYears();
			return ageInYears;
		}
		
		return 0;
	}
	
	private LocalDate getLocalDate(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	private Date getDOB(String strDob) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dob = format.parse(strDob);
		return dob;
	}
	
}
