/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.reporting.builder;

import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.reporting.cohort.definition.TodaysRegistrationCohortDefinition;
import org.openmrs.module.botswanaemr.reporting.cohort.definition.TotalRegisteredCohortDefinition;
import org.openmrs.module.reporting.cohort.EvaluatedCohort;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.module.reporting.evaluation.EvaluationContext;
import org.openmrs.module.reporting.evaluation.EvaluationException;

public class PatientDashBoardCohorts {
	
	public static EvaluatedCohort todaysRegistrations(EvaluationContext context) {
		try {
			return getService().evaluate(new TodaysRegistrationCohortDefinition(), context);
		}
		catch (EvaluationException e) {
			throw new IllegalStateException("Error evaluating Patient registered for today", e);
		}
	}
	
	public static EvaluatedCohort totalRegistrations(EvaluationContext context) {
		try {
			return getService().evaluate(new TotalRegisteredCohortDefinition(), context);
		}
		catch (EvaluationException e) {
			throw new IllegalStateException("Error evaluating Patient registered for today", e);
		}
	}
	
	private static CohortDefinitionService getService() {
		return Context.getService(CohortDefinitionService.class);
	}
}
