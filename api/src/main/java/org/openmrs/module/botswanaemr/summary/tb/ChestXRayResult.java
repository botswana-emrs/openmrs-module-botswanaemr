/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.summary.Summary;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.obs.ComplexData;
import org.openmrs.obs.ComplexObsHandler;
import org.openmrs.util.OpenmrsConstants;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import static org.openmrs.obs.ComplexObsHandler.HTML_VIEW;
import static org.openmrs.obs.ComplexObsHandler.URI_VIEW;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@NoArgsConstructor
public class ChestXRayResult extends BaseSummary implements Summary, Serializable {
	
	private static final String CHEST_NUMBER_CONCEPT = "a0f5e806-9bca-4069-8ea2-2b2a306a6393";
	
	private static final String CXR_FINDINGS_CONCEPT = "160689AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String X_RAY_DATE_CONCEPT = "162078AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String X_RAY_IMAGE_CONCEPT = "12cece22-e144-4ae7-888d-1997d26433d1";
	
	private Encounter encounter;
	
	private Double chestNumber;
	
	private Object xrayImage;
	
	private String cxrFindings;
	
	private String date;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		ChestXRayResult chestXRayResult = new ChestXRayResult();
		chestXRayResult.setEncounter(encounter);
		observations.forEach(obs -> {
			if (obs.getConcept().getUuid().equals(CHEST_NUMBER_CONCEPT)) {
				chestXRayResult.setChestNumber(getValueNumeric(obs));
			} else if (obs.getConcept().getUuid().equals(X_RAY_DATE_CONCEPT)) {
				chestXRayResult.setDate(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(X_RAY_IMAGE_CONCEPT)) {
				ComplexObsHandler obsHandler = Context.getObsService().getHandler(obs);
				if (obsHandler != null) {
					Obs complexObs = obsHandler.getObs(obs, URI_VIEW);
					if (complexObs != null) {
						chestXRayResult.setXrayImage(complexObs.getComplexData().getData());
					}
				}
			} else if (obs.getConcept().getUuid().equals(CXR_FINDINGS_CONCEPT)) {
				chestXRayResult.setCxrFindings(getValueText(obs));
			} else {
				log.debug("No available Culture & DST Report for this encounter");
			}
			
		});
		
		return chestXRayResult;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return Arrays.asList(getConcept(CHEST_NUMBER_CONCEPT), getConcept(CXR_FINDINGS_CONCEPT),
		    getConcept(X_RAY_DATE_CONCEPT), getConcept(X_RAY_IMAGE_CONCEPT));
	}
}
