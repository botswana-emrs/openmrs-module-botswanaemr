/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.NoArgsConstructor;
import lombok.ToString;
import org.openmrs.Order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@ToString
@NoArgsConstructor
@Entity
@Table(name = "drug_order_extensions")
public class DrugOrderExtension {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "drug_order_extension_id")
	private Integer drugOrderExtensionId;
	
	@OneToOne
	@JoinColumn(name = "order", nullable = false)
	private Order order;
	
	//Stringify json object
	@Column(name = "dosing_instructions", nullable = false)
	private String dosingInstructions;
	
	public Integer getDrugOrderExtensionId() {
		return drugOrderExtensionId;
	}
	
	public void setDrugOrderExtensionId(Integer drugOrderExtensionId) {
		this.drugOrderExtensionId = drugOrderExtensionId;
	}
	
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public String getDosingInstructions() {
		return dosingInstructions;
	}
	
	public void setDosingInstructions(String dosingInstructions) {
		this.dosingInstructions = dosingInstructions;
	}
	
}
