/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Meta;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Task;
import org.openmrs.Location;
import org.openmrs.LocationAttribute;
import org.openmrs.LocationAttributeType;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.AdvancedSearchRegistryPerson;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistryService;
import org.openmrs.module.botswanaemr.api.dao.BotswanaPersonRegistryDao;
import org.openmrs.module.botswanaemr.api.dao.oracle.BotswanaPersonRegistryDaoImpl;
import org.openmrs.module.botswanaemr.model.RegistrySource;
import org.openmrs.module.botswanaemr.utils.FhirUtil;

import org.openmrs.module.fhir2.FhirConstants;
import org.openmrs.module.fhir2.api.FhirTaskService;
import org.openmrs.module.fhir2.api.translators.PatientTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import java.util.Set;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.MASTER_FACILITY_CODE_UUID;

@Component("botswanaemr.BotswanaPersonRegistryService")
public class BotswanaPersonRegistryServiceImpl implements BotswanaPersonRegistryService {
	
	private FhirUtil fhirUtil = FhirUtil.getInstance();
	
	private static Log log = LogFactory.getLog(BotswanaPersonRegistryServiceImpl.class);
	
	private BotswanaRegistryResponse botswanaRegistryResponse;
	
	@Autowired
	private PatientTranslator patientTranslator;
	
	@Autowired
	private FhirTaskService fhirTaskService;
	
	/**
	 * Passport: System http://moh.bw.org/ext/identifier/ppn // System- urn:ietf:rfc:3986 Birth
	 * Certificate: System - http://moh.bw.org/ext/identifier/bcn National Id: System -
	 * http://moh.bw.org/ext/identifier/omang
	 */
	public enum IdentifierSystem {
		
		PASSPORT(
		        Context.getAdministrationService().getGlobalProperty(Constants.BOTSWANAEMR_IDENTIFIER_SYSTEM_PASSPORT,
		            "http://moh.bw.org/ext/identifier/ppn")),
		BCN(
		        Context.getAdministrationService().getGlobalProperty(
		            Constants.BOTSWANAEMR_IDENTIFIER_SYSTEM_BIRTH_CERTIFICATE, "http://moh.bw.org/ext/identifier/bcn")),
		OMANG(
		        Context.getAdministrationService().getGlobalProperty(Constants.BOTSWANAEMR_IDENTIFIER_SYSTEM_OMANG,
		            "http://moh.bw.org/ext/identifier/omang"));
		
		private final String system;
		
		IdentifierSystem(String system) {
			this.system = system;
		}
		
		public String getSystem() {
			return system;
		}
		
		private static class Constants {
			
			public static final String BOTSWANAEMR_IDENTIFIER_SYSTEM_PASSPORT = "botswanaemr.identifier.system.passport";
			
			public static final String BOTSWANAEMR_IDENTIFIER_SYSTEM_BIRTH_CERTIFICATE = "botswanaemr.identifier.system.birth-certificate";
			
			public static final String BOTSWANAEMR_IDENTIFIER_SYSTEM_OMANG = "botswanaemr.identifier.system.omang";
		}
	}
	
	public static final String NATIONAL_ID = "nationalIdNumber";
	
	public static final String PASSPORT_NUMBER = "passportNumber";
	
	public static final String BIRTH_CERTIFICATE_NUMBER = "birthCertificateNumber";
	
	@Override
	public BotswanaRegistryResponse searchPerson(RegistrySource registrySource, String personIdNumber) {
		BotswanaRegistryResponse botswanaRegistryResponse = new BotswanaRegistryResponse();
		BotswanaPersonRegistryDao botswanaPersonRegistryDao = new BotswanaPersonRegistryDaoImpl();
		
		// Check if MPI search is enabled and if not so, return an error
		if (!Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.GP_MPI_SEARCH_ENABLED, "false")
		        .equalsIgnoreCase("true")) {
			botswanaRegistryResponse.setResponse("MPI search is not enabled");
			botswanaRegistryResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
			return botswanaRegistryResponse;
		}
		
		try {
			if (registrySource.equals(RegistrySource.CUSTOM)) {
				List<BotswanaRegistryPerson> personsById = botswanaPersonRegistryDao.getPersonsById(personIdNumber);
				botswanaRegistryResponse.setResponse(personsById);
				botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
			} else {
				IdentifierSystem identifierSystem = getIdentifierSystem(registrySource);
				if (identifierSystem == null) {
					botswanaRegistryResponse.setResponse("Invalid registry source");
					botswanaRegistryResponse.setStatus(HttpStatus.BAD_REQUEST.toString());
				} else {
					List<BotswanaRegistryPerson> people = searchPersonInRegistry(personIdNumber,
					    identifierSystem.getSystem());
					setResponseBasedOnPeople(botswanaRegistryResponse, people, personIdNumber);
				}
			}
		}
		catch (SQLException | ParseException | FHIRException e) {
			botswanaRegistryResponse.setResponse(e.getMessage());
			botswanaRegistryResponse.setStatus(getHttpStatus(e).toString());
		}
		
		return botswanaRegistryResponse;
	}
	
	private IdentifierSystem getIdentifierSystem(RegistrySource registrySource) {
		switch (registrySource) {
			case OMANG:
				return IdentifierSystem.OMANG;
			case BIRTH_CERTIFICATE:
				return IdentifierSystem.BCN;
			case PASSPORT:
				return IdentifierSystem.PASSPORT;
			default:
				return null;
		}
	}
	
	private void setResponseBasedOnPeople(BotswanaRegistryResponse botswanaRegistryResponse,
	        List<BotswanaRegistryPerson> people, String personIdNumber) {
		if (people != null && !people.isEmpty()) {
			botswanaRegistryResponse.setResponse(people);
			botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
		} else {
			botswanaRegistryResponse.setResponse(String.format("No person with id %s found in registry", personIdNumber));
			botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
		}
	}
	
	private HttpStatus getHttpStatus(Exception e) {
		if (e instanceof SQLException) {
			return HttpStatus.NOT_FOUND;
		} else if (e instanceof ParseException) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		} else {
			return HttpStatus.NOT_FOUND;
		}
	}
	
	public List<BotswanaRegistryPerson> searchPersonInRegistry(String identifier, String assigningAuthority)
	        throws FHIRException {
		
		// Send the message and construct the result set
		try {
			Bundle results = this.fhirUtil.getClient().search().forResource("Patient")
			        .where(org.hl7.fhir.r4.model.Patient.IDENTIFIER.exactly().systemAndIdentifier(assigningAuthority,
			            identifier))
			        // .exactly().identifier(identifier))
			        .count(1).returnBundle(Bundle.class).execute();
			
			List<BotswanaRegistryPerson> people = new ArrayList<>();
			if (results.getEntry().isEmpty()) {
				return people;
			}
			BundleEntryComponent result = results.getEntry().get(0);
			org.hl7.fhir.r4.model.Patient pat = (org.hl7.fhir.r4.model.Patient) result.getResource();
			
			BotswanaRegistryPerson person = getBotswanaRegistryPerson(pat);
			people.add(person);
			
			return people;
		}
		catch (Exception e) {
			log.error("Error in FHIR Search", e);
			throw new FHIRException(e);
		}
	}
	
	@Override
	public BotswanaRegistryResponse getPatientByAdvancedSearch(AdvancedSearchRegistryPerson advancedSearchRegistryPerson) {
		RegistrySource registrySource = null;
		BotswanaRegistryResponse response = new BotswanaRegistryResponse();
		
		try {
			switch (advancedSearchRegistryPerson.getIdentifierType()) {
				case NATIONAL_ID:
					registrySource = RegistrySource.OMANG;
					break;
				case PASSPORT_NUMBER:
					registrySource = RegistrySource.PASSPORT;
					break;
				case BIRTH_CERTIFICATE_NUMBER:
					registrySource = RegistrySource.BIRTH_CERTIFICATE;
					break;
			}
			
			try {
				if (registrySource != null) {
					IdentifierSystem identifierSystem = getIdentifierSystem(registrySource);
					advancedSearchRegistryPerson.setIdentifierSystem(identifierSystem.getSystem());
				}
				List<BotswanaRegistryPerson> people = queryPatientInClientRegistry(advancedSearchRegistryPerson);
				
				setResponseBasedOnPeople(response, people, advancedSearchRegistryPerson.getIdNumber());
				
			}
			catch (FHIRException e) {
				response.setStatus(getHttpStatus(e).toString());
				response.setResponse(e.getStackTrace());
				response.setMessage(e.getMessage());
			}
			// check if patient exists in Client Registry
			
		}
		catch (Exception e) {
			response.setStatus(getHttpStatus(e).toString());
			response.setResponse(e.getMessage());
		}
		return response;
	}
	
	@Override
	public BotswanaRegistryResponse savePatientInClientRegistry(Patient openMRSPatient, String loggedInAt) {
		BotswanaRegistryResponse response = new BotswanaRegistryResponse();
		
		org.hl7.fhir.r4.model.Patient fhirPatient = patientTranslator.toFhirResource(openMRSPatient);
		
		String hashedPatientId = generateMD5Hash(openMRSPatient.getUuid());
		
		//add extra identifier attribute
		Identifier patientIdentifier = new Identifier().setSystem(BotswanaEmrConstants.OPEN_CR_IDENTIFIER_ATTRIBUTE)
		        .setValue(hashedPatientId);
		
		fhirPatient.addIdentifier(patientIdentifier);
		
		// metadata
		Meta meta = new Meta();
		
		Coding sourceSystem = new Coding().setSystem(BotswanaEmrConstants.CR_SOURCE_SYSTEM_URL)
		        .setCode(BotswanaEmrConstants.CR_SOURCE_SYSTEM_CODE);
		
		//facility ID:
		String mflCode = "";
		Location facilityLocation = Context.getLocationService().getLocation(loggedInAt);
		
		if (facilityLocation != null) {
			LocationAttributeType codeAttrType = Context.getLocationService()
			        .getLocationAttributeTypeByUuid(MASTER_FACILITY_CODE_UUID);
			Set<LocationAttribute> locationAttributes = facilityLocation.getAttributes();
			LocationAttribute locationAttribute = locationAttributes.stream()
			        .filter(attr -> attr.getAttributeType().equals(codeAttrType)).findFirst().orElse(null);
			
			mflCode = locationAttribute == null ? "" : locationAttribute.getValueReference();
		}
		
		Coding facilityId = new Coding().setSystem(BotswanaEmrConstants.CR_FACILITY_ID_URL).setCode(mflCode);
		
		meta.addTag(sourceSystem);
		meta.addTag(facilityId);
		
		// add metadata to the fhirPatient
		fhirPatient.setMeta(meta);
		
		try {
			//post to FHIR Client
			MethodOutcome outcome = this.fhirUtil.postPatientResource(fhirPatient).create().resource(fhirPatient).execute();
			
			if (outcome.getCreated() && outcome.getResource() instanceof org.hl7.fhir.r4.model.Patient) {
				response.setStatus(String.valueOf(HttpStatus.CREATED));
				response.setResponse("Patient resource created successfully on OpenCR.");
			} else if (outcome.getResource() instanceof org.hl7.fhir.r4.model.Patient) {
				response.setStatus(String.valueOf(HttpStatus.OK));
				response.setResponse("Patient resource updated successfully on OpenCR.");
				
			} else {
				response.setStatus(String.valueOf(HttpStatus.BAD_REQUEST));
				response.setResponse("Unexpected response when creating/updating Patient resource on OpenCR.");
			}
		}
		catch (InvalidRequestException e) {
			response.setStatus(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
			response.setResponse(e);
			
			// generate FHIR Task resource
			Task task = new Task();
			task.setStatus(Task.TaskStatus.REQUESTED);
			task.setIntent(Task.TaskIntent.ORDER);
			Reference reference = new Reference();
			reference.setType(FhirConstants.PATIENT);
			reference.setReference(FhirConstants.PATIENT + "/" + fhirPatient.getId());
			task.setFor(reference);
			
			try {
				fhirTaskService.create(task);
			}
			catch (Exception ex) {
				log.error("Failed to save FHIR Task", ex);
			}
		}
		
		return response;
	}
	
	public String generateMD5Hash(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] digest = md.digest();
			StringBuilder sb = new StringBuilder();
			for (byte b : digest) {
				sb.append(String.format("%02x", b));
			}
			return sb.toString();
		}
		catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("MD5 algorithm not found", e);
		}
	}
	
	public List<BotswanaRegistryPerson> queryPatientInClientRegistry(String personIdNumber, String assigningAuthority) {
		try {
			Bundle results = this.fhirUtil
			        .getClientRegistryClient().search().forResource("Patient").where(org.hl7.fhir.r4.model.Patient.IDENTIFIER
			                .exactly().systemAndIdentifier(assigningAuthority, personIdNumber))
			        .count(1).returnBundle(Bundle.class).execute();
			
			List<BotswanaRegistryPerson> people = new ArrayList<>();
			if (results.getEntry().isEmpty()) {
				return people;
			}
			BundleEntryComponent result = results.getEntry().get(0);
			org.hl7.fhir.r4.model.Patient pat = (org.hl7.fhir.r4.model.Patient) result.getResource();
			
			BotswanaRegistryPerson person = getBotswanaRegistryPerson(pat);
			people.add(person);
			
			return people;
		}
		catch (Exception e) {
			log.error("Error in CR Search", e);
			throw new FHIRException(e);
		}
	}
	
	public List<BotswanaRegistryPerson> queryPatientInClientRegistry(
	        AdvancedSearchRegistryPerson advancedSearchRegistryPerson) {
		try {
			// Get results bundle based on the advanced search parameters. Note that some parameters may be null
			Bundle results = this.fhirUtil.getClientRegistryClient().search().forResource("Patient")
			        .where(org.hl7.fhir.r4.model.Patient.IDENTIFIER.exactly().systemAndIdentifier(
			            advancedSearchRegistryPerson.getIdentifierSystem(), advancedSearchRegistryPerson.getIdNumber()))
			        //.where(org.hl7.fhir.r4.model.Patient.NAME.matches().value(advancedSearchRegistryPerson.getM()))
			        .where(
			            org.hl7.fhir.r4.model.Patient.BIRTHDATE.exactly().day(advancedSearchRegistryPerson.getDateOfBirth()))
			        .where(org.hl7.fhir.r4.model.Patient.FAMILY.matches().value(advancedSearchRegistryPerson.getLastName()))
			        .where(org.hl7.fhir.r4.model.Patient.GIVEN.matches().value(advancedSearchRegistryPerson.getLastName()))
			        .where(org.hl7.fhir.r4.model.Patient.GENDER.exactly().code(advancedSearchRegistryPerson.getGender()))
			        .returnBundle(Bundle.class).execute();
			
			List<BotswanaRegistryPerson> people = new ArrayList<>();
			if (results.getEntry().isEmpty()) {
				return people;
			}
			BundleEntryComponent result = results.getEntry().get(0);
			org.hl7.fhir.r4.model.Patient pat = (org.hl7.fhir.r4.model.Patient) result.getResource();
			
			BotswanaRegistryPerson person = getBotswanaRegistryPerson(pat);
			people.add(person);
			
			return people;
		}
		catch (Exception e) {
			log.error("Error in CR Search", e);
			throw new FHIRException(e);
		}
	}
	
	private BotswanaRegistryPerson getBotswanaRegistryPerson(org.hl7.fhir.r4.model.Patient pat) {
		Patient mpiPatient = fhirUtil.parseFhirPatient(pat);
		BotswanaRegistryPerson person = new BotswanaRegistryPerson();
		person.setAge(mpiPatient.getAge());
		// person.setIdNumber(mpiPatient.getIdentifiers());
		// iterate through identifiers and assign omang id, passport id, birth certificate id
		pat.getIdentifier().forEach(identifier -> {
			if (identifier.getSystem().equals(IdentifierSystem.OMANG.getSystem())) {
				person.setOmangId(identifier.getValue());
			}
			if (identifier.getSystem().equals(IdentifierSystem.PASSPORT.getSystem())) {
				person.setPassportId(identifier.getValue());
			}
			if (identifier.getSystem().equals(IdentifierSystem.BCN.getSystem())) {
				person.setBirthCertificateId(identifier.getValue());
			}
		});
		
		person.setFullName(mpiPatient.getPersonName().toString());
		person.setDateOfBirth(mpiPatient.getBirthdate());
		person.setGender(
		    mpiPatient.getGender().equalsIgnoreCase("M") || mpiPatient.getGender().equalsIgnoreCase("Male") ? "Male"
		            : "Female");
		PersonAttribute maritalStatusAttribute = mpiPatient.getAttribute(
		    Context.getPersonService().getPersonAttributeTypeByUuid(BotswanaEmrConstants.MARITAL_STATUS_ATTRIBUTE_TYPE));
		if (maritalStatusAttribute != null) {
			person.setMaritalStatus(maritalStatusAttribute.getValue());
		}
		
		return person;
	}
}
