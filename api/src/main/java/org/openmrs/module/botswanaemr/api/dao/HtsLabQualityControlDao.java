/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao;

import org.openmrs.Location;
import org.openmrs.api.db.OpenmrsDataDAO;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;

import java.io.Serializable;
import java.util.List;

public interface HtsLabQualityControlDao extends OpenmrsDataDAO {
	
	LabQualityControlTest getByLabQualityControlTestId(Serializable id);
	
	List<LabQualityControl> getLabQualityControlsThisWeekByTestingPoints(List<String> testingPoints);

	List<LabQualityControl> getAll(Location location, boolean b);
}
