/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import freemarker.template.TemplateException;
import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.annotation.Authorized;
import org.openmrs.api.APIException;
import org.openmrs.api.OpenmrsService;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.CaseLinkage;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.model.DiscussionParticipant;
import org.openmrs.module.botswanaemr.model.DrugOrderExtension;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemr.utils.EmrPrivilegeConstants;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.util.PrivilegeConstants;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nullable;
import javax.mail.Session;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The main service of this module, which is exposed for other modules. See
 * moduleApplicationContext.xml on how it is wired up.
 */
@Transactional
public interface BotswanaEmrService extends OpenmrsService {
	
	@Transactional(readOnly = true)
	List<Registration> getPatientsRegisteredOnDate(Date startDate, Date endDate, Location location, Integer limit)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public Long getPatientsRegisteredCountOnDate(Date startDate, Date endDate, String location, Integer limit)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit,
	        Location location) throws APIException;
	
	@Transactional(readOnly = true)
	public int getPatientsRegisteredOnDateCountOnly(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException;
	
	List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour, Location location) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Order> getPrescriptionsFilledOnDatePerHour(Date date, String hour) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getTbScreeningEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getTbScreeningEncountersPerHourList(Date date, String hour, Location location)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getTriageScreeningEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getArtEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getTriageScreeningEncountersPerHourList(Date date, String hour, Location location)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getScreeningsDoneOnDate(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) throws APIException;;
	
	@Transactional(readOnly = true)
	public List<Encounter> getScreeningsDoneOnDatePerHour(Date date, String hour, EncounterType encounterType,
	        Location location) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getEncountersList(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterType, Integer limit,
	        Location location) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterTypes, Integer limit,
	        Location location, Patient patient) throws APIException;
	
	//service methods to get all the points required for the payment method
	
	List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterTypes, Integer limit,
	        Location location, Patient patient, PagingInfo pagingInfo) throws APIException;
	
	/**
	 * Get payment method by internal identifier
	 *
	 * @param paymentMethodId Integer
	 * @return PaymentMethod with given internal identifier
	 * @throws APIException <strong>Should</strong> throw error if given null parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT_METHOD })
	public PaymentMethod getPaymentMethod(Integer paymentMethodId) throws APIException;
	
	/**
	 * Get payment method by exact name
	 *
	 * @param name string to match to an Payment.name
	 * @return PaymentMethod that is not retired
	 * @throws APIException <strong>Should</strong> not get retired types <strong>Should</strong> return
	 *             null if only retired type found <strong>Should</strong> not get by inexact name
	 *             <strong>Should</strong> return null with null name parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT_METHOD })
	public PaymentMethod getPaymentMethod(String name) throws APIException;
	
	/**
	 * Get all payment methods (including retired)
	 *
	 * @return payment methods list
	 * @throws APIException <strong>Should</strong> not return retired types
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT_METHOD })
	public List<PaymentMethod> getAllPaymentMethods() throws APIException;
	
	/**
	 * Get all payment method. If includeRetired is true, also get retired payment method.
	 *
	 * @param includeRetired
	 * @return payment method list
	 * @throws APIException <strong>Should</strong> not return retired types <strong>Should</strong>
	 *             include retired types with true includeRetired parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT_METHOD })
	public List<PaymentMethod> getAllPaymentMethods(boolean includeRetired) throws APIException;
	
	/**
	 * Saves a new payment methos or updates an existing payment. If an existing payment, this method
	 * will automatically apply payment.
	 *
	 * @param paymentMethod to be saved
	 * @throws APIException <strong>Should</strong> save payment method with basic details
	 *             <strong>Should</strong> update payment method successfully <strong>Should</strong>
	 *             not overwrite creator if non null <strong>Should</strong> not overwrite dateCreated
	 *             if non null <strong>Should</strong> fail if user is not supposed to edit payment
	 *             method of type of given payment
	 */
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT_METHOD })
	public PaymentMethod savePaymentMethod(PaymentMethod paymentMethod) throws APIException;
	
	//get all the service methods for the service provider details
	/**
	 * Get serviceProviderId by internal identifier
	 *
	 * @param serviceProviderId Integer
	 * @return serviceProviderId with given internal identifier
	 * @throws APIException <strong>Should</strong> throw error if given null parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_SERVICE_PROVIDER })
	public ServiceProvider getServiceProvider(Integer serviceProviderId) throws APIException;
	
	/**
	 * Get service provider by exact name
	 *
	 * @param name string to match to an ServiceProvider.name
	 * @return PaymentMethod that is not retired
	 * @throws APIException <strong>Should</strong> not get retired types <strong>Should</strong> return
	 *             null if only retired type found <strong>Should</strong> not get by inexact name
	 *             <strong>Should</strong> return null with null name parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_SERVICE_PROVIDER })
	public ServiceProvider getServiceProvider(String name) throws APIException;
	
	/**
	 * Get all payment methods (including retired)
	 *
	 * @return payment methods list
	 * @throws APIException <strong>Should</strong> not return retired types
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_SERVICE_PROVIDER })
	public List<ServiceProvider> getAllServiceProvider() throws APIException;
	
	/**
	 * Get all service provider. If includeRetired is true, also get retired service provider.
	 *
	 * @param includeRetired
	 * @return payment method list
	 * @throws APIException <strong>Should</strong> not return retired types <strong>Should</strong>
	 *             include retired types with true includeRetired parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_SERVICE_PROVIDER })
	public List<ServiceProvider> getAllServiceProvider(boolean includeRetired) throws APIException;
	
	/**
	 * Saves a new service provider or updates an existing payment. If an existing payment, this method
	 * will automatically apply payment.
	 *
	 * @param serviceProvider to be saved
	 * @throws APIException <strong>Should</strong> save service provider with basic details
	 *             <strong>Should</strong> update service provider successfully <strong>Should</strong>
	 *             not overwrite creator if non null <strong>Should</strong> not overwrite dateCreated
	 *             if non null <strong>Should</strong> fail if user is not supposed to edit service
	 *             provider of type of given payment
	 */
	@Authorized({ EmrPrivilegeConstants.GET_SERVICE_PROVIDER })
	public ServiceProvider saveServiceProvider(ServiceProvider serviceProvider) throws APIException;
	
	//get payments for the patients
	/**
	 * Saves a new payment or updates an existing payment. If an existing payment, this method will
	 * automatically apply payment.
	 *
	 * @param registration to be saved
	 * @throws APIException <strong>Should</strong> save payment with basic details
	 *             <strong>Should</strong> update paymant successfully <strong>Should</strong> not
	 *             overwrite creator if non null <strong>Should</strong> not overwrite dateCreated if
	 *             non null <strong>Should</strong> not overwrite obs and orders creator or dateCreated
	 *             <strong>Should</strong> fail if user is not supposed to edit payments of type of
	 *             given payment
	 */
	@Authorized({ EmrPrivilegeConstants.ADD_PAYMENT, EmrPrivilegeConstants.EDIT_PAYMENT })
	public Registration savePayment(Registration registration) throws APIException;
	
	/**
	 * Get payment by internal identifier
	 *
	 * @param registrationId payment id
	 * @return payment with given internal identifier
	 * @throws APIException <strong>Should</strong> throw error if given null parameter
	 *             <strong>Should</strong> fail if user is not allowed to view payment by given id
	 *             <strong>Should</strong> return payment if user is allowed to get it
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT })
	public Registration getPayment(Integer registrationId) throws APIException;
	
	/**
	 * Get all payments (not voided) for a patient, sorted by registrationDatetime ascending.
	 *
	 * @param patient
	 * @return List&lt;Payment&gt; payments (not voided) for a patient. <strong>Should</strong> not get
	 *         voided payments <strong>Should</strong> throw error when given null parameter
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT })
	public List<Registration> getPaymentByPatient(Patient patient) throws APIException;;
	
	/**
	 * Get all payments that match a variety of (nullable) criteria contained in the parameter object.
	 * Each extra value for a parameter that is provided acts as an "and" and will reduce the number of
	 * results returned
	 *
	 * @param who the patient
	 * @param loc the location
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param includeVoided the include Voided
	 * @return a list of payments ordered by increasing registrationDatetime <strong>Should</strong> get
	 *         payment modified after specified date
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.GET_PAYMENT })
	public List<Registration> getPayment(Patient who, Location loc, Date fromDate, Date toDate, boolean includeVoided)
	        throws APIException;;
	
	/**
	 * Voiding a payment essentially removes it from circulation
	 *
	 * @param registration Payment object to void
	 * @param reason String reason that it's being voided <strong>Should</strong> void payment and set
	 *            attributes <strong>Should</strong> throw error with null reason parameter
	 *            <strong>Should</strong> fail if user is not supposed to edit payment of type of given
	 *            payment
	 */
	@Transactional(readOnly = true)
	@Authorized({ EmrPrivilegeConstants.EDIT_PAYMENT })
	public Registration voidPayment(Registration registration, String reason) throws APIException;;
	
	/**
	 * Search for concepts by name and concept class
	 *
	 * @param searchTerm the search term
	 * @param conceptClass the concept class uuid or name
	 * @return Concept list
	 * @throws APIException
	 */
	@Transactional(readOnly = true)
	public List<Concept> searchConcept(String searchTerm, String conceptClass) throws APIException;
	
	List<PatientResponse> getPatientsBySearchParams(String nameOrUniqueId, String locationUuid, String programId,
	        String gender, String status, String startDateRegistered, String endDateRegistered, String condition);
	
	List<PatientResponse> getPatientsByHtsSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String testingLocation);
	
	List<PatientResponse> getPatientsByVmmcSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String visitStatus);
	
	//add case linkage functionality
	public CaseLinkage saveCaseLinkage(CaseLinkage caseLinkage) throws APIException;
	
	@Transactional(readOnly = true)
	public CaseLinkage getCaseLinkageById(Integer caseLinkageId) throws APIException;
	
	//add methods to handle cases as required
	public Cases saveCase(Cases cases) throws APIException;
	
	@Transactional(readOnly = true)
	public Cases getCase(Integer caseId) throws APIException;
	
	@Transactional(readOnly = true)
	public List<Cases> getAllCase(Patient patient, boolean includeRetired) throws APIException;
	
	//add methods to handle case encounters
	public CaseEncounter saveCaseEncounter(CaseEncounter caseEncounter) throws APIException;
	
	@Transactional(readOnly = true)
	public CaseEncounter getCaseEncounter(Integer caseEncounterId) throws APIException;
	
	@Transactional(readOnly = true)
	public List<CaseEncounter> getAllCaseEncounter(Patient patient, boolean includeVoided) throws APIException;
	
	@Transactional(readOnly = true)
	public CaseEncounter getCaseByEncounterId(Encounter encounter);
	
	DiscussionParticipant addDiscussionParticipant(DiscussionParticipant discussionParticipant) throws APIException;
	
	void deleteDiscussionParticipant(DiscussionParticipant discussionParticipant) throws APIException;
	
	@Transactional(readOnly = true)
	List<DiscussionParticipant> getDiscussionParticipantsByDiscussion(Discussion discussion);
	
	Discussion saveDiscussion(Discussion discussion) throws APIException;
	
	@Transactional(readOnly = true)
	Discussion getDiscussionByCase(Cases caze) throws APIException;
	
	@Transactional(readOnly = true)
	
	Discussion getDiscussionById(Integer discussionId) throws APIException;
	
	Message saveMessage(Message message) throws APIException;
	
	List<Message> getMessagesByDiscussion(Discussion discussion);
	
	@Transactional(readOnly = true)
	@Authorized({ PrivilegeConstants.GET_PATIENTS })
	Collection<Patient> findPersonInContactWithTBPatients(@NotNull Patient Patient) throws APIException;
	
	@Transactional(readOnly = true)
	public DrugOrder getDrugOrderById(Integer drugOrderId) throws APIException;
	
	DrugOrderExtension saveDrugOrderExtension(DrugOrderExtension drugOrderExtension) throws APIException;
	
	@Transactional(readOnly = true)
	List<DrugOrderExtension> getDrugOrderExtensionsByPatient(@NotNull Patient patient) throws APIException;
	
	@Transactional(readOnly = true)
	List<Patient> getBotswanaEmrPatients(String query, boolean includeVoided, Integer start, Integer length)
	        throws APIException;
	
	/*@Transactional(readOnly = true)
	public ServiceProvider findPatient(String name) throws APIException;*/
	
	@Transactional(readOnly = true)
	List<PatientQueue> getIncompletePatientQueues() throws APIException;;
	
	@Transactional(readOnly = true)
	List<PatientProgram> getPatientProgramByProgram(Program program) throws APIException;;
	
	@Transactional(readOnly = true)
	List<PatientProgram> getPatientProgramByProgramAndDate(Program program, Date date, @Nullable Date endDate,
	        boolean activeOnly) throws APIException;;
	
	@Transactional(readOnly = true)
	List<PatientProgram> getActivePatientProgramsByProgram(Program program, boolean includeVoided) throws APIException;;
	
	@Transactional(readOnly = true)
	List<Person> getPatientByMigrationMetadata(String migrationFacilityId, String migrationPatientId) throws APIException;;
	
	@Transactional(readOnly = true)
	List<BotswanaEmrEmailReportConfig> getBotswanaEmrEmailReportConfigs(String reportId, Location facility)
	        throws APIException;;
	
	/**
	 * Sends a general email with with mo attachments
	 *
	 * @param toEmail
	 * @param subject
	 * @param body
	 */
	void sendGeneralEmail(String toEmail, String subject, String body) throws APIException;;
	
	/**
	 * Sends an email with body and attachment file
	 *
	 * @param config
	 * @param filename
	 */
	void sendAttachmentEmail(BotswanaEmrEmailReportConfig config, String filename) throws APIException;;
	
	/**
	 * Returns the email session
	 */
	Session getMailSession();
	
	/**
	 * Reads a Freemarker template returning contents as one String
	 */
	String getFreemarkerTemplateContent(String template, Map<String, Object> model) throws IOException, TemplateException;
	
	BotswanaEmrEmailReportConfig saveBotswanaEmrEmailReportConfig(BotswanaEmrEmailReportConfig botswanaEmrEmailReportConfig)
	        throws APIException;
	
	@Transactional(readOnly = true)
	List<Encounter> getEncounterLimits(EncounterSearchCriteria searchCriteria, Integer fetchLimit) throws APIException;
	
	@Transactional(readOnly = true)
	public Registration getPatientsRegisteredPositionOnDate(Date startDate, Date endDate, String location, String position)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Obs> getObservation(Person person, Visit visit, Concept question, Location location, Integer mostRecentN)
	        throws APIException;
	
	@Transactional(readOnly = true)
	public List<Obs> getObservation(Person person, EncounterType encounterType, Visit visit, Concept question,
	        Location location, Integer mostRecentN) throws APIException;
	
	@Transactional(readOnly = true)
	public CaseEncounter getLastCaseEncounter(Patient patient) throws APIException;
	
	@Transactional(readOnly = true)
	public List<CodedOrFreeText> getAllConditionEverSaved() throws APIException;
	
	Registration getLatestPaymentEntry(Patient patient, Location location) throws APIException;
}
