/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.openmrs.ui.framework.SimpleObject;

import java.util.ArrayList;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class SimplifiedHtsSpecimenResult {
	
	private String interpretation;
	
	private String comment;
	
	private Integer id;
	
	private Integer proficiencyTestTestId;
	
	private String specimenName;
	
	private String stockRoomId;
	
	private String stockRoom;
	
	@JsonProperty("results")
	ArrayList<SimplifiedHtsTestResult> results = new ArrayList<>();
}
