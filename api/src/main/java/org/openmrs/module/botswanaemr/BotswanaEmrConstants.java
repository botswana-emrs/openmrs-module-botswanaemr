/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr;

public class BotswanaEmrConstants {
	
	public static final String ORDER_SOURCE_FACILITY_NAME_ATTRIBUTE_TYPE_UUID = "61d20b2b-8616-4fd1-81e2-2927e6e4722c";
	
	public static final String ORDER_FULFILLER_NAME_ATTRIBUTE_TYPE_UUID = "F3172144-6936-41AE-8AD5-FC2A43B7932D";
	
	public static final String ORDER_FULFILLMENT_DATE_ATTRIBUTE_TYPE_UUID = "59F31732-A4B3-4B9C-BF30-F727AC2DA632";
	
	public static final String ORDER_BATCH_NUMBER_ATTRIBUTE_TYPE_UUID = "81153f13-4aa9-44a2-ba9e-4b01eeeca26d";
	
	public static final String ORDER_EXPIRY_DATE_ATTRIBUTE_TYPE_UUID = "2aa03ad1-4e81-422e-aaf8-48b9c51fe387";
	
	public static final String INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID = "8A74EB42-8C5E-4FB6-940B-11CD971F522E";
	
	public static final String INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID = "51490fcc-f077-4297-8f91-0cfe3b757f5b";
	
	public static final String INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID = "f4551a74-2751-43f0-a1b8-596212684304";
	
	public static final String INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID = "DFF52708-66B1-4F35-A0E5-4657FD815896";
	
	public static final String INV_ITEM_CLASS_ATTRIBUTE_TYPE_UUID = "f4b46e1b-eac3-476b-be4e-5ff26870d3b4";
	
	public static final String INV_ITEM_SUB_CLASS_ATTRIBUTE_TYPE_UUID = "c7d41e22-c0b5-4ded-8389-b58fa32e7040";
	
	public static final String INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID = "6704A85B-D293-487A-91D4-C52B5EDEB7D1";
	
	public static final String INV_ITEM_MAX_STOCK_LEVEL_ATTRIBUTE_TYPE_UUID = "3E0C59FB-0130-42DC-8062-E1F7FDBAA048";
	
	public static final String INV_ITEM_EMERGENCY_ORDER_POINT_ATTRIBUTE_TYPE_UUID = "A22F4158-2B4F-43CF-9A6C-AC8DE17462A2";
	
	public static final String INV_ITEM_STOCK_DISTRIBUTION_OPERATION_TYPE_UUID = "c264f34b-c795-4576-9928-454d1fa20e09";
	
	public static final String REFERRAL_ENCOUNTER_TYPE_UUID = "9c514154-a863-4c30-a409-5d083e7920d5";
	
	public static final String HTS_TO_ART_REFERRAL_ENCOUNTER_TYPE_UUID = "6299ee3f-eebc-4a0a-890b-c688ac12b0c3";
	
	public static final String REFERRAL_ORDER_TYPE_UUID = "f1b63696-2b6c-11ec-8d3d-0242ac130003";
	
	public static final String HIV_CARE_REFERRAL_ORDER_REASON_CONCEPT_UUID = "164849AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_TEST_DATE_CONCEPT_UUID = "164400AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_TEST_ACCEPTANCE_CONCEPT_UUID = "c13f95c1-484f-4594-aa41-bd800aa605ce";
	
	public static final String BOTSWANAEMR_MODULE_ID = "botswanaemr";
	
	public static final String FORCE_PASSWORD_CHANGE = "forcePasswordChange";
	
	public static final String PRIVILEGE_GET_ENCOUNTERS = "Get Encounters";
	
	public static final String PRIVILEGE_GET_OBSERVATIONS = "Get Observations";
	
	public static final String PRIVILEGE_SCREENING_VITALS = "Screening: Capture vitals";
	
	public static final String PRIVILEGE_SCREENING_MEDICAL_DETAILS = "Screening: Capture medical details";
	
	public static final String PRIVILEGE_SCREENING_SYMPTOMS = "Screening: Capture symptoms";
	
	public static final String HTS_DIAGNOSTICS_ENCOUNTER_TYPE_UUID = "f90231be-7ea1-11ed-a1eb-0242ac120002";
	
	public static final String HTS_IPV_SCREENING_ENCOUNTER_TYPE_UUID = "662614c3-8c4f-4f6f-8759-190b24c86349";
	
	public static final String HTS_CHILDREN_OF_INDEX_INFORMATION_ENCOUNTER_TYPE_UUID = "f492c33d-af89-4930-b341-fded4538ac9f";
	
	public static final String HTS_PNS_INDEX_CLIENT_INTERVIEW_ENCOUNTER_TYPE_UUID = "c5e3a735-56db-445d-b0e9-3340e994dd32";
	
	public static final String TB_ENCOUNTER_TYPE_UUID = "38ac7edc-9dac-11ec-96c6-5f1af49b59d6";
	
	public static final String TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID = "7A4C7F96-F49D-43BA-BB51-D34C0B4C2F6B";
	
	public static final String TB_SCREENING_ENCOUNTER_TYPE_UUID = "F9AEF0F8-DAA8-461A-9CB6-4ED14A1FD895";
	
	public static final String TB_SCREENING_FORM_UUID = "98EF3ABD-3BC0-421B-8FDE-C01E27EAE967";
	
	public static final String TB_SCREENING_GEN_FORM_UUID = "76154a58-9143-11ec-aa8e-eb4c1183120e";
	
	public static final String TB_LAB_REQUEST_FORM_UUID = "1E88B447-7B50-440E-B475-09844AA0E86E";
	
	public static final String TB_XRAY_RESULTS_FORM_UUID = "33e87547-adaa-4b0b-acff-a6a7fe866e6a";
	
	public static final String TB_CONTACT_LISTING_FORM_UUID = "EBF9BC62-E788-4A8C-883E-21C8CF2F51CD";
	
	public static final String TB_DOT_FORM_UUID = "24ab0124-a2c5-11ec-b909-0242ac120002";
	
	public static final String ELIGIBILITY_AND_INITIATION_FORM_UUID = "4b058afc-f7cd-11ed-b67e-0242ac120002";
	
	public static final String ART_INITIAL_ENCOUNTER_TYPE_UUID = "0077726a-c4a3-11ec-9d64-0242ac120002";
	
	public static final String ART_FOLLOW_UP_ENCOUNTER_TYPE_UUID = "4c329b3a-c4a3-11ec-9d64-0242ac120002";
	
	public static final String ANTENATAL_HISTORY_ENCOUNTER_TYPE_UUID = "57a1d43e-bfca-4db2-8fb9-537a43fa69f0";
	
	public static final String PATIENT_REGISTRATION_PORTAL_UUID = "6351fcf4-e311-4a19-90f9-35667d99a8af";
	
	public static final String NURSING_PORTAL_UUID = "3EDC1F97-C224-4F24-B941-3DAAD1CEDB45";
	
	public static final String DOCTORS_PORTAL_UUID = "C63DEB3C-CB67-4B9A-998F-DB410DD869D2";
	
	public static final String ART_SERVICES_PORTAL_UUID = "E3708599-BD88-4130-8023-50DC9695AAAD";
	
	public static final String TB_SERVICES_PORTAL_UUID = "47117365-47FC-4B45-A1CC-76565A332AD5";
	
	public static final String HIV_TESTING_SERVICES_PORTAL_UUID = "bebf8287-bec0-42cb-a9ee-6c6ca4cb62c0";
	
	public static final String PHARMACY_PORTAL_UUID = "7f65d926-57d6-4402-ae10-a5b3bcbf7986";
	
	public static final String STOCK_MANAGEMENT_PORTAL_UUID = "CF8644E8-F868-45A4-9573-28609A3C8259";
	
	public static final String LAB_SERVICES_PORTAL_UUID = "61a9a385-6c2e-424b-8a3e-fd589b1068e6";
	
	public static final String VMMC_PORTAL_UUID = "DBF3A5F0-88D5-4C9A-B08B-0ECB8D9E0AA0";
	
	public static final String SEXUAL_REPRODUCTIVE_HEALTH_PORTAL_UUID = "F1F50A54-4E86-4126-950D-8AF10563FEC7";
	
	public static final String OPENMRS_ID_IDENTIFIER_TYPE = "05a29f94-c0ed-11e2-94be-8c13b969e334";
	
	public static final String NATIONAL_ID_IDENTIFIER_TYPE = "9C92F3DF-AE32-462F-8FB2-A78DFAF8BA7C";
	
	public static final String PASSPORT_IDENTIFIER_TYPE = "5E8BFED6-1729-4A9B-AB5A-46B27389EE5F";
	
	public static final String BIRTH_CERT_IDENTIFIER_TYPE = "3179C66B-3A00-4DEF-AC53-A07B49DF8F54";
	
	public static final String TEMPORARY_ID_IDENTIFIER_TYPE_UUID = "E10EDA31-C166-4EFC-BB84-5D46B351535B";
	
	public static final String TEMPORARY_ID_IDGEN_SOURCE_UUID = "C191E1BD-A6D4-4681-B6ED-771EE416BF2C";
	
	public static final String MALE_GENDER = "M";
	
	public static final String FEMALE_GENDER = "F";
	
	public static final String OTHER_GENDER = "O";
	
	public static final String REGULAR_PATIENT = "Regular";
	
	public static final String EMERGENCY_PATIENT = "Emergency";
	
	public static final String USER_NATIONAL_ID_ATTRIBUTE_TYPE_UUID = "d00b2500-b4ed-11ec-b983-4fc209252793";
	
	public static final String USER_TELEPHONE_CONTACT_ATTRIBUTE_TYPE_UUID = "2968053a-b4f0-11ec-927d-d3d8af202315";
	
	public static final String USER_COUNTRY_ATTRIBUTE_TYPE = "6355d97a-b4f0-11ec-a007-f71629026eee";
	
	public static final String USER_STREET_ADDRESS_ATTRIBUTE_TYPE = "a4942306-b4f0-11ec-a3de-b72ef6311990";
	
	public static final String USER_SECURITY_NO_ATTRIBUTE_TYPE = "9155507a-c0ae-11ec-9d44-9f3ca3c917f5";
	
	public static final String USER_BACKUP_EMAIL_ATTRIBUTE_TYPE = "1cc96e56-c0b0-11ec-8268-a7483fcc42c2";
	
	public static final String OCCUPATION_ATTRIBUTE_TYPE_UUID = "d34ba619-7494-4d26-a038-e1975b2bf3b2";
	
	public static final String PATIENT_AFFILIATION_ATTRIBUTE_TYPE_UUID = "1BB55503-4BB9-4CFC-AAD1-B37E8320B095";
	
	public static final String FORCE_NUMBER_ATTRIBUTE_TYPE_UUID = "E0804D7A-2350-410E-B547-32161F2C3A9D";
	
	public static final String RANK_ATTRIBUTE_TYPE_UUID = "17A30A93-E76A-4878-A006-21BDD6CACAE5";
	
	public static final String EMPLOYER_NAME_ATTRIBUTE_TYPE_UUID = "530b1383-012d-4285-8b9c-331c4306b34c";
	
	public static final String PATIENT_TYPE_ATTRIBUTE_TYPE_UUID = "5787C0AD-04F3-4F39-809E-C54790615C32";
	
	public static final String PHONE_NUMBER_ATTRIBUTE_TYPE_UUID = "14d4f066-15f5-102d-96e4-000c29c2a5d7";
	
	public static final String EMAIL_ATTRIBUTE_TYPE_UUID = "aa3174b1-efa8-4efd-b504-7d78f49a3181";
	
	public static final String PERSON_LOCATION_ATTRIBUTE = "0a93cbc6-5d65-4886-8091-47a25d3df944";
	
	public static final String EDUCATION_LEVEL_ATTRIBUTE_TYPE_UUID = "419604d9-98b9-4f7b-8cf3-4be593ec206a";
	
	public static final String CITIZEN_TYPE_ATTRIBUTE_TYPE_UUID = "60EA1904-2D15-486C-BAEA-2BC78DD498F2";
	
	public static final String OTHER_OCCUPATION_ATTRIBUTE_TYPE_UUID = "E43529ED-3C66-488A-862E-ACF6DA41F861";
	
	public static final String EMERGENCY_PATIENT_TYPE_ATTRIBUTE_TYPE_UUID = "4489FAFF-9BB4-4CE8-8118-EA78136CBD9E";
	
	public static final String MARITAL_STATUS_ATTRIBUTE_TYPE = "B89B2424-76BC-4676-ABC3-5D3BBE818560";
	
	public static final String EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE = "72C35191-B04E-435D-BC58-8CDB80C21458";
	
	public static final String OTHER_EMPLOYMENT_SECTOR_ATTRIBUTE_TYPE = "0A25C142-6C85-4B62-BF3D-D950B935E5B8";
	
	// Next Of Kin
	public static final String NOK_ID_TYPE_ATTRIBUTE_TYPE_UUID = "013B3C39-6F36-4E83-B1E7-73C86BA4428F";
	
	public static final String NOK_ID_ATTRIBUTE_TYPE_UUID = "1302F05F-EE25-4D39-B32A-441815F13ADB";
	
	public static final String NOK_FULLNAME_ATTRIBUTE_TYPE_UUID = "45216363-2D34-4AF4-8FF5-589D237DF9B7";
	
	public static final String NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID = "B38591F0-4E88-4410-9663-0B06F2DE3444";
	
	public static final String OTHER_NOK_RELATIONSHIP_ATTRIBUTE_TYPE_UUID = "66c79797-845d-49c0-8bee-d39ee67c24ee";
	
	public static final String NOK_CONTACT_1_ATTRIBUTE_TYPE_UUID = "03807BD1-FF80-4AA8-B024-4AB8347ABB8D";
	
	public static final String NOK_CONTACT_2_ATTRIBUTE_TYPE_UUID = "b51dd002-dc76-4850-aa73-b31775f567e7";
	
	public static final String NOK_CONTACT_3_ATTRIBUTE_TYPE_UUID = "8bfa0f0b-e60d-4e32-a18c-2774f80ba601";
	
	public static final String NOK_CONTACT_4_ATTRIBUTE_TYPE_UUID = "06034441-19a7-4ccc-8f38-85db0a58e7fd";
	
	public static final String NOK_CONTACT_5_ATTRIBUTE_TYPE_UUID = "6435799e-b3bb-4c45-91ff-de03f1050a70";
	
	public static final String NOK_EMAIL_ATTRIBUTE_TYPE_UUID = "a6b36385-7283-4567-b0b5-1d6cbe749bd5";
	
	public static final String NOK_CONTACT_TYPE_TYPE_UUID = "77FA6739-7C41-4898-AEA5-F92EC1A71A04";
	// End Nok
	
	public static final String ALT_PHONE_NUMBER_ATTRIBUTE_TYPE_UUID = "F434D206-EBC1-4C8E-9FC0-62685A52B940";
	
	public static final String ALT_PHONE_NUMBER_2_ATTRIBUTE_TYPE_UUID = "1EB9A64E-EDCF-4DC1-8D9F-7DC6659F91DA";
	
	public static final String ALT_PHONE_NUMBER_3_ATTRIBUTE_TYPE_UUID = "055A366D-9E32-4006-B56C-A39FBD09D116";
	
	public static final String ALT_PHONE_NUMBER_4_ATTRIBUTE_TYPE_UUID = "8160CA1B-DA7F-4864-AEB2-FEC5A198CCB5";
	
	public static final String ALT_PHONE_NUMBER_5_ATTRIBUTE_TYPE_UUID = "5C7E966B-4B47-4A7A-94A9-2FEAE9BF2F46";
	
	public static final String NUMBER_TYPE_ATTRIBUTE_TYPE_UUID = "02AD1A1F-1725-49C7-85F5-B9680AFF4FD0";
	
	public static final String ALT_NUMBER_TYPE1_ATTRIBUTE_TYPE_UUID = "1BFB4CEF-7D37-46B9-9759-CEEC15D37110";
	
	public static final String ALT_NUMBER_TYPE2_ATTRIBUTE_TYPE_UUID = "53D75FF4-FE13-4AE3-A95D-C2E4C22F3EF8";
	
	public static final String ALT_NUMBER_TYPE3_ATTRIBUTE_TYPE_UUID = "877014D1-A878-4BD3-A4AC-1038A85B05F3";
	
	public static final String ALT_NUMBER_TYPE4_ATTRIBUTE_TYPE_UUID = "5DADE639-A598-4AC6-A0B0-D519C33B9D5B";
	
	public static final String LAB_ORDER_ENCOUNTER_TYPE_UUID = "1E88B447-7B50-440E-B475-09844AA0E86E";
	
	public static final String LAB_RESULT_ENCOUNTER_TYPE_UUID = "78FCCD94-CD75-4FF1-85B5-A2FC70F0237A";
	
	public static final String XRAY_RESULT_ENCOUNTER_TYPE = "2e45901d-d343-4f17-b918-0cc72ed25c32";
	
	public static final String VMMC_OPERATION_ENCOUNTER_TYPE = "B444FBD1-EA53-4FDB-BC52-EE90E93B4E1F";
	
	public static final String VMMC_POST_OPERATION_ENCOUNTER_TYPE = "2039d684-043f-11ed-b939-0242ac120002";
	
	public static final String VMMC_CONSENT_ENCOUNTER_TYPE = "1a7743c8-1720-11ed-861d-0242ac120002";
	
	public static final String VMMC_COUNSELLING_ENCOUNTER_TYPE = "601e1bb4-1e2c-11ed-861d-0242ac120002";
	
	public static final String VMMC_BOOKING_ENCOUNTER_TYPE = "c96a4d0e-4fb2-424c-8562-9907a2814124";
	
	public static final String VMMC_INITIAL_SCREENING_ENCOUNTER_TYPE = "e721e0d8-2483-11ee-be56-0242ac120002";
	
	public static final String TB_TEST_TYPE = "165254AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DOCTORS_CONSULTATION_ENCOUNTER_TYPE = "BAEC51D8-7860-47D2-B84E-10B27D6E33D2";
	
	public static final String RETREATMENT = "159793AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONSULTATIONS_NEW_ENCOUNTER_TYPE_UUID = "0C009211-901A-4802-8EE0-6A7F47FB3965";
	
	public static final String CONSULTATIONS_EXISTING_ENCOUNTER_TYPE_UUID = "C93404F7-15A0-4E3E-9E7A-8067C870AB35";
	
	public static final String VITALS_ENCOUNTER_TYPE_UUID = "d1d19c40-083c-11ee-be56-0242ac120002";
	
	public static final String CONSULTATIONS_CLINICAL_NOTES_CONCEPT_UUID = "160029AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String FACILITY_VISIT_VISIT_TYPE_UUID = "7b0f5697-27e3-40c4-8bae-f4049abfb4ed";
	
	public static final String BP_SCREENING_VISIT_VISIT_TYPE_UUID = "6D15A3D2-6F73-4288-982A-9C5376386C57";
	
	public static final String DEFAULT_PAYMENT_METHOD_UUID = "9DE430BD-97BB-4ADE-A1F3-75C47701FA8E";
	
	public static final String NONE = "None";
	
	public static final String ORDER_TYPE_TEXT = "drugorder";
	
	public static final String DIAGNOSIS_ENCOUNTER_TYPE_UUID = "52AE669B-2B27-4DCA-A580-A1A939BBB564";
	
	public static final String HPV_REQUEST_ENCOUNTER_TYPE_UUID = "387e6934-c4ca-46dc-b97d-5e62ea5bedba";
	
	public static final String DIAGNOSIS_TYPE_DIAGNOSIS_ATTRIBUTE_TYPE = "AD65B4C9-486B-42DD-85CE-80967D2BC866";
	
	public static final String TREATED = "TREATED";
	
	public static final String AWAITING = "AWAITING";
	
	public static final String REGULAR_FOLLOW_UP_APPOINTMENT_TYPE = "6DCFD199-09A2-4843-A7D1-91EC4ADBF9F3";
	
	public static final String TASK_APPOINTMENT_TYPE = "c691c1f8-77f9-4b14-9616-ecf8d35d545e";
	
	public static final String ART_APPOINTMENT_TYPE_UUID = "32265608-5A8E-4B52-BBED-1120DFF6158B";
	
	public static final String DRUG_TRACKING_ENCOUNTER_TYPE = "2909d9e6-5037-4d7d-899b-e2113b413965";
	
	public static final String TB_CONTACT_SCREENING_ENCOUNTER_TYPE = "cb1e0993-0db0-46e0-ae23-e97a805c248f";
	
	public static final String CURRENT_DRUGS_CONCEPT_UUID = "1193AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_TREATMENT_TYPE_CONCEPT_UUID = "159792AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TABLET_CONCEPT_UUID = "1513AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

	public static final String CAPSULE_CONCEPT_UUID = "1608AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ORAL_ROUTE_CONCEPT_UUID = "160240AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String INJECTABLE_ROUTE_CONCEPT_UUID = "160242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TOPICAL_ROUTE_CONCEPT_UUID = "162797AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ONCE_DAILY_CONCEPT_UUID = "160862AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_TREATMENT_GROUP_CONCEPT_UUID = "7db20a97-032c-4b7e-a58b-4488d88944ba";
	
	public static final String VMMC_INITIAL_SCREENING_GROUP_CONCEPT_UUID = "166661AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_PATIENT_TYPE_CONCEPT_UUID = "164411AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_ENROLMENT_ENCOUNTER_TYPE_UUID = "38ac7edc-9dac-11ec-96c6-5f1af49b59d6";
	
	public static final String DRUG_SUSCEPTIBLE_TB_UUID = "788c5c4e-cd8e-429c-a4df-1f684bb5fbb0";
	
	public static final String DRUG_RESISTANT_TB_UUID = "e69735c3-381c-4acc-93cf-bd76c2e8c6f3";
	
	public static final String INITIAL_TREATMENT_PHASE_UUID = "159795AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONTINUATION_TREATMENT_PHASE_UUID = "159794AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DRUG_SUSCEPTIBLE_TB_DEFAULT_REGIMEN = "2HRZE/4HRE";
	
	public static final String ADVERSE_DRUG_REACTION_ENCOUNTER_TYPE_UUID = "BCEAB760-9ADE-4554-845E-F2AEA0232310";
	
	public static final String OBJECTIVE_ADDITIONAL_NOTES_CONCEPT_UUID = "dfeac673-857b-49c4-b198-3a766059b0c3";
	
	// Data entry forms
	public static final String VITALS_FORM_UUID = "c5683586-083c-11ee-be56-0242ac120002";
	
	public static final String PEP_INITIATION_FORM_UUID = "56b0db57-4187-4d48-b98f-ceae2b336086";
	
	public static final String VISIT_NOTE_FORM_UUID = "c75f120a-04ec-11e3-8780-2b40bef9a44b";
	
	public static final String ADMISSION_SIMPLE_FORM_UUID = "d2c7532c-fb01-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String DISCHARGE_FORM_UUID = "b5f8ffd8-fbde-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String TRANSFER_WITHIN_HOSPITAL_FORM_UUID = "a007bbfe-fbe5-11e2-8ff2-fd54ab5fdb2a";
	
	public static final String TRIAGE_SCREENING_FORM_UUID = "07fada0b-8c38-44df-bf3f-f1d4120ed697";
	
	public static final String LAB_REQUEST_FORM_UUID = "1E88B447-7B50-440E-B475-09844AA0E86E";
	
	public static final String LAB_RESULTS_FORM_UUID = "abf79206-16d1-43f8-b5bc-d278420071d6";
	
	public static final String HIV_LAB_REQUEST_FORM_UUID = "6f86e76c-efd2-11ec-8ea0-0242ac120002";
	
	public static final String HIV_LAB_RESULT_FORM_UUID = "9ea77d88-ea6c-11ec-8fea-0242ac120002";
	
	public static final String HIV_ADULT_INITIAL_CONSULTATION_FORM_UUID = "4425d1e6-cbd8-11ec-9d64-0242ac120002";
	
	public static final String HIV_PAEDS_INITIAL_CONSULTATION_FORM_UUID = "bac2859e-c49c-11ec-9d64-0242ac120002";
	
	public static final String ART_ENROLMENT_FORM_UUID = "1ea3d1a3-b159-4941-9fc8-43e8418b2105";
	
	public static final String ART_TRANSFER_OUT_FORM_UUID = "f41acd85-6883-4d8c-898e-3f236ce4ff4d";
	
	public static final String VMMC_OPERATION_FORM_UUID = "88293B71-3646-44BD-82F0-93805F479C30";
	
	public static final String VMMC_POST_OPERATION_FORM_UUID = "09b6ab9e-043f-11ed-b939-0242ac120002";
	
	public static final String VMMC_POST_OPERATION_ASSESSMENT_FORM_UUID = "9dfaf868-7431-4d1c-b59d-6a757968c145";
	
	public static final String VMMC_CONSENT_FORM_UUID = "534b99d0-1700-11ed-861d-0242ac120002";
	
	public static final String VMMC_BOOKING_FORM_UUID = "7b7ed5d0-f934-40d6-9dd9-7afb659797ff";
	
	public static final String VMMC_COUNSELLING_FORM_UUID = "526c1746-1e2c-11ed-861d-0242ac120002";
	
	public static final String VMMC_INITIAL_SCREENING_FORM_UUID = "d8227fa2-2483-11ee-be56-0242ac120002";
	
	public static final String HIV_TEST_RESULT_CONCEPT_UUID = "159427AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String STOCK_ROOM_CONCEPT_UUID = "e5eccfe3-6387-4d3e-ab74-1bc4880ef9a1";
	
	public static final String OTHER_DRUGS_CONCEPT_UUID = "166665AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_TEST_TYPE_CONCEPT_UUID = "1271AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID = "4eb02164-0054-4f27-822e-3c8fdd1007fe";
	
	public static final String HIV_STATUS_RESULT_CONCEPT_UUID = "1169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

	public static final String ART_PREP_ELIGIBLE_CONCEPT_UUID = "08d73c9c-3577-4fb3-8529-23ba5a2d8ef1";
	
	public static final String LAST_TEST_OPTION_CONCEPT_UUID = "881fd68e-00b8-471b-a237-c5bd5aa52571";
	
	public static final String LAST_TEST_RESULTS_CONCEPT_UUID = "22582104-7824-4f5b-9fd3-9e127f201306";
	
	public static final String NO_RESULTS_OBTAINED_CONCEPT_UUID = "164369AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String INDETERMINATE_CONCEPT_UUID = "1138AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_CONFIRMATORY_TEST_CONCEPT_UUID = "1042AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ADULT_FOLLOW_UP_CONSULTATION_FORM_UUID = "3A3173A3-E403-4510-9994-EB7ED9B2126C";
	
	public static final String PAEDS_FOLLOW_UP_CONSULTATION_FORM_UUID = "767BE697-C7F4-416D-B8F4-9BD163BD95A7";
	
	public static final String ADULT_ADHERENCE_ASSESSMENT_FORM_UUID = "4310ea10-d084-11ec-9d64-0242ac120002";
	
	public static final String PAEDS_ADHERENCE_ASSESSMENT_FORM_UUID = "aae3f916-e24f-11ec-8fea-0242ac120002";
	
	public static final String PHYSICAL_EXAM_FORM_UUID = "3bc08f6a-e536-11ec-8fea-0242ac120002";
	
	public static final String HTS_DIAGNOSTICS_TEST_FORM_UUID = "1116a8fc-7ea2-11ed-a1eb-0242ac120002";
	
	public static final String HTS_REASON_FOR_TEST_CONCEPT_UUID="164126AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_SELF_TEST_DISTRIBUTION_FORM_UUID = "e926446d-06ee-480c-9052-b5a4178199d9";
	
	public static final String HTS_POST_TEST_FORM_UUID = "a617d249-1cc8-4d66-9d13-061e43fb4c28";
	
	public static final String HTS_VERIFICATION_FORM_UUID = "a2806f22-8112-11ed-a1eb-0242ac120002";
	
	public static final String HTS_SUPPLEMENTARY_TEST_FORM_UUID = "35059EA9-6172-42E6-8DA4-9E46EBE6F9F6";
	
	public static final String HTS_IPV_SCREENING_FORM_UUID = "fc48c378-0f0f-4216-8506-e1dfec4e92b9";
	
	public static final String HTS_PARTNER_CHARACTERISTIC_AND_HIV_FORM_UUID = "62176b46-14f0-454c-9324-5eaa2d7a1435";
	
	public static final String HTS_CONTACTS_FORM_UUID = "3d5fff33-dbf2-495c-b388-fe4b9ceb42fb";
	
	public static final String HTS_REFERRALS_AND_OUTCOME_FORM_UUID = "81cedbce-b95f-4e32-8b50-65206874becc";
	
	public static final String HTS_CHILDREN_INDEX_INFORMATION_FORM_UUID = "74d45e63-d064-42f3-a005-efd980285ae4";
	
	public static final String HTS_CONCEPT_QUANTITY_ISSUED_UUID = "166864AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_HIV_TEST_REFERRAL_FORM_UUID = "9f3cd786-3050-4edc-9976-ee111708c326";
	
	public static final String HTS_DOCUMENT_PRIOR_TEST_FORM_UUID = "058cefec-a53f-4960-aa54-bc136c695e52";
	
	public static final String HTS_HIV_TEST_TYPE_UUID = "165309AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_DNA_PCR_CONCEPT_UUID = "844AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_POSITIVE_TEST_RESULTS_CONCEPT_UUID = "138571AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_ST_ORAL_TEST_CONCEPT_UUID = "542c4ab7-6d3a-42a0-80ed-4daabad9a647";
	
	public static final String HTS_WESTERN_BLOT_CONCEPT_UUID = "1047AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_TEST_KIT_SET_UUID = "e5da6831-8235-4f91-80c1-00a3667cf461";
	
	public static final String HTS_TEST_KIT__UUID = "5fe1e91b-8e0f-4c05-ae33-9e1f27898a48";
	
	public static final String HTS_TEST_KIT_LOT_UUID = "166455AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_TEST_KIT_EXPIRY_DATE_UUID = "162502AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HTS_IPV_SCREENING_RESULT_CONCEPT_UUID = "d9566efc-f139-4155-9c07-e4821375d460";
	
	public static final String HTS_IPV_SCREENING_NOTIFICATION_OPTIONS_CONCEPT_UUID = "e36245b9-d039-460d-8b3e-38a50a844004";
	
	public static final String HTS_ACTIVE_PARTNER_NOTIFICATION_CONCEPT_UUID = "fa22fde0-43e9-4ddb-b10f-e82fb0ea051f";
	
	public static final String HTS_MASK_HIV_TESTING_CONCEPT_UUID = "0571f49e-526e-4c2a-a2b4-65d805dc9d28";
	
	public static final String HTS_STOCKROOM_UUID = "a73ff744-0ceb-432e-bcf5-ec1d99e26cad";
	
	//SRH Family Planning Forms
	public static final String SRH_MEDICAL_SURGICAL_HISTORY_FORM_UUID = "c2a20f04-debd-447b-a4fa-daa250a32092";
	
	public static final String SRH_MEDICAL_SURGICAL_HISTORY_ENCOUNTER_TYPE_UUID = "d3372be6-218a-482a-b950-af9a3c7e1282";
	
	public static final String SRH_MEDICAL_SURGICAL_HISTORY_FORM_MODAL_TITLE = "Medical/Surgical History";
	
	public static final String SRH_SOCIAL_HABITS_FORM_UUID = "d1ec7b8f-e225-43fa-ad89-da0656198c21";
	
	public static final String SRH_SOCIAL_HABITS_ENCOUNTER_TYPE_UUID = "3e51db67-ba2f-4687-81ad-2bed853fb8e1";
	
	public static final String SRH_SOCIAL_HABITS_FORM_MODAL_TITLE = "B. Social Habits";
	
	public static final String SRH_SEXUAL_STI_HISTORY_FORM_UUID = "f5626bb0-dca9-4cdb-ae2a-4578e03e229d";
	
	public static final String SRH_SEXUAL_STI_HISTORY_ENCOUNTER_TYPE_UUID = "bfefd4d1-7a3a-4dcc-a35c-d17553e0460f";
	
	public static final String SRH_SEXUAL_STI_HISTORY_FORM_MODAL_TITLE = "Sexual/STI History";
	
	public static final String SRH_CONTRACEPTIVE_HISTORY_FORM_UUID = "a0b859c7-a671-4b8e-9b71-eaf878aed9ea";
	
	public static final String SRH_CONTRACEPTIVE_HISTORY_ENCOUNTER_TYPE_UUID = "b66137e1-ffe4-492f-993d-5face8142560";
	
	public static final String SRH_CONTRACEPTIVE_HISTORY_FORM_MODAL_TITLE = "Contraceptive History";
	
	public static final String SRH_PHYSICAL_EXAM_HISTORY_FORM_UUID = "d16f93ec-0970-41dc-9d06-c07ae8809fdb";
	
	public static final String SRH_PHYSICAL_EXAM_HISTORY_ENCOUNTER_TYPE_UUID = "e64e3b82-762d-42f4-88b5-1ce1ad029684";
	
	public static final String SRH_PHYSICAL_EXAM_HISTORY_FORM_MODAL_TITLE = "Physical Examination";
	
	public static final String SRH_PLAN_HISTORY_FORM_UUID = "91e5127b-80cf-41cb-99d0-79b69f15c400";
	
	public static final String SRH_PLAN_HISTORY_ENCOUNTER_TYPE_UUID = "68c7fd53-368a-4415-bf71-2aed6b1cc5fb";
	
	public static final String SRH_PLAN_HISTORY_FORM_MODAL_TITLE = "Plan";
	
	//SRH CCS forms
	public static final String GYNAECOLOGY_CYTOLOGY_FORM_UUID = "082aa40d-2669-438f-8353-ff34331fec34";
	
	public static final String VAT_CLIENT_SCREENING_FORM_UUID = "0640022f-e097-488b-8886-46408c22dbc9";
	
	public static final String CERVICAL_CANCER_SCREENING_FORM_UUID = "65c87e2a-1c70-4bcb-82cb-02e7866b8360";
	
	public static final String COLPOSCOPY_LEEP_SCREENING_FORM_UUID = "d35b5327-7a1c-4e9b-80f3-5903e3eaa96e";
	
	public static final String HPV_REQUEST_FORM_UUID = "7554f89d-643d-435f-b8eb-c04fcb035740";
	
	public static final String SRH_GYNAECOLOGICAL_HISTORY_FORM_UUID = "f7aef98d-260f-4614-a73a-34a34f808d32";
	
	public static final String SRH_GYNAECOLOGICAL_HISTORY_ENCOUNTER_TYPE_UUID = "94ad5611-11dc-4e34-bae9-b9e4c977792c";
	
	public static final String SRH_GYNAECOLOGICAL_HISTORY_FORM_MODAL_TITLE = "Reproductive/Gynaecological History";
	
	//SRH Obstetric forms
	public static final String SRH_ENROLMENT_FORM_UUID = "dafa3d88-b6b1-4127-afd3-28d8946b3149";
	
	public static final String LABOUR_DELIVERY_FORM_UUID = "8ad729ca-3e28-48e3-9fe7-b8ba58fff81e";
	
	public static final String PARTOGRAPH_OBSERVATION_FORM_UUID = "7db944e0-f90c-40fc-adf0-979eb0ee6725";
	
	public static final String POST_PARTUM_HOME_VISIT_FORM_UUID = "063f5f8a-d28a-4f62-8c28-21368cf6781b";
	
	public static final String POST_PARTUM_OBSERVATION_FORM_UUID = "d44bbc94-e8cc-40d1-948e-8cd8cbddeedb";
	
	public static final String PREGNANCY_SUMMARY_FORM_UUID = "5e7705a7-09ef-4294-b761-596b384fbc03";
	
	public static final String ADMISSION_AND_PREGNANCY_TERMINATION_FORM_UUID = "fe3a78de-fe9e-463e-b0bf-4b347aa59316";
	
	public static final String DELIVERY_SUMMARY_FORM_UUID = "4c4dbd4d-54a2-4ad4-8e3d-22a0a9e98a62";
	
	public static final String SRH_DISCHARGE_SUMMARY_FORM_UUID = "8ee40092-ac00-4ad9-9afc-bb6b2401af02";
	
	public static final String ANC_ANTENATAL_ENROLMENT_FORM_UUID = "a6072c4a-cd6a-11ed-afa1-0242ac120002";
	
	public static final String ANC_ANTENATAL_ENROLMENT_ENCOUNTER_TYPE_UUID = "c1a4cb24-cd6a-11ed-afa1-0242ac120002";
	
	public static final String ANC_ANTENATAL_HISTORY_FORM_UUID = "13a0d82a-d3e0-11ed-afa1-0242ac120002";
	
	public static final String TETANUS_DIPTHERIA_TRACKING_FORM_UUID = "ba358203-8341-455a-a8ed-57712914612f";
	
	public static final String ANC_NEW_RISKS_AFTER_BOOKING_FORM_UUID = "cf7eca4a-d9f6-11ed-afa1-0242ac120002";
	
	public static final String ANC_NEW_RISKS_AFTER_BOOKING_ENCOUNTER_TYPE_UUID = "35db9fde-d9f7-11ed-afa1-0242ac120002";
	
	public static final String ANC_FOETAL_GROWTH_AND_MATERNITY_WEIGHT_FORM_UUID = "c7ea41f2-1023-11ee-be56-0242ac120002";
	
	public static final String TB_TESTING_RESULTS = "160108AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DETECTED = "1301AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PROP_MPI_SERVER_USERNAME = "botswanaemr.mpiServerUsername";
	
	public static final String PROP_MPI_SERVER_URL = "botswanaemr.mpiServerUrl";
	
	public static final String CR_FETCH_URL = "botswanaemr.openCRServerFetchurl";
	
	public static final String CR_SAVE_PATIENT_URL = "botswanaemr.openCRSavePatientUrl";
	
	public static final String CR_FACILITY_ID_URL = "http://openclientregistry.org/fhir/facility";
	
	public static final String CR_SOURCE_SYSTEM_URL = "http://openclientregistry.org/fhir/source";
	
	public static final String OPEN_CR_IDENTIFIER_ATTRIBUTE = "http://omang.bw.org/ext/identifier/internalid";
	
	public static final String CR_SOURCE_SYSTEM_CODE = "openmrs";
	
	public static final String OPEN_CR_FACILITY_ID_METADATA = "botswanaemr.healthCenterName";
	
	public static final String PROP_MPI_PASSWORD = "botswanaemr.mpiServerPassword";
	
	public static final String COUGH = "143264AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String FEVER = "140238AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String WEIGHT_LOSS = "1858AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NIGHT_SWEAT = "133027AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ENLARGED_LYMPH_NODES = "70151c0e-7b3c-450c-8fbf-b58f83d87e92";
	
	public static final String TB_CONTACT_LAST_2_YEARS = "124068AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DECREASED_PLAYFULNESS = "4ccc699e-dc80-4e1d-8948-014ac7bf949b";
	
	public static final Integer YES_CONCEPT_ID = 1065;
	
	public static final String DOT_ENCOUNTER_TYPE_UUID = "2909d9e6-5037-4d7d-899b-e2113b413965";
	
	public static final String REMARKS_CONCEPT_UUID = "160632AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DATE_CONCEPT_UUID = "160753AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//No sure about this one
	public static final String DOT_ENCOUNTER_ROLE_UUID = "a0b03050-c99b-11e0-9572-0800200c9a66";
	
	public static final String SURGEON_ENCOUNTER_ROLE_UUID = "73bbb069-9781-4afc-a9d1-54b6b2270e03";
	
	public static final String ANESTHETIST_ENCOUNTER_ROLE_UUID = "240b26f9-dd88-4172-823d-4a8bfeb7841f";
	
	public static final String SCRUB_NURSE_ENCOUNTER_ROLE_UUID = "73bbb069-9781-4afc-a9d1-54b6b2270e04";
	
	public static final String OUTPATIENT_VISIT_TYPE_UUID = "73bbb069-9781-4afc-a9d1-54b6b2270e01";
	
	public static final String CPT_HIV_RESULTS_QUESTION = "1169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_STATUS_QUESTION = "1169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_STATUS_POSITIVE = "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NON_PHARMA_PLAN_CONCEPT_UUID = "f0b33e1c-8fbb-4dbf-b455-6d7b2972dfda";
	
	public static final String DISCLOSURE_PLAN_CONCEPT_UUID = "0f282283-abcf-4ede-a2d9-0d840d5d7745";
	
	public static final String HIV_STATUS_DISCLOSED_PARTNER_CONCEPT_UUID = "159423AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CURRENT_SEX_PARTNER_CONCEPT_UUID = "163568AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_DISCLOSED_TO_OTHER_FAMILY_MEMBERS_CONCEPT_UUID = "159424AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HIV_DISCLOSED_TO_FRIENDS_AND_NEIGHBOURS_CONCEPT_UUID = "159425AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String RELIGIOUS_LEADER_CONCEPT_UUID = "629f3379-3937-4163-b469-ee65661a2ef0";
	
	public static final String NO_DISCLOSURE_PLAN_CONCEPT_UUID = "bd0955c8-f290-4af1-a39d-628ff2b484b2";
	
	public static final String OPENMRS_ID_IDENTIFIER_NAME = "OpenMRS ID";
	
	public static final String OMANG_IDENTIFIER_NAME = "National ID";
	
	public static final String TEMPORARY_ID_IDENTIFIER_NAME = "Temporary ID";
	
	public static final String ADMINISTRATION_ROUTES_CONCEPT_UUID = "162394AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GLOBAL_PROPERTY_ORDER_DRUG_ROUTES_CONCEPT_UUID = "order.drugRoutesConceptUuid";
	
	public static final String MIGRATION_PATIENT_ATTRIBUTE = "6A9DA9E9-CFE1-466A-A5E8-861AE19E3020";
	
	public static final String MIGRATION_FACILITY_ATTRIBUTE = "C7203EEF-FCB2-4800-B7E1-E9BC6BFA4013";
	
	public static final String HTS_PNS_INDEX_FORM_UUID = "bb335850-8a08-11ed-a1eb-0242ac120002";
	
	public static final String HIV_LAB_TEST_REQUEST_FORM_UUID = "a0815c32-3569-48c6-a443-7385c4ae62fb";
	
	public static final String PNS_CONTACTS_OBS_GROUP_CONCEPT_UUID = "117e1335-e32b-4789-96df-cc7f3be72a85";
	
	public static final String RECEIVER_NAME_ATTRIBUTE_TYPE_UUID = "946c9d3f-ff6e-486d-b9c1-62d70565cd26";
	
	public static final String CASE_ID_VISIT_ATTRIBUTE = "67FF6F45-66C3-467E-B299-FD14BC6E8A76";
	
	public static final String ART_REGIMEN_ATTRIBUTE_TYPE_UUID = "4C1DDB2D-D553-4B68-A1CE-CA3188221B82";
	
	public static final String ART_STATUS_ATTRIBUTE_TYPE_UUID = "7A2F4060-9396-4A87-A42B-58EFD17BB343";
	
	public static final String STOP_ART_FORM_UUID = "5b89fff8-1b8e-4995-8b6d-dd59463ecc03";
	
	public static final String ART_REGIMEN_CHANGE_FORM_UUID = "c01da903-3cb2-4f42-a33a-b0968877582c";
	
	public static final String HTS_VERIFICATION_ENCOUNTER_TYPE_UUID = "9305dca8-8112-11ed-a1eb-0242ac120002";
	
	public static final String ART_ADHERENCE_ASSESMENT_ENCOUNTER_TYPE_UUID = "5e421412-d084-11ec-9d64-0242ac120002";
	
	public static final String ART_PAEDS_ADHERENCE_ASSESMENT_ENCOUNTER_TYPE_UUID = "90f6c6fe-e24b-11ec-8fea-0242ac120002";
	
	public static final String ADULT_INITIAL_CONSULTATION_ENCOUNTER_TYPE_UUID = "6406d528-cbd8-11ec-9d64-0242ac120002";
	
	public static final String HTS_PRIOR_TEST_ENCOUNTER_TYPE_UUID = "4e773190-8098-4afc-aec7-b45c9b3efb93";
	
	public static final String PREVIOUSLY_TESTED_QUESTION_CONCEPT_UUID = "addcd519-7637-4377-81b2-344e914078a1";
	
	public static final String PREVIOUSLY_TESTED_POSITIVE_CONCEPT_UUID = "11c7d569-81b2-47b6-94a8-5b27c751aeb9";
	
	public static final String ART_PROGRAM_ENROLMENT_FORM_UUID = "1ea3d1a3-b159-4941-9fc8-43e8418b2105";
	
	public static final String TRANSFERRED_OUT = "159492AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String BIRTH_CERT_IDENTIFIER_NAME = "Birth Certificate Number";
	
	public static final String PASSPORT_IDENTIFIER_NAME = "Passport Number";
	
	public static final String HTS_SUPPLEMENTARY_TEST_ENCOUNTER_TYPE_UUID = "9305dca8-8112-11ed-a1eb-0242ac120002";
	
	// public static final String HIV_TEST_CONCEPT_UUID = "1356AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	public static final String HIV_TEST_CONCEPT_UUID = "39b284d7-9135-4a58-95cf-7ab43a61b2d1";
	
	public static final String HIV_TEST_DIAGNOSIS_RESULT_CONCEPT_UUID = "1169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DATE_OF_HIV_TEST_CONCEPT_UUID = "564e32a9-e331-456d-a84b-fb910d49ef9e";
	
	public static final String ENCOUNTER_NOTE_CONCEPT_UUID = "162169AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIAGNOSTICS_POSITIVE_CONCEPT_UUID = "808694c0-6600-4ab0-acf7-45648bdf9ed1";
	
	public static final String HTS_PRIOR_ENCOUNTER_TYPE = "4e773190-8098-4afc-aec7-b45c9b3efb93";
	
	public static final String HTS_PRIOR_TEST_STATUS_CONCEPT_UUID = "addcd519-7637-4377-81b2-344e914078a1";
	
	public static final String SUPPLEMENTARY_TEST_RESULTS_CONCEPT_UUID = "9eb8e860-d137-405e-b067-01505b853834";
	
	public static final String VERIFICATION_POSITIVE_CONCEPT_UUID = "11c7d569-81b2-47b6-94a8-5b27c751aeb9";
	
	public static final String VMMC_48HRS_REVIEW_CONCEPT_UUID = "6f59826d-b6e5-47e8-b29c-23b7a3d561da";
	
	public static final String VMMC_7DAYS_REVIEW_CONCEPT_UUID = "77b6a3f6-ea43-43cb-bef7-be33c2eea413";
	
	public static final String VMMC_42DAYS_REVIEW_CONCEPT_UUID = "a40e97b2-2bad-4f2a-9504-1f740b2b4df8";
	
	public static final String VMMC_REVIEW_PERIOD_CONCEPT_UUID = "2e1f129b-a12b-4a52-81db-3e4919afa4af";
	
	public static final String VMMC_COUNSELING_OUTCOME_CONCEPT_UUID = "ad5235bb-8354-4473-8b93-8c102eb93e60";
	
	public static final String AGREED_CONCEPT_UUID = "159413AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DISAGREED_CONCEPT_UUID = "167156AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DEFERRED_CONCEPT_UUID = "5340fe8b-9d57-41d1-88ee-a39e32db5ea2";
	
	public static final String VIA_SCREENING_ENCOUNTER_TYPE_UUID = "936f1626-7291-45b9-bdcf-1f57032ef098";
	
	public static final String SCREENING_RESULT_CONCEPT_UUID = "166664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CLINICAL_SUSPICION_OF_CANCER_CONCEPT_UUID = "159008AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ECG_CONCEPT_UUID = "160845AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String RADIOLOGY_CONCEPT_UUID = "bf019a88-47c6-4545-add8-8f4faa206465";
	
	public static final String ULTRASOUND_CONCEPT_UUID = "f5f43799-3eaf-46bb-a4c8-fa5bb524a66b";
	
	public static final String CONCEPT_ADVERSE_EVENT = "Adverse Event";
	
	public static final String CONCEPT_DATE_ONSET = "Date of onset of reaction";
	
	public static final String CONCEPT_REPORT_DATE = "Date";
	
	public static final String ADVERSE_EVENTS_FORM_UUID = "675d55e7-a9d4-4da6-b538-7fa255e932f1";
	
	public static final String PRESCRIBER_ATTRIBUTE_TYPE = "105debe2-e408-4f39-8678-fbf11ec3a5cb";
	
	public static final String TB_PATIENT_REGIMEN_UUID = "64fb4291-043f-4c3e-9faa-8e1b864243ba";
	
	public static final String TB_TREATMENT_STATUS = "64fb4291-043f-4c3e-9faa-8e1b864243ba";
	
	public static final Object TB_REGIMEN_CHANGE_FORM_UUID = "e3bcc590-9308-4225-b0c9-4bbc064ef98d";
	
	public enum summaryType {
		REGISTRATION,
		SCREENING,
		PHARMACY,
		PROGRAM,
		CONSULTATION,
		ENROLLMENT
	}
	
	public enum labStatus {
		PENDING,
		COMPLETED
	}
	
	public enum registrySource {
		BDRS,
		OMANG,
		ORACLE
	}
	
	public enum typeOfHtsTest {
		TEST_1,
		TEST_2
	}
	
	public enum PrescriptionStatus {
		AWAITING,
		COMPLETED,
		IN_PROGRESS,
		PARTIALLY_COMPLETED,
		PARTIALLY_FILLED
	}
	
	//concepts will be declared here
	//Program outcomes
	public static final String TB_TREATMENT_OUTCOME = "159786AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CURED_OUTCOME = "159791AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String COMPLETED_TREATMENT = "1663AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TREATMENT_FAILURE = "159874AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIED = "160034AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String LTFU = "5240AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//Tb test concepts from the lab form
	
	public static final String TB_AFB = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_CULTURE = "159982AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_LPA = "164395AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_DST = "160046AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TB_GENE_EXPERT = "164945AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	// Screening form concepts
	public static final String CONDITIONS_GROUPING_CONCEPT = "159947AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ALLERGY_GROUPING_CONCEPT = "160647AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MEDICATIONS_GROUPING_CONCEPT = "163711AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DEFAULT_MEDICATION_FREQUENCY = "162135AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MEDICATION_DOSE_CONCEPT = "160856AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MEDICATION_DOSAGE_UNIT_CONCEPT = "1519AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MEDICATION_ROUTE_CONCEPT = "c7e89cec-973f-47b7-9b87-09d718351968";
	
	public static final String DEFAULT_CARE_SETTING = "OUTPATIENT";
	
	//TB Lab results question for elligibilty to TB enrollment
	public static final String GENE_XPERT_RESULTS = "49c3c1ff-3110-4109-a656-87cfe542eab6";
	
	public static final String Sputum_for_acid_bacili_results = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Tb_culture_results = "159982AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//Tb lab results concepts
	public static final String Mycobacterium_tuberculosis = "160007AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Trace = "1874AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Low = "1407AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Medium = "164944AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String High = "1408AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Mycobacterium_tuberculosis_detected_with_rifampin_resistance = "162203AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Mycobacterium_tuberculosis_detected_without_rifampin_resistance_misc = "162204AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Mycobacterial_Infection_Excluding_Tuberculosis_and_Leprosy = "133604AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String POSITIVE = "703AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NEGATIVE = "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Positive_one_plus = "1362AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Positive_two_plus = "1363AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Positive_three_plus = "1364AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String Scanty = "159985AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//TB drug tracking form concepts
	public static final String PHYSICIAN_CONCEPT_UUID = "1574AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GUARDIAN_CONCEPT_UUID = "160639AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String COMMUNITY_HEALTH_WORKER_CONCEPT_UUID = "1555AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String SELF_CONCEPT_UUID = "978AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MISSED_CONCEPT_UUID = "162854AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ADMINISTERED_CONCEPT_UUID = "164373AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DATE_ADMINISTERED_CONCEPT_UUID = "160753AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PATIENT_NOTE_CONCEPT = "165095AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//MDR_TB program
	public static final String MDR_TB_PROGRAM_UUID = "e5cc0479-8e22-11e8-b630-0242ac130002";
	
	//MALARIA program
	public static final String MALARIA_PROGRAM_UUID = "e5cc05b0-8e22-11e8-b630-0242ac130002";
	
	//TB program
	public static final String TB_PROGRAM_UUID = "938b5435-dc0e-4d4e-bb6f-79befd636d8f";
	
	//VMMC Program
	public static final String VMMC_PROGRAM_UUID = "8d52304c-5834-11ed-9b6a-0242ac120002";
	
	public static final String VMMC_TECHNIQUE_CONCEPT_UUID = "167119AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String VMMC_OPERATION_DIAGNOSIS_UUID = "6db3c3e6-1596-4c58-bade-de525d3241c3";
	
	public static final String VMMC_ANAESTHETIC_TYPE_RECORD_UUID = "bdabcbc3-6371-4f1f-8cb2-72f120685d74";
	
	public static final String VMMC_ANAESTHETIC_RECORD_DRUG_GIVEN_UUID = "1282AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//TPT program
	public static final String TPT_PROGRAM_UUID = "ec11f413-86bb-4075-bee0-7e8a19460f16";
	
	public static final String TPT_CONTRAINDICATION_CONCEPT_UUID = "8a8082a0-725f-43b0-8676-73d02150beff";
	
	public static final String TPT_ELIGIBLE_CONCEPT_UUID = "e1fc5ea6-43a9-4076-bb9a-880b2af36e12";
	
	//ART program
	public static final String ART_PROGRAM_UUID = "e5cc020a-8e22-11e8-b630-0242ac130002";
	
	public static final String CPT_PROGRAM_UUID = "d7bf0dcd-06e8-4291-81bc-be9669554c2c";
	
	public static final String OUTPATIENT_PROGRAM_UUID = "32ec853b-d0f4-4718-84ab-6f333fa5e085";
	
	public static final String SURGICAL_PROGRAM_UUID = "c9387f78-677e-496f-a329-b479e60ebd13";
	
	public static final String WEIGHT_CONCEPT_UUID = "5089AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TEST_ORDER_TYPE_UUID = "52a447d3-a64a-11e3-9aeb-50e549534c5e";
	
	public static final String CARE_SETTING_UUID = "6f0c9a92-6f24-11e3-af88-005056821db0";
	
	public static final String LAB_REQUEST_ENCOUNTER_TYPE = "1E88B447-7B50-440E-B475-09844AA0E86E";
	
	public static final String GENE_X_PERT_CONCEPT_UUID = "164945AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MICROSCOPY_CONCEPT_UUID = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NTRL_RESULTS_CONCEPT_UUID = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MYCOBACTERIAL_CONCEPT_UUID = "164395AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DST_CULTURE_CONCEPT_UUID = "160046AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//ART Program
	public static final String ART_LAB_REQUEST_ENCOUNTER_TYPE = "b0754e64-ea6c-11ec-8fea-0242ac120002";
	
	public static final String ART_TPT_REGISTER_FORM_UUID = "6659cf9e-4d80-11ee-be56-0242ac120002";
	
	//encounter roles
	public static final String DEFAULT_ENCOUNTER_ROLE_UUID = "a0b03050-c99b-11e0-9572-0800200c9a66";
	
	//TB lab results
	public static final String GENE_DATE_RECEIVED_CONCEPT_UUID = "39a8cb7e-4872-4691-8028-843d7376ac4f";
	
	public static final String GENE_RESULTS_CONCEPT_UUID = "49c3c1ff-3110-4109-a656-87cfe542eab6";
	
	public static final String MTB_DETECTED_CONCEPT_UUID = "8cb90829-7d55-4284-ad4e-51109379c56e";
	
	public static final String MICROSCOPY_DATE_RECEIVED_CONCEPT_UUID = "159963AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MICROSCOPY_RESULTS_CONCEPT_UUID = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String AT_WHICH_MONTH_CONCEPT_UUID = "1ce74eee-92dd-46d1-b2ce-ff5f00e86ebd";
	
	//Vitals concepts
	public static final String DIASTOLIC_BP = "5086AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String SYSTOLIC_BP = "5085AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String TEMPERATURE = "5088AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String WEIGHT = "5089AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String HEIGHT = "5090AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String RESPIRATORY_RATE = "5242AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PULSE = "5087AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PUEPERIUM_PULSE = "bf659d7d-712a-4004-be82-900e317924f7";
	
	public static final String HEAD_CIRCUMFERENCE = "5314AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String BSA = "980AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String BMI = "1342AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GLUCOSE_LEVEL = "4c879801-9dcd-4578-9454-153a36b95a04";
	
	public static final String OXYGEN_SATURATION = "163597AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String LMP = "1427AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PREGNANCY_STATUS = "5272AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONSCIOUS = "77d2ecfa-ec90-4034-9f9e-227dd2d42d40";
	
	public static final String RESPONSIVE = "9d29041c-cce5-4c24-af69-8c2555ce0bd3";
	
	public static final String FOETAL_WEIGHT_CONCEPT_UUID = "720fe9cd-0151-4a62-97ee-b6044950e724";
	
	//other concepts
	public static final String PHYSICAL_EXAMINATION_CONCEPT_UUID = "1391AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";//change to the right concept
	
	public static final String PHYSICAL_EXAM_GROUPING_CONCEPT = "a5034687-46d0-460c-8efc-c6e3e843ad9d";
	
	public static final String HEENT_CONCEPT_UUID = "163045AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CARDIOVASCULAR_CONCEPT_UUID = "163046AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String SKIN_CONCEPT_UUID = "160981AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ENDOCRINE_CONCEPT_UUID = "774af88a-eb33-4be1-ad21-6923b30d8375";
	
	public static final String ADDITIONAL_NOTES_CONCEPT_UUID = "165095AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";//change to correct concept
	
	public static final String ASSESSMENT_GROUPING_CONCEPT_UUID = "cfaac92f-1d4f-4be0-b000-655f2110d987";
	
	public static final String GI_CONCEPT_UUID = "160951AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CHEST_EXAMINATION_CONCEPT_UUID = "160689AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GU_WOMEN_HEALTH_CONCEPT_UUID = "b490bcd3-350b-4cf7-a372-2036c732a83f";
	
	public static final String CNS_CONCEPT_UUID = "160995AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GROWTH_DEVELOPMENT_CONCEPT_UUID = "295b5892-3ddc-4392-b385-e05b3796927a";
	
	public static final String URO_GENITAL_CONCEPT_ID = "16fa7497-92f4-4a70-a685-d0c874c5d443";
	
	public static final String ASSESSMENT_ADDITIONAL_NOTES_CONCEPT_UUID = "a500886e-c3a7-42a2-8257-9d1d455c3c7f"; //To fetch from OCL
	
	public static final String DIETARY_PLAN_CONCEPT_UUID = "163189AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NURSING_DIAGNOSIS_CONCEPT_UUID = "0e9ca1df-66e7-4142-9d2c-acf58523a6de";
	
	public static final String POSITIVE_HIGH_HPV_CONCEPT_UUID = "bc5ad875-0743-4143-9b82-5dae5ba5eff8";
	
	public static final String RESULT_INTERPRESENTATION_CONCEPT_UUID = "5ca8af78-68ea-4717-b4af-6c899812c706";
	
	public static final String HUMAN_PAPILLOMAVIRUS_SUBTYPE_16_CONCEPT_UUID = "9fe7993b-b1f1-4221-829f-fb429ec84b18";
	
	public static final String HUMAN_PAPILLOMAVIRUS_SUBTYPE_18_CONCEPT_UUID = "ae97950d-4884-442a-9493-2ac021aeaf8f";
	
	public static final String HUMAN_PAPILLOMAVIRUS_SUBTYPE_45_CONCEPT_UUID = "71bbdc06-796d-4bb2-b839-4bb5c65771b4";
	
	//Triage concept sets
	public static final String[] ALLERGIES_CONCEPT_SETS = new String[] { "162554AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
	        "162552AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "162553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" };
	
	public static final String SYMPTOMS_GROUPING_CONCEPT_UUID = "1727AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIET_GROUPING_CONCEPT_UUID = "1d8eb75d-1e2c-40ca-9d3e-a94065e723c2";
	
	public static final String TYPE_OF_MEAL_CONCEPT_UUID = "2e42c8f3-1a20-4469-bf7e-e1d9eeaaeea4";
	
	//todo Needs new concept
	public static final String MEAL_CONCEPT_UUID = "162545AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String BREAKFAST_CONCEPT_UUID = "ba1f8062-a8b8-4e5e-908d-606fd1dadefe";
	
	public static final String AM_SNACK_CONCEPT_UUID = "b85bddad-8f6d-415d-a6f3-401ed566282d";
	
	public static final String LUNCH_CONCEPT_UUID = "fd784dd1-3d54-40ec-83a6-4c6b7babafba";
	
	public static final String PM_SNACK_CONCEPT_UUID = "6593b2db-16cf-4164-b6bf-628b6e6ac0f3";
	
	public static final String DINNER_CONCEPT_UUID = "47be9843-21b5-4454-8111-fbdcd6ebb2a9";
	
	public static final String CALORIES_CONCEPT_UUID = "a9784813-3a89-42d0-be86-70c0a3a304fa";
	
	public static final String CARBS_CONCEPT_UUID = "8d05682a-a2e3-499c-8343-3c630869dfb8";
	
	public static final String PROTEINS_CONCEPT_UUID = "45518754-f940-42cc-aa4f-cafd7e7dd604";
	
	public static final String FATS_CONCEPT_UUID = "c70fd141-f5ef-46ec-940d-b6e32c857f57";
	
	public static final String DIETARY_PLAN_ENCOUNTER_TYPE_UUID = "d592af24-0610-453e-a027-a8cd7759b61c";
	
	public static final String NON_PHARMA_PLAN_ENCOUNTER_TYPE_UUID = "699C7881-BD70-49CA-A529-BC366D56CF5F";
	
	public static final String SYMPTOMS_CONCEPT = "161602AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIAGNOSIS_CONCEPT_CLASS = "Diagnosis";
	
	public static final String PROCEDURE_CONCEPT_CLASS = "Procedure";
	
	public static final String LIFESTYLE_GROUPING_CONCEPT_UUID = "9a42abff-1199-4727-9c4d-c9968f0719cc";
	
	public static final String PATIENT_COMPLAINT_GROUPING_CONCEPT_UUID = "d8d64e4c-5315-4ef3-8c25-a86b2ec0121c";
	
	public static final String LIFESTYLE_CONCEPT_UUID = "88e4e67f-773e-43ce-80c8-29b01ab272e1";
	
	public static final String PATIENT_COMPLAINT_CONCEPT_UUID = "160531AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PAST_OPERATIONS_GROUPING_CONCEPT = "160714AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PAST_OPERATION_CONCEPT = "1651AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PAST_OPERATION_YEAR_CONCEPT = "164393AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PAST_OPERATION_NOTES_CONCEPT = "160716AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIET_GROUPING_CONCEPT = "bb8a8f81-7b21-4ca0-946f-f4daf78d5be8";
	
	public static final String DIET_CONCEPT = "a9a8f9a1-1e2e-49cb-a405-a6f507ead6b4";
	
	public static final String DIET_AMOUNT_CONCEPT = "162524AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DIET_NOTES_CONCEPT = "ade20a36-2bb2-4006-b274-891b3d824e5b";
	
	public static final String DRUG_ORDER_ENCOUNTER_TYPE = "c1616683-5826-452c-a534-4ff4dda62ced";
	
	public static final String MILLIGRAMS_DOSE_UNIT = "161553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DAYS_DURATION_UNIT = "1072AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String REFILL_REPEAT_NUMBER_OF_OCCURRENCES = "162582AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String IN_PATIENT_CARE_SETTING = "c365e560-c3ec-11e3-9c1a-0800200c9a66";
	
	public static final String DRUG_ORDER_TYPE_UUID = "131168f4-15f5-102d-96e4-000c29c2a5d7";
	
	public static final String TEST_ORDERED_CONCEPT_UUID = "1271AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//General concepts
	public static final String SEVERITY_CONCEPT_UUID = "162820AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String FACILITY_CONCEPT_UUID = "5aa9abb8-d047-4853-a98a-0e64c3f9c618";
	
	//Referral Concepts
	public static final String REFERRING_DEPARTMENT_CONCEPT_UUID = "163775AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String RECEIVING_DEPARTMENT_CONCEPT_UUID = "164918AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//user properties
	public static final String USER_PROPERTY_NOTIFY_ON_NEW_PATIENT_IN_POOL_KEY = "notifyOnNewPatientsInPool";
	
	public static final String USER_PROPERTY_NOTIFY_ON_CURRENT_PATIENT_SELECTION_KEY = "notifyOnCurrentPatientSelection";
	
	public static final String USER_PROPERTY_NOTIFY_ON_CONSULTATION_EDITS_KEY = "notifyOnConsultationEdits";
	
	public static final String USER_PROPERTY_NOTIFY_LAB_RESULTS_KEY = "notifyOnLabResults";
	
	public static final String USER_PROPERTY_NOTIFY_SUCCESSFUL_ADMISSIONS_KEY = "notifyOnSuccessfulAdmissions";
	
	public static final String USER_PROPERTY_CURRENTLY_LOGGED_FACILITY = "userCurrentlyLoggedInFacility";
	
	public static final String CURRENT_SERVICE_DELIVERY_POINT = "currentDeliveryPoint";
	
	public static final String VISIT_LOCATION_TAG_NAME = "Visit location";
	
	public static final String LAB_ORDER_SITE_TAG_NAME = "Lab Ordering Site";
	
	public static final String BDF_FACILITY_LOCATION_TAG_NAME = "BDF Facility";
	
	public static final String LOGIN_LOCATION_TAG_NAME = "Login Location";
	
	public static final String QUEUING_LOCATION_TAG_NAME = "Queue Room";
	
	public static final String POST_REGISTRATION_LOCATION_TAG_NAME = "Post Registration";
	
	public static final String HTS_LOCATION_TAG_NAME = "HTS Service Point";
	
	public static final String REFERRING_LOCATION_TAG_NAME = "Referring Location";
	
	public static final String REFERRAL_DESTINATION_LOCATION_TAG_NAME = "Referral Destination";
	
	public static final String MILLIGRAM_CONCEPT_UUID = "161553AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MILLILITER_CONCEPT_UUID = "162263AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String BOX_CONCEPT_UUID = "162396AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String EACH_CONCEPT_UUID = "162399AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DS_TB_DRUGS = "botswanaemr.DSTBDrugs";
	
	public static final String TASK_APPOINTMENT_PATIENT_UUID = "botswanaemr.appointments.fakePatient";
	
	public static final String EDUCATION_LEVEL_CONCEPT_UUID = "1712AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONTACT_PERSON_ATTRIBUTE_TYPE_UUID = "77FA6739-7C41-4898-AEA5-F92EC1A71A04";
	
	public static final String CONTACT_PATIENT_NAME_CONCEPT_UUID = "163755AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//164351AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	public static final String CONTACT_GROUPING_CONCEPT_UUID = "164351AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONTACT_P_NAME_CONCEPT_UUID = "163258AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CONTACT_PHONE_NUMBER_CONCEPT_UUID = "159635AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CHILD_NOT_CHILD_CONCEPT_UUID = "01f911b2-d68f-47f6-a4b4-155cfc066b81";
	
	public static final String LAST_ENCOUNTER_DATE_CONCEPT_UUID = "577e7811-f87c-411b-b25f-c389ef41991c";
	
	public static final String SEXUAL_PARTNER_CONCEPT_UUID = "5617AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String IVDU_CONCEPT_UUID = "c214c523-7e11-4d44-bc53-e714df746898";
	
	public static final String MEDICATION_QUANTITY_CONCEPT_UUID = "160856AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//Roles
	public static final String ROLE_REGISTRATION_CLERK = "Organizational: Registration Clerk";
	
	public static final String ROLE_DOCTOR = "Organizational: Doctor";
	
	public static final String ROLE_PHARMACIST = "Organizational: Pharmacist";
	
	public static final String ROLE_NURSE = "Organizational: Nurse";
	
	public static final String ROLE_HOSPITAL_ADMIN = "Organizational: Hospital Administrator";
	
	public static final String ROLE_SYSTEM_ADMIN = "Organizational: System Administrator";
	
	public static final String ROLE_STOCK_MANAGER = "Organizational: Stores Manager";
	
	public static final String ROLE_LAB_SERVICES = "Organizational: Lab Services";
	
	public static final String ROLE_PROVIDER = "Provider";
	
	public static final String ROLE_TB_MANAGER = "Organizational: TB Manager";
	
	public static final String ROLE_ART_MANAGER = "Organizational: ART Manager";
	
	public static final String ROLE_VMMC_MANAGER = "Organizational: VMMC Manager";
	
	public static final String ROLE_HTS_MANAGER = "Organizational: HTS Manager";
	
	public static final String ROLE_SRH_MANAGER = "Organizational: SRH Manager";
	
	public static final String ROLE_PRIVILEGE_LEVEL_HIGH = "Privilege Level: High";
	
	//facility_location_attributes
	public static final String MASTER_FACILITY_CODE_UUID = "8a845a89-6aa5-4111-81d3-0af31c45c002";
	
	public static final String MASTER_FACILITY_STATUS_UUID = "3a27b25c-0c2b-11ed-838d-d3ac71457867";
	
	public static final String FACILITY_ADMINS_UUID = "79508a52-2270-11ed-bce7-2f5286dce306";
	
	public static final String OTHER_CODED_CONCEPT_UUID = "5622AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	//Employment sector concepts
	public static final String[] EMPLOYMENT_SECTOR_CONCEPTS = { "d0deb690-648d-4ba5-ad5e-e1dd6e93dd58",
	        "c802bdfc-5329-43ef-b8d4-c39a30abaf89", "673fbbd6-62e2-41ce-8c21-07d4e0945f97",
	        "a884b6f3-2516-4a22-b8fc-eee534766490", "3808fe32-5c3a-4bbe-bc5a-d583c5b49fb0",
	        "2b2cdf30-1330-4f70-aa13-5f9a98982b17", "ab59b640-04f0-4f1b-83b1-92fa8e8dff0e",
	        "8eacfc8d-7124-4b73-968e-54927ddcc360", "88f59059-838d-4acb-81ad-83cfea691b6e",
	        "c8d3d621-c39d-4d60-9937-e1554336b416", "d311b54e-6118-498f-afbf-551300c8d7b5",
	        "1175AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", OTHER_CODED_CONCEPT_UUID };
	
	public static final String PATIENT_AGE_CONCEPT_UUID = "163540AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DURATION_UNITS_CONCEPT = "1732AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String SYMPTOM_DURATION_CONCEPT = "1731AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String YES_CONCEPT_UUID = "1065AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NO_CONCEPT_UUID = "1066AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String NEGATIVE_UUID = "664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String UNKNOWN_CONCEPT_UUID = "1067AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String PURPOSE_ATTRIBUTE_TYPE_UUID = "de22f736-71fd-43fd-9e20-f81528bbd71a";
	
	public static final String DISTRIBUTOR_NAME_ATTRIBUTE_TYPE_UUID = "14123120-beda-48c7-aaeb-63290e4c7c3d";
	
	public static final String ORDER_NUMBER_ATTRIBUTE_TYPE_UUID = "B305C8DF-3BD4-4B98-83BB-0EAC6DF3B633";
	
	public static final String GEN_12_NUMBER_ATTRIBUTE_TYPE_UUID = "CAB259D7-AA9C-4C5C-8DA3-AA6C40891462";
	
	public static final String SELF_TEST_KIT_UUID = "fd20ab93-8380-48aa-9caa-1d3826c20256";
	
	public static final String DETERMINE_KIT_UUID = "90A2A098-7E6C-4819-90F3-A60C80C26B4D";
	
	public static final String UNIGOLD_KIT_UUID = "f7aa65bc-054d-48a7-9b94-a601f5846513";
	
	public static final String REGIMEN_CONCEPT_UUID = "2ed524c5-1817-4c5d-954c-abebd10b52a0";
	
	public static final String REGIMEN_LINE_CONCEPT_ID = "164515AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ART_NEXT_DATE_CONCEPT_UUID = "5096AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CWC_BABY_DATE_CONCEPT_UUID = "ac8cf0fb-31c2-4d89-903e-b2baef9df434";
	
	public static final String PNC_VISIT_DATE_CONCEPT_UUID = "2c5af8d2-e215-4582-8d11-319b1d347049";
	
	public static final String PV_BLEEDING_CONCEPT_UUID = "7cc85c4a-146d-4bdc-ad79-12e3704f1cee";
	
	public static final String SCM_REVIEW_DATE_CONCEPT_UUID = "2e1f129b-a12b-4a52-81db-3e4919afa4af";
	
	public static final String REVIEW_DATE_1 = "6f59826d-b6e5-47e8-b29c-23b7a3d561da";
	
	public static final String REVIEW_DATE_2 = "77b6a3f6-ea43-43cb-bef7-be33c2eea413";
	
	public static final String REVIEW_DATE_3 = "a40e97b2-2bad-4f2a-9504-1f740b2b4df8";
	
	//Max result sql queries should return
	public static final int MAX_SQL_RESULT = 10000;
	
	public static final String LAB_TEST_INTERPRETATION = "e1a57237-2b21-44a1-bf58-2ae1c5261c0c";
	
	public static final String LAB_RESULTS_TEXT = "2054bc21-befd-4e33-8e3a-b560a7641066";
	
	public static final String ART_PREP_FORM_UUID = "f42192cb-9cc9-4e45-a916-81f6db5a0426";

	public static final String ART_PREP_INITIATION_FORM_UUID = "C0C6C022-218F-44CC-B687-777BA3CB0FD6";

	public static final String ART_PEP_INITIATION_FORM_UUID = "2E7B63BE-38C5-484F-A0AF-D3B42A281D5C";

	public static final String ART_PEP_FOLLOW_UP_FORM_UUID = "886F7BE0-FF98-41A4-B81E-0A5778A61853";
	
	public static final String ART_PREP_FORM_ENCOUNTER_TYPE_UUID = "0633fdb7-07af-4d68-b895-6af808e72ed6";

	public static final String ART_PREP_INITIATION_FORM_ENCOUNTER_TYPE_UUID = "8690A680-5915-4740-845B-D0614864000F";

	public static final String ART_PEP_FOLLOW_UP_FORM_ENCOUNTER_TYPE_UUID = "F16493D0-11FD-4AD0-A7A4-A42CD2A70F32";

	public static final String ART_PEP_INITIATION_FORM_ENCOUNTER_TYPE_UUID = "E3AD661C-CAA0-424F-9DBA-37531FF6B9F6";
	
	public static final String ART_PEP_FORM_UUID = "56b0db57-4187-4d48-b98f-ceae2b336086";
	
	public static final String ART_PEP_FORM_ENCOUNTER_TYPE_UUID = "a0bcee2f-ccbe-497a-991f-5b7525067424";
	
	public static final String ART_PrEP_FOLLOW_UP_FORM_UUID = "5b6822e9-a9f6-47a6-b329-dfa7da57520f";
	
	public static final String ART_PREP_FOLLOW_UP_FORM_ENCOUNTER_TYPE_UUID = "998d9157-806f-4965-bbd8-f1fe2f8114c1";

	public static final String ART_FOLLOW_UP_FORM_UUID = "B8FE7EA8-E820-4659-BECE-7A37812B9613";

	public static final String ART_FOLLOW_UP_FORM_ENCOUNTER_TYPE_UUID = "78EC3176-4324-423B-AD51-B37E7AFE888E";
	
	public static final String MATERNITY_REGISTRY_FORM_UUID = "02C53CBE-7E74-47B8-A5E2-06C55DD7E032";
	
	public static final String MATERNITY_REGISTRY_ENCOUNTER_TYPE_UUID = "B9FB2125-BD38-4346-9B6D-A7DA940D3E89";
	
	public static final String POSTNATAL_REGISTRY_FORM_UUID = "ac688c91-d327-40d3-8a0c-5c1c51ca9ef7";
	
	public static final String POSTNATAL_REGISTRY_ENCOUNTER_TYPE_UUID = "abae5d8d-6854-4291-98cc-a156dbcfa1ee";
	
	public static final String POSTNATAL_FORM_SECOND_VISIT_ENCOUTER_TYPE = "88c02cb2-f508-4304-8859-e5a51e361cf1";
	
	public static final String POSTNATAL_FORM_THIRD_VISIT_ENCOUTER_TYPE = "f02221f6-ec34-47eb-a2a7-fb5cf735041c";
	
	public static final String ANTENATAL_REGISTRY_FORM_UUID = "874a4e38-9307-4683-8ab4-540fef3f95bf";
	
	public static final String ANTENATAL_REGISTRY_ENCOUNTER_TYPE_UUID = "0c536bec-1cfb-4711-afbb-f3388a9a90f3";
	
	public static final String COHORT_REGISTRY_FORM_UUID = "baf3d37c-46dd-4abd-b5cc-29ffea27ec19";
	
	public static final String COHORT_REGISTRY_ENCOUNTER_TYPE_UUID = "fb59ffae-a674-4408-9f0b-d3df7020da76";
	
	public static final String DOMICILIARY_FORM_UUID = "cd79825b-0c2e-4cbf-99c8-63c61764cb54";
	
	public static final String DOMICILIARY_ENCOUNTER_TYPE_UUID = "d688652e-d0c0-4e24-8425-adb5b6ec8913";
	
	public static final String ANC_FOETAL_GROWTH_AND_MATERNITY_ENCOUNTER_TYPE = "d417feb0-1023-11ee-be56-0242ac120002";
	
	public static final String FUNDAL_HEIGHT_CONCEPT_UUID = "1439AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MATERNAL_WEIGHT_CONCEPT_UUID = "5089AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GESTATION_WEEK_CONCEPT_UUID = "e22bc6cd-e51a-462a-bcdd-c6495642f387";
	
	public static final String PUERPERIUM_FORM_UUID = "d544c6bb-1daa-438c-8fc5-fa422fc329fa";
	
	public static final String PUERPERIUM_ENCOUNTER_TYPE_UUID = "74eac899-c8eb-4676-9f43-b83d4721b898";
	
	public static final String POSTNATAL_PARTOGRAPH_FORM_UUID = "491b292f-b056-4af7-ad23-97fb42045375";
	
	public static final String POSTNATAL_PARTOGRAPH_ENCOUNTER_TYPE_UUID = "0881d7f8-3de0-448a-a147-0f500a14906a";
	
	public static final String LABOUR_AND_DELIVERY_ENCOUNTER_TYPE_UUID = "8698e9a5-3b14-485b-b4d0-49bdc9ed1fa4";
	
	public static final String PARTOGRAPH_OBSERVATION_ENCOUNTER_TYPE_UUID = "025d3595-c93b-43dc-8796-177e1b00fb87";
	
	public static final String CONTRACTIONS_PER_TEN_MINUTES = "d989c325-d9cc-4d6a-bfac-d208f206aafc";
	
	public static final String URINE = "de2c9127-3913-4e24-99a3-812f2188e2f5";
	
	public static final String FETAL_HEART_RATE = "7a0b503c-a6f9-4884-ab06-e2015c468061";
	
	public static final String CERVIX_SIZE = "5dd4da91-2108-4027-88d9-ef377a63b3de";
	
	public static final String DECENT_OF_THE_HEAD = "166528AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String LTC_OUTCOME_FORM_UUID = "77cc9be6-f747-4856-8b03-9ff01d8b4b6c";
	
	public static final String LTC_OUTCOME_ENCOUNTER_TYPE_UUID = "28f0f14c-756d-4092-bf12-95c43f90e7fb";
	
	public static final String INFANT_FACILITY_VISITS_FORM_UUID = "4066ac09-5b5c-49cd-bf2e-39667b193daf";
	
	public static final String PRIMIGRAVIDA_CONCEPT_UUID = "162970AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ANTENATAL_HEIGHT_CONCEPT_UUID = "5090AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String COTRIMOXAZOLE_DRUG_UUID = "d977a995-48f2-4a39-b269-8491b2f15f71";
	
	public static final String PREP_DRUG_UUID = "a43fe97c-d514-46a0-8783-498e409fe9dd";
	
	public static final String PEP_DRUG_UUID = "410778cc-46ec-4f2b-ad49-14b3fe478eea";
	
	public static final String TPT_DRUG_UUID = "38828cdd-18da-4258-b56a-38a95946bec3";
	
	public static final String ADD_APPOINTMENT_FORM_UUID = "20510281-5e6a-46c3-89d8-1c4d09f06dd0";
	
	public static final String APPOINTMENT_TYPE_CONCEPT_UUID = "164181AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String APPOINTMENT_STATUS_CONCEPT_UUID = "66992b1e-1a11-4610-9b28-23a1bc1ff89a";
	
	public static final String APPOINTMENT_DATE_CONCEPT_UUID = "8ef00d3e-80a7-44f7-aafb-1cb3ab1a4bac";
	
	public static final String APPOINTMENT_RESCHEDULED_DATE_CONCEPT_UUID = "3c18ffaa-489a-4353-a0da-0e79ef0c5896";
	
	public static final String APPOINTMENT_CANCELLED_CONCEPT_UUID = "8cbe2e8e-31b2-4dda-ae50-83121980946e";
	
	public static final String APPOINTMENT_SERVICE_CONCEPT_UUID = "db7c7ea5-f0a6-4922-bdbd-18a7451e7215";
	
	public static final String APPOINTMENT_CANCEL_REASON_CONCEPT_UUID = "21ac1d62-ca62-4c6f-baf5-1cd791777223";
	
	public static final String ART_APPOINTMENT_ENCOUNTER_TYPE = "86284005-bcc6-4a8b-98a1-51063e1793d8";
	
	public static final String MATERNITY_CONCEPT_UUID = "bf2dc3ed-b7db-4c66-b219-49dc9b5acf25";
	
	public static final String[] ART_SERVICES_CONCEPT_SETS = new String[] { APPOINTMENT_SERVICE_CONCEPT_UUID,
	        "ccb2c168-91ff-4f7e-b447-a410b0ff644a", "d63d1bda-b753-40a2-ba15-69a5b5f9c4db",
	        "22c424de-b03b-484f-b85d-aa43e1e5c950", "3dde1364-b724-4940-8a53-8a6b0ff0e5b0",
	        "d17b9303-588f-49e4-aed6-2d006203ce0f", "5a427ff9-767e-44db-bfb7-04b2531ea874",
	        "bb384543-9702-4117-bb95-5ffdb2765d0f", "9fca41c0-bd1c-4337-96db-309b629deb47",
	        "588dc97f-adb3-4836-9d79-3e5ed526f46e", "5a18a891-d10b-4a98-ab04-250be890b704",
	        "8e00b2dc-7155-4774-bd1d-284dd2904046", "c12a10ad-ced0-4eef-af36-7cff3112b6a1", MATERNITY_CONCEPT_UUID,
	        "978bcfec-a7d8-4e7c-8659-a77ed3c4610a", "411d4c6c-a00a-48c8-a78b-a5db53497f4d",
	        "cfde46b1-fcae-4eba-9164-293249e3d13a", "82ad24f9-178e-440e-aca8-4f6b17e9e27f",
	        "fd256078-1448-4156-b8d2-8ed6017acef9", "02a96c4e-8d62-4c98-a33f-51ecf807865d",
	        " 228edf16-221f-453c-8267-74c33e09c89c", "8c42925e-86f8-45ce-a32d-2bf44e5c3077",
	        "8c42925e-86f8-45ce-a32d-2bf44e5c3077", "bb05228d-6489-4537-b0fd-80b006c42b13",
	        "e0e86d4e-4986-4c4f-9c94-bbd05c99386f", " f948b90f-a1ba-4cfa-a048-5e1133d94cb3",
	        "38e9fed6-240e-4661-a9c2-4ab205e1d3a4", "8b9c9c54-84ed-4b66-baaa-f9d9d362d3c1",
	        " 2f0d3c51-4f41-4d98-b0dd-36c3201811e3", "5fda667c-702f-4c64-bcda-e078916b35c6",
	        " 4019a54a-b576-40e4-b8c0-325df7ab7da1" };
	
	public static final String PATIENT_TYPE_CONCEPT_UUID = "1e646ca5-d30f-4158-9282-75b8cf8eefaf";
	
	public static final String SERVICE_TYPE_CONCEPT_UUID = "00aca038-4b21-4358-9dc6-77214c06ef70";
	
	public static final String ANC_CONCEPT_UUID = "160446AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ANC_VISIT_TYPE_UUID = "bcfcd1f8-7a12-4469-9d68-691a7f8569cf";
	
	public static final String MATERNITY_VISIT_TYPE_UUID = "99dde0fe-8138-4c32-a6c9-2269e7877626";
	
	public static final String SRH_ENROLMENT_ENCOUNTER_TYPE_UUID = "2032afde-a831-4e88-a262-533be19c7761";
	
	public static final String SRH_POST_PARTUM_ENCOUNTER_TYPE_UUID = "86e70c36-25f8-411f-a5aa-1ab4d5734740";
	
	public static final String ANC_BOOKER_CONCEPT_UUID = "f5a791d6-c0b6-4ba3-8217-9602b663fcc3";
	
	public static final String ANC_RECURRENT_CONCEPT_UUID = "d876d463-2579-4270-b566-b072bd354302";
	
	public static final String BBA_BOOKER_CONCEPT_UUID = "9408463f-c6ff-48dd-a4c6-314f24fde32b";
	
	public static final String NON_BBA_BOOKER_CONCEPT_UUID = "709de348-a974-4a1a-a284-d6cac91019bb";
	
	public static final String HOME_DELIVERIES_CONCEPT_UUID = "3c8012f1-3483-47fc-919b-689556021be8";
	
	public static final String ANC_BOOKER_DELIVERY_CONCEPT_UUID = "abd945f7-60b5-4ea1-8a09-84d5e23743a7";
	
	public static final String NON_BOOKER_CONCEPT_UUID = "207bf096-5bdb-4ac9-bc13-400ee10b9741";
	
	public static final String MOTHER_GROUP_CONCEPT_UUID = "476b4258-2e61-476b-b8a6-3992dcacd9aa";
	
	public static final String POSITION_OF_UTERUS_CONCEPT_UUID = "99e20be2-7d10-4ccf-b145-8b4c2d0eaf45";
	
	public static final String MID_LEVEL_CONCEPT_UUID = "f6072ae4-8c7b-45b0-830e-c3f4045743b5";
	
	public static final String CONSTISTENCY_OF_UTERUS_CONCEPT_UUID = "203280bb-0dc5-4b90-bc08-f094c6df1bf1";
	
	public static final String BULKY_CONCEPT_UUID = "f63ab0a0-1b53-48a4-9632-db2169e8d26a";
	
	public static final String SCREENING_THROUGH_CONCEPT_UUID = "163589AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_SCREENING_RESULT_CONCEPT_UUID = "166664AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_TREATMENT_PERFORMED_CONCEPT_UUID = "5170bad0-2fe7-422e-93dd-a940a3594952";
	
	public static final String CERVICAL_LMNP_CONCEPT_UUID = "1427AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_LAST_SCREENING_CONCEPT_UUID = "165429AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_DATE_NEGATIVE_CONCEPT_UUID = "160082AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_PITC_CONCEPT_UUID = "164163AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_PITC_RESULT_CONCEPT_UUID = "85407532-3f84-4cc3-9567-c6d1f09585d8";
	
	public static final String CERVICAL_PITC_DATE_CONCEPT_UUID = "160082AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_ON_ART_CONCEPT_UUID = "1149AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_ART_SITE_CONCEPT_UUID = "162724AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_ART_REFERRED_TO_CONCEPT_UUID = "161562AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String SELF_TESTING_POSITIVE_CONCEPT_UUID = "25cb5d6f-56e8-4614-956f-e38b8b7e1f88";
	
	public static final String SELF_TESTING_NEGATIVE_CONCEPT_UUID = "d3cfa7f5-105e-4e97-a75d-6cb52a0622b7";
	
	public static final String CERVICAL_SCREENING_PAST_CONCEPT_SCREENED_UUID = "165617AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_TREATMENT_TYPE_PERFORMED_CONCEPT_SCREENED_UUID = "166665AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String CERVICAL_TREATMENT_DATE_CONCEPT_SCREENED_UUID = "163526AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String ABNORMAL_RESULTS_CONCEPT_SCREENED_UUID = "4d93bd90-fcb9-4e4d-a78d-1fa5449fce40";
	
	public static final String CERVICAL_SCREENING_ENCOUNTER_TYPE_UUID = "9DAB4546-92CE-4A07-AA68-FC0755F86356";
	
	public static final String HIV_STATUS_ENCOUNTER_TYPE_UUID = "7741EA70-A5F0-400D-9C6D-FBC17BB85B4C";
	
	public static final String PEP_INITIATION_VISIT_TYPE_UUID = "EDDA1C33-2AD2-48C1-90B0-F4AAE516EBFE";
	
	public static final String PEP_FOLLOW_UP_VISIT_TYPE_UUID = "210AD335-FDD1-4F59-9886-917F91B78E67";
	
	public static final String PREP_FOLLOW_UP_VISIT_TYPE_UUID = "C91A6058-A54A-4EB5-9ED3-146AD6032D96";
	
	public static final String PREP_INITIATION_VISIT_TYPE_UUID = "AF100F1F-EB13-4838-84C9-119DF57121CB";
	
	public static final String ART_INITIATION_VISIT_TYPE_UUID = "30E55262-B9F2-4113-A9A9-DE6F5586A016";
	
	public static final String ART_FOLLOW_UP_VISIT_TYPE_UUID = "EB4D51BE-CBA3-4DF8-B52B-C05F4B4FB279";
	
	public static final String GLOBAL_PROPERTY_COTRIMOXAZOLE_DRUG_UUID = "botswanaemr.cotrimoxazoleDrugUuid";
	
	private static final String GLOBAL_PROPERTY_PREP_DRUG_UUID = "botswanaemr.prepDrugUuid";
	
	private static final String GLOBAL_PROPERTY_PEP_DRUG_UUID = "botswanaemr.pepDrugUuid";
	
	private static final String GLOBAL_PROPERTY_TPT_DRUG_UUID = "botswanaemr.tptDrugUuid";
	
	public static final String BABY_DETAILS_CONCEPT_UUID = "a0ea7eea-2b3a-49d9-aa6b-28f29c3ce280";
	
	public static final String NUMBER_OF_BABIES_CONCEPT_UUID = "21ed1533-be52-4ca4-b8e9-07b9ae6d24d6";
	
	public static final String BABY_NUMBER_CONCEPT_UUID = "160632AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String GP_MPI_SEARCH_ENABLED = "botswanaemr.mpiSearchEnabled";
	
	public static final String ORDERING_SITE_CONCEPT_UUID = "232176a9-1a50-40cf-8813-506236fd34b7";
	
	public static final String ORDER_ITEM_CODE_ATTRIBUTE_TYPE_UUID = "298f79c3-b4fe-489b-9fe1-8f52ba698539";
	
	public static final String RIFAMPICIN_RESISTANCE_CONCEPT_UUID = "159984AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String MTB_NOT_DETECTED_CONCEPT_UUID = "1302AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	public static final String DISPENSING_UNITS_CONCEPT_UUID = "162402AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

    public static final String DRUG_CONCEPT_UUID = "1282AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

    public static final String GENERAL_CLINICAL_REVIEW_APPOINTMENT_TYPE = "588dc97f-adb3-4836-9d79-3e5ed526f46e";

    public static final String NURSING_DIAGNOSIS_ENCOUNTER_TYPE_UUID = "c5ce65c6-263f-4eeb-a623-e60129e50be7";

    public static final String SACHET_CONCEPT_UUID = "e410fb91-60b5-4dcd-8439-44277eca5510";
	
}
