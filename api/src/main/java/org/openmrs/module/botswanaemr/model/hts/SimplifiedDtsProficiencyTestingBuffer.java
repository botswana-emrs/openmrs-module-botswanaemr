/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

public class SimplifiedDtsProficiencyTestingBuffer {
	
	private Integer proficiencyTestingBufferId;
	
	private String dtsBufferId;
	
	private String dtsLotNumber;
	
	private String dtsExpiryDate;
	
	private String dtsConditionOfBuffer;
	
	private String dtsReasonForUnacceptableBuffer;
	
	private String dtsCorrectiveActions;
	
	public Integer getProficiencyTestingBufferId() {
		return proficiencyTestingBufferId;
	}
	
	public void setProficiencyTestingBufferId(Integer proficiencyTestingBufferId) {
		this.proficiencyTestingBufferId = proficiencyTestingBufferId;
	}
	
	public String getDtsBufferId() {
		return dtsBufferId;
	}
	
	public void setDtsBufferId(String dtsBufferId) {
		this.dtsBufferId = dtsBufferId;
	}
	
	public String getDtsLotNumber() {
		return dtsLotNumber;
	}
	
	public void setDtsLotNumber(String dtsLotNumber) {
		this.dtsLotNumber = dtsLotNumber;
	}
	
	public String getDtsExpiryDate() {
		return dtsExpiryDate;
	}
	
	public void setDtsExpiryDate(String dtsExpiryDate) {
		this.dtsExpiryDate = dtsExpiryDate;
	}
	
	public String getDtsConditionOfBuffer() {
		return dtsConditionOfBuffer;
	}
	
	public void setDtsConditionOfBuffer(String dtsConditionOfBuffer) {
		this.dtsConditionOfBuffer = dtsConditionOfBuffer;
	}
	
	public String getDtsReasonForUnacceptableBuffer() {
		return dtsReasonForUnacceptableBuffer;
	}
	
	public void setDtsReasonForUnacceptableBuffer(String dtsReasonForUnacceptableBuffer) {
		this.dtsReasonForUnacceptableBuffer = dtsReasonForUnacceptableBuffer;
	}
	
	public String getDtsCorrectiveActions() {
		return dtsCorrectiveActions;
	}
	
	public void setDtsCorrectiveActions(String dtsCorrectiveActions) {
		this.dtsCorrectiveActions = dtsCorrectiveActions;
	}
}
