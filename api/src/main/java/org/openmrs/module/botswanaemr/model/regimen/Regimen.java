/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.regimen;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

import org.openmrs.BaseOpenmrsData;
import org.openmrs.Concept;
import org.openmrs.User;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = "bt_regimen")
public class Regimen extends BaseOpenmrsData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "regimen_id")
	private Integer regimenId;
	
	@Column(name = "regimen_name", length = 1024)
	private String regimenName;
	
	@ManyToOne
	@JoinColumn(name = "regimen_line")
	private RegimenLine regimenLine;
	
	@ManyToOne
	@JoinColumn(name = "concept_ref", nullable = false, foreignKey = @ForeignKey(name = "bt_regimen_concept_ref_concept_FK"))
	private Concept conceptRef;
	
	@Column(name = "uuid", unique = true, length = 38)
	private String uuid;
	
	@Column(name = "date_created", nullable = false)
	private Date dateCreated;
	
	@Column(name = "date_changed")
	private Date dateChanged;
	
	@Column(name = "date_voided")
	private Date dateVoided;
	
	@Column(name = "voided", nullable = false)
	private Boolean voided;
	
	@Column(name = "void_reason", length = 255)
	private String voidReason;
	
	@ManyToOne
	@JoinColumn(name = "creator", nullable = false)
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "changed_by")
	private User changedBy;
	
	@ManyToOne
	@JoinColumn(name = "voided_by")
	private User voidedBy;
	
	public Regimen(Integer regimenId) {
		this.regimenId = regimenId;
	}
	
	public Regimen(String regimenName, RegimenLine regimenLine, Concept conceptRef) {
		this.regimenName = regimenName;
		this.regimenLine = regimenLine;
		this.conceptRef = conceptRef;
	}
	
	@Override
	public Integer getId() {
		return getRegimenId();
	}
	
	@Override
	public void setId(Integer id) {
		setRegimenId(id);
	}
}
