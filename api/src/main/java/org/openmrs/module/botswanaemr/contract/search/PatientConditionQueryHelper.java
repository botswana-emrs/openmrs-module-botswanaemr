/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;

public class PatientConditionQueryHelper {
	
	private String condition;
	
	public PatientConditionQueryHelper(String conditionNonCoded) {
		this.condition = conditionNonCoded;
	}
	
	public String appendToJoinClause(String join) {
		if (StringUtils.isEmpty(this.condition)) {
			return join;
		}
		
		return join += " INNER JOIN conditions co on co.patient_id = p.person_id\n" + "            AND co.voided=FALSE\n"
		        + "            AND co.condition_non_coded='" + this.condition + "' \n";
	}
}
