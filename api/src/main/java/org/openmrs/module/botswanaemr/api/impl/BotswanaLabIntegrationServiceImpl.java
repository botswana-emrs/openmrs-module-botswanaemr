/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Task;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryResponse;
import org.openmrs.module.botswanaemr.api.BotswanaLabIntegrationService;
import org.openmrs.module.botswanaemr.utils.FhirUtil;

import org.openmrs.module.fhir2.api.FhirTaskService;
import org.openmrs.module.fhir2.api.search.param.TaskSearchParams;
import org.openmrs.module.fhir2.api.translators.PatientTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.client.exceptions.FhirClientConnectionException;
import ca.uhn.fhir.rest.param.ReferenceAndListParam;
import ca.uhn.fhir.rest.param.ReferenceOrListParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLHandshakeException;

@Component("botswanaemr.BotswanaLabIntegrationServiceImpl")
public class BotswanaLabIntegrationServiceImpl implements BotswanaLabIntegrationService {
	
	private FhirUtil fhirUtil = FhirUtil.getInstance();
	
	private static Log log = LogFactory.getLog(BotswanaLabIntegrationServiceImpl.class);
	// private BotswanaLabResponse botswanaRegistryResponse;
	
	@Autowired
	private PatientTranslator patientTranslator;
	
	@Autowired
	private FhirTaskService fhirTaskService;
	
	private HttpStatus getHttpStatus(Exception e) {
		if (e instanceof SQLException) {
			return HttpStatus.NOT_FOUND;
		} else if (e instanceof SSLHandshakeException || e instanceof FhirClientConnectionException
		        || e instanceof ParseException) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		} else {
			return HttpStatus.NOT_FOUND;
		}
	}
	
	@Override
	public BotswanaRegistryResponse fetchLabResultFromShr(String taskId) {
		BotswanaRegistryResponse botswanaRegistryResponse = new BotswanaRegistryResponse();
		// Send a request to the SHR to fetch the lab result using the taskId to this
		// end point "GET /SHR/fhir/Task/?_id=TaskID&_include=*"
		// This will get Get information about the specified task as well as the other
		// resources : Practitioner, Location, Organization, Patient, ServiceRequest,
		// DiagnosticReport
		// If the request is successful, return the lab result
		// The lab result is contained in the observation resource that resides in the
		// DiagnosticReport resource
		// If the request is not successful, return an error message
		// If the taskId is not found, return an error message
		try {
			Bundle resultsBundle = this.fhirUtil.getLabClient().search().forResource(org.hl7.fhir.r4.model.Task.class)
			        .where(org.hl7.fhir.r4.model.Task.RES_ID.exactly().identifier(taskId)).include(new Include("*"))
			        .returnBundle(org.hl7.fhir.r4.model.Bundle.class).execute();
			if (resultsBundle.getEntry().size() > 0) {
				// get the task from the bundle
				org.hl7.fhir.r4.model.Task task = null;
				org.hl7.fhir.r4.model.DiagnosticReport diagnosticReport = null;
				org.hl7.fhir.r4.model.ServiceRequest serviceRequest = null;
				
				for (Bundle.BundleEntryComponent entry : resultsBundle.getEntry()) {
					if (entry.getResource() instanceof org.hl7.fhir.r4.model.Task) {
						task = (org.hl7.fhir.r4.model.Task) entry.getResource();
					}
					if (entry.getResource() instanceof org.hl7.fhir.r4.model.DiagnosticReport) {
						diagnosticReport = (org.hl7.fhir.r4.model.DiagnosticReport) entry.getResource();
					}
					if (entry.getResource() instanceof org.hl7.fhir.r4.model.ServiceRequest) {
						serviceRequest = (org.hl7.fhir.r4.model.ServiceRequest) entry.getResource();
					}
				}
				
				botswanaRegistryResponse.setTaskStatus(task.getStatus().getDisplay());
				
				if (serviceRequest == null && diagnosticReport == null) {
					botswanaRegistryResponse.setMessage("Service Request for this lab order has not been found");
					botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
				} else {
					// Fetch diagnostic report from the service request by conducting a reverse
					// search on the service request
					if (diagnosticReport == null) {
						Bundle diagnosticReportBundle = this.fhirUtil.getLabClient().search()
						        .forResource(org.hl7.fhir.r4.model.DiagnosticReport.class)
						        .where(org.hl7.fhir.r4.model.DiagnosticReport.BASED_ON
						                .hasId(serviceRequest.getIdElement().getIdPart()))
						        .returnBundle(org.hl7.fhir.r4.model.Bundle.class).execute();
						if (diagnosticReportBundle.getEntry().size() > 0) {
							diagnosticReport = (org.hl7.fhir.r4.model.DiagnosticReport) diagnosticReportBundle.getEntry()
							        .get(0).getResource();
						}
					}
					
					if (diagnosticReport == null) {
						botswanaRegistryResponse.setMessage("Diagnostic Report for this lab order has not been found");
						botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
					} else {
						// get the observation from the diagnostic report
						List<org.hl7.fhir.r4.model.Observation> observations = new ArrayList<>();
						for (org.hl7.fhir.r4.model.Reference reference : diagnosticReport.getResult()) {
							Observation observation = this.fhirUtil.getLabClient().read()
							        .resource(org.hl7.fhir.r4.model.Observation.class).withId(reference.getReference())
							        .execute();
							if (observations != null) {
								observations.add(observation);
							}
						}
						
						if (observations.size() == 0) {
							botswanaRegistryResponse
							        .setMessage("Observation results for this lab order have not been found");
							botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
						} else {
							botswanaRegistryResponse.setMessage("Observation results for this lab order have been found");
							botswanaRegistryResponse.setResponse(observations);
							botswanaRegistryResponse.setStatus(HttpStatus.OK.toString());
						}
					}
				}
			} else {
				botswanaRegistryResponse.setTaskStatus("UNKNOWN");
				botswanaRegistryResponse.setMessage("The task associated with this lab order has not been found");
				botswanaRegistryResponse.setStatus(HttpStatus.NOT_FOUND.toString());
			}
			
		}
		catch (FhirClientConnectionException e) {
			botswanaRegistryResponse.setResponse(e);
			botswanaRegistryResponse.setMessage(e.getMessage());
			botswanaRegistryResponse.setStatus(getHttpStatus(e).toString());
		}
		
		return botswanaRegistryResponse;
	}
	
	@Override
	public String fetchTaskIdFromOrderId(String orderUuid) {
		String taskId = "";
		ReferenceOrListParam uuid = new ReferenceOrListParam().add(new ReferenceParam(orderUuid));
		TaskSearchParams taskSearchParams = new TaskSearchParams();
		ReferenceAndListParam referenceAndListParam = new ReferenceAndListParam();
		referenceAndListParam.addValue(uuid);
		taskSearchParams.setBasedOnReference(referenceAndListParam);
		
		IBundleProvider tasksBundle = fhirTaskService.searchForTasks(taskSearchParams);
		List<IBaseResource> allTasks = tasksBundle.getAllResources();
		if (allTasks.size() > 0) {
			// get the task containg a reference to the order: 
			// This is a bruteforce approach: TODO: Ensure the FHIR task search works first time
			List<Task> tasks = new ArrayList<>();
			for (IBaseResource task : allTasks) {
				org.hl7.fhir.r4.model.Task t = (org.hl7.fhir.r4.model.Task) task;
				//if (t.getBasedOn().get(0).getReference().contains(orderUuid)) {
				//	tasks.add(t);
				//}
			}
			
			if (tasks.size() > 0) {
				org.hl7.fhir.r4.model.Task task = tasks.get(tasks.size() - 1);
				taskId = task.getIdElement().getIdPart();
			}
		}
		
		return taskId;
	}
	
	@Override
	public void saveLabResult() {
		// Save the lab result to the OpenMRS database
	}
}
