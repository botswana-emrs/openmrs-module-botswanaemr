/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import static org.hibernate.criterion.Restrictions.eq;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.jetbrains.annotations.NotNull;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.botswanaemr.api.dao.HtsProficiencyTestingDao;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;

public class HtsProficiencyTestingDaoImpl implements HtsProficiencyTestingDao {
	
	public HtsProficiencyTestingDaoImpl() {
	}
	
	public DbSessionFactory getDbSessionFactory() {
		return dbSessionFactory;
	}
	
	public void setDbSessionFactory(DbSessionFactory dbSessionFactory) {
		this.dbSessionFactory = dbSessionFactory;
	}
	
	DbSessionFactory dbSessionFactory;
	
	@Override
	public List getAll(boolean b) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public int getAllCount(boolean b) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list().size();
	}
	
	@Override
	public List getAll(boolean b, Integer integer, Integer integer1) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public BaseOpenmrsObject getById(Serializable serializable) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		criteria.add(eq("id", serializable));
		return (BaseOpenmrsObject) criteria.uniqueResult();
	}
	
	@Override
	public BaseOpenmrsObject getById(Serializable serializable, @NotNull String className) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		if (className.equals(HtsSpecimenResult.class.getName())) {
			criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsSpecimenResult.class);
		} else if (className.equals(HtsProficiencyTestingTest.class.getName())) {
			criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTestingTest.class);
		} else if (className.equals(HtsTestResult.class.getName())) {
			criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsTestResult.class);
		}
		criteria.add(eq("id", serializable));
		return (BaseOpenmrsObject) criteria.uniqueResult();
	}
	
	@Override
	public BaseOpenmrsObject getByUuid(String s) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(HtsProficiencyTesting.class);
		criteria.add(eq("uuid", s));
		return (BaseOpenmrsObject) criteria.uniqueResult();
	}
	
	@Override
	public void delete(BaseOpenmrsObject baseOpenmrsObject) {
		dbSessionFactory.getCurrentSession().delete(baseOpenmrsObject);
	}
	
	@Override
	public BaseOpenmrsObject saveOrUpdate(BaseOpenmrsObject baseOpenmrsObject) {
		dbSessionFactory.getCurrentSession().save(baseOpenmrsObject);
		return baseOpenmrsObject;
	}
}
