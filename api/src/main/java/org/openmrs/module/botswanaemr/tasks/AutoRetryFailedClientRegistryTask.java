/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.tasks;

import ca.uhn.fhir.model.api.Include;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.server.IBundleProvider;
import ca.uhn.fhir.rest.param.TokenAndListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import lombok.extern.slf4j.Slf4j;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Annotation;
import org.hl7.fhir.r4.model.Meta;
import org.hl7.fhir.r4.model.Task;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utils.FhirUtil;
import org.openmrs.module.fhir2.api.FhirPatientService;
import org.openmrs.module.fhir2.api.FhirTaskService;
import org.openmrs.module.fhir2.api.search.param.TaskSearchParams;
import org.openmrs.module.fhir2.api.translators.PatientTranslator;
import org.openmrs.module.fhir2.api.util.FhirUtils;
import org.openmrs.scheduler.tasks.AbstractTask;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Slf4j
@Component("autoRetryFailedClientRegistryTask")
public class AutoRetryFailedClientRegistryTask extends AbstractTask implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;
	
	private final FhirUtil fhirUtil = FhirUtil.getInstance();
	
	@Autowired
	private FhirTaskService fhirTaskService;
	
	@Autowired
	private FhirPatientService fhirPatientService;
	
	@Autowired
	private PatientTranslator patientTranslator;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		AutoRetryFailedClientRegistryTask.applicationContext = applicationContext;
	}
	
	@Override
	public void execute() {
		if (!isExecuting()) {
			log.debug("Starting auto retry failed client registry task...");
			startExecuting();
			
			try {
				applicationContext.getAutowireCapableBeanFactory().autowireBean(this);
			}
			catch (Exception a) {
				log.error("Error initiating context", a);
				stopExecuting();
			}
			
			try {
				// Query tasks
				HashSet<Include> includes = new HashSet<>();
				includes.add(new Include("Task:for"));
				TokenAndListParam status = new TokenAndListParam()
				        .addAnd(new TokenParam(Task.TaskStatus.REQUESTED.toCode()));
				IBundleProvider resourcesBundle = fhirTaskService
				        .searchForTasks(new TaskSearchParams(null, null, null, null, status, null, null, null, null));
				
				// Filter and extract tasks from resourcesBundle
				List<IBaseResource> allResources = resourcesBundle.getAllResources();
				List<Task> taskResources = new ArrayList<>();
				for (IBaseResource resource : allResources) {
					if (resource instanceof Task) {
						taskResources.add((Task) resource);
					}
				}
				
				int maxRetries = getMaxRetries();
				for (Task task : taskResources) {
					int attempt = getAttemptFromTask(task);
					if (attempt < maxRetries) {
						
						boolean success = false;
						
						// Fetch patients associated with the tasks
						String patientUuid = FhirUtils.referenceToId(task.getFor().getReference()).get();
						Patient patient = Context.getPatientService().getPatientByUuid(patientUuid);
						
						// Convert OpenMRS Patient to a FHIR patient
						org.hl7.fhir.r4.model.Patient fhirPatient = patientTranslator.toFhirResource(patient);
						
						try {
							// Post patient resource to client registry
							MethodOutcome outcome = fhirUtil.postPatientResource(fhirPatient).create().resource(fhirPatient)
							        .execute();
							
							if (outcome.getCreated() && outcome.getResource() instanceof org.hl7.fhir.r4.model.Patient) {
								log.debug("Patient resource created successfully on the Client Registry.");
								
								completeSuccessfulTask(task);
								success = true;
							} else {
								attempt++;
								log.error("Error encountered when creating Patient resource on Client Registry.");
								break;
							}
						}
						catch (Exception e) {
							attempt++;
							log.debug("Error encountered when posting FHIR Patient Resource to Client Registry. Attempt: "
							        + attempt,
							    e);
							
						}
						finally {
							if (!success) {
								if (attempt >= maxRetries) {
									log.debug("Maximum retries reached for posting patient resource.");
									updateTaskWithAttempt(task, attempt, "Failed to post resource.");
									completeFailedTask(task);
								} else {
									updateTaskWithAttempt(task, attempt, "Retrying failed post.");
								}
							}
						}
					} else {
						completeFailedTask(task);
					}
				}
			}
			catch (Exception e) {
				log.error("Error while retrying registry tasks:", e);
			}
			finally {
				stopExecuting();
				log.debug("Finished auto retry tasks to client registry task.");
			}
		}
	}
	
	private void completeSuccessfulTask(Task task) {
		task.setStatus(Task.TaskStatus.COMPLETED);
		fhirTaskService.update(task.getIdElement().getIdPart(), task);
	}
	
	private void completeFailedTask(Task task) {
		task.setStatus(Task.TaskStatus.COMPLETED);
		fhirTaskService.update(task.getIdElement().getIdPart(), task);
	}
	
	private int getAttemptFromTask(Task task) {
		int attempt = 0;
		List<Annotation> notes = task.getNote();
		if (notes != null && !notes.isEmpty()) {
			String noteText = notes.get(notes.size() - 1).getText();
			attempt = extractAttemptNumber(noteText);
		}
		return attempt;
	}
	
	private int extractAttemptNumber(String noteText) {
		if (noteText != null && noteText.contains("Attempt ")) {
			String attemptStr = noteText.substring(noteText.indexOf("Attempt ") + 8).split(":")[0].trim();
			return Integer.parseInt(attemptStr);
		}
		return 0;
	}
	
	private void updateTaskWithAttempt(Task task, int attempt, String message) {
		Meta meta = task.getMeta();
		if (meta == null) {
			meta = new Meta();
		}
		// meta.addTag()
		// 		.setSystem(BotswanaEmrConstants.CR_SOURCE_SYSTEM_URL)
		// 		.setCode(BotswanaEmrConstants.CR_SOURCE_SYSTEM_CODE)
		// 		.setDisplay(String.valueOf(attempt));
		// task.setMeta(meta);
		
		String newNoteText = "Attempt " + attempt + ": " + message;
		
		if (newNoteText.length() > 255) {
			newNoteText = newNoteText.substring(0, 252) + "...";
		}
		
		Annotation newAnnotation = new Annotation();
		newAnnotation.setText(newNoteText);
		task.setNote(Collections.singletonList(newAnnotation));
		
		fhirTaskService.update(task.getIdElement().getIdPart(), task);
	}
	
	private int getMaxRetries() {
		String maxRetriesStr = Context.getAdministrationService().getGlobalProperty("botswanaemr.cr.maxRetries");
		return maxRetriesStr != null ? Integer.parseInt(maxRetriesStr) : 3;
	}
}
