/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import org.openmrs.Concept;
import org.openmrs.api.APIException;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The regimen service that provide the CRUD functionalities, which is exposed for other modules.
 * See moduleApplicationContext.xml on how it is wired up.
 */
@Transactional
public interface BotswanaRegimenService {
	
	//regine lines service methods
	public List<RegimenLine> getAllRegimenLine() throws APIException;
	
	public RegimenLine getRegimenLine(Integer EhrAppointmentTypeId) throws APIException;
	
	public RegimenLine getRegimenLineByUuid(String uuid) throws APIException;
	
	public RegimenLine saveRegimenLine(RegimenLine regimenLine) throws APIException;
	
	public RegimenLine retireRegimenLine(RegimenLine regimenLine, String reason) throws APIException;
	
	public RegimenLine unretireRegimenLine(RegimenLine regimenLine) throws APIException;
	
	public void purgeRegimenLine(RegimenLine regimenLine) throws APIException;
	
	//regimen service methods
	public Regimen saveRegimen(Regimen regimen) throws APIException;
	
	public Regimen getRegimen(Integer regimenId) throws APIException;
	
	public Regimen getRegimenByRegimenLine(RegimenLine regimenLine) throws APIException;
	
	public List<Regimen> getAllRegimen(boolean includeVoided) throws APIException;
	
	public Regimen updateRegimen(Regimen regimen) throws APIException;
	
	public Regimen voidRegimen(Regimen regimen, String voidReason) throws APIException;
	
	//regimen components service methods
	public RegimenComponent saveRegimenComponent(RegimenComponent regimenComponent) throws APIException;
	
	public RegimenComponent getRegimenComponentById(Integer regimenComponentId) throws APIException;
	
	List<RegimenComponent> getRegimenComponentsByRegimen(Integer regimenId) throws APIException;
	
	List<RegimenComponent> getRegimenComponentsByRegimenConcept(Concept regimenConcept) throws APIException;
	
	public List<RegimenComponent> getAllRegimenComponent(boolean includeVoided) throws APIException;
	
	public RegimenComponent getRegimenComponentByRegimen(Regimen regimen) throws APIException;
	
	public List<RegimenComponent> getAllRegimenComponentByRegimen(Regimen regimen) throws APIException;
	
	public RegimenComponent updateRegimenComponent(RegimenComponent regimenComponent) throws APIException;
	
	public RegimenComponent voidRegimenComponent(RegimenComponent regimenComponent, String voidReason) throws APIException;
	
	RegimenComponentDrug saveRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug);
	
	RegimenComponentDrug getRegimenComponentDrug(Integer id);
	
	List<RegimenComponentDrug> getAllRegimenComponentDrugs(boolean includeVoided);
	
	// Get regimenComponentDrug by regimenComponent
	List<RegimenComponentDrug> getRegimenComponentDrugsByRegimenComponent(RegimenComponent regimenComponent);
	
	// Get regimenComponentDrug by regimenConcept
	List<RegimenComponentDrug> getRegimenComponentDrugsByRegimenConcept(Concept regimenConcept);
	
	RegimenComponentDrug updateRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug);
	
	void voidRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug, String voidReason);
	
	Regimen getRegimenByConcept(Concept regimenConcept);
}
