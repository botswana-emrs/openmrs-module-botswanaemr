/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.Type;
import org.openmrs.module.botswanaemr.Registration;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PaymentsSearchBuilder {
	
	public static final String REG_SELECT_COUNT_STATEMENT = "SELECT count(payment_id) \n";
	
	public static final String REG_SELECT_STATEMENT = "SELECT payment.* \n";
	
	public static final String FROM_CLAUSE = " FROM payment \n";
	
	public static final String JOIN_CLAUSE = " INNER JOIN person p on payment.patient_id = p.person_id \n";
	
	public static final String WHERE_CLAUSE = " WHERE payment.voided=0 ";
	
	private String select;
	
	private String where;
	
	private String from;
	
	private String join;
	
	private String groupBy;
	
	private String orderBy = "";
	
	private SessionFactory sessionFactory;
	
	private Map<String, Type> types;
	
	private boolean iscount = false;
	
	public PaymentsSearchBuilder(SessionFactory sessionFactory, boolean isCount) {
		this.sessionFactory = sessionFactory;
		this.iscount = isCount;
		select = isCount ? REG_SELECT_COUNT_STATEMENT : REG_SELECT_STATEMENT;
		join = JOIN_CLAUSE;
		from = FROM_CLAUSE;
		where = WHERE_CLAUSE;
		types = new HashMap<>();
	}
	
	public PaymentsSearchBuilder withLocationAttribute() {
		PatientLocationAttributeQueryHelper helper = new PatientLocationAttributeQueryHelper();
		join = helper.appendToJoinClause(join);
		return this;
	}
	
	private void appendToWhereClause(String operator, String condition) {
		where = String.format(" %s %s %s ", where, operator, condition);
	}
	
	public NativeQuery buildSqlQuery(Integer limit, Integer offset, String locationUuid, String startDate, String endDate,
	        Integer locationId) {
		if (StringUtils.isNotEmpty(startDate)) {
			appendToWhereClause("and", "DATE(payment.date_created) >= :startDate");
		}
		if (StringUtils.isNotEmpty(endDate)) {
			appendToWhereClause("and", "DATE(payment.date_created) <= :endDate");
		}
		
		if (locationId != null) {
			appendToWhereClause("and", "payment.location_id = :locationId");
		}
		
		String query = select + from + join + where + orderBy;
		
		NativeQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(query);
		if (!this.iscount) {
			sqlQuery.addEntity(Registration.class);
			Iterator<Map.Entry<String, Type>> iterator = types.entrySet().iterator();
			
			while (iterator.hasNext()) {
				Map.Entry<String, Type> entry = iterator.next();
				sqlQuery.addScalar(entry.getKey(), entry.getValue());
			}
		}
		
		// if (StringUtils.isNotEmpty(locationUuid))
		//    sqlQuery.setParameter(PatientSearchBuilder.LOCATION_PARAM, locationUuid);
		if (StringUtils.isNotEmpty(startDate))
			sqlQuery.setParameter("startDate", startDate);
		if (StringUtils.isNotEmpty(endDate))
			sqlQuery.setParameter("endDate", endDate);
		if (locationId != null)
			sqlQuery.setParameter("locationId", locationId);
		
		return sqlQuery;
	}
}
