/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import org.hibernate.Criteria;
import org.openmrs.Concept;
import org.openmrs.api.db.DAOException;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.botswanaemr.api.dao.BotswanaRegimenDao;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;

import java.util.List;

import static org.hibernate.criterion.Restrictions.eq;

/**
 * Implementation of the logic from {@link BotswanaRegimenDao}
 */
public class HibernateBotswanaRegimenDAO implements BotswanaRegimenDao {
	
	public DbSessionFactory getDbSessionFactory() {
		return dbSessionFactory;
	}
	
	public void setDbSessionFactory(DbSessionFactory dbSessionFactory) {
		this.dbSessionFactory = dbSessionFactory;
	}
	
	private DbSessionFactory dbSessionFactory;
	
	@Override
	public List<RegimenLine> getAllRegimenLine() throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenLine.class);
		criteria.add(eq("retired", false));
		return criteria.list();
		
	}
	
	@Override
	public RegimenLine getRegimenLine(Integer regimenLineId) throws DAOException {
		return (RegimenLine) dbSessionFactory.getCurrentSession().get(RegimenLine.class, regimenLineId);
	}
	
	@Override
	public RegimenLine getRegimenLineByUuid(String uuid) throws DAOException {
		return (RegimenLine) dbSessionFactory.getCurrentSession().get(RegimenLine.class, uuid);
	}
	
	@Override
	public RegimenLine saveRegimenLine(RegimenLine regimenLine) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(regimenLine);
		return regimenLine;
	}
	
	@Override
	public RegimenLine retireRegimenLine(RegimenLine regimenLine, String reason) throws DAOException {
		return saveRegimenLine(regimenLine);
	}
	
	@Override
	public RegimenLine unretireRegimenLine(RegimenLine regimenLine) throws DAOException {
		return saveRegimenLine(regimenLine);
	}
	
	@Override
	public void purgeRegimenLine(RegimenLine regimenLine) throws DAOException {
		dbSessionFactory.getCurrentSession().delete(regimenLine);
	}
	
	@Override
	public Regimen saveRegimen(Regimen regimen) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(regimen);
		return regimen;
	}
	
	@Override
	public Regimen getRegimen(Integer regimenId) throws DAOException {
		return (Regimen) dbSessionFactory.getCurrentSession().get(Regimen.class, regimenId);
	}
	
	@Override
	public Regimen getRegimenByConcept(Concept regimenConcept) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Regimen.class);
		criteria.add(eq("conceptRef", regimenConcept));
		criteria.setMaxResults(1);
		return (Regimen) criteria.uniqueResult();
	}
	
	@Override
	public List<RegimenComponentDrug> getRegimenComponentDrugByRegimenConcept(Concept regimenConcept) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponentDrug.class);
		// Add criteria for nested object regimen.conceptRef
		criteria.createAlias("regimen", "regimen");
		criteria.add(eq("regimen.conceptRef", regimenConcept));
		criteria.add(eq("voided", false));
		return criteria.list();
	}
	
	@Override
	public List<RegimenComponentDrug> getRegimenComponentDrugByRegimenComponent(RegimenComponent regimenComponent) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponentDrug.class);
		criteria.add(eq("regimenComponent", regimenComponent));
		criteria.add(eq("voided", false));
		return criteria.list();
	}
	
	@Override
	public List<RegimenComponent> getRegimenComponentsByConcept(Concept regimenConcept) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponent.class);
		criteria.createAlias("regimen", "regimen");
		criteria.add(eq("regimen.conceptRef", regimenConcept));
		criteria.add(eq("voided", false));
		// criteria.setMaxResults(1);
		return criteria.list();
	}
	
	@Override
	public Regimen getRegimenByRegimenLine(RegimenLine regimenLine) throws DAOException {
		return (Regimen) dbSessionFactory.getCurrentSession().get(Regimen.class, regimenLine);
	}
	
	@Override
	public List<Regimen> getAllRegimen(boolean includeVoided) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Regimen.class);
		if (includeVoided) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public Regimen updateRegimen(Regimen regimen) throws DAOException {
		return saveRegimen(regimen);
	}
	
	@Override
	public Regimen voidRegimen(Regimen regimen, String voidReason) throws DAOException {
		return saveRegimen(regimen);
	}
	
	@Override
	public RegimenComponent saveRegimenComponent(RegimenComponent regimenComponent) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(regimenComponent);
		return regimenComponent;
	}
	
	@Override
	public RegimenComponent getRegimenComponentById(Integer regimenComponentId) throws DAOException {
		return (RegimenComponent) dbSessionFactory.getCurrentSession().get(RegimenComponent.class, regimenComponentId);
	}
	
	@Override
	public List<RegimenComponent> getAllRegimenComponent(boolean includeVoided) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponent.class);
		if (includeVoided) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public RegimenComponent getRegimenComponentByRegimen(Regimen regimen) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponent.class);
		criteria.add(eq("regimen", regimen));
		criteria.add(eq("voided", false));
		criteria.setMaxResults(1);
		return (RegimenComponent) criteria.uniqueResult();
	}
	
	@Override
	public List<RegimenComponent> getAllRegimenComponentByRegimen(Regimen regimen) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponent.class);
		criteria.add(eq("regimen", regimen));
		criteria.add(eq("voided", false));
		return criteria.list();
	}
	
	@Override
	public RegimenComponent updateRegimenComponent(RegimenComponent regimenComponent) throws DAOException {
		return saveRegimenComponent(regimenComponent);
	}
	
	@Override
	public RegimenComponent voidRegimenComponent(RegimenComponent regimenComponent, String voidReason) throws DAOException {
		return saveRegimenComponent(regimenComponent);
	}
	
	@Override
	public RegimenComponentDrug saveRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(regimenComponentDrug);
		return regimenComponentDrug;
	}
	
	@Override
	public RegimenComponentDrug getRegimenComponentDrugById(Integer regimenComponentDrugId) throws DAOException {
		return (RegimenComponentDrug) dbSessionFactory.getCurrentSession().get(RegimenComponentDrug.class,
		    regimenComponentDrugId);
	}
	
	@Override
	public List<RegimenComponentDrug> getAllRegimenComponentDrugs(boolean includeVoided) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(RegimenComponentDrug.class);
		if (includeVoided) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public RegimenComponentDrug updateRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug) throws DAOException {
		return saveRegimenComponentDrug(regimenComponentDrug);
	}
	
	@Override
	public RegimenComponentDrug voidRegimenComponentDrug(RegimenComponentDrug regimenComponentDrug, String voidReason)
	        throws DAOException {
		return saveRegimenComponentDrug(regimenComponentDrug);
	}
	
}
