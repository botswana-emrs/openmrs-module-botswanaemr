/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseChangeableOpenmrsData;
import org.openmrs.Encounter;
import org.openmrs.Patient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "case_encounter")
@BatchSize(size = 25)
public class CaseEncounter extends BaseChangeableOpenmrsData {
	
	public static final long serialVersionUID = 2L;
	
	public Integer getCaseEncounterId() {
		return caseEncounterId;
	}
	
	public void setCaseEncounterId(Integer caseEncounterId) {
		this.caseEncounterId = caseEncounterId;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "case_encounter_id")
	private Integer caseEncounterId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "case_id")
	private Cases cases;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "encounter_id")
	private Encounter encounter;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "patient_id")
	private Patient patient;
	
	public Cases getCases() {
		return cases;
	}
	
	public void setCaseId(Cases casesId) {
		this.cases = casesId;
	}
	
	public Encounter getEncounter() {
		return encounter;
	}
	
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	@Override
	public Integer getId() {
		return caseEncounterId;
	}
	
	@Override
	public void setId(Integer caseEncounterId) {
		this.caseEncounterId = caseEncounterId;
	}
}
