/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;

public enum IDENTIFIER {
	
	NIN("National ID"),
	PPN("Passport Number"),
	BCN("Birth Certificate Number"),
	OID(BotswanaEmrConstants.OPENMRS_ID_IDENTIFIER_NAME),
	TID(BotswanaEmrConstants.TEMPORARY_ID_IDENTIFIER_NAME);
	
	public String identificationType;
	
	IDENTIFIER(String identificationType) {
		this.identificationType = identificationType;
	}
	
	public String getIdentificationType() {
		return this.identificationType;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
