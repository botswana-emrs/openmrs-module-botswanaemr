/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.BaseModuleActivator;
import org.openmrs.module.ModuleException;
import org.openmrs.module.botswanaemr.activator.EmrHtmlFormsInitializer;
import org.openmrs.module.botswanaemr.activator.Initializer;
import org.openmrs.module.botswanaemr.inventory.InventoryInitializer;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.initializer.InitializerMessageSource;

import java.util.ArrayList;
import java.util.List;

import static org.openmrs.module.initializer.InitializerConstants.*;

/**
 * This class contains the logic that is run every time this module is either started or shutdown
 */
public class BotswanaEmrActivator extends BaseModuleActivator {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * @see #started()
	 */
	public void started() {
		// run the initializers
		for (Initializer initializer : getInitializers()) {
			initializer.started();
		}
		
		// purge the "Screening: Capture medical details" privilege from the "Privilege Level: High" role
		BotswanaEmrUtils.purgePrivilegeFromRole(BotswanaEmrConstants.ROLE_PRIVILEGE_LEVEL_HIGH,
		    BotswanaEmrConstants.PRIVILEGE_SCREENING_MEDICAL_DETAILS);
		BotswanaEmrUtils.purgePrivilegeFromRole(BotswanaEmrConstants.ROLE_PRIVILEGE_LEVEL_HIGH,
		    BotswanaEmrConstants.PRIVILEGE_SCREENING_SYMPTOMS);
		
		log.info("Started The EMR for Botswana");
	}
	
	/**
	 * @see #shutdown()
	 */
	public void shutdown() {
		log.info("Shutdown The EMR for Botswana");
	}
	
	private List<Initializer> getInitializers() {
		List<Initializer> l = new ArrayList<Initializer>();
		l.add(new EmrHtmlFormsInitializer("botswanaemr"));
		return l;
	}
	
	private void setUpInventoryIniz() {
		log.info("Start of inventory iniz");
		
		// Set active message source
		InitializerMessageSource messageSource = Context.getRegisteredComponents(InitializerMessageSource.class).get(0);
		Context.getMessageSourceService().setActiveMessageSource(messageSource);
		
		String startupLoadingMode = getInitializerService().getInitializerConfig().getStartupLoadingMode();
		
		if (PROPS_STARTUP_LOAD_DISABLED.equalsIgnoreCase(startupLoadingMode)) {
			log.info("OpenMRS config loading process disabled at initializer startup");
		} else {
			boolean throwError = PROPS_STARTUP_LOAD_FAIL_ON_ERROR.equalsIgnoreCase(startupLoadingMode);
			log.info("OpenMRS config loading process started...");
			try {
				getInitializerService().loadUnsafe(true, throwError);
				log.info("OpenMRS config loading process completed.");
			}
			catch (Exception e) {
				throw new ModuleException("An error occurred loading initializer configuration", e);
			}
		}
	}
	
	protected InventoryInitializer getInitializerService() {
		return Context.getService(InventoryInitializer.class);
	}
	
	public static String getPropertyValue(String property, String defaultValue) {
		if (System.getProperties().containsKey(property)) {
			return System.getProperty(property);
		}
		return Context.getRuntimeProperties().getProperty(property, defaultValue);
	}
	
}
