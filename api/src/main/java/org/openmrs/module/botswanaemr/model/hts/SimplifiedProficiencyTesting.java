/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class SimplifiedProficiencyTesting {
	
	private Integer proficiencyTestingId;
	
	private String panelId;
	
	private String receivedBy;
	
	private String receivedByName;
	
	private String datePanelReceived;
	
	private String testingPoint;
	
	private String resultsDueDate;
	
	private String dateTested;
	
	private String resultsReceivedBy;
	
	private String resultsReceivedByName;
	
	private Double performanceScore;
	
	private Double percentageScore;
	
	private String comment;
	
	private String reportReviewedBy;
	
	private String reportReviewedByName;
	
	private String dateReviewed;
	
	private Boolean published;
}
