/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao;

import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.CaseLinkage;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.model.DiscussionParticipant;
import org.openmrs.module.botswanaemr.model.DrugOrderExtension;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.parameter.EncounterSearchCriteria;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface BotswanaEmrDao {
	
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Location location,
	        Integer limit);
	
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit)
	        throws DAOException;
	
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit,
	        Location location) throws DAOException;
	
	public int getPatientsRegisteredOnDateCountOnly(Date startDate, Date endDate, Integer limit, Location location)
	        throws DAOException;
	
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour) throws DAOException;
	
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour, Location location);
	
	/**
	 * Get payment method by internal identifier
	 *
	 * @param paymentMethodId Integer
	 * @return PaymentMethod with given internal identifier
	 * @throws DAOException <strong>Should</strong> throw error if given null parameter
	 */
	public PaymentMethod getPaymentMethod(Integer paymentMethodId) throws DAOException;
	
	/**
	 * Get payment method by exact name
	 *
	 * @param name string to match to an PaymentMethod.name
	 * @return PaymentMethod that is not retired
	 * @throws DAOException <strong>Should</strong> not get retired types <strong>Should</strong> return
	 *             null if only retired type found <strong>Should</strong> not get by inexact name
	 *             <strong>Should</strong> return null with null name parameter
	 */
	public PaymentMethod getPaymentMethod(String name) throws DAOException;
	
	/**
	 * Get all payment methods (including retired)
	 *
	 * @return payment methods list
	 * @throws DAOException <strong>Should</strong> not return retired types
	 */
	public List<PaymentMethod> getAllPaymentMethods() throws DAOException;
	
	/**
	 * Get all payment method. If includeRetired is true, also get retired payment method.
	 *
	 * @param includeRetired
	 * @return payment method list
	 * @throws DAOException <strong>Should</strong> not return retired types <strong>Should</strong>
	 *             include retired types with true includeRetired parameter
	 */
	public List<PaymentMethod> getAllPaymentMethods(boolean includeRetired) throws DAOException;
	
	//get all the service methods for the service provider details
	
	/**
	 * Get serviceProviderId by internal identifier
	 *
	 * @param serviceProviderId Integer
	 * @return serviceProviderId with given internal identifier
	 * @throws DAOException <strong>Should</strong> throw error if given null parameter
	 */
	public ServiceProvider getServiceProvider(Integer serviceProviderId) throws DAOException;
	
	/**
	 * Get ServiceProvider by exact name
	 *
	 * @param name string to match to an ServiceProvider.name
	 * @return PaymentMethod that is not retired
	 * @throws DAOException <strong>Should</strong> not get retired types <strong>Should</strong> return
	 *             null if only retired type found <strong>Should</strong> not get by inexact name
	 *             <strong>Should</strong> return null with null name parameter
	 */
	public ServiceProvider getServiceProvider(String name) throws DAOException;
	
	/**
	 * Get all payment methods (including retired)
	 *
	 * @return payment methods list
	 * @throws DAOException <strong>Should</strong> not return retired types
	 */
	public List<ServiceProvider> getAllServiceProvider() throws DAOException;
	
	/**
	 * Get all service provider. If includeRetired is true, also get retired service provider.
	 *
	 * @param includeRetired
	 * @return payment method list
	 * @throws DAOException <strong>Should</strong> not return retired types <strong>Should</strong>
	 *             include retired types with true includeRetired parameter
	 */
	public List<ServiceProvider> getAllServiceProvider(boolean includeRetired) throws DAOException;
	
	//get payments for the patients
	
	/**
	 * Saves a new payment or updates an existing payment. If an existing payment, this method will
	 * automatically apply payment.
	 *
	 * @param registration to be saved
	 * @throws DAOException <strong>Should</strong> save payment with basic details
	 *             <strong>Should</strong> update paymant successfully <strong>Should</strong> not
	 *             overwrite creator if non null <strong>Should</strong> not overwrite dateCreated if
	 *             non null <strong>Should</strong> not overwrite obs and orders creator or dateCreated
	 *             <strong>Should</strong> fail if user is not supposed to edit payment of type of given
	 *             payment
	 */
	public Registration savePayment(Registration registration) throws DAOException;
	
	/**
	 * Get payment by internal identifier
	 *
	 * @param registrationId payment id
	 * @return payment with given internal identifier
	 * @throws DAOException <strong>Should</strong> throw error if given null parameter
	 *             <strong>Should</strong> fail if user is not allowed to view payment by given id
	 *             <strong>Should</strong> return payment if user is allowed to get it
	 */
	public Registration getPayment(Integer registrationId) throws DAOException;
	
	/**
	 * Get all payments (not voided) for a patient, sorted by registrationDatetime ascending.
	 *
	 * @param patient
	 * @return List&lt;Payment&gt; payments (not voided) for a patient. <strong>Should</strong> not get
	 *         voided payments <strong>Should</strong> throw error when given null parameter
	 */
	public List<Registration> getPaymentByPatient(Patient patient) throws DAOException;
	
	/**
	 * Get all payments that match a variety of (nullable) criteria contained in the parameter object.
	 * Each extra value for a parameter that is provided acts as an "and" and will reduce the number of
	 * results returned
	 *
	 * @param who the patient
	 * @param loc the location
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param includeVoided the include Voided
	 * @return a list of payments ordered by increasing registrationDatetime <strong>Should</strong> get
	 *         payment modified after specified date
	 */
	public List<Registration> getPayment(Patient who, Location loc, Date fromDate, Date toDate, boolean includeVoided)
	        throws DAOException;
	
	/**
	 * Voiding a payment essentially removes it from circulation
	 *
	 * @param payment Payment object to void
	 * @param reason String reason that it's being voided <strong>Should</strong> void payment and set
	 *            attributes <strong>Should</strong> throw error with null reason parameter
	 *            <strong>Should</strong> fail if user is not supposed to edit payment of type of given
	 *            Payment
	 */
	public Registration voidPayment(Registration payment, String reason) throws DAOException;
	
	/**
	 * Saves a new payment methos or updates an existing payment. If an existing payment, this method
	 * will automatically apply payment.
	 *
	 * @param paymentMethod to be saved
	 * @throws DAOException <strong>Should</strong> save payment method with basic details
	 *             <strong>Should</strong> update payment method successfully <strong>Should</strong>
	 *             not overwrite creator if non null <strong>Should</strong> not overwrite dateCreated
	 *             if non null <strong>Should</strong> fail if user is not supposed to edit payment
	 *             method of type of given payment
	 */
	public PaymentMethod savePaymentMethod(PaymentMethod paymentMethod) throws DAOException;
	
	/**
	 * Saves a new service provider or updates an existing payment. If an existing payment, this method
	 * will automatically apply payment.
	 *
	 * @param serviceProvider to be saved
	 * @throws DAOException <strong>Should</strong> save service provider with basic details
	 *             <strong>Should</strong> update service provider successfully <strong>Should</strong>
	 *             not overwrite creator if non null <strong>Should</strong> not overwrite dateCreated
	 *             if non null <strong>Should</strong> fail if user is not supposed to edit service
	 *             provider of type of given payment
	 */
	public ServiceProvider saveServiceProvider(ServiceProvider serviceProvider) throws DAOException;
	
	public List<Encounter> getEncountersOnDate(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) throws DAOException;
	
	public List<Encounter> getEncountersOnDate(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location) throws DAOException;
	
	public List<Encounter> getEncountersOnDateAndPatient(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location, Patient patient) throws DAOException;
	
	List<Encounter> getEncountersOnDateAndPatient(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location, Patient patient, PagingInfo pagingInfo) throws DAOException;
	
	public List<Encounter> getEncountersOnDatePerHour(Date date, String hour, EncounterType encounterType, Location location)
	        throws DAOException;
	
	public CaseLinkage saveCaseLinkage(CaseLinkage caseLinkage) throws DAOException;
	
	public CaseLinkage getCaseLinkageById(Integer caseLinkageId) throws DAOException;
	
	public List<Concept> searchConcept(String searchTerm, ConceptClass clazz) throws DAOException;
	
	public Cases saveCase(Cases cases) throws DAOException;
	
	public Cases getCase(Integer caseId) throws DAOException;
	
	public List<Cases> getAllCase(Patient patient, boolean includeRetired) throws DAOException;
	
	public CaseEncounter saveCaseEncounter(CaseEncounter caseEncounter) throws DAOException;
	
	List<PatientResponse> getPatientsByHtsSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String testingLocation);
	
	public CaseEncounter getCaseEncounter(Integer caseEncounterId) throws DAOException;
	
	public List<CaseEncounter> getAllCaseEncounter(Patient patient, boolean includeVoided) throws DAOException;
	
	public CaseEncounter getCaseByEncounterId(Encounter encounter);
	
	DiscussionParticipant addDiscussionParticipant(DiscussionParticipant discussionParticipant);
	
	void deleteDiscussionParticipant(DiscussionParticipant discussionParticipant);
	
	List<DiscussionParticipant> getDiscussionParticipantsByDiscussion(Discussion discussion);
	
	Discussion saveDiscussion(Discussion discussion);
	
	Discussion getDiscussionByCase(Cases caze);
	
	Discussion getDiscussionById(Integer discussionId);
	
	Message saveMessage(Message message);
	
	List<Message> getMessagesByDiscussion(Discussion discussion);
	
	List<Order> getPrescriptionsFilledOnDatePerHour(Date date, String hour) throws DAOException;
	
	Collection<Patient> findPatientsInContactWithTB(@NotNull Patient patient);
	
	DrugOrder getDrugOrderById(Integer drugOrderId) throws DAOException;
	
	DrugOrderExtension saveDrugOrderExtension(DrugOrderExtension drugOrderExtension);
	
	List<DrugOrderExtension> getDrugOrderExtensionsByPatientId(@NotNull Patient patient);
	
	List<Patient> getBotswanaEmrPatients(String query, boolean includeVoided, Integer start, Integer length);
	
	Patient getBotswanaEmrPatient(Integer patientId);
	
	List<PatientQueue> getIncompletePatientQueues();
	
	List<PatientProgram> getPatientProgramByProgram(Program program);
	
	List<PatientProgram> getPatientProgramByProgramAndDate(Program program, Date date, @Nullable Date endDate,
	        boolean activeOnly);
	
	List<PatientProgram> getActivePatientProgramsByProgram(Program program, boolean includeVoided);
	
	List<Person> getPatientByMigrationMetadata(String migrationFacilityId, String migrationPatientId);
	
	List<PatientResponse> getPatientsBySearchParams(String nameOrUniqueId, String locationUuid, String programId,
	        String gender, String status, String startDateRegistered, String endDateRegistered, String condition);
	
	List<BotswanaEmrEmailReportConfig> getBotswanaEmrEmailReportConfigs(String reportId, Location facility);
	
	BotswanaEmrEmailReportConfig saveBotswanaEmrEmailReportConfig(BotswanaEmrEmailReportConfig botswanaEmrEmailReportConfig);
	
	public CaseEncounter getLastCaseEncounter(Patient patient) throws DAOException;
	
	public List<Encounter> getEncounterLimits(EncounterSearchCriteria searchCriteria, Integer fetchLimit)
	        throws DAOException;
	
	public Long getPatientsRegisteredCountOnDate(Date startDate, Date endDate, String location, Integer limit)
	        throws DAOException;
	
	public Registration getPatientsRegisteredPositionOnDate(Date startDate, Date endDate, String location, String position)
	        throws DAOException;
	
	public List<Obs> getObservation(Person person, Visit visit, Concept question, Location location, Integer mostRecentN)
	        throws DAOException;
	
	public List<Obs> getObservation(Person person, EncounterType encounterType, Visit visit, Concept question,
	        Location location, Integer mostRecentN) throws DAOException;
	
	public List<CodedOrFreeText> getAllConditionEverSaved() throws DAOException;
	
	public List<PatientResponse> getPatientsByVmmcSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String visitStatus);
	
	Registration getLatestPaymentEntry(Patient patient, Location location) throws DAOException;
}
