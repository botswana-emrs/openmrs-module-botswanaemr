/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import static java.util.stream.Collectors.toList;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Nullable;
import org.openmrs.Drug;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.model.SimplifiedItemStockDetail;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.WellKnownOperationTypes;
import org.openmrs.module.botswanaemrInventory.api.IItemDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDataService;
import org.openmrs.module.botswanaemrInventory.api.IItemStockDetailDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationService;
import org.openmrs.module.botswanaemrInventory.api.IStockOperationTransactionDataService;
import org.openmrs.module.botswanaemrInventory.api.IStockroomDataService;
import org.openmrs.module.botswanaemrInventory.api.impl.ItemStockDataServiceImpl;
import org.openmrs.module.botswanaemrInventory.api.impl.StockOperationDataServiceImpl;
import org.openmrs.module.botswanaemrInventory.api.impl.StockOperationTransactionDataServiceImpl;
import org.openmrs.module.botswanaemrInventory.api.impl.StockroomDataServiceImpl;
import org.openmrs.module.botswanaemrInventory.model.IStockOperationType;
import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;
import org.openmrs.module.botswanaemrInventory.model.ItemStockDetail;
import org.openmrs.module.botswanaemrInventory.model.StockOperation;
import org.openmrs.module.botswanaemrInventory.model.StockOperationStatus;
import org.openmrs.module.botswanaemrInventory.model.StockOperationTransaction;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttribute;
import org.openmrs.module.botswanaemrInventory.model.StockRoomAttributeType;
import org.openmrs.module.botswanaemrInventory.model.Stockroom;
import org.openmrs.module.botswanaemrInventory.search.BaseObjectTemplateSearch;
import org.openmrs.module.botswanaemrInventory.search.ItemSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationSearch;
import org.openmrs.module.botswanaemrInventory.search.StockOperationTemplate;
import org.openmrs.ui.framework.SimpleObject;

public class StockUtils {
	
	public static StockOperation getStockOperation(Integer quantity, Integer stockRoomId, String itemUuid,
	        IStockOperationType iStockOperationType, StockOperationStatus newStockOperationStatus,
	        StockOperationStatus competeStockOperationStatus) {
		
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		
		StockOperation stockOperation = new StockOperation();
		Item item = iItemDataService.getByUuid(itemUuid);
		// TODO: Pull expiration attribute
		if (item.getHasExpiration()) {
			// TODO: Explicitly set expiration date
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.DATE, 365);
			Date expirationDate = cal.getTime();
			
			stockOperation.addItem(item, quantity, expirationDate);
			
		} else {
			stockOperation.addItem(item, quantity);
		}
		stockOperation.setOperationDate(new Date());
		
		stockOperation.setDestination(iStockroomDataService.getById(stockRoomId));
		stockOperation.setInstanceType(iStockOperationType);
		
		stockOperation.setOperationNumber(UUID.randomUUID().toString());
		if (newStockOperationStatus != null) {
			stockOperation.setStatus(newStockOperationStatus);
		}
		iStockOperationService.submitOperation(stockOperation);
		if (competeStockOperationStatus != null) {
			stockOperation.setStatus(competeStockOperationStatus);
		}
		
		return iStockOperationService.submitOperation(stockOperation);
		
	}
	
	public static StockOperation getInitialStockStockOperation(Integer quantity, Integer stockRoomId, Date operationDate,
	        Date expirationDate, String batchNo, String itemUuid) {
		StockOperation stockOperation = new StockOperation();
		
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
		IStockOperationDataService iStockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
		
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		
		Item item = iItemDataService.getByUuid(itemUuid);
		
		StockOperation batchOperation = iStockOperationDataService.getOperationByNumber(batchNo);
		
		if (batchOperation == null) {
			batchOperation = stockOperation;
		}
		
		stockOperation.addItem(item, quantity, expirationDate, batchOperation);
		
		stockOperation.setOperationDate(operationDate);
		Stockroom destStockRoom = iStockroomDataService.getById(stockRoomId);
		
		stockOperation.setDestination(destStockRoom);
		stockOperation.setInstanceType(WellKnownOperationTypes.getInitial());
		stockOperation.setDescription("Initial stock update");
		
		stockOperation.setOperationNumber(batchNo);
		stockOperation.setStatus(StockOperationStatus.NEW);
		iStockOperationService.submitOperation(stockOperation);
		stockOperation.setStatus(StockOperationStatus.COMPLETED);
		return iStockOperationService.submitOperation(stockOperation);
	}
	
	public static StockOperation getStockHoldOperation(Patient patient, Stockroom stockroom, Item item, Integer quantity,
	        String orderNumber) {
		IStockOperationService iStockOperationService = BotswanaInventoryContext.getStockOperationService();
		
		StockOperation stockOperation = new StockOperation();
		stockOperation.setOperationDate(new Date());
		stockOperation.setDestination(stockroom);
		stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
		
		stockOperation.setSource(stockroom);
		stockOperation.setPatient(patient);
		stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
		
		if (orderNumber != null) {
			stockOperation.setOperationNumber(orderNumber); // Order ID
		} else {
			stockOperation.setOperationNumber(UUID.randomUUID().toString());
		}
		
		stockOperation.setStatus(StockOperationStatus.NEW);
		
		stockOperation.addItem(item, quantity);
		
		return iStockOperationService.submitOperation(stockOperation);
	}
	
	public static Stockroom getMainStockroom(Location location) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		
		Stockroom mainStockroom = null;
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && stockRoomAttribute.getValue().contains("Main")) {
					mainStockroom = stockroom;
				}
			}
		}
		
		return mainStockroom;
	}
	
	public static Stockroom getHtsStockroom(Location location) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		
		Stockroom htsStockroom = null;
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && stockRoomAttribute.getValue().contains("Hts Bulk")) {
					htsStockroom = stockroom;
				}
			}
		}
		
		return htsStockroom;
	}
	
	public static List<Stockroom> getHtsStockrooms(Location location) {
		
		List<Stockroom> stockrooms = new ArrayList<>();
		stockrooms.add(getHtsStockroom(location));
		stockrooms.addAll(getHtsDistributionStockrooms(location));
		
		return stockrooms;
	}
	
	public static List<Stockroom> getHtsDistributionStockrooms(Location location) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		
		List<Stockroom> htsStockrooms = new ArrayList<>();
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && stockRoomAttribute.getValue().contains("Hts Distribution")) {
					htsStockrooms.add(stockroom);
				}
			}
		}
		
		return htsStockrooms;
	}
	
	public static List<Stockroom> getDispensingStockrooms(Location location) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		
		Set<Stockroom> dispensingStockrooms = new HashSet<>();
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && stockRoomAttribute.getValue().contains("Dispensing")) {
					dispensingStockrooms.add(stockroom);
				}
			}
		}
		if (!dispensingStockrooms.isEmpty()) {
			return new ArrayList<>(dispensingStockrooms);
		} else {
			return new ArrayList<>();
		}
	}
	
	public static List<Stockroom> getBulkStockrooms(Location location) {
		if (location == null) {
			return new ArrayList<>();
		}
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		
		Set<Stockroom> bulkStockrooms = new HashSet<>();
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && stockRoomAttribute.getValue().contains("Bulk")) {
					bulkStockrooms.add(stockroom);
				}
			}
		}
		if (!bulkStockrooms.isEmpty()) {
			return new ArrayList<>(bulkStockrooms);
		} else {
			return new ArrayList<>();
		}
	}
	
	public static ItemStock getItemStockByStockRoom(Item item, Stockroom stockroom) {
		
		ItemSearch itemSearch = new ItemSearch();
		itemSearch.getTemplate().setUuid(item.getUuid());
		itemSearch.getTemplate().setName(item.getName());
		List<ItemStock> itemStockListByLocation = BotswanaInventoryContext.getStockRoomDataService().getItems(stockroom,
		    itemSearch, null);
		
		ItemStock itemStock = null;
		
		if (itemStockListByLocation.size() > 0) {
			itemStock = itemStockListByLocation.get(0);
		}
		
		return itemStock;
	}
	
	public static Integer getUnitConversionValue(Item item) {
		String regex = " \\d*'s";
		String string = item.getName();
		
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(string);
		if (!matcher.find()) {
			return 1;
		} else {
			String matched = matcher.group(0);
			try {
				return Integer.parseInt(matched.trim().replace("'s", ""));
			}
			catch (NumberFormatException e) {
				e.printStackTrace();
				return 1;
			}
		}
	}
	
	public static Double calculateAverageMonthlyConsumption(Stockroom stockRoom, Item item) {
		double averageMonthlyConsumption = 0.0;
		final int NUMBER_OF_MONTHS = 3;
		
		// To determine AMC:
		// Add last three (3) months’ consumption (kits used/usage) and divide by three (3)
		// AMC = Current month + Previous two (2) months usage divided by three (3)
		// Always round up to the next whole numbe
		
		// Get stock all stock operations and filter by -ve adjustments over the past 3 months
		StockOperationSearch amcStockOperationSearch = new StockOperationSearch();
		StockOperationTemplate amcStockOperationSearchTemplate = new StockOperationTemplate();
		amcStockOperationSearchTemplate.setStockroom(stockRoom);
		amcStockOperationSearchTemplate.setItem(item);
		amcStockOperationSearchTemplate.setInstanceType(WellKnownOperationTypes.getDistribution());
		
		amcStockOperationSearch.setTemplate(amcStockOperationSearchTemplate);
		
		IStockOperationDataService stockOperationDataService = BotswanaInventoryContext.getStockOperationDataService();
		List<StockOperation> amcStockOperations = stockOperationDataService.getOperations(amcStockOperationSearch);
		
		// Calculate the sum and divide by the time duration
		Date lastMonth = org.apache.commons.lang.time.DateUtils.addMonths(new Date(), -1);
		Calendar cal = Calendar.getInstance();
		cal.setTime(lastMonth);
		Date amcStartDate = DateUtils.lastDayOfMonth(YearMonth.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1)); // Get the last date of the previous month
		Date amcEndDate = org.apache.commons.lang.time.DateUtils.addMonths(amcStartDate, -3);
		Set<StockOperation> distributionStockOperations = amcStockOperations.stream().filter(op -> {
			return op.getOperationDate().before(amcStartDate) && op.getOperationDate().after(amcEndDate);
		}).collect(Collectors.toSet());
		
		int consumption = 0;
		for (StockOperation stockOperation : distributionStockOperations) {
			List<StockOperationTransaction> stockOperationTransactions = stockOperation.getTransactions().stream()
			        .filter(t -> t.getQuantity() < 0).collect(toList());
			
			for (StockOperationTransaction stockTx : stockOperationTransactions) {
				consumption = consumption + stockTx.getQuantity();
			}
		}
		
		averageMonthlyConsumption = ((double) consumption / NUMBER_OF_MONTHS) * -1; // remember to convert to +ve values
		
		return averageMonthlyConsumption;
	}
	
	public static boolean isMainStockRoom(Stockroom stockroom, Location location) {
		return getMainStockroom(location).equals(stockroom);
	}
	
	public static boolean isDispensingStockRoom(Stockroom stockroom, Location location) {
		return getDispensingStockrooms(location).contains(stockroom);
	}
	
	public static SimpleObject extractItemStockToSimpleObject(ItemStock itemStock, ItemStockDetail itemStockDetail) {
		SimpleObject simpleObject = new SimpleObject();
		simpleObject.put("uuid", itemStock.getUuid());
		simpleObject.put("code", itemStock.getItem().getCodes().stream().findFirst().map(ItemCode::getCode).orElse(null));
		simpleObject.put("name", itemStock.getItem().getName());
		if (itemStockDetail != null) {
			simpleObject.put("quantity", itemStockDetail.getQuantity());
			StockOperation batchOperation = itemStockDetail.getBatchOperation();
			simpleObject.put("batchOperationId", batchOperation == null ? "" : batchOperation.getId());
			simpleObject.put("batchNumber", batchOperation == null ? "" : batchOperation.getOperationNumber());
			simpleObject.put("expiryDate", itemStockDetail.getExpiration() == null ? "" : itemStockDetail.getExpiration());
		} else {
			simpleObject.put("quantity", itemStock.getQuantity());
		}
		return simpleObject;
	}
	
	public static List<SimplifiedItemStockDetail> filterByExpiryDate(
	        List<SimplifiedItemStockDetail> simplifiedItemStockDetails1, Date expiryDate) {
		List<SimplifiedItemStockDetail> simplifiedItemStockDetails = new ArrayList<>();
		for (SimplifiedItemStockDetail simplifiedItemStockDetail : simplifiedItemStockDetails1) {
			if (simplifiedItemStockDetail.getQuantity() != null && simplifiedItemStockDetail.getQuantity() > 0) {
				if (simplifiedItemStockDetail.getExpiryDate() != null
				        && simplifiedItemStockDetail.getExpiryDate().before(expiryDate)) {
					simplifiedItemStockDetails.add(simplifiedItemStockDetail);
				}
			}
		}
		return simplifiedItemStockDetails;
	}
	
	public static List<SimplifiedItemStockDetail> getAllItemsInLocation(Location location) {
		List<SimplifiedItemStockDetail> simplifiedItemStockDetails = new ArrayList<>();
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location,
		    false);
		for (Stockroom stockroom : stockrooms) {
			List<ItemStockDetail> itemStockDetails = BotswanaInventoryContext.getItemStockDetailDataService()
			        .getItemStockDetailsByStockroom(stockroom, null);
			for (ItemStockDetail itemStockDetail : itemStockDetails) {
				if (itemStockDetail.getExpiration() != null && itemStockDetail.getBatchOperation() != null) {
					SimplifiedItemStockDetail simplifiedItemStockDetail = SimplifiedItemStockDetail
					        .simplify(itemStockDetail);
					simplifiedItemStockDetail.setStockRoomId(itemStockDetail.getStockroom().getId());
					
					simplifiedItemStockDetails.add(simplifiedItemStockDetail);
				}
			}
		}
		return simplifiedItemStockDetails;
	}
	
	public static void reconstructInventoryTransaction() {
		// select stockroom
		IStockroomDataService stockroomDataService = Context.getService(StockroomDataServiceImpl.class);
		Location location = Context.getLocationService().getLocation(28);
		List<Stockroom> stockrooms = stockroomDataService.getStockroomsByLocation(location, false);
		IItemStockDataService itemStockDataService = Context.getService(ItemStockDataServiceImpl.class);
		;
		
		for (Stockroom stockroom : stockrooms) {
			// select stock room items
			Set<ItemStock> itemStockSet = stockroom.getItems();
			for (ItemStock itemStock : itemStockSet) {
				// For each item
				IStockOperationDataService stockOperationDataService = Context
				        .getService(StockOperationDataServiceImpl.class);
				// stockOperationDataService.get
				IStockOperationTransactionDataService stockOperationTransactionDataService = Context
				        .getService(StockOperationTransactionDataServiceImpl.class);
				
				StockOperationSearch stockOperationSearch = new StockOperationSearch();
				StockOperationTemplate template = new StockOperationTemplate();
				template.setItem(itemStock.getItem());
				template.setStockroom(itemStock.getStockroom());
				stockOperationSearch.setTemplate(template);
				List<StockOperation> stockOperations = stockOperationDataService.getOperations(stockOperationSearch);
				
				List<StockOperationTransaction> stockOperationTransactions = stockOperationTransactionDataService
				        .getTransactionByOperations(stockOperations, null);
				// sort by date
				Integer qty = 0;
				//      --> Fetch transactions
				for (StockOperationTransaction stockOperationTransaction : stockOperationTransactions) {
					qty = stockOperationTransaction.getQuantity() + qty;
					//      --> Retrieve batch operation
					//      --> Retrieve operation items
					//      --> Calculate items in stock -> update item stock qty
					//      --> Update item stock details
				}
				itemStock.setQuantity(qty);
				itemStockDataService.save(itemStock);
				Integer batchQty = 0;
				for (ItemStockDetail itemStockDetail : itemStock.getDetails()) {
					List<StockOperationTransaction> stockOperationTransaction = stockOperationTransactionDataService
					        .getTransactionByOperation(itemStockDetail.getBatchOperation(), null);
					
				}
			}
		}
	}
	
	public static Stockroom getStockroom(String stockroomUuid) {
		IStockroomDataService iStockroomDataService = BotswanaInventoryContext.getStockRoomDataService();
		return iStockroomDataService.getByUuid(stockroomUuid);
	}
	
	public static List<Stockroom> getNonBulkStockrooms(Location sessionLocation) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(sessionLocation, false);
		StockRoomAttributeType stockRoomAttributeType = BotswanaInventoryContext.getStockRoomAttributeTypeDataService()
		        .getByUuid(INV_STOCK_ROOM_TYPE_ATTRIBUTE_TYPE_UUID);
		Set<Stockroom> nonBulkStockrooms = new HashSet<>();
		for (Stockroom stockroom : stockrooms) {
			for (StockRoomAttribute stockRoomAttribute : stockroom.getAttributes()) {
				if (stockRoomAttribute.getAttributeType().equals(stockRoomAttributeType)
				        && !stockRoomAttribute.getValue().contains("Bulk")) {
					nonBulkStockrooms.add(stockroom);
				}
			}
		}
		if (!nonBulkStockrooms.isEmpty()) {
			return new ArrayList<>(nonBulkStockrooms);
		} else {
			return new ArrayList<>();
		}
	}
	
	public static List<Stockroom> getAllStockrooms(Location sessionLocation) {
		List<Stockroom> stockrooms = BotswanaInventoryContext.getStockRoomDataService()
		        .getStockroomsByLocation(sessionLocation, false);
		
		if (!stockrooms.isEmpty()) {
			return stockrooms;
		} else {
			return new ArrayList<>();
		}
	}
	
	public static Item getItemByDrug(Drug drug) {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		String drugUuidAttributeType = INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID;
		Item itemFound = null;
		
		ItemSearch itemSearch = new ItemSearch();
		itemSearch.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.EQUAL);
		if (drug != null) {
			itemSearch.getTemplate().setName(drug.getName());
			List<Item> itemsFound = iItemDataService.getItemsByItemSearch(itemSearch);
			// if no item found, search by drug concept
			if (itemsFound.isEmpty()) {
				itemsFound = iItemDataService.getItemsByConcept(drug.getConcept());
			}
			
			if (itemsFound.size() > 1) {
				for (Item item : itemsFound) {
					Set<ItemAttribute> itemAttributeSet = new HashSet<>(item.getAttributes());
					for (ItemAttribute itemAttribute : itemAttributeSet) {
						if (itemAttribute.getAttributeType().getUuid().equals(drugUuidAttributeType)
						        && itemAttribute.getValue().equals(drug.getUuid())) {
							itemFound = item;
							break;
						}
					}
				}
			} else if (itemsFound.size() == 1) {
				itemFound = itemsFound.get(0);
			}
		}
		
		return itemFound;
	}
	
	@Nullable
	public static Stockroom getDispensingStockroom(Location location) {
		return BotswanaInventoryContext.getStockRoomDataService().getStockroomsByLocation(location, false).stream()
		        .filter(s -> s.getName().contains("Dispensing")).findFirst().orElse(null);
	}
	
	public static ItemStock getItemStock(Item item, Location location) {
		IItemStockDataService iItemStockDataService = BotswanaInventoryContext.getItemStockDataService();
		
		Stockroom stockroom = getDispensingStockroom(location);
		List<ItemStock> itemStockListByLocation = BotswanaInventoryContext.getStockRoomDataService()
		        .getItemsByRoom(stockroom, null);
		
		ItemStock itemStock = null;
		
		if (!itemStockListByLocation.isEmpty()) {
			itemStock = itemStockListByLocation.get(0);
		}
		
		return itemStock;
	}
	
	public static Item getItemFromDrug(Drug drug) {
		IItemDataService iItemDataService = BotswanaInventoryContext.getItemDataService();
		List<Item> items = iItemDataService.getItemsByConcept(drug.getConcept());
		if (items == null || items.isEmpty()) {
			ItemSearch itemSearch = new ItemSearch();
			itemSearch.setNameComparisonType(BaseObjectTemplateSearch.StringComparisonType.EQUAL);
			itemSearch.getTemplate().setName(drug.getDisplayName());
			items = iItemDataService.getItemsByItemSearch(itemSearch);
		}
		Item item = null;
		
		Boolean found = false;
		for (Item sItem : items) {
			if (found) {
				break;
			}
			for (ItemAttribute itemAttribute : sItem.getAttributes()) {
				if (itemAttribute.getValue().equals(drug.getUuid())) {
					item = sItem;
					found = true;
					break;
				}
			}
		}
		
		return item;
	}

    public static void dispenseDrugFromObsList(Set<Obs> obsList) {
    	IItemStockDetailDataService itemStockService = BotswanaInventoryContext.getItemStockDetailDataService();
    
    	Obs stockroomObs = obsList.stream()
    	        .filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.HTS_STOCKROOM_UUID)).findFirst()
    	        .orElse(null);
    	List<Obs> drugObsList = obsList.stream().filter(o -> o.getConcept().getUuid().equals(BotswanaEmrConstants.DRUG_CONCEPT_UUID))
    	        .collect(Collectors.toList());
    	
    	if (stockroomObs != null && !drugObsList.isEmpty()) {
    		Stockroom stockroom = BotswanaInventoryContext.getStockRoomDataService()
    		        .getByUuid(stockroomObs.getValueText());
    		StockOperation stockOperation = new StockOperation();
    		stockOperation.setInstanceType(WellKnownOperationTypes.getDistribution());
    		stockOperation.setSource(stockroom);
    		
    		for (Obs obs : drugObsList) {
    			Drug drug = obs.getValueDrug();
    			if (drug != null) {
    				Item item = getItemByDrug(drug);
    				if (item != null) {
    					stockOperation.addItem(item, 1);
    					stockOperation.setOperationDate(new Date());
    					
    					List<ItemStockDetail> itemStockDetailList = itemStockService
    					        .getItemStockDetailsByStockroom(stockroom, null);
    					if (!itemStockDetailList.isEmpty()) {
    						ItemStockDetail itemStockDetail = itemStockDetailList.stream()
    						        .filter(x -> x.getExpiration() != null)
    						        .min(Comparator.comparing(ItemStockDetail::getExpiration)).orElse(null);
    						
    						stockOperation.addItem(item, 1, itemStockDetail.getExpiration(),
    						    itemStockDetail.getBatchOperation());
    						stockOperation.setOperationNumber(itemStockDetail.getBatchOperation().getOperationNumber());
    						
    						stockOperation.setStatus(StockOperationStatus.NEW);
    						BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
    						stockOperation.setStatus(StockOperationStatus.COMPLETED);
    						BotswanaInventoryContext.getStockOperationService().submitOperation(stockOperation);
    					}
    				}
    			}
    		}
    	}
    }
}
