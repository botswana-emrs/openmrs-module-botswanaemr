/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import lombok.Data;

import java.util.Date;

@Data
public class SimplifiedLabQualityControlTest {
	
	private Integer id;
	
	private String nameOfTest;
	
	private String kitName;
	
	private String qualityControlOption;
	
	private String lotNumber;
	
	private String testExpiryDate;
	
	private String testResults;
	
	private String interpretation;
	
	private Integer parent;
	
	private String acceptable;
	
	private String authorizedBy;
	
	private String authorizedByName;
	
	private String incident;
	
	private String correctiveActionTaken;
	
	private String stockRoomId;
	
	private String stockRoom;
	
}
