/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;
import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.INV_ITEM_EMERGENCY_ORDER_POINT_ATTRIBUTE_TYPE_UUID;

import java.util.Objects;
import java.util.Set;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.IItemAttributeTypeDataService;
import org.openmrs.module.botswanaemrInventory.model.ItemAttribute;
import org.openmrs.module.botswanaemrInventory.model.ItemAttributeType;
import org.openmrs.module.botswanaemrInventory.model.ItemCode;
import org.openmrs.module.botswanaemrInventory.model.ItemStock;

@Data
public class InventoryItemStockSimplifier {
	
	private Integer id;
	
	private String name;
	
	private String code;
	
	private String conceptName;
	
	private String description;
	
	private String unitOfIssue;
	
	private Integer unitOfIssueId;
	
	private String department;
	
	private String category;
	
	private String status;
	
	private Integer minimumStockLevel;
	
	private Integer maximumStockLevel;
	
	private Integer emergencyOrderPoint;
	
	private String drugUuid;
	
	private String drugName;
	
	private String productClass;
	
	private String productSubClass;
	
	private String stockRoom;
	
	public enum ItemStatus {
		
		ACTIVE("Active"),
		NOT_ACTIVE("Not Active");
		
		public final String label;
		
		ItemStatus(String label) {
			this.label = label;
		}
		
		public String getLabel() {
			return label;
		}
		
		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
		}
	}
	
	private static final IItemAttributeTypeDataService itemAttributeTypeDataService = BotswanaInventoryContext
	        .getItemAttributeTypeDataService();
	
	public static InventoryItemStockSimplifier simplify(ItemStock itemStock) {
		InventoryItemStockSimplifier simplifiedItemStock = new InventoryItemStockSimplifier();
		Set<ItemAttribute> itemAttributes = itemStock.getItem().getAttributes();
		ItemAttributeType status = itemAttributeTypeDataService.getByUuid(INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType category = itemAttributeTypeDataService.getByUuid(INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType unitOfIssue = itemAttributeTypeDataService.getByUuid(INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType drugUuid = itemAttributeTypeDataService.getByUuid(INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType maxStockLevelUuid = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_MAX_STOCK_LEVEL_ATTRIBUTE_TYPE_UUID);
		ItemAttributeType emergOrderPointUuid = itemAttributeTypeDataService
		        .getByUuid(INV_ITEM_EMERGENCY_ORDER_POINT_ATTRIBUTE_TYPE_UUID);
		
		simplifiedItemStock.setId(itemStock.getItem().getId());
		simplifiedItemStock.setName(itemStock.getItem().getName());
		ItemCode itemCode = itemStock.getItem().getCodes().stream().findFirst().orElse(null);
		simplifiedItemStock.setCode(itemCode != null ? itemCode.getCode() : null);
		simplifiedItemStock.setDescription(itemStock.getItem().getDescription());
		String unitOfIssueValue = getItemAttributeValue(itemAttributes, unitOfIssue);
		Concept unitOfIssueConcept = BotswanaEmrUtils.getConcept(unitOfIssueValue);
		simplifiedItemStock.setUnitOfIssue(unitOfIssueConcept != null ? unitOfIssueConcept.getDisplayString() : "");
		simplifiedItemStock.setUnitOfIssueId(unitOfIssueConcept != null ? unitOfIssueConcept.getConceptId() : null);
		simplifiedItemStock.setDepartment(itemStock.getItem().getDepartment().getName());
		simplifiedItemStock.setCategory(getItemAttributeValue(itemAttributes, category));
		simplifiedItemStock.setStatus(getItemAttributeValue(itemAttributes, status));
		simplifiedItemStock.setDrugUuid(getItemAttributeValue(itemAttributes, drugUuid));
		simplifiedItemStock.setMinimumStockLevel(itemStock.getItem().getMinimumQuantity());
		Drug drug = Context.getConceptService().getDrugByUuid(simplifiedItemStock.getDrugUuid());
		simplifiedItemStock.setDrugName(drug == null ? "" : drug.getName());
		Concept concept = itemStock.getItem().getConcept();
		simplifiedItemStock.setConceptName(concept == null ? "" : concept.getName().getName());
		simplifiedItemStock.setStockRoom(itemStock.getStockroom().getName());
		
		return simplifiedItemStock;
	}
	
	private static String getItemAttributeValue(Set<ItemAttribute> itemAttributes, ItemAttributeType itemAttributeType) {
		for (ItemAttribute itemAttribute : itemAttributes) {
			if (Objects.equals(itemAttribute.getAttributeType(), itemAttributeType)) {
				return itemAttribute.getValue();
			}
		}
		return null;
	}
}
