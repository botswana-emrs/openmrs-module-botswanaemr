/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;
import org.openmrs.module.botswanaemr.utilities.DateUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Slf4j
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SputumLabResult implements Serializable, Summary {
	
	private static final String GENE_X_EXPERT_CONCEPT = "164945AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String MICROSCOPY_CONCEPT = "307AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String AT_MONTH = "1ce74eee-92dd-46d1-b2ce-ff5f00e86ebd";
	
	private static final String DATE_OF_TEST = "";
	
	private static final String TEST_RESULTS = "";
	
	private Encounter encounter;
	
	private String atWhichMonth;
	
	private String date;
	
	private String tbTest;
	
	private String result;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		return null;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return null;
	}
	
	private boolean compareQuestion(Obs obs, String concept) {
		if (obs.getConcept() != null) {
			return obs.getConcept().getUuid().equals(concept);
		}
		return false;
	}
	
}
