/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.inventory;

import org.openmrs.module.botswanaemrInventory.model.Item;
import org.openmrs.module.initializer.Domain;
import org.openmrs.module.initializer.api.ConfigDirUtil;
import org.openmrs.module.initializer.api.loaders.BaseCsvLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class InventoryLoader extends BaseCsvLoader<Item, InventoryCsvParser> {
	
	@Autowired
	public void setParser(InventoryCsvParser parser) {
		this.parser = parser;
	}
	
	public static final String DOMAIN_INV = "inventory";
	
	@Override
	public Integer getOrder() {
		return Domain.values().length + 1;
	}
	
	@Override
	public ConfigDirUtil getDirUtil() {
		return new ConfigDirUtil(iniz.getConfigDirPath(), iniz.getChecksumsDirPath(), DOMAIN_INV, cfg.skipChecksums());
	}
	
	protected void preload(File file) {
	}
	
	@Override
	public String getDomainName() {
		return DOMAIN_INV;
	}
}
