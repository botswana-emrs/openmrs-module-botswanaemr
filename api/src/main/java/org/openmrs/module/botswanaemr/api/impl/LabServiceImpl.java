/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import lombok.NoArgsConstructor;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.TestOrder;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.api.dao.LabDAO;
import org.openmrs.module.botswanaemr.lab.Lab;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;

import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
public class LabServiceImpl extends BaseOpenmrsService implements LabService {
	
	protected LabDAO dao;
	
	public void setDao(LabDAO dao) {
		this.dao = dao;
	}
	
	public Lab saveLab(Lab lab) throws APIException {
		return dao.saveLab(lab);
	}
	
	public List<Lab> getAllActiveLabs() throws APIException {
		return dao.getAllActiveLab();
	}
	
	public List<Lab> getAllLab() throws APIException {
		return dao.getAllLab();
	}
	
	public Lab getLabById(Integer labId) throws APIException {
		return dao.getLabById(labId);
	}
	
	public Lab getLabByName(String name) throws APIException {
		return dao.getLabByName(name);
	}
	
	public LabTest getLabTestById(Integer labTestId) throws APIException {
		return dao.getLabTestById(labTestId);
	}
	
	public LabTest getLabTestByOrder(Order order) throws APIException {
		return dao.getLabTestByOrder(order);
	}
	
	public LabTest getLabTestBySampleNumber(String sampleNumber) throws APIException {
		return dao.getLabTestBySampleNumber(sampleNumber);
	}
	
	public LabTest saveLabTest(LabTest labTest) throws APIException {
		return dao.saveLabTest(labTest);
	}
	
	public void deleteLab(Lab lab) throws APIException {
		dao.deleteLab(lab);
	}
	
	public Lab getLabByRole(Role role) throws APIException {
		return dao.getLabByRole(role);
	}
	
	public List<Lab> getLabByRoles(List<Role> roles) throws APIException {
		return dao.getLabByRoles(roles);
	}
	
	public List<LabTest> getLatestLabTestByLabAndDate(Lab lab, Date date) throws APIException {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			Date today = Context.getDateFormat().parse(Context.getDateFormat().format(cal.getTime()));
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + 1);
			Date nextDay = Context.getDateFormat().parse(Context.getDateFormat().format(cal.getTime()));
			return dao.getLatestLabTestByDate(today, nextDay, lab);
		}
		catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<LabTest> getLatestLabTestByDateOnly(Date date) throws APIException, ParseException {
		return dao.getLatestLabTestByDateOnly(date);
	}
	
	public String getNextSampleNumber(Lab lab, Date date) throws APIException {
		List<LabTest> tests = getLatestLabTestByLabAndDate(lab, date);
		if (tests != null && tests.size() > 0) {
			LabTest test = tests.get(tests.size() - 1);
			String sampleNumber = test.getSampleNumber();
			sampleNumber = sampleNumber.substring(11);
			int number = NumberUtils.toInt(sampleNumber) + 1;
			return Context.getDateFormat().format(date) + "-" + number;
		}
		return Context.getDateFormat().format(date) + "-" + 1;
	}
	
	public void deleteLabTest(LabTest labtest) throws APIException {
		dao.deleteLabTest(labtest);
	}
	
	public void deleteLabTestByOrder(Order order) throws APIException {
		LabTest labTest = getLabTestByOrder(order);
		if (labTest != null) {
			dao.deleteLabTest(labTest);
		}
	}
	
	public List<LabTest> getCompletedLaboratoryTests(Date date, String phrase, Set<Concept> allowableTests)
	        throws APIException, ParseException {
		
		List<Patient> patients = null;
		if (!StringUtils.isBlank(phrase)) {
			patients = Context.getPatientService().getPatients(phrase);
		}
		
		return dao.getLaboratoryTestsByDiscontinuedDate(date, allowableTests, patients);
	}
	
	public List<LabTest> getLaboratoryTestsByDateAndPatientOnGivenDay(Date date, Patient patient)
	        throws APIException, ParseException {
		return dao.getLaboratoryTestsByDateAndPatient(date, patient);
	}
	
	public List<Order> getOrders(Patient patient, Date date, Concept concept) throws APIException, ParseException {
		return dao.getOrders(patient, date, concept);
	}
	
	public LabTest getLaboratoryTest(Encounter encounter) throws APIException {
		return dao.getLaboratoryTest(encounter);
	}
	
	public List<LabTest> getAllLabTests() throws APIException {
		return dao.getAllLabTests();
	}
	
	public void saveOrderAndLabTest(Set<Concept> savedSetOrders, Encounter encounter) throws APIException {
		//to provide the correct lab form encounter type
		OrderType labOrderType = Context.getOrderService().getOrderTypeByUuid(BotswanaEmrConstants.TEST_ORDER_TYPE_UUID);
		
		if (savedSetOrders != null && savedSetOrders.size() > 0) {
			for (Concept concept : savedSetOrders) {
				if (concept != null && encounter != null) {
					Order order = addOrder(encounter, concept, labOrderType);
					//save this order
					Context.getOrderService().saveOrder(order, null);
					//using this order, now create a LabTest order in the simple lab table and object
					
					Context.getService(LabService.class).saveLabTest(addLabOrder(order));
					saveLabTest(addLabOrder(order));
				}
			}
		}
	}
	
	private Order addOrder(Encounter encounter, Concept concept, OrderType orderType) {
		Order order = new TestOrder();
		order.setConcept(concept);
		order.setCreator(encounter.getCreator());
		order.setDateCreated(encounter.getDateCreated());
		if (BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()) != null) {
			order.setOrderer(BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()));
		}
		order.setPatient(encounter.getPatient());
		order.setDateActivated(encounter.getDateCreated());
		order.setAccessionNumber("0");
		order.setOrderType(orderType);
		order.setEncounter(encounter);
		order.setCareSetting(Context.getOrderService().getCareSettingByUuid(BotswanaEmrConstants.CARE_SETTING_UUID));
		return order;
	}
	
	private LabTest addLabOrder(Order order) {
		LabTest labTest = new LabTest();
		labTest.setAcceptDate(order.getDateCreated());
		labTest.setOrder(order);
		//Not sure about this
		labTest.setLabTestStatus(0);
		labTest.setPatient(order.getPatient());
		labTest.setConcept(order.getConcept());
		labTest.setStatus(BotswanaEmrConstants.labStatus.PENDING.name());
		labTest.setCreator(order.getCreator());
		labTest.setEncounter(order.getEncounter());
		return labTest;
	}
	
	public List<LabTest> getLaboratoryTestsByPatient(Patient patient) throws APIException {
		return dao.getLaboratoryTestsByPatient(patient);
	}
	
	@Override
	public List<LabTest> getAllCompletedLabTestOrders(@NotNull Patient patient) {
		return dao.getAllCompletedLabTestOrders(patient);
	}
	
	@Override
	public List<Concept> searchLabTest(String text) throws APIException {
		ConceptClass test = Context.getConceptService().getConceptClassByUuid("8d4907b2-c2cc-11de-8d13-0010c6dffd0f");
		ConceptClass testPanels = Context.getConceptService().getConceptClassByUuid("8d492026-c2cc-11de-8d13-0010c6dffd0f");
		return dao.searchConceptsByNameAndClass(text, test, testPanels);
	}
	
	@Override
	public List<LabTest> getLaboratoryTestListByEncounter(Encounter encounter) throws APIException {
		return dao.getLaboratoryTestListByEncounter(encounter);
	}
}
