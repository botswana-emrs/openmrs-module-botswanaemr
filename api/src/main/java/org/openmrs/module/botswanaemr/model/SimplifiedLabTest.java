/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.Order;
import org.openmrs.Patient;

import java.util.Map;

public class SimplifiedLabTest {
	
	private Integer patientId;
	
	private String patientNames;
	
	private String testName;
	
	private String requestedBy;
	
	private Integer testId;
	
	public String getPatientNames() {
		return patientNames;
	}
	
	public void setPatientNames(String patientNames) {
		this.patientNames = patientNames;
	}
	
	public String getTestName() {
		return testName;
	}
	
	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	public String getDateAdded() {
		return dateAdded;
	}
	
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	
	public String getDateReceived() {
		return dateReceived;
	}
	
	public void setDateReceived(String dateReceived) {
		this.dateReceived = dateReceived;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	private String dateAdded;
	
	private String dateReceived;
	
	private String status;
	
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	private Order order;
	
	public Integer getLabOrderTestId() {
		return labOrderTestId;
	}
	
	public void setLabOrderTestId(Integer labOrderTestId) {
		this.labOrderTestId = labOrderTestId;
	}
	
	private Integer labOrderTestId;
	
	public Patient getPatient() {
		return patient;
	}
	
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	private Patient patient;
	
	public String getResults() {
		return results;
	}
	
	public void setResults(String results) {
		this.results = results;
	}
	
	private String results;
	
	public Map<String, String> getResultsList() {
		return resultsList;
	}
	
	public void setResultsList(Map<String, String> resultsList) {
		this.resultsList = resultsList;
	}
	
	private Map<String, String> resultsList;
	
	public Integer getPatientId() {
		return patientId;
	}
	
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	
	public String getRequestedBy() {
		return requestedBy;
	}
	
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	
	public Integer getTestId() {
		return testId;
	}
	
	public void setTestId(Integer testId) {
		this.testId = testId;
	}
}
