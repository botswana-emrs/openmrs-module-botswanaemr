/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.User;

import javax.persistence.*;
import java.util.Date;

/**
 * A message exchanged between healthcare providers as part of a {@link Discussion}.
 */
@Entity
@Table(name = "message")
public class Message {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "message_id")
	private Integer messageId;
	
	@Column(name = "date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "authored_by", nullable = false)
	private User authoredBy;
	
	@Column(name = "content")
	private String content;
	
	@ManyToOne
	@JoinColumn(name = "discussion_id", nullable = false)
	private Discussion discussion;
	
	public Integer getMessageId() {
		return messageId;
	}
	
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public User getAuthoredBy() {
		return authoredBy;
	}
	
	public void setAuthoredBy(User authoredBy) {
		this.authoredBy = authoredBy;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public Discussion getDiscussion() {
		return discussion;
	}
	
	public void setDiscussion(Discussion discussion) {
		this.discussion = discussion;
	}
}
