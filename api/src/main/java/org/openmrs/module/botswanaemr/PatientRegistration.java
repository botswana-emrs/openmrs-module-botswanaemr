/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.openmrs.ui.framework.SimpleObject;

import java.io.Serializable;
import java.util.ArrayList;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public @Data class PatientRegistration implements Serializable {
	
	String patientType;
	
	String idType;
	
	String citizenType;
	
	String idNumber;
	
	String givenName;
	
	String middleName;
	
	String familyName;
	
	String gender;
	
	String dob;
	
	boolean dobEstimated;
	
	String email;
	
	String contactNumber;
	
	String altContactNumber;
	
	String altContactNumber2;
	
	String altContactNumber3;
	
	String altContactNumber4;
	
	String altContactNumber5;
	
	String numberType;
	
	String altNumberType1;
	
	String altNumberType2;
	
	String altNumberType3;
	
	String altNumberType4;
	
	String altNumberType5;
	
	String occupation;
	
	String affiliation;
	
	String forceNumber;
	
	String rank;
	
	String otherOccupation;
	
	String employerName;
	
	String country;
	
	String address2;
	
	String cityVillage;
	
	String address4;
	
	String address5;
	
	String address6;
	
	String address7;
	
	String patientFacilityLocation;
	
	String education;
	
	String emergencyPatientType;
	
	String maritalStatus;
	
	String employmentSector;
	
	String otherEmploymentSector;
	
	String location;
	
	String MigrationPatientId;
	
	String MigrationFacilityId;
	
	@JsonProperty("nok")
	ArrayList<NextOfKin> nok = new ArrayList<>();
	
	@JsonProperty("relationShips")
	ArrayList<SimpleObject> relationShips;
	
	@JsonIgnoreProperties
	public @Data static class NextOfKin {
		
		String nokIdNumber;
		
		@JsonProperty("nokFullName")
		String nokFullName;
		
		String nokRelationship;
		
		String otherNokRelationship;
		
		String nokContact;
		
		String extraNokContact;
		
		String thirdContact;
		
		String fourthContact;
		
		String fifthContact;
		
		String nokEmail;
		
		String idType;
		
		String contactPersonType;
		
		String district;
		
		String city;
		
	}
	
}
