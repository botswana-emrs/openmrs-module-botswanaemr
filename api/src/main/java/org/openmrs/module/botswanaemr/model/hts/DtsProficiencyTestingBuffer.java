/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import lombok.Data;
import org.openmrs.BaseOpenmrsObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "dts_proficiency_testing_buffer")
public class DtsProficiencyTestingBuffer extends BaseOpenmrsObject {
	
	@Id
	@GeneratedValue
	@Column(name = "pt_buffer_id")
	private Integer id;
	
	@Column(name = "dts_buffer_id")
	private String dtsBufferId;
	
	@Column(name = "lot_number")
	private String dtsLotNumber;
	
	@Column(name = "expiry_date")
	private Date dtsExpiryDate;
	
	@Column(name = "condition_of_specimen")
	private String dtsConditionOfSpecimen;
	
	@Column(name = "reason_for_unacceptable_specimen")
	private String dtsReasonForUnacceptableSpecimen;
	
	@Column(name = "corrective_actions")
	private String dtsCorrectiveActions;
	
	@ManyToOne
	@JoinColumn(name = "pt_id")
	private HtsProficiencyTesting dtsProficiencyTesting;
	
	public HtsProficiencyTesting getDtsProficiencyTesting() {
		return dtsProficiencyTesting;
	}
	
	public void setDtsProficiencyTesting(HtsProficiencyTesting dtsProficiencyTesting) {
		this.dtsProficiencyTesting = dtsProficiencyTesting;
	}
	
	@Override
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 41;
	}
}
