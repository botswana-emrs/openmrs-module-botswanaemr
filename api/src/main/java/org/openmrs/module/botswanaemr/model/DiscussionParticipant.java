/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.User;

import javax.persistence.*;

@Entity
@Table(name = "discussion_participant")
public class DiscussionParticipant {
	
	@EmbeddedId
	private DiscussionParticipantKey id = new DiscussionParticipantKey();
	
	@ManyToOne
	@MapsId("discussionId")
	@JoinColumn(name = "discussion_id")
	private Discussion discussion;
	
	@ManyToOne
	@MapsId("participantId")
	@JoinColumn(name = "participant_id")
	private User user;
	
	@Column(name = "no_of_messages_read")
	private int noOfMessagesRead;
	
	public DiscussionParticipantKey getId() {
		return id;
	}
	
	public void setId(DiscussionParticipantKey id) {
		this.id = id;
	}
	
	public Discussion getDiscussion() {
		return discussion;
	}
	
	public void setDiscussion(Discussion discussion) {
		this.discussion = discussion;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public int getNoOfMessagesRead() {
		return noOfMessagesRead;
	}
	
	public void setNoOfMessagesRead(int noOfMessagesRead) {
		this.noOfMessagesRead = noOfMessagesRead;
	}
}
