/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;

import java.util.Set;

public class ObsData {
	
	public Obs codedObs;
	
	public Obs commentObs;
	
	public Obs nonCodedObs;
	
	public void setObs(Set<Obs> memberObS) {
		for (Obs obs : memberObS) {
			if (obs.getValueCoded() != null) {
				codedObs = obs;
			} else if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.PATIENT_NOTE_CONCEPT)) {
				commentObs = obs;
			} else {
				nonCodedObs = obs;
			}
		}
		
	}
}
