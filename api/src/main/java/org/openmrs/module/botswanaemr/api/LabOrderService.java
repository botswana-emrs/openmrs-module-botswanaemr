/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api;

import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.TestOrder;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

public interface LabOrderService {
	
	/**
	 * Gets active orders for a given patient
	 * 
	 * @param patient the patient
	 * @return {@link List} of active {@link Order}(s)
	 */
	List<Order> getActiveOrders(@NotNull Patient patient);
	
	void createLabTest(@NotNull Order order);
	
	void createLabTestsFromOrdersSet(Set<Order> orderSet);
	
	TestOrder createOrder(@NotNull Encounter encounter);
	
	/**
	 * Updates both order and labTest
	 * 
	 * @param patient the patient
	 * @param encounter the encounter containing results
	 */
	void updateLabOrder(@NotNull Patient patient, @NotNull Encounter encounter);
	
	//	Order placeDrugOrder(@NotNull Patient patient, @NotNull Encounter encounter, String drugUuid, Integer duration, Integer refills, Double dosage,
	//			String frequencyUuid, String routeConceptUuid);
	
	List<Order> getAllActiveMedications(@NotNull Patient patient);
	
	Set<Order> createOrderByEncounterAndListOfConcepts(@NotNull Encounter encounter, List<Concept> conceptList);
}
