/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.openmrs.Form;
import org.openmrs.User;

import java.io.Serializable;
import java.util.Date;

public class SimplifiedEncounter implements Serializable {
	
	private Integer visitId;
	
	private Integer encounterId;
	
	private String encounterUuid;
	
	private String status;
	
	private Integer patientId;
	
	private String pin;
	
	private String patientNames;
	
	private String patientAge;
	
	private String patientGender;
	
	private String patientDateOfBirth;
	
	private String patientRegistrationDate;
	
	private String consultationDate;
	
	private User creator;
	
	private Date encounterDatetime;
	
	private String partnerName;
	
	private Form form;
	
	public SimplifiedEncounter() {
	}
	
	public Integer getVisitId() {
		return visitId;
	}
	
	public void setVisitId(Integer visitId) {
		this.visitId = visitId;
	}
	
	public Integer getEncounterId() {
		return encounterId;
	}
	
	public void setEncounterId(Integer encounterId) {
		this.encounterId = encounterId;
	}
	
	public String getEncounterUuid() {
		return encounterUuid;
	}
	
	public void setEncounterUuid(String encounterUuid) {
		this.encounterUuid = encounterUuid;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getPatientId() {
		return patientId;
	}
	
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	
	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	public String getPatientNames() {
		return patientNames;
	}
	
	public void setPatientNames(String patientNames) {
		this.patientNames = patientNames;
	}
	
	public String getPatientAge() {
		return patientAge;
	}
	
	public void setPatientAge(String patientAge) {
		this.patientAge = patientAge;
	}
	
	public String getPatientGender() {
		return patientGender;
	}
	
	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}
	
	public String getPatientDateOfBirth() {
		return patientDateOfBirth;
	}
	
	public void setPatientDateOfBirth(String patientDateOfBirth) {
		this.patientDateOfBirth = patientDateOfBirth;
	}
	
	public String getPatientRegistrationDate() {
		return patientRegistrationDate;
	}
	
	public void setPatientRegistrationDate(String patientRegistrationDate) {
		this.patientRegistrationDate = patientRegistrationDate;
	}
	
	public String getConsultationDate() {
		return consultationDate;
	}
	
	public void setConsultationDate(String consultationDate) {
		this.consultationDate = consultationDate;
	}
	
	public Form getForm() {
		return form;
	}
	
	public void setForm(Form form) {
		this.form = form;
	}
	
	public String getPartnerName() {
		return partnerName;
	}
	
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
	public Date getEncounterDatetime() {
		return encounterDatetime;
	}
	
	public void setEncounterDatetime(Date encounterDatetime) {
		this.encounterDatetime = encounterDatetime;
	}
	
	public User getCreator() {
		return creator;
	}
	
	public void setCreator(User creator) {
		this.creator = creator;
	}
	
}
