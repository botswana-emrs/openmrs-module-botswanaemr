/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseChangeableOpenmrsMetadata;
import org.openmrs.Privilege;

/**
 * An PaymentMethod defines how a certain kind of {@link Registration}.
 *
 * @see Registration
 */
@Entity
@Table(name = "service_provider")
@BatchSize(size = 25)
public class ServiceProvider extends BaseChangeableOpenmrsMetadata {
	
	public static final long serialVersionUID = 789L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "service_provider_id")
	private Integer serviceProviderId;
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	/** default constructor */
	public ServiceProvider() {
	}
	
	/**
	 * Constructor with id <strong>Should</strong> set payment method id with given parameter
	 */
	public ServiceProvider(Integer serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	
	/**
	 * Required values constructor. This is the minimum number of values that must be non-null in order
	 * to have a successful save to the database
	 *
	 * @param name the name of this payment method
	 * @param description a short description of why this payment exists
	 */
	public ServiceProvider(String name, String description) {
		setName(name);
		setDescription(description);
	}
	
	/**
	 * @return Returns the paymentMethodId.
	 */
	public Integer getServiceProviderId() {
		return serviceProviderId;
	}
	
	/**
	 * @param serviceProviderId The encounterTypeId to set.
	 */
	public void setServiceProviderId(Integer serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	
	/**
	 * Gets privilege which can view this type of payment
	 * 
	 * @return the viewPrivilege the privilege instance
	 */
	public Privilege getViewPrivilege() {
		return viewPrivilege;
	}
	
	/**
	 * Sets privilege which can view this type of payment
	 * 
	 * @param viewPrivilege the viewPrivilege to set
	 */
	public void setViewPrivilege(Privilege viewPrivilege) {
		this.viewPrivilege = viewPrivilege;
	}
	
	/**
	 * Gets privilege which can edit this type of payment
	 * 
	 * @return the editPrivilege the privilege instance
	 */
	public Privilege getEditPrivilege() {
		return editPrivilege;
	}
	
	/**
	 * Sets privilege which can edit this type of Payment
	 * 
	 * @param editPrivilege the editPrivilege to set
	 */
	public void setEditPrivilege(Privilege editPrivilege) {
		this.editPrivilege = editPrivilege;
	}
	
	private Privilege viewPrivilege;
	
	private Privilege editPrivilege;
	
	/**
	 * @since 1.5
	 * @see org.openmrs.OpenmrsObject#getId()
	 */
	@Override
	public Integer getId() {
		return getServiceProviderId();
	}
	
	/**
	 * @since 1.5
	 * @see org.openmrs.OpenmrsObject#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		setServiceProviderId(id);
	}
}
