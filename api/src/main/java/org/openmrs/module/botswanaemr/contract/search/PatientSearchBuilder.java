/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.contract.PatientResponse;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PatientSearchBuilder {
	
	public static final String SELECT_STATEMENT = "SELECT distinct p.uuid,\n" + "                pi.identifier,\n"
	        + "                p.birthdate     as birthDate,\n" + "                pt.patient_id   as patientId,\n"
	        + "                pn.given_name   as givenName,\n" + "                pn.middle_name  as middleName,\n"
	        + "                pn.family_name  as familyName,\n" + "                p.gender,\n"
	        + "                p.death_date    as deathDate,\n" + "                pt.date_created as dateCreated,\n"
	        + "                p.dead,\n" + "                v.visit_id as visitNumber,"
	        + "                t.dateLastTested as dateLastTested";
	
	public static final String WHERE_CLAUSE = " where pt.voided = 0 and p.voided = 0 and pn.voided = 0 and pn.preferred=true ";
	
	public static final String FROM_TABLE = " FROM patient pt ";
	
	public static final String JOIN_CLAUSE = " INNER JOIN person p on pt.patient_id = p.person_id\n"
	        + "         LEFT JOIN person_name pn on p.person_id = pn.person_id\n"
	        + "         LEFT JOIN patient_identifier pi ON pi.patient_id = pt.patient_id\n"
	        + "            AND pi.identifier_type = (SELECT patient_identifier_type_id FROM patient_identifier_type WHERE name = 'OpenMRS ID')\n"
	        + "            AND pi.voided = 0\n"
	        + "         LEFT JOIN visit v on v.patient_id = pt.patient_id AND v.date_stopped IS NULL AND v.voided=0\n"
	        + "         LEFT JOIN (SELECT MAX(value_datetime) as dateLastTested, person_id AS patient_id FROM obs WHERE voided=0 AND concept_id IN (SELECT concept_id FROM concept WHERE uuid = '"
	        + BotswanaEmrConstants.DATE_OF_HIV_TEST_CONCEPT_UUID + "' OR uuid = '"
	        + BotswanaEmrConstants.HIV_TEST_DATE_CONCEPT_UUID
	        + "') GROUP BY patient_id) t on t.patient_id = pt.patient_id\n";
	
	public static final String ORDER_BY = " ORDER BY pt.date_created desc LIMIT :limit OFFSET :offset";
	
	private static final String LIMIT_PARAM = "limit";
	
	private static final String OFFSET_PARAM = "offset";
	
	public static final String LOCATION_PARAM = "locationUuidParam";
	
	public static final String GENDER_PARAM = "genderParam";
	
	public static final String START_DATE_REGISTERED_PARAM = "startDateRegisteredParam";
	
	public static final String END_DATE_REGISTERED_PARAM = "endDateRegisteredParam";
	
	private String select;
	
	private String where;
	
	private String from;
	
	private String join;
	
	private String groupBy;
	
	private String orderBy;
	
	private SessionFactory sessionFactory;
	
	private Map<String, Type> types;
	
	public PatientSearchBuilder(SessionFactory sessionFactory) {
		select = SELECT_STATEMENT;
		where = WHERE_CLAUSE;
		from = FROM_TABLE;
		join = JOIN_CLAUSE;
		orderBy = ORDER_BY;
		groupBy = "";
		this.sessionFactory = sessionFactory;
		types = new HashMap<>();
		
	}
	
	public PatientSearchBuilder withPatientName(String name, boolean includeIdentifierSearch) {
		PatientNameQueryHelper patientNameQueryHelper = new PatientNameQueryHelper(name, includeIdentifierSearch);
		where = patientNameQueryHelper.appendToWhereClause(where);
		return this;
	}
	
	public PatientSearchBuilder withLocationAttribute() {
		PatientLocationAttributeQueryHelper helper = new PatientLocationAttributeQueryHelper();
		join = helper.appendToJoinClause(join);
		return this;
	}
	
	public PatientSearchBuilder withPatientProgram(String programUuid) {
		PatientProgramQueryHelper programQueryHelper = new PatientProgramQueryHelper(programUuid);
		join = programQueryHelper.appendToJoinClause(join);
		return this;
	}
	
	public PatientSearchBuilder withPatientCondition(String condition) {
		PatientConditionQueryHelper patientConditionQueryHelper = new PatientConditionQueryHelper(condition);
		join = patientConditionQueryHelper.appendToJoinClause(join);
		return this;
	}
	
	public PatientSearchBuilder withHtsTestingLocation(String testingLocation) {
		PatientHtsTestingPointsQueryHelper patientConditionQueryHelper = new PatientHtsTestingPointsQueryHelper(
		        testingLocation);
		join = patientConditionQueryHelper.appendToJoinClause(join);
		return this;
	}
	
	private void appendToWhereClause(String operator, String condition) {
		where = String.format(" %s %s %s ", where, operator, condition);
	}
	
	public NativeQuery buildSqlQuery(Integer limit, Integer offset, String locationUuid, String gender,
	        String startDateRegistered, String endDateRegistered, String status) {
		if (StringUtils.isNotEmpty(gender)) {
			appendToWhereClause("and", "p.gender= :genderParam ");
		}
		if (StringUtils.isNotEmpty(startDateRegistered)) {
			appendToWhereClause("and", "DATE(pt.date_created)>=DATE(:startDateRegisteredParam)");
		}
		if (StringUtils.isNotEmpty(endDateRegistered)) {
			appendToWhereClause("and", "DATE(pt.date_created)<=DATE(:endDateRegisteredParam)");
		}
		if (StringUtils.isNotEmpty(status)) {
			if (status.equals("Active")) {
				appendToWhereClause("and", "p.dead = 0");
			} else if (status.equals("Expired")) {
				appendToWhereClause("and", "p.dead = 1");
			}
		}
		String query = select + from + join + where + orderBy;
		
		NativeQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(query)
		        .addScalar("patientId", StandardBasicTypes.INTEGER).addScalar("uuid", StandardBasicTypes.STRING)
		        .addScalar("identifier", StandardBasicTypes.STRING).addScalar("givenName", StandardBasicTypes.STRING)
		        .addScalar("middleName", StandardBasicTypes.STRING).addScalar("familyName", StandardBasicTypes.STRING)
		        .addScalar("gender", StandardBasicTypes.STRING).addScalar("birthDate", StandardBasicTypes.DATE)
		        .addScalar("dateCreated", StandardBasicTypes.DATE).addScalar("deathDate", StandardBasicTypes.DATE)
		        .addScalar("dead", StandardBasicTypes.BOOLEAN);
		
		Iterator<Map.Entry<String, Type>> iterator = types.entrySet().iterator();
		
		while (iterator.hasNext()) {
			Map.Entry<String, Type> entry = iterator.next();
			sqlQuery.addScalar(entry.getKey(), entry.getValue());
		}
		
		sqlQuery.setParameter(LIMIT_PARAM, limit);
		sqlQuery.setParameter(OFFSET_PARAM, offset);
		if (StringUtils.isNotEmpty(locationUuid))
			sqlQuery.setParameter(LOCATION_PARAM, locationUuid);
		if (StringUtils.isNotEmpty(gender))
			sqlQuery.setParameter(GENDER_PARAM, gender);
		if (StringUtils.isNotEmpty(startDateRegistered))
			sqlQuery.setParameter(START_DATE_REGISTERED_PARAM, startDateRegistered);
		if (StringUtils.isNotEmpty(endDateRegistered))
			sqlQuery.setParameter(END_DATE_REGISTERED_PARAM, endDateRegistered);
		
		sqlQuery.setResultTransformer(Transformers.aliasToBean(PatientResponse.class));
		return sqlQuery;
	}
	
	public PatientSearchBuilder withVmmcVisitStatusFilter(String visitStatus) {
		PatientVmmcQueryHelper vmmcQueryHelper = new PatientVmmcQueryHelper(visitStatus);
		join = vmmcQueryHelper.appendVisitStatusFilterToJoinClause(join);
		
		return this;
	}
	
	public PatientSearchBuilder withLastVisitDateFilter(String visitStartDate, String visitEndDate) {
		PatientVmmcQueryHelper vmmcQueryHelper = new PatientVmmcQueryHelper(visitStartDate, visitEndDate);
		
		join = vmmcQueryHelper.appendLastVisitDateToJoinClause(join);
		
		select = vmmcQueryHelper.appendSelectVisitDateToSelectClause(select);
		
		return this;
	}
}
