/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.contract.search;

import org.apache.commons.lang3.StringUtils;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;

public class PatientVmmcQueryHelper {
	
	private final String visitStatus;
	
	private final String visitStartDate;
	
	private final String visitEndDate;
	
	public PatientVmmcQueryHelper() {
		this.visitStatus = "";
		this.visitStartDate = "";
		this.visitEndDate = "";
	}
	
	public PatientVmmcQueryHelper(String visitStatus) {
		this.visitStatus = visitStatus;
		this.visitStartDate = "";
		this.visitEndDate = "";
	}
	
	public PatientVmmcQueryHelper(String visitStartDate, String visitEndDate) {
		this.visitStartDate = visitStartDate;
		this.visitEndDate = visitEndDate;
		this.visitStatus = "";
	}
	
	public String appendVisitStatusFilterToJoinClause(String join) {
		if (StringUtils.isEmpty(this.visitStatus)) {
			return join;
		}
		
		String encounterString = join;
		// Descriptions of the visit statuses can be found here https://bitri.atlassian.net/browse/EMRS-2230
		switch (visitStatus) {
			case "Counselled":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et "
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE + "'))\n");
				break;
			case "Booked":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et "
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_BOOKING_ENCOUNTER_TYPE + "'))\n");
				break;
			case "Defaulted":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_BOOKING_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN encounter e1 on e1.patient_id = p.person_id\n"
				        + "            AND e1.voided=0\n"
				        + "            AND e1.encounter_type NOT IN (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid = ('" + BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE + "'))\n");
				
				break;
			case "Circumcised":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_OPERATION_ENCOUNTER_TYPE + "'))\n");
				break;
			case "48Hrs review":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_REVIEW_PERIOD_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_48HRS_REVIEW_CONCEPT_UUID + "') \n");
				break;
			case "7Days Review":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_REVIEW_PERIOD_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_7DAYS_REVIEW_CONCEPT_UUID + "') \n");
				break;
			case "42Days Review":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_POST_OPERATION_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_REVIEW_PERIOD_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_42DAYS_REVIEW_CONCEPT_UUID + "') \n");
				break;
			case "Enrolled":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_COUNSELING_OUTCOME_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.AGREED_CONCEPT_UUID + "') \n");
				break;
			case "Declined":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et"
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_COUNSELING_OUTCOME_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.DISAGREED_CONCEPT_UUID + "') \n");
				
				break;
			case "Deferred":
				encounterString += (" INNER JOIN encounter e on e.patient_id = p.person_id\n"
				        + "            AND e.voided=0\n"
				        + "            AND e.encounter_type = (SELECT encounter_type_id FROM encounter_type et "
				        + " WHERE et.uuid IN ('" + BotswanaEmrConstants.VMMC_COUNSELLING_ENCOUNTER_TYPE + "'))\n");
				encounterString += (" INNER JOIN obs o on o.person_id = p.person_id\n"
				        + "            AND e.encounter_id=o.encounter_id\n" + "            AND o.voided=0\n"
				        + "            AND o.concept_id = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.VMMC_COUNSELING_OUTCOME_CONCEPT_UUID + "')"
				        + "            AND o.valueCoded = (SELECT concept_id FROM concept WHERE uuid ='"
				        + BotswanaEmrConstants.DEFERRED_CONCEPT_UUID + "') \n");
				break;
			default:
				encounterString = "";
		}
		
		return encounterString;
	}
	
	public String appendLastVisitDateToJoinClause(String join) {
		String lastVisitDateQuery = "  LEFT JOIN (SELECT max(date_started) as visit_date,patient_id FROM visit v WHERE v.voided = 0 GROUP BY v.patient_id) lv ON lv.patient_id = pt.patient_id\n";
		if (!StringUtils.isEmpty(this.visitStartDate)) {
			lastVisitDateQuery += " AND lv.visit_date >= '" + this.visitStartDate + "' \n";
		}
		if (!StringUtils.isEmpty(this.visitEndDate)) {
			lastVisitDateQuery += " AND lv.visit_date <= '" + this.visitEndDate + "' \n";
		}
		return join + lastVisitDateQuery;
	}
	
	public String appendSelectVisitDateToSelectClause(String select) {
		String lastVisitStatusSelect = " ,lv.visit_date as lastVisitDate ";
		return select + lastVisitStatusSelect;
	}
}
