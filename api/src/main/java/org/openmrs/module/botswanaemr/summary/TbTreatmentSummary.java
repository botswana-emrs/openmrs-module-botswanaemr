/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary;

import org.openmrs.Patient;
import org.openmrs.module.botswanaemr.model.DrugRegimen;
import org.openmrs.module.botswanaemr.summary.tb.AdverseDrugReaction;
import org.openmrs.module.botswanaemr.summary.tb.ChestXRayResult;
import org.openmrs.module.botswanaemr.summary.tb.CultureDstReport;
import org.openmrs.module.botswanaemr.summary.tb.DiabetesMellitus;
import org.openmrs.module.botswanaemr.summary.tb.SputumLabResult;
import org.openmrs.module.botswanaemr.summary.tb.TBEnrollmentDetails;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author corneliouzbett
 */
public interface TbTreatmentSummary {
	
	List<CultureDstReport> getCultureDstReport(@NotNull Patient patient);
	
	List<DiabetesMellitus> getDiabetesScreening(@NotNull Patient patient);
	
	List<ChestXRayResult> getChestXRayResults(@NotNull Patient patient);
	
	TBEnrollmentDetails getTBEnrollmentDetails(@NotNull Patient patient);
	
	List<SputumLabResult> getSputumExamination(@NotNull Patient patient);
	
	List<AdverseDrugReaction> getAdverseDrugReaction(@NotNull Patient patient);
	
	DrugRegimen getRegimen(Patient patient);
	
}
