/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Slf4j
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdverseDrugReaction extends BaseSummary implements Summary, Serializable {
	
	private static final String ADVERSE_DRUG_REACTION_CONCEPT = "160632AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String OTHER_DRUGS_CONCEPT = "163101AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private static final String SUSPECTED_DRUGS_CONCEPT = "1193AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private Encounter encounter;
	
	private String adverseDrugReaction;
	
	private String otherDrugs;
	
	private String suspectedDrugs;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		AdverseDrugReaction adverseDrugReaction = new AdverseDrugReaction();
		adverseDrugReaction.setEncounter(encounter);
		observations.forEach(obs -> {
			if (obs.getConcept().getUuid().equals(ADVERSE_DRUG_REACTION_CONCEPT)) {
				adverseDrugReaction.setAdverseDrugReaction(getValueText(obs));
			} else if (obs.getConcept().getUuid().equals(OTHER_DRUGS_CONCEPT)) {
				adverseDrugReaction.setOtherDrugs(obs.getValueText());
			} else if (obs.getConcept().getUuid().equals(SUSPECTED_DRUGS_CONCEPT)) {
				adverseDrugReaction.setSuspectedDrugs(getValueCoded(obs));
			} else {
				log.debug("No available adverse drug reaction for this encounter");
			}
		});
		return adverseDrugReaction;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return Arrays.asList(getConcept(ADVERSE_DRUG_REACTION_CONCEPT), getConcept(OTHER_DRUGS_CONCEPT),
		    getConcept(SUSPECTED_DRUGS_CONCEPT));
	}
}
