/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.TbTreatmentService;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.TB_PROGRAM_UUID;

@Component("botswana.emr.tbTreatmentService")
public class TbTreatmentServiceImpl implements TbTreatmentService {
	
	@Override
	public Optional<PatientProgram> getTBEnrollment(@NotNull Patient patient) {
		Program program = Context.getProgramWorkflowService().getProgramByUuid(TB_PROGRAM_UUID);
		List<PatientProgram> patientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, program, null,
		    null, null, null, false);
		//Remove inactive
		patientPrograms.removeIf(patientProgram -> !patientProgram.getActive());
		if (!patientPrograms.isEmpty()) {
			return Optional.of(patientPrograms.get(0));
		}
		return Optional.empty();
	}
}
