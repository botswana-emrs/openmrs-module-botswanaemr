/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.BasicAuthInterceptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.utils.client.EFhirClientException;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonName;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;

import static org.apache.commons.lang3.Validate.notNull;

public class FhirUtil {
	
	private static final Log log = LogFactory.getLog(FhirUtil.class);
	
	// locking object
	private final static Object s_lockObject = new Object();
	
	// Instance
	private static FhirUtil s_instance = null;
	
	/**
	 * Creates a new message utility
	 */
	private FhirUtil() {
	}
	
	/**
	 * Get an instance of the message utility
	 */
	public static FhirUtil getInstance() {
		if (s_instance == null)
			synchronized (s_lockObject) {
				if (s_instance == null)
					s_instance = new FhirUtil();
			}
		return s_instance;
	}
	
	/**
	 * @param fhirPatient The FHIR patient to be parsed
	 * @return The OpenMRS patient
	 * @summary Parse a FHIR patient into an OpenMRS patient
	 */
	// TODO: replace with fhir2 functionality (translator MpiPatient <-> Patient)
	public Patient parseFhirPatient(org.hl7.fhir.r4.model.Patient fhirPatient) {
		Patient patient = new Patient();
		
		notNull(patient, "The existing Openmrs Patient object should not be null");
		notNull(fhirPatient, "The Patient object should not be null");
		
		//		Set UUID
		//		patient.setUuid(fhirPatient.getId());
		
		// Attempt to copy names
		for (HumanName name : fhirPatient.getName()) {
			PersonName pn = this.interpretFhirName(name);
			patient.addName(pn);
		}
		
		// Copy gender
		if (Enumerations.AdministrativeGender.FEMALE.equals(fhirPatient.getGender()))
			patient.setGender("F");
		else if (Enumerations.AdministrativeGender.MALE.equals(fhirPatient.getGender()))
			patient.setGender("M");
		else
			patient.setGender("U");
		
		// Copy DOB
		if (fhirPatient.hasBirthDate()) {
			patient.setBirthdate(fhirPatient.getBirthDate());
			if (fhirPatient.getBirthDateElement().getValueAsString().length() < 10) // Approx
				patient.setBirthdateEstimated(true);
		}
		
		// Death details
		if (fhirPatient.hasDeceased()) {
			try {
				fhirPatient.getDeceasedBooleanType();
				patient.setDead(fhirPatient.getDeceasedBooleanType().booleanValue());
			}
			catch (FHIRException ignored) {}
			try {
				fhirPatient.getDeceasedDateTimeType();
				patient.setDead(true);
				patient.setDeathDate(fhirPatient.getDeceasedDateTimeType().getValue());
			}
			catch (FHIRException ignored) {}
		}
		
		// Marital Status
		if (fhirPatient.hasMaritalStatus()) {
			try {
				String maritalStatusCode = fhirPatient.getMaritalStatus().getCodingFirstRep().getCode();
				// Translate marital status code to OpenMRS marital status
				String maritalStatus;
				switch (maritalStatusCode) {
					case "M":
						maritalStatus = "Married";
						break;
					case "L":
						maritalStatus = "Separated";
						break;
					case "S":
					case "U":
						maritalStatus = "Single";
						break;
					case "D":
						maritalStatus = "Divorced";
						break;
					case "W":
						maritalStatus = "Widowed";
						break;
					default:
						maritalStatus = "Unspecified";
						break;
				}
				patient.addAttribute(new PersonAttribute(Context.getPersonService()
				        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.MARITAL_STATUS_ATTRIBUTE_TYPE), maritalStatus));
			}
			catch (FHIRException ignored) {}
		}
		
		return patient;
	}
	
	/**
	 * Interpret the FHIR HumanName as a Patient Name
	 *
	 * @return The interpreted name
	 */
	private PersonName interpretFhirName(HumanName name) {
		if (name == null) {
			return null;
		}
		PersonName pn = new PersonName();
		pn.setUuid(name.getId());
		
		if (name.getFamily() == null || name.getFamily().isEmpty())
			pn.setFamilyName("(NULL)");
		else
			pn.setFamilyName(name.getFamily());
		
		// Given name
		if (!name.hasGiven())
			pn.setGivenName("(NULL)");
		else
			pn.setGivenName(name.getGivenAsSingleString());
		
		pn.setPrefix(name.getPrefixAsSingleString());
		
		if (HumanName.NameUse.OFFICIAL.equals(name.getUse()))
			pn.setPreferred(true);
		
		return pn;
	}
	
	public IGenericClient getClient() throws EFhirClientException {
		FhirContext ctx = FhirContext.forR4();
		
		IGenericClient client = ctx.newRestfulGenericClient(Context.getAdministrationService()
		        .getGlobalProperty(BotswanaEmrConstants.PROP_MPI_SERVER_URL, "http://hapi.fhir.org/baseR4"));
		
		client.setEncoding(EncodingEnum.JSON);
		ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		
		// Basic Auth
		client.registerInterceptor(new BasicAuthInterceptor(
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_SERVER_USERNAME,
		            "postman"),
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_PASSWORD, "postman")));
		
		return client;
	}
	
	public IGenericClient postPatientResource(org.hl7.fhir.r4.model.Patient fhirPatient) throws EFhirClientException {
		FhirContext ctx = FhirContext.forR4();
		IGenericClient client = ctx.newRestfulGenericClient(Context.getAdministrationService()
		        .getGlobalProperty(BotswanaEmrConstants.CR_SAVE_PATIENT_URL, "http://hapi.fhir.org/baseR4"));
		
		client.setEncoding(EncodingEnum.JSON);
		ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		client.registerInterceptor(new BasicAuthInterceptor(
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_SERVER_USERNAME,
		            "postman"),
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_PASSWORD, "postman")));
		
		return client;
	}
	
	public IGenericClient getClientRegistryClient() throws EFhirClientException {
		FhirContext ctx = FhirContext.forR4();
		
		IGenericClient client = ctx.newRestfulGenericClient(Context.getAdministrationService()
		        .getGlobalProperty(BotswanaEmrConstants.CR_FETCH_URL, "http://hapi.fhir.org/baseR4"));
		
		client.setEncoding(EncodingEnum.JSON);
		ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		
		// Basic Auth
		client.registerInterceptor(new BasicAuthInterceptor(
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_SERVER_USERNAME,
		            "postman"),
		        Context.getAdministrationService().getGlobalProperty(BotswanaEmrConstants.PROP_MPI_PASSWORD, "postman")));
		return client;
	}
	
	// Lab Client
	public IGenericClient getLabClient() throws EFhirClientException {
		FhirContext ctx = FhirContext.forR4();
		IGenericClient client = ctx.newRestfulGenericClient(
		    Context.getAdministrationService().getGlobalProperty("botswanaemr.shrBaseUrl", "http://hapi.fhir.org/baseR4"));
		client.setEncoding(EncodingEnum.JSON);
		ctx.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);
		client.registerInterceptor(new BasicAuthInterceptor(
		        Context.getAdministrationService().getGlobalProperty("botswanaemr.shrUserName", "bemr-client"),
		        Context.getAdministrationService().getGlobalProperty("botswanaemr.shrPassword", "bemr-client")));
		return client;
	}
	
}
