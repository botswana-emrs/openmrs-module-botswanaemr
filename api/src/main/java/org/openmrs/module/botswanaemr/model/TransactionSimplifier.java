/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.Data;

import java.util.Date;

@Data
public class TransactionSimplifier {
	
	private Integer transactionReference;
	
	private Date transactionDate;
	
	private String facilityName;
	
	private Integer quantityReceived;
	
	private Integer quantityIssued;
	
	private Date expiryDate;
	
	private String batchNumber;
	
	private Integer adjustment;
	
	private Integer losses;
	
	private Integer quantityOnHand;
	
	private String remarks;
	
	private String initials;
	
	private Integer stockRoomId;
	
	private String stockRoomName;
	
	private String unitOfIssue;
	
	private String commodityCode;
	
	private String commodityName;
	
	private Double minimumStockLevel;
	
	private Double maximumStockLevel;
	
	private Double emergencyOrderPoint;
}
