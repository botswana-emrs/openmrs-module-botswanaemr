/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.APIException;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.PatientService;
import org.openmrs.api.context.Context;
import org.openmrs.api.db.DAOException;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.botswanaemr.api.dao.BotswanaEmrDao;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.CaseLinkage;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.model.DiscussionParticipant;
import org.openmrs.module.botswanaemr.model.DrugOrderExtension;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Nullable;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Implementations of business logic methods for botswanaEMR
 */
public class BotswanaEmrServiceImpl extends BaseOpenmrsService implements BotswanaEmrService {
	
	protected static final Log log = LogFactory.getLog(BotswanaEmrServiceImpl.class);
	
	private Session session = null;
	
	public BotswanaEmrDao getDao() {
		return dao;
	}
	
	public void setDao(BotswanaEmrDao dao) {
		this.dao = dao;
	}
	
	private BotswanaEmrDao dao;
	
	@Override
	public List<Registration> getPatientsRegisteredOnDate(Date startDate, Date endDate, Location location, Integer limit)
	        throws APIException {
		return dao.getPatientsCountRegisteredOnDate(startDate, endDate, location, limit);
	}
	
	@Override
	public Long getPatientsRegisteredCountOnDate(Date startDate, Date endDate, String location, Integer limit)
	        throws APIException {
		return dao.getPatientsRegisteredCountOnDate(startDate, endDate, location, limit);
	}
	
	public List<Registration> getPatientsRegisteredOnDate(Date startDate, Date endDate, Integer limit) throws APIException {
		return dao.getPatientsCountRegisteredOnDate(startDate, endDate, limit);
	}
	
	@Override
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit,
	        Location location) throws DAOException {
		return dao.getPatientsCountRegisteredOnDate(startDate, endDate, limit, location);
	}
	
	@Override
	public int getPatientsRegisteredOnDateCountOnly(Date startDate, Date endDate, Integer limit, Location location)
	        throws DAOException {
		return dao.getPatientsRegisteredOnDateCountOnly(startDate, endDate, limit, location);
	}
	
	@Override
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour, Location location) throws APIException {
		return dao.getPatientsSeenOnDatePerHour(date, hour, location);
	}
	
	@Override
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour) throws APIException {
		return dao.getPatientsSeenOnDatePerHour(date, hour);
	}
	
	@Override
	public List<Order> getPrescriptionsFilledOnDatePerHour(Date date, String hour) throws APIException {
		return dao.getPrescriptionsFilledOnDatePerHour(date, hour);
	}
	
	@Override
	public List<Encounter> getTbScreeningEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException {
		EncounterType tbEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TB_ENCOUNTER_TYPE_UUID);
		return dao.getEncountersOnDate(startDate, endDate, tbEncounterType, limit, location);
	}
	
	@Override
	public List<Encounter> getTbScreeningEncountersPerHourList(Date date, String hour, Location location)
	        throws APIException {
		EncounterType tbEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TB_ENCOUNTER_TYPE_UUID);
		return dao.getEncountersOnDatePerHour(date, hour, tbEncounterType, location);
	}
	
	@Override
	public List<Encounter> getTriageScreeningEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException {
		EncounterType triageScreeningEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		return dao.getEncountersOnDate(startDate, endDate, triageScreeningEncounterType, limit, location);
	}
	
	@Override
	public List<Encounter> getArtEncountersList(Date startDate, Date endDate, Integer limit, Location location)
	        throws APIException {
		EncounterType triageScreeningEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.ART_INITIAL_ENCOUNTER_TYPE_UUID);
		return dao.getEncountersOnDate(startDate, endDate, triageScreeningEncounterType, limit, location);
	}
	
	@Override
	public List<Encounter> getTriageScreeningEncountersPerHourList(Date date, String hour, Location location)
	        throws APIException {
		EncounterType triageScreeningEncounterType = Context.getEncounterService()
		        .getEncounterTypeByUuid(BotswanaEmrConstants.TRIAGE_SCREENING_ENCOUNTER_TYPE_UUID);
		return dao.getEncountersOnDatePerHour(date, hour, triageScreeningEncounterType, location);
	}
	
	@Override
	public List<Encounter> getScreeningsDoneOnDate(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) {
		return dao.getEncountersOnDate(startDate, endDate, encounterType, limit, location);
	}
	
	@Override
	public List<Encounter> getScreeningsDoneOnDatePerHour(Date date, String hour, EncounterType encounterType,
	        Location location) throws APIException {
		return dao.getEncountersOnDatePerHour(date, hour, encounterType, location);
	}
	
	@Override
	public List<Encounter> getEncountersList(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) throws APIException {
		return dao.getEncountersOnDate(startDate, endDate, encounterType, limit, location);
	}
	
	@Override
	public List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterTypes, Integer limit,
	        Location location) throws APIException {
		return dao.getEncountersOnDate(startDate, endDate, encounterTypes, limit, location);
	}
	
	@Override
	public List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterTypes, Integer limit,
	        Location location, Patient patient) throws APIException {
		return getEncountersList(startDate, endDate, encounterTypes, limit, location, patient, null);
	}
	
	@Override
	public List<Encounter> getEncountersList(Date startDate, Date endDate, List<EncounterType> encounterTypes, Integer limit,
	        Location location, Patient patient, PagingInfo pagingInfo) throws APIException {
		return dao.getEncountersOnDateAndPatient(startDate, endDate, encounterTypes, limit, location, patient, pagingInfo);
	}
	
	@Override
	public PaymentMethod getPaymentMethod(Integer paymentMethodId) throws APIException {
		return dao.getPaymentMethod(paymentMethodId);
	}
	
	@Override
	public PaymentMethod getPaymentMethod(String name) throws APIException {
		return dao.getPaymentMethod(name);
	}
	
	@Override
	public List<PaymentMethod> getAllPaymentMethods() throws APIException {
		return dao.getAllPaymentMethods();
	}
	
	@Override
	public List<PaymentMethod> getAllPaymentMethods(boolean includeRetired) throws APIException {
		return dao.getAllPaymentMethods(includeRetired);
	}
	
	@Override
	public PaymentMethod savePaymentMethod(PaymentMethod paymentMethod) throws APIException {
		return dao.savePaymentMethod(paymentMethod);
	}
	
	@Override
	public ServiceProvider getServiceProvider(Integer serviceProviderId) throws APIException {
		return dao.getServiceProvider(serviceProviderId);
	}
	
	@Override
	public ServiceProvider getServiceProvider(String name) throws APIException {
		return dao.getServiceProvider(name);
	}
	
	@Override
	public List<ServiceProvider> getAllServiceProvider() throws APIException {
		return dao.getAllServiceProvider();
	}
	
	@Override
	public List<ServiceProvider> getAllServiceProvider(boolean includeRetired) throws APIException {
		return dao.getAllServiceProvider(includeRetired);
	}
	
	@Override
	public ServiceProvider saveServiceProvider(ServiceProvider serviceProvider) throws APIException {
		return dao.saveServiceProvider(serviceProvider);
	}
	
	@Override
	public Registration savePayment(Registration payment) throws APIException {
		return dao.savePayment(payment);
	}
	
	@Override
	public Registration getPayment(Integer registrationId) throws APIException {
		return dao.getPayment(registrationId);
	}
	
	@Override
	public List<Registration> getPaymentByPatient(Patient patient) {
		return dao.getPaymentByPatient(patient);
	}
	
	@Override
	public List<Registration> getPayment(Patient who, Location loc, Date fromDate, Date toDate, boolean includeVoided) {
		return dao.getPayment(who, loc, fromDate, toDate, includeVoided);
	}
	
	@Override
	public Registration voidPayment(Registration payment, String reason) {
		return dao.voidPayment(payment, reason);
	}
	
	@Override
	public List<Concept> searchConcept(String searchTerm, String conceptClass) throws APIException {
		ConceptClass conceptClassObject = Context.getConceptService().getConceptClassByUuid(conceptClass);
		if (conceptClassObject == null) {
			conceptClassObject = Context.getConceptService().getConceptClassByName(conceptClass);
		}
		return dao.searchConcept(searchTerm, conceptClassObject);
	}
	
	@Override
	public List<PatientProgram> getPatientProgramByProgram(Program program) {
		return dao.getPatientProgramByProgram(program);
	}
	
	@Override
	public List<PatientProgram> getPatientProgramByProgramAndDate(Program program, Date startDate, @Nullable Date endDate,
	        boolean activeOnly) {
		return dao.getPatientProgramByProgramAndDate(program, startDate, endDate, activeOnly);
	}
	
	@Override
	public List<PatientProgram> getActivePatientProgramsByProgram(Program program, boolean includeVoided) {
		return dao.getActivePatientProgramsByProgram(program, includeVoided);
	}
	
	@Override
	public List<Person> getPatientByMigrationMetadata(String migrationFacilityId, String migrationPatientId) {
		return dao.getPatientByMigrationMetadata(migrationFacilityId, migrationPatientId);
	}
	
	@Override
	public List<PatientResponse> getPatientsBySearchParams(String nameOrUniqueId, String locationUuid, String programId,
	        String gender, String status, String startDateRegistered, String endDateRegistered, String condition) {
		return dao.getPatientsBySearchParams(nameOrUniqueId, locationUuid, programId, gender, status, startDateRegistered,
		    endDateRegistered, condition);
	}
	
	@Override
	public List<PatientResponse> getPatientsByHtsSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String testingLocation) {
		return dao.getPatientsByHtsSearchParams(nameOrUniqueId, locationUuid, gender, status, startDateRegistered,
		    endDateRegistered, testingLocation);
	}
	
	@Override
	public List<PatientResponse> getPatientsByVmmcSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String visitStatus) {
		return dao.getPatientsByVmmcSearchParams(nameOrUniqueId, locationUuid, gender, status, startDateRegistered,
		    endDateRegistered, visitStatus);
	}
	
	@Override
	public CaseLinkage saveCaseLinkage(CaseLinkage caseLinkage) throws APIException {
		return dao.saveCaseLinkage(caseLinkage);
	}
	
	@Override
	public CaseLinkage getCaseLinkageById(Integer caseLinkageId) throws APIException {
		return dao.getCaseLinkageById(caseLinkageId);
	}
	
	@Override
	public Cases saveCase(Cases cases) throws APIException {
		return dao.saveCase(cases);
	}
	
	@Override
	public Cases getCase(Integer caseId) throws APIException {
		return dao.getCase(caseId);
	}
	
	@Override
	public List<Cases> getAllCase(Patient patient, boolean includeRetired) throws APIException {
		return dao.getAllCase(patient, includeRetired);
	}
	
	@Override
	public CaseEncounter saveCaseEncounter(CaseEncounter caseEncounter) throws APIException {
		return dao.saveCaseEncounter(caseEncounter);
	}
	
	@Override
	public CaseEncounter getCaseEncounter(Integer caseEncounterId) throws APIException {
		return dao.getCaseEncounter(caseEncounterId);
	}
	
	@Override
	public List<CaseEncounter> getAllCaseEncounter(Patient patient, boolean includeVoided) throws APIException {
		return dao.getAllCaseEncounter(patient, includeVoided);
	}
	
	@Override
	public CaseEncounter getCaseByEncounterId(Encounter encounter) {
		return dao.getCaseByEncounterId(encounter);
	}
	
	@Override
	public DiscussionParticipant addDiscussionParticipant(DiscussionParticipant discussionParticipant) throws APIException {
		return dao.addDiscussionParticipant(discussionParticipant);
	}
	
	@Override
	public void deleteDiscussionParticipant(DiscussionParticipant discussionParticipant) throws APIException {
		dao.deleteDiscussionParticipant(discussionParticipant);
	}
	
	@Override
	public List<DiscussionParticipant> getDiscussionParticipantsByDiscussion(Discussion discussion) {
		return dao.getDiscussionParticipantsByDiscussion(discussion);
	}
	
	@Override
	public Discussion saveDiscussion(Discussion discussion) throws APIException {
		return dao.saveDiscussion(discussion);
	}
	
	@Override
	public Discussion getDiscussionByCase(Cases caze) throws APIException {
		return dao.getDiscussionByCase(caze);
	}
	
	@Override
	public Discussion getDiscussionById(Integer discussionId) throws APIException {
		return dao.getDiscussionById(discussionId);
	}
	
	@Override
	public Message saveMessage(Message message) throws APIException {
		return dao.saveMessage(message);
	}
	
	@Override
	public List<Message> getMessagesByDiscussion(Discussion discussion) {
		return dao.getMessagesByDiscussion(discussion);
	}
	
	@Override
	public Collection<Patient> findPersonInContactWithTBPatients(Patient patient) throws APIException {
		return this.dao.findPatientsInContactWithTB(patient);
	}
	
	@Override
	public DrugOrder getDrugOrderById(Integer drugOrderId) throws APIException {
		return dao.getDrugOrderById(drugOrderId);
	}
	
	@Override
	public DrugOrderExtension saveDrugOrderExtension(DrugOrderExtension drugOrderExtension) throws APIException {
		return dao.saveDrugOrderExtension(drugOrderExtension);
	}
	
	@Override
	public List<DrugOrderExtension> getDrugOrderExtensionsByPatient(Patient patient) throws APIException {
		return dao.getDrugOrderExtensionsByPatientId(patient);
	}
	
	/**
	 * @see PatientService#getPatients(String, boolean, Integer, Integer)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Patient> getBotswanaEmrPatients(String query, boolean includeVoided, Integer start, Integer length)
	        throws APIException {
		List<Patient> patients = new ArrayList<>();
		if (StringUtils.isBlank(query)) {
			return patients;
		}
		
		return dao.getBotswanaEmrPatients(query, includeVoided, start, length);
	}
	
	@Override
	public List<PatientQueue> getIncompletePatientQueues() {
		return dao.getIncompletePatientQueues();
	}
	
	@Override
	public List<BotswanaEmrEmailReportConfig> getBotswanaEmrEmailReportConfigs(String reportId, Location facility) {
		return dao.getBotswanaEmrEmailReportConfigs(reportId, facility);
	}
	
	@Override
	public void sendGeneralEmail(String toEmail, String subject, String body) {
		try {
			MimeMessage msg = msg();
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(toEmail, false));
			log.info("Message is ready");
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(body, "text/HTML;");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport.send(msg);
			log.info("EMail Sent Successfully!!");
		}
		catch (Exception e) {
			log.debug(e.getMessage());
			
		}
	}
	
	@Override
	public void sendAttachmentEmail(BotswanaEmrEmailReportConfig config, String filename) {
		try {
			MimeMessage msg = msg();
			msg.setSubject(config.getSubject(), "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(config.getMailTo(), false));
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(config.getMailContent());
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filename);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport.send(msg);
			log.info("EMail Sent Successfully with attachment!!");
		}
		catch (MessagingException e) {
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private MimeMessage msg() throws MessagingException, UnsupportedEncodingException {
		MimeMessage msg = new MimeMessage(getMailSession());
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("format", "flowed");
		msg.addHeader("Content-Transfer-Encoding", "8bit");
		msg.setFrom(new InternetAddress("no_reply@example.com", "Botswana EMR"));
		msg.setReplyTo(InternetAddress.parse("no_reply@botswanaemr.gov", true));
		
		return msg;
	}
	
	@Override
	public Session getMailSession() {
		if (session == null) {
			
			AdministrationService as = Context.getAdministrationService();
			Properties p = new Properties();
			p.put("mail.transport.protocol", as.getGlobalProperty("mail.transport_protocol", "smtp"));
			p.put("mail.smtp.host", as.getGlobalProperty("mail.smtp_host", "smtp.gmail.com"));
			p.put("mail.smtp.port", as.getGlobalProperty("mail.smtp_port", "587")); // mail.smtp_port
			p.put("mail.smtp.auth", as.getGlobalProperty("mail.smtp_auth", "true")); // mail.smtp_auth
			p.put("mail.smtp.starttls.enable", as.getGlobalProperty("mail.smtp.starttls.enable", "true"));
			p.put("mail.debug", as.getGlobalProperty("mail.debug", "false"));
			p.put("mail.from", as.getGlobalProperty("mail.from", ""));
			
			final String user = as.getGlobalProperty("mail.user", "");
			final String password = as.getGlobalProperty("mail.password", "");
			
			if (StringUtils.isNotBlank(user) && StringUtils.isNotBlank(password)) {
				session = Session.getInstance(p, new Authenticator() {
					
					public PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(user, password);
					}
				});
			} else {
				session = Session.getInstance(p);
			}
		}
		return session;
	}
	
	@Override
	public String getFreemarkerTemplateContent(String template, Map<String, Object> model)
	        throws IOException, TemplateException {
		Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
		configuration.setClassForTemplateLoading(this.getClass(), "/mailtemplates");
		StringWriter stringWriter = new StringWriter();
		configuration.getTemplate(template).process(model, stringWriter);
		return stringWriter.getBuffer().toString();
	}
	
	@Override
	public BotswanaEmrEmailReportConfig saveBotswanaEmrEmailReportConfig(
	        BotswanaEmrEmailReportConfig botswanaEmrEmailReportConfig) throws APIException {
		return dao.saveBotswanaEmrEmailReportConfig(botswanaEmrEmailReportConfig);
	}
	
	@Override
	public List<Encounter> getEncounterLimits(EncounterSearchCriteria searchCriteria, Integer fetchLimit)
	        throws APIException {
		return dao.getEncounterLimits(searchCriteria, fetchLimit);
	}
	
	public Registration getPatientsRegisteredPositionOnDate(Date startDate, Date endDate, String location, String position)
	        throws APIException {
		return dao.getPatientsRegisteredPositionOnDate(startDate, endDate, location, position);
	}
	
	public List<Obs> getObservation(Person person, Visit visit, Concept question, Location location, Integer mostRecentN)
	        throws APIException {
		return dao.getObservation(person, visit, question, location, mostRecentN);
	}
	
	public List<Obs> getObservation(Person person, EncounterType encounterType, Visit visit, Concept question,
	        Location location, Integer mostRecentN) throws APIException {
		return dao.getObservation(person, encounterType, visit, question, location, mostRecentN);
	}
	
	public CaseEncounter getLastCaseEncounter(Patient patient) throws APIException {
		return dao.getLastCaseEncounter(patient);
	}
	
	@Override
	public List<CodedOrFreeText> getAllConditionEverSaved() throws APIException {
		return dao.getAllConditionEverSaved();
	}
	
	@Override
	public Registration getLatestPaymentEntry(Patient patient, Location location) throws APIException {
		return dao.getLatestPaymentEntry(patient, location);
	}
}
