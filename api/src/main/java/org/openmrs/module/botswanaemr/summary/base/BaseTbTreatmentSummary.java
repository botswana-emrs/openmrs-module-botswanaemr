/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.base;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.TbTreatmentService;
import org.openmrs.module.botswanaemr.exceptions.ProgramEnrollmentException;
import org.openmrs.module.botswanaemr.utilities.DateUtils;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class BaseTbTreatmentSummary {
	
	protected TbTreatmentService getTbService() {
		return Context.getRegisteredComponent("botswana.emr.tbTreatmentService", TbTreatmentService.class);
	}
	
	protected List<Obs> getObservations(@NotNull Patient patient, @NotNull List<Concept> questions) {
		PatientProgram patientProgram = getTbService().getTBEnrollment(patient).orElse(null);
		Date enrollmentDate = null;
		if (patientProgram == null) {
			log.debug("Patient with the uuid " + patient.getUuid() + " is not enrolled to Tb program");
			//			throw new ProgramEnrollmentException(
			//					"Patient with the uuid " + patient.getUuid() + " is not enrolled to Tb program");
		} else {
			enrollmentDate = patientProgram.getDateEnrolled();
		}
		assert enrollmentDate != null;
		//DateUtils.getMonth(enrollmentDate);
		
		return Context.getObsService().getObservations(Collections.singletonList(patient), null, questions, null, null, null,
		    null, null, null, patientProgram.getDateEnrolled(), patientProgram.getDateCompleted(), false);
	}
	
	protected Map<Encounter, List<Obs>> groupByEncounter(List<Obs> observations) {
		return observations.stream().collect(Collectors.groupingBy(Obs::getEncounter));
	}
	
	protected List<Obs> getObservations(@NotNull Patient patient, @NotNull Encounter encounter) {
		return Context.getObsService().getObservations(Collections.singletonList(patient),
		    Collections.singletonList(encounter), null, null, null, null, null, null, null, null, null, false);
	}
	
	protected List<Obs> getObservationsByEncounterList(@NotNull Patient patient, @NotNull List<Encounter> encounter) {
		return Context.getObsService().getObservations(Collections.singletonList(patient), encounter, null, null, null, null,
		    null, null, null, null, null, false);
	}
	
}
