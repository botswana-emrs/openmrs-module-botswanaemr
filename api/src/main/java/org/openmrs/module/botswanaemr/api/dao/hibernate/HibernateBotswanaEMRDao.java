/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.openmrs.CodedOrFreeText;
import org.openmrs.Concept;
import org.openmrs.ConceptClass;
import org.openmrs.Condition;
import org.openmrs.DrugOrder;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientIdentifier;
import org.openmrs.PatientProgram;
import org.openmrs.Person;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.PersonName;
import org.openmrs.Program;
import org.openmrs.Visit;
import org.openmrs.api.context.Context;
import org.openmrs.api.db.DAOException;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.api.db.hibernate.HibernatePersonDAO;
import org.openmrs.api.db.hibernate.PatientSearchCriteria;
import org.openmrs.api.db.hibernate.PersonLuceneQuery;
import org.openmrs.api.db.hibernate.search.LuceneQuery;
import org.openmrs.collection.ListPart;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.Registration;
import org.openmrs.module.botswanaemr.PaymentMethod;
import org.openmrs.module.botswanaemr.ServiceProvider;
import org.openmrs.module.botswanaemr.api.dao.BotswanaEmrDao;
import org.openmrs.module.botswanaemr.contract.PatientResponse;
import org.openmrs.module.botswanaemr.contract.search.PatientSearchBuilder;
import org.openmrs.module.botswanaemr.contract.search.RegistrationsSearchBuilder;
import org.openmrs.module.botswanaemr.model.BotswanaEmrEmailReportConfig;
import org.openmrs.module.botswanaemr.model.CaseEncounter;
import org.openmrs.module.botswanaemr.model.CaseLinkage;
import org.openmrs.module.botswanaemr.model.Cases;
import org.openmrs.module.botswanaemr.model.Discussion;
import org.openmrs.module.botswanaemr.model.DiscussionParticipant;
import org.openmrs.module.botswanaemr.model.DrugOrderExtension;
import org.openmrs.module.botswanaemr.model.Message;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.module.botswanaemrInventory.PagingInfo;
import org.openmrs.module.patientqueueing.model.PatientQueue;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.openmrs.util.OpenmrsConstants;

import javax.annotation.Nullable;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static org.hibernate.criterion.Restrictions.and;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.isNotNull;
import static org.hibernate.criterion.Restrictions.or;

@Slf4j
@SuppressWarnings("unused")
public class HibernateBotswanaEMRDao implements BotswanaEmrDao {
	
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	
	private final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public DbSessionFactory getDbSessionFactory() {
		return dbSessionFactory;
	}
	
	public void setDbSessionFactory(DbSessionFactory dbSessionFactory) {
		this.dbSessionFactory = dbSessionFactory;
	}
	
	private DbSessionFactory dbSessionFactory;
	
	@Override
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Location location,
	        Integer limit) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class, "pay");
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null && endDate != null) {
			startFromDate = fromDate(startDate);
			endFromDate = toDate(endDate);
			
			this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "dateCreated");
		}
		if (startDate != null) {
			criteria.add(Restrictions.ge("dateCreated", DateUtils.getDateStartOfDay(startDate)));
		}
		if (endDate != null) {
			criteria.add(Restrictions.le("dateCreated", DateUtils.getDateEndOfDay(endDate)));
		}
		criteria.add(eq("voided", false));
		// criteria.createAlias("pay.patient", "patient");
		// criteria.createAlias("patient.attributes", "attributes");
		// criteria.createAlias("attributes.attributeType", "attributesTypes");
		if (location != null) {
			// criteria.add(eq("attributesTypes.name", "LocationAttribute"));
			// criteria.add(eq("attributes.value", location.getUuid()));
			criteria.add(eq("pay.location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		if (limit != null) {
			criteria.setFirstResult(0);
			criteria.setMaxResults(limit);
		}
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit)
	        throws DAOException {
		return getPatientsCountRegisteredOnDate(startDate, endDate, null, limit);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Registration> getPatientsCountRegisteredOnDate(Date startDate, Date endDate, Integer limit,
	        Location location) throws DAOException {
		String startFromDate = startDate != null ? fromDate(startDate) : "";
		String endFromDate = endDate != null ? toDate(endDate) : "";
		NativeQuery nativeQuery;
		if (location == null) {
			nativeQuery = new RegistrationsSearchBuilder(dbSessionFactory.getHibernateSessionFactory(), false)
			        // .withLocationAttribute()
			        .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, null, startFromDate, endFromDate, null);
		} else {
			nativeQuery = new RegistrationsSearchBuilder(dbSessionFactory.getHibernateSessionFactory(), false)
			        // .withLocationAttribute()
			        .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, location.getUuid(), startFromDate, endFromDate,
			            location.getLocationId());
		}
		
		return nativeQuery.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public int getPatientsRegisteredOnDateCountOnly(Date startDate, Date endDate, Integer limit, Location location)
	        throws DAOException {
		String startFromDate = startDate != null ? fromDate(startDate) : "";
		String endFromDate = endDate != null ? toDate(endDate) : "";
		NativeQuery nativeQuery = location != null
		        ? new RegistrationsSearchBuilder(dbSessionFactory.getHibernateSessionFactory(), true)
		                // .withLocationAttribute()
		                .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, location.getUuid(), startFromDate,
		                    endFromDate, location.getLocationId())
		        : new RegistrationsSearchBuilder(dbSessionFactory.getHibernateSessionFactory(), true)
		                .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, "", startFromDate, endFromDate, null);
		return ((BigInteger) nativeQuery.getSingleResult()).intValue();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour) throws DAOException {
		return getPatientsSeenOnDatePerHour(date, hour, null);
	}
	
	@Override
	public List<Registration> getPatientsSeenOnDatePerHour(Date date, String hour, Location location) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class);
		String startDateStr = dateFormatter.format(date);
		String startFromDateTime = startDateStr + " " + hour + ":00:00";
		String endToDateTime = startDateStr + " " + hour + ":59:59";
		this.handleStartAndEndEncounterDatetime(criteria, startFromDateTime, endToDateTime, "dateCreated");
		if (location != null) {
			criteria.add(eq("location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return CollectionUtils.isNotEmpty(criteria.list()) ? criteria.list() : null;
	}
	
	@Override
	public PaymentMethod getPaymentMethod(Integer paymentMethodId) throws DAOException {
		return (PaymentMethod) dbSessionFactory.getCurrentSession().get(PaymentMethod.class, paymentMethodId);
	}
	
	@Override
	public PaymentMethod getPaymentMethod(String name) throws DAOException {
		return (PaymentMethod) dbSessionFactory.getCurrentSession().get(PaymentMethod.class, name);
	}
	
	@Override
	public List<PaymentMethod> getAllPaymentMethods() throws DAOException {
		return null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<PaymentMethod> getAllPaymentMethods(boolean includeRetired) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(PaymentMethod.class);
		
		criteria.addOrder(Order.asc("name"));
		
		if (!includeRetired) {
			criteria.add(eq("retired", false));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	public ServiceProvider getServiceProvider(Integer serviceProviderId) throws DAOException {
		return (ServiceProvider) dbSessionFactory.getCurrentSession().get(ServiceProvider.class, serviceProviderId);
	}
	
	@Override
	public ServiceProvider getServiceProvider(String name) throws DAOException {
		return (ServiceProvider) dbSessionFactory.getCurrentSession().get(ServiceProvider.class, name);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ServiceProvider> getAllServiceProvider() throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(ServiceProvider.class);
		
		criteria.addOrder(Order.asc("name"));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ServiceProvider> getAllServiceProvider(boolean includeRetired) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(ServiceProvider.class);
		
		criteria.addOrder(Order.asc("name"));
		
		if (!includeRetired) {
			criteria.add(eq("retired", false));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	public Registration savePayment(Registration registration) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(registration);
		return registration;
	}
	
	@Override
	public Registration getPayment(Integer registrationId) throws DAOException {
		return (Registration) dbSessionFactory.getCurrentSession().get(Registration.class, registrationId);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Registration> getPaymentByPatient(Patient patient) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class);
		criteria.add(eq("patient", patient));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Registration> getPayment(Patient who, Location loc, Date fromDate, Date toDate, boolean includeVoided)
	        throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class);
		
		criteria.add(eq("voided", includeVoided));
		
		if (who != null) {
			criteria.add(eq("patient", who));
		}
		if (loc != null) {
			criteria.add(eq("location", loc));
		}
		if (fromDate != null && toDate != null) {
			criteria.add(Restrictions.ge("registrationDatetime", fromDate));
		}
		if (toDate != null) {
			criteria.add(Restrictions.le("registrationDatetime", fromDate));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	public Registration voidPayment(Registration registration, String reason) throws DAOException {
		Registration paymentFound = (Registration) dbSessionFactory.getCurrentSession().get(ServiceProvider.class,
		    registration);
		paymentFound.setVoided(true);
		dbSessionFactory.getCurrentSession().saveOrUpdate(registration);
		return registration;
	}
	
	@Override
	public PaymentMethod savePaymentMethod(PaymentMethod paymentMethod) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(paymentMethod);
		return paymentMethod;
	}
	
	@Override
	public ServiceProvider saveServiceProvider(ServiceProvider serviceProvider) throws DAOException {
		dbSessionFactory.getCurrentSession().saveOrUpdate(serviceProvider);
		return serviceProvider;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Encounter> getEncountersOnDate(Date startDate, Date endDate, EncounterType encounterType, Integer limit,
	        Location location) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Encounter.class);
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null && endDate != null) {
			startFromDate = fromDate(startDate);
			endFromDate = toDate(endDate);
		}
		criteria.add(eq("voided", false));
		criteria.add(eq("encounterType", encounterType));
		this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "encounterDatetime");
		
		criteria.addOrder(Order.desc("dateCreated"));
		if (limit != null) {
			criteria.setFirstResult(0);
			criteria.setMaxResults(limit);
		}
		
		if (location != null) {
			criteria.add(eq("location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	public List<Encounter> getEncountersOnDate(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Encounter.class);
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null && endDate != null) {
			startFromDate = fromDate(startDate);
			endFromDate = toDate(endDate);
		}
		criteria.add(eq("voided", false));
		for (EncounterType encounterType : encounterTypes) {
			criteria.add(Restrictions.in("encounterType", encounterTypes));
		}
		
		this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "encounterDatetime");
		
		criteria.addOrder(Order.desc("dateCreated"));
		if (limit != null) {
			criteria.setFirstResult(0);
			criteria.setMaxResults(limit);
		}
		
		if (location != null) {
			criteria.add(eq("location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	public List<Encounter> getEncountersOnDateAndPatient(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location, Patient patient) throws DAOException {
		return getEncountersOnDateAndPatient(startDate, endDate, encounterTypes, limit, location, patient, null);
	}
	
	@Override
	public List<Encounter> getEncountersOnDateAndPatient(Date startDate, Date endDate, List<EncounterType> encounterTypes,
	        Integer limit, Location location, Patient patient, PagingInfo pagingInfo) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Encounter.class);
		
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null) {
			startFromDate = fromDate(startDate);
		}
		if (endDate != null) {
			endFromDate = toDate(endDate);
		}
		criteria.add(eq("voided", false));
		for (EncounterType encounterType : encounterTypes) {
			criteria.add(Restrictions.in("encounterType", encounterTypes));
		}
		
		this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "encounterDatetime");
		
		criteria.addOrder(Order.desc("dateCreated"));
		
		if (pagingInfo != null && pagingInfo.getPage() > 0 && pagingInfo.getPageSize() > 0) {
			criteria.setFirstResult((pagingInfo.getPage() - 1) * pagingInfo.getPageSize());
			criteria.setMaxResults(pagingInfo.getPageSize());
			criteria.setFetchSize(pagingInfo.getPageSize());
		} else {
			if (limit != null) {
				criteria.setFirstResult(0);
				criteria.setMaxResults(limit);
			}
		}
		
		if (patient != null) {
			criteria.add(eq("patient", patient));
		}
		
		if (location != null) {
			criteria.add(eq("location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Encounter> getEncountersOnDatePerHour(Date date, String hour, EncounterType encounterType, Location location)
	        throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Encounter.class);
		
		String startDateStr = dateFormatter.format(date);
		String startFromDateTime = startDateStr + " " + hour + ":00:00";
		String endToDateTime = startDateStr + " " + hour + ":59:59";
		
		criteria.add(Restrictions.and(eq("encounterType", encounterType)));
		criteria.add(Restrictions.and(isNotNull("form")));
		this.handleStartAndEndEncounterDatetime(criteria, startFromDateTime, endToDateTime, "encounterDatetime");
		
		if (location != null) {
			criteria.add(eq("location", location));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	private String fromDate(Date startDate) {
		return dateFormatter.format(startDate) + " 00:00:00";
	}
	
	private String toDate(Date endDate) {
		return dateFormatter.format(endDate) + " 23:59:59";
	}
	
	private void handleStartAndEndEncounterDatetime(Criteria criteria, String fromDatetime, String toDatetime,
	        String property) {
		try {
			if (StringUtils.isNotBlank(fromDatetime)) {
				criteria.add(Restrictions.ge(property, dateTimeFormatter.parse(fromDatetime)));
			}
			
			if (StringUtils.isNotBlank(toDatetime)) {
				criteria.add(Restrictions.le(property, dateTimeFormatter.parse(toDatetime)));
			}
		}
		catch (Exception e) {
			if (e instanceof ParseException) {
				log.error("Error parsing date: {}", e.getMessage());
			}
			log.debug("Error converting date {}", e.getMessage(), e);
		}
	}
	
	@Override
	public CaseLinkage saveCaseLinkage(CaseLinkage caseLinkage) throws DAOException {
		return (CaseLinkage) dbSessionFactory.getCurrentSession().merge(caseLinkage);
	}
	
	@Override
	public CaseLinkage getCaseLinkageById(Integer caseLinkageId) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(CaseLinkage.class);
		criteria.add(eq("caseLinkageId", caseLinkageId));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return (CaseLinkage) criteria.uniqueResult();
	}
	
	@Override
	public List<Concept> searchConcept(String searchTerm, ConceptClass clazz) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Concept.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(eq("retired", false));
		if (clazz != null) {
			criteria.add(eq("conceptClass", clazz));
		}
		if (StringUtils.isNotBlank(searchTerm)) {
			criteria.createAlias("names", "names");
			criteria.add(Restrictions.like("names.name", searchTerm, MatchMode.ANYWHERE));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	public Cases saveCase(Cases cases) throws DAOException {
		return (Cases) dbSessionFactory.getCurrentSession().merge(cases);
	}
	
	@Override
	public Cases getCase(Integer caseId) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Cases.class);
		criteria.add(eq("caseId", caseId));
		return (Cases) criteria.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Cases> getAllCase(Patient patient, boolean includeRetired) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Cases.class);
		criteria.add(eq("retired", includeRetired));
		if (patient != null) {
			criteria.add(eq("patient", patient));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	public CaseEncounter saveCaseEncounter(CaseEncounter caseEncounter) throws DAOException {
		return (CaseEncounter) dbSessionFactory.getCurrentSession().merge(caseEncounter);
	}
	
	@Override
	public List<PatientProgram> getPatientProgramByProgram(Program program) {
		if (program == null) {
			return new ArrayList<>();
		}
		
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(PatientProgram.class);
		criteria.add(eq("program", program));
		return criteria.list();
	}
	
	@Override
	public List<PatientProgram> getPatientProgramByProgramAndDate(Program program, Date startDate, @Nullable Date endDate,
	        boolean activeOnly) {
		if (program == null) {
			return new ArrayList<>();
		}
		
		endDate = endDate != null ? endDate : startDate;
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(PatientProgram.class);
		criteria.add(eq("program", program));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		if (activeOnly) {
			criteria.add(Restrictions.isNull("dateCompleted"));
		}
		
		String startDateStr = dateFormatter.format(startDate);
		String endDateStr = dateFormatter.format(endDate);
		
		String startFromDateTime = startDateStr + " " + "00:00:00";
		String endToDateTime = endDateStr + " " + "23:59:59";
		
		try {
			criteria.add(Restrictions.and(Restrictions.ge("dateEnrolled", dateTimeFormatter.parse(startFromDateTime)),
			    Restrictions.le("dateEnrolled", dateTimeFormatter.parse(endToDateTime))));
		}
		catch (Exception e) {
			if (e instanceof ParseException) {
				log.error("Error parsing date: {}", e.getMessage());
			}
			log.debug("Error converting date {}", e.getMessage(), e);
		}
		return criteria.list();
	}
	
	@Override
	public List<PatientProgram> getActivePatientProgramsByProgram(Program program, boolean includeVoided) {
		// TODO : Filter unique patients
		if (program == null) {
			return new ArrayList<>();
		}
		
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(PatientProgram.class);
		criteria.add(eq("program", program));
		criteria.add(eq("voided", includeVoided));
		criteria.add(Restrictions.isNull("dateCompleted"));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	public List<Person> getPatientByMigrationMetadata(String migrationFacilityId, String migrationPatientId) {
		PersonAttributeType migrationFacilityAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.MIGRATION_FACILITY_ATTRIBUTE);
		PersonAttributeType migrationPatientAttributeType = Context.getPersonService()
		        .getPersonAttributeTypeByUuid(BotswanaEmrConstants.MIGRATION_PATIENT_ATTRIBUTE);
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Person.class);
		//criteria.add(eq("program", program));
		//criteria.add(eq("voided", includeVoided));
		//criteria.add(eq(""));
		// PersonAttribute
		
		String sqlString = "select DISTINCT person_id  from ("
		        + "select pa.person_id  from person_attribute pa where (pa.person_attribute_type_id = :facilityAttributeType AND pa.value = :facilityAttributeValue) AND pa.person_id IN "
		        + "(select pa2.person_id  from person_attribute pa2 where (pa2.person_id = pa.person_id AND pa2.person_attribute_type_id = :patientAttributeType AND value = :patientAttributeValue))"
		        + ") p";
		
		String sql = "SELECT DISTINCT person FROM (" + "  SELECT pa.person FROM personAttribute pa"
		        + "  WHERE (pa.personAttributeType = :facilityAttributeType AND pa.value = :facilityAttributeValue)"
		        + "    AND pa.person IN (" + "      SELECT pa2.person FROM personAttribute pa2"
		        + "      WHERE (pa2.person = pa.person AND pa2.personAttributeType = :patientAttributeType AND value = :patientAttributeValue)"
		        + "    )" + ") p";
		// Context.getPatientService().getDuplicatePatientsByAttributes();
		
		NativeQuery sqlquery = dbSessionFactory.getHibernateSessionFactory().getCurrentSession()
		        .createNativeQuery(sqlString);
		
		sqlquery.setParameter("facilityAttributeType", migrationFacilityAttributeType.getId());
		sqlquery.setParameter("facilityAttributeValue", migrationFacilityId);
		sqlquery.setParameter("patientAttributeType", migrationPatientAttributeType.getId());
		sqlquery.setParameter("patientAttributeValue", migrationPatientId);
		
		//Collection patientIds = sqlquery.list();
		//if (!patientIds.isEmpty()) {
		//	Query query = dbSessionFactory.getCurrentSession().createQuery(
		//			"from Patient p1 where p1.patientId in (:ids)");
		//	query.setParameterList("ids", patientIds);
		//	patients = query.list();
		//}
		List<Integer> patientIds = new ArrayList<>();
		patientIds = sqlquery.list();
		
		Query<Person> query = dbSessionFactory.getHibernateSessionFactory().getCurrentSession()
		        .createQuery("from Patient p1 where p1.patientId in (:ids)");
		query.setParameterList("ids", patientIds);
		List<Person> people = query.list();
		
		return query.list();
	}
	
	@Override
	public List<PatientResponse> getPatientsBySearchParams(String nameOrUniqueId, String locationUuid, String programId,
	        String gender, String status, String startDateRegistered, String endDateRegistered, String condition) {
		NativeQuery nativeQuery = new PatientSearchBuilder(dbSessionFactory.getHibernateSessionFactory())
		        .withPatientName(nameOrUniqueId, true).withLocationAttribute().withPatientProgram(programId)
		        .withPatientCondition(condition).buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, locationUuid, gender,
		            startDateRegistered, endDateRegistered, status);
		return nativeQuery.list();
	}
	
	@Override
	public List<PatientResponse> getPatientsByHtsSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String testingLocation) {
		NativeQuery nativeQuery = new PatientSearchBuilder(dbSessionFactory.getHibernateSessionFactory())
		        .withPatientName(nameOrUniqueId, true).withLocationAttribute().withHtsTestingLocation(testingLocation)
		        .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, locationUuid, gender, startDateRegistered,
		            endDateRegistered, status);
		return nativeQuery.list();
	}
	
	@Override
	public CaseEncounter getCaseEncounter(Integer caseEncounterId) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(CaseEncounter.class);
		criteria.add(eq("caseEncounterId", caseEncounterId));
		return (CaseEncounter) criteria.uniqueResult();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CaseEncounter> getAllCaseEncounter(Patient patient, boolean includeVoided) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(CaseEncounter.class);
		criteria.add(eq("voided", includeVoided));
		if (patient != null) {
			criteria.add(eq("patient", patient));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	public CaseEncounter getCaseByEncounterId(Encounter encounter) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(CaseEncounter.class);
		if (encounter != null) {
			criteria.add(eq("encounter", encounter));
		}
		if (!criteria.list().isEmpty()) {
			return (CaseEncounter) criteria.list().get(0);
		} else {
			return null;
		}
	}
	
	public DiscussionParticipant addDiscussionParticipant(DiscussionParticipant discussionParticipant) {
		return (DiscussionParticipant) dbSessionFactory.getCurrentSession().merge(discussionParticipant);
	}
	
	public void deleteDiscussionParticipant(DiscussionParticipant discussionParticipant) {
		dbSessionFactory.getCurrentSession().delete(discussionParticipant);
	}
	
	public List<DiscussionParticipant> getDiscussionParticipantsByDiscussion(Discussion discussion) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(DiscussionParticipant.class);
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		if (discussion != null) {
			criteria.add(eq("discussion", discussion));
			return criteria.list();
		}
		return new ArrayList<>();
	}
	
	@Override
	public Discussion saveDiscussion(Discussion discussion) {
		return (Discussion) dbSessionFactory.getCurrentSession().merge(discussion);
	}
	
	@Override
	public Discussion getDiscussionByCase(Cases caze) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Discussion.class);
		criteria.add(eq("caze", caze));
		return (Discussion) criteria.uniqueResult();
	}
	
	@Override
	public Discussion getDiscussionById(Integer discussionId) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Discussion.class);
		criteria.add(eq("discussionId", discussionId));
		return (Discussion) criteria.uniqueResult();
	}
	
	@Override
	public Message saveMessage(Message message) {
		return (Message) dbSessionFactory.getCurrentSession().merge(message);
	}
	
	@Override
	public List<Message> getMessagesByDiscussion(Discussion discussion) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Message.class);
		if (discussion != null) {
			criteria.add(eq("discussion", discussion));
			return criteria.list();
		}
		return new ArrayList<>();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<org.openmrs.Order> getPrescriptionsFilledOnDatePerHour(Date date, String hour) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(org.openmrs.Order.class);
		String startDateStr = dateFormatter.format(date);
		String startFromDateTime = startDateStr + " " + hour + ":00:00";
		String endToDateTime = startDateStr + " " + hour + ":59:59";
		this.handleStartAndEndEncounterDatetime(criteria, startFromDateTime, endToDateTime, "dateActivated");
		return CollectionUtils.isNotEmpty(criteria.list()) ? criteria.list() : null;
	}
	
	@Override
	public Collection<Patient> findPatientsInContactWithTB(Patient patient) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(org.openmrs.Patient.class);
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		//Exclude the voided patients from the list
		criteria.add(eq("voided", false));
		
		criteria.createAlias("attributes", "at");
		criteria.add(eq("at.value", patient.getUuid()));
		
		return criteria.list();
	}
	
	@Override
	public DrugOrder getDrugOrderById(Integer drugOrderId) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(DrugOrder.class);
		criteria.add(eq("orderId", drugOrderId));
		return (DrugOrder) criteria.uniqueResult();
	}
	
	@Override
	public DrugOrderExtension saveDrugOrderExtension(DrugOrderExtension drugOrderExtension) {
		return (DrugOrderExtension) dbSessionFactory.getCurrentSession().merge(drugOrderExtension);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DrugOrderExtension> getDrugOrderExtensionsByPatientId(Patient patient) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(DrugOrderExtension.class);
		Criteria patientCriteria = criteria.createCriteria("order", "o").createCriteria("o.patient", "eop");
		if (patient != null) {
			criteria.add(eq("eop.uuid", patient.getUuid()));
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return criteria.list();
	}
	
	@Override
	public List<Patient> getBotswanaEmrPatients(String query, boolean includeVoided, Integer start, Integer length) {
		if (StringUtils.isBlank(query) || (length != null && length < 1)) {
			return Collections.emptyList();
		}
		
		Integer tmpStart = start;
		if (tmpStart == null || tmpStart < 0) {
			tmpStart = 0;
		}
		
		Integer tmpLength = length;
		if (tmpLength == null) {
			tmpLength = HibernatePersonDAO.getMaximumSearchResults();
		}
		
		List<Patient> patients = findPatients(query, includeVoided, tmpStart, tmpLength);
		
		return new ArrayList<>(patients);
	}
	
	public List<Patient> findPatients(String query, boolean includeVoided, Integer start, Integer length) {
		Integer tmpStart = start;
		if (tmpStart == null) {
			tmpStart = 0;
		}
		Integer maxLength = HibernatePersonDAO.getMaximumSearchResults();
		Integer tmpLength = length;
		if (tmpLength == null || tmpLength > maxLength) {
			tmpLength = maxLength;
		}
		query = LuceneQuery.escapeQuery(query);
		
		List<Patient> patients = new LinkedList<>();
		
		String minChars = Context.getAdministrationService()
		        .getGlobalProperty(OpenmrsConstants.GLOBAL_PROPERTY_MIN_SEARCH_CHARACTERS);
		
		if (minChars == null || !StringUtils.isNumeric(minChars)) {
			minChars = "" + OpenmrsConstants.GLOBAL_PROPERTY_DEFAULT_MIN_SEARCH_CHARACTERS;
		}
		if (query.length() < Integer.valueOf(minChars)) {
			return patients;
		}
		
		LuceneQuery<PatientIdentifier> identifierQuery = getPatientIdentifierLuceneQuery(query, includeVoided, false);
		
		long identifiersSize = identifierQuery.resultSize();
		if (identifiersSize > tmpStart) {
			ListPart<Object[]> patientIdentifiers = identifierQuery.listPartProjection(tmpStart, tmpLength,
			    "patient.personId");
			patientIdentifiers.getList()
			        .forEach(patientIdentifier -> patients.add(getBotswanaEmrPatient((Integer) patientIdentifier[0])));
			
			tmpLength -= patientIdentifiers.getList().size();
			tmpStart = 0;
		} else {
			tmpStart -= (int) identifiersSize;
		}
		
		if (tmpLength == 0) {
			return patients;
		}
		
		PersonLuceneQuery personLuceneQuery = new PersonLuceneQuery(
		        dbSessionFactory.getCurrentSession().getSessionFactory());
		
		LuceneQuery<PersonName> nameQuery = personLuceneQuery.getPersonNameQuery(query, includeVoided, identifierQuery);
		long namesSize = nameQuery.resultSize();
		if (namesSize > tmpStart) {
			ListPart<Object[]> personNames = nameQuery.listPartProjection(tmpStart, tmpLength, "person.personId");
			personNames.getList().forEach(personName -> patients.add(getBotswanaEmrPatient((Integer) personName[0])));
			
			tmpLength -= personNames.getList().size();
			tmpStart = 0;
		} else {
			tmpStart -= (int) namesSize;
		}
		
		if (tmpLength == 0) {
			return patients;
		}
		
		LuceneQuery<PersonAttribute> attributeQuery = personLuceneQuery.getPatientAttributeQuery(query, includeVoided,
		    nameQuery);
		long attributesSize = attributeQuery.resultSize();
		if (attributesSize > tmpStart) {
			ListPart<Object[]> personAttributes = attributeQuery.listPartProjection(tmpStart, tmpLength, "person.personId");
			personAttributes.getList()
			        .forEach(personAttribute -> patients.add(getBotswanaEmrPatient((Integer) personAttribute[0])));
		}
		
		return patients;
	}
	
	/**
	 * @param patientId internal patient identifier
	 * @return patient with given internal identifier
	 * @see org.openmrs.api.PatientService#getPatient(java.lang.Integer)
	 */
	@Override
	public Patient getBotswanaEmrPatient(Integer patientId) {
		return (Patient) dbSessionFactory.getHibernateSessionFactory().getCurrentSession().get(Patient.class, patientId);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<PatientQueue> getIncompletePatientQueues() {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(PatientQueue.class);
		criteria.add(or(eq("status", PatientQueue.Status.PENDING), eq("status", PatientQueue.Status.PICKED)));
		return criteria.list();
	}
	
	private LuceneQuery<PatientIdentifier> getPatientIdentifierLuceneQuery(String query, boolean includeVoided,
	        boolean matchExactly) {
		LuceneQuery<PatientIdentifier> luceneQuery = getPatientIdentifierLuceneQuery(query, matchExactly);
		if (!includeVoided) {
			luceneQuery.include("voided", false);
			luceneQuery.include("patient.voided", false);
		}
		
		luceneQuery.include("patient.isPatient", true);
		luceneQuery.skipSame("patient.personId");
		
		return luceneQuery;
	}
	
	private LuceneQuery<PatientIdentifier> getPatientIdentifierLuceneQuery(String paramQuery, boolean matchExactly) {
		String query = removeIdentifierPadding(paramQuery);
		List<String> tokens = tokenizeIdentifierQuery(query);
		query = StringUtils.join(tokens, " OR ");
		List<String> fields = new ArrayList<>();
		fields.add("identifierPhrase");
		fields.add("identifierType");
		String matchMode = Context.getAdministrationService()
		        .getGlobalProperty(OpenmrsConstants.GLOBAL_PROPERTY_PATIENT_IDENTIFIER_SEARCH_MATCH_MODE);
		if (matchExactly) {
			fields.add("identifierExact");
		} else if (OpenmrsConstants.GLOBAL_PROPERTY_PATIENT_SEARCH_MATCH_START.equals(matchMode)) {
			fields.add("identifierStart");
		} else {
			fields.add("identifierAnywhere");
		}
		return LuceneQuery.newQuery(PatientIdentifier.class,
		    (Session) dbSessionFactory.getHibernateSessionFactory().getCurrentSession(), query, fields);
		
	}
	
	private String removeIdentifierPadding(String query) {
		String regex = Context.getAdministrationService()
		        .getGlobalProperty(OpenmrsConstants.GLOBAL_PROPERTY_PATIENT_IDENTIFIER_REGEX, "");
		if (Pattern.matches("^\\^.{1}\\*.*$", regex)) {
			String padding = regex.substring(regex.indexOf("^") + 1, regex.indexOf("*"));
			Pattern pattern = Pattern.compile("^" + padding + "+");
			query = pattern.matcher(query).replaceFirst("");
		}
		return query;
	}
	
	/**
	 * Copied over from PatientSearchCriteria... I have no idea how it is supposed to work, but tests
	 * pass...
	 *
	 * @param query
	 * @return
	 * @see PatientSearchCriteria
	 */
	private List<String> tokenizeIdentifierQuery(String query) {
		List<String> searchPatterns = new ArrayList<>();
		
		String patternSearch = Context.getAdministrationService()
		        .getGlobalProperty(OpenmrsConstants.GLOBAL_PROPERTY_PATIENT_IDENTIFIER_SEARCH_PATTERN, "");
		
		if (StringUtils.isBlank(patternSearch)) {
			searchPatterns.add(query);
		} else {
			// split the pattern before replacing in case the user searched on a comma
			// replace the @SEARCH@, etc in all elements
			for (String pattern : patternSearch.split(",")) {
				searchPatterns.add(replaceSearchString(pattern, query));
			}
		}
		return searchPatterns;
	}
	
	private String replaceSearchString(String regex, String identifierSearched) {
		String returnString = regex.replaceAll("@SEARCH@", identifierSearched);
		if (identifierSearched.length() > 1) {
			// for 2 or more character searches, we allow regex to use last character as check digit
			returnString = returnString.replaceAll("@SEARCH-1@",
			    identifierSearched.substring(0, identifierSearched.length() - 1));
			returnString = returnString.replaceAll("@CHECKDIGIT@",
			    identifierSearched.substring(identifierSearched.length() - 1));
		} else {
			returnString = returnString.replaceAll("@SEARCH-1@", "");
			returnString = returnString.replaceAll("@CHECKDIGIT@", "");
		}
		return returnString;
	}
	
	@Override
	public List<BotswanaEmrEmailReportConfig> getBotswanaEmrEmailReportConfigs(String reportId, Location facility) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(BotswanaEmrEmailReportConfig.class);
		if (reportId != null && !reportId.isEmpty())
			criteria.add(eq("mailReportId", reportId));
		if (facility != null)
			criteria.add(eq("facilityId", facility));
		return CollectionUtils.isNotEmpty(criteria.list()) ? criteria.list() : null;
	}
	
	@Override
	public BotswanaEmrEmailReportConfig saveBotswanaEmrEmailReportConfig(
	        BotswanaEmrEmailReportConfig botswanaEmrEmailReportConfig) {
		dbSessionFactory.getCurrentSession().saveOrUpdate(botswanaEmrEmailReportConfig);
		
		return botswanaEmrEmailReportConfig;
		
	}
	
	@Override
	public CaseEncounter getLastCaseEncounter(Patient patient) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(CaseEncounter.class);
		criteria.add(eq("voided", false));
		criteria.addOrder(Order.desc("dateCreated"));
		criteria.setMaxResults(1);
		return (CaseEncounter) criteria.uniqueResult();
	}
	
	@Override
	public List<Encounter> getEncounterLimits(EncounterSearchCriteria searchCriteria, Integer fetchLimit)
	        throws DAOException {
		Criteria crit = dbSessionFactory.getCurrentSession().createCriteria(Encounter.class);
		if (searchCriteria.getPatient() != null && searchCriteria.getPatient().getPatientId() != null) {
			crit.add(Restrictions.eq("patient", searchCriteria.getPatient()));
		}
		if (searchCriteria.getEncounterTypes() != null && !searchCriteria.getEncounterTypes().isEmpty()) {
			crit.add(Restrictions.in("encounterType", searchCriteria.getEncounterTypes()));
		}
		if (searchCriteria.getVisits() != null && !searchCriteria.getVisits().isEmpty()) {
			crit.add(Restrictions.in("visit", searchCriteria.getVisits()));
		}
		if (!searchCriteria.getIncludeVoided()) {
			crit.add(Restrictions.eq("voided", false));
		}
		crit.addOrder(Order.desc("encounterDatetime"));
		if (fetchLimit != null) {
			crit.setMaxResults(fetchLimit);
		}
		crit.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("associationProperty", FetchMode.SELECT);
		return crit.list();
	}
	
	public Long getPatientsRegisteredCountOnDate(Date startDate, Date endDate, String location, Integer limit)
	        throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class, "pay");
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null && endDate != null) {
			startFromDate = fromDate(startDate);
			endFromDate = toDate(endDate);
			
			this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "dateCreated");
		}
		criteria.add(eq("voided", false));
		criteria.createAlias("pay.patient", "patient");
		criteria.createAlias("patient.attributes", "attributes");
		criteria.createAlias("attributes.attributeType", "attributesTypes");
		if (location != null) {
			criteria.add(eq("attributesTypes.name", "LocationAttribute"));
			criteria.add(eq("attributes.value", location));
		}
		if (limit != null) {
			criteria.setFirstResult(0);
			criteria.setMaxResults(limit);
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.list().get(0);
	}
	
	public Registration getPatientsRegisteredPositionOnDate(Date startDate, Date endDate, String location, String position)
	        throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class, "pay");
		String startFromDate = "";
		String endFromDate = "";
		if (startDate != null && endDate != null) {
			startFromDate = fromDate(startDate);
			endFromDate = toDate(endDate);
			
			this.handleStartAndEndEncounterDatetime(criteria, startFromDate, endFromDate, "dateCreated");
		}
		criteria.add(eq("voided", false));
		criteria.createAlias("pay.patient", "patient");
		criteria.createAlias("patient.attributes", "attributes");
		criteria.createAlias("attributes.attributeType", "attributesTypes");
		if (location != null) {
			criteria.add(eq("attributesTypes.name", "LocationAttribute"));
			criteria.add(eq("attributes.value", location));
		}
		if (position.equals("last")) {
			criteria.addOrder(Order.desc("dateCreated"));
			criteria.setMaxResults(1);
		}
		if (position.equals("first")) {
			criteria.addOrder(Order.asc("dateCreated"));
			criteria.setMaxResults(1);
		}
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		return (Registration) criteria.uniqueResult();
	}
	
	@Override
	public List<Obs> getObservation(Person person, Visit visit, Concept question, Location location, Integer mostRecentN)
	        throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Obs.class, "obs");
		criteria.add(Restrictions.in("person", person));
		if (visit != null && !(visit.getNonVoidedEncounters().isEmpty())) {
			criteria.add(Restrictions.in("encounter", visit.getNonVoidedEncounters()));
		}
		if (question != null) {
			criteria.add(Restrictions.eq("concept", question));
		}
		if (location != null) {
			criteria.add(Restrictions.eq("location", location));
		}
		criteria.add(Restrictions.eq("voided", false));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		if (mostRecentN != null && mostRecentN > 0) {
			criteria.setMaxResults(mostRecentN);
		}
		criteria.addOrder(Order.desc("obsDatetime"));
		return criteria.list();
	}
	
	@Override
	public List<Obs> getObservation(Person person, EncounterType encounterType, Visit visit, Concept question,
	        Location location, Integer mostRecentN) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Obs.class, "obs");
		criteria.add(Restrictions.in("person", person));
		if (visit != null && !(visit.getNonVoidedEncounters().isEmpty())) {
			criteria.add(Restrictions.in("encounter", visit.getNonVoidedEncounters()));
		}
		if (question != null) {
			criteria.add(Restrictions.eq("concept", question));
		}
		if (location != null) {
			criteria.add(Restrictions.eq("location", location));
		}
		criteria.add(Restrictions.eq("voided", false));
		criteria.setFetchMode("associationProperty", FetchMode.SELECT);
		if (mostRecentN != null && mostRecentN > 0) {
			criteria.setMaxResults(mostRecentN);
		}
		criteria.createAlias("obs.encounter", "enc");
		if (encounterType != null) {
			criteria.add(Restrictions.eq("enc.encounterType", encounterType));
		}
		criteria.addOrder(Order.desc("obsDatetime"));
		return criteria.list();
	}
	
	@Override
	public List<CodedOrFreeText> getAllConditionEverSaved() throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Condition.class);
		criteria.setProjection(Projections.distinct(Projections.property("condition")));
		return criteria.list();
	}
	
	@Override
	public List<PatientResponse> getPatientsByVmmcSearchParams(String nameOrUniqueId, String locationUuid, String gender,
	        String status, String startDateRegistered, String endDateRegistered, String visitStatus) {
		NativeQuery nativeQuery = new PatientSearchBuilder(dbSessionFactory.getHibernateSessionFactory())
		        .withPatientName(nameOrUniqueId, true).withLocationAttribute().withVmmcVisitStatusFilter(visitStatus)
		        .withLastVisitDateFilter(startDateRegistered, endDateRegistered)
		        .buildSqlQuery(BotswanaEmrConstants.MAX_SQL_RESULT, 0, locationUuid, gender, null, null, status);
		return nativeQuery.list();
	}
	
	@Override
	public Registration getLatestPaymentEntry(Patient patient, Location location) throws DAOException {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(Registration.class, "payment");
		criteria.add(and(eq("patient", patient), eq("location", location)));
		criteria.addOrder(Order.desc("registrationDatetime"));
		criteria.setMaxResults(1);
		return (Registration) criteria.uniqueResult();
	}
}
