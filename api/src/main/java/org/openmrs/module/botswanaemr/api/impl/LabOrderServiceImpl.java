/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.CareSetting;
import org.openmrs.Concept;
import org.openmrs.Drug;
import org.openmrs.Encounter;
import org.openmrs.EncounterProvider;
import org.openmrs.Obs;
import org.openmrs.Order;
import org.openmrs.OrderType;
import org.openmrs.Patient;
import org.openmrs.Provider;
import org.openmrs.TestOrder;
import org.openmrs.api.OrderService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabOrderService;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.*;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.DRUG_ORDER_TYPE_UUID;

@Slf4j
@Component("botswana.emr.labOrderService")
public class LabOrderServiceImpl implements LabOrderService {
	
	@Override
	public List<Order> getActiveOrders(Patient patient) {
		OrderType labOrderType = Context.getOrderService().getOrderTypeByUuid(BotswanaEmrConstants.TEST_ORDER_TYPE_UUID);
		return Context.getOrderService().getActiveOrders(patient, labOrderType, null, null);
	}
	
	@Override
	public void createLabTest(@NotNull Order order) {
		if (order != null) {
			LabTest labTest = new LabTest();
			labTest.setAcceptDate(order.getDateCreated());
			labTest.setOrder(order);
			labTest.setLabTestStatus(0);
			labTest.setPatient(order.getPatient());
			labTest.setConcept(order.getConcept());
			labTest.setStatus(BotswanaEmrConstants.labStatus.PENDING.name());
			labTest.setCreator(order.getCreator());
			labTest.setEncounter(order.getEncounter());
			//save the lab test
			Context.getService(LabService.class).saveLabTest(labTest);
		} else {
			log.debug("Order can't be null");
		}
	}
	
	@Override
	public void createLabTestsFromOrdersSet(Set<Order> orderSet) {
		if (orderSet != null) {
			for (Order order : orderSet) {
				if (order != null) {
					LabTest labTest = new LabTest();
					labTest.setAcceptDate(order.getDateCreated());
					labTest.setOrder(order);
					labTest.setLabTestStatus(0);
					labTest.setPatient(order.getPatient());
					labTest.setConcept(order.getConcept());
					labTest.setStatus(BotswanaEmrConstants.labStatus.PENDING.name());
					labTest.setCreator(order.getCreator());
					labTest.setEncounter(order.getEncounter());
					//save the lab test
					Context.getService(LabService.class).saveLabTest(labTest);
				} else {
					log.debug("Order can't be null");
				}
			}
		}
	}
	
	@Override
	public TestOrder createOrder(Encounter encounter) {
		TestOrder order = null;
		OrderType labOrderType = Context.getOrderService().getOrderTypeByUuid(BotswanaEmrConstants.TEST_ORDER_TYPE_UUID);
		CareSetting careSetting = Context.getOrderService().getCareSettingByUuid(BotswanaEmrConstants.CARE_SETTING_UUID);
		
		if (encounter != null && labOrderType != null && careSetting != null) {
			Set<Concept> finalConceptsToCreateOrder = getLabOrTestConceptUsed(encounter.getAllObs());
			
			for (Concept orderConcept : finalConceptsToCreateOrder) {
				order = new TestOrder();
				order.setConcept(orderConcept);
				order.setCreator(encounter.getCreator());
				order.setDateCreated(encounter.getDateCreated());
				if (BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()) != null) {
					order.setOrderer(BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()));
				}
				order.setPatient(encounter.getPatient());
				order.setDateActivated(encounter.getDateCreated());
				order.setAccessionNumber("0");
				order.setOrderType(labOrderType);
				order.setEncounter(encounter);
				order.setCareSetting(careSetting);
				//save the order
				Context.getOrderService().saveOrder(order, null);
			}
		}
		return order;
	}
	
	@Override
	public void updateLabOrder(Patient patient, Encounter encounter) {
		List<Obs> whichTestOrderLabResults = Context.getObsService().getObservations(Collections.singletonList(patient),
		    Collections.singletonList(encounter), null, null, null, null, null, null, null, null, null, false);
		
		List<Concept> testOrderedConcepts = new ArrayList<>();
		//TO-DO
		ArrayList<String> testConcepts = new ArrayList<>(
		        Arrays.asList(BotswanaEmrConstants.TB_TEST_TYPE, BotswanaEmrConstants.HIV_CONFIRMATORY_TEST_CONCEPT_UUID));
		whichTestOrderLabResults.forEach(obs -> {
		    //which order- 165254AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		    //Microscopy - 165254AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		    //mycobacterial - 159982AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		    //DST - 164395AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		    //LPA - 160046AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		    if (testConcepts.contains(obs.getConcept().getUuid())) {
				if (obs.getValueCoded() != null) {
					testOrderedConcepts.add(obs.getValueCoded());
					log.error("Test ordered: {}", obs.getValueCoded());
				}
			}
		});
		
		testOrderedConcepts.forEach(tbTestConcept -> updateLabOrder(patient, encounter, tbTestConcept));
		testOrderedConcepts.forEach(testConcept -> updateLabOrder(patient, encounter, testConcept));
	}
	
	@Override
	public List<Order> getAllActiveMedications(Patient patient) {
		OrderService orderService = Context.getOrderService();
		OrderType drugOrderType = orderService.getOrderTypeByUuid(DRUG_ORDER_TYPE_UUID);
		return orderService.getActiveOrders(patient, drugOrderType, getCareSetting(), null);
	}
	
	private Provider getProvider(Set<EncounterProvider> encounterProviders) {
		if (!encounterProviders.isEmpty()) {
			return new ArrayList<>(encounterProviders).get(0).getProvider();
		}
		return null;
	}
	
	private CareSetting getCareSetting() {
		return Context.getOrderService().getCareSettingByUuid(BotswanaEmrConstants.CARE_SETTING_UUID);
	}
	
	private Concept getConcept(@NotNull String conceptUuid) {
		return Context.getConceptService().getConceptByUuid(conceptUuid);
	}
	
	private Drug getDrug(@NotNull String drugUuid) {
		return Context.getConceptService().getDrugByUuid(drugUuid);
	}
	
	private void updateLabOrder(@NotNull Patient patient, Encounter encounter, Concept concept) {
		List<LabTest> pendingLabTests = this.getActiveLabTests(patient);
		pendingLabTests.forEach(labTest -> {
			if (labTest.getOrder().getConcept().getUuid().equals(concept.getUuid())) {
				//complete the order
				
				Order pendingOrderToComplete = labTest.getOrder().cloneForRevision();
				pendingOrderToComplete.setEncounter(encounter);
				//just default to initial orderer for now
				pendingOrderToComplete.setOrderer(labTest.getOrder().getOrderer());
				
				Context.getOrderService().saveOrder(pendingOrderToComplete, null);
				
				//set status to completed
				//difference btw setLabTestStatus & status
				labTest.setLabTestStatus(1);
				labTest.setEncounterResult(encounter);
				labTest.setStatus(BotswanaEmrConstants.labStatus.COMPLETED.name());
				Context.getService(LabService.class).saveLabTest(labTest);
			}
		});
	}
	
	private List<LabTest> getActiveLabTests(@NotNull Patient patient) {
		List<LabTest> labTests = Context.getService(LabService.class).getLaboratoryTestsByPatient(patient);
		//remove completed orders
		labTests.removeIf(labTest -> labTest.getStatus().equals(BotswanaEmrConstants.labStatus.COMPLETED.name()));
		
		return labTests;
	}
	
	private Set<Concept> getConceptQuestionsUsed(Set<Obs> obsSet) {
		Set<Concept> validCollectedQuestions = new HashSet<>();
		if (obsSet != null && obsSet.size() > 0) {
			for (Obs obs : obsSet) {
				if (obs != null && obs.getValueCoded() != null
				        && (obs.getConcept().getUuid().equals(BotswanaEmrConstants.TB_TEST_TYPE)
				                || obs.getConcept().getUuid().equals(BotswanaEmrConstants.HIV_TEST_CONCEPT_UUID))) {
					validCollectedQuestions.add(obs.getValueCoded());
				}
			}
		}
		return validCollectedQuestions;
	}
	
	private Set<Concept> getLabOrTestConceptUsed(Set<Obs> obsSet) {
		Set<Concept> validCollected = new HashSet<>();
		if (obsSet != null && obsSet.size() > 0) {
			for (Obs obs : obsSet) {
				if (obs != null && obs.getValueCoded() != null) {
					if ((obs.getConcept().getUuid().equals(BotswanaEmrConstants.TEST_ORDERED_CONCEPT_UUID) ||
						obs.getConcept().getUuid().equals(BotswanaEmrConstants.TB_TEST_TYPE))
					        && (obs.getValueCoded().getConceptClass().getName().equals("LabSet")
					                || obs.getValueCoded().getConceptClass().getName().equals("Test"))) {
						validCollected.add(obs.getValueCoded());
					}
				}
				
			}
		}
		return validCollected;
	}
	
	public Set<Order> createOrderByEncounterAndListOfConcepts(@NotNull Encounter encounter, List<Concept> conceptList) {
		Order order;
		Set<Order> orderSet = new HashSet<>();
		OrderType labOrderType = Context.getOrderService().getOrderTypeByUuid(BotswanaEmrConstants.TEST_ORDER_TYPE_UUID);
		CareSetting careSetting = Context.getOrderService().getCareSettingByUuid(BotswanaEmrConstants.CARE_SETTING_UUID);
		
		if (encounter != null && labOrderType != null && careSetting != null) {
			Set<Concept> finalConceptsToCreateOrder = new HashSet<>(conceptList);
			for (Concept orderConcept : finalConceptsToCreateOrder) {
				order = new TestOrder();
				order.setConcept(orderConcept);
				order.setCreator(encounter.getCreator());
				order.setDateCreated(encounter.getDateCreated());
				if (BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()) != null) {
					order.setOrderer(BotswanaEmrUtils.getProvider(Context.getAuthenticatedUser().getPerson()));
				}
				order.setPatient(encounter.getPatient());
				order.setDateActivated(encounter.getDateCreated());
				order.setAccessionNumber("0");
				order.setOrderType(labOrderType);
				order.setEncounter(encounter);
				order.setCareSetting(careSetting);
				//save the order
				Context.getOrderService().saveOrder(order, null);
				orderSet.add(order);
			}
		}
		return orderSet;
	}
}
