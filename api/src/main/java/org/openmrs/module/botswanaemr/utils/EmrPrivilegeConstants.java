/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utils;

import org.openmrs.annotation.AddOnStartup;
import org.openmrs.annotation.HasAddOnStartupPrivileges;

@HasAddOnStartupPrivileges
public class EmrPrivilegeConstants {
	
	private EmrPrivilegeConstants() {
	}
	
	@AddOnStartup(description = "Able to get payment method in BotswanaEMR system")
	public static final String GET_PAYMENT_METHOD = "Get BotswanaEMR Payment Methods";
	
	@AddOnStartup(description = "Able to get service provider in BotswanaEMR system")
	public static final String GET_SERVICE_PROVIDER = "Get BotswanaEMR Service Provider";
	
	@AddOnStartup(description = "Able to add Payment in BotswanaEMR system")
	public static final String ADD_PAYMENT = "Add BotswanaEMR Payment";
	
	@AddOnStartup(description = "Able to edit Payment in BotswanaEMR system")
	public static final String EDIT_PAYMENT = "Edit BotswanaEMR Payment";
	
	@AddOnStartup(description = "Able to get Payment in BotswanaEMR system")
	public static final String GET_PAYMENT = "Get BotswanaEMR Payment";
}
