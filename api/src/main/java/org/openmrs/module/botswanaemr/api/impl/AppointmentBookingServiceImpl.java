/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.openmrs.Location;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.api.AppointmentBookingService;
import org.openmrs.module.botswanaemr.api.dao.AppointmentBookingDao;
import org.openmrs.module.botswanaemr.model.appointment.AppointmentBookingLimit;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppointmentBookingServiceImpl extends BaseOpenmrsService implements AppointmentBookingService {
	
	private AppointmentBookingDao appointmentBookingDao;
	
	public AppointmentBookingDao getAppointmentBookingDao() {
		return appointmentBookingDao;
	}
	
	public void setAppointmentBookingDao(AppointmentBookingDao appointmentBookingDao) {
		this.appointmentBookingDao = appointmentBookingDao;
	}
	
	@Override
	public void saveAppointmentBookingLimit(AppointmentBookingLimit appointmentBookingLimit) {
		appointmentBookingDao.saveOrUpdate(appointmentBookingLimit);
	}
	
	@Override
	public void updateAppointmentBookingLimit(AppointmentBookingLimit appointmentBookingLimit) {
		appointmentBookingDao.saveOrUpdate(appointmentBookingLimit);
	}
	
	@Override
	public AppointmentBookingLimit getAppointmentBookingLimit(Integer id) {
		return appointmentBookingDao.getById(id);
	}
	
	@Override
	public AppointmentBookingLimit getAppointmentBookingLimit(Location location, String day) {
		return appointmentBookingDao.getAppointmentBookingLimit(location, day);
	}
	
	@Override
	public List<AppointmentBookingLimit> getAppointmentBookingLimits(Location location) {
		return appointmentBookingDao.getAppointmentBookingLimits(location);
	}
	
	@Override
	public List<AppointmentBookingLimit> getList() {
		return appointmentBookingDao.getAll(false);
	}
	
	@Override
	public void voidAppointmentBookingLimit(AppointmentBookingLimit appointmentBookingLimit) {
		appointmentBookingDao.delete(appointmentBookingLimit);
	}
}
