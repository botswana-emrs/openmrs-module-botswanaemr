/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utilities;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Months;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public class DateUtils {
	
	public static String getStringDate(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}
	
	public static Month getMonth(Date date) {
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.getMonth();
	}
	
	public static int getMonthsBetween(Date fromDate) {
		return getMonthsBetween(fromDate, new Date());
	}
	
	public static int getMonthsBetween(Date fromDate, Date toDate) {
		YearMonth from = getYearMonth(fromDate);
		YearMonth to = getYearMonth(toDate);
		int yearDiff = to.getYear() - from.getYear();
		return (yearDiff + to.getMonthValue()) - from.getMonthValue();
	}
	
	public static YearMonth getYearMonth(Date date) {
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return YearMonth.of(localDate.getYear(), localDate.getMonth());
	}
	
	public static Date lastDayOfMonth(Date date) {
		return Date.from(Instant.from(YearMonth.from(date.toInstant()).atEndOfMonth()));
	}
	
	public static Date lastDayOfMonth(YearMonth date) {
		return Date.from(date.atEndOfMonth().atTime(23, 59, 59).toInstant(ZoneOffset.MAX));
	}
	
	public static Date firstDayOfMonth(Date date) {
		return Date.from(Instant.from(YearMonth.from(date.toInstant()).atDay(1)));
	}
	
	public static Date firstDayOfMonth(YearMonth date) {
		return Date.from(date.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	public static int getDateOfTheMonth(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).getDayOfMonth();
	}
	
	public static int getHoursInBetween(Date start, Date end) {
		DateTime time1 = new DateTime(start);
		DateTime time2 = new DateTime(end);
		
		return Hours.hoursBetween(time1, time2).getHours();
	}
	
	public static int getMonthsInBetween(Date start, Date end) {
		DateTime time1 = new DateTime(start);
		DateTime time2 = new DateTime(end);
		
		return Months.monthsBetween(time1, time2).getMonths();
	}
	
	public static Date getDateStartOfDay(Date startDate) {
		return new DateTime(startDate).withTimeAtStartOfDay().toDate();
	}
	
	public static Date getDateEndOfDay(Date endDate) {
		return new DateTime(endDate).withTime(23, 59, 59, 999).toDate();
	}
	
	public static Date addMonthsToDate(Date latestDate, int i) {
		return new DateTime(latestDate).plusMonths(i).toDate();
	}
}
