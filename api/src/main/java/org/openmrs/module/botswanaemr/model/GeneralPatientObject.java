/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

public class GeneralPatientObject {
	
	private String creator;
	
	private String name;
	
	private String associatedEncounter;
	
	private String typeText;
	
	public String getCreator() {
		return creator;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getAssociatedEncounter() {
		return associatedEncounter;
	}
	
	public void setAssociatedEncounter(String associatedEncounter) {
		this.associatedEncounter = associatedEncounter;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public void setTypeText(String typeText) {
		this.typeText = typeText;
	}
	
	public String getTypeText() {
		return typeText;
	}
	
	private String gender;
	
	private String duration;
}
