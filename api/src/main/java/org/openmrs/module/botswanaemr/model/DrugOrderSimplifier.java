/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.DrugOrder;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.PharmacyUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

@Slf4j
// Lombok @Data annotation generates getters, setters, toString, equals, and hashCode methods
@Data
public class DrugOrderSimplifier {
	
	private String drug;
	
	private String dosage;
	
	private String refillDuration;
	
	private String refillRepeat;
	
	private String frequency;
	
	private String route;
	
	private Integer orderId;
	
	private Integer fillCount;
	
	private Integer totalRefills;
	
	private Integer drugOrderId;
	
	private String drugOrderUuid;
	
	private String status;
	
	private boolean refills;
	
	private String dateRefilled;
	
	private Double calculatedQuantity;
	
	private String timeRefilled;
	
	private String facility;
	
	private String pharmacist;
	
	private Integer stockAvailability;
	
	private Integer itemId;
	
	private String itemUuid;
	
	private String itemCode;
	
	private String storeRoomLocation;
	
	private Integer stockroomId;
	
	private String requestingFacility;
	
	private String holdingFacility;
	
	private String currentFacility;
	
	private String orderReason;
	
	private Integer drugId;
	
	private Integer drugConceptId;
	
	private String dosingInstructions;
	
	public DrugOrderSimplifier() {
	}
	
	public DrugOrderSimplifier(DrugOrder drugOrder) {
		this.orderId = drugOrder.getOrderId();
		this.drugOrderId = drugOrder.getOrderId(); 
		this.drugOrderUuid = drugOrder.getUuid();
		this.drug = determineDrug(drugOrder);
		this.frequency = determineFrequency(drugOrder);
		this.route = determineRoute(drugOrder);
		this.calculatedQuantity = calculateQuantity(drugOrder);
		this.fillCount = PharmacyUtils.refillsDone(drugOrder.getOrderId());
		this.totalRefills = PharmacyUtils.getTotalRefills(drugOrder.getOrderId());
		this.dosage = determineDosage(drugOrder);
		this.refillDuration = determineRefillDuration(drugOrder);
		this.refillRepeat = determinRefillRepeat(drugOrder);
		this.status = determineStatus(drugOrder);
		this.dosingInstructions = determineDosingInstructions(drugOrder);
		determineDrugIds(drugOrder);
	}
	
	private String determineDosingInstructions(DrugOrder drugOrder) {
		String instructions = drugOrder.getInstructions();
		if (instructions != null) {
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				Map<String, Object> instructionsMap = objectMapper.readValue(instructions, Map.class);
				if (instructionsMap.containsKey("dosingInstructions")) {
					return (String) instructionsMap.get("dosingInstructions");
				}
			} catch (Exception e) {
				log.error("Error parsing dosing instructions for drug order", e);
			}
		}
		return null;
	}
	
	private String determineStatus(DrugOrder drugOrder) {
		if (drugOrder.getFulfillerStatus() != null) {
			return drugOrder.getFulfillerStatus().name();
		} else {
			return "";
		}
	}

	private String determinRefillRepeat(DrugOrder drugOrder) {
		if (drugOrder.getNumRefills() != null) {
			return String.valueOf(drugOrder.getNumRefills());
		} else {
			return "";
		}
	}

	private String determineRefillDuration(DrugOrder drugOrder) {
		if (drugOrder.getDuration() != null && drugOrder.getDurationUnits() != null) {
			return String.valueOf(drugOrder.getDuration() + " " + drugOrder.getDurationUnits().getDisplayString());
		} else {
			return "";
		}
	}
	
	private String determineDosage(DrugOrder drugOrder) {
		// If dose is null, return empty string
		if (drugOrder.getDose() == null || drugOrder.getDoseUnits() == null) {
			return "";
		}
		return Math.round(drugOrder.getDose()) + " " + drugOrder.getDoseUnits().getDisplayString();
	}
	
	private String determineDrug(DrugOrder drugOrder) {
		if (drugOrder.getDrug() != null) {
			return drugOrder.getDrug().getDisplayName();
		} else if (drugOrder.getDrugNonCoded() != null) {
			return drugOrder.getDrugNonCoded();
		} else {
			return null;
		}
	}
	
	private String determineFrequency(DrugOrder drugOrder) {
		return (drugOrder.getFrequency() != null && drugOrder.getFrequency().getName() != null)
		        ? drugOrder.getFrequency().getName()
		        : "";
	}
	
	private String determineRoute(DrugOrder drugOrder) {
		return drugOrder.getRoute() != null ? drugOrder.getRoute().getDisplayString() : "";
	}
	
	private Double calculateQuantity(DrugOrder drugOrder) {
		String[] dosageFormUuids = { BotswanaEmrConstants.TABLET_CONCEPT_UUID, BotswanaEmrConstants.CAPSULE_CONCEPT_UUID };
		Boolean isTabletOrCapsule = false;
		if (drugOrder.getDrug() != null && drugOrder.getDrug().getDosageForm() != null
		        && drugOrder.getDrug().getDosageForm().getUuid() != null) {
			for (String dosageFormUuid : dosageFormUuids) {
				if (drugOrder.getDrug().getDosageForm().getUuid().equals(dosageFormUuid)) {
					isTabletOrCapsule = true;
					continue;
				}
			}
		}
		if (isTabletOrCapsule) {
			if (drugOrder.getFrequency() != null && drugOrder.getDuration() != null) {
				return drugOrder.getFrequency().getFrequencyPerDay() * drugOrder.getDuration();
			} else {
				return 0.0;
			}
		} else {
			return 0.0;
		}
	}
	
	private void determineDrugIds(DrugOrder drugOrder) {
		if (drugOrder.getDrug() != null) {
			this.drugId = drugOrder.getDrug().getDrugId();
			this.drugConceptId = drugOrder.getDrug().getConcept().getConceptId();
		} else if (drugOrder.getDrugNonCoded() != null) {
			this.drugId = null;
			this.drugConceptId = drugOrder.getConcept().getConceptId();
		}
	}
	
	public static DrugOrderSimplifier simplify(DrugOrder drugOrder) {
		return new DrugOrderSimplifier(drugOrder);
	}
}
