/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import static org.hibernate.criterion.Restrictions.eq;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Location;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.botswanaemr.api.dao.HtsLabQualityControlDao;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;

public class HtsLabQualityControlTestDaoImpl implements HtsLabQualityControlDao {
	
	DbSessionFactory sessionFactory;
	
	@Override
	public List getAll(boolean b) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}

	@Override 
	public List<LabQualityControl> getAll(Location location, boolean b) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		criteria.add(eq("location", location));
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();

	}	
	@Override
	public int getAllCount(boolean b) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list().size();
	}
	
	@Override
	public List getAll(boolean b, Integer integer, Integer integer1) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		if (b) {
			criteria.add(eq("voided", false));
		}
		return criteria.list();
	}
	
	@Override
	public BaseOpenmrsObject getById(Serializable serializable) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		criteria.add(eq("id", serializable));
		return (BaseOpenmrsObject) criteria.uniqueResult();
	}
	
	@Override
	public BaseOpenmrsObject getByUuid(String s) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		criteria.add(eq("uuid", s));
		return (BaseOpenmrsObject) criteria.uniqueResult();
	}
	
	@Override
	public void delete(BaseOpenmrsObject baseOpenmrsObject) {
		sessionFactory.getCurrentSession().delete(baseOpenmrsObject);
	}
	
	@Override
	public BaseOpenmrsObject saveOrUpdate(BaseOpenmrsObject baseOpenmrsObject) {
		sessionFactory.getCurrentSession().saveOrUpdate(baseOpenmrsObject);
		return baseOpenmrsObject;
	}
	
	@Override
	public LabQualityControlTest getByLabQualityControlTestId(Serializable id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(LabQualityControlTest.class);
		criteria.add(eq("id", id));
		return (LabQualityControlTest) criteria.uniqueResult();
	}
	
	@Override
	public List<LabQualityControl> getLabQualityControlsThisWeekByTestingPoints(List<String> testingPoints) {
		return null;
	}
}
