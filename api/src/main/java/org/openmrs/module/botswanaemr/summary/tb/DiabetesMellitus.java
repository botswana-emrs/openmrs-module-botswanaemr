/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;
import org.openmrs.module.botswanaemr.utilities.DateUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiabetesMellitus extends BaseSummary implements Summary, Serializable {
	
	private static final String DATE_OF_TEST = "29ea4ffb-f6da-4952-b02e-63cc60b3e723";
	
	private static final String BLOOD_SUGAR_RESULT = "0cd9f5b3-038b-4cea-b839-ad4b0d755758";
	
	private Encounter encounter;
	
	private String dateOfTest;
	
	private Double bloodSugarResult;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		DiabetesMellitus mellitus = new DiabetesMellitus();
		mellitus.setEncounter(encounter);
		observations.forEach(obs -> {
			if (obs.getConcept().getUuid().equals(DATE_OF_TEST)) {
				mellitus.setDateOfTest(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(BLOOD_SUGAR_RESULT)) {
				mellitus.setBloodSugarResult(getValueNumeric(obs));
			} else {
				log.debug("No available Culture & DST Report for this encounter");
			}
		});
		return mellitus;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return Arrays.asList(getConcept(DATE_OF_TEST), getConcept(BLOOD_SUGAR_RESULT));
	}
}
