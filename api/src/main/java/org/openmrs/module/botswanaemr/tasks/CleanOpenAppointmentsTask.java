/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.tasks;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.openmrs.GlobalProperty;
import org.openmrs.Visit;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.VisitService;
import org.openmrs.api.context.Context;
import org.openmrs.module.appointmentscheduling.Appointment;
import org.openmrs.module.appointmentscheduling.api.AppointmentService;
import org.openmrs.module.emrapi.adt.AdtService;
import org.openmrs.module.emrapi.visit.VisitDomainWrapper;
import org.openmrs.scheduler.tasks.AbstractTask;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CleanOpenAppointmentsTask extends AbstractTask {
	
	@Override
	public void execute() {
		AppointmentService appointmentsService = Context.getService(AppointmentService.class);
		AdministrationService administrationService = Context.getService(AdministrationService.class);
		GlobalProperty schedulerMarksMissedProperty = administrationService.getGlobalPropertyObject("SchedulerMarksMissed");
		Boolean schedulerMarksMissed = Boolean.valueOf(schedulerMarksMissedProperty.getPropertyValue());
		if (!schedulerMarksMissed) {
			return;
		}
		
		String appointmentCloserDaysInterval = administrationService.getGlobalProperty("AppointmentCloserDaysInterval", "3");
		List<Appointment.AppointmentStatus> states = new LinkedList<Appointment.AppointmentStatus>();
		//Add additional statuses to be checked i.e WAITING, WALKIN, INCONSULTATION
		states.add(Appointment.AppointmentStatus.SCHEDULED);
		Date endOfYesterday = new DateTime().withTime(23, 59, 59, 999)
		        .minusDays(Integer.parseInt(appointmentCloserDaysInterval)).toDate();
		
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
		
		List<Appointment> appointmentsInStates = appointmentsService.getAppointmentsByStatus(states);
		AdtService adtService = Context.getService(AdtService.class);
		for (Appointment appointment : appointmentsInStates) {
			// Consider appointments with the SCHEDULED status whose timeslot end_date is before today.
			if (appointment.getTimeSlot().getEndDate().before(today)) {
				Appointment.AppointmentStatus status = appointment.getStatus();
				cal.setTime(appointment.getTimeSlot().getStartDate());
				cal.add(Calendar.DATE, -Integer.parseInt(appointmentCloserDaysInterval));
				Date appointmentStartDateLesInterval = cal.getTime();
				switch (status) {
					case SCHEDULED:
						//get all visits for this patient over interval from appointment date
						List<VisitDomainWrapper> patientVisits = adtService.getVisits(appointment.getPatient(),
						    appointment.getVisit().getLocation(), appointmentStartDateLesInterval,
						    appointment.getTimeSlot().getEndDate());
						if (patientVisits.isEmpty()) {
							appointmentsService.changeAppointmentStatus(appointment, Appointment.AppointmentStatus.MISSED);
						} else {
							appointmentsService.changeAppointmentStatus(appointment,
							    Appointment.AppointmentStatus.COMPLETED);
						}
						break;
					case WAITING:
					case WALKIN:
						appointmentsService.changeAppointmentStatus(appointment, Appointment.AppointmentStatus.MISSED);
						break;
					case INCONSULTATION:
						appointmentsService.changeAppointmentStatus(appointment, Appointment.AppointmentStatus.COMPLETED);
						break;
					default:
						appointmentsInStates.remove(appointment);
						break;
				}
				
			} else {
				appointmentsInStates.remove(appointment);
			}
		}
		
	}
}
