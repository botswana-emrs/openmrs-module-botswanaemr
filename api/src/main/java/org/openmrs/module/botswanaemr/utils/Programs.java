/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.utils;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.parameter.EncounterSearchCriteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.CPT_PROGRAM_UUID;
import static org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils.getConcept;

@Slf4j
public class Programs {
	
	public static boolean eligibleForProgramEnrollment(Patient patient, EncounterSearchCriteria encounterSearchCriteria,
	        PatientProgram patientProgram, Program program) {
		boolean eligible = false;
		List<Encounter> getActiveLabEncountersForPatient = Context.getEncounterService()
		        .getEncounters(encounterSearchCriteria);
		List<Obs> obsSet = new ArrayList<>();
		if (program.equals(Context.getProgramWorkflowService().getProgramByUuid(BotswanaEmrConstants.TB_PROGRAM_UUID))) {
			if (patient != null) {
				obsSet = Context.getObsService().getObservations(Collections.singletonList(patient.getPerson()),
				    getActiveLabEncountersForPatient, tbResultsQuestions(), tbResultsAnswers(), null, null, null, null, null,
				    null, null, false);
				
				List<Obs> tbScreeningResults = BotswanaEmrUtils
				        .getTbQuestionsObservations(Collections.singletonList(patient));
				
				log.debug("TB Screening results: {}", tbScreeningResults);
				if (tbScreeningResults.size() > 0) {
					for (Obs result : tbScreeningResults) {
						//if one of the results is positive(yes), then the patient is eligible for enrollment
						if (result.getValueCoded().getConceptId().equals(BotswanaEmrConstants.YES_CONCEPT_ID)) {
							eligible = true;
							break;
						}
						
						// Check for HIV status, If +ve and under 12 years then eligible for TB enrollment
						if (result.getConcept().getUuid().equals(BotswanaEmrConstants.HIV_STATUS_QUESTION)) {
							if (result.getValueCoded().getUuid().equals(BotswanaEmrConstants.HIV_STATUS_POSITIVE)
							        && patient.getAge() < 12) {
								eligible = true;
							}
						}
					}
				}
				
				if (patientProgram == null && !eligible && obsSet.size() > 0) {
					eligible = true;
				}
			}
			
		} else if (program
		        .equals(Context.getProgramWorkflowService().getProgramByUuid(BotswanaEmrConstants.ART_PROGRAM_UUID))) {
			if (patient != null) {
				obsSet = Context.getObsService().getObservations(Collections.singletonList(patient),
				    getActiveLabEncountersForPatient, artResultsQuestions(), artResultsAnswers(), null, null, null, null,
				    null, null, null, false);
			}
			if (patientProgram == null && !obsSet.isEmpty()) {
				eligible = true;
			}
			
		} else if (program.getUuid().equals(CPT_PROGRAM_UUID)) {
			//HIV positive and enrolled in TB program
			
			//Get Active patient programs
			boolean isTBEnrolled = false;
			
			List<PatientProgram> patientPrograms = Context.getProgramWorkflowService().getPatientPrograms(patient, null,
			    null, null, null, null, false);
			for (PatientProgram programIn : patientPrograms) {
				if (programIn.getDateCompleted() == null
				        && programIn.getProgram().getUuid().equals(BotswanaEmrConstants.TB_PROGRAM_UUID)) {
					isTBEnrolled = true;
					break;
				}
			}
			
			// boolean isHIVPositive = isHivPositive(patient, getActiveLabEncountersForPatient);
			boolean isHIVPositive = BotswanaEmrUtils.isHivPositive(patient);
			
			if (patientProgram == null && isHIVPositive && isTBEnrolled) {
				eligible = true;
			}
		}
		
		return eligible;
	}
	
	private static List<Concept> cptResultsAnswers() {
		//		List<Concept> cptResultsAnswers = new ArrayList<>();
		//		cptResultsAnswers.add(getConcept(BotswanaEmrConstants.Positive));
		//		cptResultsAnswers.add(getConcept(BotswanaEmrConstants.Negative));
		return null;
	}
	
	private static List<Concept> cptResultsQuestions() {
		return Collections.singletonList(getConcept(BotswanaEmrConstants.HIV_CONFIRMATORY_TEST_CONCEPT_UUID));
	}
	
	private static List<Concept> hivConfirmatoryTestConceptAnswer() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.HIV_CONFIRMATORY_TEST_CONCEPT_UUID));
	}
	
	private static List<Concept> hivTestTypeQuestion() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.HIV_TEST_TYPE_CONCEPT_UUID));
	}
	
	public static Map<Concept, String> getTreatmentOutcomes() {
		Map<Concept, String> outcomesMap = new HashMap<Concept, String>();
		outcomesMap.put(getConcept(BotswanaEmrConstants.CURED_OUTCOME), "Cured");
		outcomesMap.put(getConcept(BotswanaEmrConstants.DIED), "Died");
		outcomesMap.put(getConcept(BotswanaEmrConstants.LTFU), "Loss to follow-up");
		outcomesMap.put(getConcept(BotswanaEmrConstants.TREATMENT_FAILURE), "Treatment Failure");
		outcomesMap.put(getConcept(BotswanaEmrConstants.COMPLETED_TREATMENT), "Completed Treatment");
		
		return outcomesMap;
	}
	
	public static Map<Concept, String> getArtTreatmentOutcomes() {
		Map<Concept, String> outcomesMap = new HashMap<Concept, String>();
		outcomesMap.put(getConcept(BotswanaEmrConstants.DIED), "Died");
		outcomesMap.put(getConcept(BotswanaEmrConstants.LTFU), "Loss to follow-up");
		outcomesMap.put(getConcept(BotswanaEmrConstants.TREATMENT_FAILURE), "Treatment Failure");
		outcomesMap.put(getConcept(BotswanaEmrConstants.TRANSFERRED_OUT), "Transfer Out");
		return outcomesMap;
	}
	
	private static List<Concept> artResultsQuestions() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.HIV_TEST_VERIFICATION_RESULT_CONCEPT_UUID),
		    getConcept(BotswanaEmrConstants.PREVIOUSLY_TESTED_QUESTION_CONCEPT_UUID));
	}
	
	private static List<Concept> artResultsAnswers() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.POSITIVE),
		    getConcept(BotswanaEmrConstants.PREVIOUSLY_TESTED_POSITIVE_CONCEPT_UUID));
	}
	
	private static List<Concept> tbResultsQuestions() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.GENE_XPERT_RESULTS),
		    getConcept(BotswanaEmrConstants.Sputum_for_acid_bacili_results),
		    getConcept(BotswanaEmrConstants.Tb_culture_results), getConcept(BotswanaEmrConstants.TB_TESTING_RESULTS));
	}
	
	public static List<Concept> tbScreeningQuestions() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.COUGH), getConcept(BotswanaEmrConstants.FEVER),
		    getConcept(BotswanaEmrConstants.WEIGHT_LOSS), getConcept(BotswanaEmrConstants.HIV_STATUS_QUESTION),
		    getConcept(BotswanaEmrConstants.NIGHT_SWEAT), getConcept(BotswanaEmrConstants.ENLARGED_LYMPH_NODES),
		    getConcept(BotswanaEmrConstants.TB_CONTACT_LAST_2_YEARS),
		    getConcept(BotswanaEmrConstants.DECREASED_PLAYFULNESS));
	}
	
	private static List<Concept> tbResultsAnswers() {
		return Arrays.asList(getConcept(BotswanaEmrConstants.Mycobacterium_tuberculosis),
		    getConcept(BotswanaEmrConstants.Trace), getConcept(BotswanaEmrConstants.Low),
		    getConcept(BotswanaEmrConstants.Medium), getConcept(BotswanaEmrConstants.High),
		    getConcept(BotswanaEmrConstants.Mycobacterium_tuberculosis_detected_with_rifampin_resistance),
		    getConcept(BotswanaEmrConstants.Mycobacterium_tuberculosis_detected_without_rifampin_resistance_misc),
		    getConcept(BotswanaEmrConstants.Mycobacterial_Infection_Excluding_Tuberculosis_and_Leprosy),
		    getConcept(BotswanaEmrConstants.Positive_one_plus), getConcept(BotswanaEmrConstants.Positive_two_plus),
		    getConcept(BotswanaEmrConstants.Positive_three_plus), getConcept(BotswanaEmrConstants.Scanty),
		    getConcept(BotswanaEmrConstants.POSITIVE), getConcept(BotswanaEmrConstants.DETECTED));
	}
	
	private static boolean isHivPositive(Patient patient, List<Encounter> getActiveLabEncountersForPatient) {
		List<Obs> obsSet = Context.getObsService().getObservations(Collections.singletonList(patient),
		    getActiveLabEncountersForPatient, artResultsQuestions(), artResultsAnswers(), null, null, null, null, null, null,
		    null, false);
		
		// Get obs for the HIV confirmatory tests
		List<Obs> hivConfirmatoryTestObs = Context.getObsService().getObservations(Collections.singletonList(patient),
		    getActiveLabEncountersForPatient, hivTestTypeQuestion(), hivConfirmatoryTestConceptAnswer(), null, null, null,
		    null, null, null, null, false);
		
		// Filter to retrieve test results for the the HIV confirmatory test
		List<Obs> tmpObsSet = new ArrayList<>();
		for (Obs obs : hivConfirmatoryTestObs) {
			tmpObsSet.addAll(
			    obsSet.stream().filter(o -> o.getObsGroup().getGroupMembers().contains(obs)).collect(Collectors.toList()));
		}
		
		obsSet = tmpObsSet;
		
		return !obsSet.isEmpty();
	}
}
