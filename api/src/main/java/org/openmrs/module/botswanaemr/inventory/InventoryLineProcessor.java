/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.inventory;

import org.apache.commons.lang.StringUtils;
import org.openmrs.Concept;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.utils.UuidGenerator;
import org.openmrs.module.botswanaemrInventory.BotswanaInventoryContext;
import org.openmrs.module.botswanaemrInventory.api.*;
import org.openmrs.module.botswanaemrInventory.model.*;
import org.openmrs.module.initializer.api.BaseLineProcessor;
import org.openmrs.module.initializer.api.CsvLine;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class InventoryLineProcessor extends BaseLineProcessor<Item> {
	
	private final IDepartmentDataService iDepartmentDataService;
	
	private final IItemAttributeDataService iItemAttributeDataService;
	
	private final IItemAttributeTypeDataService iItemAttributeTypeDataService;
	
	private final IItemDataService itemDataService;
	
	public InventoryLineProcessor() {
		iDepartmentDataService = BotswanaInventoryContext.getDepartmentDataService();
		iItemAttributeDataService = BotswanaInventoryContext.getItemAttributeDataService();
		iItemAttributeTypeDataService = BotswanaInventoryContext.getItemAttributeTypeDataService();
		itemDataService = BotswanaInventoryContext.getItemDataService();
	}
	
	@Override
	public Item fill(Item item, CsvLine line) throws IllegalArgumentException {
		//Set item code
		String itemCodeVal = line.get(InventoryConstants.HEADER_CODE);
		ItemCode itemCode = null;
		if (StringUtils.isNotEmpty(itemCodeVal) && item.getCodes() != null) {
			itemCode = item.getCodes().stream().filter(code -> code.getName().equals(itemCodeVal)).findAny().orElse(null);
			
		}
		if (itemCode == null) {
			itemCode = new ItemCode(itemCodeVal, itemCodeVal); // TODO: Save item code
			item.addCode(itemCode);
		}
		
		//Set name
		String nameVal = line.get(InventoryConstants.HEADER_NAME, true);
		if (StringUtils.isNotEmpty(nameVal)) {
			item.setName(nameVal);
		}
		
		//Set description
		String descriptionVal = line.get(InventoryConstants.HEADER_DESCRIPTION, true);
		if (StringUtils.isNotEmpty(descriptionVal)) {
			item.setDescription(descriptionVal);
		}
		
		// Set item department
		String departmentVal = line.get(InventoryConstants.HEADER_SECTION, true) != null
		        ? line.get(InventoryConstants.HEADER_SECTION, true)
		        : "N/A";
		
		Department department;
		if (StringUtils.isNotEmpty(departmentVal)) {
			Department foundDepartment = iDepartmentDataService.getAll().stream()
			        .filter(x -> x.getName().equals(departmentVal)).findFirst().orElse(null);
			if (foundDepartment != null && foundDepartment != item.getDepartment()) {
				item.setDepartment(foundDepartment);
			} else if (item.getDepartment() == null
			        || (item.getDepartment() != null && !item.getDepartment().getName().equals(departmentVal))) {
				department = new Department();
				department.setName(departmentVal);
				department.setUuid(UuidGenerator.getNextUuid());
				department.setDateCreated(new Date());
				department = iDepartmentDataService.save(department);
				item.setDepartment(department);
			}
		}
		
		// Set attributes
		String classVal = line.getString(InventoryConstants.HEADER_CLASS, "");
		String subClassVal = line.getString(InventoryConstants.HEADER_SUB_CLASS, "");
		String unitIssueVal = line.getString(InventoryConstants.HEADER_UNIT_ISSUE, "");
		String categoryVal = line.getString(InventoryConstants.HEADER_CATEGORY, "");
		String statusVal = line.getString(InventoryConstants.HEADER_STATUS, "");
		String drugVal = line.getString(InventoryConstants.HEADER_DRUG, "");
		
		Set<String> attributesTracker = new HashSet<>();
		if (item.getAttributes() != null) {
			for (ItemAttribute itemAttribute : item.getAttributes()) {
				switch (itemAttribute.getAttributeType().getUuid()) {
					case BotswanaEmrConstants.INV_ITEM_CLASS_ATTRIBUTE_TYPE_UUID:
						itemAttribute.setValue(classVal);
						attributesTracker.add("class");
						break;
					case BotswanaEmrConstants.INV_ITEM_SUB_CLASS_ATTRIBUTE_TYPE_UUID:
						itemAttribute.setValue(subClassVal);
						attributesTracker.add("subClass");
						break;
					case BotswanaEmrConstants.INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID:
						itemAttribute.setValue(unitIssueVal);
						attributesTracker.add("unitIssue");
						break;
					case BotswanaEmrConstants.INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID:
						itemAttribute.setValue(categoryVal);
						attributesTracker.add("category");
						break;
					case BotswanaEmrConstants.INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID:
						itemAttribute.setValue(statusVal);
						attributesTracker.add("status");
						break;
					case BotswanaEmrConstants.INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID:
						if (StringUtils.isNotEmpty(categoryVal) && categoryVal.equals("NON DRUG")) {
							Concept concept = Context.getConceptService().getConceptByUuid(drugVal);
							drugVal = concept != null ? concept.getName().getName() : drugVal;
						}
						itemAttribute.setValue(drugVal);
						attributesTracker.add("drug");
						// TODO : Throw exception if drug is not found in database
						break;
				}
			}
		}
		
		item = itemDataService.save(item);
		
		if (item.getId() == null) {
			throw new IllegalArgumentException("Inventory Item could not be saved");
		}
		
		if (!attributesTracker.contains("class")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_CLASS_ATTRIBUTE_TYPE_UUID);
			// TODO : Skip and throw exception if attr type is missing
			item.addAttribute(generateItemAttribute(attributeType, classVal, item));
		}
		if (!attributesTracker.contains("subClass")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_SUB_CLASS_ATTRIBUTE_TYPE_UUID);
			item.addAttribute(generateItemAttribute(attributeType, subClassVal, item));
		}
		if (!attributesTracker.contains("unitIssue")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_ISSUE_UNIT_ATTRIBUTE_TYPE_UUID);
			item.addAttribute(generateItemAttribute(attributeType, unitIssueVal, item));
		}
		if (!attributesTracker.contains("category")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_CATEGORY_ATTRIBUTE_TYPE_UUID);
			item.addAttribute(generateItemAttribute(attributeType, categoryVal, item));
		}
		if (!attributesTracker.contains("status")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_STATUS_ATTRIBUTE_TYPE_UUID);
			item.addAttribute(generateItemAttribute(attributeType, statusVal, item));
		}
		if (!attributesTracker.contains("drug")) {
			ItemAttributeType attributeType = iItemAttributeTypeDataService
			        .getByUuid(BotswanaEmrConstants.INV_ITEM_DRUG_UUID_ATTRIBUTE_TYPE_UUID);
			if (StringUtils.isNotEmpty(categoryVal) && categoryVal.equals("NON DRUG")) {
				Concept concept = Context.getConceptService().getConceptByUuid(drugVal);
				drugVal = concept != null ? concept.getName().getName() : drugVal;
			}
			item.addAttribute(generateItemAttribute(attributeType, drugVal, item));
		}
		
		return item;
	}
	
	private ItemAttribute generateItemAttribute(ItemAttributeType attributeType, String value, Item item) {
		ItemAttribute attribute = new ItemAttribute();
		attribute.setUuid(UuidGenerator.getNextUuid());
		attribute.setAttributeType(attributeType);
		attribute.setValue(value);
		attribute.setDateCreated(new Date());
		attribute.setOwner(item);
		return iItemAttributeDataService.save(attribute);
	}
}
