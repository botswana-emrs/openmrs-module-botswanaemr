/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.tb;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Obs;
import org.openmrs.module.botswanaemr.summary.Summary;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @author corneliouzbett
 */
@EqualsAndHashCode(callSuper = true)
@Slf4j
@Data
@NoArgsConstructor
public class TBEnrollmentDetails extends BaseSummary implements Summary, Serializable {
	
	private final static String COUNSELLED_CONCEPT = "73755ab4-6a3d-4110-8f6c-c636324fe623";
	
	private final static String ENROLLED_CONCEPT = "163777AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String TREATMENT_CLASSIFICATION = "161356AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String TREATMENT_GROUP = "7db20a97-032c-4b7e-a58b-4488d88944ba";
	
	private final static String PATIENT_CATEGORY = "164411AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String EXTRA_PULMONARY_SITE = "160040AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String HIV_TEST = "164844AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String HIV_TEST_DATE = "164400AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String ART_CONCEPT = "160119AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String ARTDATE_CONCEPT = "159599AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String CPT_CONCEPT = "160434AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String CPTDATE_CONCEPT = "164361AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	
	private final static String TPT_CONCEPT = "b57f205c-210c-4234-903e-1193fd7ef8e4";
	
	private final static String TPTDATE_CONCEPT = "d494045f-ee16-4cda-b1d7-06a0172b50da";
	
	private Encounter encounter;
	
	private String counselled;
	
	private String enrolled;
	
	private String TreatmentClassification;
	
	private String TreatmentGroup;
	
	private String PatientCategory;
	
	private String hivStatusTest;
	
	private String hivStatusTestDate;
	
	private String noOfScreened;
	
	private String noOfExpected;
	
	private String ART;
	
	private String CPT;
	
	private String TPT;
	
	private String ARTDate;
	
	private String CPTDate;
	
	private String TPTDate;
	
	@Override
	public Object summarize(Encounter encounter, List<Obs> observations) {
		TBEnrollmentDetails enrollmentDetails = new TBEnrollmentDetails();
		enrollmentDetails.setEncounter(encounter);
		for (Obs obs : observations) {
			if (obs.getConcept().getUuid().equals(COUNSELLED_CONCEPT)) {
				enrollmentDetails.setCounselled(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(ENROLLED_CONCEPT)) {
				enrollmentDetails.setEnrolled(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(TREATMENT_CLASSIFICATION)) {
				enrollmentDetails.setTreatmentClassification(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(PATIENT_CATEGORY)) {
				enrollmentDetails.setPatientCategory(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(TREATMENT_GROUP)) {
				enrollmentDetails.setTreatmentGroup(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(HIV_TEST)) {
				enrollmentDetails.setHivStatusTest(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(HIV_TEST_DATE)) {
				enrollmentDetails.setHivStatusTestDate(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(TPT_CONCEPT)) {
				enrollmentDetails.setTPT(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(TPTDATE_CONCEPT)) {
				enrollmentDetails.setTPTDate(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(CPT_CONCEPT)) {
				enrollmentDetails.setCPT(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(CPTDATE_CONCEPT)) {
				enrollmentDetails.setCPTDate(getValueDate(obs));
			} else if (obs.getConcept().getUuid().equals(ART_CONCEPT)) {
				enrollmentDetails.setART(getValueCoded(obs));
			} else if (obs.getConcept().getUuid().equals(ARTDATE_CONCEPT)) {
				enrollmentDetails.setARTDate(getValueDate(obs));
			} else {
				log.debug("No available results for this encounter");
			}
		}
		enrollmentDetails.setNoOfScreened("N/A");
		enrollmentDetails.setNoOfExpected("N/A");
		return enrollmentDetails;
	}
	
	@Override
	public List<Concept> getQuestions() {
		return Arrays.asList(getConcept(COUNSELLED_CONCEPT), getConcept(ENROLLED_CONCEPT), getConcept(ART_CONCEPT),
		    getConcept(ARTDATE_CONCEPT), getConcept(CPT_CONCEPT), getConcept(CPTDATE_CONCEPT), getConcept(TPT_CONCEPT),
		    getConcept(TPTDATE_CONCEPT), getConcept(HIV_TEST), getConcept(TREATMENT_CLASSIFICATION),
		    getConcept(TREATMENT_GROUP), getConcept(EXTRA_PULMONARY_SITE), getConcept(HIV_TEST_DATE),
		    getConcept(PATIENT_CATEGORY));
	}
}
