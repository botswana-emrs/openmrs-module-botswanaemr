/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.regimen;

import javax.persistence.*;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.openmrs.BaseOpenmrsMetadata;
import org.openmrs.User;

@Entity
@Table(name = "bt_regimen_line")
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegimenLine extends BaseOpenmrsMetadata {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "regimen_line_id", nullable = false)
	private Integer regimenLineId;
	
	@Column(name = "code", length = 255)
	private String code;
	
	@Column(name = "name", length = 255, nullable = false)
	private String name;
	
	@Column(name = "description", length = 1024)
	private String description;
	
	@Column(name = "uuid", unique = true, length = 38)
	private String uuid;
	
	@Column(name = "date_created", nullable = false)
	private Date dateCreated;
	
	@Column(name = "date_changed")
	private Date dateChanged;
	
	@Column(name = "date_retired")
	private Date dateRetired;
	
	@Column(name = "retired", nullable = false)
	private Boolean retired;
	
	@Column(name = "retire_reason", length = 255)
	private String retireReason;
	
	@ManyToOne
	@JoinColumn(name = "creator", nullable = false)
	private User creator;
	
	@ManyToOne
	@JoinColumn(name = "changed_by")
	private User changedBy;
	
	@ManyToOne
	@JoinColumn(name = "retired_by")
	private User retiredBy;
	
	@Override
	public Integer getId() {
		return getRegimenLineId();
	}
	
	@Override
	public void setId(Integer id) {
		setRegimenLineId(id);
	}
}
