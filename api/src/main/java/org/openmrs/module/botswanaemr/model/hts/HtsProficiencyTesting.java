/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Person;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "hts_proficiency_testing")
@BatchSize(size = 25)
@Data
public class HtsProficiencyTesting extends BaseOpenmrsData {
	
	@Column(name = "panel_id")
	private String panelId;
	
	@Column(name = "date_panel_received")
	private Date datePanelReceived;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "received_by", updatable = false)
	private Person receivedBy;
	
	@Column(name = "results_due_date")
	private Date resultsDueDate;
	
	@Column(name = "testing_point")
	private String testingPoint;
	
	@OneToMany(mappedBy = "htsProficiencyTesting")
	@ToString.Exclude
	private Set<HtsProficiencyTestingTest> htsProficiencyTestingTest = new HashSet<>();
	
	@OneToMany(mappedBy = "dtsProficiencyTesting")
	@ToString.Exclude
	private Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffer = new HashSet<>();
	
	@Column(name = "date_tested")
	private Date dateTested;
	
	@ManyToOne()
	@JoinColumn(name = "results_received_by", updatable = true)
	private Person resultsReceivedBy;
	
	@Column(name = "performance_score")
	private Double performanceScore;
	
	@Column(name = "percentage_score")
	private Double percentageScore;
	
	@Column(name = "comment")
	private String comment;
	
	@ManyToOne()
	@JoinColumn(name = "report_reviewed_by", updatable = true)
	private Person reportReviewedBy;
	
	@Column(name = "date_reviewed")
	private Date dateReviewed;
	
	@Column(name = "published")
	private Boolean published = Boolean.FALSE;
	
	@Id
	@GeneratedValue
	@Column(name = "pt_id")
	private Integer id;
	
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 41;
	}
}
