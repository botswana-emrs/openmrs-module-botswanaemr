/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

import org.hibernate.annotations.OrderBy;
import org.openmrs.User;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * A discussion between healthcare providers relating to a specific patient case.
 */
@Entity
@Table(name = "discussion")
public class Discussion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "discussion_id")
	private Integer discussionId;
	
	@Column(name = "title")
	private String title;
	
	@OneToOne
	@JoinColumn(name = "case_id", nullable = false)
	private Cases caze;
	
	@ManyToOne
	@JoinColumn(name = "started_by", nullable = false)
	private User startedBy;
	
	@Column(name = "date_started")
	private Date dateStarted;
	
	@ManyToOne
	@JoinColumn(name = "closed_by")
	private User closedBy;
	
	@Column(name = "date_closed")
	private Date dateclosed;
	
	@Column(name = "closed")
	private Boolean closed;
	
	@OneToMany(mappedBy = "discussion", cascade = CascadeType.MERGE)
	private Set<DiscussionParticipant> participants;
	
	@OneToMany(mappedBy = "discussion")
	@OrderBy(clause = "message_id ASC")
	private Set<Message> messages;
	
	public Integer getDiscussionId() {
		return discussionId;
	}
	
	public void setDiscussionId(Integer discussionId) {
		this.discussionId = discussionId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Cases getCaze() {
		return caze;
	}
	
	public void setCaze(Cases caze) {
		this.caze = caze;
	}
	
	public User getStartedBy() {
		return startedBy;
	}
	
	public void setStartedBy(User startedBy) {
		this.startedBy = startedBy;
	}
	
	public Date getDateStarted() {
		return dateStarted;
	}
	
	public void setDateStarted(Date dateStarted) {
		this.dateStarted = dateStarted;
	}
	
	public User getClosedBy() {
		return closedBy;
	}
	
	public void setClosedBy(User closedBy) {
		this.closedBy = closedBy;
	}
	
	public Date getDateclosed() {
		return dateclosed;
	}
	
	public void setDateclosed(Date dateclosed) {
		this.dateclosed = dateclosed;
	}
	
	public Boolean getClosed() {
		return closed;
	}
	
	public void setClosed(Boolean closed) {
		this.closed = closed;
	}
	
	public Set<DiscussionParticipant> getParticipants() {
		return participants;
	}
	
	public void setParticipants(Set<DiscussionParticipant> participants) {
		this.participants = participants;
	}
	
	public Set<Message> getMessages() {
		return messages;
	}
	
	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}
}
