/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Concept;
import org.openmrs.Location;
import org.openmrs.Person;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "hts_lab_quality_control")
@BatchSize(size = 25)
@Data
public class LabQualityControl extends BaseOpenmrsData {
	
	//@JoinTable(name = "person")
	//@JoinColumn(name = "person_id")
	//private Person operator;
	
	@ManyToOne
	@JoinColumn(name = "operator", updatable = false)
	private Person operator;
	
	@ManyToOne
	@JoinColumn(name = "reason_for_test", foreignKey = @ForeignKey(name = "hts_lab_quality_control_concept_FK_3"))
	private Concept reasonForTest;
	
	@OneToMany(mappedBy = "labQualityControl", cascade = CascadeType.ALL)
	@ToString.Exclude
	private Set<LabQualityTestReason> reasons = new HashSet<>();
	
	@Column(name = "date_tested")
	private Date dateTested;
	
	@ManyToOne
	@JoinColumn(name = "acceptable", foreignKey = @ForeignKey(name = "hts_lab_quality_control_concept_FK"))
	private Concept acceptable;
	
	@Column(name = "no_of_strips")
	private Integer noOfStrips;
	
	@ManyToOne
	@JoinColumn(name = "authorized_by")
	private Person authorizedBy;
	
	@Column(name = "incident")
	private String incident;
	
	@Column(name = "other_reason_for_test")
	private String otherReasonForTest;
	
	@Column(name = "corrective_action_taken")
	private String correctiveActionTaken;
	
	// @JoinColumn(name = "lqc_id")
	@OneToMany(mappedBy = "labQualityControl")
	@ToString.Exclude
	private Set<LabQualityControlTest> htsTest;
	
	@Column(name = "published")
	private Boolean published = Boolean.FALSE;
	
	@Column(name = "testing_point")
	private String testingPoint;

	// add locationId field a foreign key to location table
	@ManyToOne
	@JoinColumn(name = "location_id", foreignKey = @ForeignKey(name = "hts_lab_quality_control_location_FK"))
	private Location location;
	
	@Getter
	@Id
	@GeneratedValue
	@Column(name = "lqc_id")
	private Integer id;
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 40;
	}
}
