/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.summary.impl;

import static org.openmrs.module.botswanaemr.BotswanaEmrConstants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.Encounter;
import org.openmrs.EncounterType;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PatientProgram;
import org.openmrs.Program;
import org.openmrs.api.EncounterService;
import org.openmrs.api.ProgramWorkflowService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.LabService;
import org.openmrs.module.botswanaemr.lab.LabTest;
import org.openmrs.module.botswanaemr.model.DrugRegimen;
import org.openmrs.module.botswanaemr.summary.TbTreatmentSummary;
import org.openmrs.module.botswanaemr.summary.base.BaseTbTreatmentSummary;
import org.openmrs.module.botswanaemr.summary.tb.AdverseDrugReaction;
import org.openmrs.module.botswanaemr.summary.tb.ChestXRayResult;
import org.openmrs.module.botswanaemr.summary.tb.CultureDstReport;
import org.openmrs.module.botswanaemr.summary.tb.DiabetesMellitus;
import org.openmrs.module.botswanaemr.summary.tb.SputumLabResult;
import org.openmrs.module.botswanaemr.summary.tb.TBEnrollmentDetails;
import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.botswanaemr.utilities.DateUtils;
import org.openmrs.parameter.EncounterSearchCriteria;
import org.springframework.stereotype.Component;

@Slf4j
@Component("botswana.emr.tbTreatmentSummary")
public class TbTreatmentSummaryImpl extends BaseTbTreatmentSummary implements TbTreatmentSummary {
	
	@Override
	public List<CultureDstReport> getCultureDstReport(Patient patient) {
		List<Obs> observations = getObservations(patient, new CultureDstReport().getQuestions());
		List<CultureDstReport> cultureDstReports = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : groupByEncounter(observations).entrySet()) {
			if (entry.getKey().getEncounterType().getUuid().equals(LAB_RESULT_ENCOUNTER_TYPE_UUID)) {
				cultureDstReports.add((CultureDstReport) new CultureDstReport().summarize(entry.getKey(), entry.getValue()));
			}
		}
		return cultureDstReports;
	}
	
	@Override
	public List<DiabetesMellitus> getDiabetesScreening(Patient patient) {
		List<Obs> observations = getObservations(patient, new DiabetesMellitus().getQuestions());
		List<DiabetesMellitus> diabetesMellituses = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : groupByEncounter(observations).entrySet()) {
			diabetesMellituses.add((DiabetesMellitus) new DiabetesMellitus().summarize(entry.getKey(), entry.getValue()));
		}
		return diabetesMellituses;
	}
	
	@Override
	public List<ChestXRayResult> getChestXRayResults(Patient patient) {
		List<Obs> observations = getObservations(patient, new ChestXRayResult().getQuestions());
		List<ChestXRayResult> chestXRayResults = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : groupByEncounter(observations).entrySet()) {
			chestXRayResults.add((ChestXRayResult) new ChestXRayResult().summarize(entry.getKey(), entry.getValue()));
		}
		return chestXRayResults;
	}
	
	@Override
	public TBEnrollmentDetails getTBEnrollmentDetails(Patient patient) {
		List<Obs> observations = getObservations(patient, new TBEnrollmentDetails().getQuestions());
		List<TBEnrollmentDetails> tbEnrollmentDetails = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : groupByEncounter(observations).entrySet()) {
			tbEnrollmentDetails
			        .add((TBEnrollmentDetails) new TBEnrollmentDetails().summarize(entry.getKey(), entry.getValue()));
		}
		return tbEnrollmentDetails.isEmpty() ? new TBEnrollmentDetails() : tbEnrollmentDetails.get(0);
	}
	
	@Override
	public List<SputumLabResult> getSputumExamination(Patient patient) {
		List<LabTest> labTests = Context.getService(LabService.class).getAllCompletedLabTestOrders(patient);
		//remove all pending lab test orders
		labTests.removeIf(labTest -> labTest.getStatus().equals(BotswanaEmrConstants.labStatus.PENDING.name()));
		
		List<SputumLabResult> sputumLabResults = new ArrayList<>();
		for (LabTest labTest : labTests) {
			sputumLabResults.add(getResult(patient, labTest));
		}
		return sputumLabResults;
	}
	
	@Override
	public List<AdverseDrugReaction> getAdverseDrugReaction(Patient patient) {
		List<Obs> observations = getObservations(patient, new AdverseDrugReaction().getQuestions());
		List<AdverseDrugReaction> adverseDrugReactions = new ArrayList<>();
		for (Map.Entry<Encounter, List<Obs>> entry : groupByEncounter(observations).entrySet()) {
			if (entry.getKey().getEncounterType().getUuid().equals(ADVERSE_DRUG_REACTION_ENCOUNTER_TYPE_UUID)) {
				adverseDrugReactions
				        .add((AdverseDrugReaction) new AdverseDrugReaction().summarize(entry.getKey(), entry.getValue()));
			}
		}
		return adverseDrugReactions;
	}
	
	@Override
	public DrugRegimen getRegimen(Patient patient) {
		
		ProgramWorkflowService programWorkflowService = Context.getProgramWorkflowService();
		Program tbProgram = programWorkflowService.getProgramByUuid(TB_PROGRAM_UUID);
		PatientProgram patientProgram = programWorkflowService
		        .getPatientPrograms(patient, tbProgram, null, null, null, null, false).stream()
		        .filter(p -> p.getDateCompleted() == null).findFirst().orElse(null);
		
		Date lastActiveEnrolmentDate = null;
		if (patientProgram != null) {
			lastActiveEnrolmentDate = patientProgram.getDateEnrolled();
		}
		
		EncounterService encounterService = Context.getEncounterService();
		EncounterType enrollmentEncounterType = encounterService.getEncounterTypeByUuid(TB_ENROLMENT_ENCOUNTER_TYPE_UUID);
		EncounterSearchCriteria encounterSearchCriteria = new EncounterSearchCriteria(patient, null, lastActiveEnrolmentDate,
		        null, null, null, Collections.singletonList(enrollmentEncounterType), null, null, null, false);
		List<Encounter> tbEnrollmentEncounters = encounterService.getEncounters(encounterSearchCriteria);
		Encounter tbEnrolmentEncounter;
		Obs treatmentGroupObs;
		DrugRegimen drugRegimen = null;
		if (!tbEnrollmentEncounters.isEmpty()) {
			tbEnrollmentEncounters = BotswanaEmrUtils.sortEncountersByEncounterDatetime(tbEnrollmentEncounters);
			tbEnrolmentEncounter = tbEnrollmentEncounters.get(0);
			treatmentGroupObs = tbEnrolmentEncounter.getObs().stream()
			        .filter(o -> o.getConcept().getUuid().equals(TB_TREATMENT_GROUP_CONCEPT_UUID)).findFirst().orElse(null);
			if (treatmentGroupObs != null && treatmentGroupObs.getValueCoded().getUuid().equals(DRUG_SUSCEPTIBLE_TB_UUID)) {
				drugRegimen = new DrugRegimen(patient, patientProgram, DRUG_SUSCEPTIBLE_TB_DEFAULT_REGIMEN);
			}
		}
		
		if (drugRegimen == null) {
			drugRegimen = new DrugRegimen(patient, patientProgram, NONE);
		}
		
		return drugRegimen;
	}
	
	private SputumLabResult getResult(Patient patient, LabTest labTest) {
		//List<Obs> obsList = getObservationsByEncounterList(patient, labTest.getEncounterResults());
		SputumLabResult sputumLabResult = new SputumLabResult();
		if (labTest.getOrder().getConcept().getUuid().equals(BotswanaEmrConstants.GENE_X_PERT_CONCEPT_UUID)) {
			sputumLabResult.setTbTest("Gene-Xpert");
		} else if (labTest.getOrder().getConcept().getUuid().equals(BotswanaEmrConstants.MICROSCOPY_CONCEPT_UUID)) {
			sputumLabResult.setTbTest("Microscopy");
		}
		//the order
		for (Obs observation : getObservations(patient, labTest.getEncounter())) {
			if (observation.getConcept().getUuid().equals(BotswanaEmrConstants.AT_WHICH_MONTH_CONCEPT_UUID)) {
				if (observation.getValueCoded() != null) {
					sputumLabResult.setAtWhichMonth(observation.getValueCoded().getDisplayString());
				}
			}
		}
		//results
		List<Obs> obsList = getObservationsByEncounterList(patient, Collections.singletonList(labTest.getEncounterResult()));
		for (Obs obs : obsList) {
			//49c3c1ff-3110-4109-a656-87cfe542eab6
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.GENE_RESULTS_CONCEPT_UUID)) {
				if (obs.getValueCoded() != null) {
					sputumLabResult.setResult(obs.getValueCoded().getDisplayString());
				}
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.MTB_DETECTED_CONCEPT_UUID)) {
				if (obs.getValueCoded() != null) {
					String result = sputumLabResult.getResult() + ": " + obs.getValueCoded().getDisplayString();
					sputumLabResult.setResult(result);
				}
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.MICROSCOPY_RESULTS_CONCEPT_UUID)) {
				if (obs.getValueCoded() != null) {
					sputumLabResult.setResult(obs.getValueCoded().getDisplayString());
				}
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.MICROSCOPY_DATE_RECEIVED_CONCEPT_UUID)) {
				if (obs.getValueDate() != null) {
					sputumLabResult.setDate(DateUtils.getStringDate(obs.getValueDate(), "dd-MM-yyyy"));
				}
			}
			if (obs.getConcept().getUuid().equals(BotswanaEmrConstants.GENE_DATE_RECEIVED_CONCEPT_UUID)) {
				if (obs.getValueDate() != null) {
					sputumLabResult.setDate(DateUtils.getStringDate(obs.getValueDate(), "dd-MM-yyyy"));
				}
			}
		}
		
		return sputumLabResult;
	}
}
