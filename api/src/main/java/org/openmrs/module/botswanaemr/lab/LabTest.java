/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.lab;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.Order;
import org.openmrs.Patient;
import org.openmrs.User;

import java.io.Serializable;
import java.util.Date;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LabTest implements Serializable {
	
	private static final long serialVersionUID = 552808832594082704L;
	
	private Integer labTestId;
	
	private Lab lab;
	
	private Date acceptDate;
	
	private String sampleNumber;
	
	private Order order;
	
	private int labTestStatus;
	
	private Patient patient;
	
	private Concept concept;
	
	private User creator;
	
	private String status;
	
	private Encounter encounter;
	
	//Hibernate has a weird way of handling collections
	//private Set<Encounter> encounterResults = new HashSet<>();
	private Encounter encounterResult;
}
