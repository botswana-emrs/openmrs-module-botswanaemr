/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.reporting.queries;

import org.openmrs.module.botswanaemr.utilities.BotswanaEmrUtils;
import org.openmrs.module.reporting.common.DateUtil;

import java.util.Date;

public class DashBoardQueries {
	
	/**
	 * Get Total Registrations Completed patients
	 * 
	 * @Rturn String
	 */
	public static String getTotalRegistartionsCompleted() {
		String query = "SELECT " + "	p.patient_id FROM patient p" + "WHERE  p.voided=0";
		
		return String.format(query);
	}
	
	/**
	 * Get Total Today's Registrations
	 * 
	 * @return String
	 */
	public static String getTodaysRegistration() {
		String query = "SELECT " + "	p.patient_id FROM patient p" + "WHERE p.date_created BETWEEN '" + getStartOfDay()
		        + "'" + " AND '" + getEndOfDay() + "'";
		
		return String.format(query);
	}
	
	private static String getStartOfDay() {
		return BotswanaEmrUtils.formatDateWithTime(DateUtil.getStartOfDay(new Date()), null);
	}
	
	private static String getEndOfDay() {
		return BotswanaEmrUtils.formatDateWithTime(DateUtil.getEndOfDay(new Date()), null);
	}
}
