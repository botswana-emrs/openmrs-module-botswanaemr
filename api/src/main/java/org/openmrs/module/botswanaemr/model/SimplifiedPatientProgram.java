/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model;

public class SimplifiedPatientProgram extends SimplifiedPatient {
	
	private String programId;
	
	private String programName;
	
	private String patientUuid;
	
	private boolean isTodayDot;
	
	private Integer encounterIdentifier;
	
	private String treatmentPhase;
	
	public String getProgramId() {
		return programId;
	}
	
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	
	public String getProgramName() {
		return programName;
	}
	
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
	public String getPatientUuid() {
		return patientUuid;
	}
	
	public void setPatientUuid(String patientUuid) {
		this.patientUuid = patientUuid;
	}
	
	public boolean getIsTodayDot() {
		return isTodayDot;
	}
	
	public void setIsTodayDot(boolean todayDot) {
		isTodayDot = todayDot;
	}
	
	public Integer getEncounterIdentifier() {
		return encounterIdentifier;
	}
	
	public void setEncounterIdentifier(Integer encounterIdentifier) {
		this.encounterIdentifier = encounterIdentifier;
	}
	
	public String getTreatmentPhase() {
		return treatmentPhase;
	}
	
	public void setTreatmentPhase(String treatmentPhase) {
		this.treatmentPhase = treatmentPhase;
	}
}
