/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.openmrs.BaseOpenmrsObject;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.api.dao.HtsProficiencyTestingDao;
import org.openmrs.module.botswanaemr.model.hts.DtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;
import org.springframework.stereotype.Component;

@Component
public class HtsProficiencyTestingServiceImpl extends BaseOpenmrsService implements HtsProficiencyTestingService {
	
	public HtsProficiencyTestingDao getHtsProficiencyTestingDao() {
		return htsProficiencyTestingDao;
	}
	
	public void setHtsProficiencyTestingDao(HtsProficiencyTestingDao ProficiencyTestingDao) {
		this.htsProficiencyTestingDao = ProficiencyTestingDao;
	}
	
	private HtsProficiencyTestingDao htsProficiencyTestingDao;
	
	@Override
	public List getList() {
		return htsProficiencyTestingDao.getAll(false);
	}
	
	@Override
	public HtsProficiencyTesting getHtsProficiencyTesting(int id) {
		return (HtsProficiencyTesting) htsProficiencyTestingDao.getById(id);
	}
	
	@Override
	public BaseOpenmrsObject getById(Serializable id, String className) {
		return htsProficiencyTestingDao.getById(id, className);
	}
	
	public void saveHtsProficiencyTesting(HtsProficiencyTesting proficiencyTesting) {
		htsProficiencyTestingDao.saveOrUpdate(proficiencyTesting);
		proficiencyTesting = getHtsProficiencyTesting(proficiencyTesting.getId());
		if (!proficiencyTesting.getHtsProficiencyTestingTest().isEmpty()) {
			for (HtsProficiencyTestingTest proficiencyTestingTest : proficiencyTesting.getHtsProficiencyTestingTest()) {
				proficiencyTestingTest.setHtsProficiencyTesting(proficiencyTesting);
				htsProficiencyTestingDao.saveOrUpdate(proficiencyTestingTest);
				for (HtsSpecimenResult htsSpecimenResult : proficiencyTestingTest.getSpecimenResults()) {
					htsSpecimenResult.setHtsProficiencyTestingTest(proficiencyTestingTest);
					htsProficiencyTestingDao.saveOrUpdate(htsSpecimenResult);
					for (HtsTestResult htsTestResult : htsSpecimenResult.getResults()) {
						htsTestResult.setHtsSpecimenResult(htsSpecimenResult);
						htsProficiencyTestingDao.saveOrUpdate(htsTestResult);
					}
				}
			}
		}
		if (!proficiencyTesting.getDtsProficiencyTestingBuffer().isEmpty()) {
			for (DtsProficiencyTestingBuffer proficiencyTestingBuffer : proficiencyTesting
			        .getDtsProficiencyTestingBuffer()) {
				proficiencyTestingBuffer.setDtsProficiencyTesting(proficiencyTesting);
				htsProficiencyTestingDao.saveOrUpdate(proficiencyTestingBuffer);
			}
		}
	}
	
	@Override
	public void updateHtsProficiencyTesting(HtsProficiencyTesting ProficiencyTesting) {
		htsProficiencyTestingDao.saveOrUpdate(ProficiencyTesting);
	}
	
	@Override
	public void voidHtsProficiencyTesting(HtsProficiencyTesting ProficiencyTesting) {
		htsProficiencyTestingDao.delete(ProficiencyTesting);
		
	}
	
	@Override
	public List<HtsProficiencyTestingTest> getHtsProficiencyTestingTests(HtsProficiencyTesting ProficiencyTesting) {
		return htsProficiencyTestingDao.getAll(false);
	}
	
	@Override
	public HtsProficiencyTestingTest saveOrUpdateHtsProficiencyTestingTest(
	        HtsProficiencyTestingTest ProficiencyTestingTest) {
		return (HtsProficiencyTestingTest) htsProficiencyTestingDao.saveOrUpdate(ProficiencyTestingTest);
	}
	
	@Override
	public void voidHtsProficiencyTestingTest(HtsProficiencyTestingTest htsProficiencyTestingTest) {
		htsProficiencyTestingDao.delete(htsProficiencyTestingTest);
	}
	
	@Override
	public void addHtsProficiencyTestingTests(Set<HtsProficiencyTestingTest> htsProficiencyTestingTestSet) {
	}
	
	@Override
	public List<DtsProficiencyTestingBuffer> getDtsProficiencyTestingBuffer(HtsProficiencyTesting proficiencyTesting) {
		return htsProficiencyTestingDao.getAll(false);
	}
	
	@Override
	public DtsProficiencyTestingBuffer saveOrUpdateDtsProficiencyTestingBuffer(
	        DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer) {
		return (DtsProficiencyTestingBuffer) htsProficiencyTestingDao.saveOrUpdate(dtsProficiencyTestingBuffer);
	}
	
	@Override
	public void voidDtsProficiencyTestingBuffer(DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer) {
		htsProficiencyTestingDao.delete(dtsProficiencyTestingBuffer);
	}
	
	@Override
	public void addDtsProficiencyTestingBuffer(Set<DtsProficiencyTestingBuffer> dtsProficiencyTestingBuffers) {
	}
}
