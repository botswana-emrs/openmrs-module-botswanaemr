/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.model.hts;

import lombok.Data;
import org.openmrs.BaseOpenmrsData;
import org.openmrs.Concept;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hts_lab_quality_test_reasons")
@Data
public class LabQualityTestReason extends BaseOpenmrsData {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "lab_quality_control_id")
	private LabQualityControl labQualityControl;
	
	@ManyToOne
	@JoinColumn(name = "concept_id", foreignKey = @ForeignKey(name = "FK_lab_quality_test_reasons_concept_id"))
	private Concept concept;
	
	// Constructors, getters, and setters
	public Integer getId() {
		return this.id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return 45;
	}
}
