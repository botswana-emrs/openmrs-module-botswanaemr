/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.tasks;

import lombok.extern.slf4j.Slf4j;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.api.BotswanaEmrService;
import org.openmrs.module.patientqueueing.api.PatientQueueingService;
import org.openmrs.scheduler.tasks.AbstractTask;

@Slf4j
public class AutoCompletePatientQueuesTask extends AbstractTask {
	
	@Override
	public void execute() {
		BotswanaEmrService botswanaEmrService = Context.getService(BotswanaEmrService.class);
		PatientQueueingService patientQueueingService = Context.getService(PatientQueueingService.class);
		if (!isExecuting()) {
			log.debug("Starting auto complete patient queues task...");
			startExecuting();
			try {
				botswanaEmrService.getIncompletePatientQueues().forEach(patientQueueingService::completePatientQueue);
			}
			catch (Exception e) {
				log.error("Error while auto completing patient queues:", e);
			}
			finally {
				stopExecuting();
			}
		}
	}
}
