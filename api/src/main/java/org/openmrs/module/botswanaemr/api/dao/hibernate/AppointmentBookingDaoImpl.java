/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import org.hibernate.Criteria;
import org.openmrs.Location;
import org.openmrs.api.db.hibernate.DbSessionFactory;
import org.openmrs.module.botswanaemr.api.dao.AppointmentBookingDao;
import org.openmrs.module.botswanaemr.model.appointment.AppointmentBookingLimit;

import java.io.Serializable;
import java.util.List;

import static org.hibernate.criterion.Restrictions.eq;

public class AppointmentBookingDaoImpl implements AppointmentBookingDao {
	
	DbSessionFactory dbSessionFactory;
	
	public AppointmentBookingDaoImpl() {
	}
	
	public DbSessionFactory getDbSessionFactory() {
		return dbSessionFactory;
	}
	
	public void setDbSessionFactory(DbSessionFactory dbSessionFactory) {
		this.dbSessionFactory = dbSessionFactory;
	}
	
	@Override
	public List getAll(boolean includeVoided) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("voided", includeVoided));
		return criteria.list();
	}
	
	@Override
	public int getAllCount(boolean includeVoided) {
		return getAll(includeVoided).size();
	}
	
	@Override
	public List getAll(boolean includeVoided, Integer firstResult, Integer maxResults) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("voided", includeVoided));
		return criteria.list();
	}
	
	@Override
	public AppointmentBookingLimit getById(Serializable id) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("id", id));
		return (AppointmentBookingLimit) criteria.uniqueResult();
	}
	
	@Override
	public AppointmentBookingLimit getByUuid(String uuid) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("uuid", uuid));
		return (AppointmentBookingLimit) criteria.uniqueResult();
	}
	
	@Override
	public void delete(AppointmentBookingLimit persistent) {
		dbSessionFactory.getCurrentSession().delete(persistent);
		
	}
	
	@Override
	public AppointmentBookingLimit saveOrUpdate(AppointmentBookingLimit newOrPersisted) {
		dbSessionFactory.getCurrentSession().saveOrUpdate(newOrPersisted);
		return newOrPersisted;
	}
	
	@Override
	public AppointmentBookingLimit getAppointmentBookingLimit(Location location, String day) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("location", location));
		criteria.add(eq("day", day));
		return (AppointmentBookingLimit) criteria.uniqueResult();
	}
	
	@Override
	public List<AppointmentBookingLimit> getAppointmentBookingLimits(Location location) {
		Criteria criteria = dbSessionFactory.getCurrentSession().createCriteria(AppointmentBookingLimit.class);
		criteria.add(eq("location", location));
		return criteria.list();
	}
}
