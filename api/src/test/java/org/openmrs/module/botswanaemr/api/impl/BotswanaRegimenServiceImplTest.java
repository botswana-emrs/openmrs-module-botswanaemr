/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.impl;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openmrs.Concept;
import org.openmrs.ConceptName;
import org.openmrs.Drug;
import org.openmrs.api.db.ConceptDAO;
import org.openmrs.api.impl.ConceptServiceImpl;
import org.openmrs.module.botswanaemr.api.dao.BotswanaRegimenDao;
import org.openmrs.module.botswanaemr.model.regimen.Regimen;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponent;
import org.openmrs.module.botswanaemr.model.regimen.RegimenComponentDrug;
import org.openmrs.module.botswanaemr.model.regimen.RegimenLine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BotswanaRegimenServiceImplTest {
	
	@Mock
	private BotswanaRegimenDao botswanaRegimenDao;
	
	@InjectMocks
	private BotswanaRegimenServiceImpl botswanaRegimenService;
	
	@Mock
	private ConceptDAO conceptDao;
	
	@InjectMocks
	private ConceptServiceImpl conceptService;
	
	@BeforeEach
	public void setup() {
		Concept concept1 = new Concept();
		concept1.setConceptId(1);
		concept1.addName(new ConceptName("ABC", Locale.ENGLISH));
		lenient().when(conceptService.getConcept(1)).thenReturn(concept1);
		
		Concept concept2 = new Concept();
		concept2.setConceptId(2);
		concept2.addName(new ConceptName("3TC", Locale.ENGLISH));
		lenient().when(conceptService.getConcept(2)).thenReturn(concept2);
		
		Concept concept3 = new Concept();
		concept3.setConceptId(3);
		concept3.addName(new ConceptName("DTG", Locale.ENGLISH));
		lenient().when(conceptService.getConcept(3)).thenReturn(concept3);
		
		Concept concept4 = new Concept();
		concept4.setConceptId(4);
		concept4.addName(new ConceptName("ABC/3TC/DTG", Locale.ENGLISH));
		lenient().when(conceptService.getConcept(4)).thenReturn(concept4);
		
		Regimen regimen1 = new Regimen(1);
		regimen1.setConceptRef(concept4);
		regimen1.setRegimenName("ABC/3TC/DTG");
		lenient().when(botswanaRegimenDao.getRegimen(1)).thenReturn(regimen1);
		
		// when(botswanaRegimenService.getBotswanaRegimenDao()).thenReturn(botswanaRegimenDao);
		
	}
	
	@Ignore
	@Test
	public void getAllRegimenLineReturnsExpectedList() {
		List<RegimenLine> expectedList = Collections.singletonList(new RegimenLine());
		when(botswanaRegimenDao.getAllRegimenLine()).thenReturn(expectedList);
		
		List<RegimenLine> result = botswanaRegimenService.getAllRegimenLine();
		
		assertEquals(expectedList, result);
	}
	
	@Ignore
	@Test
	public void getRegimenLineReturnsExpectedRegimenLine() {
		RegimenLine expectedRegimenLine = new RegimenLine();
		when(botswanaRegimenDao.getRegimenLine(anyInt())).thenReturn(expectedRegimenLine);
		
		RegimenLine result = botswanaRegimenService.getRegimenLine(1);
		
		assertEquals(expectedRegimenLine, result);
	}
	
	@Ignore
	@Test
	public void saveRegimenLineReturnsExpectedRegimenLine() {
		RegimenLine expectedRegimenLine = new RegimenLine();
		when(botswanaRegimenDao.saveRegimenLine(any(RegimenLine.class))).thenReturn(expectedRegimenLine);
		
		RegimenLine result = botswanaRegimenService.saveRegimenLine(new RegimenLine());
		
		assertEquals(expectedRegimenLine, result);
	}
	
	@Ignore
	@Test
	public void getAllRegimenReturnsExpectedList() {
		List<Regimen> expectedList = Collections.singletonList(new Regimen());
		when(botswanaRegimenDao.getAllRegimen(anyBoolean())).thenReturn(expectedList);
		
		List<Regimen> result = botswanaRegimenService.getAllRegimen(true);
		
		assertEquals(expectedList, result);
	}
	
	@Ignore
	@Test
	public void saveRegimenComponentWithDrugsReturnsExpectedRegimenComponent() {
		
		// Create a RegimenComponent and add the 3 RegimenComponentDrugs
		RegimenComponent expectedRegimenComponent = new RegimenComponent();
		expectedRegimenComponent.setRegimen(new Regimen());
		expectedRegimenComponent.setComponentLabel("ABC + 3TC + DTG");
		expectedRegimenComponent.setRegimenComponentDrugs(getRegimenDrugComponents());
		
		// Mock the saveRegimenComponent method to return the expectedRegimenComponent
		when(botswanaRegimenDao.saveRegimenComponent(any(RegimenComponent.class))).thenReturn(expectedRegimenComponent);
		
		// Call the saveRegimenComponent method
		RegimenComponent result = botswanaRegimenService.saveRegimenComponent(new RegimenComponent());
		
		// Assert that the returned RegimenComponent is the expectedRegimenComponent
		assertEquals(expectedRegimenComponent, result);
		
		// Assert that the returned RegimenComponent has 3 RegimenComponentDrugs
		assertEquals(3, result.getRegimenComponentDrugs().size());
	}
	
	private List<RegimenComponentDrug> getRegimenDrugComponents() {
		Drug drug1 = new Drug();
		Concept concept1 = conceptService.getConcept(1);
		drug1.setConcept(concept1);
		RegimenComponentDrug regimenComponentDrug1 = new RegimenComponentDrug();
		regimenComponentDrug1.setId(1);
		regimenComponentDrug1.setUuid("12345678-1234-1234-1234-123456789012");
		Regimen regimen = botswanaRegimenService.getRegimen(1);
		regimenComponentDrug1.setRegimen(regimen);
		regimenComponentDrug1.setDrug(drug1);
		regimenComponentDrug1.setDose(300.00);
		regimenComponentDrug1.setUnits(new Concept());
		
		Drug drug2 = new Drug();
		Concept concept2 = conceptService.getConcept(2);
		drug2.setConcept(concept2);
		RegimenComponentDrug regimenComponentDrug2 = new RegimenComponentDrug();
		regimenComponentDrug2.setId(2);
		regimenComponentDrug2.setUuid("12345678-1234-1234-1234-123456789013");
		regimenComponentDrug2.setRegimen(regimen);
		regimenComponentDrug2.setDrug(drug2);
		regimenComponentDrug2.setDose(150.00);
		regimenComponentDrug2.setUnits(new Concept());
		
		Drug drug3 = new Drug();
		Concept concept3 = conceptService.getConcept(3);
		drug3.setConcept(concept3);
		RegimenComponentDrug regimenComponentDrug3 = new RegimenComponentDrug();
		regimenComponentDrug3.setId(3);
		regimenComponentDrug3.setUuid("12345678-1234-1234-1234-123456789014");
		regimenComponentDrug3.setRegimen(regimen);
		regimenComponentDrug3.setDrug(drug3);
		regimenComponentDrug3.setDose(50.00);
		regimenComponentDrug3.setUnits(new Concept());
		
		return Arrays.asList(regimenComponentDrug1, regimenComponentDrug2, regimenComponentDrug3);
		
	}
	
	@Ignore
	@Test
	public void getRegimenComponentByIdReturnsExpectedRegimenComponent() {
		// Create a RegimenComponent
		RegimenComponent expectedRegimenComponent = new RegimenComponent();
		
		// Mock the getRegimenComponentById method to return the expectedRegimenComponent
		when(botswanaRegimenDao.getRegimenComponentById(anyInt())).thenReturn(expectedRegimenComponent);
		
		// Call the getRegimenComponentById method
		RegimenComponent result = botswanaRegimenService.getRegimenComponentById(1);
		
		// Assert that the returned RegimenComponent is the expectedRegimenComponent
		assertEquals(expectedRegimenComponent, result);
	}
	
	@Ignore
	@Test
	public void getRegimenComponentDrugsByRegimenComponentReturnsExpectedList() {
		RegimenComponent regimenComponent = new RegimenComponent();
		RegimenComponentDrug regimenComponentDrug = new RegimenComponentDrug();
		regimenComponentDrug.setRegimenComponent(regimenComponent);
		List<RegimenComponentDrug> expectedList = Collections.singletonList(regimenComponentDrug);
		when(botswanaRegimenDao.getRegimenComponentDrugByRegimenComponent(regimenComponent)).thenReturn(expectedList);
		
		List<RegimenComponentDrug> result = botswanaRegimenService
		        .getRegimenComponentDrugsByRegimenComponent(regimenComponent);
		
		assertEquals(expectedList, result);
	}
	
	@Test
	public void getRegimenComponentDrugsByRegimenComponentReturnsEmptyListWhenNoDrugs() {
		RegimenComponent regimenComponent = new RegimenComponent();
		List<RegimenComponentDrug> expectedList = Collections.emptyList();
		when(botswanaRegimenDao.getRegimenComponentDrugByRegimenComponent(regimenComponent)).thenReturn(expectedList);
		
		List<RegimenComponentDrug> result = botswanaRegimenService
		        .getRegimenComponentDrugsByRegimenComponent(regimenComponent);
		
		assertEquals(expectedList, result);
	}
	
	@Test
	public void getRegimenComponentDrugsByRegimenConceptReturnsExpectedList() {
		List<RegimenComponentDrug> expectedList = getRegimenDrugComponents();
		when(botswanaRegimenDao.getRegimenComponentDrugByRegimenConcept(any())).thenReturn(expectedList);
		
		Concept concept = conceptService.getConcept(4);
		List<RegimenComponentDrug> result = botswanaRegimenService.getRegimenComponentDrugsByRegimenConcept(concept);
		
		assertEquals(expectedList, result);
		assertThat(result, hasSize(3));
	}
}
