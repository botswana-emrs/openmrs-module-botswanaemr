/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openmrs.Concept;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsLabControlService;
import org.openmrs.module.botswanaemr.api.impl.HtsLabQualityControlServiceImpl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControl;
import org.openmrs.module.botswanaemr.model.hts.LabQualityControlTest;
import org.openmrs.test.BaseModuleContextSensitiveTest;

@Ignore
public class HtsLabQualityControlDaoImplTest extends BaseModuleContextSensitiveTest {
	
	private String UUID;
	
	private HtsLabControlService service;
	
	@Override
	public Boolean useInMemoryDatabase() {
		return true;
	}
	
	@Before
	public void start() throws SQLException {
		initializeInMemoryDatabase();
		UUID = "0CE60E16-61D8-4DC5-BD4A-2DF0245CFC4E";
		service = Context.getService(HtsLabControlService.class);
	}
	
	@After
	public void stop() {
		
	}
	
	@Test
	public void saveOrUpdate_shouldSaveLabQualityControlEntry() {
		LabQualityControl labQualityControl = new LabQualityControl();
		User operator = Context.getService(UserService.class).getUser(1);
		Concept positive = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.POSITIVE);
		Concept pass = Context.getConceptService().getConceptByUuid("2c6c9444-280b-48b3-9fc6-4872fd95fcb8");
		// Concept failed = Context.getConceptService().getConceptByUuid("583bc8fc-3de9-4988-b271-ee8d0e14c2ce");
		
		labQualityControl.setUuid(UUID);
		labQualityControl.setId(1);
		labQualityControl.setOperator(operator.getPerson());
		labQualityControl.setDateTested(new Date());
		labQualityControl.setAcceptable(pass);
		labQualityControl.setAuthorizedBy(operator.getPerson());
		labQualityControl.setIncident("None");
		labQualityControl.setCorrectiveActionTaken("None");
		labQualityControl.setCreator(Context.getAuthenticatedUser());
		labQualityControl.setDateCreated(new Date());
		
		LabQualityControlTest labQualityControlTest = new LabQualityControlTest();
		labQualityControlTest.setId(1);
		labQualityControlTest.setUuid("6039EC0D-4E4C-4BAB-8839-17316E8575F7");
		labQualityControlTest.setExpiryDate(new Date());
		labQualityControlTest.setLotNumber("Lot 1");
		labQualityControlTest.setTypeOfTest(BotswanaEmrConstants.typeOfHtsTest.TEST_1.toString());
		
		labQualityControl.setHtsTest(Collections.singleton(labQualityControlTest));
		service.saveLabQualityControl(labQualityControl);
		
		List<LabQualityControl> labQualityControlList = service.getList();
		
		Context.flushSession();
		
		Assert.assertEquals(1, labQualityControlList.size());
		
		service.saveLabQualityControl(labQualityControl);
		
		LabQualityControl labQualityControl1 = service.getLabQualityControl(labQualityControl.getId());
		
		Assert.assertEquals(labQualityControl1.getReasonForTest(), "Updated Reason");
		Assert.assertEquals(1, labQualityControl1.getHtsTest().size());
		
	}
}
