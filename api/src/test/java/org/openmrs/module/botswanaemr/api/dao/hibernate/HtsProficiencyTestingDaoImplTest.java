/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.hibernate;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openmrs.Concept;
import org.openmrs.User;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.module.botswanaemr.BotswanaEmrConstants;
import org.openmrs.module.botswanaemr.api.HtsProficiencyTestingService;
import org.openmrs.module.botswanaemr.model.hts.DtsProficiencyTestingBuffer;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTesting;
import org.openmrs.module.botswanaemr.model.hts.HtsProficiencyTestingTest;
import org.openmrs.module.botswanaemr.model.hts.HtsSpecimenResult;
import org.openmrs.module.botswanaemr.model.hts.HtsTestResult;
import org.openmrs.test.BaseModuleContextSensitiveTest;
import org.springframework.transaction.annotation.Transactional;

@Ignore
public class HtsProficiencyTestingDaoImplTest extends BaseModuleContextSensitiveTest {
	
	private String UUID;
	
	private HtsProficiencyTestingService service;
	
	@Override
	public Boolean useInMemoryDatabase() {
		return true;
	}
	
	@Before
	public void start() throws SQLException {
		initializeInMemoryDatabase();
		UUID = "0CE60E16-61D8-4DC5-BD4A-2DF0245CFC4E";
		service = Context.getService(HtsProficiencyTestingService.class);
	}
	
	@After
	public void stop() {
		
	}
	
	@Transactional
	@Test
	public void saveOrUpdate_shouldSaveHtsProficiencyTestingEntry() {
		HtsProficiencyTesting htsProficiencyTesting = new HtsProficiencyTesting();
		User operator = Context.getService(UserService.class).getUser(1);
		Concept positive = Context.getConceptService().getConceptByUuid(BotswanaEmrConstants.POSITIVE);
		Concept pass = Context.getConceptService().getConceptByUuid("2c6c9444-280b-48b3-9fc6-4872fd95fcb8");
		// Concept failed = Context.getConceptService().getConceptByUuid("583bc8fc-3de9-4988-b271-ee8d0e14c2ce");
		
		htsProficiencyTesting.setUuid(UUID);
		htsProficiencyTesting.setId(1);
		htsProficiencyTesting.setTestingPoint("testing point");
		htsProficiencyTesting.setPanelId("12345");
		htsProficiencyTesting.setDatePanelReceived(new Date());
		htsProficiencyTesting.setReceivedBy(operator.getPerson());
		htsProficiencyTesting.setCreator(Context.getAuthenticatedUser());
		htsProficiencyTesting.setDateCreated(new Date());
		
		HtsProficiencyTestingTest htsProficiencyTestingTest = new HtsProficiencyTestingTest();
		htsProficiencyTestingTest.setId(1);
		htsProficiencyTestingTest.setUuid("6039EC0D-4E4C-4BAB-8839-17316E8575F7");
		htsProficiencyTestingTest.setExpiryDate(new Date());
		htsProficiencyTestingTest.setLotNumber("Lot 1");
		
		DtsProficiencyTestingBuffer dtsProficiencyTestingBuffer = new DtsProficiencyTestingBuffer();
		dtsProficiencyTestingBuffer.setId(1);
		dtsProficiencyTestingBuffer.setUuid("107D0C79-742D-4883-902D-EEBAFF88756A");
		dtsProficiencyTestingBuffer.setDtsExpiryDate(new Date());
		dtsProficiencyTestingBuffer.setDtsLotNumber("Lot 2");
		
		HtsSpecimenResult specimenResult = new HtsSpecimenResult();
		specimenResult.setId(1);
		specimenResult.setInterpretation(BotswanaEmrConstants.POSITIVE);
		specimenResult.setComment("Conclusive test");
		//		specimenResult.setHtsProficiencyTestingTest(htsProficiencyTestingTest);
		htsProficiencyTestingTest.getSpecimenResults().add(specimenResult);
		
		HtsTestResult htsTestResult = new HtsTestResult();
		htsTestResult.setId(1);
		htsTestResult.setResultNumber("1");
		htsTestResult.setKitName("c2ce43cc-0345-40c0-91b2-24155f458d4e");
		htsTestResult.setLotNumber("45");
		htsTestResult.setExpiryDate(new Date());
		htsTestResult.setKitResults("1228AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		//		htsTestResult.setHtsSpecimenResult(specimenResult);
		specimenResult.getResults().add(htsTestResult);
		
		HtsTestResult htsTestResult2 = new HtsTestResult();
		htsTestResult2.setId(2);
		htsTestResult2.setResultNumber("2");
		htsTestResult2.setKitName("166453AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		htsTestResult2.setLotNumber("90");
		htsTestResult2.setExpiryDate(new Date());
		htsTestResult2.setKitResults("1229AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		//		htsTestResult2.setHtsSpecimenResult(specimenResult);
		specimenResult.getResults().add(htsTestResult2);
		
		htsProficiencyTesting.setHtsProficiencyTestingTest(Collections.singleton(htsProficiencyTestingTest));
		htsProficiencyTesting.setDtsProficiencyTestingBuffer(Collections.singleton(dtsProficiencyTestingBuffer));
		
		service.saveHtsProficiencyTesting(htsProficiencyTesting);
		
		List<HtsProficiencyTesting> htsProficiencyTestingList = service.getList();
		
		Context.flushSession();
		
		Assert.assertEquals(1, htsProficiencyTestingList.size());
		
		htsProficiencyTesting.setTestingPoint("Updated Testing Point");
		
		service.saveHtsProficiencyTesting(htsProficiencyTesting);
		
		HtsProficiencyTesting htsProficiencyTesting1 = service.getHtsProficiencyTesting(htsProficiencyTesting.getId());
		
		Assert.assertEquals(htsProficiencyTesting1.getTestingPoint(), "Updated Testing Point");
		Assert.assertEquals(1, htsProficiencyTesting1.getHtsProficiencyTestingTest().size());
		Assert.assertEquals(1, htsProficiencyTesting1.getDtsProficiencyTestingBuffer().size());
		
		Set<HtsProficiencyTestingTest> htsProficiencyTestingTestSet = htsProficiencyTesting1.getHtsProficiencyTestingTest();
		Assert.assertEquals(1, htsProficiencyTestingTestSet.size());
		
		Set<HtsSpecimenResult> specimenResultSet = htsProficiencyTestingTestSet.stream().findFirst().get()
		        .getSpecimenResults();
		Assert.assertEquals(1, specimenResultSet.size());
		Assert.assertEquals(BotswanaEmrConstants.POSITIVE, specimenResultSet.stream().findFirst().get().getInterpretation());
		
		Set<HtsTestResult> testResultSet = specimenResultSet.stream().findFirst().get().getResults();
		Assert.assertEquals(2, testResultSet.size());
	}
}
