/*
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/. OpenMRS is also distributed under
 * the terms of the Healthcare Disclaimer located at http://openmrs.org/license.
 *
 * Copyright (C) OpenMRS Inc. OpenMRS is a registered trademark and the OpenMRS
 * graphic logo is a trademark of OpenMRS Inc.
 */
package org.openmrs.module.botswanaemr.api.dao.oracle;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openmrs.module.botswanaemr.api.BotswanaPersonRegistry.BotswanaRegistryPerson;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringJUnit4ClassRunner.class)
public class BotswanaPersonRegistryDaoImpTest {

    private static final String PERSON_ID = "235262727";


    @Test
    @Ignore
    //Test makes a network request
    public void testGetPersonsById() throws SQLException, ParseException {
        BotswanaPersonRegistryDaoImpl botswanaPersonRegistryDao = new BotswanaPersonRegistryDaoImpl();
        List<BotswanaRegistryPerson> results = botswanaPersonRegistryDao.getPersonsById(PERSON_ID);

        assertThat(results, hasSize(1));
    }
}
