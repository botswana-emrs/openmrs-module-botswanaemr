FROM maven:3.6.2-jdk-8-slim

WORKDIR /app

COPY . .

RUN mvn clean install
