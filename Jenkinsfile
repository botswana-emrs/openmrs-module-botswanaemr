pipeline {
    agent any
    
    triggers{
        cron('53 20 * * *')
    }
    tools{
        jdk 'OpenJDK8'
        maven 'Maven3'   
    }
    environment {
        DOCKER_IMAGE='botswanaemr-web-dev'
        VERSION='latest'
        SERVER_USERNAME = credentials('username-server-id')
        SERVER_HOST = credentials('demo-server-host-id')
        SSH_COMMAND = credentials('ssh-command-id')
        REMOTE_DIR = '/opt'
        SOURCE_PATH = '/var/lib/jenkins/workspace/Botswana_Emr_BitBucket'
        SOURCE_PATH_OMOD = '/var/lib/jenkins/workspace/Botswana_Emr_BitBucket/omod/target/*.omod'
        SONAR_HOME = tool 'sonar-scanner'
        PROJECT_NAME = 'Botswana-EMR'
    }
    stages {
        stage('Clean Workspace') {
            steps {
                cleanWs()
            }
        }
        stage('Checkout') {
            steps {
                // Checkout code from SCM
                checkout scm
            }
        }
        stage('Build package') {
            steps {
                sh 'mvn -B -DskipTest clean package'
            }
        }
        
        stage('Run Tests') {
            steps {
                sh 'mvn test'
            }
        }

        stage('Docker Build & push'){
            steps {
                script {
                        withDockerRegistry(credentialsId: 'e8d9143f-49eb-44eb-a7bc-5171c7ff1758', toolName: 'Docker') 
                        {
                            sh '''
                             mvn openmrs-sdk:build-distro -DdbSql=db/initial_db.sql -Ddir=docker
                             cd docker/web && docker build -t $DOCKER_IMAGE -f Dockerfile . 
                             docker tag $DOCKER_IMAGE intellisoftdev/$DOCKER_IMAGE:$VERSION
                             docker push intellisoftdev/$DOCKER_IMAGE:$VERSION
                            '''
                        }
                }
            }
        }

        stage('SonarCube Analysis') {
            steps {
                withSonarQubeEnv('sonar-server') {
                    sh '''
                    JAVA_HOME=/usr/lib/jvm/temurin-17-jdk-amd64
                    export JAVA_HOME
                    java -version
                    /var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation/sonar-scanner/bin/sonar-scanner -Dsonar.projectName=$PROJECT_NAME -Dsonar.projectKey=$PROJECT_NAME\
                    -Dsonar.java.binaries=. \
                    '''
                }
            }
        }

        stage('Deploy: copy module and restart demo-server') {
            steps {
                sshagent(['ssh-credentials-id']) {
                    sh '''
                    ls
                    zip -r configuration.zip configuration
                    scp $SSH_COMMAND $SOURCE_PATH/configuration.zip $SERVER_USERNAME@$SERVER_HOST:$REMOTE_DIR/copy_file
                    ssh $SSH_COMMAND $SERVER_USERNAME@$SERVER_HOST "cd $REMOTE_DIR/copy_file && ls "
                    ssh $SSH_COMMAND $SERVER_USERNAME@$SERVER_HOST "cd $REMOTE_DIR/copy_file && sudo mv configuration.zip $REMOTE_DIR/docker-emr/web/"
                    ssh $SSH_COMMAND $SERVER_USERNAME@$SERVER_HOST "cd /opt/docker-emr/web/ && sudo unzip -o configuration.zip |sudo docker restart bemrs_db |sudo docker restart bemrs_web"
                    '''
                }
            }
        }

    }
    post {
        always {
            cleanWs()
        }
    }
}
