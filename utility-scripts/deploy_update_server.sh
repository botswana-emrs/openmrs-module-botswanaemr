#!/bin/bash

## Server

cd /opt/openmrs-docker/banda_openmrs_volume_master # working direectory

zip -r modules.bkp.zip modules/
rm -rf modules
unzip modules.zip 

zip -r configuration.bkp.zip configuration/
unzip configuration.zip 

docker cp openmrs-2.5.0.war banda_openmrs_master:/usr/local/tomcat/webapps/openmrs.war

docker restart banda_openmrs_master