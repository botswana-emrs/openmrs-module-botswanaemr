#!/bin/bash

## local machine

SERVER_ROOT="root@botswanaemrdemo.intellisoftkenya.com"

cd ~/openmrs/refapp250 # Working directory

zip -r modules.zip modules/
scp modules.zip $SERVER_ROOT:/opt/openmrs-docker/banda_openmrs_volume_master

zip -r configuration.zip configuration/
scp configuration.zip $SERVER_ROOT:/opt/openmrs-docker/banda_openmrs_volume_master/

cd ~/openmrs/refapp250 # Working directory
scp openmrs-2.5.0.war $SERVER_ROOT:/opt/openmrs-docker/banda_openmrs_volume_master