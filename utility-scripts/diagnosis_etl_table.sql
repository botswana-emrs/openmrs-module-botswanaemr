-- Step 1: Create the Stored Procedure
-- Encapsulate the script in a stored procedure called update_diagnosis_etl.

DELIMITER $$

USE analysis_db$$

CREATE PROCEDURE update_diagnosis_etl()
BEGIN
    DROP TABLE IF EXISTS diagnosis_etl;
    CREATE TABLE diagnosis_etl
    (
        diagnosis_id           INT           NOT NULL UNIQUE PRIMARY KEY,
        encounter_id           INT           NOT NULL,
        patient_id             INT           NOT NULL,
        diagnosis_non_coded    VARCHAR(150)  NOT NULL,
        encounter_location     INT           NOT NULL,
        diagnosis_state        VARCHAR(15)   NOT NULL,
        encounter_date         DATETIME      NULL,

        INDEX mamba_idx_diagnosis_id (diagnosis_id),
        INDEX mamba_idx_patient_id (patient_id),
        INDEX mamba_idx_icd11_diagnosis (diagnosis_non_coded)
    )
        CHARSET = UTF8MB4;

    INSERT INTO diagnosis_etl (
        diagnosis_id,
        encounter_id,
        patient_id,
        diagnosis_non_coded,
        encounter_location,
        diagnosis_state,
        encounter_date
    )
    SELECT 
        ed.diagnosis_id,
        e.encounter_id, 
        ed.patient_id,
        ed.diagnosis_non_coded,
        e.location_id AS encounter_location,
        CASE 
            WHEN da.value_reference = 'R' THEN 'REPEAT' 
            WHEN da.value_reference = 'N' THEN 'NEW' 
            ELSE '' 
        END AS diagnosis_state,
        DATE(e.encounter_datetime) AS encounter_date
    FROM 
        openmrs.encounter_diagnosis ed 
        INNER JOIN openmrs.encounter e ON ed.encounter_id = e.encounter_id 
        INNER JOIN openmrs.diagnosis_attribute da ON da.diagnosis_id = ed.diagnosis_id;
END$$

DELIMITER ;

-- Step 2: Create the Scheduled Event
-- The event will run the stored procedure every hour.

DELIMITER $$

CREATE EVENT IF NOT EXISTS hourly_update_diagnosis_etl
ON SCHEDULE EVERY 1 HOUR
STARTS CURRENT_TIMESTAMP
DO
BEGIN
    CALL update_diagnosis_etl();
END$$

DELIMITER ;

-- Ensure the MySQL event scheduler is enabled by running:
SET GLOBAL event_scheduler = ON;
-- You can check the status of the scheduler with:
SHOW VARIABLES LIKE 'event_scheduler';

-- Step 3: Verify
CALL update_diagnosis_etl();
-- Verify the event exists:
SHOW EVENTS FROM analysis_db;

