SET foreign_key_checks = 0;

-- delete records
delete from reporting_report_request;
update concept_name  set changed_by  =1 where changed_by  >=8;
update concept_name  set creator =1 where creator >=8;
update payment_method  set changed_by  =1 where changed_by  >=8;
update payment_method  set creator =1 where creator >=8;
update payment_method  set retired_by =1 where retired_by >=8;
update location  set changed_by  =1 where changed_by  >=8;
update location  set creator =1 where creator >=8;
update service_provider  set retired_by  =1 where retired_by  >=8;
update service_provider  set changed_by  =1 where changed_by  >=8;
update service_provider  set creator =1 where creator >=8;
delete from visit;
delete from patient;
update users set creator =1 where creator >=8;
delete from relationship ;
delete from provider where person_id >=8;
update payment_method  set changed_by = 1 where changed_by >=8;
delete from registration;
delete from patient_program;
delete from location_attribute;
delete from inv_item_stock;
delete from inv_stockroom;
delete from inv_reserved_transaction;
delete from inv_item_stock_detail;
delete from inv_transaction;
delete from inv_stock_operation_item;
delete from inv_stockroom_operations;
delete from inv_stock_operation;
delete from inv_reserved_transaction;
update inv_item_code set changed_by  = 1 where changed_by  >=8;
update inv_item_code set creator = 1 where creator >=8;
update inv_item set changed_by  = 1 where changed_by  >=8;
update inv_item set creator = 1 where creator >=8;
delete from patient_identifier;
delete from idgen_log_entry;
delete from discussion_participant;
delete from message;
delete from discussion;
update concept_name set changed_by =1 where changed_by >=8;
delete from case_encounter;
delete from appointmentscheduling_appointment_request;
delete from appointmentscheduling_appointment_status_history;
delete from appointmentscheduling_appointment;
delete from appointmentscheduling_time_slot;
delete from appointmentscheduling_block_type_map;
delete from appointmentscheduling_appointment_block;
delete from allergy_reaction;
delete from allergy;
delete from notification_alert_recipient;
delete from notification_alert;
delete from patient_queue;
delete from encounter_diagnosis;
delete from simplelabentry_labtest;
delete from test_order;
delete from drug_order;
delete from referral_order;
delete from orders;
delete from obs;
delete from encounter_provider;
delete from encounter;
delete from patient;
update person set changed_by =1 where changed_by >= 8;
update person set creator =1 where creator >= 8;
delete from person_name where person_id NOT IN (select person_id from users where user_id < 8);
delete from person where person_id NOT IN (select person_id from users where user_id< 8);
update htmlformentry_html_form SET changed_by = 1 where changed_by >=8;
delete from user_property where user_id >=8;
delete from user_role where user_id >=8;
delete from address_hierarchy_address_to_entry_map;
delete from person_address;
delete from conditions;
delete from person_attribute;
delete from users where user_id >= 8;

-- truncate tables to reseed he identity columns
SET foreign_key_checks = 0;
TRUNCATE TABLE reporting_report_request;
update concept_name  set changed_by  =1 where changed_by  >=8;
update concept_name  set creator =1 where creator >=8;
update payment_method  set changed_by  =1 where changed_by  >=8;
update payment_method  set creator =1 where creator >=8;
update payment_method  set retired_by =1 where retired_by >=8;
update location  set changed_by  =1 where changed_by  >=8;
update location  set creator =1 where creator >=8;
update service_provider  set retired_by  =1 where retired_by  >=8;
update service_provider  set changed_by  =1 where changed_by  >=8;
update service_provider  set creator =1 where creator >=8;
TRUNCATE TABLE visit ;
TRUNCATE TABLE patient;
update users set creator =1 where creator >=8;
TRUNCATE TABLE relationship ;
update payment_method  set changed_by = 1 where changed_by >=8;
TRUNCATE TABLE registration;
TRUNCATE TABLE patient_program;
TRUNCATE TABLE location_attribute;
TRUNCATE TABLE inv_item_stock;
TRUNCATE TABLE inv_stockroom;
TRUNCATE TABLE inv_reserved_transaction;
TRUNCATE TABLE inv_item_stock_detail;
TRUNCATE TABLE inv_transaction;
TRUNCATE TABLE inv_stock_operation_item;
TRUNCATE TABLE inv_stockroom_operations;
TRUNCATE TABLE inv_stock_operation;
TRUNCATE TABLE inv_reserved_transaction;
update inv_item_code set changed_by  = 1 where changed_by  >=8;
update inv_item_code set creator = 1 where creator >=8;
update inv_item set changed_by  = 1 where changed_by  >=8;
update inv_item set creator = 1 where creator >=8;
TRUNCATE TABLE patient_identifier;
TRUNCATE TABLE idgen_log_entry;
TRUNCATE TABLE discussion_participant;
TRUNCATE TABLE message;
TRUNCATE TABLE discussion;
update concept_name set changed_by =1 where changed_by >=8;
TRUNCATE TABLE case_encounter;
TRUNCATE TABLE appointmentscheduling_appointment_request;
TRUNCATE TABLE appointmentscheduling_appointment_status_history;
TRUNCATE TABLE appointmentscheduling_appointment;
TRUNCATE TABLE appointmentscheduling_time_slot;
TRUNCATE TABLE appointmentscheduling_block_type_map;
TRUNCATE TABLE appointmentscheduling_appointment_block;
TRUNCATE TABLE allergy_reaction;
TRUNCATE TABLE allergy;
TRUNCATE TABLE notification_alert_recipient;
TRUNCATE TABLE notification_alert;
TRUNCATE TABLE patient_queue;
TRUNCATE TABLE encounter_diagnosis;
TRUNCATE TABLE simplelabentry_labtest;
TRUNCATE TABLE test_order;
TRUNCATE TABLE drug_order;
TRUNCATE TABLE referral_order;
TRUNCATE TABLE orders;
TRUNCATE TABLE obs;
TRUNCATE TABLE encounter_provider;
TRUNCATE TABLE encounter;
TRUNCATE TABLE patient;
update person set changed_by =1 where changed_by >= 8;
update person set creator =1 where creator >= 8;
update htmlformentry_html_form SET changed_by = 1 where changed_by >=8;
TRUNCATE TABLE address_hierarchy_address_to_entry_map;
TRUNCATE TABLE person_address;
TRUNCATE TABLE conditions;
TRUNCATE TABLE person_attribute;
TRUNCATE TABLE fhir_task;
TRUNCATE TABLE fhir_reference;
TRUNCATE TABLE fhir_task_based_on_reference;
TRUNCATE TABLE inv_stock_operation_type;
SET foreign_key_checks = 1;


-- Clear liquibasechangelog
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-41' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-42' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-43' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-32' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-33' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-34' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-35' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-36' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-37' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-38' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-39' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-40' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-23' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-24' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-25' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-26' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-27' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-28' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-29' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-30' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-31' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-13' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-14' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-15' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-16' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-17' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-18' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-19' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-20' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-21' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-22' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-06' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-07' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-08' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-09' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-10' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-11' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-12' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-22' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-23' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-24' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-01' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-02' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-03' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-04' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-roles-01-01-05' AND AUTHOR='reagan-meant' AND FILENAME='liquibase_roles.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-14' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-15' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-16' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-17' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-18' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-19' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-20' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-21' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-05' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-06' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-07' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-08' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-09' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-10' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-11' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-12' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-13' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-01' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-02' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-03' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-09-04' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='update_fhir_diagnostic_report_table_20220511' AND AUTHOR='moses_mutesa' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='drop_fhir_duration_unit_map_20220412' AND AUTHOR='mseaton' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='fix_target_uuid_column' AND AUTHOR='moses_mutesa' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-29' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-30' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-31' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-32' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-33' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-add-stock-room-type-05-09-2022' AND AUTHOR='corneliouzbett' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-add-stock-room-attribute-type-03-09-2022' AND AUTHOR='corneliouzbett' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-add-stock-room-attribute-03-09-2022' AND AUTHOR='corneliouzbett' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-0807B1EE-E28E-4315-8FFE-95BE93DCC10D' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-4F84864C-2612-41FF-B4BA-56F0A442535C' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-535E637B-4C15-40AC-80BC-42C5D07C660C' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-9B33CDB6-8AAF-4BEB-BBCC-8F1B81F26BB4' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-A297D3C0-417E-4157-A7DB-5CEEBBDE4591' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-BB5E353E-7770-4294-A7A2-FD71E64F1431' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-F06C4B69-CEE2-41BE-AAF1-DBC180F719ED' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-19' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-20' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-21' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-22' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-23' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-24' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-25' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-26' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-27' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-28' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-35' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-0E2B0BA6-B348-4EC1-816F-9FED48972DC6' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-18' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-16' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-17' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-14' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-15' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-13' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-11' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-12' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-09' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-10' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-08' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-06' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-07' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-05' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-03' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-04' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-01' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-01-01-02' AND AUTHOR='reagan' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='8a33750a-22ca-11ed-9da1-038b698e19a7' AND AUTHOR='injiri' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='add_drug_extensions_table_10082022' AND AUTHOR='corneliouzbett' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.5.0-3' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.5.0-4' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.7.0-0' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-8' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-9' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.2.0-1' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.2.0-2' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.3.0-1' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.3.0-2' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.3.0-3' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.5.0-1' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.5.0-2' AND AUTHOR='Stephen Kinyori' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-10' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-11' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-1.0.0-3' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-2' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-4' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-5' AND AUTHOR='marios' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-6' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0.0-7' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-v1.0.0-2' AND AUTHOR='reagan-meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='openhmis.inventory-v1.0-1' AND AUTHOR='ibewes' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='botswana.inventory-v1.5.0-2' AND AUTHOR='Reagan Meant' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='create-openelis-pull-task-2020-04-03' AND AUTHOR='pmanko' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='7217fdf0-0dd1-11ed-a2ad-d3543309e9b7' AND AUTHOR='injiri' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='fd334374-0dac-11ed-911c-d3a90f34dd47' AND AUTHOR='injiri' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='0af92648-04fd-41c6-82b7-fb7b70b9c277' AND AUTHOR='gitahi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='6c57b461-4c71-400a-98f8-00da0f91ee39' AND AUTHOR='gitahi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='9a3e079b-0d4e-4312-9dfa-354fce419c45' AND AUTHOR='gitahi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='0BACDE96-B22C-4556-83C7-335360EFA6AE' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='1FCD5FF5-4744-412A-A4EF-685257E0BA47' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='BCB01E2E-4390-42A5-AF31-D6DC03E29317' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='update_content_type_database_type' AND AUTHOR='alalo' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='5BA5BE16-DF7B-4BD5-A06F-0E233BB56361' AND AUTHOR='ningosi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='7B7B9686-CE14-42A5-BF18-10364E580F85' AND AUTHOR='ningosi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='ED76F88E-53E0-4735-BD57-8C334F2D2148' AND AUTHOR='ningosi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='delete-concepts-checksums-20211025' AND AUTHOR='iniz' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='756eb84c-c2fc-11ec-84ab-e38af92b6329-23041457' AND AUTHOR='injiri' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='258D6EE6-6792-44DD-9057-03CB876B4463' AND AUTHOR='moshonk' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='Add-column-encounter-result-to-lab-test-20220328' AND AUTHOR='corneliouzbett' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='3323CFB5-762B-4F95-B1B8-15D91CB27117' AND AUTHOR='ningosi' AND FILENAME='liquibase.xml';
DELETE FROM liquibasechangelog
WHERE ID='F4B79A3E-9A9F-453B-B8C8-F3E796FB8260' AND AUTHOR='ningosi' AND FILENAME='liquibase.xml';


delete from liquibasechangelog where id in ('botswana.inventory-01-01-21', 'botswana.inventory-01-01-22', 'botswana.inventory-01-01-23', 'botswana.inventory-01-01-24', 'botswana.inventory-01-01-25', 'botswana.inventory-01-01-26', 'botswana.inventory-01-01-27', 'botswana.inventory-01-01-35', 'botswana.inventory-19-10-35');

update inv_stock_operation_type set name = 'Requisition' where stock_operation_type_id = 8;


ALTER TABLE fhir_reference ADD CONSTRAINT target_uuid UNIQUE KEY (target_uuid);
